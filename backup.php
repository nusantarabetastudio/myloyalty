<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\FormulaEarn;
use App\Models\Admin\FormulaEarnBlood;
use App\Models\Admin\FormulaEarnCardType;
use App\Models\Admin\FormulaEarnCategory;
use App\Models\Admin\FormulaEarnDepartment;
use App\Models\Admin\FormulaEarnDivision;
use App\Models\Admin\FormulaEarnGender;
use App\Models\Admin\FormulaEarnItems;
use App\Models\Admin\FormulaEarnMember;
use App\Models\Admin\FormulaEarnPayment;
use App\Models\Admin\FormulaEarnReligion;
use App\Models\Admin\FormulaEarnSegment;
use App\Models\Admin\FormulaEarnSubCategory;
use App\Models\Admin\FormulaEarnTenant;
use App\Models\Admin\Post;
use App\Models\Admin\PostTemp;
use App\Models\Admin\PostTempPayment;
use App\Models\Customer;
use App\Models\Master\Merchandise;
use App\Models\Master\Tenant;
use Carbon\Carbon;
use Excel;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Master\Product;
use App\Models\Master\PaymentMethod;

class EarningController extends Controller
{

    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    /**
     * RewardPointController constructor.
     * @param $session
     */
    public function __construct(Session $session)
    {
        $this->parent = 'Earning';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        $this->menu_id = '';
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Upload by CSV';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        return view('earningvoid.index', $data);
    }

    public function result()
    {
        $data['breadcrumbs'] = 'Earning Result';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;

        $data['post'] = Post::all();

        return view('earningvoid.result', $data);
    }

    public function proccess(Request $request)
    {
        // setting up rules
        $rules = [
            'csv_file' => 'required|mimes:csv,txt',
            'payment_file' => 'mimes:csv,txt',
        ];
        // Validate
        $this->validate($request, $rules);

        $destinationPath = 'uploads'; // upload path
        $extension = $request->file('csv_file')->getClientOriginalExtension();
        $extension_payment = $request->file('payment_file')->getClientOriginalExtension();

        if ($extension == 'csv' and $extension_payment = 'csv') {

            $fileName = date('d-m-Y') . '_' . rand(11111, 99999) . '_transaction.' . $extension; // renameing
            $fileName_payment = date('d-m-Y') . '_' . rand(11111, 99999) . '_payment.' . $extension_payment; // renameing

            $request->file('csv_file')->move($destinationPath, $fileName); // uploading file to given path
            // sending back with message

            $request->file('payment_file')->move($destinationPath, $fileName_payment); // uploading file to given path
            // sending back with message

            $ress = Excel::load($destinationPath . '/' . $fileName, function ($reader) {
                $reader->all();
            })->get();

            $ress_payment = Excel::load($destinationPath . '/' . $fileName_payment, function ($reader) {
                $reader->all();
            })->get();

            $this->earningPointPost($ress, $ress_payment);

            Session::flash('success', 'Earning Point Proccess Success');
            $data = array();
            $data['earning'] = $ress;

            return view('earningvoid.data', $data);
        } else {
            Session::flash('error', 'Please Upload CSV file extension for Transaction and Payment');
            return view('earningvoid.index');
        }
    }



    public function earningPointPost($data = array(), $data_payment = array())
    {

        if (!empty($data)) {
            $DOC_NO = 'docs.' . date('Y-m-d H:i:s');

            if (!empty($data_payment)) {

                DB::table('POSTTEMPPAYMENT')->delete();

                foreach ($data_payment as $row_payment) {
                    // Convert all format datetime to timestamp
                    $rcpdate = Carbon::createFromTimestamp(strtotime($row_payment[2 - env('LOCALARRAY', 0)]));
                    // Assign to $row->rcpdate
                    $row_payment[2 - env('LOCALARRAY', 0)] = date('Y-m-d', strtotime($rcpdate->toDateString()));
                    // Convert all format time to timestamp
                    $rcptime = Carbon::createFromTimestamp(strtotime($row_payment[3 - env('LOCALARRAY', 0)]));
                    // Assign to $row->rcptime
                    $row_payment[3 - env('LOCALARRAY', 0)] = date('H:i:s', strtotime($rcptime->toTimeString()));

                    $calcBonus = $this-> _checkBonusPayment($row_payment);

                    $point_bonus = $calcBonus['point'];
                    $multiple = $calcBonus['multiple'];

                    $postTempPayment = new PostTempPayment();
                    $postTempPayment->POSTTEMPPAYMENT_RECID = Uuid::uuid();
                    $postTempPayment->STOREID = $row_payment[1 - env('LOCALARRAY', 0)]; // on windows CSV array start from 1
                    $postTempPayment->RCPDATE = $row_payment[2 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPTIME = $row_payment[3 - env('LOCALARRAY', 0)];
                    $postTempPayment->TERMID = $row_payment[4 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPNBR = $row_payment[5 - env('LOCALARRAY', 0)];
                    $postTempPayment->CARDID = $row_payment[6 - env('LOCALARRAY', 0)];
                    $postTempPayment->TENDNBR = $row_payment[7 - env('LOCALARRAY', 0)];
                    $postTempPayment->ITEMTOTAL = $row_payment[8 - env('LOCALARRAY', 0)];
                    $postTempPayment->BIN = (!empty($row_payment[9 - env('LOCALARRAY', 0)])) ? $row_payment[9 - env('LOCALARRAY', 0)] : '0';
                    $postTempPayment->POINT_BONUS = $point_bonus;
                    $postTempPayment->MULTIPLE = $multiple;
                    $postTempPayment->save();
                }
            }

            DB::table('POSTTEMP')->delete();

            foreach ($data as $row) {
                // Convert all format datetime to timestamp
                $rcpdate = Carbon::createFromTimestamp(strtotime($row[2 - env('LOCALARRAY', 0)]));
                // Assign to $row->rcpdate
                $row[2 - env('LOCALARRAY', 0)] = date('Y-m-d', strtotime($rcpdate->toDateString()));

                // Convert all format time to timestamp
                $rcptime = Carbon::createFromTimestamp(strtotime($row[3 - env('LOCALARRAY', 0)]));
                // Assign to $row->rcptime
                $row[3 - env('LOCALARRAY', 0)] = date('H:i:s', strtotime($rcptime->toTimeString()));

                $calcBonus = $this->_checkBonusPoint($row);

                $point_bonus = $calcBonus['point'];
                $split_bonus = $calcBonus['split'];
                $multiple = $calcBonus['multiple'];

                $postTemp = new PostTemp();
                $postTemp->POSTTEMP_RECID = Uuid::uuid();
                $postTemp->STOREID = $row[1 - env('LOCALARRAY', 0)];
                $postTemp->RCPDATE = $row[2 - env('LOCALARRAY', 0)];
                $postTemp->RCPTIME = $row[3 - env('LOCALARRAY', 0)];
                $postTemp->TERMID = $row[4 - env('LOCALARRAY', 0)];
                $postTemp->CARDID = $row[5 - env('LOCALARRAY', 0)];
                $postTemp->RCPNBR = $row[6 - env('LOCALARRAY', 0)];
                $postTemp->INTCODE = $row[7 - env('LOCALARRAY', 0)];
                $postTemp->ITEMTOTAL = $row[8 - env('LOCALARRAY', 0)];
                $postTemp->ITEMTOTSIGN = $row[9 - env('LOCALARRAY', 0)];
                $postTemp->ITEMQTY = $row[10 - env('LOCALARRAY', 0)];
                $postTemp->DEPTCODE = $row[11 - env('LOCALARRAY', 0)];
                $postTemp->VATCODE = $row[12 - env('LOCALARRAY', 0)];
                $postTemp->DISCOUNT = $row[13 - env('LOCALARRAY', 0)];
                $postTemp->TOTALAFTERDISC = $row[14 - env('LOCALARRAY', 0)];
                $postTemp->ITMIDX = (!empty($row[15 - env('LOCALARRAY', 0)])) ? $row[15 - env('LOCALARRAY', 0)] : 0;
                $postTemp->POINT_BONUS = $point_bonus;
                $postTemp->SPLIT_BONUS = $split_bonus;
                $postTemp->MULTIPLE = $multiple;
                $postTemp->save();
            }

            $newErn = PostTemp::select('CARDID', DB::raw('SUM(TOTALAFTERDISC) as total'), DB::raw('SUM(POINT_BONUS) as total_point_bonus'), DB::raw('SUM(SPLIT_BONUS) as total_split_bonus'))
                ->groupBy('CARDID');

            //  echo '<pre>'; print_r($newErn->toSql()); exit;

            foreach ($newErn->get() as $row) {

                $bonus_combine_fix = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'))
                    ->where('CARDID', $row->CARDID)
                    ->where('MULTIPLE', 0)
                    ->orderBy('point', 'DESC')
                    ->groupBy('INTCODE')
                    ->first();   // for get best point

                $bonus_combine_multiple = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'))
                    ->where('CARDID', $row->CARDID)
                    ->where('MULTIPLE', 1)
                    ->orderBy('point', 'DESC')
                    ->groupBy('INTCODE')
                    ->first();  // for get best point


                $bonus_payment = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                    ->where('CARDID', $row->CARDID)
                    ->orderBy('point', 'DESC')
                    ->first();

                $bonus_combine_fix_point = (isset($bonus_combine_fix->point) ? $bonus_combine_fix->point : 0);
                $bonus_combine_fix_split = (isset($bonus_combine_fix->split) ? $bonus_combine_fix->split : 0);
                $bonus_payment_point = (isset($bonus_payment->point) ? $bonus_payment->point : 0);

                $bonus_combine_multiple_point = (isset($bonus_combine_multiple->point) ? $bonus_combine_multiple->point : 0);
                $bonus_combine_multiple_split = (isset($bonus_combine_multiple->split) ? $bonus_combine_multiple->split : 0);

                $point = $bonus_combine_fix_point + $bonus_combine_multiple_point+$bonus_payment_point;
                $split = $bonus_combine_fix_split + $bonus_combine_multiple_split;

                $customer = Customer::where('CUST_BARCODE', $row->CARDID)->first();
                $postTemp = PostTemp::where('CARDID', $row->CARDID)->first();
                $sumErn = $row->total;

                $sumErn_split = $sumErn - $split;

                $point_regular = $this->_checkRegularPoint($postTemp, $sumErn_split, $customer);

                $point_regular = ($point_regular == null) ? 0 : $point_regular;

                if (env('DEBUG_BONUS', false)) {
                    echo '<hr/>Split : ' . $sumErn_split . '=' . $sumErn . '-' . $row->total_split_bonus;
                    echo '<br/>';
                    echo '<hr/>Point Regular : ' . $point_regular;
                    echo '<br/>';
                }

                $ld = 0;
                $get_reward = $this->_checkRewardPoint($postTemp, $customer);

                if ($get_reward['fixed'] == 1) {
                    $point_reward = $get_reward['point'];
                } else {
                    $point_reward = $point_regular * $get_reward['point']; //multiple
                }

                if (isset($customer->CUST_RECID)) {

                    $tenant = Tenant::where('TNT_CODE', $postTemp->STOREID)->first();

                    // Assign to $row->rcpdate
                    //  $postTemp->RCPDATE = date('Y-m-d', strtotime(str_replace('-', '', $postTemp->RCPDATE)));

                    if (!env('DEBUG_BONUS', false)) {

                        $post = new Post();
                        $post->POS_RECID = Uuid::uuid();
                        $post->POS_CUST_RECID = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_PNUM = substr($postTemp->STOREID, 0, 4);
                        $post->POS_PTYPE = substr($postTemp->STOREID, -2, 2);
                        $post->POS_STORE = $tenant->TNT_RECID;
                        $post->POS_BARCODE = $row->CARDID;
                        $post->POS_POST_DATE = $postTemp->RCPDATE;
                        $post->POS_POST_TIME = $postTemp->RCPTIME;
                        $post->POS_STATION_ID = $postTemp->TERMID;
                        $post->POS_SHIFT_ID = $postTemp->TERMID;
                        $post->POS_DOC_NO = $DOC_NO;
                        $post->POS_CASHIER_ID = $postTemp->TERMID;
                        $post->POS_AMOUNT = $sumErn;
                        $post->POS_POINT_LD = $ld;
                        $post->POS_POINT_REWARD = $point_reward;
                        $post->POS_POINT_REGULAR = $point_regular;
                        $post->POS_POINT_BONUS = $point;
                        $post->POS_RECEIPT_TYPE = '1';
                        $post->POS_USERBY = '-';
                        $post->POS_CREATEBY = '-';
                        $post->POS_UPDATE = 1;
                        $post->POS_TYPE = 0;
                        $post->POS_TRANSF_FROM = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_TRANSF_TO = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_RECEIPT_NO = $postTemp->RCPNBR;
                        $post->TENDNBR = (isset($postTemp->payment->TENDNBR)) ? $postTemp->payment->TENDNBR : 0;
                        $post->BIN = (isset($postTemp->payment->BIN)) ? $postTemp->payment->BIN : 0;
                        $post->save();
                    }
                }
            }

            if (env('DEBUG_BONUS', false)) {
                exit;
            }

            DB::table('POSTTEMP')->delete();
            DB::table('POSTTEMPPAYMENT')->delete();
        }
    }

    public function _checkRegularPoint($row, $sumErn, $customer)
    {
        if (empty($customer)) {
            die('Customer Not Found');
        }

        $point = 0;
        $formula = FormulaEarn::where('ERN_TYPE', 0)
            ->where('ERN_FORMULA', 0) // regular point
            ->where('ERN_FORMULA_DTL', 0)
            ->where('ERN_ACTIVE', 1) // active
            ->where('ERN_VALUE', 0)
            ->where('ERN_FRDATE', '<=', $row->RCPDATE) // from date
            ->where('ERN_TODATE', '>=', $row->RCPDATE); // to date
        // to datetime
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

        $formula_load = $formula->get();

        if (!empty($formula_load)) {

            foreach ($formula_load as $formula_ress) {

                if (!empty($formula_ress) && $formula_ress->ERN_AMOUNT != 0) { // jika dapet point
                    if ($row->RCPDATE == $formula_ress->ERN_FRDATE or $row->RCPDATE == $formula_ress->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME) // from datetime
                        ->where('ERN_TOTIME', '>=', $row->RCPTIME);
                        $formula_ress = $formula->first();
                    }

                    $temp1 = (float) $sumErn;
                    $temp2 = (float) $formula_ress->ERN_AMOUNT;

                    if ($formula_ress->ERN_MULTIPLE_BASIC == 1) { // jika multiple basic ON
                        $pointamount = floor($temp1 / $temp2);
                        // ex : 95.000/50.000 = 1

                        $point = $pointamount * $formula_ress->ERN_POINT;
                        //jika normal point
                    } else {
                        if ($sumErn >= $formula_ress->ERN_AMOUNT) {
                            $point = 1 * $formula_ress->ERN_POINT;
                        } else {
                            $point = 0;
                        }
                    }

                    // Min Max Amount
                    if ($formula_ress->ERN_ACTIVE_MAX == 1) {

                        if ($sumErn < $formula_ress->ERN_MIN_AMOUNT) { //jika Belanja Kurang Dari Minimum Amount
                            $point = 0;
                        }

                        if ($sumErn >= $formula_ress->ERN_MIN_AMOUNT && $sumErn <= $formula_ress->ERN_MAX_AMOUNT) {
                            //jika belanja diantara rentang Min Max
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {
                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;
                            }
                        }

                        if ($sumErn > $formula_ress->ERN_MAX_AMOUNT && $formula_ress->ERN_MAX_AMOUNT != 0) { //jika Belanja Lebih Dari Max Amount
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {

                                $moreErn = $sumErn - $formula_ress->ERN_MAX_AMOUNT;
                                $moreAmount = floor($moreErn / $formula_ress->ERN_AMOUNT);
                                $pointamount = floor($formula_ress->ERN_MAX_AMOUNT / $formula_ress->ERN_AMOUNT);

                                $morePoint = $moreAmount * $formula_ress->ERN_POINT;
                                $point_inmax = ($pointamount * $formula_ress->ERN_POINT) * $formula_ress->ERN_MULTIPLE_VALUE;
                                $point = $point_inmax + $morePoint;
                                //point < Max dikalikan multipler dan yang > max ($morePoint) diberikan normal point kemudian keduanya dijumlahkan
                                //                        echo 'Total Belanja = ' . $sumErn . '<br/>';
                                //                        echo 'Max = ' . $formula_ress->ERN_MAX_AMOUNT . '<br/>';
                                //                        echo 'Min = ' . $formula_ress->ERN_MIN_AMOUNT . '<br/>';
                                //                        echo 'Sisa Total Belanja = ' . $moreAmount . '<br/>';
                                //                        echo 'Point Dalam Max = ' . '(' . $pointamount . '*' . $formula_ress->ERN_POINT . ') *' . $formula_ress->ERN_MULTIPLE_VALUE . ' = ' . $point_inmax . '<br/>';
                                //                        echo 'Point Lebih = ' . $moreAmount . ' * ' . $formula_ress->ERN_POINT . ' = ' . $morePoint . '<br/>';
                                //                        echo 'Total Point = ' . $point;
                                //                        exit;
                            }
                        }
                    }

                    if ($formula_ress->ERN_ACTIVE_MAXAMOUNT == 1) { // jika MAX AMOUNT active
                        if ($formula_ress->ERN_ACTIVE_REST == 1) { // jika REST AMOUNT active
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $temp3 = $sumErn - $formula_ress->ERN_MAXAMOUNT_VALUE; //selisih lebih

                                $pointamount = floor($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2);
                                $point = $pointamount * $formula_ress->ERN_POINT;

                                $pointamountress = floor($temp3 / $formula_ress->ERN_AMOUNT_REST);
                                $pointress = $pointamountress * $formula_ress->ERN_POINT_REST;

                                $point = $point + $pointress; //point normal + point ress
                            }
                        } elseif ($formula_ress->ERN_ACTIVE_REST == 0) { // jika REST AMOUNT inactive
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $pointamount = floor($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2);
                                $point = $pointamount * $formula_ress->ERN_POINT;
                            }
                        }
                    }
                }

                if (isset($customer->CUST_GENDER)) {
                    switch ($customer->CUST_GENDER) {
                        case 'M':
                            $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                            break;
                        case 'F':
                            $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                            break;
                    } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
                }

                $formulaMember = FormulaEarnMember::where('MEM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('MEM_LOK_RECID', '=', $customer->CUST_MEMTYPE)->get();
                $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
                $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
                $formulaCardType = FormulaEarnCardType::where('CDTY_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CDTY_LOK_RECID', '=', $customer->CUST_CARDTYPE)->get();
                //card type di point formula diubah refer ke z_cardtype bukan ke z_lookup kembali
                $formulaCardPayment = FormulaEarnPayment::where('PAY_ERN_RECID', '=', $formula_ress->ERN_RECID)->get();
                //formula ern store belum dibuat model dan table nya
                
                $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();
				$formulaTenant = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();


                if (count($formulaMember) < 1) {
                    $point = 0; //memtype tidak ditemukan
                    // echo 'member type tidak ditemukan <br/>';
                }
                if (count($formulaGender) < 1) {
                    $point = 0; //gender tidak ditemukan
                    //echo 'gender tidak ditemukan'; exit;
                }
                if (count($formulaReligion) < 1) {
                    $point = 0; //gender tidak ditemukan
                    //  echo 'religion tidak ditemukan <br/>';
                }
//        if(count($formulaCardType)<1) {
                //            $point = 0; //gender tidak ditemukan
                //          //  echo 'card type tidak ditemukan <br/>';
                //        }
                if (count($formulaBlood) < 1) {
                    $point = 0; //gender tidak ditemukan
                    //  echo 'blood tidak ditemukan <br/>';
                }

                if (isset($point) && $point < 0) {
                    $point = 0;
                }

                if ($point == null) {
                    $point = 0;
                }
                //echo $pointamount.' - '.$ressamount.' - '.$resspoint.' - '.$point; exit;
                return $point;

            }}
    }


    public function _checkBonusPayment($row)
    {
        $point = 0;
        $split = 0;
        $bonus_type = [
            0 => 'Payment'
        ];

        //dd($row); exit;
        //  echo '<pre>'; print_r($row); exit;

//        $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

        $multiple = 0;

        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA', 1)
                ->where('ERN_FORMULA_DTL', 0)
                ->where('ERN_ACTIVE', 1)
                ->where('ERN_FRDATE', '<=', $row[2 - env('LOCALARRAY', 0)])
                ->where('ERN_TODATE', '>=', $row[2 - env('LOCALARRAY', 0)]);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();

            if (!empty($formula_load)) {

                foreach ($formula_load as $formula_ress) {


                    $check_time = true;

                    if ($row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_FRDATE or $row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row[3 - env('LOCALARRAY', 0)]) // from datetime
                        ->where('ERN_TOTIME', '>=', $row[3 - env('LOCALARRAY', 0)])
                            ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula > first())) ? true : false;
                    }

                    if ($key_bonus_type == 0 and $check_time == true) { // jika payment

                        $payment_method = PaymentMethod::where('code',$row[7 - env('LOCALARRAY', 0)])->first();
                        $formulaItem = FormulaEarnPayment::where('PAY_ERN_RECID',$formula_ress->ERN_RECID)->where('PAY_LOK_RECID',$payment_method['id'])->get();

                    }

                    if (count($formulaItem) > 0) {

                        $multiple = $formula_ress->ERN_MULTIPLE;

                        if ($formula_ress->ERN_VALUE == 0) { // By Amount
                            $pointamount = floor($row[8 - env('LOCALARRAY', 0)] / $formula_ress->ERN_AMOUNT);
                            $point = $pointamount * $formula_ress->ERN_POINT;

                        }


                        if (isset($formula_ress->ERN_MULTIPLE)) {
                            if ($formula_ress->ERN_MULTIPLE == 1) {
                                if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                    $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                }
                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE; // Multiple

                            } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                if (isset($pointamount)) {
                                    $point = $point / $pointamount; // Fix Point
                                }
                            }
                        }

                    }

                }

                if ($point < 0) {
                    $point = 0;
                }


            }

        }

        $return = array('point' => $point,  'multiple' => $multiple);


        return $return;
    }

    public function _checkBonusPoint($row)
    {
        $point = 0;
        $split = 0;
        $bonus_type = [1 => 'Point Earn Bonus by Product',
            2 => 'Point Earn Bonus by Supplier',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment'
        ];

        $multiple = 0;

        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA', 1)
                ->where('ERN_FORMULA_DTL', 0)
                ->where('ERN_ACTIVE', 1)
                ->where('ERN_FRDATE', '<=', $row[2 - env('LOCALARRAY', 0)])
                ->where('ERN_TODATE', '>=', $row[2 - env('LOCALARRAY', 0)]);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();

            if (!empty($formula_load)) {

                foreach ($formula_load as $formula_ress) {

                    if (env('DEBUG_BONUS', FALSE)) {
                        echo 'PF Bonus Recid : ';
                        print_r($formula_ress['ERN_RECID']);
                        echo '<br/>';
                        echo 'Multiple : ';
                        print_r($formula_ress['ERN_MULTIPLE']);
                        echo '<br/>';
                    }


                    $check_time = true;

                    if ($row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_FRDATE or $row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row[3 - env('LOCALARRAY', 0)]) // from datetime
                        ->where('ERN_TOTIME', '>=', $row[3 - env('LOCALARRAY', 0)])
                            ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula > first())) ? true : false;
                    }

                    if ($key_bonus_type == 1 and $check_time == true) { // jika produk
                        //Product
                        $product = Product::select('PRO_CODE','PRO_DESCR', 'PRO_RECID')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                        $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('ITM_PRODUCT', '=', $product['PRO_RECID'])->get();

                    } elseif ($key_bonus_type == 3 and $check_time == true) { // jika departemen

                        $product = Product::select('PRO_CODE','PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                        $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                        $formulaItem = FormulaEarnDepartment::where('DEPT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DEPT_DEPARTMENT', '=', $merchant->department->LOK_RECID)->get();

                    } elseif ($key_bonus_type == 4 and $check_time == true) { // jika Division

                        $product = Product::select('PRO_CODE','PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                        $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                        $formulaItem = FormulaEarnDivision::where('DIV_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DIV_DIVISION', '=', $merchant->division->LOK_RECID)->get();

                    } elseif ($key_bonus_type == 5 and $check_time == true) { // jika Category

                        $product = Product::select('PRO_CODE','PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                        $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                        $formulaItem = FormulaEarnCategory::where('CAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CAT_CATEGORY', '=', $merchant->category->LOK_RECID)->get();

                    } elseif ($key_bonus_type == 6 and $check_time == true) { // jika Sub Category

                        $product = Product::select('PRO_CODE','PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                        $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                        $formulaItem = FormulaEarnSubCategory::where('SCAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SCAT_SUBCATEGORY', '=', $merchant->subcategory->LOK_RECID)->get();

                    } elseif ($key_bonus_type == 7 and $check_time == true) { // jika Segment

                        $product = Product::select('PRO_CODE','PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                        $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                        $formulaItem = FormulaEarnSegment::where('SEG_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SEG_SEGMENT', '=', $merchant->segment->LOK_RECID)->get();

                    }

                    if (count($formulaItem) > 0) {



                        if (env('DEBUG_BONUS', false)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount <br/>' : 'by quantity <br/>';
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;

                        if ($formula_ress->ERN_VALUE == 0) { // By Amount
                            $pointamount = floor($row[14 - env('LOCALARRAY', 0)] / $formula_ress->ERN_AMOUNT);

                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', false)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }
                        }
                        if ($formula_ress->ERN_VALUE == 1) { // By Quantity
                            $row[10 - env('LOCALARRAY', 0)] = $row[10 - env('LOCALARRAY', 0)] / 1000; //Qty dibagi per-1000

                            $pointamount = floor($row[10 - env('LOCALARRAY', 0)] / $formula_ress->ERN_QTY);

                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', false)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                            }
                        }

                        if (isset($formula_ress->ERN_MULTIPLE)) {
                            if ($formula_ress->ERN_MULTIPLE == 1) {
                                if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                    $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                }
                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE; // Multiple
                                $split = $row[14 - env('LOCALARRAY', 0)]; // Total harga per item untuk perhitungan split  pada multiple
                            } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                if (isset($pointamount)) {
                                    $point = $point / $pointamount; // Fix Point
                                }
                            }
                        }

                    }

                }

                if ($point < 0) {
                    $point = 0;
                    $split = 0;
                }
            }

        }

        $return = array('point' => $point, 'split' => $split, 'multiple' => $multiple);

        if (env('DEBUG_BONUS', false)) {
            print_r($return);
            echo ' <br/> <hr/>';
        }

        return $return;
    }

    public function _checkRewardPoint($row, $customer)
    {
        $point = 0;
        $fixed = 0;

        $formula = FormulaEarn::where('ERN_FORMULA', 2)
            ->where('ERN_ACTIVE', 1)
            ->where('ERN_VALUE', 0)
            ->where('ERN_FRDATE', '<=', $row->RCPDATE)
            ->where('ERN_TODATE', '>=', $row->RCPDATE); //ketika belanja masuk priode
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);


        $formula_ress = $formula->first();

        if (!empty($formula_ress)) {

            if ($row->RCPDATE == $formula_ress->ERN_FRDATE) { //jika tanggal masuk di batas awal 
                $formula = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME);
                $formula_ress = $formula->first();
            }
            elseif ($row->RCPDATE == $formula_ress->ERN_TODATE) { //jika tanggal masuk di batas batas akhir
                $formula = $formula->where('ERN_TOTIME', '>=', $row->RCPTIME);
                $formula_ress = $formula->first();
            }

            if (isset($formula_ress->ERN_FIXED)) {
                $fixed = $formula_ress->ERN_FIXED;

                if ($formula_ress->ERN_FORMULA_DTL == 1) {
                    //Member Get Member Reward
                } elseif ($formula_ress->ERN_FORMULA_DTL == 2) {
                    $cust_dob = explode('-', $customer->CUST_DOB);
                    $rcpdate = explode('-', $row->RCPDATE);
                    //Birthday Reward
                    if ($cust_dob[1] == $rcpdate[1]) { // kondisi Reward on same Month
                        $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                    }
                } elseif ($formula_ress->ERN_FORMULA_DTL == 5) {
                    $cust_dob = explode('-', $customer->CUST_DOB);
                    $rcpdate = explode('-', $row->RCPDATE);

                    $bd = $rcpdate[0] . '-' . $cust_dob[1] . '-' . $cust_dob[2];

                    $cust_trans = strtotime($row->RCPDATE); // tanggal transaksi
                    $cust_bd_weekly_start = strtotime($bd); // tanggal awal periode Birthday
                    $cust_bd_weekly_end = strtotime($bd . " +7 days"); // tanggal akhir periode Birthday

                    //range seminggu setelah ulang tahun
                    //Birthday Reward

//                echo "Transaksi : {$row->RCPDATE} <br/>";
                    //                echo "DOB : {$customer->CUST_DOB} <br/>";
                    //                echo "BD : {$bd} <br/>";
                    //
                    //                echo "Time Transaction : {$cust_trans} <br/>";
                    //                echo "Time BD Start : {$cust_bd_weekly_start} <br/>";
                    //                echo "Time BD End : {$cust_bd_weekly_end} <br/>";
                    //

                    if ($cust_trans >= $cust_bd_weekly_start and $cust_trans <= $cust_bd_weekly_end) { // kondisi Reward on same Weekly
                        $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;

//                    echo 'Fixed : '.$formula_ress->ERN_FIXED.'<br/>';
                        //                    echo 'Point : '.$formula_ress->ERN_POINT.'<br/>';
                        //                    echo 'Multiple Value : '.$formula_ress->ERN_MULTIPLE_VALUE.'<br/>';
                        //                    echo $point; exit;
                    }
                } elseif ($formula_ress->ERN_FORMULA_DTL == 6) {
                    $cust_dob = explode('-', $customer->CUST_DOB);
                    $rcpdate = explode('-', $row->RCPDATE);
                    //Birthday Reward

                    if ($cust_dob[1] == $rcpdate[1] and $cust_dob[2] == $rcpdate[2]) { // kondisi Reward on same Daily
                        $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                    }
                } elseif ($formula_ress->ERN_FORMULA_DTL == 3) {
                    //Butler Service Reward
                } elseif ($formula_ress->ERN_FORMULA_DTL == 4) {
                    // Reward New Registration dan Update Profile on Web Site
                } elseif ($formula_ress->ERN_FORMULA_DTL == 7) {
                    // Company Anniversary
                    $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                } elseif ($formula_ress->ERN_FORMULA_DTL == 8) {
                    // Store Anniversary

                    $tenant = Tenant::where('TNT_CODE', $row->STOREID)->first();

                    $found = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                        ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                        ->where('TENT_ACTIVE', 1)->get();

                    if (count($found) > 0) {
                        $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                    }
                }
            }

        }

        if ($point < 0) {
            $point = 0;
        }

        $ress = array('point' => $point, 'fixed' => $fixed);

        return $ress;
    }

    public function _availableDays($date, $id)
    {
        if ($id !== null) {

            $formulaEarn = FormulaEarn::find($id); // Find Formula ID

            $day = Carbon::createFromFormat('Y-m-d', $date); // Parse to Carbon object

            if ($formulaEarn->ERN_SUNDAY == 1 && $day->isSunday()) {
                return true;
            } elseif ($formulaEarn->ERN_MONDAY == 1 && $day->isMonday()) {
                return true;
            } elseif ($formulaEarn->ERN_TUESDAY == 1 && $day->isTuesday()) {
                return true;
            } elseif ($formulaEarn->ERN_WEDNESDAY == 1 && $day->isWednesday()) {
                return true;
            } elseif ($formulaEarn->ERN_THURSDAY == 1 && $day->isThursday()) {
                return true;
            } elseif ($formulaEarn->ERN_FRIDAY == 1 && $day->isFriday()) {
                return true;
            } elseif ($formulaEarn->ERN_SATURDAY == 1 && $day->isSaturday()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}

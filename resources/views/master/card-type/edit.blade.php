@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">

                    <br>
                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-body">
                            <form method="POST" action="{{ route('master.card-type.update', $card_type->CARD_RECID) }}" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Card Type :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="CARD_DESC" required placeholder="Card Type" value="{{ $card_type->CARD_DESC }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Evaluation / Days :</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="CARD_EXPIRED" required min="0" placeholder="30" value="{{ $card_type->CARD_EXPIRED }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Multiplier Point :</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="CARD_POINT" required value="{{ $card_type->CARD_POINT }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Expense :</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="CARD_EXPENSE" required value="{{ $card_type->CARD_EXPENSE }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Target Amount:</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="CARD_TARGET_AMOUNT" required value="{{ $card_type->CARD_TARGET_AMOUNT }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Level Up :</label>
                                        <div class="col-sm-9">
                                            <select name="" class="form-control select2">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Minimum Amount:</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="CARD_MIN_AMOUNT" required value="{{ $card_type->CARD_MIN_AMOUNT }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Level Down :</label>
                                        <div class="col-sm-9">
                                            <select name="" id="" class="form-control select2">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label for="" class="col-sm-3 control-label text-left">Image :  </label>
                                        <div class="col-sm-9">
                                            <input type="file" name="CARD_IMAGE" id="input-file-max-fs" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ ($card_type->CARD_IMAGE) ? $card_type->CARD_IMAGE : url('assets/global/portraits/5.jpg') }}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 text-left control-label">Status:</label>
                                        <div class="col-sm-9">
                                            <input type="checkbox" id="" value="1" class="form-control" name="CARD_ACTIVE" data-plugin="switchery" data-size="small" {{ (isset($card_type) && $card_type->CARD_ACTIVE == 1) || !isset($card_type) ? 'checked' : '' }} {{ isset($card_type) ? '' : 'disabled' }}>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="com-sm-6">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ url('master/card-type/destroy') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('.select2').select2({
                placeholder : 'Choose One'
            });
        })
    </script>

@endsection
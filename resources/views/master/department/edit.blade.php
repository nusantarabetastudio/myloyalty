@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-sm-2">
                    <a href="{{ route('master.department.index') }}" class="btn btn-primary"><i class="icon md-chevron-left" aria-hidden="true"></i> Back</a>
                </div>
            </div>
            <br>

            <div class="row row-lg">
                <div class="col-lg-12">
                    <br>
                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Department</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('master.department.update', $merc->MERC_RECID) }}">
                                {{ csrf_field() }}
                                {!! method_field('PUT') !!}
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-5 control-label text-left">Description :</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="MERC_DEPARTMENT" value="{{ $merc->MERC_DEPARTMENT }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <input type="checkbox" {{ ($merc->MERC_ACTIVE == 1) ? 'checked' : '' }} name="MERC_ACTIVE"> Active
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="pull-left">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
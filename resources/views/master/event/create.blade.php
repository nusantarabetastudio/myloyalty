@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/fonts/font-awesome/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-markdown/bootstrap-markdown.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
@endsection


@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    @if($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <form class="form-horizontal" method="post" action="{{ isset($event) ? route('master.event.update', $event->EVT_RECID) : route('master.event.store') }}" enctype="multipart/form-data">
                        {!! isset($event) ? csrf_field() . method_field('PUT') : csrf_field() !!}

                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label for="" class="col-sm-3 control-label text-left">Description:</label>
                                <div class="col-sm-9">
                                    <input type="text" name="EVT_DESC" class="form-control" value="{{ isset($event) ? $event->EVT_DESC : old('EVT_DESC') }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label text-left">Category :</label>
                                <div class="col-sm-9">
                                    <select class="form-control input-sm select2" name="EVT_CATEGORY">
                                        <option></option>
                                        @foreach($categories as $category)
                                            @if(isset($event) && $event->EVT_CATEGORY == $category->LOK_RECID)
                                                <option value="{{ $category->LOK_RECID }}" selected>{{ $category->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $category->LOK_RECID }}">{{ $category->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label text-left">Start Date :</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control date" name="EVT_FRDATE" value="{{ isset($event) ? $event->EVT_FRDATE->format('d/m/Y') : old('EVT_FRDATE') }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label text-left">End Date :</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control date" name="EVT_TODATE" value="{{ isset($event) ? $event->EVT_TODATE->format('d/m/Y') : old('EVT_TODATE') }}" required>
                                </div>
                            </div>
                            {{-- <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Venue : </label>
                                <div class="col-sm-7">
                                    <select class="form-control input-sm select2" name="EVT_VENUE">
                                        <option></option>
                                        <option {{ isset($event) && $event->EVT_VENUE == 'Venue 1' ? 'selected' : '' }} value="Venue 1">Venue 1</option>
                                        <option {{ isset($event) && $event->EVT_VENUE == 'Venue 2' ? 'selected' : '' }} value="Venue 2">Venue 2</option>
                                        <option {{ isset($event) && $event->EVT_VENUE == 'Venue 3' ? 'selected' : '' }} value="Venue 3">Venue 3</option>
                                    </select>
                                </div>
                            </div> --}}
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label text-left">From:</label>
                                <div class="col-sm-9 form-inline">
                                    <input type="text" name="EVT_FRTIME_H" class="form-control input-sm" min="0" maxlength="2" style="width:55px;" value="{{ isset($event) ? substr($event->EVT_FRTIME, 0,2) : '' }}" required>
                                    <input type="text" name="EVT_FRTIME_M" class="form-control input-sm" min="0" maxlength="2" style="width:55px;" value="{{ isset($event) ? substr($event->EVT_FRTIME, 3,2) : '' }}" required>
                                    <input type="text" name="EVT_FRTIME_S" class="form-control input-sm" min="0" maxlength="2" style="width:55px;" value="{{ isset($event) ? substr($event->EVT_FRTIME, 6,2) : '' }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label text-left">To:</label>
                                <div class="col-sm-9 form-inline">
                                    <input type="text" name="EVT_TOTIME_H" class="form-control input-sm" min="0" maxlength="2" style="width:55px;" value="{{ isset($event) ? substr($event->EVT_TOTIME, 0,2) : '' }}" required>
                                    <input type="text" name="EVT_TOTIME_M" class="form-control input-sm" min="0" maxlength="2" style="width:55px;" value="{{ isset($event) ? substr($event->EVT_TOTIME, 3,2) : '' }}" required>
                                    <input type="text" name="EVT_TOTIME_S" class="form-control input-sm" min="0" maxlength="2" style="width:55px;" value="{{ isset($event) ? substr($event->EVT_TOTIME, 6,2) : '' }}" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label for="active">
                                    <input type="checkbox" name="EVT_ACTIVE" id="active" {{ isset($event) && $event->EVT_ACTIVE == 1 ? 'checked' : '' }}> Active
                                </label>
                            </div>
                        </div>
                        <div class="panel" style="border:1px solid #000;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group form-material-sm">
                                            <div class="col-sm-6">
                                                <div class="col-sm-7">
                                                    <input type="file" id="input-file-max-fs" name="EVT_IMAGE_1" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $event->EVT_IMAGE_1 or ''}}" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="col-sm-7">
                                                    <input type="file" id="input-file-max-fs" name="EVT_IMAGE_2" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $event->EVT_IMAGE_2 or ''}}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label for="synopsis" class="col-sm-1 control-label text-left">Synopsis:</label>
                                <div class="col-sm-11">
                                    <textarea id="synopsis" class="form-control" data-provide="markdown" data-iconlibrary="fa" rows="11" name="EVT_SYNOPSIS">{{ isset($event) ? $event->EVT_SYNOPSIS : old('EVT_SYNOPSIS') }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($promotion) ? 'Update' : 'Submit' }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/marked/marked.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/to-markdown/to-markdown.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
    <script>
        $(function() {
            $('.select2').select2({
                placeholder : 'Choose One'
            });

            $('.date').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            })

        })
    </script>
@endsection


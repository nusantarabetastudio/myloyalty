@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <form class="form-horizontal">
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Vendor Code</label>
                                <div class="col-sm-7">
                                    <input type="text" name="fromDate" value="{{$vendors->VDR_CODE}}" class="form-control input-sm date" required disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Vendor Name</label>
                                <div class="col-sm-7">
                                    <input type="text" name="toDate" value="{{$vendors->VDR_NAME}}" class="form-control input-sm date" required disabled>
                                </div>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <table class="tablesaw table-striped  dataTable table-bordered table-hover" id="datatable-table">
                <thead>
                <tr>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $data)
                	<tr>
                		<td>{{$data->PRO_CODE}}</td>
                		<td>{{$data->PRO_DESCR}}</td>
                		<td>@if($data->PRO_ACTIVE>=1)
                                <i class="icon md-check">
                            @else
                                <i class="icon md-close">
                            @endif
                		</td>
                	</tr>
                @endforeach
                </tbody>
            </table> 
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        var oTable;
        var val =''; // role id
        $(function(){
            oTable = $('#datatable-table').DataTable({
                processing: true
            });
        });
    </script>
@endsection
@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <br>
            <div class="row row-lg">
                <div class="col-sm-2">
                    <a href="{{ url('master/vendor') }}" class="btn btn-primary"><i class="icon md-chevron-left" aria-hidden="true"></i> Back</a>
                </div>
            </div>
            <br>

            <div class="panel" style="border:1px solid #000;">
                <div class="panel-body">
                    <form class="form-horizontal" action="{{ route('master.vendor.update', $vendor->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        @if($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Vendor Code :</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="vdr_code" value="{{ $vendor->VDR_CODE }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Vendor Name :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="vdr_name" value="{{ $vendor->VDR_NAME }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Contact Name:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="vdr_contact" value="{{ $vendor->VDR_CONTACT }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Phone :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="vdr_phone" value="{{ $vendor->VDR_PHONE }}" required>
                                </div>
                            </div>
                            {{--<div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Extension :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="" required>
                                </div>
                            </div>--}}
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Faximile :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="vdr_fax_no" value="{{ $vendor->VDR_FAX_NO }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">E-mail :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="vdr_email" value="{{ $vendor->VDR_EMAIL }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Address :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="vdr_address" value="{{ $vendor->VDR_ADDRESS }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">City :</label>
                                <div class="col-sm-7">
                                    <select class="form-control input-sm" name="vdr_city">
                                        <option value="" selected disabled>Choose one</option>
                                        <option @if($vendor->VDR_CITY == 1) selected @endif value="1">Jakarta</option>
                                        <option @if($vendor->VDR_CITY == 2) selected @endif value="2">Bandung</option>
                                        <option @if($vendor->VDR_CITY == 3) selected @endif value="3">Semarang</option>
                                        <option @if($vendor->VDR_CITY == 4) selected @endif value="4">Malang</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label for="" class="col-sm-2 control-label text-left">Active</label>
                                <div class="col-md-10">
                                    <input type="checkbox" {{ ($vendor->VDR_ACTIVE == 1) ? 'checked' : '' }} name="vdr_active">
                                </div>
                            </div>

                        </div>
                        <div class="">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmCustomer" name="frmCustomer" class="form-horizontal" novalidate="">
                                {{ csrf_field() }}
                                <div class="form-group error">
                                    <label for="inputTask" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="name" name="name" placeholder="Name" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="address" name="address" placeholder="Address" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                            <input type="hidden" id="customer_id" name="customer_id" value="0">
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmCustomer" name="frmCustomer" class="form-horizontal" novalidate="">
                                {{ csrf_field() }}
                                <div class="form-group error">
                                    <label for="inputTask" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="name" name="name" placeholder="Name" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="address" name="address" placeholder="Address" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                            <input type="hidden" id="customer_id" name="customer_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
            <meta name="_token" content="{!! csrf_token() !!}" />
        </div>
    </div>
@endsection
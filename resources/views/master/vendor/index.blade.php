@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <br><br>
            <table class="table table-hover dataTable table-striped width-full" id="d-vendor">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Vendor Code</th>
                    <th>Vendor Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

<!--                 <div class="panel" style="border:1px solid #000;">
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('master.vendor.store') }}" method="POST">
                            {{ csrf_field() }}
                            @if($errors->has())
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            <input type="hidden" name="VDR_PNUM" value="7008">
                            <input type="hidden" name="VDR_PTYPE" value="01">
                            <div class="form-group form-material-sm">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Vendor Code :</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="VDR_CODE" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Vendor Name :</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="VDR_NAME" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-material-sm">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Contact Name:</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="VDR_CONTACT" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Phone :</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="VDR_PHONE" required>
                                    </div>
                                </div>
                                {{--<div class="col-sm-4">
                                    <label class="col-sm-5 control-label text-left">Extension :</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="" required>
                                    </div>
                                </div>--}}
                            </div>
                            <div class="form-group form-material-sm">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Faximile :</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="VDR_FAX_NO" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">E-mail :</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="VDR_EMAIL" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-material-sm">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Address :</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="VDR_ADDRESS" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">City :</label>
                                    <div class="col-sm-7">
                                        <select class="form-control input-sm" name="VDR_CITY">
                                            <option value="" selected disabled>Choose one</option>
                                            <option value="1">Jakarta</option>
                                            <option value="2">Bandung</option>
                                            <option value="3">Semarang</option>
                                            <option value="4">Malang</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            </div>
                        </form>
                    </div>
                </div> -->


<!--                 <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <form action="{{ url('master/vendor/destroy') }}" method="post" role="form">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <input type="hidden" name="id" id="vendor_id">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Information !</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn blue">Delete</button>
                                </div>
                            </form>

                        </div>
                        
                    </div>
                </div> -->
            <meta name="_token" content="{!! csrf_token() !!}" />
        </div>
    </div>
@endsection

@section('scripts')
    <!-- datatables -->
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            var start;
            oTable = $('#d-vendor').DataTable({
                processing: true,
                serverSide: true,
                bSort: false,
                ajax: '{{ url('master/vendor/data') }}',
                columns : [
                    { data: 'VDR_RECID', name: 'VDR_RECID'},
                    { data: 'VDR_CODE', name: 'VDR_CODE'},
                    { data: 'VDR_NAME', name: 'VDR_NAME'},
                    { data: 'VDR_ACTIVE', name: 'VDR_ACTIVE'},
                    { data: 'action', name: 'action'}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#vendor_id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });
        })

    </script>

@endsection
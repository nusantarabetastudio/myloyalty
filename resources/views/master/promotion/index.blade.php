@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <a href="{{ route('master.promotion.create') }}" class="btn btn-primary waves-effect waves-light"><i class="icon md-plus" aria-hidden="true"></i> Add Promotion</a>
                    <br /><br>
                    <table class="table table-hover dataTable table-striped width-full" id="d-promotion">
                        <thead>
                        <tr>
                            <th>Promotions</th>
                            <th>Subtitle</th>
                            <th>Store</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ url('master/promotion/destroy') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#d-promotion').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('master/promotion/data') }}',
                columns : [
                    { data : 'PRM_DESC', name: 'PRM_DESC'},
                    { data : 'PRM_SUBTITLE', name: 'PRM_SUBTITLE'},
                    { data : 'STORE', name: 'STORE'},
                    { data : 'PRM_FRDATE', name: 'PRM_FRDATE'},
                    { data : 'PRM_TODATE', name: 'PRM_TODATE'},
                    { data : 'PRM_ACTIVE', name: 'PRM_ACTIVE'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ]
            });
            $('#delete-modal').on('show.bs.modal', function(event){
                $('#id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });
        })
    </script>
@endsection
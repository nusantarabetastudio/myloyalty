@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
@endsection


@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    @if($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <form class="form-horizontal" method="post" action="{{ isset($promotion) ? route('master.promotion.update', $promotion->PRM_RECID) : route('master.promotion.store') }}" enctype="multipart/form-data">
                        {!! isset($promotion) ? csrf_field() . method_field('PUT') : csrf_field() !!}

                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label text-left">Description :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="PRM_DESC" value="{{ isset($promotion) ? $promotion->PRM_DESC : old('PRM_DESC') }}" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label text-left">Subtitle :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="PRM_SUBTITLE" value="{{ isset($promotion) ? $promotion->PRM_SUBTITLE : old('PRM_SUBTITLE') }}" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Store :</label>
                                <div class="col-sm-7">
                                    <select class="form-control input-sm select2" name="PRM_TENANT">
                                        <option></option>
                                        @foreach($stores as $store)
                                            @if(isset($promotion))
                                                @if($promotion->PRM_TENANT == $store->TNT_RECID)
                                                    <option value="{{ $store->TNT_RECID }}" selected>{{ $store->TNT_CODE . ' - ' . $store->TNT_DESC }}</option>
                                                @else
                                                    <option value="{{ $store->TNT_RECID }}">{{ $store->TNT_CODE . ' - ' . $store->TNT_DESC }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $store->TNT_RECID }}">{{ $store->TNT_CODE . ' - ' . $store->TNT_DESC }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Start Date :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control date" name="PRM_FRDATE" value="{{ isset($promotion) ? $promotion->PRM_FRDATE->format('d/m/Y') : '' }}" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">End Date :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control date" name="PRM_TODATE" value="{{ isset($promotion) ? $promotion->PRM_TODATE->format('d/m/Y') : '' }}" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label text-left">Information :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="PRM_TEXT" value="{{ isset($promotion) ? $promotion->PRM_TEXT : old('PRM_TEXT') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label text-left">Informasi :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="PRM_TEXT_IND" value="{{ isset($promotion) ? $promotion->PRM_TEXT_IND : old('PRM_TEXT_IND') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label text-left">Foot note :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="PRM_FOOT_NOTE" value="{{ isset($promotion) ? $promotion->PRM_FOOT_NOTE : old('PRM_FOOT_NOTE') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Payment Method :</label>
                                <div class="col-sm-7">
                                    <select class="form-control input-sm select2" name="PRM_PAYMENT">
                                        <option></option>
                                        @foreach($payment_methods as $payment)
                                            @if(isset($promotion))
                                                @if($promotion->PRM_PAYMENT == $payment->LOK_RECID)
                                                    <option value="{{ $payment->LOK_RECID }}" selected>{{ $payment->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $payment->LOK_RECID }}">{{ $payment->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $payment->LOK_RECID }}">{{ $payment->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-12 col-sm-offset-2">
                                <input type="checkbox" name="PRM_ACTIVE" {{ isset($promotion) && $promotion->PRM_ACTIVE == 1 ?  'checked'  : '' }}> Active
                            </div>
                        </div>

                        <div class="panel" style="border:1px solid #000;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group form-material-sm">
                                            <div class="col-sm-6">
                                                <div class="col-sm-7">
                                                    <input type="file" id="input-file-max-fs" name="PRM_IMAGE_1" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $promotion->PRM_IMAGE_1 or ''}}" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="col-sm-7">
                                                    <input type="file" id="input-file-max-fs" name="PRM_IMAGE_2" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $promotion->PRM_IMAGE_2 or ''}}" /> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($promotion) ? 'Update' : 'Submit' }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
    <script>
        $(function() {
            $('.select2').select2({
                placeholder : 'Choose One'
            });

            $('.date').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            })
        })
    </script>
@endsection


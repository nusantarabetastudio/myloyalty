@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
@endsection
@section('content')
	<div class="panel">
		<div class="panel-body">
			<div class="row row-lg">
				<div class="col-lg-12">
				<form class="form-horizontal">
	                <div class="form-group form-material-sm">
	                    <div class="col-sm-6">
	                        <label class="col-sm-4 control-label text-left">Store Code :</label>
	                        <div class="col-sm-6">
	                            <input type="text" class="form-control" name="TNT_CODE" value="{{ isset($store) ? $store->TNT_CODE : old('TNT_CODE') }}" disabled>
	                        </div>
	                    </div>
	                    <div class="col-sm-6">
	                        <label class="col-sm-5 control-label text-left">Store Name :</label>
	                        <div class="col-sm-7">
	                            <input type="text" class="form-control" name="TNT_DESC" value="{{ isset($store) ? $store->TNT_DESC : old('TNT_DESC') }}" disabled>
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group form-material-sm">
	                    <div class="col-sm-6">
	                        <label class="col-sm-4 control-label text-left">Phone :</label>
	                        <div class="col-sm-6">
	                            <input type="text" class="form-control" name="TNT_PHONE" value="{{ isset($store) ? $store->TNT_PHONE : old('TNT_PHONE') }}" disabled>
	                        </div>
	                    </div>
	                    <div class="col-sm-6">
	                        <label class="col-sm-5 control-label text-left">Contact Name :</label>
	                        <div class="col-sm-7">
	                            <input type="text" class="form-control" name="TNT_CONTACT" value="{{ isset($store) ? $store->TNT_CONTACT : old('TNT_CONTACT') }}" disabled>
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group form-material-sm">
	                    <div class="col-sm-6">
	                        <label class="col-sm-4 control-label text-left">Email :</label>
	                        <div class="col-sm-6">
	                            <input type="text" class="form-control" name="TNT_EMAIL" value="{{ isset($store) ? $store->TNT_EMAIL : old('TNT_EMAIL') }}" disabled>
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group form-material-sm">
	                    <div class="col-sm-6">
	                        <label class="col-sm-4 control-label text-left">Format :</label>
	                        <div class="col-sm-6">
	                            <select class="form-control input-sm select2" name="TNT_CATEGORY" disabled>
	                                <option value=""></option>
	                                @foreach($tenant_categories as $tenant_category)
	                                    @if(isset($store) && $store->TNT_CATEGORY == $tenant_category->LOK_RECID)
	                                        <option value="{{ $tenant_category->LOK_RECID }}" selected>{{ $tenant_category->LOK_DESCRIPTION }}</option>
	                                    @else
	                                        <option value="{{ $tenant_category->LOK_RECID }}">{{ $tenant_category->LOK_DESCRIPTION }}</option>
	                                    @endif
	                                @endforeach
	                            </select>
	                        </div>
	                    </div>
	                    <div class="col-sm-6">
	                        <label class="col-sm-5 control-label text-left">Address :</label>
	                        <div class="col-sm-7">
	                            <input type="text" class="form-control" name="TNT_UNIT" value="{{ isset($store) ? $store->TNT_UNIT : old('TNT_UNIT') }}" disabled>
	                        </div>
	                    </div>
	                </div>
	                <div class="form-group form-material-sm">
	                    <div class="col-sm-6">
	                        <label class="col-sm-4 control-label text-left">Latitude :</label>
	                        <div class="col-sm-6">
	                            <input type="text" class="form-control" name="TNT_LATITUDE" value="{{ isset($store) ? $store->TNT_LATITUDE : old('TNT_LATITUDE') }}" disabled>
	                        </div>
	                    </div>
	                    <div class="col-sm-6">
	                        <label class="col-sm-5 control-label text-left">Langitude :</label>
	                        <div class="col-sm-7">
	                            <input type="text" class="form-control" name="TNT_LONGITUDE" value="{{ isset($store) ? $store->TNT_LONGITUDE : old('TNT_LONGITUDE') }}" disabled>
	                        </div>
	                    </div>
	                </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Country :</label>
                                <div class="col-sm-6">
                                    <select name="TNT_COUNTRY" class="form-control input-sm select2" id="country" disabled>
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                            @if($country->LOK_DESCRIPTION == 'INDONESIA')
                                                <option value="{{ $country->LOK_RECID }}" selected>{{ $country->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $country->LOK_RECID }}" {{ old('TNT_COUNTRY') == $country->LOK_RECID ? 'selected' : '' }}>{{ $country->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Province :</label>
                                <div class="col-sm-7">
                                    <select name="TNT_PROVINCE" class="form-control input-sm select2" id="province" disabled>
                                        <option value=""></option>
                                        @if(isset($store))
                                            @foreach($provinces as $province)
                                                @if(isset($store) && $province->LOK_RECID == $store->TNT_PROVINCE)
                                                    <option value="{{ $province->LOK_RECID }}" class="province-data" selected>{{ $province->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $province->LOK_RECID }}" class="province-data">{{ $province->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">City :</label>
                                <div class="col-sm-6">
                                    <select name="TNT_CITY" class="form-control input-sm select2" id="city" disabled>
                                        <option value=""></option>
                                        @if(isset($store))
                                            @foreach($cities as $city)
                                                @if(isset($store) && $city->LOK_RECID == $store->TNT_CITY)
                                                    <option value="{{ $city->LOK_RECID }}" class="city-data" selected>{{ $city->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $city->LOK_RECID }}" class="city-data">{{ $city->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Area :</label>
                                <div class="col-sm-7">
                                    <select name="TNT_AREA" class="form-control input-sm select2" id="area" disabled>
                                        <option value=""></option>
                                        @foreach($areas as $area)
                                            @if(isset($store) && $area->LOK_RECID == $store->TNT_AREA)
                                                <option value="{{ $area->LOK_RECID }}" selected>{{ $area->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $area->LOK_RECID }}">{{ $area->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
	                <div class="form-group form-material-sm">
	                    <div class="col-sm-12 col-sm-offset-2">
	                            <input type="checkbox" disabled name="TNT_ACTIVE" {{ isset($store) && $store->TNT_ACTIVE == 1 ?  'checked'  : '' }}> Active
	                    </div>
	                </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                
                            </div>
                            <div class="col-lg-10">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <div class="col-sm-7">
                                            <input type="file" id="input-file-max-fs" name="TNT_IMAGE_URL_1" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $store->TNT_IMAGE_URL_1 or ''}}" disabled />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-7">
                                            <input type="file" id="input-file-max-fs" name="TNT_IMAGE_URL_2" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $store->TNT_IMAGE_URL_2 or ''}}" disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
				</form>

				</div>				
			</div>
		</div>
	</div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
@endsection
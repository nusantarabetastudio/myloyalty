@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
@endsection


@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    @if($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <form class="form-horizontal" method="post" action="{{ isset($store) ? route('master.store.update', $store->TNT_RECID) : route('master.store.store') }}" enctype="multipart/form-data">
                        {!! isset($store) ? csrf_field() . method_field('PUT') : csrf_field() !!}
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Store Code :</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="TNT_CODE" value="{{ isset($store) ? $store->TNT_CODE : old('TNT_CODE') }}" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Store Name :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="TNT_DESC" value="{{ isset($store) ? $store->TNT_DESC : old('TNT_DESC') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Phone :</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="TNT_PHONE" value="{{ isset($store) ? $store->TNT_PHONE : old('TNT_PHONE') }}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Contact Name :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="TNT_CONTACT" value="{{ isset($store) ? $store->TNT_CONTACT : old('TNT_CONTACT') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Email :</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="TNT_EMAIL" value="{{ isset($store) ? $store->TNT_EMAIL : old('TNT_EMAIL') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Format :</label>
                                <div class="col-sm-6">
                                    <select class="form-control input-sm select2" name="TNT_CATEGORY" required>
                                        <option value=""></option>
                                        @foreach($tenant_categories as $tenant_category)
                                            @if(isset($store) && $store->TNT_CATEGORY == $tenant_category->LOK_RECID)
                                                <option value="{{ $tenant_category->LOK_RECID }}" selected>{{ $tenant_category->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $tenant_category->LOK_RECID }}">{{ $tenant_category->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Address :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="TNT_UNIT" value="{{ isset($store) ? $store->TNT_UNIT : old('TNT_UNIT') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Latitude :</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="TNT_LATITUDE" value="{{ isset($store) ? $store->TNT_LATITUDE : old('TNT_LATITUDE') }}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Langitude :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="TNT_LONGITUDE" value="{{ isset($store) ? $store->TNT_LONGITUDE : old('TNT_LONGITUDE') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">Country :</label>
                                <div class="col-sm-6">
                                    <select name="TNT_COUNTRY" class="form-control input-sm select2" id="country">
                                        <option value=""></option>
                                        @foreach($countries as $country)
                                            @if($country->LOK_DESCRIPTION == 'INDONESIA')
                                                <option value="{{ $country->LOK_RECID }}" selected>{{ $country->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $country->LOK_RECID }}" {{ old('TNT_COUNTRY') == $country->LOK_RECID ? 'selected' : '' }}>{{ $country->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Province :</label>
                                <div class="col-sm-7">
                                    <select name="TNT_PROVINCE" class="form-control input-sm select2" id="province" required>
                                        <option value=""></option>
                                        @if(isset($store))
                                            @foreach($provinces as $province)
                                                @if(isset($store) && $province->LOK_RECID == $store->TNT_PROVINCE)
                                                    <option value="{{ $province->LOK_RECID }}" class="province-data" selected>{{ $province->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $province->LOK_RECID }}" class="province-data">{{ $province->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-4 control-label text-left">City :</label>
                                <div class="col-sm-6">
                                    <select name="TNT_CITY" class="form-control input-sm select2" id="city">
                                        <option value=""></option>
                                        @if(isset($store))
                                            @foreach($cities as $city)
                                                @if(isset($store) && $city->LOK_RECID == $store->TNT_CITY)
                                                    <option value="{{ $city->LOK_RECID }}" class="city-data" selected>{{ $city->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $city->LOK_RECID }}" class="city-data">{{ $city->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Area :</label>
                                <div class="col-sm-7">
                                    <select name="TNT_AREA" class="form-control input-sm select2" id="area">
                                        <option value=""></option>
                                        @foreach($areas as $area)
                                            @if(isset($store) && $area->LOK_RECID == $store->TNT_AREA)
                                                <option value="{{ $area->LOK_RECID }}" selected>{{ $area->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $area->LOK_RECID }}">{{ $area->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-12 col-sm-offset-2">
                                    <input type="checkbox" name="TNT_ACTIVE" {{ isset($store) && $store->TNT_ACTIVE == 1 ?  'checked'  : '' }}> Active
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                
                            </div>
                            <div class="col-lg-10">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <div class="col-sm-7">
                                            <input type="file" id="input-file-max-fs" name="TNT_IMAGE_URL_1" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $store->TNT_IMAGE_URL_1 or ''}}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-7">
                                            <input type="file" id="input-file-max-fs" name="TNT_IMAGE_URL_2" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $store->TNT_IMAGE_URL_2 or ''}}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($store) ? 'Update' : 'Submit' }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
    <script>
        $(function() {
            $('.select2').select2({
                placeholder : 'Choose One'
            });
            
            $('#country').on('change', function() {
                var val = $(this).val();
                $.ajax({
                    url: '{{ url('/api/v1/country/') }}/' + val + '/province',
                    success: function(data) {
                        $('.province-data').remove();
                        $.each(data.data, function(i, data) {
                            $('#province').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('province-data'));
                        });
                        $('#province').select2({
                            'placeholder' : 'Choose One',
                            'allowClear' : true
                        }).change();
                    }
                });
            });

            @if (!isset($store))
            $('#country').change();
            @endif
            $('#province').on('change', function() {
                var val = $(this).val();
                if (val) {
                    $.ajax({
                        url: '{{ url('/api/v1/province/') }}/' + val + '/city',
                        success: function(data) {
                            $('.city-data').remove();
                            $.each(data.data, function(i, data) {
                                $('#city').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('city-data'));
                            });
                            $('#city').select2({
                                'placeholder' : 'Choose One',
                                'allowClear' : true
                            }).change();
                        }
                    });
                } else {
                    $('.city-data').remove();
                    $('#city').select2({
                        'placeholder' : 'Choose One',
                        'allowClear' : true
                    });
                }
            });
            $('#city').on('change', function() {
                var val = $(this).val();
                if (val) {
                    $.ajax({
                        url: '{{ url('/api/v1/city/') }}/' + val + '/area',
                        success: function(data) {
                            $('.area-data').remove();
                            $.each(data.data, function(i, data) {
                                $('#area').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('area-data'));
                            });
                            $('#area').select2({
                                'placeholder' : 'Choose One',
                                'allowClear' : true
                            }).change();
                        }
                    });
                } else {
                    $('.area-data').remove();
                    $('#area').select2({
                        'placeholder' : 'Choose One',
                        'allowClear' : true
                    });
                }
            });
        })
    </script>
@endsection


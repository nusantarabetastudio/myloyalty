@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection

@section('content')
    <!-- Panel Inline Form -->
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <br/>
                    <table class="table table-hover dataTable table-striped width-full" id="d-city">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>City</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>

                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-body">
                            <form method="POST" action="{{ route('master.city.store') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="control-label">Province :</label>
                                            <select name="ADDR_CITY" class="form-control input-sm select2">
                                                @foreach($provinces as $province)
                                                        <option value="{{ $province->LOK_RECID }}" {{ old('ADDR_COUNTRY') == $province->LOK_RECID ? 'selected' : '' }}>{{ $province->LOK_DESCRIPTION }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">City :</label>
                                        <input type="text" class="form-control" name="LOK_DESCRIPTION" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                            <input type="checkbox" name="LOK_ACTIVE" checked> Active
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="com-sm-6">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ url('master/city/destroy') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            var start;
            oTable = $('#d-city').DataTable({
                ajax: '{{ url('master/city/data') }}',
                columns : [
                    { data : 'LOK_MAPPING', name: 'LOK_MAPPING'},
                    { data : 'LOK_DESCRIPTION', name: 'LOK_DESCRIPTION'},
                    { data : 'LOK_ACTIVE', name: 'LOK_ACTIVE'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ]
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });

            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });
        })
    </script>

@endsection
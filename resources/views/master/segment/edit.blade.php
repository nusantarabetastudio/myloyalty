@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection


@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <br />
                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sub Category</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('master.segment.update', $merc->MERC_RECID) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" name="MERC_CODE" value="{{ getSegment($merc->MERC_CODE) }}">
                                <input type="hidden" name="MERC_DEPARTMENT" id="department_name" value="{{ $merc->MERC_DEPARTMENT }}">
                                <input type="hidden" name="MERC_DIVISION" id="division_name" value="{{ $merc->MERC_DIVISION }}">
                                <input type="hidden" name="MERC_CATEGORY" id="category_name" value="{{ $merc->MERC_CATEGORY }}">
                                <input type="hidden" name="MERC_SUBCATEGORY" id="subcategory_name" value="{{ $merc->MERC_SUBCATEGORY }}">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-5 control-label text-left">Department :</label>
                                        <div class="col-sm-7">
                                            <select class="form-control input-sm select2" name="MERC_DEPARTMENT_CODE" id="department_id">
                                                <option value="" selected disabled>Select Department</option>
                                                @foreach($departments as $department)
                                                    @if($department->MERC_DEPARTMENT == $merc->MERC_DEPARTMENT)
                                                        <option value="{{ $department->MERC_CODE }}" selected>{{ $department->MERC_DEPARTMENT }}</option>
                                                    @else
                                                        <option value="{{ $department->MERC_CODE }}">{{ $department->MERC_DEPARTMENT }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="division_id" class="col-sm-3 control-label text-left">Division :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control input-sm select2" name="MERC_DIVISION_CODE" id="division_id">
                                                <option value="" selected disabled>Select Division</option>
                                                @foreach($divisions as $division)
                                                    @if($division->MERC_DIVISION == $merc->MERC_DIVISION)
                                                        <option value="{{ $division->MERC_CODE }}" selected>{{ $division->MERC_DIVISION }}</option>
                                                    @else
                                                        <option value="{{ $division->MERC_CODE }}">{{ $division->MERC_DIVISION }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label for="category_id" class="col-sm-5 control-label text-left">Category</label>
                                        <div class="col-sm-7">
                                            <select class="form-control input-sm select2" name="MERC_CATEGORY_CODE" id="category_id">
                                                <option value="" selected disabled>Select Division</option>
                                                @foreach($categories as $category)
                                                    @if($category->MERC_CATEGORY == $merc->MERC_CATEGORY)
                                                        <option value="{{ $category->MERC_CODE }}" selected>{{ $category->MERC_CATEGORY }}</option>
                                                    @else
                                                        <option value="{{ $category->MERC_CODE }}">{{ $category->MERC_CATEGORY }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="" class="col-sm-3 control-label text-left">Sub Category</label>
                                        <div class="col-sm-9">
                                            <select class="form-control input-sm select2" name="MERC_SUBCATEGORY_CODE" id="subcategory_id">
                                                <option value="" selected disabled>Select Sub Category</option>
                                                @foreach($subcategories as $subcategory)
                                                    @if($subcategory->MERC_SUBCATEGORY == $merc->MERC_SUBCATEGORY)
                                                        <option value="{{ $subcategory->MERC_CODE }}" selected>{{ $subcategory->MERC_SUBCATEGORY }}</option>
                                                    @else
                                                        <option value="{{ $subcategory->MERC_CODE }}">{{ $subcategory->MERC_SUBCATEGORY }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-5 control-label text-left">Description :</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="MERC_SEGMENT" value="{{ $merc->MERC_SEGMENT }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <input type="checkbox" name="MERC_ACTIVE" {{ ($merc->MERC_ACTIVE == 1) ? 'checked' : '' }}> Active
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="pull-left">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {

            $('#department_id').change(function(e) {
                e.preventDefault();
                $('#department_name').val($("#department_id option:selected").text());
            });

            $('#division_id').change(function(e) {
                e.preventDefault();
                $('#division_name').val($("#division_id option:selected").text());
            });

            $('#category_id').change(function(e) {
                e.preventDefault();
                $('#category_name').val($("#category_id option:selected").text());
            });

            $('#subcategory_id').change(function(e) {
                e.preventDefault();
                $('#subcategory_name').val($("#subcategory_id option:selected").text());
            });

            $('.select2').select2({
                placeholder : 'Choose One'
            });
        })

    </script>
@endsection
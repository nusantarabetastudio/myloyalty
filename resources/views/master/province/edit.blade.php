@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection

@section('content')
    <!-- Panel Inline Form -->
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <br/>
                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-body">
                            <form method="POST" action="{{ route('master.province.update', $lookup->LOK_RECID) }}" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Country :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control select2" name="country_id">
                                                @foreach ($countries as $country)
                                                <option value="{{ $country->LOK_RECID }}" {{ $lookup->LOK_CODEMST == $country->LOK_RECID ? 'selected' : '' }}>{{ $country->LOK_DESCRIPTION }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Province :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="LOK_DESCRIPTION" value="{{ $lookup->LOK_DESCRIPTION }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <input type="checkbox" name="LOK_ACTIVE" {{ $lookup->LOK_ACTIVE == 1 ? 'checked' : '' }} > Active
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="com-sm-6">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->


@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        var oTable;
        $(function () {
            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });
        })
    </script>

@endsection
@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-sm-2">
                    <a href="{{ route('master.division.index') }}" class="btn btn-primary"><i class="icon md-chevron-left" aria-hidden="true"></i> Back</a>
                </div>
                <div class="col-lg-12">
                    <br>
                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Division</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('master.division.update', $merc->MERC_RECID) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" name="MERC_CODE" value="{{ getDivision($merc->MERC_CODE) }}">
                                <input type="hidden" name="MERC_DEPARTMENT" id="department_name" value="{{ $merc->MERC_DEPARTMENT }}">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-5 control-label text-left">Department :</label>
                                        <div class="col-sm-7">
                                            <select class="form-control input-sm select2" name="MERC_DEPARTMENT_CODE" id="department_id">
                                                <option value="" selected disabled>Select Department</option>
                                                @foreach($departments as $department)
                                                    @if($department->MERC_DEPARTMENT == $merc->MERC_DEPARTMENT)
                                                        <option value="{{ $department->MERC_CODE }}" selected>{{ $department->MERC_DEPARTMENT }}</option>
                                                    @else
                                                        <option value="{{ $department->MERC_CODE }}">{{ $department->MERC_DEPARTMENT }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-5 control-label text-left">Description :</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="MERC_DIVISION" value="{{ $merc->MERC_DIVISION }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <input type="checkbox" name="MERC_ACTIVE" {{ ($merc->MERC_ACTIVE == 1) ? 'checked' : '' }}> Active
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="pull-left">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            $('#department_id').change(function(e) {
                e.preventDefault();
                $('#department_name').val($("#department_id option:selected").text());
            });

            $('.select2').select2({
                placeholder : 'Choose One'
            });
        })
    </script>
@endsection
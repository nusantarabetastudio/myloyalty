@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection
@section('content')
    <!-- Panel Inline Form -->
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="panel" style="border:1px solid #000; display: {{ isset($ciity) ? 'block' : 'none' }} ;">
                        <div class="panel-body">
                            <form method="POST" action="{{ route('master.area.update', $lookup->LOK_RECID) }}" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Area :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="LOK_DESCRIPTION" value="{{ $lookup->LOK_DESCRIPTION }}" required>
                                        </div>
                                    </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">City :</label>
                                        <div class="col-sm-9">
                                            <select name="LOK_CODEMST" class="form-control input-sm select2" id="LOK_CODEMST" required>
                                                <option value=""></option>
                                                @foreach($citys as $city)
                                                @if (isset($ciity) && $ciity->LOK_CODEMST == $city->LOK_CODEMST)
                                                    <option value="{{ $city->LOK_RECID }}" selected>{{ $ciity->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $city->LOK_RECID }}">{{ $city->LOK_DESCRIPTION }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <input type="checkbox" name="LOK_ACTIVE" {{ $lookup->LOK_ACTIVE == 1 ? 'checked' : '' }}> Active
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="com-sm-6">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });
        })
    </script>

@endsection
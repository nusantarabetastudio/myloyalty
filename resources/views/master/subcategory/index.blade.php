@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection


@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <br />
                    <table class="table table-hover dataTable table-striped width-full" id="d-subcategory">
                        <thead>
                        <tr>
                            <th>No. </th>
                            <th>Subcategory Code</th>
                            <th>Department Name</th>
                            <th>Division Name</th>
                            <th>Category Name</th>
                            <th>Subcategory Name</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <br>
                    {{-- <div class="panel" style="border:1px solid #000;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sub Category</h3>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('master.subcategory.store') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="MERC_CODE" value="{{ checkCode2($code) }}">
                                <input type="hidden" name="MERC_DEPARTMENT" id="department_name" value="">
                                <input type="hidden" name="MERC_DIVISION" id="division_name" value="">
                                <input type="hidden" name="MERC_CATEGORY" id="category_name" value="">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-5 control-label text-left">Department :</label>
                                        <div class="col-sm-7">
                                            <select class="form-control input-sm select2" name="MERC_DEPARTMENT_CODE" id="department_id">
                                                <option value="" selected disabled>Select Department</option>
                                                @foreach($departments as $department)
                                                    <option value="{{ $department->MERC_CODE }}">{{ $department->MERC_DEPARTMENT }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">Division :</label>
                                        <div class="col-sm-9">
                                            <select class="form-control input-sm select2" name="MERC_DIVISION_CODE" id="division_id">
                                                <option value="" selected disabled>Select Division</option>
                                                @foreach($divisions as $division)
                                                    <option value="{{ $division->MERC_CODE }}">{{ $division->MERC_DIVISION }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label for="" class="col-sm-5 control-label text-left">Category</label>
                                        <div class="col-sm-7">
                                            <select class="form-control input-sm select2" name="MERC_CATEGORY_CODE" id="category_id">
                                                <option value="" selected disabled>Select Category</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->MERC_CODE }}">{{ $category->MERC_CATEGORY }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-5 control-label text-left">Description :</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="MERC_SUBCATEGORY" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <input type="checkbox" name="MERC_ACTIVE" checked> Active
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="pull-left">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ url('master/subcategory/destroy') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="subcategory_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>


    <script type="text/javascript">
        var oTable;
        $(function () {
            var start;
            oTable = $('#d-subcategory').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('master/subcategory/data') }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data: 'LOK_RECID', name: 'LOK_RECID', searchable: false, orderable: false},
                    { data : 'LOK_MAPPING', name: 'LOK_MAPPING'},
                    { data: function(data) {
                        return (data && data.category && data.category.division && data.category.division.department) ? data.category.division.department.LOK_DESCRIPTION : '-'}, name: 'department', searchable: false, orderable: false
                    },
                    { data: function(data) {
                        return (data && data.category && data.category.division) 
                            ? data.category.division.LOK_DESCRIPTION 
                            : '-'
                        }, name: 'division', searchable: false, orderable: false
                    },
                    { data: function(data) {
                        return (data && data.category) 
                            ? data.category.LOK_DESCRIPTION 
                            : '-'
                        }, name: 'category.LOK_DESCRIPTION', searchable: false, orderable: false
                    },
                    { data: 'LOK_DESCRIPTION', name: 'LOK_DESCRIPTION'},
                    { data: 'LOK_ACTIVE', name: 'LOK_ACTIVE', searchable: false, orderable: false}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#subcategory_id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });

            $('#department_id').change(function(e) {
                e.preventDefault();
                $('#department_name').val($("#department_id option:selected").text());
            });
            $('#division_id').change(function(e) {
                e.preventDefault();
                $('#division_name').val($("#division_id option:selected").text());
            });
            $('#category_id').change(function(e) {
                e.preventDefault();
                $('#category_name').val($("#category_id option:selected").text());
            });
            $('.select2').select2({
                placeholder : 'Choose One'
            });
        })

    </script>

@endsection
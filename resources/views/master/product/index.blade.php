@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <table class="table table-hover dataTable table-striped width-full" id="d-product">
                        <thead>
                        <tr>
                            <th>Article Number</th>
                            <th>Article Name</th>
                            <th>Unit</th>
                            <th>Brand Code</th>
                            <th>Brand Name</th>
                            <th>MC Code</th>
                            <th>MC Name</th>
                            <th>Active</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ url('master/product/destroy') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#d-product').DataTable({
                processing: true,
                serverSide: true,
                bSort: false,
                ajax: {
                    url: '{{ url('master/product/data') }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data : 'PRO_CODE', name: 'PRO_CODE'},
                    { data : 'PRO_DESCR', name: 'PRO_DESCR'},
                    { data : 'unit', name: 'unit.LOK_DESCRIPTION', searchable: false},
                    { data : function(data) {
                            return (data && data.brand) ? data.brand.code : '-'
                        }, name: 'brand.code', searchable: true, orderable: false
                    },
                    { data : function(data) {
                        return (data && data.brand) ? data.brand.name : '-'
                        }, name: 'brand.name', searchable: true, orderable: false
                    },
                    { data : 'MERC_CODE', name: 'merchandise.MERC_CODE', searchable: true},
                    { data : 'LOK_DESCRIPTION', name: 'merchandise.segment.LOK_DESCRIPTION', searchable: true, orderable: false},
                    { data : 'PRO_ACTIVE', name: 'PRO_ACTIVE', searchable: false, orderable: false}
                ]
            });
            $('#delete-modal').on('show.bs.modal', function(event){
                $('#id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });
        })
    </script>
@endsection
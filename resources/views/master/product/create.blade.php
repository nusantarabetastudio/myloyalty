@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection


@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    @if($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <form class="form-horizontal" method="post" action="{{ isset($product) ? route('master.product.update', $product->PRO_RECID) : route('master.product.store') }}" enctype="multipart/form-data">
                        {!! isset($product) ? csrf_field() . method_field('PUT') : csrf_field() !!}
                       {{-- <div class="form-group">
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Department</label>
                                <div class="col-sm-7">
                                    <select name="PRO_MERCHANDISE" class="form-control input-sm select2">
                                        <option></option>
                                        @foreach($departments as $department)
                                            @if(isset($product) && $product->PRO_MERCHANDISE == $department->MERC_CODE)
                                                <option value="{{ $department->MERC_CODE }}" selected>{{ $department->MERC_DEPARTMENT }}</option>
                                            @else
                                                <option value="{{ $department->MERC_CODE }}">{{ $department->MERC_DEPARTMENT }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Division</label>
                                <div class="col-sm-7">
                                    <select name="PRO_MERCHANDISE" class="form-control input-sm select2">
                                        <option></option>
                                        @foreach($divisions as $division)
                                            @if(isset($product) && $product->PRO_MERCHANDISE == $division->MERC_CODE)
                                                <option value="{{ $division->MERC_CODE }}" selected>{{ $division->MERC_DIVISION }}</option>
                                            @else
                                                <option value="{{ $division->MERC_CODE }}">{{ $division->MERC_DIVISION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Category</label>
                                <div class="col-sm-7">
                                    <select name="PRO_MERCHANDISE" class="form-control input-sm select2">
                                        <option></option>
                                        @foreach($categories as $category)
                                            @if(isset($product) && $product->PRO_MERCHANDISE == $category->MERC_CODE)
                                                <option value="{{ $category->MERC_CODE }}" selected>{{ $category->MERC_CATEGORY }}</option>
                                            @else
                                                <option value="{{ $category->MERC_CODE }}">{{ $category->MERC_CATEGORY }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>--}}
                        {{--<div class="form-group">
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Sub Category</label>
                                <div class="col-sm-7">
                                    <select name="PRO_MERCHANDISE" class="form-control input-sm select2">
                                        <option></option>
                                        @foreach($sub_categories as $sub_category)
                                            @if(isset($product) && $product->PRO_MERCHANDISE == $sub_category->MERC_CODE)
                                                <option value="{{ $sub_category->MERC_CODE }}" selected>{{ $sub_category->MERC_SUBCATEGORY }}</option>
                                            @else
                                                <option value="{{ $sub_category->MERC_CODE }}">{{ $sub_category->MERC_SUBCATEGORY }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Segment</label>
                                <div class="col-sm-7">
                                    <select name="PRO_MERCHANDISE" class="form-control input-sm select2">
                                        <option></option>
                                        @foreach($segments as $segment)
                                            @if(isset($product) && $product->PRO_MERCHANDISE == $segment->MERC_CODE)
                                                <option value="{{ $segment->MERC_CODE }}" selected>{{ $segment->MERC_SEGMENT }}</option>
                                            @else
                                                <option value="{{ $segment->MERC_CODE }}">{{ $segment->MERC_SEGMENT }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>--}}
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Unit</label>
                                <div class="col-sm-7">
                                    <select name="PRO_UNIT" class="form-control input-sm select2">
                                        <option value=""></option>
                                        @foreach($units as $unit)
                                            @if(isset($product) && $product->PRO_UNIT == $unit->LOK_RECID )
                                                <option value="{{ $unit->LOK_RECID }}" selected>{{ $unit->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $unit->LOK_RECID }}">{{ $unit->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">PLU Code :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="PRO_CODE" value="{{ isset($product) ? $product->PRO_CODE : '' }}" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">MC Mapping :</label>
                                <div class="col-sm-7">
                                    <select name="PRO_MERCHANDISE" id="" class="form-control select2">
                                        <option value=""></option>
                                        @foreach($merchandises as $merchandise)
                                            @if(isset($product) && $product->PRO_MERCHANDISE == $merchandise->MERC_CODE)
                                                <option value="{{ $merchandise->MERC_CODE }}" selected>{{ $merchandise->MERC_CODE }}</option>
                                            @else
                                                <option value="{{ $merchandise->MERC_CODE }}">{{ $merchandise->MERC_CODE }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5">
                                <label class="col-sm-4 control-label text-left">Description :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="PRO_DESCR" value="{{ isset($product) ?  $product->PRO_DESCR  : '' }}" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="PRO_ACTIVE" {{ isset($product) && $product->PRO_ACTIVE == 1 ?  'checked'  : '' }}> Active
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($product) ? 'Update' : 'Submit' }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>

    <script>
        $(function() {
            $('.select2').select2({
                placeholder : 'Choose One',
                allowClear : true
            });
        })
    </script>
@endsection


@extends('layouts.loyalty')

@section('content')
    <!-- Panel Inline Form -->
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">

                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-body">
                            <form method="POST" action="{{ route('master.id-type.update', $lookup->LOK_RECID) }}" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="col-sm-3 control-label text-left">ID Type :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="LOK_DESCRIPTION" value="{{ $lookup->LOK_DESCRIPTION }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <input type="checkbox" name="LOK_ACTIVE" {{ $lookup->LOK_ACTIVE == 1 ? 'checked' : '' }}> Active
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="com-sm-6">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->

@endsection

@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(function () {

        })
    </script>

@endsection
@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection

@section('content')
    <!-- Panel Inline Form -->
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <br/>
                    <table class="table table-hover dataTable table-striped width-full" id="d-bank-card-type">
                        <thead>
                        <tr>
                            <th>NO.</th>
                            <th>CODE</th>
                            <th>NAME</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>

                    <div class="panel" style="border:1px solid #000;">
                        <div class="panel-body">
                            <form method="POST" action="{{ route('master.bank-card-type.store') }}" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3 control-label text-left">Bank Card Type Name :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3 control-label text-left">Bank Card Type Code :</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="code" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3 control-label text-left">Bank :</label>
                                        <div class="col-sm-9">
                                            <select name="bank_id" class="select2 form-control">
                                                @foreach($banks as $bank)
                                                <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="com-sm-6">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel Inline Form -->

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ url('master/bank-card-type/destroy') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
        var oTable;
        $(function () {
            var start;
            oTable = $('#d-bank-card-type').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('master/bank-card-type/data') }}',
                columns : [
                    { data : 'id', name: 'id', searchable: false, orderable: false},
                    { data : 'code', name: 'code'},
                    { data : 'name', name: 'name'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });
        })
    </script>

@endsection
@extends('layouts.loyalty')
@section('css')
	<link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
@endsection
@section('content')
<div class="panel">
	@if(Session::has('notif_success'))
		<div class="alert alert-success">
			{{Session::get('notif_success')}}
		</div>
	@endif
	<h3 class="panel-title"></h3>
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-detail">
				<div class="panel-body">
					<form class="form-horizontal" action="{{ route('report.point-balance-customer.display-pdf') }}" method="POST">
					{!! csrf_field() !!}
					<h4>Point Balance Customer</h4>
		    			<div class="form-group">
                            <div class="col-sm-6">
                                <label class="control-label">Periode</label>
                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                </span>
                                    <input type="text" class="form-control input-sm date" name="periode"  required>
                                </div>
                            </div>
		    			</div>
		    			<div class="form-group">
		    				<div class="col-sm-6">
		    					<label class="control-label">From Barcode</label>
	                            <input type="text" class="form-control" name="fromBarcode" value="0000000000" maxlength="10" required />
		    				</div>
		    				<div class="col-sm-6">
		    					<label class="control-label">To Barcode</label>
	                            <input type="text" class="form-control" name="toBarcode" value="9999999999" maxlength="10" required />
		    				</div>
		    			</div>
		    			<div class="form-group">
		    				<div class="col-sm-6">
		    					<label class="control-label">Order By :</label>
	                            <select name="orderBy" class="form-control setting select2" required>
	                                <option value="earning">Total Earning</option>
	                                <option value="redemption">Total Redemption</option>
	                                <option value="balance">Point Balance</option>
	                            </select>
		    				</div>
		    				<div class="col-sm-6">
		    					<label class="control-label">Total Record</label>
	                            <input type="numeric" class="form-control" name="limit" placeholder="Blank for no limit" />
		    				</div>
		    			</div>
						<h4>Sort By</h4>
		    			<div class="form-group">
		    				<div class="col-sm-12">
			    				<div class="col-sm-5">
									<label class="custom-control">
									    <input name="sortBy" value="Ascending" type="radio" class="custom-control-input" checked required />
									    <span>Ascending</span>
									</label>
								</div>
								<div class="col-sm-offset-2 col-sm-5">
									<label class="custom-control">
									  <input name="sortBy" value="Descending" type="radio" class="custom-control-input" required />
									  <span>Descending</span>
									</label>
								</div>
		    				</div>
		    			</div>
						<div class="form-group">
		    				<div class="col-sm-6">
		    					<button type="submit" class="btn btn-primary w100"><i class="fa fa-file-text-o"></i> View in PDF</button>
		    				</div>
		    				<div class="col-sm-6">
		    					<button type="submit" formaction="{{ route('report.point-balance-customer.download-pdf') }}" class="btn btn-danger w100"><i class="fa fa-file-pdf-o"></i> Download in PDF</button>
		    				</div>
						</div>
						<div class="form-group">
		    				<div class="col-sm-6">
		    					<button type="submit" formaction="{{ route('report.point-balance-customer.download-excel') }}" class="btn btn-success w100"><i class="fa fa-file-excel-o"></i> Download in Excel</button>
		    				</div>
		    				<div class="col-sm-6">
		    					<button type="submit" formaction="{{ route('report.point-balance-customer.download-csv') }}" class="btn btn-info w100"><i class="fa fa-file-text-o"></i> Download in CSV</button>
		    				</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    
    <script type="text/javascript">
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
    	$('.input-daterange').datepicker();
            $('.date').datepicker({
                format : 'yyyy-mm-dd',
                orientation: "bottom left"
            });
    </script>
@endsection

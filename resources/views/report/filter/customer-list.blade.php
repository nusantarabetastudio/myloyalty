@extends('layouts.loyalty')
@section('css')
	<link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
@endsection
@section('content')
<div class="panel">
	@if (Session::has('notif_success'))
		<div class="alert alert-success">
			{{ Session::get('notif_success') }}
		</div>
	@endif
	<h3 class="panel-title"></h3>
    <div class="container">
	    <div class="col-sm-offset-2 col-sm-8">
	    	<div class="panel panel-detail">
	    		<div class="panel-body">
					<form class="form-horizontal" action="{{ route('report.customer-list.display-pdf') }}" method="POST">
					{!! csrf_field() !!}
					<h4>Customer List Report Filter</h4>
		    			<div class="form-group">
		    				<div class="col-sm-12">
		    					<label class="control-label">Date Filter</label>
	                            <div class="input-daterange">
	                                <div class="input-group">
	                                    <span class="input-group-addon">From</span>
	                                    <input type="text" class="form-control valueDate"  name="fromDate" value=""  required />
	                                </div>
	                                <div class="input-group">
	                                    <span class="input-group-addon">to</span>
	                                    <input type="text" class="form-control limit-value" name="toDate" value=""  required />
	                                </div>
		                        </div>
		    				</div>
		    			</div>
		    			<div class="form-group">
		    				<div class="col-sm-6">
		    					<label class="control-label">From Barcode</label>
	                            <input type="text" class="form-control" name="fromBarcode" value="0000000000" maxlength="10" required />
		    				</div>
		    				<div class="col-sm-6">
		    					<label class="control-label">To Barcode</label>
	                            <input type="text" class="form-control" name="toBarcode" value="9999999999" maxlength="10" required />
		    				</div>
		    			</div>
		    			<div class="form-group">
		    				<div class="col-sm-6">
		    					<label class="control-label">Store :</label>
	                            <select name="store" class="form-control setting select2" required>
	                                <option value="All">All</option>
								    @foreach ($tenants as $tenant)
										<option value="{{ $tenant->TNT_RECID }}">{{ $tenant->TNT_CODE . ' - ' . $tenant->TNT_DESC }}</option>
								    @endforeach
	                            </select>
		    				</div>
		    				<div class="col-sm-6">
		    					<label class="control-label">Total Record</label>
	                            <input type="numeric" class="form-control" name="limit" placeholder="Blank for no limit" />
		    				</div>
		    			</div>
						<h4>Show</h4>
		    			<div class="form-group">
		    				<div class="col-sm-12">
			    				<div class="col-sm-5">
									<label class="custom-control">
									    <input name="active" value="Active" type="radio" class="custom-control-input" checked required />
									    <span>Active Customers</span>
									</label>
								</div>
								<div class="col-sm-offset-2 col-sm-5">
									<label class="custom-control">
									  <input name="active" value="Inactive" type="radio" class="custom-control-input" required />
									  <span>Inactive Customers</span>
									</label>
								</div>
		    				</div>
		    			</div>
						<div class="form-group">
		    				<div class="col-sm-6">
		    					<button type="submit" class="btn btn-primary w100"><i class="fa fa-file-text-o"></i> View in PDF</button>
		    				</div>
		    				<div class="col-sm-6">
		    					<button type="submit" formaction="{{ route('report.customer-list.download-pdf') }}" class="btn btn-danger w100"><i class="fa fa-file-pdf-o"></i> Download in PDF</button>
		    				</div>
						</div>
						<div class="form-group">
		    				<div class="col-sm-6">
		    					<button type="submit" formaction="{{ route('report.customer-list.download-excel') }}" class="btn btn-success w100"><i class="fa fa-file-excel-o"></i> Download in Excel</button>
		    				</div>
		    				<div class="col-sm-6">
		    					<button type="submit" formaction="{{ route('report.customer-list.download-csv') }}" class="btn btn-info w100"><i class="fa fa-file-text-o"></i> Download in CSV</button>
		    				</div>
						</div>
					</form>
	    		</div>
	    	</div>
	    </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
    	$('.input-daterange').datepicker();
    </script>
@endsection
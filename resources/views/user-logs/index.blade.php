@extends('layouts.loyalty')
@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('notif_success'))
                <div class="alert alert-success">
                    {{ Session::get('notif_success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-6">
                    <div class="dataTables_length">
                    <label>Show
                        <select class="form-control limit">
                            <option data-limit="10">10</option>
                            <option data-limit="25">25</option>
                            <option data-limit="50">50</option>
                            <option data-limit="100">100</option>
                        </select>
                        entries</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="dataTables_filter">
                        <label>
                            Search:
                            <input type="search" class="form-control search-user-logs">
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-lg-12">
		            <div class="sk-folding-cube loading-wrapper" style="display:none;">
		                <div class="sk-cube1 sk-cube"></div>
		                <div class="sk-cube2 sk-cube"></div>
		                <div class="sk-cube4 sk-cube"></div>
		                <div class="sk-cube3 sk-cube"></div>
		            </div>
                    <table class="tablesaw table-striped dataTable table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Date Accessed</th>
                                <th>Description</th>
                                <th>User Name</th>
                                <th>IP</th>
                            </tr>
                        </thead>
                        <tbody>
	                    	@foreach($userLogs as $data)
	                    	<tr class="item">
                                <?php
                                    $createdAt = explode('.', $data->created_at);
                                    $createdAt = $createdAt[0];
                                ?>
	                    		<td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $createdAt)->format('d M Y H:i:s') }}</td>
	                    		<td>{{ $data->description }}</td>
                                <td>{{ $data->user->USER_USERNAME }}</td>
	                    		<td>{{ $data->IP }}</td>
	                    	</tr>
	                    	@endforeach
			                <tr class="iTemplate" id="item-template">
			                    <td class="item-createdat"></td>
			                    <td class="item-route"></td>
                                <td class="item-user"></td>
			                    <td class="item-ip"></td>
			                </tr>
                        </tbody>
                    </table>
            	</div>
            </div>
            <div class="row mart20">
                <div class="col-sm-12 text-right pagination-wrapper">
                    <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginator->previous_page }}">
                        Previous
                    </button>
                    Page 
                    <span class="current-page" data-page="{{ $paginator->current_page }}">
                        {{ $paginator->current_page }}
                    </span>
                     / 
                    <span class="total-page">{{ $paginator->total_pages }}</span>
                    <button class="btn btn-primary next-btn pagination-btn{{ (!$paginator->next_page) ? ' disabled' : '' }}" data-page="{{ $paginator->next_page }}">
                        Next
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
	<script type="text/javascript">
        $(document).ready(function() {
            $('.pagination-btn').click(function() {
                $('.current-page').data('page', $(this).data('page'));
                getData();
            });
            $('.limit').change(function() {
                getData(true);
            });
            $('.search-user-logs').keyup(function(e) {
                if(e.keyCode == 13) {
                    getData(true);
                }
            });
            function getData(resetPage = false) {
                var limit = $('.limit').find('option:selected').data('limit');
                var page = $('.current-page').data('page');
                if (resetPage) page = 1;
                var search = $('.search-user-logs').val();
                var pageUrl = '{{ route('user-logs.data') }}?limit=' + limit + '&page=' + page +'&s='+ search;
                if (pageUrl) {
                    $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: pageUrl,
                        success: function(data) {
                            $('.loading-wrapper').hide().closest('table').css('opacity', 1);
                            var template = $('#item-template');
                            var items = data.DATA;
                            var clone;

                            $('.item').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
                                $('.item-createdat', clone).text(item.created_at);
                                $('.item-route', clone).text(item.description);
                                $('.item-user', clone).text(item.user.USER_USERNAME);
                                $('.item-ip', clone).text(item.IP);
                                template.before(clone);
                            }
                            $('.current-page').text(data.paginator.current_page);
                            $('.previous-btn').data("page", data.paginator.previous_page);
                            $('.next-btn').data("page", data.paginator.next_page);
                            $('.total-page').text(data.paginator.total_pages);

                            $('.pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
        });
	</script>
@endsection
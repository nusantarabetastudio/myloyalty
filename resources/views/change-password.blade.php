@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="col-sm-6">
                @if(Session::has('msg'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ Session::get('msg') }}</li>
                        </ul>
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning">
                        <ul>
                            <li>{{ Session::get('error') }}</li>
                        </ul>
                    </div>
                @elseif (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('change-password.update', Session::get('user')->USER_USERNAME) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="" class="control-label">Old Password</label>
                        <input type="password" class="form-control" name="oldPassword">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">New Password</label>
                        <input type="password" class="form-control" name="newPassword">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Update Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
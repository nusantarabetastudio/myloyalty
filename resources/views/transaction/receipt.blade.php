<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>{{ $customer->CUST_NAME }}</title>
    <style>
        .middle {
            text-align: center;
        }
        .left {
            text-align: left;
        }
        .table {
            border: none;
        }
        .right {
            text-align: right;
        }
        .table {
            width: 100%;
        }
        .current {
            border-top: dashed;
        }
        .margin-top-10 {
            margin-top: 10px;
        }
        .wrapper {
            margin: -20px;
            padding: -20px;
        }
    </style>
</head>
<body>

<div class="wrapper">
    <div class="middle">
        EARNING RECEIPT
    </div>
    <hr>
    <div class="left">
        {{ $data['CUST_BARCODE'] }}<br>
        {{ $data['CUST_NAME'] }}
    </div>
    <hr>
    <div class="left">
        No: - <br>
        Date: {{ $data['RCPDATE'] . ' ' . $data['RCPTIME'] }}
    </div>
    <hr>
    <div class="table">
        <table>
            <tr>
                <td width="230"> {{ $store->TNT_CODE }} {{ $store->TNT_DESC }}</td>
                <td class="right">{{ $data['TOTALAFTERDISC'] }}</td>
            </tr>
            <tr>
                <td width="230">Single Receipt</td>
                <td class="right current">{{ $data['TOTALAFTERDISC'] }}</td>
            </tr>
        </table>
    </div>
    <hr>
    <div class="middle">
        Points
    </div>
    <hr>
    <div class="left">
        REDEEM
    </div>

    <div class="table">
        <table>
            <tr>
                <td width="230">Before</td>
                <td class="right">{{ isset($data['BEFORE_EARNING']) ? $data['BEFORE_EARNING'] : '0' }}</td>
            </tr>
            <tr>
                <td width="230" style="vertical-align: top">Earning</td>
                <td class="right">{{ $earning['point_regular'] }}<br/>@if($earning['point_reward']!=0) {{$earning['point_reward'] }} @endif<br/>@if($earning['point_bonus']!=0) {{ $earning['point_bonus'] }} @endif</td>
            </tr>
            @php
                $after_earning = $earning['point_regular']+$earning['point_reward']+$earning['point_bonus'];
                $before = $data['BEFORE_EARNING'];
                $current = $after_earning + $before;
            @endphp
            <tr>
                <td width="230">Current</td>
                <td class="right current">{{ $current }}</td>
            </tr>
        </table>
    </div>

    <div class="left margin-top-10">
        REWARD
    </div>

    <div class="table">
        <table>
            <tr>
                <td width="230">Before</td>
                <td class="right">-</td>
            </tr>
            <tr>
                <td width="230">Earning</td>
                <td class="right">-</td>
            </tr>
            <tr>
                <td width="230">Current</td>
                <td class="right current">-</td>
            </tr>
        </table>
    </div>
    <hr>
    <div class="left margin-top-10">
        {{ Session::get('user')->USER_USERNAME }} {{ date('d/m/Y H:i:s') }}
    </div>
</div>

</body>
</html>
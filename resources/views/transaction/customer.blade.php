@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <br>
            <div class="row">
                <div class="col-lg-12 text-right">
                    @if($type== 'testing')
                        <a href="{{ route('destroy.testing.post.temp') }}" class="btn btn-lg btn-danger">Reset Testing Post Temp</a>
                    @endif
                    @if($type == 'posting')
                        <a href="{{url('helper/delete-post-temp')}}" class="btn btn-lg btn-danger">Reset Post Temp</a>
                        <a href="{{url('earning/upload')}}" class="btn btn-lg btn-primary">Earning By Uploading CSV</a>
                    @endif
                </div>
            </div>
            <br/>
            <table class="table table-hover dataTable table-striped width-full" id="void-transaction">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Barcode Number</th>
                    <th>ID Type</th>
                    <th>ID Number</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#void-transaction').DataTable({
                processing: true,
                serverSide: true,
                @if ($type == 'posting')
                ajax: '{{ route('transaction.posting.customer.data') }}',
                @elseif ($type == 'void')
                ajax: '{{ route('transaction.void.customer.data') }}',
                @elseif ($type == 'adjustment')
                ajax: '{{ route('transaction.adjustment.customer.data') }}',
                @elseif ($type == 'manual')
                ajax: '{{ route('transaction.manual.customer.data') }}',
                @elseif ($type == 'testing')
                ajax: '{{ route('transaction.testing.customer.data') }}',
                @elseif($type == 'manipulate')
                ajax: '{{ route('transaction.manipulate.customer.data') }}',
                @endif
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'CUST_NAME', name: 'CUST_NAME'},
                    { data: 'mobilePhone', name: 'mobilePhone.PHN_NUMBER'},
                    { data: 'CUST_BARCODE', name: 'CUST_BARCODE'},
                    { data: 'idType', name: 'idType.LOK_DESCRIPTION'},
                    { data: 'CUST_IDCARDS', name: 'CUST_IDCARDS'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });
        })
    </script>

@endsection
@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.min.css') }}">
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="nav-tabs-horizontal">
                <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li role="presentation"><a href="{{ route('transaction.posting.detail', $customer->CUST_RECID) }}" role="tab">Posting Transaction</a></li>
                    <li role="presentation"><a href="{{ route('transaction.void.detail', $customer->CUST_RECID) }}" role="tab">Void Transaction</a></li>
                    <li role="presentation"><a href="{{ route('transaction.adjustment.detail', $customer->CUST_RECID) }}">Adjustment Transaction</a></li>
                    <li class="pull-right" role="presentation"><a class="red-nav" href="{{ route('transaction.manual.customer') }}"><i class="glyphicon glyphicon-triangle-left"></i> Select Another Customer</a></li>
                </ul>
                <div class="tab-content padding-top-10">
                    <div class="tab-pane active" role="tabpanel">
                        <div class="panel panel-detail">
                            <div class="panel-body">
                                <form class="form-horizontal">
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Barcode No:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_BARCODE }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Name:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_NAME }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Total Expense:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_EXPENSE_EARN }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Lucky Draw:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->point->total_lucky_draw_point }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Redeem:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->point->total_redeem_point }}">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <form action="{{ route('transaction.manual.process') }}" class="form-horizontal" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="CARDID" value="{{ $customer->CUST_BARCODE }}">
                            <input type="hidden" name="CUST_RECID" value="{{ $customer->CUST_RECID }}">
                            <input type="hidden" name="BEFORE_EARNING" value="{{ $customer->point->total_redeem_point }}">
                            <input type="hidden" name="CUST_NAME" value="{{ $customer->CUST_NAME }}">
                            <input type="hidden" name="CUST_BARCODE" value="{{ $customer->CUST_BARCODE }}">
                            <input type="hidden" name="RCPDATE" value="{{ date('Y-m-d') }}">
                            <input type="hidden" name="RCPTIME" value="{{ date('H:i:s') }}">
                            <div class="panel panel-detail">
                                <div class="panel-body">
                                    @if($errors->has())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}<br>
                                            @endforeach
                                        </div>
                                    @elseif(Session::has('success'))
                                        <div class="alert alert-success">
                                            {{ Session::get('success') }}
                                        </div>
                                    @endif
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Store Name :</label>
                                            <div class="col-sm-7">
                                                <select name="STOREID" class="form-control select2">
                                                    <option value=""></option>
                                                    @foreach($stores as $store)
                                                        <option value="{{ $store->TNT_CODE }}">{{ $store->TNT_CODE . ' ' . $store->TNT_DESC }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Posting Date :</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" disabled value="{{ date('Y-m-d') }}" name="RCPDATE" id="date">
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" placeholder="HHMMSS" name="RCPTIME" disabled value="{{ date('H:i:s') }}" id="time" maxlength="6">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Bank :</label>
                                            <div class="col-sm-7">
                                                <select name="BIN" class="form-control select2" id="">
                                                    <option value=""></option>
                                                    @foreach($banks as $bank)
                                                        <option value="{{ $bank->code }}">{{ $bank->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Payment Method :</label>
                                            <div class="col-sm-7">
                                                <select name="TENDNBR" class="form-control select2">
                                                    <option value=""></option>
                                                    @foreach($payment_methods as $payment_method)
                                                        <option value="{{ $payment_method->code }}">{{ $payment_method->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label for="amount" class="col-sm-5 control-label text-left">Amount :</label>
                                            <div class="col-sm-7">
                                                <input type="number" min="0" id="amount" name="TOTALAFTERDISC" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                            {{--<div class="panel panel-detail">
                                <div class="panel-body">
                                    <table class="table table-hover table-striped width-full" id="table-container">
                                        <thead>
                                            <tr>
                                                <th>ARTICLE</th>
                                                <th>QTY</th>
                                                <th>AMOUNT</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody id="product-container">
                                            <tr class="no-data">
                                                <td colspan="4">No Data</td>
                                            </tr>
                                            <tr id="material-template" class="iTemplate">
                                                <th class="material-article"></th>
                                                <th class="material-qty">-</th>
                                                <th class="material-amount">-</th>
                                                <th><a href="javascript:void(0)" class="material-delete">Delete</a></th>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="total">
                                                <td class="bold">Total</td>
                                                <td class="total-qty bold">0</td>
                                                <td class="total-amount bold">0</td>
                                                <input type="hidden" name="TOTALAFTERDISC" id="totalAmount">
                                                <input type="hidden" name="ITEMQTY" id="totalQty">
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2({
                width: '100%',
                placeholder: 'Choose One',
                allowClear: true
            });

            $('.datepicker').datepicker({
                format : 'yyyy-mm-dd',
                orientation: "bottom left"
            });

          /*  $('.js-article').select2({
                width: '100%',
                allowClear: true,
                placeholder: 'Choose One',
                ajax: {
                    url: '',
                    dataType : 'json',
                    delay : 250,
                    data : function (params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj) {
                                return { id: obj.PRO_CODE, text: obj.PRO_DESCR};
                            })
                        };
                    },
                    cache: true
                },
                beforeSend: function (xhr) {
                    var token = '';

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                minimumInputLength: 3
            });

            var countProduct = 1;

            $('#add-product-btn').click(function() {
                var productName = $('#article option:selected').text();
                var productId = $('#article option:selected').val();
                var qty = parseInt($('#qty').val());
                var amount = parseInt($('#amount').val());

                if(productId !== null && productId !== '' && qty !== null && amount !== null) {
                    $('#product-container tr.no-data').hide();

                    addProduct(productName, productId, qty, amount);
                } else {
                    alert('All fields can not be empty.');

                }
            });

            $('#qty, #amount').keyup(function(e) {
                e.preventDefault();

                if (e.keyCode == 13) {
                    var productName = $('#article option:selected').text();
                    var productId = $('#article option:selected').val();
                    var qty = parseInt($('#qty').val());
                    var amount = parseInt($('#amount').val());

                    if(productId !== null && productId !== '' && qty !== null && amount !== null) {
                        $('#table-product-container tr.no-data').hide();
                        addProduct(productName, productId, qty, amount);

                    } else {
                        alert('Product, Qty, Amount can not be empty.');
                    }
                }
            });

            function addProduct(productName, productId, qty, amount) {

                var template = $('#material-template');
                var clone = template.clone().removeAttr('id').removeClass('iTemplate');
                var totalQty = parseInt($('.total-qty').text());
                var totalAmount = parseInt($('.total-amount').text());
                $('.material-article', clone).html(
                        '<span class="article">'+ productName +'</span>'+
                        '<input type="hidden" class="add-item-id" value="'+productId+'" name="products['+ countProduct +'][id]">'
                );

                $('.material-qty', clone).html(
                        '<span class="qty">'+ qty +'</span>'+
                        '<input type="hidden" class="add-item-qty" value="'+qty+'" name="products['+ countProduct +'][qty]">'
                );
                $('.material-amount', clone).html(
                        '<span class="amount">'+ amount +'</span>'+
                        '<input type="hidden" class="add-item-amount" value="'+amount+'" name="products['+ countProduct +'][amount]">'
                );
                $('.material-delete', clone).click(function() {
                    var qty = parseInt($('.qty', clone).text());
                    var totalQty =parseInt($('.total-qty').text());
                    totalQty = totalQty - qty;
                    $('.total-qty').text(totalQty);
                    $('#totalQty').val(totalQty);

                    var amount = parseInt($('.amount', clone).text());
                    var totalAmount = parseInt($('.total-amount').text());
                    totalAmount = totalAmount - amount;
                    $('.total-amount').text(totalAmount);
                    $('#totalAmount').val(totalAmount);

                    $('.material-article', clone).html('');

                    $(this).parent().parent().remove();
                });


                totalQty = qty + totalQty;
                $(".total-qty").text(totalQty);
                $('#totalQty').val(totalQty);

                totalAmount = amount + totalAmount;
                $('.total-amount').text(totalAmount);
                $('#totalAmount').val(totalAmount);

                template.before(clone);
                countProduct++;

            }*/
        });


    </script>

@endsection
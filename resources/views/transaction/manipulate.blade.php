@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.min.css') }}">
@endsection
@section('content')
<div class="panel">
	<div class="panel-body">
		<div class="panel panel-detail">
			<div class="panel-body">
		        <form class="form-horizontal">
		            <div class="form-group form-material-sm">
		                <div class="col-sm-4">
		                    <label class="col-sm-5 control-label text-left">Barcode No:</label>
		                    <div class="col-sm-7">
		                        <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_BARCODE }}">
		                    </div>
		                </div>
		                <div class="col-sm-8">
		                    <label class="col-sm-3 control-label text-left">Name:</label>
		                    <div class="col-sm-9">
		                        <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_NAME }}">
		                    </div>
		                </div>
		            </div>
		            <div class="form-group form-material-sm">
		                <div class="col-sm-4">
		                    <label class="col-sm-5 control-label text-left">Total Expense:</label>
		                    <div class="col-sm-7">
		                        <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_EXPENSE_EARN }}">
		                    </div>
		                </div>
		                <div class="col-sm-4">
		                    <label class="col-sm-8 control-label text-left">Total Earn Lucky Draw:</label>
		                    <div class="col-sm-4">
		                        <input type="text" class="form-control" disabled="disabled" value="{{ $customer->luckyDrawPoint }}">
		                    </div>
		                </div>
		                <div class="col-sm-4">
		                    <label class="col-sm-8 control-label text-left">Total Earn Redeem:</label>
		                    <div class="col-sm-4">
		                        <input type="text" class="form-control" disabled="disabled" value="{{ $customer->redeemPoint }}">
		                    </div>
		                </div>
		            </div>
		        </form>
			</div>
		</div>
		<div class="panel panel-detail">
			<div class="panel-body">
				<form class="form-horizontal" action="{{ route('transaction.manipulate.process', $customer->CUST_RECID) }}" method="POST">
					{!! csrf_field() !!}
					@if(count($errors) >0)
				        @foreach($errors->all() as $error)
						<div class="alert alert-danger">
							{{ $error }}
						</div>
						@endforeach
					@endif
					<div class="form-group">
		                <div class="col-sm-6">
		                    <label class="col-sm-5 control-label text-left">Amount</label>
		                    <div class="col-sm-7">
		                        <input type="number" class="form-control" min="0" id="amount" name="amount" value="{{ old('amount') }}">
		                    </div>
		                </div>
		                <div class="col-sm-6">
		                	<label class="col-sm-5 control-label text-left">Point</label>
		                	<div class="col-sm-7">
		                		<input type="number" class="form-control" id="point" name="point" value="{{ old('point') }}">
		                	</div>
		                </div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<label class="col-sm-5 control-label text-left">Type</label>
							<div class="col-sm-7">
								<select id="type" class="form-control select2" name="type">
									<option value="add" {{ old('type') == 'add' ? 'selected' : '' }}>Add Point</option>
									<option value="minus" {{ old('type') == 'minus' ? 'selected' : '' }}>Minus Point</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							
						</div>
					</div>
					<button type="submit" class="btn btn-primary pull-right" id="add-btn">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2({
                width: '100%',
                allowClear: true,
                placeholder: 'Choose One'
            });

            $('.datepicker').datepicker({
                format : 'yyyy-mm-dd',
                orientation: "bottom left"
            });
        });


    </script>

@endsection
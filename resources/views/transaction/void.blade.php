@extends('layouts.loyalty')

@section('content')
<form class="form-horizontal" action="" method="POST">
    <div class="panel">
        <div class="panel-body">
            <div class="nav-tabs-horizontal">
                <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li role="presentation"><a href="{{ route('transaction.posting.detail', $customer->CUST_RECID) }}" role="tab">Posting Transaction</a></li>
                    <li class="active" role="presentation"><a href="{{ route('transaction.void.detail', $customer->CUST_RECID) }}" role="tab">Void Transaction</a></li>
                    <li role="presentation"><a href="{{ route('transaction.adjustment.detail', $customer->CUST_RECID) }}">Adjustment Transaction</a></li>
                    <li class="pull-right" role="presentation"><a class="red-nav" href="{{ route('transaction.void.customer') }}"><i class="glyphicon glyphicon-triangle-left"></i> Select Another Customer</a></li>
                </ul>
                <div class="tab-content padding-top-10">
                    <div class="tab-pane active" role="tabpanel">
                        <div class="panel panel-detail">
                            <div class="panel-body">
                                
                                    {{ csrf_field() }}
                                    @if($errors->has())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}<br>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Barcode No:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="" value="{{ $customer->CUST_BARCODE }}" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Name:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="" value="{{ $customer->CUST_NAME }}" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Total Expense:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="" value="{{ $customer->CUST_EXPENSE_EARN }}" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Lucky Draw:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="" value="{{ $customer->point->total_lucky_draw_point }}" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Redeem:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" value="{{ $customer->point->total_redeem_point }}" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        <br>
                        <table class="table table-hover dataTable table-striped width-full" id="void-transaction">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Store</th>
                                <th>Amount</th>
                                <th>LD</th>
                                <th>RD</th>
                                {{--<th>Reff</th>
                                <th>Task</th>--}}
                                <th>Void</th>
                            </tr>
                            </thead>
                        </table>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        var start;
        $(function () {
            oTable = $('#void-transaction').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'POST',
                    url: '{{ route('transaction.void.transaction.data', $customer->CUST_BARCODE) }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'POS_POST_DATE', name: 'POS_POST_DATE'},
                    { data: 'POS_POST_TIME', name: 'POS_POST_TIME'},
                    { data: 'store.TNT_DESC', name: 'store.TNT_DESC'},
                    { data: 'POS_AMOUNT', name: 'POS_AMOUNT'},
                    { data: 'POS_POINT_LD', name: 'POS_POINT_LD'},
                    { data: 'POS_POINT_REGULAR', name: 'POS_POINT_REGULAR'},
                    /*{ data: 'ref', name: 'ref'},
                    { data: 'task', name: 'task'},*/
                    { data: 'void', name: 'void', searchable: false, orderable: false}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });
        })
    </script>

@endsection
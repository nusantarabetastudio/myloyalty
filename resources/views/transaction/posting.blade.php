@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="nav-tabs-horizontal">
                <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="{{ route('transaction.posting.detail', $customer->CUST_RECID) }}" role="tab">Posting Transaction</a></li>
                    <li role="presentation"><a href="{{ route('transaction.void.detail', $customer->CUST_RECID) }}" role="tab">Void Transaction</a></li>
                    <li role="presentation"><a href="{{ route('transaction.adjustment.detail', $customer->CUST_RECID) }}">Adjustment Transaction</a></li>
                    <li class="pull-right" role="presentation"><a class="red-nav" href="{{ route('transaction.posting.customer') }}"><i class="glyphicon glyphicon-triangle-left"></i> Select Another Customer</a></li>
                </ul>
                <div class="tab-content padding-top-10">
                    <div class="tab-pane active" role="tabpanel">
                        <div class="panel panel-detail">
                            <div class="panel-body">
                                <form class="form-horizontal">
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Barcode No:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_BARCODE }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Name:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_NAME }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Total Expense:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->CUST_EXPENSE_EARN }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Lucky Draw:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->point->total_lucky_draw_point }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Redeem:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->point->total_redeem_point }}">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="panel panel-detail">
                            <div class="panel-body">
                                <form action="" class="form-horizontal" method="POST">
                                    {{ csrf_field() }}
                                    @if($errors->has())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}<br>
                                            @endforeach
                                        </div>
                                    @elseif(Session::has('success'))
                                        <div class="alert alert-success">
                                            {{ Session::get('success') }}
                                        </div>
                                    @endif
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Store Name:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" value="{{ $customer->tenant !== null ? $customer->tenant->TNT_DESC : '-' }}" disabled="disabled">
                                                <input type="hidden" name="POS_STORE" class="form-control" value="{{ $customer->tenant !== null ? $customer->tenant->TNT_RECID : 'null' }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Posting Date:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" value="{{ Carbon\Carbon::now()->addDay()->toDateTimeString() }}" disabled="disabled">
                                                <input type="hidden" name="POS_POST_DATE" class="form-control" value="{{ Carbon\Carbon::now()->addDay()->toDateTimeString() }}">
                                                <input type="hidden" name="POS_CUST_RECID" class="form-control" value="{{ $customer->CUST_RECID}}">
                                                <input type="hidden" name="POS_BARCODE" class="form-control" value="{{ $customer->CUST_BARCODE}}">
                                                <input type="hidden" name="POS_DOC_NO" class="form-control" value="By Posting Transaction">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Amount:</label>
                                            <div class="col-sm-7">
                                                <input type="text" min="0" class="form-control" value="0" name="POS_AMOUNT">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-6 control-label text-left">Point Lucky Draw:</label>
                                            <div class="col-sm-6">
                                                <input type="number" min="0" class="form-control" value="0" name="POS_POINT_LD">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-6 control-label text-left">Point Redeem:</label>
                                            <div class="col-sm-6">
                                                <input type="number" min="0" class="form-control" value="0" name="POS_POINT_REGULAR">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="pull-right btn btn-primary">Post</button>
                                </form>
                            </div>
                        </div>

                        <div class="panel panel-detail">
                            <div class="panel-heading margin-left-5">
                                <h2>History</h2>
                            </div>
                            <div class="panel-body">
                                <table class="table table-hover dataTable table-striped width-full" id="master-transaction">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Store</th>
                                        <th>Posting Date</th>
                                        <th>Posting Time</th>
                                        <th>Amount</th>
                                        <th>Point LD</th>
                                        <th>Point RD</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div class="panel panel-detail">
                            <div class="panel-heading">
                                <h2>Detail</h2>
                            </div>
                            <div class="panel-body">
                                <table class="table table-hover dataTable table-striped width-full" id="detail-transaction">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Trans Date</th>
                                        <th>Trans Time</th>
                                        <th>Article</th>
                                        <th>Product Name</th>
                                        <th>QTY</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        var oTable2;
        $(function () {
            var start;
            var start2;
            oTable = $('#master-transaction').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('transaction.posting.transaction.master.data', $customer->CUST_BARCODE) }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                lengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'store.TNT_DESC', name: 'store.TNT_DESC'},
                    { data: 'POS_POST_DATE', name: 'POS_POST_DATE'},
                    { data: 'POS_POST_TIME', name: 'POS_POST_TIME'},
                    { data: 'POS_AMOUNT', name: 'POS_AMOUNT'},
                    { data: 'POS_POINT_LD', name: 'POS_POINT_LD'},
                    { data: 'POS_POINT_REGULAR', name: 'POS_POINT_REGULAR'}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });

            oTable2 = $('#detail-transaction').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('transaction.posting.transaction.detail.data', $customer->tenant !== null ? $customer->tenant->TNT_RECID : null) }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                lengthMenu : [[5, 25, 50, -1], [5, 25, 50, "All"]],
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'detail.PSD_TRAN_DATE', name: 'detail.PSD_TRAN_DATE'},
                    { data: 'detail.PSD_TRAN_TIME', name: 'detail.PSD_TRAN_TIME'},
                    { data: 'detail.article.PRO_CODE', name: 'detail.article.PRO_CODE'},
                    { data: 'detail.article.PRO_DESCR', name: 'detail.article.PRO_DESCR'},
                    { data: 'PSD_QTY', name: 'PSD_QTY'},
                    { data: 'PSD_AMOUNT', name: 'PSD_AMOUNT'}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start2 = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start2 );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });
        })
    </script>

@endsection
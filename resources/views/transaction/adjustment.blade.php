@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="nav-tabs-horizontal">
                <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li role="presentation"><a href="{{ route('transaction.posting.detail', $customer->CUST_RECID) }}" role="tab">Posting Transaction</a></li>
                    <li role="presentation"><a href="{{ route('transaction.void.detail', $customer->CUST_RECID) }}" role="tab">Void Transaction</a></li>
                    <li class="active" role="presentation"><a href="{{ route('transaction.adjustment.detail', $customer->CUST_RECID) }}">Adjustment Transaction</a></li>
                    <li class="pull-right" role="presentation"><a class="red-nav" href="{{ route('transaction.adjustment.customer', $customer->CUST_RECID) }}"><i class="glyphicon glyphicon-triangle-left"></i> Select Another Customer</a></li>
                </ul>
                <div class="tab-content padding-top-10">
                    <div class="tab-pane active" role="tabpanel">
                        <div class="panel panel-detail">
                            <div class="panel-body">
                                <form class="form-horizontal" action="#" method="POST">
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Barcode No:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" value="{{ $customer->CUST_BARCODE }}" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Name:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" value="{{ $customer->CUST_NAME }}" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Total Expense:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="" value="{{ $customer->CUST_EXPENSE_EARN }}" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Lucky Draw:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" value="{{ $customer->point->total_lucky_draw_point }}" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-8 control-label text-left">Total Earn Redeem:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" value="{{ $customer->point->total_redeem_point }}" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="panel panel-detail marb0">
                            <div class="panel-body">
                                <form class="form-horizontal" action="{{ route('transaction.adjustment.process') }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <input type="hidden" class="pos_id" name="POS_RECID">
                                    @if($errors->has())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}<br>
                                            @endforeach
                                        </div>
                                    @endif

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {{ Session::get('success') }}
                                        </div>
                                    @endif
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Store Name:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control store_name" name="" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="col-sm-3 control-label text-left">Posting Date:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control pos_date" name="" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Amount:</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control pos_amount" name="POS_AMOUNT" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-6 control-label text-left">Point Lucky Draw:</label>
                                            <div class="col-sm-6">
                                                <input type="number" min="0" class="form-control pos_point_ld" name="POS_POINT_LD" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-6 control-label text-left">Point Redeem:</label>
                                            <div class="col-sm-6">
                                                <input type="number" min="0" class="form-control pos_point_regular" name="POS_POINT_REGULAR" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>
                        <table class="table table-hover dataTable table-striped width-full" id="adjustment-transaction">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Store</th>
                                <th>Amount</th>
                                <th>Lucky Draw</th>
                                <th>Point Regular</th>
                                <th>Select</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        var start;
        jQuery(function ($) {
            oTable = $('#adjustment-transaction').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('transaction.adjustment.transaction.data', $customer->CUST_RECID) }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'POS_POST_DATE', name: 'POS_POST_DATE'},
                    { data: 'POS_POST_TIME', name: 'POS_POST_TIME'},
                    { data: 'store', name: 'store'},
                    { data: 'POS_AMOUNT', name: 'POS_AMOUNT'},
                    { data: 'POS_POINT_LD', name: 'POS_POINT_LD'},
                    { data: 'POS_POINT_REGULAR', name: 'POS_POINT_REGULAR'},
                    { data: 'select', name: 'select', searchable: false, orderable: false}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'desc' ]]
            });

            $('#adjustment-transaction tbody').on('click', '#pos-id', function() {
                var pos_id = $(this).val();

                $.ajax({
                    url: '{{ url('api/v1/pos') }}/' + pos_id,
                    dataType : 'json',
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    success: function(data) {
                        $('.pos_id').val(data.POS_RECID);
                        $('.store_name').val(data.store.TNT_DESC);
                        $('.pos_date').val(data.POS_POST_DATE);
                        $('.pos_amount').val(data.POS_AMOUNT);
                        $('.pos_point_ld').val(data.POS_POINT_LD);
                        $('.pos_point_regular').val(data.POS_POINT_REGULAR);
                    }

                });

            });
        })
    </script>

@endsection
@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.min.css') }}">
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="nav-tabs-horizontal">
                <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="pull-right" role="presentation"><a class="red-nav" href="{{ route('transaction.testing.customer') }}"><i class="glyphicon glyphicon-triangle-left"></i> Select Another Customer</a></li>
                </ul>
                <div class="tab-content padding-top-10">
                    <div class="tab-pane active" role="tabpanel">
                        <div class="panel panel-detail">
                            <div class="panel-body">
                                <table class="table table-bordered table-dark">
                                    
                                        @foreach($post_test as $row)
                                        <thead>
                                        <tr>
                                            <td>Customer</td>
                                            <td>Barcode</td>
                                            <td>Date</td>
                                            <td>Time</td>
                                            <td>Amount</td>
                                            <td>Point Regular</td>
                                            <td>Point Reward</td>
                                            <td>Point Bonus</td>
                                        </tr>
                                    </thead>
                                            <tr>
                                                <td>{{ $row->customer->CUST_NAME }}</td>
                                                <td>{{ $row->POS_BARCODE }}</td>
                                                <td>{{ $row->POS_POST_DATE->format('d-m-Y') }}</td>
                                                <td>{{ substr($row->POS_POST_TIME, 0, 8) }}</td>
                                                <td>{{ number_format($row->POS_AMOUNT,2,',','.') }}</td>
                                                <td>{{ number_format($row->POS_POINT_REGULAR,0,',','.') }}</td>
                                                <td>{{ number_format($row->POS_POINT_REWARD,0,',','.') }}</td>
                                                <td>{{ number_format($row->POS_POINT_BONUS,0,',','.') }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8">
                                                    <span class="text-center text-danger" style='text-align: center'> Actived Point Formula </span>
                                                    <table width="100%" class="table table-bordered table-success"  > 
                                                        <tr style="background-color: #001f9f; font-weight: bolder; color: #ffffff"><th> Type </th><th> Point Formula </th> <th> Get Fix Point / Multiple </th> </tr>
                                                        
                                                     @foreach($row->postPF()->select(DB::raw('MAX(POSTF_POINT) as point'),DB::raw('MAX(POSTF_TYPE) as type'),'POSTF_ERN_RECID','POSTF_POST_RECID')->groupBy('POSTF_POST_RECID','POSTF_ERN_RECID')->orderBy('type','ASC')->get() as $row_pf)
                                                     @if($row_pf->point!=0) 
                                                        <tr style="font-size: smaller;"> <td> 
                                                     @if($row_pf->type==1) {{'Regular'}} @endif
                                                     @if($row_pf->type==2) {{'Reward'}}  @endif
                                                     @if($row_pf->type==3) {{'Bonus'}}  @endif
                                                     @if($row_pf->type==4) {{'Bonus Payment'}} @endif
                                                     @if($row_pf->type==5) {{'Lucky Draw'}}
                                                     @endif
                                                            </td> <td> @if(isset($row_pf->formula->ERN_DESC)) {{ $row_pf->formula->ERN_DESC }} @endif </td> <td> 
                                                            @if($row_pf->type==5) 
                                                            @php 
                                                            $LD = DB::table('PointRandom')->select('RAND_POINT_1')->where('RAND_POS_RECID',$row_pf->POSTF_POST_RECID)->get();  
                                                            @endphp
                                                            <p>
                                                                @foreach($LD as $row_ld)
                                                                {{$row_ld->RAND_POINT_1}}
                                                                <br/>
                                                                @endforeach
                                                            </p>
                                                            @endif
                                                            @if($row_pf->type!=5) 
                                                            @if(isset($row_pf->point)) {{ $row_pf->point }} @endif
                                                            
                                                             @endif

                                                            </td> </tr>
                                                     @endif
                                                     @endforeach   
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                        @endforeach
                                   
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

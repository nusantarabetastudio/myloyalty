@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-sm-2">
                    <a href="{{ route('instant-gift.create') }}" class="btn btn-primary"><i class="icon md-plus" aria-hidden="true"></i> Create Instant Gift Formula</a>
                </div>
            </div>
            <br>
            <table class="table table-hover dataTable table-striped width-full" id="datatable-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Description</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

            <div class="modal fade bs-modal-sm modal-3d-slit" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <form action="{{ url('instant-gift/destroy') }}" method="post" role="form">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <input type="hidden" name="id" id="instant_id">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Information !</h4>
                            </div>
                            <div class="modal-body">
                                will deleting : <strong id="name"></strong>
                                <br>
                                Are you sure ?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Delete</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        var start;
        jQuery(function ($) {

            $.fn.dataTable.ext.errMode = 'none';

            oTable = $('#datatable-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('instant-gift/data') }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data: 'id', name: 'id'},
                    { data: 'GIFT_DESCRIPTION', name: 'GIFT_DESCRIPTION'},
                    { data: 'GIFT_FROM', name: 'GIFT_FROM'},
                    { data: 'GIFT_TO', name: 'GIFT_TO'},
                    { data: 'GIFT_ACTIVE', name: 'GIFT_ACTIVE'},
                    { data: 'action', name: 'action'}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#instant_id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });
        })

    </script>
@endsection

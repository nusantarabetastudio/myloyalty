@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.min.css') }}">
@endsection

@section('content')
    <div class="panel">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ isset($instantGift) ? route('instant-gift.update', $instantGift->GIFT_RECID) : route('instant-gift.store') }}" class="form-horizontal" method="post">
            {{ csrf_field() }}
            @if(isset($instantGift))
                {{ method_field('PUT') }}
            @endif
            <div class="panel-body">
                <!-- Instant Gift -->
                <div class="panel panel-detail">
                    <div class="panel-body">
                        <h4>Instant Gift</h4>
                        <!-- Description -->
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="description" class="control-label col-md-2 text-left">Description</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="GIFT_DESCRIPTION" id="description" value="{{ isset($instantGift) ? $instantGift->GIFT_DESCRIPTION : old('GIFT_DESCRIPTION') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Date -->
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="description" class="control-label col-md-2 text-left">Applicable From</label>
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm date" id="fromDate" name="GIFT_FROM" value="{{ isset($instantGift) ? $instantGift->GIFT_FROM->format('d/m/Y') : old('GIFT_FROM') }}" required>
                                            <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="description" class="control-label col-md-2 text-left">To </label>
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm date" id="toDate" name="GIFT_TO" value="{{ isset($instantGift) ? $instantGift->GIFT_TO->format('d/m/Y') : old('GIFT_TO') }}" required>
                                            <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Type -->
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label col-md-2 text-left">Type</label>
                                    <div class="col-md-10">
                                        <select name="GIFT_TYPE" class="form-control" id="gift_type">
                                            <option value="" disabled selected>Choose One</option>
                                            @foreach($gift_types as $key => $value)
                                                <option @if(isset($instantGift) && $instantGift->GIFT_TYPE == $key ) selected @endif value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="active" class="col-md-2 text-left">Active</label>
                                    <div class="col-md-10">
                                        <input type="checkbox" id="active" name="GIFT_ACTIVE" data-plugin="switchery" {{ (isset($instantGift) && $instantGift->GIFT_ACTIVE == 1) || !isset($instantGift) ? 'checked'  : '' }} {{ isset($instantGift) ? '' : 'disabled' }} data-size="small">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Filter Amount -->
                <div class="col-md-6 padding-left-0">
                    <div class="panel panel-detail">
                        <div class="panel-body">
                            <h4>Filter Amount</h4>
                            <!-- Amount -->
                            <div class="col-md-12">
                                <label for="sign" class="col-md-2 text-left control-label">Amount (Expense)</label>
                                <div class="col-md-10">
                                    <select name="GIFT_SIGN" id="" class="form-control">
                                        <option value="" selected disabled>Choose One</option>
                                        @foreach($signs as $key => $value)
                                            <option @if(isset($instantGift) && $instantGift->GIFT_SIGN == $key ) selected @endif value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- From Amount and To Amount -->
                            <div class="col-md-12">
                                <div class="col-md-10 col-md-offset-2">
                                    <div class="col-md-4 pad0">
                                        <input type="number" name="GIFT_FRAMOUNT" value="{{ isset($instantGift) ? $instantGift->GIFT_FRAMOUNT : old('GIFT_FRAMOUNT') }}" class="form-control">
                                    </div>
                                    <div class="col-md-4 pad0">
                                        <h3 class="text-center">AND</h3>
                                    </div>
                                    <div class="col-md-4 pad0">
                                        <input type="number" name="GIFT_TOAMOUNT" value="{{ isset($instantGift) ? $instantGift->GIFT_TOAMOUNT : old('GIFT_TOAMOUNT') }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Number of Gift -->
                <div class="col-md-6 padding-right-0">
                    <div class="panel panel-detail">
                        <div class="panel-body">
                            <h4>Number of Gift</h4>
                            <!-- Amount -->
                            <div class="col-md-12">
                                <label for="sign" class="col-md-3 text-left control-label">Number of Gift</label>
                                <div class="col-md-9">
                                    <div class="col-md-2 pad0">
                                        <input type="number" name="GIFT_NUMOFGIFT" class="form-control" value="{{ isset($instantGift) ? $instantGift->GIFT_NUMOFGIFT : old('GIFT_NUMOFGIFT') }}">
                                    </div>
                                    <div class="col-md-10">
                                        <select name="GIFT_PERIOD" class="form-control">
                                            <option value="" selected disabled>Choose One</option>
                                            @foreach($periods as $key => $value)
                                                <option @if(isset($instantGift) && $instantGift->GIFT_PERIOD == $key ) selected @endif value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- From Amount and To Amount -->
                            <div class="col-md-12 margin-top-10">
                                <div class="form-group">
                                    <div class="pull-left">
                                        <label for="sendEmail">
                                            <input id="sendEmail" type="checkbox" name="GIFT_SEND_EMAIL" data-plugin="switchery" data-size="small" {{ isset($instantGift) && $instantGift->GIFT_SEND_EMAIL == 1 ? 'checked' : old('GIFT_SEND_EMAIL') }}> Send to Email
                                        </label>
                                    </div>

                                    <div class="pull-right">
                                        <label for="sendSMS">
                                            <input id="sendSMS" type="checkbox" name="GIFT_SEND_SMS" data-plugin="switchery" data-size="small" value="{{ isset($instantGift) && $instantGift->GIFT_SEND_SMS == 1 ? 'checked' : old('GIFT_SEND_SMS') }}"> Send to SMS
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  Binding -->
                <div class="col-md-12 pad0">
                    <div class="nav-tabs-horizontal nav-tabs-inverse nav-tabs-animate">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li class="active" role="presentation">
                                <a data-toggle="tab" href="#tabProduct" aria-controls="tabProduct"
                                   role="tab" aria-expanded="true">
                                    Product
                                </a>
                            </li>
                            <li role="presentation">
                                <a data-toggle="tab" href="#tabStore" aria-controls="tabStore"
                                   role="tab" aria-expanded="false">
                                    Store
                                </a>
                            </li>
                            <li role="presentation">
                                <a data-toggle="tab" href="#tabEmailSMS" aria-controls="tabEmailSMS"
                                   role="tab" aria-expanded="true">
                                    Email & SMS
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content padding-top-20">
                            <div class="tab-pane active animation-fade" id="tabProduct" role="tabpanel">
                                <div class="col-md-12">
                                    <div class="row mar0">
                                        <button class="btn btn-primary selected-product-btn pull-right marlr5">Select All</button>
                                        <button class="btn btn-danger unselected-product-btn pull-right marlr5">Unselect All</button>
                                    </div>
                                    <table class="table table-hover table-striped width-full">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Actived</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $row)
                                            <tr>
                                                <td>{{ $row->description }}</td>
                                                <td>
                                                    <input type="checkbox" id="inputBasicOn" name="PRDR_PRODUCT[]" value="{{ $row->id }}" data-plugin="switchery" class="product-switch"
                                                    >
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane animation-fade" id="tabStore" role="tabpanel">
                                <div class="col-md-12">
                                    <div class="row mar0">
                                        <button class="btn btn-primary selected-store-btn pull-right marlr5">Select All</button>
                                        <button class="btn btn-danger unselected-store-btn pull-right marlr5">Unselect All</button>
                                    </div>
                                    <br />
                                    <table class="table table-hover dataTable table-striped width-full">
                                        <thead>
                                        <tr>
                                            <th>Store</th>
                                            <th>Actived</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($store as $row)
                                            <tr>
                                                <td>{{ $row->TNT_DESC }}</td>
                                                <td>
                                                    <input type="checkbox"
                                                           @if(isset($instantGift))
                                                               @foreach($instantGift->tenants as $tenant)
                                                                   @if($tenant->GIFT_TENANT == $row->TNT_RECID)
                                                                       checked
                                                                   @endif
                                                               @endforeach
                                                           @endif
                                                       id="inputBasicOn" name="GIFT_TENANT[]" value="{{ $row->TNT_RECID }}" data-plugin="switchery" class="store-switch"
                                                    >
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane animation-fade" id="tabEmailSMS" role="tabpanel">
                                <div class="col-md-12">
                                    <h4>Email Content</h4>
                                    <textarea name="GIFT_EMAIL_MESSAGE" id="email_content" cols="30" rows="10">{{ isset($instantGift) ? $instantGift->GIFT_EMAIL_MESSAGE : old('GIFT_EMAIL_MESSAGE') }}</textarea>
                                    <hr>
                                    <h4>SMS Content</h4>
                                    <textarea class="form-control" name="GIFT_SMS_MESSAGE" id="" cols="160" rows="10">{{ isset($instantGift) ? $instantGift->GIFT_SMS_MESSAGE : old('GIFT_SMS_MESSAGE') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 margin-top-10 row">
                <button class="btn btn-primary pull-right" type="submit">{{ isset($instantGift) ? 'Update' : 'Submit' }}</button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.date').datepicker({
                'format': 'dd/mm/yyyy',
                'orientation': 'bottom left'
            });

            tinymce.init({
                selector: '#email_content',
                height: 500,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                content_css: '//www.tinymce.com/css/codepen.min.css'
            });

            $('.selected-store-btn').click(function(e) {
                e.preventDefault();
                $('#tabStore .store-switch').prop('checked', false);
                $('#tabStore .store-switch').click();
            });

            $('.unselected-store-btn').click(function(e) {
                e.preventDefault();
                $('#tabStore .store-switch').prop('checked', true);
                $('#tabStore .store-switch').click();
            });

            $('.selected-product-btn').click(function(e) {
                e.preventDefault();
                $('#tabProduct .product-switch').prop('checked', false);
                $('#tabProduct .product-switch').click();
            });

            $('.unselected-product-btn').click(function(e) {
                e.preventDefault();
                $('#tabProduct .product-switch').prop('checked', true);
                $('#tabProduct .product-switch').click();
            });
        })
    </script>
@endsection
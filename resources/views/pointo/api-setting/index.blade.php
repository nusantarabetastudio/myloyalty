@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection
@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <form action="{{ route('pointo.api-setting.update')}}" class="form-horizontal" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-12">
                                <fieldset>
                                    <legend>Mypoint API</legend>
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-2">API Url :</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="api_url" id="api-url" value="{{ $pointo->url }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-2">User :</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="user" id="user" value="{{ $pointo->username }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-2">Password :</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="password" id="password" value="{{ $pointo->password }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-2">Token :</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="token" id="token" value="{{ $pointo->token }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-2">Customer :</label>
                                        <div class="col-md-10">
                                            <select name="customer" id="customer" class="form-control select2">
                                                <option value="" disabled selected>Choose One</option>
                                                @foreach($customers as $customer)
                                                    @if($client->CLN_APEX_CUSTOMER == $customer->CUST_RECID)
                                                        <option value="{{ $customer->CUST_RECID }}" selected>{{ $customer->CUST_NAME }}</option>
                                                    @else
                                                        <option value="{{ $customer->CUST_RECID }}">{{ $customer->CUST_NAME }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label col-md-2">Status :</label>
                                        <div class="col-md-10">
                                            <input type="checkbox" name="status" data-plugin="switchery"  checked>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary mart20">Update</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script type="text/javascript">

        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
    </script>
@endsection
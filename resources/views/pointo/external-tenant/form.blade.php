@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection
@section('content')
    @if(Session::has('notif_success'))
        <div class="alert alert-danger">
            {{ Session::get('notif_success') }}
        </div>
    @endif
    @if(count($errors) >0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method="POST" action="{{ isset($ExternalTenant) ? route('pointo.external-tenant.update', $ExternalTenant->TNT_RECID) : route('pointo.external-tenant.store')}}" class="form-horizontal">
        {{ csrf_field() }}
        {{ isset($ExternalTenant) ? method_field('PUT') : ''}}
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <div class="col-sm-12">
                                    <label class="col-sm-3 control-label text-left">Description : </label>
                                    <div class="col-sm-9">
                                        <input type="text" name="description" value="{{ isset($ExternalTenant) ? $ExternalTenant->TNT_DESC : '' }}" class="form-control input-sm " required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-12">
                                    <label class="col-sm-3 control-label text-left">Card Name : </label>
                                    <div class="col-sm-9">
                                        <input type="text" name="card_name" value="{{ isset($ExternalTenant) ? $ExternalTenant->TNT_TEXT : '' }}" class="form-control input-sm " required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-12">
                                    <label class="col-sm-3 control-label text-left">Card Code : </label>
                                    <div class="col-sm-9">
                                        <input type="text" name="card_code" value="{{ isset($ExternalTenant) ? $ExternalTenant->TNT_UNIT : '' }}" class="form-control input-sm " required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-12">
                                    <label class="col-sm-3 control-label text-left">Status : </label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" name="status"{{ isset($ExternalTenant) && ($ExternalTenant->TNT_ACTIVE == 1) ? ' checked' : '' }}  data-plugin="switchery" />
                                    </div>
                                </div>
                            </div>
							<div class="form-group pull-right">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary mart20">{{ isset($ExternalTenant) ? 'Update' : 'Submit' }}</button>
                                </div>
							</div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
    </script>
@endsection
@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">

@endsection
@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('notif_success'))
                <div class="alert alert-success">
                    {{ Session::get('notif_success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <a href="{{ route('pointo.internal-card-link.create') }}" class="btn btn-primary">Add Internal Card</a>
                    <br/><br/>
                    <table class="table table-hover dataTable table-striped width-full" id="datatable-table-cardLink">
                        <thead>
                            <tr>
                                <th>Internal Card Code</th>
                                <th>Card Type</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ route('pointo.internal-card-link.destroy') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" id="modal-id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#datatable-table-cardLink').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('pointo.internal-card-link.data') }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data: 'CARD_DESC', name: 'CARD_DESC'},
                    { data: 'parent.CARD_DESC', name: 'parent.CARD_DESC', searchable: false, orderable: false},
                    { data: 'status', name: 'status', searchable: false, orderable: false},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ]
            });
        })

        $('#delete-modal').on('show.bs.modal', function(event){
            $('#modal-id').val($(event.relatedTarget).data('id'));
        });
    </script>
@endsection
@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('notif_success'))
                <div class="alert alert-success">
                    {{ Session::get('notif_success') }}
                </div>
            @endif
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    {{ Session::has('notif_error') ? Session::get('notif_error') : ''}}
                </div>
            @endif
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <h1>Migrate {{ ucfirst($name) }} Master Data</h1>
                    <form id='form' action='/upload/{{ $name }}' method='POST' class='form-horizontal' enctype='multipart/form-data'>
                        {!! csrf_field() !!}
                        <input type='file' name='csv_file' />
                        <br />
                        <button type='submit' id='submit-btn' class='btn btn-primary'>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#submit-btn').click(function() {
            $(this).attr('disabled', 'disabled');
            $('#form').submit();
        });
    });
</script>
@endsection
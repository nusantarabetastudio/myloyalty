@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}">
@endsection

@section('content')
    <div clas="row row-lg">
        <div class="col-lg-12">

 <div class="panel">
                        <div class="panel-detail">
                            <div class="panel-body">
                                <div class="col-lg-8">
                                    <table class="table table-hover" id="d-table">
                                        <thead>
                                        <tr>
                                            <th>Repeat</th>
                                            <th>Repeat Every</th>
                                            <th>Start On</th>
                                            <th>End</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        
                                        @foreach($expired_data as $row)
                                            <tr>
                                                <td>{{ ($row['REPEAT']==0)?'Monthly':'Yearly'}}</td>
                                                <td>{{$row['EVERY']}} {{ ($row['REPEAT']==0)?'Month':'Year'}}</td>
                                                <td>{{ Carbon\Carbon::parse($row['START_ON'])->format('d-M-Y')}}</td>
                                                <td>
                                                @if ($row['ENDS']==0) 
                                                    Never
                                                @elseif ($row['ENDS']==1) 
                                                    After &nbsp;
                                                    {{$row['ENDS_AFTER']}} {{ ($row['REPEAT']==0)?'Month':'Year'}}    
                                                @elseif ($row['ENDS']==2) 
                                                    On 
                                                    {{ Carbon\Carbon::parse($row['ENDS_ON'])->format('d-M-Y')}}
                                                @else
                                                    -
                                                @endif

                                                </td>
                                            </tr>
                                          @endforeach  
                                           
                                        </tbody>
                                    </table>
                                </div>
                                 <div class="col-lg-4">
                                   <h4>Point Expired History</h4>
                                   <div class="col-md-12 center">

                                       <span class="text-danger">Last <b>POINT EXPIRED</b> On : {{ ($last_on['PE_DATETIME']!=null)?Carbon\Carbon::parse($last_on['PE_DATETIME'])->format('d-M-Y H:i:s'):'-' }}</span>
                                  <br/> <hr/> <br/>
                                       <span class="text-danger">Last <b>ROLLBACK</b> On : {{ ($last_on['ROLLBACK_DATETIME']!=null)?Carbon\Carbon::parse($last_on['ROLLBACK_DATETIME'])->format('d-M-Y H:i:s'):'-' }}</span>

                                       <form method="POST" action="{{ route('expired-point.rollback')}}" class="form-horizontal">
                                       {{ csrf_field() }}
                                           <button type="submit" onclick="return confirm('Are You Sure To ROLLBACK Last Expired Point ?')" class="btn btn btn-warning mart20">Rollback</button>
                                       </form>


                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <div class="nav-tabs-horizontal">
                <div class="tab-pane active">

                
                    <div class="panel">
                        <div class="panel-body">

                            @if (Session::has('notif_success'))
                                <div class="alert alert-success">
                                    {{ Session::get('notif_success') }}
                                </div>
                            @endif
                            <form method="POST" action="{{ route('expired-point.update')}}" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <input type="hidden" value="{{ $point_expired->EXPIRED_RECID }}" name="id">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <h1 class="page-title">Point Expired Setting</h1>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <label class="col-sm-3 control-label text-left" for="repeats">Repeats</label>
                                    <div class="col-sm-9">
                                        <select name="REPEAT" id="repeats" class="form-control">
                                            <option value="" selected disabled>Choose One</option>
                                            @foreach($repeats as $key => $value)
                                                @if($point_expired->REPEAT == $key)
                                                    <option value="{{ $key }}" selected>{{ $value }}</option>
                                                @else
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <label class="col-sm-3 control-label text-left" for="repeats_every">Repeat Every</label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-3 pad0 mar0">
                                            <select name="EVERY" class="form-control" id="repeats_every">
                                                <option value="" selected disabled>Choose One</option>
                                                @foreach($numbers as $row)
                                                    @if($point_expired->EVERY == $row)
                                                        <option value="{{ $row }}" selected>{{ $row }}</option>
                                                    @else
                                                        <option value="{{ $row }}">{{ $row }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-9" id="years">
                                            <h4 class="margin-top-5">Years</h4>
                                        </div>
                                        <div class="col-sm-9" id="month">
                                            <h4 class="margin-top-5">Months</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm" id="repeats-by-container">
                                    <label class="col-sm-3 control-label text-left" for="start_on">Repeat by</label>
                                    <div class="col-sm-3">
                                        <label for="0"><input type="radio" id="0" name="REPEAT_BY" {{ ($point_expired->REPEAT_BY == 0) ? 'checked' : '' }} value="0"> Day of the Month</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="1"><input type="radio" id="1" name="REPEAT_BY" {{ ($point_expired->REPEAT_BY == 1) ? 'checked' : '' }} value="1"> Day of the Week</label>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <label class="col-sm-3 control-label text-left" for="start_on">Starts On</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control date" id="date" name="START_ON" value="{{ $point_expired->START_ON->format('d/m/Y') }}" placeholder="DD/MM/YYYY" required>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <label class="col-sm-3 control-label text-left" for="start_on">Ends </label>
                                    <div class="col-sm-2">
                                        <div class="row">
                                            <label for="never">
                                                <input type="radio" name="ENDS" value="0" {{ $point_expired->ENDS == 0 ? 'checked' : null }} id="never">
                                                Never
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label for="after">
                                                <input type="radio" name="ENDS" value="1" {{ $point_expired->ENDS == 1 ? 'checked' : null }} id="after">
                                                After
                                            </label>

                                            <input type="number" class="form-control" name="ENDS_AFTER" value="{{ $point_expired->ENDS_AFTER }}" id="endAfter">
                                        </div>
                                        <div class="row">
                                            <label for="endDate">
                                                <input type="radio" name="ENDS" value="2" {{ $point_expired->ENDS == 2 ? 'checked' : null }} id="endDate">
                                                On
                                            </label>

                                            <input type="text" class="form-control datetime" placeholder="hh:mm:ss" name="ENDS_ON" id="endOn" value="{{ $point_expired->ENDS_ON }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary mart20">Done</button>
                                        <button type="button" class="btn btn-default mart20" onclick="window.location.href='{{ route('expired-point.edit') }}'">Cancel</button>
                                    </div>
                                </div>
                            </form>


                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                <form method="POST" action="{{url('expired-point-proses')}}" class="form-horizontal" style="text-align: center;">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="1" name="manual">
                                    <button type="submit" onclick="return confirm('Are You Sure To Manual Procces POINT EXPIRED ?')" class="btn btn btn-danger mart20">MANUAL PROSES POINT EXPIRED</button>
                                </form>
                                    </div>
                                </div>



                        </div>
                    </div>

                   
                </div>
                
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <!-- Datetime picker -->
    <script src="{{ asset('assets/global/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
        $('#years').hide();
        $('#month').hide();
        $('#repeats-by-container').hide();
        var valEnds;
        var val;
        $(document).ready(function() {
            $('.date').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#d-table').DataTable();

            $('#endOn').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });

            @if(isset($point_expired))

                @if($point_expired->REPEAT == 0)
                    $('#month').show();
                    $('#years').hide();
                    $('#repeats-by-container').show();
                @elseif($point_expired->REPEAT == 1)
                    $('#month').hide();
                    $('#years').show();
                    $('#repeats-by-container').hide();
                @endif

                $('#repeats').on('change', function() {
                    val = $(this).val();

                    if(val == 0) {
                        $('#month').show();
                        $('#years').hide();
                        $('#repeats-by-container').show();
                    } else if(val == 1){
                        $('#month').hide();
                        $('#years').show();
                        $('#repeats-by-container').hide();
                    }
                });

                @if($point_expired->ENDS == 0 && $point_expired->ENDS !== null && $point_expired->ENDS != '')
                    $('#endAfter').attr('disabled', true);
                    $('#endOn').attr('disabled', true);
                    $('#endOn').val(null);
                    $('#endAfter').val(null);
                @elseif($point_expired->ENDS == 1 && $point_expired->ENDS !== null && $point_expired->ENDS != '')
                    $('#endAfter').attr('disabled', false);
                    $('#endOn').attr('disabled', true);
                    $('#endOn').val(null);
                @elseif($point_expired->ENDS == 2 && $point_expired->ENDS !== null && $point_expired->ENDS != '')
                    $('#endAfter').attr('disabled', true);
                    $('#endOn').attr('disabled', false);
                    $('#endAfter').val(null);
                @else
                    $('#endAfter').attr('disabled', true);
                    $('#endOn').attr('disabled', true);
                    $('#endAfter').val(null);
                    $('#endOn').val(null);
                @endif



            $('input[name="ENDS"]').click(function () {
                valEnds = $(this).val();

                if(valEnds == 0) {
                    $('#endAfter').attr('disabled', true);
                    $('#endOn').attr('disabled', true);
                    $('#endOn').val(null);
                    $('#endAfter').val(null);
                } else if(valEnds == 1) {
                    $('#endAfter').attr('disabled', false);
                    $('#endOn').attr('disabled', true);
                    $('#endOn').val(null);

                } else if(valEnds == 2) {
                    $('#endAfter').attr('disabled', true);
                    $('#endOn').attr('disabled', false);
                    $('#endAfter').val(null);
                } else {
                    $('#endAfter').attr('disabled', true);
                    $('#endOn').attr('disabled', true);
                    $('#endAfter').val(null);
                    $('#endOn').val(null);
                }
            });
            @endif

        });
    </script>
@endsection
@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">

@endsection
@section('content')
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <div class="tab-content padding-top-10">
                            <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                                <div class="panel">
                                    <div class="panel-body">
                                        @if(Session::has('msg'))
                                            <div class="alert alert-success">
                                                {{ Session::get('msg') }}
                                            </div>
                                        @endif
                                        @if(Session::has('notif_error'))
                                            <div class="alert alert-danger">
                                                {{ Session::get('notif_error') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="row row-lg">
                                        <!-- @if( ! isset($user)) -->
                                        <div class="col-lg-6">
                                            <a href="javascript:void(0)" id="btn-new" class="btn btn-primary">New User</a>
                                        </div>
                                            <br><br>
                                            <div class="col-lg-12">
                                                <table class="tablesaw table-striped  dataTable table-bordered table-hover" id="datatable-table">
                                                    <thead>
                                                    <tr>
                                                        <th data-tablesaw-sortable-col>Username</th>
                                                        <th data-tablesaw-sortable-col>Role</th>
                                                        <th data-tablesaw-sortable-col>First Name</th>
                                                        <th data-tablesaw-sortable-col>Last Name</th>
                                                        <th data-tablesaw-sortable-col>Email</th>
                                                        <th data-tablesaw-sortable-col>Phone</th>
                                                        <th data-tablesaw-sortable-col>Store</th>
                                                        <th data-tablesaw-sortable-col>Active</th>
                                                        <th data-tablesaw-sortable-col>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($users as $data)
                                                    <tr>
                                                        <td>{{ $data->USER_USERNAME }}</td>
                                                        <td>{{ $data->role->ROLE_NAME }}</td>
                                                        <td>{{ $data->USER_FIRST_NAME }}</td>
                                                        <td>{{ $data->USER_LAST_NAME }}</td>
                                                        <td>{{ $data->USER_EMAIL }}</td>
                                                        <td>{{ $data->USER_PHONE }}</td>
                                                        <td>{{ $data->tenant->TNT_DESC }}</td>
                                                        <td>{!! $data->USER_ACTIVE == 1 ? '<span class="text-success md-check"></span>' : '<span class="text-danger md-close"></span>' !!}</td>
                                                        <td>
                                                            <a href="{{ route('config.user.edit', $data->USER_USERNAME) }}" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
                                                            <button class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row waves-effect waves-classic" data-toggle="modal" data-target="#modalDelete" data-username="{{ $data->USER_USERNAME }}" ><i class="icon md-delete"></i></button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            <!-- @endif -->
                                                <div class="panel" id="form-input" style="display: {{ isset($user) ? 'block' : 'none' }} ;">
                                                    <div class="panel-body">
                                                        <form method="POST" action="{{ isset($user) ? route('config.user.update', $user->USER_USERNAME) : route('config.user.store') }}" class="form-horizontal">
                                                            {{ csrf_field() }}
                                                            {{ isset($user) ? method_field('PUT') : ''}}
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <label class="control-label">First Name</label>
                                                                    <input type="text" name="firstName" class="form-control" required value="{{ $user->USER_FIRST_NAME or '' }}">
                                                                    <label class="control-label">Username</label>
                                                                    <input type="text" name="username" class="form-control" required value="{{ $user->USER_USERNAME or '' }}" {{ isset($user) ? 'disabled' : '' }}>
                                                                    <label class="control-label">Phone</label>
                                                                    <input type="text" name="phone" class="form-control" value="{{ $user->USER_PHONE or '' }}">
                                                                    <label class="control-label">Store</label>
                                                                    <select name="tenantId" id="" class="form-control select2">
                                                                        <option value=""></option>
                                                                        @foreach($tenants as $tenant)
                                                                            @if(isset($user) && $user->USER_TNT_RECID == $tenant->id)
                                                                                <option value="{{ $tenant->id }}" selected>{{ $tenant->description }} - {{ $tenant->code }}</option>
                                                                            @else
                                                                                <option value="{{ $tenant->id }}">{{ $tenant->description }} - {{ $tenant->code }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label class="control-label">Last Name</label>
                                                                    <input type="text" name="lastName" class="form-control" required value="{{ $user->USER_LAST_NAME or '' }}">
                                                                    <label class="control-label">Password</label>
                                                                    <input type="password" class="form-control input-sm" name="password" {{ isset($user) ? 'disabled' : '' }} required>
                                                                    <label class="control-label">Role</label>
                                                                    <select name="roleId" class="form-control select2" id="roles">
                                                                        <option value=""></option>
                                                                        @foreach($roles as $role)
                                                                            @if(isset($user) && $user->USER_ROLE_RECID == $role->ROLE_RECID)
                                                                                <option value="{{ $role->ROLE_RECID }}" selected>{{ $role->ROLE_NAME }}</option>
                                                                            @else
                                                                                <option value="{{ $role->ROLE_RECID }}">{{ $role->ROLE_NAME }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label class="control-label">Email</label>
                                                                    <input type="email" name="email" class="form-control" value="{{ $user->USER_EMAIL or '' }}" required>
                                                                    <label class="control-label">Address</label>
                                                                    <input type="text" name="address" class="form-control" value="{{ $user->USER_ADDRESS or '' }}" required>
                                                                    <label class="control-label">Gender</label>
                                                                    <select name="gender" id="" class="form-control">
                                                                        <option value="">Choose One</option>
                                                                        <option value="M" {{ isset($user) && $user->USER_GENDER == 'M' ? 'selected' :'' }}>Male</option>
                                                                        <option value="F" {{ isset($user) && $user->USER_GENDER == 'F' ? 'selected' : '' }}>Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="com-sm-4 pull-right">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($user) ? 'Update' : 'Submit' }}</button>
                                                                    <button type="reset" class="btn btn-default waves-effect waves-light">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <!-- Modal -->
    <div class="modal fade modal-3d-flip-vertical" id="modalDelete" aria-hidden="true" aria-labelledby="examplePositionCenter"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <form action="{{ url('config/user/destroy') }}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="username" id="username">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Confirm..</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        var oTable;
        var val =''; // role id
        $(document).ready(function(){

            oTable = $('#datatable-table').DataTable({
                processing: true
            });

            $('#btn-new').on('click', function(e) {
                $('#form-input').toggle();
            });

            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });

            $('#modalDelete').on('show.bs.modal', function(event){
                $('#username').val($(event.relatedTarget).data('username'));
            });
        });
    </script>
@endsection
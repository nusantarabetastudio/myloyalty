@extends('layouts.loyalty')
@section('content')
	<div clas="row row-lg">
		<div class="col-lg-12">
			<div class="nav-tabs-horizontal">
				<div class="tab-pane active" row="tabpanel">
					<div class="panel">
						@if (Session::has('notif_success'))
							<div class="alert alert-success">
								{{ Session::get('notif_success') }}
							</div>
						@endif
						<div class="panel-body">
							<form method="POST" action="{{ route('config.general-preference.update')}}" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
								<div class="form-group form-material-sm">
									<div class="col-sm-12">
										<h1 class="page-title">General Setting</h1>
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">Completed Profile Reward</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="completed_profile_reward" value="{{ $completedProfileReward }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">Registration Earn Reward</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="registration_earn_reward" value="{{ $registrationEarnReward }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">Super Administrator Role Name</label>
									<div class="col-sm-9">
										<select name="superadmin" class="form-control">
											@foreach($roles as $role)
												<option value="{{ $role->ROLE_RECID }}"{{ ($role->ROLE_RECID == $superadmin) ? ' selected' : '' }}>{{ $role->ROLE_NAME }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">Administrator Role Name</label>
									<div class="col-sm-9">
										<select name="admin" class="form-control">
											@foreach($roles as $role)
												<option value="{{ $role->ROLE_RECID }}"{{ ($role->ROLE_RECID == $admin) ? ' selected' : '' }}>
													{{ $role->ROLE_NAME }}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">Supervisor Role Name</label>
									<div class="col-sm-9">
										<select name="supervisor" class="form-control">
											@foreach($roles as $role)
												<option value="{{ $role->ROLE_RECID }}"{{ ($role->ROLE_RECID == $supervisor) ? ' selected' : '' }}>
													{{ $role->ROLE_NAME }}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">Customer Service Role Name</label>
									<div class="col-sm-9">
										<select name="cs" class="form-control">
											@foreach($roles as $role)
												<option value="{{ $role->ROLE_RECID }}"{{ ($role->ROLE_RECID == $cs) ? ' selected' : '' }}>
													{{ $role->ROLE_NAME }}
												</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-left">Point Value</label>
									<div class="col-sm-1 text-right">
										<input type="checkbox" name="active" value="1" {{ !isset($client->CLN_PV_ACTIVE) ? 'checked' : ''}}>
									</div>
									<div class="col-sm-8">
										<input type="text" name="point_value" value="{{ $client->CLN_PV }}" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-left">Minimum Transaction Bonus</label>
									<div class="col-sm-9">
										<input type="number" name="minimum_transaction_bonus" value="{{ $minimum_transaction_bonus }}" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-left">Minimum Transaction Reward</label>
									<div class="col-sm-9">
										<input type="number" name="minimum_transaction_reward" value="{{ $minimum_transaction_reward }}" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label text-left">Expired Date <small>(Format: MM-DD)</small></label>
									<div class="col-sm-9">
										<input type="text" name="expired_date" value="{{ $expiredDate }}" class="form-control">
									</div>
								</div>
								<div class="form-group form-material-sm">
									<div class="col-sm-12">
										<h1 class="page-title">SMTP Setting</h1>
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">SMTP Host</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="smtp_host" value="{{ $smtp_host }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">SMTP Post</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="smtp_port" value="{{ $smtp_port }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">SMTP Encryption</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="smtp_encryption" value="{{ $smtp_encryption }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">SMTP Username</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="smtp_username" value="{{ $smtp_username }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
									<label class="col-sm-3 control-label text-left">SMTP Password</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="smtp_password" value="{{ $smtp_password }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary mart20">Update</button>
                                    </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
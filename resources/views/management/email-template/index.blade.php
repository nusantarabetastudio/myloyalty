@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/fonts/font-awesome/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/summernote/summernote.css') }}">
@endsection
@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-body">
					<h3>Setting Email Template</h3>
					<div class="tab-content">
						<div class=" tab-pane animation-fade active" id="category-1" role="tabpanel">
							<div class="panel-group panel-group-simple panel-group-continuous" id="accordion2"
							 aria-multiselectable="true" role="tablist">
								<div class="panel">
									<div class="panel-heading" id="question-1" role="tab">
										<a class="panel-title" aria-controls="earningEmail" aria-expanded="true" data-toggle="collapse"
										 href="#earningEmail" data-parent="#accordion2">
											<h3>Earning e-mail</h3>
										</a>
									</div>
									<div class="panel-collapse collapse in" id="earningEmail" aria-labelledby="question-1"
									 role="tabpanel">
										<div class="row">
											<div class="panel">
												<div class="panel-body">
												  <div id="earning-email" data-plugin="summernote">
												  	Earning-Email:
												  </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading" id="question-2" role="tab">
										<a class="panel-title" aria-controls="redeemEarning" aria-expanded="false" data-toggle="collapse" href="#redeemEarning" data-parent="#accordion2">
											<h3>Redeem e-mail</h3>
										</a>
									</div>
									<div class="panel-collapse collapse" id="redeemEarning" aria-labelledby="question-2"
									 role="tabpanel">
										<div class="row">
											<div class="panel">
												<div class="panel-body">
												  <div id="redeem-email" data-plugin="summernote">
												  	Redeem-Email:
												  </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading" id="question-2" role="tab">
										<a class="panel-title" aria-controls="new-cust-email" aria-expanded="false" data-toggle="collapse" href="#new-cust-email" data-parent="#accordion2">
											<h3>New Customer E-mail</h3>
										</a>
									</div>
									<div class="panel-collapse collapse" id="new-cust-email" aria-labelledby="question-2"
									 role="tabpanel">
										<div class="row">
											<div class="panel">
												<div class="panel-body">
												  <div id="redeem-email" data-plugin="summernote">
												  	new customer e-mail:
												  </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
		            <div class="form-group ">
		                <div class="col-sm-12">
		                	<div class="col-sm-3"><h3>E-mail Active :</h3></div>
		                    <div class="col-sm-9">
		                        <input type="checkbox" name="status"  data-plugin="switchery" />
		                    </div>
		                </div>
		            </div>
					<div class="form-group pull-right">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary mart20">Submit</button>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('assets/global/vendor/animsition/animsition.js') }}"></script>
	<script src="{{ asset('assets/global/vendor/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('assets/global/js/components/summernote.js') }}"></script>
	<script src="{{ asset('assets/examples/js/forms/editor-summernote.js') }}"></script>
@endsection
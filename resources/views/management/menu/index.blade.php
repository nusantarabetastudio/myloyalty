@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">

@endsection
@section('content')
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <div class="tab-content padding-top-10">
                            <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                                <div class="panel">
                                    <div class="panel-body">
                                        @if(Session::has('msg'))
                                            <div class="alert alert-success">
                                                {{ Session::get('msg') }}
                                            </div>
                                        @endif
                                        @if(Session::has('notif_error'))
                                            <div class="alert alert-danger">
                                                {{ Session::get('notif_error') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="row row-lg">
                                        <!-- @if( ! isset($menu)) -->
                                        <div class="col-lg-6">
                                            <a href="javascript:void(0)" id="btn-new" class="btn btn-primary">New Menu</a>
                                        </div>
                                            <br><br>
                                            <div class="col-lg-12">
                                                <table class="tablesaw table-striped  dataTable table-bordered table-hover" id="datatable-table">
                                                    <thead>
                                                    <tr>
                                                        <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Menu</th>
                                                        <th data-tablesaw-sortable-col data-tablesaw-priority="1">Route</th>
                                                        <th data-tablesaw-sortable-col data-tablesaw-priority="2">Parent</th>
                                                        <th data-tablesaw-sortable-col data-tablesaw-priority="3">Icon</th>
                                                        <th data-tablesaw-sortable-col data-tablesaw-priority="4">Active</th>
                                                        <th data-tablesaw-sortable-col data-tablesaw-priority="5">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($menus as $data)
                                                    <tr>
                                                        <td>{{ $data->MENU_NAME or '-' }}</td>
                                                        <td>{{ $data->MENU_ROUTES or '-' }}</td>
                                                        <td>{{ $data->parent_menu->MENU_NAME or '-' }}</td>
                                                        <td>{{ $data->MENU_ICON or '-' }}</td>
                                                        <td>{{ $data->MENU_ACTIVE }}</td>
                                                        <td>
                                                            <a href="{{ route('config.menu.edit', $data->MENU_RECID) }}" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
                                                            <button class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row waves-effect waves-classic" data-toggle="modal" data-target="#modalDelete" data-menu-id="{{ $data->MENU_RECID }}" ><i class="icon md-delete"></i></button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            <!-- @endif -->
                                                <div class="panel" id="form-input" style="display: {{ isset($menu) ? 'block' : 'none' }} ;">
                                                    <div class="panel-body">
                                                        <form method="POST" action="{{ isset($menu) ? route('config.menu.update', $menu->MENU_RECID) : route('config.menu.store') }}" class="form-horizontal">
                                                            {{ csrf_field() }}
                                                            {{ isset($menu) ? method_field('PUT') : ''}}
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="" class="control-label">Menu Name :</label>
                                                                    <div>
                                                                        <input type="text" name="name" class="form-control" value="{{ $menu->MENU_NAME or old('name') }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="control-label">Route :</label>
                                                                    <div>
                                                                        <input type="text" name="route" class="form-control" value="{{ $menu->MENU_ROUTES or old('route') }}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="" class="control-label">Parent : </label>
                                                                    <div>
                                                                        <select name="parent" id="parent" class="form-control select2">
                                                                            <option value=""></option>
                                                                            @foreach($menus as $data)
                                                                                @if(isset($menu) && $menu->MENU_PARENT == $data->MENU_RECID )
                                                                                    <option value="{{ $data->MENU_RECID }}" selected>{{ $data->MENU_NAME }}</option>
                                                                                @else
                                                                                    <option value="{{ $data->MENU_RECID }}">{{ $data->MENU_NAME }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="" class="control-label">Icon :</label>
                                                                    <div>
                                                                        <input type="text" name="icon" class="form-control" value="{{ $menu->MENU_ICON or old('icon') }}">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="" class="control-label">Active :</label>
                                                                    <div>
                                                                        <input type="checkbox" class="form-control" name="active" data-plugin="switchery" data-size="small" {{ (isset($menu) && $menu->MENU_ACTIVE == 1) || !isset($menu) ? 'checked' : '' }} {{ isset($menu) ? '' : 'disabled' }}>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($menu) ? 'Update' : 'Submit' }}</button>
                                                                    <button type="reset" class="btn btn-default waves-effect waves-light">Reset</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <!-- Modal -->
    <div class="modal fade modal-3d-flip-vertical" id="modalDelete" aria-hidden="true" aria-labelledby="examplePositionCenter"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <form action="{{ url('config/menu/destroy') }}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="menuId" id="menu_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Confirm..</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        var oTable;
        $(function(){
            oTable = $('#datatable-table').DataTable({
                processing: true
            });
        });
        $('#btn-new').on('click', function(e) {
            $('#form-input').toggle();
        });
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });

        $('#modalDelete').on('show.bs.modal', function(event){
            $('#menu_id').val($(event.relatedTarget).data('menu-id'));
        });
    </script>
@endsection
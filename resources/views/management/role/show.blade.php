@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-treeview/bootstrap-treeview.min.css') }}">
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <form class="form-horizontal">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="" class="control-label col-sm-3">Role Name</label>
                        <div class="col-sm-9">
                           <div class="form-control-static">{{ $role->ROLE_NAME }}</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-3">Status</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">{{ $role->ROLE_ACTIVE == 1 ? 'Active' : 'In-Active' }}</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/bootstrap-treeview/bootstrap-treeview.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/bootstrap-treeview.min.js') }}"></script>
    <script src="{{ asset('assets/examples/js/advanced/treeview.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>
@endsection
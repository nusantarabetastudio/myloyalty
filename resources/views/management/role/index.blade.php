@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">

@endsection
@section('content')
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <div class="tab-content padding-top-10">
                            <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                                <div class="panel">
                                    <div class="panel-body">
                                        @if(Session::has('msg'))
                                            <div class="alert alert-success">
                                                {{ Session::get('msg') }}
                                            </div>
                                        @endif
                                        @if(Session::has('notif_error'))
                                            <div class="alert alert-danger">
                                                {{ Session::get('notif_error') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="row row-lg">
                                        <!-- @if( ! isset($role)) -->
                                        <div class="col-lg-6">
                                            <a href="javascript:void(0)" id="btn-new" class="btn btn-primary">New Role</a>
                                        </div>
                                            <br><br>
                                            <div class="col-lg-12">
                                                <table class="tablesaw table-striped  dataTable table-bordered table-hover" id="datatable-table">
                                                    <thead>
                                                    <tr>
                                                        <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Role</th>
                                                        <th data-tablesaw-sortable-col data-tablesaw-priority="1">Active</th>
                                                        <th data-tablesaw-sortable-col data-tablesaw-priority="2">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($roles as $data)
                                                    <tr>
                                                        <td>{{ $data->ROLE_NAME or '-' }}</td>
                                                        <td>{{ $data->ROLE_ACTIVE or '-' }}</td>
                                                        <td>
                                                            <a href="{{ route('config.role.show', $data->ROLE_RECID) }}" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row waves-effect waves-classic" data-toggle="tooltip" data-original-title="Detail"><i class="icon md-search-in-file" aria-hidden="true"></i></a>
                                                            <a href="{{ route('config.role.edit', $data->ROLE_RECID) }}" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
                                                            <button class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row waves-effect waves-classic" data-toggle="modal" data-target="#modalDelete" data-role-id="{{ $data->ROLE_RECID }}" ><i class="icon md-delete"></i></button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            <!-- @endif -->
                                                <div class="panel" id="form-input" style="display: {{ isset($role) ? 'block' : 'none' }} ;">
                                                    <div class="panel-body">
                                                        <form method="POST" action="{{ isset($role) ? route('config.role.update', $role->ROLE_RECID) : route('config.role.store') }}" class="form-horizontal">
                                                            {{ csrf_field() }}
                                                            {{ isset($role) ? method_field('PUT') : ''}}
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="" class="control-label">Role Name :</label>
                                                                    <div>
                                                                        <input type="text" name="name" class="form-control" value="{{ $role->ROLE_NAME or old('name') }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    @if(isset($role))
                                                                        <div style="overflow: scroll; height: 200px;">
                                                                            <ul>
                                                                                @foreach($currentMenus as $id => $name)
                                                                                    <li>
                                                                                        {{ $name }}
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    @else

                                                                    @endif
                                                                    <label for="" class="control-label">Access-able Menu : </label>
                                                                    <select id="new-menu" class="form-control select2 pull-left">
                                                                        <option value=""></option>
                                                                        @foreach($menus as $data)
                                                                                <option value="{{ $data->MENU_RECID }}">{{ $data->MENU_NAME }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                        <button type="button" class="btn btn-primary pull-right" id="addMenu">Add Menu</button>
                                                                    <div class="form-group form-material-sm">
                                                                        <div class="col-sm-12">
                                                                            <table class="table table-hover dataTable table-striped width-full" id="table-container">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>Menu Name</th>
                                                                                    <th>Action</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody id="material-container">
                                                                                @if(isset($role))
                                                                                    @foreach($currentMenus as $menuId => $menuName)
                                                                                        <tr>
                                                                                            <td>{{ $menuName }}</td>
                                                                                            <input type="hidden" name="menuIds[]" value="{{ $menuId }}">
                                                                                            <td><a class="removeMenu" href="#">Delete</a></td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                @endif
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="control-label">Active :</label>
                                                                    <div>
                                                                        <input type="checkbox" id="" class="form-control" name="active" data-plugin="switchery" data-size="small" {{ (isset($role) && $role->ROLE_ACTIVE == 1) || !isset($role) ? 'checked' : '' }} {{ isset($role) ? '' : 'disabled' }}>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($role) ? 'Update' : 'Submit' }}</button>
                                                                    <button type="reset" class="btn btn-default waves-effect waves-light">Reset</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <!-- Modal -->
    <div class="modal fade modal-3d-flip-vertical" id="modalDelete" aria-hidden="true" aria-labelledby="examplePositionCenter"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <form action="{{ url('config/role/destroy') }}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="roleId" id="role_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Confirm..</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal -->
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var oTable;

            var addMenuVal = $('#new-menu').val();

            if(addMenuVal == '') {
                $('#addMenu').attr('disabled', true);
            } else {
                $('#addMenu').attr('disabled', false);
            }

            $(function(){
                oTable = $('#datatable-table').DataTable({
                    processing: true
                });
            });
            $('#btn-new').on('click', function() {
                $('#form-input').toggle();
            });

            $('.select2').select2({
                'placeholder' : 'Choose',
                'width' : '50%'
            });

            $('#modalDelete').on('show.bs.modal', function(event){
                $('#role_id').val($(event.relatedTarget).data('role-id'));
            });

            $('#new-menu').on('change', function () {
                if(addMenuVal !== null) {
                    $('#addMenu').attr('disabled', false);
                }
            });
            
            $('#addMenu').click(function () {
                var menuName = $('#new-menu option:selected').text();
                var btnDelete = '<a class="removeMenu" href="#">Delete</a>';
                var menuHiddenId = '<input type="hidden" value="'+ $('#new-menu').val() +'" name="menuIds[]">';
                var rowField = '<tr>'+ '<td>'+ menuName+ '</td>' + '<td>' + btnDelete + menuHiddenId + '</td>' + '</tr>';
                $('#material-container').append(rowField);

            });


            $('#material-container').on('click', '.removeMenu', function() {
                $(this).parent().parent().remove();
            });


        });
    </script>
@endsection
@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/fonts/font-awesome/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/summernote/summernote.css') }}">
@endsection
@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-body">
					<h3>Setting SMS Template</h3>
					<div class="tab-content">
						<div class=" tab-pane animation-fade active" id="category-1" role="tabpanel">
							<div class="panel-group panel-group-simple panel-group-continuous" id="accordion2"
							 aria-multiselectable="true" role="tablist">
								<div class="panel">
									<div class="panel-heading" id="question-1" role="tab">
										<a class="panel-title" aria-controls="earningSms" aria-expanded="true" data-toggle="collapse"
										 href="#earningSms" data-parent="#accordion2">
											<h3>Earning Sms</h3>
										</a>
									</div>
									<div class="panel-collapse collapse in" id="earningSms" aria-labelledby="question-1"
									 role="tabpanel">
										<div class="row">
											<div class="panel">
												<div class="panel-body">
													<textarea class="form-control" id="earning-sms" rows="3" placeholder="Type here..."></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading" id="question-2" role="tab">
										<a class="panel-title" aria-controls="redeemSms" aria-expanded="false" data-toggle="collapse" href="#redeemSms" data-parent="#accordion2">
											<h3>Redeem Sms</h3>
										</a>
									</div>
									<div class="panel-collapse collapse" id="redeemSms" aria-labelledby="question-2"
									 role="tabpanel">
										<div class="row">
											<div class="panel">
												<div class="panel-body">
													<textarea class="form-control" id="redeem-sms" rows="3" placeholder="Type here..."></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading" id="question-2" role="tab">
										<a class="panel-title" aria-controls="new-cust-sms" aria-expanded="false" data-toggle="collapse" href="#new-cust-sms" data-parent="#accordion2">
											<h3>New Customer sms</h3>
										</a>
									</div>
									<div class="panel-collapse collapse" id="new-cust-sms" aria-labelledby="question-2"
									 role="tabpanel">
										<div class="row">
											<div class="panel">
												<div class="panel-body">
													<textarea class="form-control" id="New-customer-sms" rows="3" placeholder="Type here..."></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
		            <div class="form-group ">
		                <div class="col-sm-12">
		                	<div class="col-sm-3"><h3>SMS Active :</h3></div>
		                    <div class="col-sm-9">
		                        <input type="checkbox" name="status"  data-plugin="switchery" />
		                    </div>
		                </div>
		            </div>
					<div class="form-group pull-right">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary mart20">Submit</button>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('assets/global/vendor/animsition/animsition.js') }}"></script>
	<script src="{{ asset('assets/global/vendor/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('assets/global/js/components/summernote.js') }}"></script>
	<script src="{{ asset('assets/examples/js/forms/editor-summernote.js') }}"></script>
@endsection
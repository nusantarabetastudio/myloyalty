@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
@endsection

@section('content')
<form action="{{ $form_action }}" class="form-horizontal" method="post">
    {{ csrf_field() }}
    @if(isset($formulaRedeem))
        {{ method_field('PUT') }}
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4 padlr0">
                <div class="col-md-12">
                    <div class="panel panel-close">
                        <div class="panel-body">
                            <div class="form-group form-material-sm">
                                <div class="col-sm-12">
                                    <label class="control-label">From Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" class="form-control input-sm date" id="fromDate" name="fromDate" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_FRDATE->format('d/m/Y') : old('fromDate') }}" required>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="control-label">Time</label>
                                    <div class="input-group w100">
                                        <div class="col-sm-4 padl0">
                                            <input type="number" name="fromHour" class="form-control" min="0" max="24" value="{{ isset($formulaRedeem) ? substr($formulaRedeem->RDM_TOTIME, 0, 2) : old('toHour') }}" placeholder="hh">
                                        </div>
                                        <div class="col-sm-4 padlr5">
                                            <input type="number" name="fromMinute" class="form-control" min="0" max="59" value="{{ isset($formulaRedeem) ? substr($formulaRedeem->RDM_TOTIME, 3, 2) : old('toMinute') }}" placeholder="mm">
                                        </div>
                                        <div class="col-sm-4 padr0">
                                            <input type="number" name="fromSecond" class="form-control" min="0" max="59" value="{{ isset($formulaRedeem) ? substr($formulaRedeem->RDM_TOTIME, 6, 2) : old('toSecond') }}" placeholder="ss">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-close">
                        <div class="panel-body">
                            <div class="form-group form-material-sm">
                                <div class="col-sm-12">
                                    <label class="control-label">To Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                        <input type="text" class="form-control input-sm date" id="toDate" name="toDate" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_TODATE->format('d/m/Y') : old('toDate') }}" required>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="control-label">Time</label>
                                    <div class="input-group w100">
                                        <div class="col-sm-12 pad0">
                                            <div class="col-sm-4 padl0">
                                                <input type="number" name="toHour" class="form-control" min="0" max="24" value="{{ isset($formulaRedeem) ? substr($formulaRedeem->RDM_TOTIME, 0, 2) : old('toHour') }}" placeholder="hh">
                                            </div>
                                            <div class="col-sm-4 padlr5">
                                                <input type="number" name="toMinute" class="form-control" min="0" max="59" value="{{ isset($formulaRedeem) ? substr($formulaRedeem->RDM_TOTIME, 3, 2) : old('toMinute') }}" placeholder="mm">
                                            </div>
                                            <div class="col-sm-4 padr0">
                                                <input type="number" name="toSecond" class="form-control" min="0" max="59" value="{{ isset($formulaRedeem) ? substr($formulaRedeem->RDM_TOTIME, 6, 2) : old('toSecond') }}" placeholder="ss">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 padlr0">
                <div class="col-md-12">
                    <div class="panel panel-close">
                        <div class="panel-body">
                            <div class="form-group form-material-sm">
                                <div class="col-sm-12">
                                    <div class="form-group marb0">
                                        <div class="col-sm-10">
                                            <label class="control-label">Description</label>
                                            <input type="text" class="form-control input-sm" name="RDM_DESC" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_DESC : old('RDM_DESC') }}" required>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="control-label text-center">Active</label><br />
                                            <input type="checkbox" class="text-center" name="RDM_ACTIVE" {{ (isset($formulaRedeem) && $formulaRedeem->RDM_ACTIVE == 1) || !isset($formulaRedeem) ? 'checked'  : '' }} {{ isset($formulaRedeem) ? '' : 'disabled' }} data-plugin="switchery" data-size="small">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="panel panel-close">
                        <div class="panel-body">
                            <div class="form-group form-material-sm">
                                <div class="col-sm-12">
                                    <div class="form-group marb0">
                                        <div class="col-sm-4">
                                            <label class="control-label">Discount</label>
                                            <input type="number" name="RDM_AMOUNT" class="form-control" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_AMOUNT : old('RDM_AMOUNT') }}" id="discount">
                                        </div>
                                        @if($type == 'tebus-gratis')
                                            <div class="col-sm-4">
                                                <label class="control-label text-center">Value Point</label><br />
                                                <input type="number" name="RDM_VALUE" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_VALUE : old('RDM_VALUE') }}" class="form-control" id="valuePoint">
                                            </div>
                                        @endif
                                        <div class="col-sm-4">
                                            <label class="control-label text-center">Point</label><br />
                                            @if($type == 'tebus-gratis')
                                                <input type="hidden" id="point-after" name="RDM_POINT" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_POINT : old('RDM_POINT') }}">
                                                <input type="number" id="point" class="form-control" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_POINT : old('RDM_POINT') }}" min="0" disabled="disabled">
                                            @else
                                                <input type="number" name="RDM_POINT" id="point" class="form-control" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_POINT : old('RDM_POINT') }}" min="0">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="panel panel-close">
                        <div class="panel-body">
                            <div class="form-group form-material-sm">
                                <div class="col-sm-12">
                                    <div class="form-group marb0">
                                        <div class="col-sm-6">
                                            <label class="control-label text-center">Quantity</label><br />
                                            <input type="number" name="RDM_BY_QTY" class="form-control" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_BY_QTY : old('RDM_BY_QTY') }}" min="0">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label text-center">Transaction</label><br />
                                            <input type="number" name="RDM_BY_TRANS" class="form-control" value="{{ isset($formulaRedeem) ? $formulaRedeem->RDM_BY_TRANS : old('RDM_BY_TRANS') }}" min="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-close">
                        <div class="panel-body">
                            <div class="form-group form-material-sm">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label text-center">Week</label><br />
                                            <div class="checkbox-custom checkbox-primary">
                                                <input type="checkbox" name="week[0]" {{ isset($formulaRedeem) && $formulaRedeem->RDM_SUNDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                <label class="padr25">Sunday</label>
                                                <input type="checkbox" name="week[1]" {{ isset($formulaRedeem) && $formulaRedeem->RDM_MONDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                <label class="padr25">Monday</label>
                                                <input type="checkbox" name="week[2]" {{ isset($formulaRedeem) && $formulaRedeem->RDM_TUESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                <label class="padr25">Tuesday</label>
                                                <input type="checkbox" name="week[3]" {{ isset($formulaRedeem) && $formulaRedeem->RDM_WEDNESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                <label class="padr25">Wednesday</label>
                                                <br>
                                                <input type="checkbox" name="week[4]" {{ isset($formulaRedeem) && $formulaRedeem->RDM_THURSDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                <label class="padr25">Thursday</label>
                                                <input type="checkbox" name="week[5]" {{ isset($formulaRedeem) && $formulaRedeem->RDM_FRIDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                <label class="padr25">Friday</label>
                                                <input type="checkbox" name="week[6]" {{ isset($formulaRedeem) && $formulaRedeem->RDM_SATURDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                <label class="padr25">Saturday</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group marb0 pull-right">
                                        <div class="col-sm-12">
                                            <a class="btn btn-primary" id="week-select">Selected</a>
                                            <a class="btn btn-danger" id="week-unselect">Unselected</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li class="active" role="presentation"><a data-toggle="tab" href="#tabStore" aria-controls="payment" role="tab" aria-expanded="false">Store</a>
                            </li>
                            <li role="presentation" class=""><a data-toggle="tab" href="#product" aria-controls="product" role="tab" aria-expanded="false">Product</a>
                            </li>
                        </ul>
                        <div class="tab-content padding-top-20">
                            {{--<div class="tab-pane active" id="payment" role="tabpanel">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <a class="btn btn-primary" id="add-payment-btn">Add Payment Method</a>
                                    </div>
                                    <div class="col-sm-6 pull-right">
                                        <input type="text" name="payment" id="new-payment-name" class="form-control" placeholder="Add New Payment Here..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <table class="table table-hover dataTable table-striped width-full" id="payment-table-container">
                                            <thead>
                                                <tr>
                                                    <th>Payment</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="payment-container">
                                                <tr>
                                                    <th class="payment-name">Cash</th>
                                                    <th><a class="payment-delete">Delete</a></th>
                                                </tr>
                                                <tr>
                                                    <th class="payment-name">Debit Mandiri</th>
                                                    <th><a class="payment-delete">Delete</a></th>
                                                </tr>
                                                <tr id="payment-template" class="iTemplate">
                                                    <th class="payment-name"></th>
                                                    <th><a class="payment-delete">Delete</a></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>--}}
                            <div class="tab-pane active" id="tabStore" role="tabpanel">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="row mar0">
                                            <button type="button" class="btn btn-primary selected-store-btn pull-right marlr5">Select All</button>
                                            <button type="button" class="btn btn-danger unselected-store-btn pull-right marlr5">Unselect All</button>
                                        </div>
                                        <br />
                                        <table class="table table-hover dataTable table-striped width-full" id="store-table">
                                            <thead>
                                            <tr>
                                                <th>Store</th>
                                                <th>Active</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($store as $row)
                                                <tr>
                                                    <td>{{ $row->TNT_DESC }}</td>
                                                    <td>
                                                        <input type="checkbox"
                                                               @if(isset($formulaRedeem))
                                                                   @if($formulaRedeem->formulaTenants()->count() > 0)
                                                                        @foreach($formulaRedeem->formulaTenants as $tenant)
                                                                            @if($tenant->RDT_TENANT == $row->TNT_RECID)
                                                                                checked
                                                                            @endif
                                                                        @endforeach
                                                                   @endif
                                                               @endif
                                                               id="inputBasicOn" name="RDT_TENANT[]" value="{{ $row->TNT_RECID }}" data-plugin="switchery" class="store-switch">
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="product" role="tabpanel">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="row mar0">
                                            <button type="button" class="btn btn-primary selected-product-btn pull-right marlr5">Select All</button>
                                            <button type="button" class="btn btn-danger unselected-product-btn pull-right marlr5">Unselect All</button>
                                        </div>
                                        <br />
                                        <table class="table table-hover dataTable table-striped width-full" id="product-table">
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($products as $row)
                                                <tr>
                                                    <td>{{ $row->description }}</td>
                                                    <td>
                                                        <input type="checkbox"
                                                               @if(isset($formulaRedeem))
                                                                   @if($formulaRedeem->formulaProducts()->count() > 0)
                                                                       @foreach($formulaRedeem->formulaProducts as $product)
                                                                           @if($product->PRDR_PRODUCT == $row->id)
                                                                                checked
                                                                           @endif
                                                                       @endforeach
                                                                   @endif
                                                               @endif
                                                               id="inputBasicOn" name="PRDR_PRODUCT[]" value="{{ $row->id }}" data-plugin="switchery" class="product-switch"
                                                        >
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">Submit</button>
        </div>
    </div>
</form>
@endsection

@section('scripts')
    <!-- datatables -->
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script>
        $(function () {
            $('#fromDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });
            $('#toDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#discount, #valuePoint').change(function() {
                var discount = $('#discount').val();
                var valuePoint = $('#valuePoint').val();
                var point = discount / valuePoint;
                $('#point').val(point);
                $('#point-after').val(parseInt(point));
            });

            $('.selected-store-btn').click(function(e) {
                e.preventDefault();
                $('#tabStore .store-switch').prop('checked', false);
                $('#tabStore .store-switch').click();
            });

            $('.unselected-store-btn').click(function(e) {
                e.preventDefault();
                $('#tabStore .store-switch').prop('checked', true);
                $('#tabStore .store-switch').click();
            });

            $('.selected-product-btn').click(function(e) {
                e.preventDefault();
                $('#product .product-switch').prop('checked', false);
                $('#product .product-switch').click();
            });

            $('.unselected-product-btn').click(function(e) {
                e.preventDefault();
                $('#product .product-switch').prop('checked', true);
                $('#product .product-switch').click();
            });

            $('#week-select').click(function() {
                $('.week-checkbox').prop('checked', true);
            });

            $('#week-unselect').click(function() {
                $('.week-checkbox').prop('checked', false);
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#content_id').val($(event.relatedTarget).data('id'));
            });

            $('#payment-container').on('click', '.payment-delete', function() {
                $(this).parent().parent().remove();
            });

            $('#product-container').on('click', '.product-delete', function() {
                $(this).parent().parent().remove();
            });
        });
    </script>
@endsection
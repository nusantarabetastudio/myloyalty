@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-sm-2">
                    @if($type == 'tebusMurah')
                        <a href="{{ route('redeem-point-formula.tebus-murah.create') }}" class="btn btn-primary"><i class="icon md-plus" aria-hidden="true"></i> Create New Formula</a>
                    @else
                        <a href="{{ route('redeem-point-formula.tebus-gratis.create') }}" class="btn btn-primary"><i class="icon md-plus" aria-hidden="true"></i> Create New Formula</a>
                    @endif
                </div>
            </div>
            <br>
            <table class="table table-hover dataTable table-striped width-full" id="datatable-table">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

            <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <form action="{{ $type == 'tebusMurah' ? route('redeem-point-formula.tebus-murah.destroy') : route('redeem-point-formula.tebus-gratis.destroy') }}" method="post" role="form">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <input type="hidden" name="id" id="content_id">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Information !</h4>
                            </div>
                            <div class="modal-body">
                                Are you sure ?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn blue">Delete</button>
                            </div>
                        </form>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <meta name="_token" content="{!! csrf_token() !!}" />
        </div>
    </div>
@endsection

@section('scripts')
    <!-- datatables -->
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            var start;
            oTable = $('#datatable-table').DataTable({
                processing: true,
                serverSide: true,
                @if($type == 'tebusMurah')
                ajax: '{{ route('redeem-point-formula.tebus-murah.data') }}',
                @elseif($type == 'tebusGratis')
                ajax: '{{ route('redeem-point-formula.tebus-gratis.data') }}',
                @elseif($type == 'regularRedeem')
                ajax: '{{ route('redeem-point-formula.regular-redeem.data') }}',
                @endif
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'RDM_DESC', name: 'RDM_DESC'},
                    { data: 'type', name: 'type'},
                    { data: 'from', name: 'from'},
                    { data: 'to', name: 'to'},
                    { data: 'active', name: 'active'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#content_id').val($(event.relatedTarget).data('id'));
            });
        })

    </script>

@endsection
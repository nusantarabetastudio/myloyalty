@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.min.css') }}">
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <b>Whoops...</b>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ isset($formulaEarn) ? route('formula.bonus.update', $formulaEarn->ERN_RECID) : route('formula.bonus.store') }}" method="POST" class="form-horizontal" id="myForm" enctype="multipart/form-data">
        @if(isset($formulaEarn))
            {{ method_field('PUT') }}
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 padlr0">
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">From Date</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                            <input type="text" class="form-control input-sm date" id="fromDate" name="fromDate" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_FRDATE->format('d/m/Y') : old('fromDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="control-label">Time</label>
                                        <div class="input-group w100">
                                            <div class="col-sm-4 padl0">
                                                <input type="number" name="fromHour" class="form-control" min="0" max="24" placeholder="hh" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 0, 2) : old('fromHour') }}" >
                                            </div>
                                            <div class="col-sm-4 padlr5">
                                                <input type="number" name="fromMinute" class="form-control" min="0" max="59" placeholder="mm" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 3, 2) : old('fromMinute') }}">
                                            </div>
                                            <div class="col-sm-4 padr0">
                                                <input type="number" name="fromSecond" class="form-control" min="0" max="59" placeholder="ss" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 6, 2) : old('fromSecond') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">To Date</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                            <input type="text" class="form-control input-sm date" id="toDate" name="toDate" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_TODATE->format('d/m/Y') : old('toDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="control-label">Time</label>
                                        <div class="input-group w100">
                                            <div class="col-sm-12 pad0">
                                                <div class="col-sm-4 padl0">
                                                    <input type="number" name="toHour" class="form-control" min="0" max="24" placeholder="hh" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 0, 2) : old('toHour') }}">
                                                </div>
                                                <div class="col-sm-4 padlr5">
                                                    <input type="number" name="toMinute" class="form-control" min="0" max="59" placeholder="mm" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 3, 2) : old('toMinute') }}">
                                                </div>
                                                <div class="col-sm-4 padr0">
                                                    <input type="number" name="toSecond" class="form-control" min="0" max="59" placeholder="ss" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 6, 2) : old('toSecond') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 padlr0">
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="form-group marb0">
                                            <div class="col-sm-12">
                                                <br/>
                                            </div>
                                        </div> 
                                        <div class="form-group marb0">
                                            <div class="col-sm-12">
                                                <h2>Point Formula :   </h2>
                                                <label><input checked type="radio" name="ERN_FORMULA" value="1" required {{ (isset($formulaEarn) AND $formulaEarn->ERN_FORMULA==1 )  ? 'checked' : '' }} > Bonus Point</label> &nbsp;
                                                <label><input type="radio" name="ERN_FORMULA" value="3" required {{ (isset($formulaEarn) AND $formulaEarn->ERN_FORMULA==3 )  ? 'checked' : '' }} > Point Lucky Draw</label> &nbsp;
                                            </div>

                                        </div>
                                        <div class="form-group marb0">
                                            <div class="col-sm-10">
                                                <label class="control-label">Description</label>
                                                <input type="text" class="form-control input-sm" name="ERN_DESC" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_DESC : old('ERN_DESC') }}" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="control-label text-center">Active</label><br />
                                                    <input type="checkbox" id="inputBasicOn" name="ERN_ACTIVE" {{ (isset($formulaEarn) && $formulaEarn->ERN_ACTIVE == 1) || !isset($formulaEarn) ? 'checked'  : '' }} {{ isset($formulaEarn) ? '' : 'disabled' }}  data-plugin="switchery" data-size="small">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{--<div class="col-sm-6">
                                                <label for="cards" class="control-label">Cards</label>
                                                <select name="ERN_CARDS" id="cards" class="form-control input-sm">
                                                    <option value="">Choose One</option>
                                                    @foreach($cards as $row)
                                                        @if(isset($formulaEarn) && $formulaEarn->ERN_CARDS == $row->LOK_RECID)
                                                            <option value="{{ $row->LOK_RECID }}" selected>{{ $row->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $row->LOK_RECID }}">{{ $row->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>--}}
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5">
                                                <label class="control-label">Value Type:</label>
                                                <select name="ERN_VALUE" class="form-control input-sm select2" required id="value_type">
                                                    <option value="">Choose One</option>
                                                    <option value="0" {{isset($formulaEarn) && $formulaEarn->ERN_VALUE == 0 ? 'selected'  : '' }}>Amount</option>
                                                    <option value="1" {{isset($formulaEarn) && $formulaEarn->ERN_VALUE == 1 ? 'selected'  : '' }}>Quantity</option>
                                                </select>
                                            </div>

                                            <div id="amount">
                                                <div class="col-sm-4">
                                                    <label class="col-sm-6 control-label text-left">Amount :</label>
                                                    <input type="number" name="ERN_AMOUNT" class="form-control input-sm" min="0" value="{{ isset($formulaEarn)  ? $formulaEarn->ERN_AMOUNT : old('ERN_AMOUNT') }}">
                                                </div>
                                            </div>
                                            <div id="quantity">
                                                <div class="col-sm-4">
                                                    <label class="col-sm-6 control-label text-left">Quantity:</label>
                                                    <input type="number" name="ERN_QTY" class="form-control input-sm" min="0" value="{{ isset($formulaEarn)  ? $formulaEarn->ERN_QTY : old('ERN_QTY') }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label">Point:</label>
                                                <input type="number" name="ERN_POINT" class="form-control input-sm" min="0" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_POINT : old('ERN_POINT') }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Point Type:</label>
                                                <select name="ERN_TYPE" class="form-control input-sm" required>
                                                    <option value="">Choose One</option>
                                                    <option value="0" {{ isset($formulaEarn) && $formulaEarn->ERN_TYPE == 0 ? 'selected'  : '' }}>Earning Point</option>
                                                    <option value="1" {{ isset($formulaEarn) && $formulaEarn->ERN_TYPE == 1 ? 'selected'  : '' }}>Redeem Point</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="control-label text-center">Multiply</label><br />
                                                <input type="checkbox" id="multiply" name="ERN_MULTIPLE" {{ isset($formulaEarn) && $formulaEarn->ERN_MULTIPLE == 1 ? 'checked'  : '' }} data-plugin="switchery" data-size="small">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label for="">Formula Type</label>
                                                <select name="ERN_FORMULA_TYPE" id="formula-type" class="form-control input-sm">
                                                    <option value="">Choose One</option>
                                                    @foreach($formulaType as $key=>$row)
                                                        @if(isset($formulaEarn) && $key == $formulaEarn->ERN_FORMULA_TYPE )
                                                            <option value="{{ $key }}" selected>{{ $row }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $row }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                <label for="multiple_value">Multiple Value</label>
                                                <input type="number" class="form-control"  min="0" name="ERN_MULTIPLE_VALUE" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_MULTIPLE_VALUE : old('ERN_MULTIPLE_VALUE') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label text-center">Week</label><br />
                                                <div class="checkbox-custom checkbox-primary">
                                                    <input type="checkbox" name="week[0]" {{ isset($formulaEarn) && $formulaEarn->ERN_SUNDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Sunday</label>
                                                    <input type="checkbox" name="week[1]" {{ isset($formulaEarn) && $formulaEarn->ERN_MONDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Monday</label>
                                                    <input type="checkbox" name="week[2]" {{ isset($formulaEarn) && $formulaEarn->ERN_TUESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Tuesday</label>
                                                    <input type="checkbox" name="week[3]" {{ isset($formulaEarn) && $formulaEarn->ERN_WEDNESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Wednesday</label>
                                                    <br>
                                                    <input type="checkbox" name="week[4]" {{ isset($formulaEarn) && $formulaEarn->ERN_THURSDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Thursday</label>
                                                    <input type="checkbox" name="week[5]" {{ isset($formulaEarn) && $formulaEarn->ERN_FRIDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Friday</label>
                                                    <input type="checkbox" name="week[6]" {{ isset($formulaEarn) && $formulaEarn->ERN_SATURDAY == 1 ? 'checked' : ''}} class="week-checkbox">
                                                    <label class="padr25">Saturday</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group marb0 pull-right">
                                            <div class="col-sm-12">
                                                <a class="btn btn-primary" id="week-select">Selected</a>
                                                <a class="btn btn-danger" id="week-unselect">Unselected</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="nav-tabs-horizontal nav-tabs-inverse nav-tabs-animate">
                                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                                        <li class="active" role="presentation">
                                            <a data-toggle="tab" id="formulaType" href="#tabFormulaType" aria-controls="tabFormulaType"
                                               role="tab" aria-expanded="true">
                                                Formula Type
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a data-toggle="tab" href="#tabPayment" aria-controls="tabPayment"
                                               role="tab" aria-expanded="false">
                                                Payment
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a data-toggle="tab" href="#tabStore" aria-controls="tabStore"
                                               role="tab" aria-expanded="true">
                                                Store
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabGender" aria-controls="tabGender"
                                               role="tab" aria-expanded="true">
                                                Gender
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabBlood" aria-controls="tabBlood"
                                               role="tab" aria-expanded="true">
                                                Blood
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabReligion" aria-controls="tabReligion"
                                               role="tab" aria-expanded="true">
                                                Religion
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content padding-top-20">
                                        <div class="tab-pane active animation-fade" id="tabFormulaType" role="tabpanel">
                                            <div class="text-center">
                                                <h2>Select Formula Type</h2>
                                            </div>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabPayment" role="tabpanel">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="bold">Hanya berlaku pada formula type jenis payment.</p>
                                                </div>
                                            </div>
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-payment-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-payment-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full">
                                                <thead>
                                                <tr>
                                                    <th>Payment</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($payments as $row)
                                                    <tr>
                                                        <td>{{$row->name}}</td>
                                                        <td><input type="checkbox" class="payment-switch" name="PAY_LOK_RECID[]" value="{{$row->id}}" data-plugin="switchery"
                                                            @if(isset($formulaEarn))
                                                                @foreach($formulaEarn->payment as $payment)
                                                                    @if($payment->PAY_LOK_RECID == $row->id) checked @endif
                                                                @endforeach
                                                            @endif
                                                            ></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabStore" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-store-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-store-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full">
                                                <thead>
                                                <tr>
                                                    <th>Store</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($store as $row)
                                                    <tr>
                                                        <td>{{$row->TNT_DESC}}</td>
                                                        <td><input type="checkbox" class="store-switch" name="TENT_TNT_RECID[]" value="{{ $row->TNT_RECID }}" data-plugin="switchery"
                                                            @if(isset($formulaEarn))
                                                                @foreach($formulaEarn->tenant as $tenant)
                                                                    @if($tenant->TENT_TNT_RECID == $row->TNT_RECID) checked @endif
                                                                @endforeach
                                                            @else
                                                                checked
                                                            @endif
                                                            ></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabGender" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-gender-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-gender-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Gender</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($gender as $row)
                                                    <tr>
                                                        <td>{{$row->LOK_DESCRIPTION }}</td>
                                                        <td><input type="checkbox" class="gender-switch" name="GEN_LOK_RECID[]" value="{{ $row->LOK_RECID }}" data-plugin="switchery"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->gender as $gender)
                                                                   @if($gender->GEN_LOK_RECID == $row->LOK_RECID) checked @endif
                                                                   @endforeach
                                                                   @else
                                                                   checked
                                                                    @endif
                                                            ></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabBlood" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-blood-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-blood-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Blood</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($blood_type as $row)
                                                    <tr>
                                                        <td>{{$row->LOK_DESCRIPTION}}</td>
                                                        <td><input type="checkbox" class="blood-switch" name="BLOD_LOK_RECID[]" value="{{$row->LOK_RECID}}" data-plugin="switchery"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->blood as $bloodType)
                                                                   @if($bloodType->BLOD_LOK_RECID == $row->LOK_RECID) checked @endif
                                                                   @endforeach
                                                                   @else
                                                                   checked
                                                                    @endif
                                                            ></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabReligion" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-religion-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-religion-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Religion</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($religion as $row)
                                                    <tr>
                                                        <td>{{ $row->LOK_DESCRIPTION }}</td>
                                                        <td><input type="checkbox" class="religion-switch" name="RELI_LOK_RECID[]" value="{{$row->LOK_RECID}}" data-plugin="switchery"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->religion as $religion)
                                                                   @if($religion->RELI_LOK_RECID == $row->LOK_RECID) checked @endif
                                                                    @endforeach
                                                                    @else
                                                                        checked
                                                                    @endif
                                                            ></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success" id="btnSubmit">Submit</button>
            </div>
        </div>
    </form>

    <div class="modal fade modal-danger modal-3d-slit" id="modalWarning" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Warning!</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onClick="btnSubmit()">Submit</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var val = $('#formula-type').val();

            $('.select-all-religion-btn').click(function() {
                $('.religion-switch').prop('checked', false);
                $('.religion-switch').click();
            });

            $('.unselect-all-religion-btn').click(function() {
                $('.religion-switch').prop('checked', true);
                $('.religion-switch').click();
            });

            $('.select-all-payment-btn').click(function() {
                $('.payment-switch').prop('checked', false);
                $('.payment-switch').click();
            });

            $('.unselect-all-payment-btn').click(function() {
                $('.payment-switch').prop('checked', true);
                $('.payment-switch').click();
            });

            $('.select-all-store-btn').click(function() {
                $('.store-switch').prop('checked', false);
                $('.store-switch').click();
            });

            $('.unselect-all-store-btn').click(function() {
                $('.store-switch').prop('checked', true);
                $('.store-switch').click();
            });

            $('.select-all-gender-btn').click(function() {
                $('.gender-switch').prop('checked', false);
                $('.gender-switch').click();
            });

            $('.unselect-all-gender-btn').click(function() {
                $('.gender-switch').prop('checked', true);
                $('.gender-switch').click();
            });

            $('.select-all-blood-btn').click(function() {
                $('.blood-switch').prop('checked', false);
                $('.blood-switch').click();
            });

            $('.unselect-all-blood-btn').click(function() {
                $('.blood-switch').prop('checked', true);
                $('.blood-switch').click();
            });

            $('#amount').hide();
            $('#quantity').hide();

            var data, obj, objDepartment, objDivision, objCategory, objSubCategory, objSegment, objVendor;

            if(val == 1) { // By Product
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divProduct());

                $('.js-product').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.product.index') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.PRO_RECID, text: obj.PRO_DESCR + ' (' + obj.PRO_CODE + ')' , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 3
                });

                @if(isset($formulaEarn))
                    @if($formulaEarn->product->first())
                        @foreach ($formulaEarn->product as $product)
                            $('#product_id').append('<option selected value="{{ $product->ITM_PRODUCT }}">{{ $product->item->PRO_DESCR }} ({{ $product->item->PRO_CODE }})</option>');
                        @endforeach
                        $('.product_id').change();
                    @endif
                @endif

            }
            else if(val == 2){ // Brand
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divBrand());

                $('.js-brand').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.master.brand') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.id, text: obj.code + ' - ' + obj.name , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 3
                });

                @if(isset($formulaEarn))
                    @if($formulaEarn->brand()->count() > 0)

                        data = '{!! str_replace("'", '&#39', json_encode($formulaEarn->brand->toArray())) !!}';
                        obj = jQuery.parseJSON(data);
                        for(var i=0;i<obj.length;i++){

                            if(obj[i].brand){
                                $('.brand_id').append('<option selected value="'+obj[i].BRAND_BRAND_RECID+'">'+obj[i].brand.name+'</option>');
                            }
                        }
                    @endif

                    $('.brand_id').change();
                @endif

            }
            else if(val == 3) { // Department
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divDepartment());

                $('.js-department').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.master.department') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 2
                });

                @if(isset($formulaEarn))
                        @if($formulaEarn->department()->count() > 0)

                            objDepartment = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->department->toArray())) !!}');

                            for(var i=0;i<objDepartment.length;i++){
                                if(objDepartment[i].department){
                                    $('.department_id').append('<option selected value="'+objDepartment[i].DEPT_DEPARTMENT+'">'+objDepartment[i].department.LOK_DESCRIPTION+'</option>');
                                }
                            }
                        @endif
                $('.department_id').change();

                @endif

            }
            else if(val == 4) { // Division
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divDivision());

                $('.js-division').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.master.division') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 2
                });

                @if(isset($formulaEarn))
                    @if($formulaEarn->division()->count() > 0)

                        objDivision = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->division->toArray())) !!}');
                        for(var i=0;i<objDivision.length;i++){
                            if(objDivision[i].division){
                                $('.division_id').append('<option selected value="'+objDivision[i].DIV_DIVISION+'">'+objDivision[i].division.LOK_DESCRIPTION+'</option>');
                            }
                        }
                    @endif
                    $('.division_id').change();

                @endif
            }
            else if(val == 5) { // Category
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divCategory());

                $('.js-category').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.master.category') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 2
                });

                @if(isset($formulaEarn))
                    @if($formulaEarn->category()->count() > 0)

                    objCategory = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->category->toArray())) !!}');
                    for(var i=0;i<objCategory.length;i++){
                        if(objCategory[i].category){
                            $('.category_id').append('<option selected value="'+objCategory[i].CAT_CATEGORY+'">'+objCategory[i].category.LOK_DESCRIPTION+'</option>');
                        }
                    }
                    @endif
                    $('.category_id').change();
                @endif
            }
            else if(val == 6) { // Sub Category
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divSubCategory());

                $('.js-sub-category').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.master.subcategory') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 2
                });

                @if(isset($formulaEarn))
                    @if($formulaEarn->subcategory()->count() > 0)

                        objSubCategory = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->subcategory->toArray())) !!}');
                        for(var i=0;i<objSubCategory.length;i++){
                            if(objSubCategory[i].subcategory){
                                $('.subcategory_id').append('<option selected value="'+objSubCategory[i].SCAT_SUBCATEGORY+'">'+objSubCategory[i].subcategory.LOK_DESCRIPTION+'</option>');
                            }
                        }
                    @endif
                    $('.subcategory_id').change();
                @endif
            }
            else if(val == 7) { // Segment
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divSegment());

                $('.js-segment').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.master.segment') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 2
                });

                @if(isset($formulaEarn))
                    @if($formulaEarn->segment()->count() > 0)

                    objSegment = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->segment->toArray())) !!}');
                    for(var i=0;i<objSegment.length;i++){
                        if(objSegment[i].segment){
                            $('.segment_id').append('<option selected value="'+objSegment[i].SEG_SEGMENT+'">'+objSegment[i].segment.LOK_DESCRIPTION+'</option>');
                        }
                    }
                    @endif
                    $('.segment_id').change();
                @endif
            }
            else if(val == 8) { // Vendor
                $('#tabFormulaType').html('');
                $('#tabFormulaType').append(divVendor());

                $('.js-vendor').select2({
                    width: '100%',
                    ajax: {
                        url: '{{ route('api.master.vendor') }}',
                        dataType : 'json',
                        delay : 250,
                        data : function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(obj) {
                                    return { id: obj.VDR_RECID, text: obj.VDR_NAME , selected:true };
                                })
                            };
                        },
                        cache: true
                    },
                    beforeSend: function (xhr) {
                        var token = '{{ csrf_token() }}';

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    minimumInputLength: 2
                });

                @if(isset($formulaEarn))

                    @if($formulaEarn->vendor()->count() > 0)

                        objVendor = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->vendor->toArray())) !!}');

                        for(var i=0;i<objVendor.length;i++){

                            if(objVendor[i].vendor){
                                $('.vendor_id').append('<option selected value="'+objVendor[i].VND_VENDOR_RECID+'">'+objVendor[i].vendor.VDR_NAME+'</option>');
                            }
                        }
                    @endif
                $('.vendor_id').change();
                @endif
            }
            else {
                $('#tabFormulaType').html('');
            }

            function divBrand() {
                return '<div id="tabBrand"><br>\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6 margin-bottom-15">\
                <h3>Select Brand</h3>\
                <select class="form-control js-brand brand_id" id="brand_id" name="BRAND_BRAND_RECID[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                </div>'
            }

            function divProduct() {
                return '<div id="tabProduct"><br>\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6">\
                <h4>Select Product</h4>\
                <select class="form-control js-product product_id" id="product_id" name="ITM_PRODUCT[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                <div class="col-sd-6">\
                <h4>Product CSV File</h4>\
                <input type="file" name="csv_file" />\
                </div>\
                </div>'
            }

            function divDepartment() {
                return '<div id="tabDepartment"><br>\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6 margin-bottom-15">\
                <h3>Select Department</h3>\
                <select class="form-control js-department department_id" id="department_id" name="DEPT_DEPARTMENT[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                </div>'
            }

            function divDivision() {
                return '<div id="tabDivision">\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6 margin-bottom-15">\
                <h3>Select Division</h3>\
                <select class="form-control js-division division_id" id="division_id" name="DIV_DIVISION[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                </div>'
            }

            function divCategory() {
                return '<div id="tabCategory">\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6 margin-bottom-15">\
                <h3>Select Category</h3>\
                <select class="form-control js-category category_id" id="category_id" name="CAT_CATEGORY[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                </div>'
            }

            function divSubCategory() {
                return '<div id="tabSubCategory">\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6 margin-bottom-15">\
                <h3>Select SubCategory</h3>\
                <select class="form-control js-sub-category subcategory_id" id="subcategory_id" name="SCAT_SUBCATEGORY[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                </div>'
            }

            function divSegment() {
                return '<div id="tabSegment">\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6 margin-bottom-15">\
                <h3>Select Segment</h3>\
                <select class="form-control js-segment segment_id" id="segment_id" name="SEG_SEGMENT[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                </div>'
            }

            function divVendor() {
                return '<div id="tabVendor">\
                <div class="form-group marb0">\
                <div class="col-sm-12">\
                </div>\
                </div>\
                <br/>\
                <div class="col-md-6 margin-bottom-15">\
                <h3>Select Vendor</h3>\
                <select class="form-control js-vendor vendor_id" id="vendor_id" name="VND_VENDOR_RECID[]" multiple>\
                <option></option>\
                </select>\
                </div>\
                </div>'
            }

            $('#fromDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });
            $('#toDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });
            $('#week-select').click(function() {
                $('.week-checkbox').prop('checked', true);
            });
            $('#week-unselect').click(function() {
                $('.week-checkbox').prop('checked', false);
            });
            $('#delete-modal').on('show.bs.modal', function(event){
                $('#content_id').val($(event.relatedTarget).data('id'));
            });

            $('#formula-type').on('change', function() {
                val = $(this).val();

                if(val == 1) { // By Product
                    $('#tabFormulaType').html('');
                    $('#tabFormulaType').append(divProduct());

                    $('.js-product').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.product.index') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.PRO_RECID, text: obj.PRO_DESCR + ' (' + obj.PRO_CODE + ')', selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 3
                    });
                }
                else if(val == 2){ // brand.
                    $('#tabFormulaType').html('');

                    $('#tabFormulaType').append(divBrand());

                    $('.js-brand').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.master.brand') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.id, text: obj.code + ' - ' + obj.name, selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 3
                    });

                    @if(isset($formulaEarn))
                        @if($formulaEarn->brand()->count() > 0)
                            data = '{!! str_replace("'", '&#39', json_encode($formulaEarn->brand->toArray())) !!}';

                            obj = jQuery.parseJSON(data);

                            for(var i=0;i<obj.length;i++){
                                if(obj[i].brand){
                                    $('.brand_id').append('<option selected value="'+obj[i].BRAND_BRAND_RECID+'">'+obj[i].brand.name+'</option>');
                                }
                            }
                        @endif

                        $('.brand_id').change();
                    @endif
                }
                else if(val == 3) { // Department
                    $('#tabFormulaType').html('');
                    $('#tabFormulaType').append(divDepartment());

                    $('.js-department').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.master.department') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION, selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 2
                    });

                    @if(isset($formulaEarn))
                            @if($formulaEarn->department()->count() > 0)
                            objDepartment = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->department->toArray())) !!}');

                        for(var i=0; i<objDepartment.length; i++){
                            if(objDepartment[i].department){
                                $('.department_id').append('<option selected value="'+objDepartment[i].DEPT_DEPARTMENT+'">'+objDepartment[i].department.LOK_DESCRIPTION+'</option>');
                            }
                        }
                        @endif

                        $('.department_id').change();
                    @endif

                }
                else if(val == 4) { // Division
                    $('#tabFormulaType').html('');
                    $('#tabFormulaType').append(divDivision());

                    $('.js-division').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.master.division') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION, selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 2
                    });

                    @if(isset($formulaEarn))
                        @if($formulaEarn->division()->count() > 0)
                            objDivision = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->division->toArray())) !!}');

                            for(var i=0; i<objDivision.length; i++){
                                if(objDivision[i].division){
                                    $('.division_id').append('<option selected value="'+objDivision[i].DEPT_DEPARTMENT+'">'+objDivision[i].division.LOK_DESCRIPTION+'</option>');
                                }
                            }
                        @endif

                        $('.division_id').change();
                    @endif
                }
                else if(val == 5) { // Category
                    $('#tabFormulaType').html('');
                    $('#tabFormulaType').append(divCategory());

                    $('.js-category').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.master.category') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION, selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 2
                    });

                    @if(isset($formulaEarn))
                            @if($formulaEarn->category()->count() > 0)
                        objCategory = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->category->toArray())) !!}');

                    for(var i=0; i<objCategory.length; i++){
                        if(objCategory[i].category){
                            $('.category_id').append('<option selected value="'+objCategory[i].CAT_CATEGORY+'">'+objCategory[i].category.LOK_DESCRIPTION+'</option>');
                        }
                    }
                    @endif

                    $('.category_id').change();
                    @endif
                }
                else if(val == 6) { // Sub Category
                    $('#tabFormulaType').html('');
                    $('#tabFormulaType').append(divSubCategory());

                    $('.js-sub-category').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.master.subcategory') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION, selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 2
                    });

                    @if(isset($formulaEarn))
                            @if($formulaEarn->subcategory()->count() > 0)
                        objSubCategory = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->subcategory->toArray())) !!}');

                    for(var i=0; i<objSubCategory.length; i++){
                        if(objSubCategory[i].subcategory){
                            $('.subcategory_id').append('<option selected value="'+objSubCategory[i].SCAT_SUBCATEGORY+'">'+objSubCategory[i].subcategory.LOK_DESCRIPTION+'</option>');
                        }
                    }
                    @endif
                    $('.subcategory_id').change();
                    @endif
                }
                else if(val == 7) { // Segment
                    $('#tabFormulaType').html('');
                    $('#tabFormulaType').append(divSegment());
                    $('.js-segment').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.master.segment') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.LOK_RECID, text: obj.LOK_DESCRIPTION, selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 2
                    });

                    @if(isset($formulaEarn))
                            @if($formulaEarn->segment()->count() > 0)
                        objSegment = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->segment->toArray())) !!}');

                    for(var i=0; i<objSegment.length; i++){
                        if(objSegment[i].segment){
                            $('.segment_id').append('<option selected value="'+objSegment[i].SEG_SEGMENT+'">'+objSegment[i].segment.LOK_DESCRIPTION+'</option>');
                        }
                    }
                    @endif
                    $('.segment_id').change();
                    @endif
                }
                else if(val == 8) { // Vendor
                    $('#tabFormulaType').html('');
                    $('#tabFormulaType').append(divVendor());
                    $('.js-vendor').select2({
                        width: '100%',
                        ajax: {
                            url: '{{ route('api.master.vendor') }}',
                            dataType : 'json',
                            delay : 250,
                            data : function (params) {
                                return {
                                    q: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function(obj) {
                                        return { id: obj.VDR_RECID, text: obj.VDR_CODE + ' - ' + obj.VDR_NAME, selected:true};
                                    })
                                };
                            },
                            cache: true
                        },
                        beforeSend: function (xhr) {
                            var token = '{{ csrf_token() }}';

                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        },
                        minimumInputLength: 2
                    });

                    @if(isset($formulaEarn))
                        @if($formulaEarn->vendor()->count() > 0)
                            objVendor = jQuery.parseJSON('{!! str_replace("'", '&#39', json_encode($formulaEarn->vendor->toArray())) !!}');

                        for(var i=0; i<objVendor.length; i++){
                            if(objVendor[i].vendor){
                                $('.vendor_id').append('<option selected value="'+objVendor[i].VND_VENCOR_RECID+'">'+objVendor[i].vendor.VDR_NAME+'</option>');
                            }
                        }
                    @endif
                        $('.vendor_id').change();
                    @endif
                }
                else {
                    $('#tabFormulaType').html('');
                }
            });

            @if(isset($formulaEarn))
                var value_type = '{{ $formulaEarn->ERN_VALUE }}';
                if(value_type == 0 && value_type !== undefined) {
                    $('#quantity').hide().val(0);
                    $('#amount').show();
                }
                else if(value_type == 1){
                    $('#quantity').show();
                    $('#amount').hide();
                    $('#amount').val(parseInt(0));
                }
            @endif


            $('#value_type').on('change', function() {
                var value_type = $(this).val();
                if(value_type == 0 && value_type !== undefined) {
                    $('#quantity').hide().val(0);
                    $('#amount').show();
                }
                else if(value_type == 1){
                    $('#quantity').show();
                    $('#amount').hide().val(0);
                }
            });

            $('#btnSubmit').click(function(e) {
                e.preventDefault();
                {{-- #When Edit formula --}}
                @if(isset($formulaEarn))
                    if(val == 1){ // If Product
                        var selectedProduct = $('.product_id').val();
                        var productItems = jQuery.parseJSON(JSON.stringify({!! json_encode($formulaEarnProducts->toArray()) !!}));
                        var existingArrayProduct = [];

                            for(var product = 0; product < selectedProduct.length; product++) {
                                for(var item=0; item < productItems.length; item++) {
                                    if(selectedProduct[product] == productItems[item].ITM_RECID){
                                        var matchProduct = productItems[item];
                                        existingArrayProduct.push(matchProduct);
                                    }
                                }
                            }
                            if(existingArrayProduct.length > 0) {
                                $('#modalWarning').on('show.bs.modal', function() {
                                    var modal = $(this);

                                    modal.find('.modal-body').html('');
                                    modal.find('.modal-title').text('Warning');
                                    modal.find('.modal-body').append(
                                        '<div class="item-warning">'+
                                        existingArrayProduct.length + ' Product has been exists on another Point Formula <br>'+
                                        '</div>'
                                    );
                                    for(var text = 0; text < existingArrayProduct.length; text++ ) {
                                        modal.find('.modal-body .item-warning').append(existingArrayProduct[text].item.PRO_DESCR + '<br>')
                                    }
                                });
                                $('#modalWarning').on('hidden.bs.modal', function() {
                                    var modal = $(this);
                                    existingArrayProduct = [];
                                    modal.find('.modal-body .item-warning').html('');
                                });

                                $('#modalWarning').modal('toggle');
                            } else {
                                btnSubmit();
                            }
                        } else {
                        btnSubmit();
                    }

                @else
                {{-- #When create formula --}}
                    if(val == 1) {
                    var selectedProductOnCreate = $('.product_id').val();
                    var productItemsOnCreate = jQuery.parseJSON(JSON.stringify({!! $formulaEarnProducts->toJson() !!}));
                    var existingArrayProductOnCreate = [];

                    if(productItemsOnCreate !== null && selectedProductOnCreate !== null) {
                        for (var productOnCreate = 0; productOnCreate < selectedProductOnCreate.length; productOnCreate++) {
                            for (var itemOnCreate = 0; itemOnCreate < productItemsOnCreate.length; itemOnCreate++) {
                                if (productItemsOnCreate[itemOnCreate].item !== null) {
                                    if (selectedProductOnCreate[productOnCreate] == productItemsOnCreate[itemOnCreate].ITM_PRODUCT) {
                                        var matchProductOnCreate = productItemsOnCreate[itemOnCreate];
                                        existingArrayProductOnCreate.push(matchProductOnCreate);
                                    }
                                }
                            }
                        }
                    }
                    if(existingArrayProductOnCreate.length > 0) {
                        $('#modalWarning').on('show.bs.modal', function() {
                            var modal = $(this);
                            modal.find('.modal-body').html('');
                            modal.find('.modal-title').text('Warning')
                            modal.find('.modal-body').append(
                                '<div class="item-warning">'+
                                existingArrayProductOnCreate.length + ' Product has been exists on another Point Formula <br>'+
                                '</div>');
                            for(var textOnCreate = 0; textOnCreate < existingArrayProductOnCreate.length; textOnCreate++ ) {
                                modal.find('.modal-body .item-warning').append(existingArrayProductOnCreate[textOnCreate].item.PRO_DESCR + '<br>')
                            }
                        });
                        $('#modalWarning').on('hidden.bs.modal', function() {
                            var modal = $(this);
                            existingArrayProductOnCreate = [];
                            modal.find('.modal-body .item-warning').html('');
                        });
                        $('#modalWarning').modal('toggle')
                    } else {
                        btnSubmit();
                    }
                } else {
                        btnSubmit();
                }
                @endif
            });
        });
        function btnSubmit() {
            $('#myForm').submit();
        }
    </script>
@endsection

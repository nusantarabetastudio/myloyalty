@extends('layouts.loyalty')

@section('css')

@endsection

@section('content')

    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-error">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-sm-2">
                    <a href="{{ route('formula.bonus.create') }}" class="btn btn-primary"><i class="icon md-plus" aria-hidden="true"></i> Create Bonus Point Formula</a>
                </div>
            </div>
            <br>
            <table class="table table-hover dataTable table-striped width-full" id="datatable-table">
                <thead>
                <tr>
                    <th>Group</th>
                    <th>Description</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
            <div class="modal fade" id="active-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="{{ route('formula.bonus.updateStatus') }}" method="post" role="form">
                         {{ csrf_field() }}
                            <input type="hidden" name="id" id="active_id">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Information !</h4>
                            </div>
                            <div class="modal-body">
                                will Update : <strong id="active_name"></strong>
                                <br>
                                Are you sure ?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade bs-modal-sm modal-3d-slit" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <form action="{{ url('formula/bonus/destroy') }}" method="post" role="form">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <input type="hidden" name="id" id="content_id">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Information !</h4>
                            </div>
                            <div class="modal-body">
                                will deleting : <strong id="name"></strong>
                                <br>
                                Are you sure ?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Delete</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="modal fade" id="view-transaction-details" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#datatable-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url('formula/bonus/data') }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data: 'ERN_FORMULA_TYPE', name: 'ERN_FORMULA_TYPE'},
                    { data: 'ERN_DESC', name: 'ERN_DESC'},
                    { data: 'ERN_FRDATE', name: 'ERN_FRDATE'},
                    { data: 'ERN_TODATE', name: 'ERN_TODATE'},
                    { data: 'ERN_ACTIVE', name: 'ERN_ACTIVE'},
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#content_id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
            });
            $('#active-modal').on('show.bs.modal', function(event){
                $('#active_id').val($(event.relatedTarget).data('id'));
                $('#active_name').text($(event.relatedTarget).data('name'));
            });
        })

    </script>
@endsection


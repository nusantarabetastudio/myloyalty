@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/icheck/icheck.css') }}">
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <b>Whoops...</b>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ isset($formulaEarn) ? route('formula.reward.update', $formulaEarn->ERN_RECID) : route('formula.reward.store') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        @if(isset($formulaEarn))
            {{ method_field('PUT') }}
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 padlr0">
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="control-label">From Date</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                            <input type="text" class="form-control input-sm date" id="fromDate" name="fromDate" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_FRDATE->format('d/m/Y') : old('fromDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="control-label">Time</label>
                                        <div class="input-group w100">
                                            <div class="col-sm-4 padl0">
                                                <input type="number" name="fromHour" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 0, 2) : old('fromHour') }}" min="0" max="24" placeholder="hh">
                                            </div>
                                            <div class="col-sm-4 padlr5">
                                                <input type="number" name="fromMinute" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 3, 2) : old('fromMinute') }}" min="0" max="59" placeholder="mm">
                                            </div>
                                            <div class="col-sm-4 padr0">
                                                <input type="number" name="fromSecond" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 6, 2) : old('fromSecond') }}" min="0" max="59" placeholder="ss">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="control-label">To Date</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                            <input type="text" class="form-control input-sm date" id="toDate" name="toDate" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_TODATE->format('d/m/Y') : old('toDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="control-label">Time</label>
                                        <div class="input-group w100">
                                            <div class="col-sm-12 pad0">
                                                <div class="col-sm-4 padl0">
                                                    <input type="number" name="toHour" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 0, 2) : old('toHour') }}" min="0" max="24" placeholder="hh">
                                                </div>
                                                <div class="col-sm-4 padlr5">
                                                    <input type="number" name="toMinute" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 3, 2) : old('toMinute') }}" min="0" max="59" placeholder="mm">
                                                </div>
                                                <div class="col-sm-4 padr0">
                                                    <input type="number" name="toSecond" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 6, 2) : old('toSecond') }}" min="0" max="59" placeholder="ss">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 padlr0">
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <div class="form-group marb0">
                                            <div class="col-sm-10">
                                                <label class="control-label">Description</label>
                                                <input type="text" class="form-control input-sm" name="ERN_DESC" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_DESC : old('ERN_DESC') }}" required>
                                            </div>
                                            <div class="col-sm-2">
                                                <label class="control-label text-center">Active</label><br />
                                                <input type="checkbox" id="inputBasicOn" name="ERN_ACTIVE" {{ (isset($formulaEarn) && $formulaEarn->ERN_ACTIVE == 1) || !isset($formulaEarn) ? 'checked'  : '' }} {{ isset($formulaEarn) ? '' : 'disabled' }} data-plugin="switchery" data-size="small">
                                            </div>
                                        </div>
                                        <div class="form-group marb0 margin-top-10">
                                            <div class="col-sm-7">
                                                <label class="control-label">Point Type</label>
                                                <select name="ERN_TYPE" class="form-control input-sm" required>
                                                    <option value="">Choose One</option>
                                                    <option value="1" {{ isset($formulaEarn) && $formulaEarn->ERN_TYPE == 1 ? 'selected'  : '' }}>Redeem Point</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-6 col-md-12">
                                                <label class="control-label label-point">Multiple Point</label>
                                                <div class="input-group">
                                                    <input type="number" name="ERN_MULTIPLE_VALUE" id="ernMultiple" class="form-control" min="0" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_POINT : old('ERN_MULTIPLE_VALUE') }}">
                                                    <input type="number" name="ERN_POINT" id="ernPoint" class="form-control" min="0" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_POINT : old('ERN_POINT') }}">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" name="ERN_FIXED" class="form-control js-switch js-fixed-checkbox"  {{ isset($formulaEarn) && $formulaEarn->ERN_FIXED == 1 ? 'selected'  : '' }}>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-heading">
                                <h3 class="panel-title">Maximum Amount</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-lg-3 control-label text-left">Status :</label>
                                        <div class="col-lg-6">
                                            <input type="checkbox" class="js-ern-active-max" name="ERN_ACTIVE_MAX" {{ isset($formulaEarn) && $formulaEarn->ERN_ACTIVE_MAX == 1 ? 'checked'  : '' }}>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-lg-3 control-label text-left">Max. Amount :</label>
                                        <div class="col-lg-6">
                                            <input type="number" class="form-control input-sm ern-max-amount" name="ERN_MAX_AMOUNT" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_MAX_AMOUNT : old('ERN_MAX_AMOUNT') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-lg-3 control-label text-left">Min. Amount :</label>
                                        <div class="col-lg-6">
                                            <input type="number" class="form-control input-sm ern-min-amount" name="ERN_MIN_AMOUNT" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_MIN_AMOUNT : old('ERN_MIN_AMOUNT') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label text-center">Week</label><br />
                                                <div class="checkbox-custom checkbox-primary">
                                                    <input type="checkbox" name="week[0]" {{ isset($formulaEarn) && $formulaEarn->ERN_SUNDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Sunday</label>
                                                    <input type="checkbox" name="week[1]" {{ isset($formulaEarn) && $formulaEarn->ERN_MONDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Monday</label>
                                                    <input type="checkbox" name="week[2]" {{ isset($formulaEarn) && $formulaEarn->ERN_TUESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Tuesday</label>
                                                    <input type="checkbox" name="week[3]" {{ isset($formulaEarn) && $formulaEarn->ERN_WEDNESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Wednesday</label>
                                                    <br>
                                                    <input type="checkbox" name="week[4]" {{ isset($formulaEarn) && $formulaEarn->ERN_THURSDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Thursday</label>
                                                    <input type="checkbox" name="week[5]" {{ isset($formulaEarn) && $formulaEarn->ERN_FRIDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Friday</label>
                                                    <input type="checkbox" name="week[6]" {{ isset($formulaEarn) && $formulaEarn->ERN_SATURDAY == 1 ? 'checked' : ''}} class="week-checkbox">
                                                    <label class="padr25">Saturday</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group marb0 pull-right">
                                            <div class="col-sm-12">
                                                <a class="btn btn-primary" id="week-select">Selected</a>
                                                <a class="btn btn-danger" id="week-unselect">Unselected</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="nav-tabs-horizontal nav-tabs-inverse nav-tabs-animate">
                                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                                        <li class="active" role="presentation">
                                            <a data-toggle="tab" href="#tabRewards" aria-controls="tabRewards"
                                               role="tab" aria-expanded="true">
                                                Rewards
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="panel-group panel-group-continuous" id="" aria-multiselectable="true" role="tablist">
                                        <div class="panel">
                                            <div class="panel-heading" role="tab">
                                                <a class="panel-title" data-toggle="collapse" href="#birthdayReward" aria-expanded="false">
                                                    Birthday Reward
                                                </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="birthdayReward" aria-labelledby="" role="tabpanel">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input1" name="ERN_FORMULA_DTL" value="2"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 2 ? 'checked' : null }}
                                                            />
                                                            <label for="input1">Birthday Reward Monthly</label>
                                                        </li>
                                                    </ul>
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input2" name="ERN_FORMULA_DTL" value="5"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 5 ? 'checked' : null }}
                                                            />
                                                            <label for="input2">Birthday Reward Weekly</label>
                                                        </li>
                                                    </ul>
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input3" name="ERN_FORMULA_DTL" value="6"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 6 ? 'checked' : null }}
                                                            />
                                                            <label for="input3">Birthday Reward Daily</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="panel">
                                            <div class="panel-heading" role="tab">
                                                <a class="panel-title" data-toggle="collapse" href="#newRegistrationReward" aria-expanded="false">
                                                    New Registration Reward
                                                </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="newRegistrationReward" aria-labelledby="" role="tabpanel">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input4" name="ERN_FORMULA_DTL" value="4"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 4 ? 'checked' : null }}
                                                            />
                                                            <label for="input4">Reward New Registration</label>
                                                        </li>
                                                    </ul>
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input5" name="ERN_FORMULA_DTL" value="4"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 4 ? 'checked' : null }}
                                                            />
                                                            <label for="input5">Reward Update Profile on Web Site</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="panel">
                                            <div class="panel-heading" role="tab">
                                                <a class="panel-title" data-toggle="collapse" href="#anniversaryReward" aria-expanded="false">
                                                    Store Binding - Reward
                                                </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="anniversaryReward" aria-labelledby="" role="tabpanel">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input6" name="ERN_FORMULA_DTL" value="7"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 7 ? 'checked' : null }}
                                                            />
                                                            <label for="input6">Company</label>
                                                        </li>
                                                    </ul>
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input7" name="ERN_FORMULA_DTL" value="8"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 8 ? 'checked' : null }}
                                                            />
                                                            <label for="input7">Store</label>

                                                            <div class="">
                                                                <table class="table table-hover dataTable table-striped width-full margin-top-15" id="table-store">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Store</th>
                                                                        <th>Choose</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="">
                                                                        @foreach($tenantsArray as $row_tenant)
                                                                            @if(isset($formulaEarn))
                                                                                <tr>
                                                                                    <td>{{ $row_tenant->TNT_DESC }}</td>
                                                                                    <td><input type="checkbox" name="TENT_TNT_RECID[]" value="{{ $row_tenant->TNT_RECID }}"
                                                                                        @if($formulaEarn->tenant !== null)
                                                                                            @foreach($formulaEarn->tenant as $formulaTenant)
                                                                                                @if($formulaTenant->TENT_TNT_RECID == $row_tenant->TNT_RECID)
                                                                                                    {{  'checked' }}
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                @endif
                                                                                        ></td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{ $row_tenant->TNT_DESC }}</td>
                                                                                    <td><input type="checkbox" name="TENT_TNT_RECID[]" value="{{ $row_tenant->TNT_RECID }}" ></td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="panel">
                                            <div class="panel-heading" role="tab">
                                                <a class="panel-title" data-toggle="collapse" href="#otherReward" aria-expanded="false">
                                                    Other Reward
                                                </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="otherReward" aria-labelledby="" role="tabpanel">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input8" name="ERN_FORMULA_DTL" value="1"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 1 ? 'checked' : null }}
                                                            />
                                                            <label for="input8">Member Get Member Reward</label>
                                                        </li>
                                                    </ul>
                                                    <ul class="list-unstyled example">
                                                        <li class="margin-bottom-15">
                                                            <input type="radio" class="icheckbox-primary" id="input9" name="ERN_FORMULA_DTL" value="3"
                                                                    {{ isset($formulaEarn) && $formulaEarn->ERN_FORMULA_DTL == 3 ? 'checked' : null }}
                                                            />
                                                            <label for="input9">Buttler Service Reward</label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success">{{ isset($formulaEarn) ? 'Update' : 'Submit' }}</button>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/icheck/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/icheck.js') }}"></script>
    <script>
        $(document).ready(function () {
            var activeMax = document.querySelector('.js-ern-active-max');

            var sMaxAmount = new Switchery(activeMax, {
                size: 'small',
                color: '#3949ab'
            });


            $('#fromDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#toDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#week-select').click(function() {
                $('.week-checkbox').prop('checked', true);
            });

            $('#week-unselect').click(function() {
                $('.week-checkbox').prop('checked', false);
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#content_id').val($(event.relatedTarget).data('id'));
            });

            if (Array.prototype.forEach) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

                elems.forEach(function(html) {
                    var switchery = new Switchery(html, {
                        size: 'small'
                    });
                });
            } else {
                var elems = document.querySelectorAll('.js-switch');

                for (var i = 0; i < elems.length; i++) {
                    var switchery = new Switchery(elems[i], {
                        size: 'small'
                    });
                }
            }

            var fixedCheckbox = document.querySelector('.js-fixed-checkbox');

            @if(isset($formulaEarn))
                {{-- If Fixed Point is True --}}
                @if($formulaEarn->ERN_FIXED == 1)

                    $('.label-point').text('Fixed Point');
                    $('#ernPoint').show();
                    $('#ernPoint').show({{ $formulaEarn->ERN_POINT }});
                    $('#ernMultiple').hide();
                    $('#ernMultiple').val(0);
                @else
                 {{-- If Fixed Point is False --}}
                    $('.label-point').text('Multiple Point');
                    $('#ernMultiple').show();
                    $('#ernMultiple').val({{ $formulaEarn->ERN_MULTIPLE_VALUE }});
                    $('#ernPoint').hide();
                    $('#ernPoint').val(0);
                @endif

            @else

            if(fixedCheckbox.checked) {
                $('.label-point').text('Fixed Point');
                $('#ernPoint').show();
                $('#ernMultiple').hide();
                $('#ernMultiple').val(0);
            } else {
                $('.label-point').text('Multiple Point');
                $('#ernMultiple').show();
                $('#ernPoint').hide();
                $('#ernPoint').val(0);
            }
            @endif

            fixedCheckbox.onchange = function() {
                var checkboxChecked = this.checked;

                if(checkboxChecked) {
                    $('.label-point').text('Fixed Point');
                    $('#ernPoint').show();
                    $('#ernMultiple').hide();
                    $('#ernMultiple').val(0);
                } else {
                    $('.label-point').text('Multiple Point');
                    $('#ernMultiple').show();
                    $('#ernPoint').hide();
                    $('#ernPoint').val(0);
                }
            };
        });
    </script>
@endsection
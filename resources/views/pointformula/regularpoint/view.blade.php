<div class="modal-header text-danger">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">View Tenant</h4>
</div>
<div class="modal-body">
    <table class="table table-hover dataTable table-striped width-full" id="payment-table-container">
        <thead>
            <tr>
                <th>No</th>
                <th>Code</th>
                <th>Tenant</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
        <?php $no=1; ?>
        @foreach($tenant as $data)
            <tr>
                <td>{{ $no }}</td>
                <td>{{$data->TNT_CODE}}</td>
                <td>{{ $data->TNT_DESC }}</td>
                <td>{{ $data->ERN_DESC }}</td>
            </tr>
        <?php $no++ ?>
        @endforeach
        </tbody>
    </table>
</div>
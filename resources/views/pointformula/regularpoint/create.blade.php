@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <b>Whoops...</b>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ isset($formulaEarn) ? route('formula.regular.update', $formulaEarn->ERN_RECID) : route('formula.regular.store') }}" id="myForm" method="POST" class="form-horizontal">
        @if(isset($formulaEarn))
            {{ method_field('PUT') }}
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 padlr0">
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="control-label">From Date</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                            <input type="text" class="form-control input-sm date" id="fromDate" name="fromDate" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_FRDATE->format('d/m/Y') : old('fromDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="control-label">Time</label>
                                        <div class="input-group w100">
                                            <div class="col-sm-4 padl0">
                                                <input type="number" name="fromHour" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 0, 2) : old('fromHour') }}" min="0" max="24" placeholder="hh">
                                            </div>
                                            <div class="col-sm-4 padlr5">
                                                <input type="number" name="fromMinute" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 3, 2) : old('fromMinute') }}" min="0" max="59" placeholder="mm">
                                            </div>
                                            <div class="col-sm-4 padr0">
                                                <input type="number" name="fromSecond" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_FRTIME, 6, 2) : old('fromSecond') }}" min="0" max="59" placeholder="ss">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="control-label">To Date</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                            <input type="text" class="form-control input-sm date" id="toDate" name="toDate" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_TODATE->format('d/m/Y') : old('toDate') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="control-label">Time</label>
                                        <div class="input-group w100">
                                            <div class="col-sm-12 pad0">
                                                <div class="col-sm-4 padl0">
                                                    <input type="number" name="toHour" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 0, 2) : old('toHour') }}" min="0" max="24" placeholder="hh">
                                                </div>
                                                <div class="col-sm-4 padlr5">
                                                    <input type="number" name="toMinute" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 3, 2) : old('toMinute') }}" min="0" max="59" placeholder="mm">
                                                </div>
                                                <div class="col-sm-4 padr0">
                                                    <input type="number" name="toSecond" class="form-control" value="{{ isset($formulaEarn) ? substr($formulaEarn->ERN_TOTIME, 6, 2) : old('toSecond') }}" min="0" max="59" placeholder="ss">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-heading">
                                <h3 class="panel-title">Groups</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-4">
                                        <label class="control-label text-center">Referer</label><br />
                                        <input type="checkbox" id="inputBasicOn" name="ERN_REFERER" {{ isset($formulaEarn) && $formulaEarn->ERN_REFERER == 1 ? 'checked' : old('ERN_REFERER') }} data-plugin="switchery" data-size="small">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label text-center">Parent Family</label><br />
                                        <input type="checkbox" id="inputBasicOn" name="ERN_PARENT" {{ isset($formulaEarn) && $formulaEarn->ERN_PARENT == 1 ? 'checked' : old('ERN_PARENT') }} data-plugin="switchery" data-size="small">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    --}}
                </div>
                <div class="col-md-8 padlr0">
                    <div class="col-md-12">
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="col-md-12 ">
                                    <div class="form-group marb0">
                                        <div class="col-sm-12">
                                            <label class="control-label">Description</label>
                                            <input type="text" class="form-control input-sm" name="ERN_DESC" id="description" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_DESC : old('ERN_DESC') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group marb0 margin-top-10">
                                        <div class="col-sm-12">
                                            <label class="control-label">Point Type:</label>
                                            <select name="ERN_TYPE" class="form-control input-sm" required>
                                                <option>Choose One</option>
                                                <option value="0" {{ isset($formulaEarn) && $formulaEarn->ERN_TYPE == 0 ? 'selected' : '' }}>Earn Point</option>
                                                <option value="1" {{ isset($formulaEarn) && $formulaEarn->ERN_TYPE == 1 ? 'selected' : '' }}>Redeem Point</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-6">
                                            <label class="control-label">Amount :</label>
                                                <input type="number" class="form-control input-sm" name="ERN_AMOUNT" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_AMOUNT : old('ERN_AMOUNT') }}" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Point:</label>
                                            <input type="number" class="form-control input-sm" name="ERN_POINT" min="0" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_POINT : old('ERN_POINT') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label class="control-label text-center">Active</label><br />
                                            @if(isset($formulaEarn))
                                                <input type="checkbox" id="inputBasicOn" name="ERN_ACTIVE" {{ $formulaEarn->ERN_ACTIVE == 1 ? 'checked'  : '' }} data-plugin="switchery" data-size="small">
                                            @else
                                                <input type="checkbox" id="inputBasicOn" name="ERN_ACTIVE" checked disabled data-plugin="switchery" data-size="small">
                                            @endif
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label text-center">Multiply</label><br />
                                                <input type="checkbox" name="ERN_MULTIPLE_BASIC" {{ isset($formulaEarn) && $formulaEarn->ERN_MULTIPLE_BASIC == 1 ? 'checked'  : '' }} {{ isset($formulaEarn) ? '' : 'checked' }} data-plugin="switchery" data-size="small">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6" style="display:none;">
                                    <div class="form-group">
                                        <div class="col-lg-12 margin-bottom-15 margin-top-15">
                                            <label class="control-label text-left col-lg-4">Max Amount</label>
                                            <div class="col-lg-8">
                                                <input type="checkbox" class="form-control js-switch js-max-amount" name="ERN_ACTIVE_MAXAMOUNT" {{ isset($formulaEarn) && $formulaEarn->ERN_ACTIVE_MAXAMOUNT == 1 ? 'checked'  : '' }}>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 margin-bottom-15">
                                            <label for="" class="control-label text-left col-lg-4">Amount</label>
                                            <div class="col-lg-8">
                                                <input type="number" name="ERN_MAXAMOUNT_VALUE" id="max-amount-value" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_MAXAMOUNT_VALUE : old('ERN_MAXAMOUNT_VALUE') }}" min="0" class="form-control">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-lg-12 margin-bottom-15">
                                            <label class="control-label text-left col-lg-4">Rest Amount</label>
                                            <div class="col-lg-8">
                                                <input type="checkbox" class="form-control js-switch js-rest-amount" name="ERN_ACTIVE_REST" {{ isset($formulaEarn) && $formulaEarn->ERN_ACTIVE_REST == 1 ? 'checked'  : '' }}>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 margin-bottom-15">
                                            <label for="" class="control-label text-left col-lg-4">Amount</label>
                                            <div class="col-lg-8">
                                                <input type="number" name="ERN_AMOUNT_REST" id="rest-amount" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_AMOUNT_REST : old('ERN_AMOUNT_REST') }}" min="0" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 margin-bottom-15">
                                            <label for="" class="control-label text-left col-lg-4">Rest Point</label>
                                            <div class="col-lg-8">
                                                <input type="number" name="ERN_POINT_REST" id="rest-point" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_POINT_REST : old('ERN_POINT_REST') }}" min="0" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 margin-bottom-15">
                                            <label for="" class="col-sm-4 control-label text-left">Multiply</label>
                                            <div class="col-lg-1">
                                                <input type="checkbox" id="rest-multiple" class="js-switch js-rest-multiple" name="ERN_MULTIPLE_REST" {{ isset($formulaEarn) && $formulaEarn->ERN_MULTIPLE_REST == 1 ? 'checked'  : '' }}>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding-0">
                        <!-- Panel  -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Maximum Amount</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-lg-3 control-label text-left">Status :</label>
                                        <div class="col-lg-6">
                                            <input type="checkbox" class="js-ern-active-max" name="ERN_ACTIVE_MAX" {{ isset($formulaEarn) && $formulaEarn->ERN_ACTIVE_MAX == 1 ? 'checked'  : '' }}>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-lg-3 control-label text-left">Min. Amount :</label>
                                        <div class="col-lg-6">
                                            <input type="number" class="form-control input-sm ern-min-amount" name="ERN_MIN_AMOUNT" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_MIN_AMOUNT : old('ERN_MIN_AMOUNT') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <label class="col-lg-3 control-label text-left">Max. Amount :</label>
                                        <div class="col-lg-6">
                                            <input type="number" class="form-control input-sm ern-max-amount" name="ERN_MAX_AMOUNT" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_MAX_AMOUNT : old('ERN_MAX_AMOUNT') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="" class="col-lg-3 control-label text-left">Multiplier</label>
                                        <div class="col-lg-1">
                                            <input type="checkbox" id="inputBasicOn" name="ERN_MULTIPLE" {{ isset($formulaEarn) && $formulaEarn->ERN_MULTIPLE == 1 ? 'checked' : '' }} data-plugin="switchery" data-size="small">
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="col-lg-2 margin-left-0">
                                                <strong>X</strong>
                                            </div>
                                            <div class="col-sm-10 margin-left-0">
                                                <input type="number" name="ERN_MULTIPLE_VALUE" class="form-control input-sm" value="{{ isset($formulaEarn) ? $formulaEarn->ERN_MULTIPLE_VALUE : old('ERN_MULTIPLE_VALUE') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding-0">
                        <!-- Panel week -->
                        <div class="panel panel-close">
                            <div class="panel-body">
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label text-center">Week</label><br />
                                                <div class="checkbox-custom checkbox-primary">
                                                    <input type="checkbox" name="week[0]" {{ isset($formulaEarn) && $formulaEarn->ERN_SUNDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Sunday</label>
                                                    <input type="checkbox" name="week[1]" {{ isset($formulaEarn) && $formulaEarn->ERN_MONDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Monday</label>
                                                    <input type="checkbox" name="week[2]" {{ isset($formulaEarn) && $formulaEarn->ERN_TUESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Tuesday</label>
                                                    <input type="checkbox" name="week[3]" {{ isset($formulaEarn) && $formulaEarn->ERN_WEDNESDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Wednesday</label>
                                                    <br>
                                                    <input type="checkbox" name="week[4]" {{ isset($formulaEarn) && $formulaEarn->ERN_THURSDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Thursday</label>
                                                    <input type="checkbox" name="week[5]" {{ isset($formulaEarn) && $formulaEarn->ERN_FRIDAY == 1 ? 'checked' : '' }} class="week-checkbox">
                                                    <label class="padr25">Friday</label>
                                                    <input type="checkbox" name="week[6]" {{ isset($formulaEarn) && $formulaEarn->ERN_SATURDAY == 1 ? 'checked' : ''}} class="week-checkbox">
                                                    <label class="padr25">Saturday</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group marb0 pull-right">
                                            <div class="col-sm-12">
                                                <a class="btn btn-primary" id="week-select">Selected</a>
                                                <a class="btn btn-danger" id="week-unselect">Unselected</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="nav-tabs-horizontal nav-tabs-inverse nav-tabs-animate">
                                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a data-toggle="tab" href="#tabPayment" aria-controls="tabPayment"
                                               role="tab" aria-expanded="false">
                                                Payment
                                            </a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a data-toggle="tab" href="#tabMemberType" aria-controls="tabMemberType"
                                               role="tab" aria-expanded="false">
                                                Member Type
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabGender" aria-controls="tabGender"
                                               role="tab" aria-expanded="true">
                                                Gender
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabReligion" aria-controls="tabReligion"
                                               role="tab" aria-expanded="true">
                                                Religion
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabCardType" aria-controls="tabCardType"
                                               role="tab" aria-expanded="true">
                                                Card Type
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabStore" aria-controls="tabStore"
                                               role="tab" aria-expanded="true">
                                                Store
                                            </a>
                                        </li>
                                        <li class="" role="presentation">
                                            <a data-toggle="tab" href="#tabBlood" aria-controls="tabBlood"
                                               role="tab" aria-expanded="true">
                                                Blood Type
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content padding-top-20">
                                        <div class="tab-pane active animation-fade" id="tabPayment"
                                             role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-payment-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-payment-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full">
                                                <thead>
                                                <tr>
                                                    <th>Payment</th>
                                                    <th>Actived</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($payments as $payment)
                                                    <tr>
                                                        <td>{{ $payment->name }}</td>
                                                        <td>
                                                            <input type="checkbox"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->payment as $formulaPayment)
                                                                   @if($formulaPayment->PAY_LOK_RECID == $payment->id) checked @endif
                                                                   @endforeach
                                                                   @endif
                                                           id="inputBasicOn" name="PAY_LOK_RECID[]" class="payment-switch" value="{{ $payment->id }}" data-plugin="switchery">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabMemberType" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-member-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-member-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Member Type</th>
                                                    <th>Actived</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($member_type as $memberType)
                                                    <tr>
                                                        <td>{{ $memberType->LOK_DESCRIPTION }}</td>
                                                        <td>
                                                            <input type="checkbox"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->member as $formulaMemberType)
                                                                   @if($formulaMemberType->MEM_LOK_RECID == $memberType->LOK_RECID) checked @endif @endforeach
                                                                   @endif
                                                                   id="inputBasicOn" name="MEM_LOK_RECID[]" class="member-switch" value="{{ $memberType->LOK_RECID }}" data-plugin="switchery">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabGender" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-gender-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-gender-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Gender</th>
                                                    <th>Actived</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($gender as $row)
                                                    <tr>
                                                        <td>{{ $row->LOK_DESCRIPTION }}</td>
                                                        <td>
                                                            <input type="checkbox"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->gender as $formulaGender)
                                                                   @if($formulaGender->GEN_LOK_RECID == $row->LOK_RECID) checked @endif @endforeach
                                                                   @endif
                                                                   id="inputBasicOn" name="GEN_LOK_RECID[]" class="gender-switch" value="{{ $row->LOK_RECID }}" data-plugin="switchery">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabReligion" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-reli-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-reli-btn pull-right marlr5">Unselect All</button>
                                            </div>

                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Religion</th>
                                                    <th>Actived</th>
                                                </tr>
                                                </thead>
                                                 <tbody>
                                                 @foreach($religion as $row)
                                                     <tr>
                                                         <td>{{ $row->LOK_DESCRIPTION }}</td>
                                                         <td>
                                                             <input type="checkbox"
                                                                    @if(isset($formulaEarn))
                                                                    @foreach($formulaEarn->religion as $formulaReligion)
                                                                    @if($formulaReligion->RELI_LOK_RECID == $row->LOK_RECID) checked @endif @endforeach
                                                                    @endif
                                                                    id="inputBasicOn" name="RELI_LOK_RECID[]" class="reli-switch" value="{{ $row->LOK_RECID }}" data-plugin="switchery"
                                                             >
                                                         </td>
                                                     </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabCardType" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-card-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-card-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Card Type</th>
                                                    <th>Actived</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($card_type as $row)
                                                    <tr>
                                                        <td>{{ $row->CARD_DESC }}</td>
                                                        <td>
                                                            <input type="checkbox"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->cardType as $formulaCardType)
                                                                   @if($formulaCardType->CDTY_LOK_RECID == $row->CARD_RECID) checked @endif @endforeach
                                                                   @endif
                                                                   id="inputBasicOn" name="CDTY_LOK_RECID[]" class="card-switch" value="{{ $row->CARD_RECID }}" data-plugin="switchery"
                                                            >
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabStore" role="tabpanel">
                                            {{--<button type="button" class="btn btn-primary waves-effect waves-light"><i class="icon md-plus" aria-hidden="true"></i> Add Store</button>--}}
                                            <div class="row mar0">
                                                <button class="btn btn-primary selected-store-btn pull-right marlr5">Select All</button>
                                                <button class="btn btn-danger unselected-store-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full">
                                                <thead>
                                                <tr>
                                                    <th>Store</th>
                                                    <th>Actived</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($store as $row)
                                                    <tr>
                                                        <td>{{ $row->TNT_DESC }}</td>
                                                        <td>
                                                            <input type="checkbox"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->tenant as $tenant)
                                                                   @if($tenant->TENT_TNT_RECID == $row->TNT_RECID) checked @endif
                                                                   @endforeach
                                                                   @else
                                                                    checked
                                                                   @endif
                                                                   id="inputBasicOn" name="TENT_TNT_RECID[]" value="{{ $row->TNT_RECID }}" data-plugin="switchery" class="store-switch"
                                                            >
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane animation-fade" id="tabBlood" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-blood-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-blood-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br />
                                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                                                <thead>
                                                <tr>
                                                    <th>Blood</th>
                                                    <th>Actived</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($blood_type as $row)
                                                    <tr>
                                                        <td>{{ $row->LOK_DESCRIPTION }}</td>
                                                        <td>
                                                            <input type="checkbox"
                                                                   @if(isset($formulaEarn))
                                                                   @foreach($formulaEarn->blood as $formulaBlood)
                                                                   @if($formulaBlood->BLOD_LOK_RECID == $row->LOK_RECID) checked @endif
                                                                   @endforeach
                                                                    @else
                                                                    checked
                                                                   @endif
                                                                   id="inputBasicOn" name="BLOD_LOK_RECID[]" value="{{ $row->LOK_RECID }}" class="blood-switch" data-plugin="switchery"
                                                            >
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button name="submit" type="submit" class="btn btn-success">{{ isset($formulaEarn) ? 'Update' : 'Submit' }}</button>
            </div>
    </form>

    <div class="modal modal-warning fade bs-modal-sm modal-3d-slit" id="modalWarning" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Information !</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <b>Sorry...</b>
                    </p>
                    <p>
                        Max. Amount value should not be less than Min. Amount
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script>
        $(function () {
            var maxAmount = document.querySelector('.js-max-amount'),
                restAmount = document.querySelector('.js-rest-amount'),
                restMultiple = document.querySelector('.js-rest-multiple'),
                activeMax = document.querySelector('.js-ern-active-max');

            var sMaxAmount = new Switchery(maxAmount, {
                size: 'small',
                color: '#3949ab'
            });

            var sRestAmount = new Switchery(restAmount, {
                size: 'small',
                color: '#3949ab'
            });

            var sRestMultiple = new Switchery(restMultiple, {
                size: 'small',
                color: '#3949ab'
            });

            var ernActiveMax = new Switchery(activeMax, {
                size: 'small',
                color: '#3949ab'
            });

            if(sMaxAmount.isChecked()) {
                $('#max-amount-value').attr('disabled', false);
                sRestAmount.enable();
            } else {
                $('#max-amount-value').attr('disabled', true).val(0);
                sRestAmount.disable();
            }

            if(sRestAmount.isChecked()) {
                $('#rest-amount').attr('disabled', false);
                $('#rest-point').attr('disabled', false);
                sRestMultiple.enable();
            } else {
                $('#rest-amount').attr('disabled', true).val(0);
                $('#rest-point').attr('disabled', true).val(0);
                sRestMultiple.disable();
            }

            if(sMaxAmount.isChecked()) {
                sRestAmount.enable();
            } else {
                sRestAmount.disable();
            }

            if(sRestAmount.isChecked()) {
                sRestMultiple.enable();
            } else {
                sRestMultiple.disable();
            }

            maxAmount.onchange = function() {
                var maxAmountChecked = this.checked;
                var restAmountChecked =  sRestAmount.isChecked();
                if(maxAmountChecked) {
                    $('#max-amount-value').attr('disabled', false);
                    sRestAmount.enable();
                } else {
                    $('#max-amount-value').attr('disabled', true).val(0);
                    if(restAmountChecked) {
                        restAmount.click();
                    }
                    sRestAmount.disable();

                }
            };

            restAmount.onchange = function() {
                var restAmountChecked = this.checked;
                var restMultipleChecked = sRestMultiple.isChecked();
                if(restAmountChecked) {
                    $('#rest-amount').attr('disabled', false);
                    $('#rest-point').attr('disabled', false);
                    sRestMultiple.enable();
                } else {
                    $('#rest-amount').attr('disabled', true).val(0);
                    $('#rest-point').attr('disabled', true).val(0);
                    if(restMultipleChecked) {
                        restMultiple.click();
                    }
                    sRestMultiple.disable();
                }
            };

            $('.selected-store-btn').click(function(e) {
                e.preventDefault();
                $('#tabStore .store-switch').prop('checked', false);
                $('#tabStore .store-switch').click();
            });

            $('.unselected-store-btn').click(function(e) {
                e.preventDefault();
                $('#tabStore .store-switch').prop('checked', true);
                $('#tabStore .store-switch').click();
            });

            $('.select-all-blood-btn').click(function() {
                $('.blood-switch').prop('checked', false);
                $('.blood-switch').click();
            });

            $('.unselect-all-blood-btn').click(function() {
                $('.blood-switch').prop('checked', true);
                $('.blood-switch').click();
            });

            $('.select-all-card-btn').click(function() {
                $('.card-switch').prop('checked', false);
                $('.card-switch').click();
            });

            $('.unselect-all-card-btn').click(function() {
                $('.card-switch').prop('checked', true);
                $('.card-switch').click();
            });

            $('.select-all-reli-btn').click(function() {
                $('.reli-switch').prop('checked', false);
                $('.reli-switch').click();
            });

            $('.unselect-all-reli-btn').click(function() {
                $('.reli-switch').prop('checked', true);
                $('.reli-switch').click();
            });

            $('.select-all-payment-btn').click(function() {
                $('.payment-switch').prop('checked', false);
                $('.payment-switch').click();
            });

            $('.unselect-all-payment-btn').click(function() {
                $('.payment-switch').prop('checked', true);
                $('.payment-switch').click();
            });

            $('.select-all-gender-btn').click(function() {
                $('.gender-switch').prop('checked', false);
                $('.gender-switch').click();
            });

            $('.unselect-all-gender-btn').click(function() {
                $('.gender-switch').prop('checked', true);
                $('.gender-switch').click();
            });

            $('.select-all-member-btn').click(function() {
                $('.member-switch').prop('checked', false);
                $('.member-switch').click();
            });

            $('.unselect-all-member-btn').click(function() {
                $('.member-switch').prop('checked', true);
                $('.member-switch').click();
            });

            $('#fromDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#toDate').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#week-select').click(function() {
                $('.week-checkbox').prop('checked', true);
            });

            $('#week-unselect').click(function() {
                $('.week-checkbox').prop('checked', false);
            });

            $('#myForm').submit(function() {
                var min = parseInt($('.ern-min-amount').val());
                var max = parseInt($('.ern-max-amount').val());

                if(ernActiveMax.isChecked()) {
                    if(max <= min) {
                        $('#modalWarning').modal('toggle');
                        return false;
                    }
                    else {
                        return true;
                    }
                } else {
                    return true;
                }
            });

            $('.ern-max-amount').change(function() {
                var min = parseInt($('.ern-min-amount').val());
                var max = parseInt($('.ern-max-amount').val());
                if (min == !null) {
                    $(".ern-min-amount").focus();
                }
                if (max <= min) {
                    $('#modalWarning').modal('toggle');
                }
            });

            $('.ern-min-amount').change(function() {
                var min = parseInt($('.ern-min-amount').val());
                var max = parseInt($('.ern-max-amount').val());
                if (max == !null) {
                    $(".ern-max-amount").focus();
                }
                if (max <= min) {
                    $('#modalWarning').modal('toggle');
                }
            });
        });
    </script>
@endsection
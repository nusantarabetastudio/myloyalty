@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <br>
            <form method="GET" action="{{ route('cardExchange.search') }}">
                <div class="row row-lg">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for..." name="s" value="{{ $search }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary buttonSubmit" type="submit">Search</button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
            <br><br>
            <table class="table table-hover dataTable table-striped width-full" id="d-customer">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Barcode Number</th>
                    <th>ID Number</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $customer)
                        <tr>
                            <td>{{ $customer->CUST_NAME }}</td>
                            <td>{{ $customer->mobilePhone ? $customer->mobilePhone->PHN_NUMBER : '-' }}</td>
                            <td>{{ $customer->CUST_BARCODE }}</td>
                            <td>{{ $customer->CUST_IDCARDS }}</td>
                            <td>
                                <a href="{{ url("customer/card-exchange/". $customer->CUST_RECID ."/edit") }}"><button type="button" class="btn btn-success btn-xs">Change Card</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $customers->appends(Request::only('s'))->links() !!}
        </div>
    </div>
@endsection

@section('scripts')
@endsection
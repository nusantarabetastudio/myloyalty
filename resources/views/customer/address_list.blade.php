@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">
    

@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/edit' }}" aria-controls="exampleTabsOne" role="tab">Customer Profile</a></li>
                            <li class="active" role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/address' }}" aria-controls="exampleTabsTwo">Address</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/phone' }}" aria-controls="exampleTabsTwo">Phone</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/email' }}" aria-controls="exampleTabsTwo">Email</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/interest' }}" aria-controls="exampleTabsThree">Interest</a></li>
                        </ul>
                        <div class="tab-content padding-top-10">
                            <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                                @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                                @endif
                                @if(Session::has('error'))
                                <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                </div>
                                @endif
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="row row-lg">
                                    @if(! isset($address))
                                    @if(empty($customer->addresses()->first()))
                                    <a href="javascript:void(0)" id="new-address" class="btn btn-primary">New Address</a>
                                    @endif
                                    <br><br>
                                    <div class="col-lg-12">
                                        <table class="table table-hover dataTable table-striped width-full" id="d-address">
                                            <thead>
                                                <tr>
                                                    <th>Address</th>
                                                    <th>Country</th>
                                                    <th>Province</th>
                                                    <th>City</th>
                                                    <th>Postal</th>
                                                    <th>Area</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        @endif
                                    </div>
                                    <div class="panel" id="form-input" style=" display: {{ isset($address) ? 'block' : 'none' }} ;">
                                        <div class="panel-body">
                                            <form method="POST" action="{{ isset($address) ? route('customer.address.update', $address->ADDR_RECID) : route('customer.address.store') }}" class="form-horizontal">
                                                {{ csrf_field() }}
                                                {{ isset($address) ? method_field('PUT') : ''}}
                                                <input type="hidden" name="CUST_RECID" value="{{ $customer->CUST_RECID }}">
                                                <div class="form-group form-material-sm">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Address</label>
                                                        <input type="text" name="ADDR_ADDRESS" class="form-control input-md" value="{{ isset($address) ? $address->ADDR_ADDRESS : '' }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material-sm">
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Country:</label>
                                                        <select name="ADDR_COUNTRY" class="form-control input-sm select2" id="country" required>
                                                            <option value=""></option>
                                                            @foreach($countries as $country)
                                                            @if($country->LOK_DESCRIPTION == 'INDONESIA')
                                                            <option value="{{ $country->LOK_RECID }}" selected>{{ $country->LOK_DESCRIPTION }}</option>
                                                            @else
                                                            <option value="{{ $country->LOK_RECID }}">{{ $country->LOK_DESCRIPTION }}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Province:</label>
                                                        <select name="ADDR_PROVINCE" class="form-control select2" id="province" required>
                                                            <option value="">Choose One</option>
                                                            @if(isset($address))
                                                            @foreach($provinces as $province)
                                                            @if(isset($address) && $province->LOK_RECID == $address->ADDR_PROVINCE)
                                                            <option value="{{ $province->LOK_RECID }}" class="province-data" selected>{{ $province->LOK_DESCRIPTION }}</option>
                                                            @else
                                                            <option value="{{ $province->LOK_RECID }}" class="province-data">{{ $province->LOK_DESCRIPTION }}</option>
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material-sm">
                                                    <div class="col-sm-6">
                                                        <label class="control-label">City</label>
                                                        <select name="ADDR_CITY" class="form-control select2" id="city">
                                                            <option value="">Choose One</option>
                                                            @if(isset($address))
                                                            @foreach($cities as $city)
                                                            @if(isset($address) && $city->LOK_RECID == $address->ADDR_CITY)
                                                            <option value="{{ $city->LOK_RECID }}" class="city-data" selected>{{ $city->LOK_DESCRIPTION }}</option>
                                                            @else
                                                            <option value="{{ $city->LOK_RECID }}" class="city-data">{{ $city->LOK_DESCRIPTION }}</option>
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="control-label">Area</label>
                                                        <select name="ADDR_AREA" class="form-control select2" id="">
                                                            <option value="">Choose One</option>
                                                            @foreach($areas as $area)
                                                            @if(isset($address) && $area->LOK_RECID == $address->ADDR_AREA)
                                                            <option value="{{ $area->LOK_RECID }}" selected>{{ $area->LOK_DESCRIPTION }}</option>
                                                            @else
                                                            <option value="{{ $area->LOK_RECID }}">{{ $area->LOK_DESCRIPTION }}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material-sm">
                                                    <div class="col-md-6 iTemplate">
                                                        <label class="control-label ">Address Type</label>
                                                        <select name="ADDR_TYPE" class="form-control select2" id="" required >
                                                        <option value="1" {{ isset($address) && $address->ADDR_TYPE == 1 ? 'selected' :'' }}>Alamat KTP</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="control-label">Postal Code</label>
                                                        <input type="text" class="form-control input-md" name="ADDR_POSTAL" value="{{ isset($address) ?  $address->ADDR_POSTAL : '' }}">
                                                    </div>
                                                </div>
                                                <div class="form-group form-material-sm">
                                                    <div class="com-sm-6 pull-right">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($address) ? 'Update' : 'Save' }}</button>
                                                        <button type="reset" class="btn btn-default waves-effect waves-light">Cancel</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="{{ route('customer.address.delete') }}" method="post" role="form">
                {!! csrf_field() !!}
                {!! method_field('DELETE') !!}
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="customer_id" id="customer_id">
                <input type="hidden" name="type" id="ADDR_TYPE">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Information !</h4>
                </div>
                <div class="modal-body">
                    Are you sure ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Delete</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#d-address').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'POST',
                    url : '{{ url('customer/'.$customer->CUST_RECID.'/address/data') }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data : 'ADDR_ADDRESS', name: 'ADDR_ADDRESS'},
                    { data : 'ADDR_COUNTRY', name: 'ADDR_COUNTRY'},
                    { data : 'ADDR_PROVINCE', name: 'ADDR_PROVINCE'},
                    { data : 'ADDR_CITY', name: 'ADDR_CITY'},
                    { data : 'ADDR_POSTAL', name: 'ADDR_POSTAL'},
                    { data : 'ADDR_AREA', name: 'ADDR_AREA'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ]
            });

            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });

            $('.date').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
                $('#customer_id').val($(event.relatedTarget).data('customer-id'));
                $('#ADDR_TYPE').val($(event.relatedTarget).data('type'));
            });

            $('#new-address').on('click', function(e) {
                $('#form-input').toggle();
            });

            $('#country').on('change', function() {
                var val = $(this).val();
                $.ajax({
                    url: '{{ url('/api/v1/country/') }}/' + val + '/province',
                    success: function(data) {
                        $('.province-data').remove();
                        $.each(data.data, function(i, data) {
                            $('#province').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('province-data'));
                        });
                        $('#province').select2({
                            'placeholder' : 'Choose One',
                            'allowClear' : true
                        }).change();
                    }
                });
            });

            @if (!isset($address))
            $('#country').change();
            @endif
            $('#province').on('change', function() {
                var val = $(this).val();
                if (val) {
                    $.ajax({
                        url: '{{ url('/api/v1/province/') }}/' + val + '/city',
                        success: function(data) {
                            $('.city-data').remove();
                            $.each(data.data, function(i, data) {
                                $('#city').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('city-data'));
                            });
                            $('#city').select2({
                                'placeholder' : 'Choose One',
                                'allowClear' : true
                            }).change();
                        }
                    });
                } else {
                    $('.city-data').remove();
                    $('#city').select2({
                        'placeholder' : 'Choose One',
                        'allowClear' : true
                    });
                }
            });
            $('#city').on('change', function() {
                var val = $(this).val();
                if (val) {
                    $.ajax({
                        url: '{{ url('/api/v1/city/') }}/' + val + '/area',
                        success: function(data) {
                            $('.area-data').remove();
                            $.each(data.data, function(i, data) {
                                $('#area').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('area-data'));
                            });
                            $('#area').select2({
                                'placeholder' : 'Choose One',
                                'allowClear' : true
                            }).change();
                        }
                    });
                } else {
                    $('.area-data').remove();
                    $('#area').select2({
                        'placeholder' : 'Choose One',
                        'allowClear' : true
                    });
                }
            });

        })
    </script>
    <script src="{{ asset('js/customer.js') }}"></script>
@endsection
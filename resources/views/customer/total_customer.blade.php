@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection
@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel dasboard-customer">
				<div class="panel-body">
					<div class="tab-content">
						<div class=" tab-pane animation-fade active" id="category-1" role="tabpanel">
							<div class="panel-group panel-group-simple panel-group-continuous" id="accordion2"
							 aria-multiselectable="true" role="tablist">
								<div class="panel">
									<div class="panel-heading" id="question-1" role="tab">
										<a class="panel-title" aria-controls="byStore" aria-expanded="true" data-toggle="collapse"
										 href="#byStore" data-parent="#accordion2">
											<h3>Total Customer By Store</h3>
										</a>
									</div>
									<div class="panel-collapse collapse in" id="byStore" aria-labelledby="question-1"
									 role="tabpanel">
										<div class="row">
											<a href="{{URL::to('/customer/exportbystore')}}"><button id="exportButton" class="btn btn-danger clearfix"> Export to Excel</button></a>
										</div>
										<div class="panel-body">
											<table class="tablesaw table-striped  dataTable table-bordered table-hover" id="datatable-table-store">
												<thead>
												    <tr>
												        <th>Store Name</th>
												        <th>Customer</th>
												    </tr>
												</thead>
												<tbody>
													@foreach($customersByStore as $customerByStore)
													<tr>
														<th>{{ $customerByStore->TNT_DESC }}</th>
														<th>{{ $customerByStore->total_customer_bystore }}</th>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading" id="question-2" role="tab">
										<a class="panel-title" aria-controls="byCardExchange" aria-expanded="false" data-toggle="collapse" href="#byCardExchange" data-parent="#accordion2">
											<h3>Total Customer By Card Exchange</h3>
										</a>
									</div>
									<div class="panel-collapse collapse" id="byCardExchange" aria-labelledby="question-2"
									 role="tabpanel">
										<div class="row">
											<form method="GET" action="{{ route('customer.exportByExchange') }}" class="form-horizontal">
												<div class="col-md-6">
													<div class="input-daterange" data-plugin="datepicker">
														<div class="input-group">
														  <span class="input-group-addon">
														    <i class="icon wb-calendar" aria-hidden="true"></i>
														  </span>
														  <input type="text" class="form-control" id="fromDate-card" name="fromDatecard" value="{{date('m/01/Y')}}" />
														</div>
														<div class="input-group">
														  <span class="input-group-addon">to</span>
														  <input type="text" class="form-control" id="toDate-card" name="toDatecard" value="{{date('m/d/Y')}}" />
														</div>
												    </div>
												</div>
												<div class="col-md-4">
													<li id="submitBtn-card" class="btn btn-primary waves-effect waves-light submitBtn-card">Submit</li>
													<button id="exportButton" class="btn btn-danger clearfix"> Export to Excel</button>
												</div>
											</form>
										</div>
										<div class="panel-body">
											<table class="tablesaw table-striped  dataTable table-bordered table-hover datatable-table-cardEx" id="datatable-table-cardEx">
												<thead>
												    <tr>
												        <th>Store Name</th>
												        <th>Customer</th>
												    </tr>
												</thead>
												<tbody>
												@foreach($tenants as $tenant)
													<tr class="item-card">
														<th>{{ $tenant->TNT_DESC}}</th>
														<th>0</th>
													</tr>
												@endforeach
													<tr class="iTemplate" id="item-template-card">
														<th class="item-store-card"></th>
														<th class="item-customer-card"></th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading" id="question-1" role="tab">
										<a class="panel-title" aria-controls="newCustomer" aria-expanded="true" data-toggle="collapse" href="#newCustomer" data-parent="#accordion2">
											<h3>Total Customer New Customer</h3>
										</a>
									</div>
									<div class="panel-collapse collapse" id="newCustomer" aria-labelledby="question-1" role="tabpanel">
										<div class="row">
											<form method="GET" action="{{ route('customer.exportBynewCustomer') }}" class="form-horizontal">
												<div class="col-md-6">
													<div class="input-daterange" data-plugin="datepicker">
														<div class="input-group">
															<span class="input-group-addon">
																<i class="icon wb-calendar" aria-hidden="true"></i>
															</span>
															<input type="text" class="form-control" id="fromData" name="fromData" value="{{date('m/01/Y')}}" />
														</div>
														<div class="input-group">
															<span class="input-group-addon">to</span>
															<input type="text" class="form-control" id="toDate" name="toDate" value="{{date('m/d/Y')}}" />
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<li id="submitBtn" class="btn btn-primary waves-effect waves-light">Submit</li>
													<button id="exportButton" class="btn btn-danger clearfix"> Export to Excel</button>
												</div>
											</form>
										</div>
										<div class="panel-body">
											<table class="tablesaw table-striped  dataTable table-bordered table-hover datatable-table-cust" id="datatable-table-cust">
												<thead>
													<tr>
														<th>Store</th>
														<th>Customer</th>
													</tr>
												</thead>
												<tbody>
												@foreach($tenants as $tenant)
													<tr class="item">
														<th>{{ $tenant->TNT_DESC}}</th>
														<th>0</th>
													</tr>
												@endforeach
													<tr class="iTemplate" id="item-template">
														<th class="item-store"></th>
														<th class="item-customer"></th>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		    <div class="row">
		        <div class="col-md-12" id="result"><i class="loading iTemplate"></li></div>
		    </div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/global/highcharts.js') }}"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script type="text/javascript">
		var val = '';
	    $(document).ready(function(){
	        var oTableStore = $('#datatable-table-store').DataTable({
	            processing: true
	        });
	        var oTableCardEx = $('.datatable-table-cardEx').DataTable({
	            processing: true
	        });
	        // console.log(oTableCardEx);
	        var oTableCust =$('.datatable-table-cust').DataTable({
	        	processing: true
	        });
	        $('.select2').select2({
	            'placeholder' : 'Choose One',
	            'allowClear' : true,
	            'width' : '100%'
	        });
		    $('.date').datepicker({
		        format : 'yyyy/mm/dd',
		        orientation: "bottom left"
		    });
		    $('#submitBtn').click(function() {
		    	var fromData = $('#fromData').val();
		    	var toDate = $('#toDate').val();
		    	var newCust = 'newCust';
                $('#result').fadeIn();
                $('.loading').removeClass('iTemplate');
                $('.dasboard-customer').hide();
		    	$.ajax({
		    		type : "get",
		    		url : '{{ url("/customer/dataCustomer") }}',
		    		data : {fromData : fromData, toDate : toDate, newCust : newCust},
		    		success: function(data) {
                        $('#result').hide();
                        $('.loading').addClass('iTemplate');
                        $('.dasboard-customer').fadeIn();
		    			$('.datatable-table-cust').DataTable().destroy();
		    			var template = $('#item-template');
		    			var items = data;
		    			var clone;
		    			$('.item').remove();
		    			for ($i = 0; $i < items.length; $i++) {
		    				var item = items[$i];
		    				clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
		    				$('.item-store', clone).text(item.STORENAME);
		    				$('.item-customer', clone).text(item.CUSTOMER);
		    				template.before(clone);
		    			}
		    			$('.datatable-table-cust').DataTable();
		    		}
		    	});
		    });
		    $('#submitBtn-card').click(function() {
		    	var fromDatecard = $('#fromDate-card').val();
		    	var toDatecard = $('#toDate-card').val();
		    	var card = 'card';
                $('#result').fadeIn();
                $('.loading').removeClass('iTemplate');
                $('.dasboard-customer').hide();
		    	$.ajax({
		    		type : "get",
		    		url : '{{ url("/customer/dataCustomerCard") }}',
		    		data : {fromDatecard : fromDatecard, toDatecard : toDatecard, card : card},
		    		success: function(data) {
                        $('#result').hide();
                        $('.loading').addClass('iTemplate');
                        $('.dasboard-customer').fadeIn();
		    			$('.datatable-table-cardEx').DataTable().destroy();
		    			var template = $('#item-template-card');
		    			var items = data;
		    			var clone;
		    			$('.item-card').remove();
		    			for ($i = 0; $i < items.length; $i++) {
		    				var item = items[$i];
		    				clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item-card');
		    				$('.item-store-card', clone).text(item.Store);
		    				$('.item-customer-card', clone).text(item.totalcutomer);
		    				template.before(clone);
		    			}
		    			$('.datatable-table-cardEx').DataTable();
		    		}
		    	});
		    });
		});
	</script>
    <script type="text/javascript">
	<?php
		$totalCommaSeperated = implode(', ', $totalCustomers['total']);
		$monthCommaSeperated = implode(', ', $totalCustomers['month']);
		$numberCustomer = implode(', ', $totalCustomers['numberCustomer']);
	?>
	$(function () {
	    Highcharts.chart('container', {
	        title: {
	            text: 'Total Customer  : '+{{$customer->total}},
	            x: -20 //center
	        },
	        xAxis: {
	            categories: [{!! $monthCommaSeperated !!}]
	        },
	        yAxis: {
	            title: {
	                text: 'Total Record'
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            valueSuffix: ' Customers'
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: [{
	            name: 'Total Customer',
	            data: [{{ $totalCommaSeperated }}]
	        },{
	            name: 'Total Customer Amonth',
	            data: [{{ $numberCustomer }}]
	        }],
	    });
	});
    </script>
@endsection
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Transaction Detail Receipt's</h4>
</div>
<div class="modal-body">
    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="receipt-number-datatable">
    	<thead>
    		<tr>
    			<th>Receipt Number</th>
    			<th>Total Transaction</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($earnings as $earning)
                <tr>
                    <td>{{ $earning->PSD_DOC_NO }}</td>
                    <td>{{ thousandSeparator($earning->total_amount) }}</td>
                    <td><a href="{{ route('customer.detail.view-product', [$id, $earning->PSD_DOC_NO]) }}" type="button" class="btn btn-success btn-xs item-detail" data-toggle="modal" data-target="#view-product-modal">View Product</a></td>
                </tr>
    		@endforeach
    	</tbody>
    </table>
</div>
<script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript">
        var oTable;
        $(function(){
            oTable = $('#receipt-number-datatable').DataTable({
                processing: true
            });
        });
</script>
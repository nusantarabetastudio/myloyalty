@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">

@endsection

@section('content')

    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li role="presentation"><a href="{{ route('customer.detail', $customer->CUST_RECID) }}" >Customer Profile</a></li>
                            <li role="presentation"><a href="{{route('customer.detail.point-history', $customer->CUST_RECID)}}" aria-controls="exampleTabsTwo" role="tab">Customer Transaction</a></li>
                            <li class="active" role="presentation"><a href="{{ route('customer.detail.logs', $customer->CUST_RECID) }}">Customer Logs</a></li>
                            <li role="presentation"><a href="{{ route('customer.detail.card-exchange', $customer->CUST_RECID) }}">Customer Card exchange</a></li>
                        </ul>
                        <div class="tab-content padding-top-20">
                            <div class="tab-pane active" id="exampleTabsFour" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="dataTables_length">
                                            <label>
                                                Show
                                                <select class="form-control limit">
                                                <option data-limit="10">10</option>
                                                <option data-limit="25">25</option>
                                                <option data-limit="50">50</option>
                                                <option data-limit="100">100</option>
                                                </select>
                                                entries
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="dataTables_filter">
                                            <label>
                                                Search:
                                                <input type="search" class="form-control search">
                                                <input type="hidden" class="idcustomer" value="{{ $customerId }}">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="sk-folding-cube loading-wrapper" style="display:none;">
                                    <div class="sk-cube1 sk-cube"></div>
                                    <div class="sk-cube2 sk-cube"></div>
                                    <div class="sk-cube4 sk-cube"></div>
                                    <div class="sk-cube3 sk-cube"></div>
                                </div>
                                <table class="tablesaw table-striped  dataTable table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Store</th>
                                        <th>Old Data</th>
                                        <th>New Data</th>
                                        <th>Type</th>
                                        <th>User</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($exchange as $data)
                                        <tr class="item">
                                            <td>{{$data->date ?: '-'}}</td>
                                            <td>{{$data->store_code ?: '-'}} - {{$data->store_name ?: '-'}}</td>
                                            <td>{{$data->old_data ?: '-'}}</td>
                                            <td>{{$data->new_data ?: '-'}}</td>
                                            <td>{{$data->type ?: '-'}}</td>
                                            <td>{{$data->user_username ?: '-'}}</td>
                                        </tr>
                                    @endforeach()
                                        <tr class="iTemplate" id="item-template">
                                            <td class="item-date"></td>
                                            <td class="item-store_code"></td>
                                            <td class="item-old_data"></td>
                                            <td class="item-new_data"></td>
                                            <td class="item-type"></td>
                                            <td class="item-user_username"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                    <div class="row mart20">
                                        <div class="col-sm-12 text-right pagination-wrapper">
                                            <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginator->previous_page }}">
                                                Previous
                                            </button>
                                            Page
                                            <span class="current-page" data-page="{{ $paginator->current_page }}">
                                                {{ $paginator->current_page }}
                                            </span>
                                             /
                                            <span class="total-page">{{ $paginator->total_pages }}</span>
                                            <button class="btn btn-primary next-btn pagination-btn{{ (!$paginator->next_page) ? ' disabled' : '' }}" data-page="{{ $paginator->next_page }}">
                                                Next
                                            </button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.pagination-btn').click(function() {
                $('.current-page').data('page', $(this).data('page'));
                getData();
            });
            $('.limit').change(function() {
                getData(true);
            });
            $('.search').keyup(function(e) {
                if(e.keyCode == 13) {
                    getData(true);
                }
            });
            function getData(resetPage = false){
                var limit = $('.limit').find('option:selected').data('limit');
                var page = $('.current-page').data('page');
                var search = $('.search').val();
                var idcustomer = $('.idcustomer').val();
                var value = 'exchange';

                if (resetPage) page = 1;
                var dataUrl = '{{ route('customer.customerTransaction') }}?limit=' + limit + '&page=' + page + '&s=' + search + '&idcustomer=' + idcustomer + '&value=' + value;

                if (dataUrl) {
                    $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: dataUrl,
                        success: function(data) {
                            var template = $('#item-template');
                            var items = data.DATA;
                            $('.item').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
                                $('.item-date', clone).text(item.date ? item.date : '-');
                                $('.item-store_code', clone).text(item.store_code ? item.store_code : '-' + ' - ' + item.store_name ? item.store_name : '-');
                                $('.item-old_data', clone).text(item.old_data ? item.old_data : '-');
                                $('.item-new_data', clone).text(item.new_data ? item.new_data : '-');
                                $('.item-type', clone).text(item.type ? item.type : '-');
                                $('.item-user_username', clone).text(item.user_username ? item.user_username : '-');
                                template.before(clone);

                            }
                            $('.current-page').text(data.paginator.current_page).data('page', data.paginator.current_page);
                            $('.previous-btn').data("page", data.paginator.previous_page );
                            $('.next-btn').data("page", data.paginator.next_page);
                            $('.total-page').text(data.paginator.total_pages);
                            $('.pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
        });
    </script>
@endsection
@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">

@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            @if(isset($customer))
                                <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab">Customer Profile</a></li>
                                <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/address' }}" aria-controls="exampleTabsTwo">Address</a></li>
                                <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/phone' }}" aria-controls="exampleTabsTwo">Phone</a></li>
                                <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/email' }}" aria-controls="exampleTabsTwo">Email</a></li>
                                <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/interest' }}" aria-controls="exampleTabsThree">Interest</a></li>
                            @endif
                        </ul>
                        <div class="tab-content padding-top-20">
                            <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                @if($errors->has())
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}<br>
                                        @endforeach
                                    </div>
                                @endif
                                <form class="form-horizontal" id="form" action="{{ isset($customer) ? url('customer/'.$customer->CUST_RECID) : route('customer.store')}}" method="POST" enctype="multipart/form-data">
                                    @if(isset($customer))
                                        {!! method_field('PUT') !!}
                                    @endif
                                    {{ csrf_field() }}
                                @if(isset($customer))
                                    @if($customer->CUST_PNUM == null)
                                        <input type="hidden" name="CUST_PNUM" value="{{ $customer->CUST_PNUM }}">
                                        <input type="hidden" name="CUST_PTYPE" value="{{ $customer->CUST_PTYPE }}">
                                    @else

                                    @endif
                                @endif
                                <div class="form-group form-material-sm marb0">
                                    <div class="col-sm-10">
                                        <div class="form-group form-material-sm">
                                            <div class="col-sm-6">
                                                <label class="control-label">Barcode No:</label>
                                                <input type="text" class="form-control input-sm" id="barcode" name="CUST_BARCODE" value="{{ isset($customer) ? $customer->CUST_BARCODE : old('CUST_BARCODE') }}" {{ isset($customer) ? 'disabled' : '' }} required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Name:</label>
                                                <input type="text" class="form-control input-sm" name="CUST_NAME" value="{{ isset($customer) ? $customer->CUST_NAME : old('CUST_NAME') }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group form-material-sm">
                                            <div class="col-sm-6">
                                                <label class="control-label">ID Type:</label>
                                                <select name="CUST_IDTYPE" class="form-control input-sm select2" required>
                                                    <option value="" disabled selected>Choose One</option>
                                                    @foreach($id_types as $id_type)
                                                        @if(isset($customer) && $id_type->LOK_RECID == $customer->CUST_IDTYPE)
                                                            <option value="{{ $id_type->LOK_RECID }}" selected>{{ $id_type->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $id_type->LOK_RECID }}" {{ old('CUST_IDTYPE') == $id_type->LOK_RECID ? 'selected' : '' }}>{{ $id_type->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">ID Number:</label>
                                                <input type="text" class="form-control input-sm" name="CUST_IDCARDS" value="{{ isset($customer) ? $customer->CUST_IDCARDS : old('CUST_IDCARDS') }}" required pattern="[A-Z0-9]+" maxlength="20">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-2" style="margin-bottom:-80px;">
                                        <div class="row">
                                            <input type="file" name="profile_picture" id="input-file-max-fs" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ isset($customer) ? $customer->CUST_PROFILEPICTURE : url('assets/global/portraits/5.jpg') }}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-5">
                                        <label class="control-label">Gender:</label>
                                        <select name="CUST_GENDER" class="form-control input-sm col-sm-8 select2">
                                            <option value="">Choose One</option>
                                            @foreach($genders as $gender)
                                                @if(isset($customer) && $customer->CUST_GENDER == $gender->LOK_RECID)
                                                    <option value="{{ $gender->LOK_RECID }}" selected>{{ $gender->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $gender->LOK_RECID }}" {{ old('CUST_GENDER') == $gender->LOK_RECID ? 'selected' : '' }}>{{ $gender->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <label class="control-label">Marital Status:</label>
                                        <select name="CUST_MARITAL" class="form-control input-sm select2" id="marital">
                                            <option value="">Choose One</option>
                                            @foreach($marital_statuses as $marital)
                                                @if(isset($customer) && $customer->CUST_MARITAL == $marital->LOK_RECID)
                                                    <option value="{{ $marital->LOK_RECID }}" selected>{{ $marital->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $marital->LOK_RECID }}" {{ old('CUST_MARITAL') == $marital->LOK_RECID ? 'selected' : '' }}>{{ $marital->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-2" style="margin-top: 28px;">
                                        <label class="control-label">Status:</label>
                                        <input type="checkbox" id="" value="1" class="form-control" name="CUST_ACTIVE" data-plugin="switchery" data-size="small" {{ (isset($customer) && $customer->CUST_ACTIVE == 1) || !isset($customer) ? 'checked' : '' }} {{ isset($customer) ? '' : 'disabled' }}>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-6">
                                        <label class="control-label">Date of Birth:</label>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                        </span>
                                            <input type="text" class="form-control input-sm date" id="date_of_birth" name="CUST_DOB" value="{{ isset($customer->CUST_DOB) ? $customer->CUST_DOB->format('d/m/Y') : old('CUST_DOB') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">Place of Birth:</label>
                                        <input type="text" class="form-control input-sm" name="CUST_POD" value="{{ isset($customer) ? $customer->CUST_POD : old('CUST_POD') }}">
                                    </div>
                                </div>
                                @if(!isset($customer))
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-12">
                                            <label class="">Address:</label>
                                            <input type="text" class="form-control input-sm" name="ADDR_ADDRESS" value="{{ isset($customer) ? $customer->addresses[0]->ADDR_ADDRESS : old('ADDR_ADDRESS') }}">
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-6">
                                            <label class="control-label">Country:</label>
                                            <select name="ADDR_COUNTRY" class="form-control input-sm select2" id="country">
                                                <option value=""></option>
                                                @foreach($countries as $country)
                                                    @if($country->LOK_DESCRIPTION == 'INDONESIA')
                                                        <option value="{{ $country->LOK_RECID }}" selected>{{ $country->LOK_DESCRIPTION }}</option>
                                                    @else
                                                        <option value="{{ $country->LOK_RECID }}" {{ old('ADDR_COUNTRY') == $country->LOK_RECID ? 'selected' : '' }}>{{ $country->LOK_DESCRIPTION }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Province:</label>
                                            <select name="ADDR_PROVINCE" id="province" class="form-control input-sm select2">
                                                <option value="">Choose One</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="control-label">City:</label>
                                            <select name="ADDR_CITY" class="form-control input-sm select2" id="city">
                                                <option value="">Choose One</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">Area:</label>
                                            <select name="ADDR_AREA" class="form-control input-sm select2" id="area">
                                                <option value="">Choose One</option>
                                                    @foreach($areas as $area)
                                                        <option value="{{ $area->LOK_RECID }}" {{ old('ADDR_AREA') == $area->LOK_RECID ? 'selected' : '' }}>{{ $area->LOK_DESCRIPTION }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">ZIP Code:</label>
                                            <input type="text" class="form-control" name="ADDR_POSTAL" value="{{ isset($customer) ? $customer->addresses->first()->ADDR_POSTAL : old('ADDR_POSTAL')  }}">
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="control-label">Home Phone:</label>
                                            <input type="text" class="form-control" name="PHN_HOME" value="{{ isset($customer) ? $customer->phones[1]->PHN_NUMBER : old('PHN_HOME') }}" pattern="[A-Z0-9]+">
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">Office Phone:</label>
                                            <input type="text" class="form-control" name="PHN_OFFICE" value="{{ isset($customer) ? $customer->phones[2]->PHN_NUMBER : old('PHN_OFFICE') }}" pattern="[A-Z0-9]+">
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">Mobile Phone:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">+62</span>
                                                <input type="text" id="mobile-phone" class="form-control" name="PHN_MOBILE" value="{{ isset($customer) ? $customer->phones[0]->PHN_NUMBER : old('PHN_MOBILE') }}" pattern="[A-Z0-9]+" required>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group form-material-sm">
                                    @if(!isset($customer))
                                        <div class="col-sm-4">
                                            <label class="control-label">Email:</label>
                                            <input type="email" class="form-control" name="EML_EMAIL" value="{{ isset($customer) ? $customer->emails[0]->EML_EMAIL : old('EML_EMAIL') }}" required>
                                        </div>
                                    @endif
                                    <div class="{{ (!isset($customer)) ? 'col-sm-4' : 'col-sm-6' }}">
                                        <label class="control-label">Nationality:</label>
                                        <select name="CUST_NATIONALITY" class="form-control input-sm select2">
                                            <option value=""></option>
                                            @foreach($nationalities as $nationality)
                                                @if(isset($customer) && $customer->CUST_NATIONALITY == $nationality->LOK_RECID)
                                                    <option value="{{ $nationality->LOK_RECID }}" selected>{{ $nationality->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $nationality->LOK_RECID }}" {{ old('CUST_NATIONALITY') == $nationality->LOK_RECID ? 'selected' : '' }}>{{ $nationality->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="{{ (!isset($customer)) ? 'col-sm-4' : 'col-sm-6' }}">
                                        <label class="control-label">Blood:</label>
                                        <select name="CUST_BLOOD" class="form-control input-sm select2">
                                            <option value=""></option>
                                            @foreach($bloods as $blood)
                                                @if(isset($customer) && $blood->LOK_RECID == $customer->CUST_BLOOD)
                                                    <option value="{{ $blood->LOK_RECID }}" selected>{{ $blood->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $blood->LOK_RECID }}" {{ old('CUST_BLOOD') == $blood->LOK_RECID ? 'selected' : '' }}>{{ $blood->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group form-material-sm">
                                    <div class="col-sm-4">
                                        <label class="control-label">Religion:</label>
                                        <select name="CUST_RELIGION" class="form-control input-sm select2">
                                            <option value=""></option>
                                            @foreach($religions as $religion)
                                                @if(isset($customer) && $religion->LOK_RECID == $customer->CUST_RELIGION)
                                                    <option value="{{ $religion->LOK_RECID }}" selected>{{ $religion->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $religion->LOK_RECID }}" {{ old('CUST_RELIGION') == $religion->LOK_RECID ? 'selected' : '' }}>{{ $religion->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label">Occupation:</label>
                                        <select name="CUST_OCCUPATION" class="form-control input-sm select2">
                                            <option value=""></option>
                                            @foreach($occupations as $occupation)
                                                @if(isset($customer) && $occupation->LOK_RECID == $customer->CUST_OCCUPATION)
                                                    <option value="{{ $occupation->LOK_RECID }}" selected>{{ $occupation->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $occupation->LOK_RECID }}" {{ old('CUST_OCCUPATION') == $occupation->LOK_RECID ? 'selected' : '' }}>{{ $occupation->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label">Card Type:</label>
                                        <select name="CUST_CARDTYPE" class="form-control input-sm select2">
                                            @foreach($cardTypes as $cardType)
                                                @if(isset($customer) && $cardType->CARD_RECID == $customer->CUST_CARDTYPE)
                                                    <option value="{{ $cardType->CARD_RECID }}" {{ old('CUST_CARDTYPE') == $cardType->LOK_RECID ? 'selected' : '' }}>{{ $cardType->CARD_DESC }}</option>
                                                @else
                                                    <option value="{{ $cardType->CARD_RECID }}" >{{ $cardType->CARD_DESC }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @if (!isset($customer))
                                    <div class="col-sm-4">
                                        <label class="control-label">Interest:</label>
                                        <select name="interests[]" class="form-control input-sm select2" multiple="multiple">
                                            @foreach($interests as $interest)
                                                <option value="{{ $interest->LOK_RECID }}" >{{ $interest->LOK_DESCRIPTION }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                </div>
                                <br>
                                <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">{{ isset($customer) ? 'Update' : 'Submit' }}</button>
                                </form>
                            </div>
                            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">

                            </div>
                            <div class="tab-pane" id="exampleTabsThree" role="tabpanel">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>

    <script type="text/javascript">
        var url = '{{ url('/') }}';

        function openFileOption() {
            $('#file').click();
        }

        $(function () {
            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });

            $('.date').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            /* Remove First '0' on mobile phone */
            $('#mobile-phone').change(function() {
                var mobilePhone = $(this).val();
                while(true) {
                    if (mobilePhone.charAt(0) === '0' || mobilePhone.charAt(0) === '+') {
                        mobilePhone = mobilePhone.slice(1);
                        $(this).val(mobilePhone);
                    } else {
                        break;
                    }
                }
            });

            $('#country').on('change', function() {
                var val = $(this).val();
                $.ajax({
                    url: '{{ url('/api/v1/country/') }}/' + val + '/province',
                    success: function(data) {
                        $('.province-data').remove();
                        $.each(data.data, function(i, data) {
                            $('#province').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('province-data'));
                        });
                        $('#province').select2({
                            'placeholder' : 'Choose One',
                            'allowClear' : true
                        }).change();
                    }
                });
            });

            $('#country').change();
            $('#province').on('change', function() {
                var val = $(this).val();
                if (val) {
                    $.ajax({
                        url: '{{ url('/api/v1/province/') }}/' + val + '/city',
                        success: function(data) {
                            $('.city-data').remove();
                            $.each(data.data, function(i, data) {
                                $('#city').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('city-data'));
                            });
                            $('#city').select2({
                                'placeholder' : 'Choose One',
                                'allowClear' : true
                            }).change();
                        }
                    });
                } else {
                    $('.city-data').remove();
                    $('#city').select2({
                        'placeholder' : 'Choose One',
                        'allowClear' : true
                    });
                }
            });

            $('#province').change();
            $('#city').on('change', function() {
                var val = $(this).val();
                if (val) {
                    $.ajax({
                        url: '{{ url('/api/v1/city/') }}/' + val + '/area',
                        success: function(data) {
                            $('.area-data').remove();
                            $.each(data.data, function(i, data) {
                                $('#area').append($('<option>', {value: data.LOK_RECID, text: data.LOK_DESCRIPTION}).addClass('area-data'));
                            });
                            $('#area').select2({
                                'placeholder' : 'Choose One',
                                'allowClear' : true
                            }).change();
                        }
                    });
                } else {
                    $('.area-data').remove();
                    $('#area').select2({
                        'placeholder' : 'Choose One',
                        'allowClear' : true
                    });
                }
            });

            $('#barcode').formatter({
                'pattern' : '@{{9999999999}}',
                'persistence' : true
            })

            $('#submitBtn').click(function(e) {
                if ($('#barcode').val().length < 10) {
                    e.preventDefault();
                    alert('Please Input 10 Digit Barcode!');
                }
            });

            // Warning
            $(window).on('beforeunload', function(){
                return 'Are you sure you want to leave?';
            });

            // Form Submit
            $(document).on("submit", "form", function(event){
                // disable unload warning
                $(window).off('beforeunload');
            });
        })
    </script>
    <script src="{{ asset('js/customer.js') }}"></script>
@endsection
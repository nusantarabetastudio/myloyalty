@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form action="{{ route('customer.card-exchange.update', $customer->CUST_RECID) }}" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Current Card</legend>
                            <div class="form-group">
                                <label for="" class="control-label col-md-2">Barcode : </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="" value="{{ $customer->CUST_BARCODE }}" disabled>
                                    <input type="hidden" name="EXC_OLDCARD" value="{{ $customer->CUST_BARCODE }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label col-md-2">Name : </label>
                                <div class="col-md-10">
                                    <input type="text"  class="form-control" id="" value="{{ $customer->CUST_NAME }}" disabled>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label for="" class="control-label col-md-2">Name on Card : </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="CUST_ALIASNAME" id="" value="{{ $customer->CUST_ALIASNAME }}">
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <label for="" class="control-label col-md-2">Expired Date : </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="" value="{{ $customer->CUST_EXPIREDATE ? $customer->CUST_EXPIREDATE->format('d/m/Y') : '-' }}" disabled>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <legend>Change To</legend>
                            <div class="form-group">
                                <label for="" class="control-label col-md-2">Barcode: </label>
                                <div class="col-md-10">
                                    <input type="text" name="EXC_NEWCARD" class="form-control" autocomplete="off" id="barcode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label col-md-2">Card Type : </label>
                                <div class="col-md-10">
                                    <input type="hidden" name="EXC_OLDCARDTYPE" class="form-control" value="{{ $customer->CUST_CARDTYPE }}">
                                    <select name="EXC_NEWCARDTYPE" id="" class="form-control select2">
                                        <option value=""></option>
                                        @foreach($cardTypes as $cardType)
                                            <option value="{{ $cardType->CARD_RECID }}">{{ $cardType->CARD_DESC }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label col-md-2">Change Type : </label>
                                <div class="col-md-10">
                                    <select name="EXC_TYPE" id="" class="form-control select2">
                                        <option value=""></option>
                                        <option value="3">Replacement</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label col-md-2">Reason : </label>
                                <div class="col-md-10">
                                    <textarea name="EXC_NOTES" id="" class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>

    <script>
        $(function() {
            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true
            });

            $('#barcode').formatter({
                'pattern' : '@{{9999999999}}',
                'persistence' : true
            })

            $('#submitBtn').click(function(e) {
                if ($('#barcode').val().length < 10) {
                    e.preventDefault();
                    alert('Please Input 10 Digit Barcode!');
                }
            });
        })
    </script>
@endsection

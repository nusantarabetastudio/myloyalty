@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">

@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li role="presentation"><a href="{{ route('customer.detail', $customer->CUST_RECID) }}" >Customer Profile</a></li>
                            <li role="presentation"><a href="{{route('customer.detail.point-history', $customer->CUST_RECID)}}" >Customer Transaction</a></li>
                            
                            <li  role="presentation"><a href="{{ route('customer.detail.logs', $customer->CUST_RECID) }}">Customer Logs</a></li>
                            <li class="active" role="presentation"><a href="{{ route('customer.detail.card-exchange', $customer->CUST_RECID) }}" aria-controls="exampleTabsTwo" role="tab">Customer Card exchange</a></li>
                        </ul>
                        <div class="tab-content padding-top-20">
                            <div class="tab-pane active" id="exampleTabsFour" role="tabpanel">
                                <table class="table table-hover dataTable table-striped width-full" id="d-category">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Old Card</th>
                                        <th>New Card</th>
                                        <th>Type</th>
                                        <th>Store</th>
                                        <th>CS</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#d-category').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('customer.detail.card-exchange.data', $customer->CUST_RECID) }}',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns : [
                    { data : 'EXC_DATE', name: 'EXC_DATE'},
                    { data : 'EXC_TIME', name: 'EXC_TIME'},
                    { data : 'EXC_OLDCARD', name: 'EXC_OLDCARD'},
                    { data : 'EXC_NEWCARD', name: 'EXC_NEWCARD'},
                    { data : 'type', name: 'type'},
                    { data : 'store', name: 'store'},
                    { data : 'EXC_USER', name: 'EXC_USER'}
                ]
            });
        });
    </script>
@endsection
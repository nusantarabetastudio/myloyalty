@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">

@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/edit' }}" aria-controls="exampleTabsOne" role="tab">Customer Profile</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/address' }}" aria-controls="exampleTabsTwo">Address</a></li>
                            <li class="active" role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/phone' }}" aria-controls="exampleTabsTwo">Phone</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/email' }}" aria-controls="exampleTabsTwo">Email</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/interest' }}" aria-controls="exampleTabsThree">Interest</a></li>
                        </ul>
                        <div class="tab-content padding-top-10">
                            <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                                <div class="panel">
                                    <div class="panel-body">
                                        @if(Session::has('success'))
                                            <div class="alert alert-success">
                                                {{ Session::get('success') }}
                                            </div>
                                        @endif
                                        @if(Session::has('error'))
                                            <div class="alert alert-danger">
                                                {{ Session::get('error') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="row row-lg">
                                            @if( ! isset($phone))
                                            <a href="javascript:void(0)" id="new" class="btn btn-primary">New Phone</a>
                                            <br><br>
                                            <div class="col-lg-12">
                                                <table class="table table-hover dataTable table-striped width-full" id="d-tables">
                                                    <thead>
                                                    <tr>
                                                        <th>Type</th>
                                                        <th>Number</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                @endif
                                                <div class="panel" id="form-input" style="border:1px solid #000; display: {{ isset($phone) ? 'block' : 'none' }} ;">
                                                    <div class="panel-body">
                                                        <form method="POST" action="{{ isset($phone) ? route('customer.phone.update', $phone->PHN_RECID) : route('customer.phone.store') }}" class="form-horizontal">
                                                            {{ csrf_field() }}
                                                            {{ isset($phone) ? method_field('PUT') : ''}}
                                                            <input type="hidden" name="CUST_RECID" value="{{ $customer->CUST_RECID }}">
                                                            <div class="form-group form-material">
                                                                <div class="col-md-6">
                                                                    <label class="control-label text-left">Phone Type</label>
                                                                    @if (isset($phone) && $phone->PHN_TYPE == 1)
                                                                        <select name="PHN_TYPE" class="form-control select2" id="" required readonly='true'>
                                                                            <option value="1" selected>Mobile 1</option>
                                                                        </select>
                                                                    @else
                                                                    <select name="PHN_TYPE" class="form-control select2" id="" required>
                                                                        <option value="">Choose One</option>
                                                                        @if (empty($customer->phones()->where('PHN_TYPE', 1)->first()))
                                                                        <option value="1" {{ isset($phone) && $phone->PHN_TYPE == 1 ? 'selected' :'' }}>Mobile 1</option>
                                                                        @endif
                                                                        <option value="2" {{ isset($phone) && $phone->PHN_TYPE == 2 ? 'selected' :'' }}>Mobile 2</option>
                                                                        <option value="3" {{ isset($phone) && $phone->PHN_TYPE == 3 ? 'selected' :'' }}>Mobile 3</option>
                                                                        <option value="4" {{ isset($phone) && $phone->PHN_TYPE == 4 ? 'selected' :'' }}>Business 1</option>
                                                                        <option value="5" {{ isset($phone) && $phone->PHN_TYPE == 5 ? 'selected' :'' }}>Business 2</option>
                                                                        <option value="6" {{ isset($phone) && $phone->PHN_TYPE == 6 ? 'selected' :'' }}>Business 3</option>
                                                                        <option value="7" {{ isset($phone) && $phone->PHN_TYPE == 7 ? 'selected' :'' }}>Home 1</option>
                                                                        <option value="8" {{ isset($phone) && $phone->PHN_TYPE == 8 ? 'selected' :'' }}>Home 2</option>
                                                                        <option value="9" {{ isset($phone) && $phone->PHN_TYPE == 9 ? 'selected' :'' }}>Home 3</option>
                                                                        <option value="10" {{ isset($phone) && $phone->PHN_TYPE == 10 ? 'selected' :'' }}>Company 1</option>
                                                                        <option value="11" {{ isset($phone) && $phone->PHN_TYPE == 11 ? 'selected' :'' }}>Company 2</option>
                                                                        <option value="12" {{ isset($phone) && $phone->PHN_TYPE == 12 ? 'selected' :'' }}>Company 3</option>
                                                                        <option value="13" {{ isset($phone) && $phone->PHN_TYPE == 13 ? 'selected' :'' }}>Facsimile 1</option>
                                                                        <option value="14" {{ isset($phone) && $phone->PHN_TYPE == 14 ? 'selected' :'' }}>Facsimile 2</option>
                                                                        <option value="15" {{ isset($phone) && $phone->PHN_TYPE == 15 ? 'selected' :'' }}>Facsimile 3</option>
                                                                    </select>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label class="control-label text-left">Number</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">+62</span>
                                                                        <input type="text" id="phone-number" class="form-control" name="PHN_NUMBER" value="{{ isset($phone) ? $phone->PHN_NUMBER : '' }}" pattern="[A-Z0-9]+" required>
                                                                    </div>
                                                                </div>
                                                                {{-- <div class="col-md-1">
                                                                    <div class="checkbox-custom checkbox-primary">
                                                                        <input type="checkbox" name="PHN_ACTIVE" id="active" {{ isset($phone) && $phone->PHN_ACTIVE == 1 ? 'checked' :'' }} />
                                                                        <label for="active">Active </label>
                                                                    </div>
                                                                </div> --}}
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="com-sm-6 pull-right">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light"> {{ isset($phone) ? 'Update' : 'Save' }}</button>
                                                                    <button type="reset" class="btn btn-default waves-effect waves-light">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ route('customer.phone.delete') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="customer_id" id="customer_id">
                    <input type="hidden" name="type" id="phone_type">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            /* Remove First '0' on mobile phone */
            $('#phone-number').change(function() {
                var mobilePhone = $(this).val();
                while(true) {
                    if (mobilePhone.charAt(0) === '0' || mobilePhone.charAt(0) === '+') {
                        mobilePhone = mobilePhone.slice(1);
                        $(this).val(mobilePhone);
                    } else {
                        break;
                    }
                }
            });
            oTable = $('#d-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('customer/'.$customer->CUST_RECID.'/phone/data') }}',
                columns : [
                    { data : 'PHN_TYPE', name: 'PHN_TYPE'},
                    { data : 'PHN_NUMBER', name: 'PHN_NUMBER'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ]
            });

            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });

            $('.date').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
                $('#customer_id').val($(event.relatedTarget).data('customer-id'));
                $('#phone_type').val($(event.relatedTarget).data('type'));
            });

            $('#new').on('click', function() {
                $('#form-input').toggle();
            });
        })
    </script>
    <script src="{{ asset('js/customer.js') }}"></script>
@endsection
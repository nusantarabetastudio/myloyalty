<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Transaction Detail Receipt #{{ $receiptNo }} Products</h4>
</div>
<div class="modal-body">
    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="product-datatable">
        <thead>
            <tr>
                <th>Product</th>
                <th>Qty</th>
                <th>Point</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach($earnings as $earning)
                <tr>
                    <td>{{ $earning->article->PRO_DESCR }}</td>
                    <td>{{ $earning->PSD_QTY ? $earning->PSD_QTY : '-' }}</td>
                    <td>{{ $earning->PSD_POINT ? $earning->PSD_POINT : '-' }}</td>
                    <td>{{ $earning->PSD_AMOUNT ? $earning->PSD_AMOUNT : '-' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript">
        var oTable;
        $(function(){
            oTable = $('#product-datatable').DataTable({
                processing: true
            });
        });
</script>
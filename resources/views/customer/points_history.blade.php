@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">

@endsection

@section('content')

    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li role="presentation"><a href="{{ route('customer.detail', $customer->CUST_RECID) }}" >Customer Profile</a></li>
                            <li class="active" role="presentation"><a href="{{route('customer.detail.point-history', $customer->CUST_RECID)}}" aria-controls="exampleTabsTwo" role="tab">Customer Transaction</a></li>

                            <li  role="presentation"><a href="{{ route('customer.detail.logs', $customer->CUST_RECID) }}">Customer Logs</a></li>
                            <li role="presentation"><a href="{{ route('customer.detail.card-exchange', $customer->CUST_RECID) }}">Customer Card exchange</a></li>
                        </ul>
                        <div class="tab-content padding-top-20">
                            <div class="tab-pane active" id="exampleTabsFour" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="dataTables_length">
                                            <label>
                                                Show
                                                <select class="form-control limit">
                                                <option data-limit="10">10</option>
                                                <option data-limit="25">25</option>
                                                <option data-limit="50">50</option>
                                                <option data-limit="100">100</option>
                                                </select>
                                                entries
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="dataTables_filter">
                                            <label>
                                                Search:
                                                <input type="search" class="form-control search">
                                                <input type="hidden" class="idcustomer" value="{{ $customerId }}">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="sk-folding-cube loading-wrapper" style="display:none;">
                                    <div class="sk-cube1 sk-cube"></div>
                                    <div class="sk-cube2 sk-cube"></div>
                                    <div class="sk-cube4 sk-cube"></div>
                                    <div class="sk-cube3 sk-cube"></div>
                                </div>
                                <table class="tablesaw table-striped  dataTable table-bordered table-hover" id="point-history">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Store</th>
                                        <th>Amount</th>
                                        <th>RD</th>
                                        <th>Reff</th>
                                        <th>Doc No</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pointHistory as $data)
                                    	<tr class="item">
                                    		<td>{{ $data->date ?: '-'}}</td>
                                    		<td>{{ $data->time ? Carbon\Carbon::parse($data->time)->format('H:i:s') : '-'}}</td>
                                    		<td>{{ $data->store_code ?: '-'}}-{{$data->store_name ?: '-'}}</td>
                                    		<td>{{ $data->amount ?: '-'}}</td>
                                    		<td>{{ thousandSeparator($data->redeem_point) ?: '-'}}</td>
                                    		<td>{{ $data->receipt_no ?: '-'}}</td>
                                            <td>{{ $data->doc_no ?: '-'}}</td>
                                    		<td>{{ $data->type ?: '-'}}</td>
                                            <td>
                                                @if($data->type == 'Earning')
                                                <a href="{{ route('customer.detail.view-receipt', $data->id) }}" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#view-details">Detail</a>
                                                @endif
                                            </td>
                                    	</tr>
                                    @endforeach()
                                        <tr class="iTemplate" id="item-template">
                                            <td class="item-date"></td>
                                            <td class="item-time"></td>
                                            <td class="item-store-code"></td>
                                            <td class="item-amount"></td>
                                            <td class="item-redeem-Point"></td>
                                            <td class="item-receipt-no"></td>
                                            <td class="item-doc-no"></td>
                                            <td class="item-type"></td>
                                            <td class="item-action"><a href="" type="button" class="btn btn-success btn-xs item-detail" data-toggle="modal" data-target="#view-details">Detail</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                    <div class="row mart20">
                                        <div class="col-sm-12 text-right pagination-wrapper">
                                            <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginator->previous_page }}">
                                                Previous
                                            </button>
                                            Page
                                            <span class="current-page" data-page="{{ $paginator->current_page }}">
                                                {{ $paginator->current_page }}
                                            </span>
                                             /
                                            <span class="total-page">{{ $paginator->total_pages }}</span>
                                            <button class="btn btn-primary next-btn pagination-btn{{ (!$paginator->next_page) ? ' disabled' : '' }}" data-page="{{ $paginator->next_page }}">
                                                Next
                                            </button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
    <div class="modal fade" id="view-details" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

            </div>
        </div>
    </div>
    <div class="modal fade" id="view-product-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

            </div>
        </div>
    </div>
@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.pagination-btn').click(function() {
                $('.current-page').data('page', $(this).data('page'));
                getData();
            });
            $('.limit').change(function() {
                getData(true);
            });
            $('.search').keyup(function(e) {
                if(e.keyCode == 13) {
                getData(true);
                }
            });
            function getData(resetPage = false){
                var limit = $('.limit').find('option:selected').data('limit');
                var page = $('.current-page').data('page');
                var search = $('.search').val();
                var idcustomer = $('.idcustomer').val();
                var value = 'points';

                if (resetPage) page = 1;
                var dataUrl = '{{ route('customer.customerTransaction') }}?limit=' + limit + '&page=' + page + '&s=' + search + '&idcustomer=' + idcustomer + '&value='+value;

                if (dataUrl) {
                    $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: dataUrl,
                        success: function(data) {
                            var template = $('#item-template');
                            var items = data.DATA;
                            $('.item').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
                                $('.item-date', clone).text(item.date ? item.date : '-');
                                $('.item-time', clone).text(item.time ? item.time : '-');
                                $('.item-store-code', clone).text(item.store_code ? item.store_code : '-' +'-'+ item.store_name ? item.store_name : '-');
                                $('.item-amount', clone).text(item.amount ? item.amount : '-');
                                $('.item-redeem-Point', clone).text(item.redeem_point ? item.redeem_point : '-');
                                $('.item-doc-no', clone).text(item.doc_no ? item.doc_no : '-');
                                $('.item-receipt-no', clone).text(item.receipt_no ? item.receipt_no : '-');
                                $('.item-type', clone).text(item.type ? item.type : '-');

                                if(item.type == 'Earning'){
                                    $('.item-detail', clone).attr('href', '{{ url('customer/transaction') }}/' + item.id + '/receipt');
                                } else {
                                    $('.item-detail', clone).remove();
                                }
                                template.before(clone);
                            }
                            $('.current-page').text(data.paginator.current_page).data('page', data.paginator.current_page);
                            $('.previous-btn').data("page", data.paginator.previous_page );
                            $('.next-btn').data("page", data.paginator.next_page);
                            $('.total-page').text(data.paginator.total_pages);
                            $('.pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
            $('#view-details').on('hidden.bs.modal', function (e) {
                $(this).data('bs.modal', null);
                $(this).find('.modal-content').html("");
            });
            $('#view-product-modal').on('hidden.bs.modal', function (e) {
                $(this).data('bs.modal', null);
                $(this).find('.modal-content').html("");
            });
        });
    </script>
@endsection
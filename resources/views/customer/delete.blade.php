@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if(Session::has('notif_error'))
                <div class="alert alert-danger">
                    {{ Session::get('notif_error') }}
                </div>
            @endif
            <h3>Delete Customer</h3>
            @if($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form class="form-horizontal" id="form" action="{{ route('customer.process-mass-delete') }}" method="POST" enctype="multipart/form-data">
                 {!! csrf_field() !!}
                <div class="form-group form-material-sm">
                    <div class="col-sm-12">
                        <label>Delete Customer CSV file</label>
                        <br />
                        <input type="file" name="csv_file">
                    </div>
                </div>
                <div class="form-group form-material-sm">
                    <div class="col-sm-12">
                        <button type="button" id="cancel-btn" class="btn btn-danger">Cancel</button>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
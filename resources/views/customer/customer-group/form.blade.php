@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
@endsection
@section('content')
    @if(Session::has('notif_success'))
        <div class="alert alert-danger">
            {{ Session::get('notif_success') }}
        </div>
    @endif
    @if(count($errors) >0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method="POST" action="{{route('customer.customer-group.store')}}" class="form-horizontal">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">Customer Group Name : </label>
                                        <input type="text" class="form-control"  name="form_Name"/>
                                    </div>
                                </div>
                            </div>                         
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group ">
                                    <div class="col-sm-12">
                                        <label class="control-label">List Filter : </label>
                                        <select name="setting[0][type]" class="form-control setting">
                                            <option value="">Choose One</option>
                                            <option value="CUST_CODE" data-type="int">Code</option>
                                            <option value="CUST_DOB" data-type="date">DOB</option>
                                            <option value="CUST_JOINDATE" data-type="date">Join Date</option>
                                            <option value="CUST_EXPIREDATE" data-type="date">Expired Date</option>
                                            <option value="CUST_EXPENSE_EARN" data-type="int">Expense Earning</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 customer-condition iTemplate">
                                <div class="form-group ">
                                    <div class="col-sm-12 col-condition">
                                        <div class="col-sm-6">
                                            <select name="setting[0][operator]" class="form-control">
                                                <option value="="> Equals (=)</option>
                                                <option value="!=">Does not equal (!=)</option>
                                                <option value=">">Greater than (>)</option>
                                                <option value=">="> Greater than or equal to (>=)</option>
                                                <option value="<"> Less than (<)</option>
                                                <option value="<="> Less than or equal to (<=)</option>
                                                <option value="between"> Between </option>
                                                <option value="notbetween"> Not Between </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-sm" name="setting[0][values]" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 customer-condition-date iTemplate">
                                <div class="form-group ">
                                    <div class="col-sm-12 col-condition">
                                        <div class="col-sm-12">
                                            <div class="input-daterange">
                                                <div class="input-group">
                                                  <span class="input-group-addon">
                                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                                  </span>
                                                  <input type="text" class="form-control"  name="setting[0][valueDate]" value="" />
                                                </div>
                                                <div class="input-group">
                                                  <span class="input-group-addon">to</span>
                                                  <input type="text" class="form-control" name="setting[0][limitValue]" value="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 col-condition add-new-customer-group iTemplate">
                                <li class="btn btn-primary add-new-customer-group-btn">Add</li>
                            </div>
                        </div>
                        <div class="row iTemplate" id="customer-group-template">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select name="" class="form-control setting" required>
                                            <option value="0">Choose One</option>
                                            <option value="CUST_CODE" data-type="int">Code</option>
                                            <option value="CUST_DOB" data-type="date">DOB</option>
                                            <option value="CUST_JOINDATE" data-type="date">Join Date</option>
                                            <option value="CUST_EXPIREDATE" data-type="date">Expired Date</option>
                                            <option value="CUST_EXPENSE_EARN" data-type="date">Expense Earning</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 condition iTemplate">
                                <div class="form-group ">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <select name="" class="form-control operator">
                                                <option value="0">Choose One</option>
                                                <option value="="> Equals (=)</option>
                                                <option value="!=">Does not equal (!=)</option>
                                                <option value=">">Greater than (>)</option>
                                                <option value=">="> Greater than or equal to (>=)</option>
                                                <option value="<"> Less than (<)</option>
                                                <option value="<="> Less than or equal to (<=)</option>
                                                <option value="between"> Between </option>
                                                <option value="notbetween"> Not Between </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-sm values" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 condition-date iTemplate">
                                <div class="form-group ">
                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <div class="input-daterange">
                                                <div class="input-group">
                                                  <span class="input-group-addon">
                                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                                  </span>
                                                  <input type="text" class="form-control valueDate"  name="" value="" />
                                                </div>
                                                <div class="input-group">
                                                  <span class="input-group-addon">to</span>
                                                  <input type="text" class="form-control limit-value" name="" value="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 customer-group-delete"><a>Delete</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                @include('customer.customer-group.partials.tab-customer-group')
            </div>
            <button type="submit" class="btn btn-success mart20">Submit</button>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
    $('.input-daterange').datepicker();
    //select store
    $('.select-all-store-btn').click(function() {
        $('.store-switch').prop('checked', false);
        $('.store-switch').click();
    });
    $('.unselect-all-store-btn').click(function() {
        $('.store-switch').prop('checked', true);
        $('.store-switch').click();
    });
    // select type
    $('.select-all-type-btn').click(function() {
        $('.type-switch').prop('checked', false);
        $('.type-switch').click();
    });
    $('.unselect-all-type-btn').click(function() {
        $('.type-switch').prop('checked', true);
        $('.type-switch').click();
    });
    // select gender
    $('.select-all-gender-btn').click(function() {
        $('.gender-switch').prop('checked', false);
        $('.gender-switch').click();
    });
    $('.unselect-all-gender-btn').click(function() {
        $('.gender-switch').prop('checked', true);
        $('.gender-switch').click();
    });
    // select marital
    $('.select-all-marital-btn').click(function() {
        $('.marital-switch').prop('checked', false);
        $('.marital-switch').click();
    });
    $('.unselect-all-marital-btn').click(function() {
        $('.marital-switch').prop('checked', true);
        $('.marital-switch').click();
    });
    // select nationality
    $('.select-all-nationality-btn').click(function() {
        $('.nationality-switch').prop('checked', false);
        $('.nationality-switch').click();
    });
    $('.unselect-all-nationality-btn').click(function() {
        $('.nationality-switch').prop('checked', true);
        $('.nationality-switch').click();
    });
    // select religion
    $('.select-all-religion-btn').click(function() {
        $('.religion-switch').prop('checked', false);
        $('.religion-switch').click();
    });
    $('.unselect-all-religion-btn').click(function() {
        $('.religion-switch').prop('checked', true);
        $('.religion-switch').click();
    });
    // select blod
    $('.select-all-blod-btn').click(function() {
        $('.blod-switch').prop('checked', false);
        $('.blod-switch').click();
    });
    $('.unselect-all-blod-btn').click(function() {
        $('.blod-switch').prop('checked', true);
        $('.blod-switch').click();
    });
    // select cardType
    $('.select-all-cardType-btn').click(function() {
        $('.cardType-switch').prop('checked', false);
        $('.cardType-switch').click();
    });
    $('.unselect-all-cardType-btn').click(function() {
        $('.cardType-switch').prop('checked', true);
        $('.cardType-switch').click();
    });
    </script>
    <script>
        $(document).ready(function() {
            $('.setting').change(function() {
                var list_filter_type = $('.setting option:selected').data('type');
                if (list_filter_type =='date') {
                    $('.customer-condition-date').removeClass('iTemplate');
                    $('.customer-condition').addClass('iTemplate');
                    $(".add-new-customer-group").removeClass('iTemplate');
                } else {
                    $(".customer-condition").removeClass('iTemplate');
                    $('.customer-condition-date').addClass('iTemplate');
                    $(".add-new-customer-group").removeClass('iTemplate');
                }
            });
            var countCustomerGroup = 1;
            $('.add-new-customer-group-btn').click(function(e) {
                var template = $('#customer-group-template');
                var clone = template.clone().removeAttr('id').removeClass('iTemplate');
                $('.setting', clone).attr('name', 'setting' + '[' + countCustomerGroup +'][type]');
                $('.operator', clone).attr('name', 'setting' + '[' + countCustomerGroup +'][operator]');
                $('.valueDate', clone).attr('name', 'setting' + '[' + countCustomerGroup +'][valueDate]');
                $('.values', clone).attr('name', 'setting' + '[' + countCustomerGroup +'][values]');
                $('.limit-value', clone).attr('name', 'setting' + '[' + countCustomerGroup +'][limitValue]');
                $('.customer-group-delete', clone).click(function() {
                    $(this).parent().remove();
                });
                $('.setting', clone).change(function() {
                    var list_filter_type = $('.setting option:selected', clone).data('type');
                    if (list_filter_type =='date') {
                        $('.condition-date', clone).removeClass('iTemplate');
                        $('.condition', clone).addClass('iTemplate');
                        $(".add-new-customer-group", clone).removeClass('iTemplate');
                    } else {
                        $(".condition", clone).removeClass('iTemplate');
                        $('.condition-date', clone).addClass('iTemplate');
                        $(".add-new-customer-group", clone).removeClass('iTemplate');
                    }
                });
                $('.input-daterange', clone).datepicker();
                template.before(clone);
                countCustomerGroup++;
            });
        });
    </script>
@endsection
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="nav-tabs-horizontal nav-tabs-inverse nav-tabs-animate">
                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a data-toggle="tab" href="#tabStore" aria-controls="tabStore" role="tab" aria-expanded="false">
                            Store
                            </a>
                        </li>
                        <li role="presentation">
                            <a data-toggle="tab" href="#tabType" aria-controls="tabtype" role="tab" aria-expanded="false">
                                Member Type
                            </a>
                        </li>
                        <li role="presentation">
                            <a data-toggle="tab" href="#tabGender" aria-controls="tabGender" role="tab" aria-expanded="false">
                                Gender
                            </a>
                        </li>
                        <li role="presentation">
                            <a data-toggle="tab" href="#tabMarital" aria-controls="tabMarital" role="tab" aria-expanded="false">
                                Marital
                            </a>
                        </li>
                        <li role="presentation">
                            <a data-toggle="tab" href="#tabNationality" aria-controls="tabNationality" role="tab" aria-expanded="false">
                                Nationality
                            </a>
                        </li>
                        <li role="presentation">
                            <a data-toggle="tab" href="#tabReligion" aria-controls="tabReligion" role="tab" aria-expanded="false">
                                Religion
                            </a>
                        </li>
                        <li role="presentation">
                            <a data-toggle="tab" href="#tabBlod" aria-controls="tabBlod" role="tab" aria-expanded="false">
                                Blood
                            </a>
                        </li>
                        <li role="presentation">
                            <a data-toggle="tab" href="#tabCardType" aria-controls="tabCardType" role="tab" aria-expanded="false">
                                Card Type
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content padding-top-20">
                        <div class="tab-pane active animation-fade" id="tabStore" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-store-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-store-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Store</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($stores as $store)
                                <tr>
                                    <td>{{$store->TNT_DESC}}</td>
                                    <td><input type="checkbox" class="store-switch" name="master[CUST_STORE][{{$i}}]" data-plugin="switchery" value="{{$store->TNT_RECID}}"></td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane animation-fade" id="tabType" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-type-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-type-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Member Type</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($memberTypes as $memberType)
                                <tr>
                                    <td>{{$memberType->LOK_DESCRIPTION}}</td>
                                    <td><input type="checkbox" class="type-switch" name="master[CUST_MEMTYPE][{{$i}}]" data-plugin="switchery" value="{{$memberType->LOK_RECID}}"></td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane animation-fade" id="tabGender" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-gender-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-gender-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Gender</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($genders as $gender)
                                <tr>
                                    <td>{{$gender->LOK_DESCRIPTION}}</td>
                                    <td><input type="checkbox" class="gender-switch" name="master[CUST_GENDER][{{$i}}]" data-plugin="switchery" value="{{$gender->LOK_RECID}}"></td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane animation-fade" id="tabMarital" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-marital-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-marital-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Marital</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($maritals as $marital)
                                <tr>
                                    <td>{{$marital->LOK_DESCRIPTION}}</td>
                                    <td><input type="checkbox" class="marital-switch" name="master[CUST_MARITAL][{{$i}}]" data-plugin="switchery" value="{{$marital->LOK_RECID}}"></td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane animation-fade" id="tabNationality" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-nationality-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-nationality-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Nationality</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($nationalities as $nationaliti)
                                <tr>
                                    <td>{{$nationaliti->LOK_DESCRIPTION}}</td>
                                    <td><input type="checkbox" name="master[CUST_NATIONALITY][{{$i}}]" class="nationality-switch" value="{{$nationaliti->LOK_RECID}}" data-plugin="switchery"></td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane animation-fade" id="tabReligion" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-religion-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-religion-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Nationality</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($religions as $religion)
                                <tr>
                                    <td>{{$religion->LOK_DESCRIPTION}}</td>
                                    <td><input type="checkbox" class="religion-switch" name="master[CUST_RELIGION][{{$i}}]" value="{{$religion->LOK_RECID}}" data-plugin="switchery"></td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane animation-fade" id="tabBlod" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-blod-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-blod-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Blood</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($bloods as $blood)
                                <tr>
                                    <td>{{$blood->LOK_DESCRIPTION}}</td>
                                    <td><input type="checkbox" name="master[CUST_BLOOD][{{$i}}]" class="blod-switch" data-plugin="switchery" value="{{$blood->LOK_RECID}}"></td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane animation-fade" id="tabCardType" role="tabpanel">
                            <div class="row mar0">
                                <button type="button" class="btn btn-primary select-all-cardType-btn pull-right marlr5">Select All</button>
                                <button type="button" class="btn btn-danger unselect-all-cardType-btn pull-right marlr5">Unselect All</button>
                            </div>
                            <br/>
                            <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
                            <thead>
                                <tr>
                                    <th>Nationality</th>
                                    <th>Actived</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach($cardTypes as $cardType)
                                <tr>
                                    <td>{{$cardType->CARD_DESC}}</td>
                                    <td><input type="checkbox" name="master[CUST_CARDTYPE][{{$i}}]" class="cardType-switch" data-plugin="switchery" value="{{$cardType->CARD_RECID}}"></td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
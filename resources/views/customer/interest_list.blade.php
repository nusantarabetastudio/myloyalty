@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">

@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/edit' }}" aria-controls="exampleTabsOne" role="tab">Customer Profile</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/address' }}" aria-controls="exampleTabsTwo">Address</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/phone' }}" aria-controls="exampleTabsTwo">Phone</a></li>
                            <li role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/email' }}" aria-controls="exampleTabsTwo">Email</a></li>
                            <li class="active" role="presentation"><a href="{{ url('customer/'. $customer->CUST_RECID) . '/interest' }}" aria-controls="exampleTabsThree">Interest</a></li>
                        </ul>
                        <div class="tab-content padding-top-10">
                            <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                                <div class="panel">
                                    <div class="panel-body">
                                        @if(Session::has('success'))
                                            <div class="alert alert-success">
                                                {{ Session::get('success') }}
                                            </div>
                                        @endif
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="row row-lg">
                                            @if( ! isset($interest))
                                            <a href="javascript:void(0)" id="new" class="btn btn-primary">New Interest</a>
                                            <br><br>
                                            <div class="col-lg-12">
                                                <table class="table table-hover dataTable table-striped width-full" id="d-tables">
                                                    <thead>
                                                    <tr>
                                                        <th>Interest</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                @endif
                                                <div class="panel" id="form-input" style="border:1px solid #000; display: {{ isset($interest) ? 'block' : 'none' }} ;">
                                                    <div class="panel-body">
                                                        <form method="POST" action="{{ isset($interest) ? route('customer.interest.update', $interest->INT_RECID) : route('customer.interest.store') }}" class="form-horizontal">
                                                            {{ csrf_field() }}
                                                            {{ isset($interest) ? method_field('PUT') : ''}}
                                                            <input type="hidden" name="CUST_RECID" value="{{ $customer->CUST_RECID }}">
                                                            <div class="form-group form-material">
                                                                <div class="col-md-4">
                                                                    <label class="col-sm-2 control-label text-left" for="type">Interest</label>
                                                                    <div class="col-sm-10">
                                                                        <select name="INT_CATCODE" class="form-control select2" id="type">
                                                                            <option value="">Choose One</option>
                                                                            @foreach($master_interests as $master_interest)
                                                                                @if(isset($interest)  && $master_interest->LOK_RECID == $interest->INT_CATCODE)
                                                                                    <option value="{{ $master_interest->LOK_RECID }}" selected>{{ $master_interest->LOK_DESCRIPTION }}</option>
                                                                                @else
                                                                                    <option value="{{ $master_interest->LOK_RECID }}">{{ $master_interest->LOK_DESCRIPTION }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="checkbox-custom checkbox-primary">
                                                                        <input type="checkbox" name="INT_ACTIVE" id="active" {{ isset($interest) && $interest->INT_ACTIVE == 1 ? 'checked' :'' }} />
                                                                        <label for="active">Active </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="com-sm-6 pull-right">
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($interest) ? 'Update' : 'Submit' }}</button>
                                                                    <button type="reset" class="btn btn-default waves-effect waves-light">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ route('customer.interest.delete') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="customer_id" id="customer_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#d-tables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('customer/'.$customer->CUST_RECID.'/interest/data') }}',
                columns : [
                    { data : 'INT_CATCODE', name: 'INT_CATCODE'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ]
            });

            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });

            $('#delete-modal').on('show.bs.modal', function(event){
                $('#id').val($(event.relatedTarget).data('id'));
                $('#name').text($(event.relatedTarget).data('name'));
                $('#customer_id').val($(event.relatedTarget).data('customer-id'));
            });

            $('#new').on('click', function() {
                $('#form-input').toggle();
            });
        })
    </script>
    <script src="{{ asset('js/customer.js') }}"></script>
@endsection
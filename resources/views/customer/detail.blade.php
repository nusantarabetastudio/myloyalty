@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">

@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li class="active" role="presentation"><a  data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab">Customer Profile</a></li>
                            <li role="presentation"><a href="{{ route('customer.detail.point-history', $customer->CUST_RECID) }}" aria-controls="exampleTabsTwo" role="tab">Customer Transaction</a></li>
                            <li role="presentation"><a href="{{ route('customer.detail.logs', $customer->CUST_RECID) }}">Customer Logs</a></li>
                            <li role="presentation"><a href="{{ route('customer.detail.card-exchange', $customer->CUST_RECID) }}">Customer Card exchange</a></li>
                        </ul>
                        <div class="tab-content padding-top-20">
                            <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                @if(isset($customer))
                                <div class="panel panel-detail form-horizontal">
                                    <div class="panel-body">
                                        <div class="form-group form-material-sm">
                                            <div class="col-sm-6">
                                                <label class="col-sm-5 control-label text-left">Total Expense:</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ thousandSeparator($customer->CUST_EXPENSE_EARN) }}">
                                                </div>
                                            </div>
                                            {{-- <div class="col-sm-4">
                                                <label class="col-sm-8 control-label text-left">Total Earn Lucky Draw:</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{{ $customer->points->where('code', '1')->first()->point }}">
                                                </div>
                                            </div> --}}
                                            <div class="col-sm-6">
                                                <label class="col-sm-5 control-label text-left">Point Balance:</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ $customer->points->where('code', '2')->first() ? thousandSeparator($customer->points->where('code', '2')->first()->point) : '0' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <form class="form-horizontal">
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-10">
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">Barcode No :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="CUST_BARCODE" value="{{ isset($customer) ? $customer->CUST_BARCODE : '' }}" maxlength="10" pattern="[0-9]+" required disabled>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">Name :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="CUST_NAME" value="{{ isset($customer) ? $customer->CUST_NAME : '' }}" required disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">ID Type :</label>
                                                    <div class="col-sm-8">
                                                        <select name="CUST_IDTYPE" class="form-control input-sm select2" required disabled>
                                                            <option value="" disabled>Choose One</option>
                                                            @foreach($id_types as $id_type)
                                                                @if($id_type->LOK_RECID == $customer->CUST_IDTYPE)
                                                                    <option value="{{ $id_type->LOK_RECID }}" selected>{{ $id_type->LOK_DESCRIPTION }}</option>
                                                                @else
                                                                    <option value="{{ $id_type->LOK_RECID }}">{{ $id_type->LOK_DESCRIPTION }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">ID Number :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="CUST_IDCARDS" value="{{ isset($customer) ? $customer->CUST_IDCARDS : '' }}" required pattern="[a-zA-Z0-9]+" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">Gender :</label>
                                                    <div class="col-sm-8">
                                                        <select name="CUST_GENDER" class="form-control input-sm col-sm-8 select2" required disabled>
                                                            <option value="">Choose One</option>
                                                            @foreach($genders as $gender)
                                                                @if(isset($customer) && $customer->CUST_GENDER == $gender->LOK_RECID)
                                                                    <option value="{{ $gender->LOK_RECID }}" selected>{{ $gender->LOK_DESCRIPTION }}</option>
                                                                @else
                                                                    <option value="{{ $gender->LOK_RECID }}">{{ $gender->LOK_DESCRIPTION }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">Marital Status :</label>
                                                    <div class="col-sm-8">
                                                        <select name="CUST_MARITAL" class="form-control input-sm select2" id="marital" required disabled>
                                                            <option value="">Choose One</option>
                                                            @foreach($marital_statuses as $marital)
                                                                @if($marital->LOK_RECID == $customer->CUST_MARITAL)
                                                                    <option value="{{ $marital->LOK_RECID }}" selected>{{ $marital->LOK_DESCRIPTION }}</option>
                                                                @else
                                                                    <option value="{{ $marital->LOK_RECID }}">{{ $marital->LOK_DESCRIPTION }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">Date of Birth :</label>
                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                </span>
                                                            <input type="text" class="form-control input-sm date" id="date_of_birth" name="CUST_DOB" value="{{ isset($customer->CUST_DOB) ? $customer->CUST_DOB->format('d/m/Y') : '' }}" required disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-sm-4 control-label text-left">Place of Birth :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="CUST_POD" value="{{ isset($customer) ? $customer->CUST_POD : '' }}" required disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="{{ url(($customer->CUST_PROFILEPICTURE) ? $customer->CUST_PROFILEPICTURE : 'assets/global/portraits/5.jpg') }}" alt="..." style="width: 100%;margin-bottom: 10px;">
                                            <select name="CUST_ACTIVE" class="form-control input-sm" disabled="disabled">
                                                <option value="0" {{ isset($customer) && $customer->CUST_ACTIVE == 0 ? 'selected' : '' }}>Inactive</option>
                                                <option value="1" {{ (isset($customer) && $customer->CUST_ACTIVE == 1) || !isset($customer) ? 'selected' : '' }}>Active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-12">
                                            <label class="col-sm-2 control-label text-left">Address :</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control input-sm" name="ADDR_ADDRESS" value="{{ isset($customer) && !empty($customer->addressKtp->first()) ? $customer->addressKtp->first()->ADDR_ADDRESS : '' }}" required disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-12">
                                            <label class="col-sm-2 control-label text-left">Registered at:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control input-sm" value="{{ isset($customer) && !empty($customer->store) ? $customer->store->TNT_DESC : '' }}" required disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Country :</label>
                                            <div class="col-sm-7">
                                                <select name="ADDR_COUNTRY" class="form-control input-sm select2" id="country" required disabled>
                                                    <option value=""></option>
                                                    @foreach($countries as $country)
                                                        @if(!empty($customer->addressKtp->first()) && $country->LOK_RECID == $customer->addressKtp->first()->ADDR_COUNTRY)
                                                            <option value="{{ $country->LOK_RECID }}" selected>{{ $country->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $country->LOK_RECID }}">{{ $country->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Province :</label>
                                            <div class="col-sm-7">
                                                <select name="ADDR_PROVINCE" class="form-control input-sm select2" required disabled>
                                                    <option value="">Choose One</option>
                                                    @foreach($provinces as $province)
                                                        @if(!empty($customer->addressKtp->first()) && $province->LOK_RECID == $customer->addressKtp->first()->ADDR_PROVINCE)
                                                            <option value="{{ $province->LOK_RECID }}" selected>{{ $province->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $province->LOK_RECID }}">{{ $province->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">City :</label>
                                            <div class="col-sm-7">
                                                <select name="ADDR_CITY" class="form-control input-sm select2" required disabled>
                                                    <option value="">Choose One</option>
                                                    @foreach($cities as $city)
                                                        @if(!empty($customer->addressKtp->first()) && $city->LOK_RECID == $customer->addressKtp->first()->ADDR_CITY)
                                                            <option value="{{ $city->LOK_RECID }}" selected>{{ $city->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $city->LOK_RECID }}">{{ $city->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Area :</label>
                                            <div class="col-sm-7">
                                                <select name="ADDR_AREA" class="form-control input-sm select2" id="area" required disabled>
                                                    <option value="">Choose One</option>
                                                    @foreach($areas as $area)
                                                        @if(!empty($customer->addressKtp->first()) && $area->LOK_RECID == $customer->addressKtp->first()->ADDR_AREA)
                                                            <option value="{{ $area->LOK_RECID }}" selected>{{ $area->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $area->LOK_RECID }}">{{ $area->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">ZIP Code :</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="ADDR_POSTAL" value="{{ isset($customer) && !empty($customer->addressKtp->first()) ? $customer->addresses->first()->ADDR_POSTAL : '' }}" required disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Mobile Phone :</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="PHN_MOBILE" value="{{ $customer->mobilePhone()->get()->first() ? $customer->mobilePhone()->get()->first()->PHN_NUMBER  : '-'}}" required disabled>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Home Phone :</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="PHN_HOME" value="{{ $customer->homePhone !== null ? $customer->homePhone()->get()->first()->PHN_NUMBER : '' }}" required disabled>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Office Phone :</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="PHN_OFFICE" value="{{ $customer->companyPhone !== null ? $customer->companyPhone()->get()->first()->PHN_NUMBER : '' }}" required disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Nationality :</label>
                                            <div class="col-sm-7">
                                                <select name="CUST_NATIONALITY" class="form-control input-sm select2" required disabled>
                                                    <option value=""></option>
                                                    @foreach($nationalities as $nationality)
                                                        @if($nationality->LOK_RECID == $customer->CUST_NATIONALITY)
                                                            <option value="{{ $nationality->LOK_RECID }}" selected>{{ $nationality->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $nationality->LOK_RECID }}">{{ $nationality->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Email :</label>
                                            <div class="col-sm-7">
                                                <input type="email" class="form-control" name="EML_EMAIL" value="{{ $customer->emails->first() ? $customer->emails->first()->EML_EMAIL : '-' }}" required disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-material-sm">
                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Blood :</label>
                                            <div class="col-sm-7">
                                                <select name="CUST_BLOOD" class="form-control input-sm select2" required disabled>
                                                    <option value=""></option>
                                                    @foreach($bloods as $blood)
                                                        @if($blood->LOK_RECID == $customer->CUST_BLOOD)
                                                            <option value="{{ $blood->LOK_RECID }}" selected>{{ $blood->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $blood->LOK_RECID }}">{{ $blood->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Religion :</label>
                                            <div class="col-sm-7">
                                                <select name="CUST_RELIGION" class="form-control input-sm select2" required disabled>
                                                    <option value=""></option>
                                                    @foreach($religions as $religion)
                                                        @if($religion->LOK_RECID == $customer->CUST_RELIGION)
                                                            <option value="{{ $religion->LOK_RECID }}" selected>{{ $religion->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $religion->LOK_RECID }}">{{ $religion->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="col-sm-5 control-label text-left">Occupation :</label>
                                            <div class="col-sm-7">
                                                <select name="CUST_OCCUPATION" class="form-control input-sm select2" required disabled>
                                                    <option value=""></option>
                                                    @foreach($occupations as $occupation)
                                                        @if($occupation->LOK_RECID == $customer->CUST_OCCUPATION)
                                                            <option value="{{ $occupation->LOK_RECID }}" selected>{{ $occupation->LOK_DESCRIPTION }}</option>
                                                        @else
                                                            <option value="{{ $occupation->LOK_RECID }}">{{ $occupation->LOK_DESCRIPTION }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    {{-- <div class="panel" style="border:1px solid #000;">
                                        <div class="panel-body">
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Store :</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" name="" value="{{ isset($customer) ? $customer->CUST_STORE : '' }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Member Type :</label>
                                                    <div class="col-sm-7">
                                                        <select name="CUST_MEMTYPE" class="form-control input-sm select2" required disabled>
                                                            <option value=""></option>
                                                            @foreach($mem_types as $mem_type)
                                                                @if($mem_type->LOK_RECID == $customer->CUST_MEMTYPE)
                                                                    <option value="{{ $mem_type->LOK_RECID }}" selected>{{ $mem_type->LOK_DESCRIPTION }}</option>
                                                                @else
                                                                    <option value="{{ $mem_type->LOK_RECID }}">{{ $mem_type->LOK_DESCRIPTION }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Card Type :</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control input-sm select2" disabled>
                                                            @foreach($card_types as $card_type)
                                                                @if($card_type->LOK_RECID == $customer->CUST_CARDTYPE)
                                                                    <option value="{{ $card_type->LOK_RECID }}" selected>{{ $card_type->LOK_DESCRIPTION }}</option>
                                                                @else
                                                                    <option value="{{ $card_type->LOK_RECID }}">{{ $card_type->LOK_DESCRIPTION }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-5">
                                                    <label class="col-sm-4 control-label text-left">MGM :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="CUST_REFERER" value="{{ isset($customer) ? $customer->CUST_REFERER : '' }}" required disabled>
                                                    </div>
                                                </div>

                                                <div class="col-sm-7">
                                                    <label class="col-sm-4 control-label text-left">Referer Name :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="" value="" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-5">
                                                    <label class="col-sm-4 control-label text-left">Family Group :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="" value="{{ isset($customer) ? '' : '' }}" disabled>
                                                    </div>
                                                </div>

                                                <div class="col-sm-7">
                                                    <label class="col-sm-4 control-label text-left">Parent Name :</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-sm" name="CUST_PARENT" value="{{ isset($customer) ? $customer->CUST_PARENT : '' }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Join Date :</label>
                                                    <div class="col-sm-7">
                                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon md-calendar" aria-hidden="true"></i>
                                                </span>
                                                            <input type="text" class="form-control input-sm" id="" value="{{ $customer->CUST_JOINDATE->format('d/m/Y') }}" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Expired Date :</label>
                                                    <div class="col-sm-7">
                                                        <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon md-calendar" aria-hidden="true"></i>
                                                </span>
                                                            <input type="text" class="form-control input-sm" id="expired_date" value="{{ $customer->CUST_EXPIREDATE->format('d/m/Y') }}" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-material-sm">
                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Total Expenses:</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" value="{{ $customer->CUST_EXPENSE_EARN }}" disabled>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Total Earn Lucky Draw :</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" value="{{ isset($customer) ? $customer->points->where('code', '1')->first()->point : '' }}" disabled>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <label class="col-sm-5 control-label text-left">Total Earn Redeem :</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control input-sm" value="{{ isset($customer) ? $customer->points->where('code', '2')->first()->point : '' }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </form>
                            </div>
                            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                            </div>
                            <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                            </div>
                            <div class="tab-pane" id="exampleTabsFour" role="tabpanel">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>

    <script type="text/javascript">
        var url = '{{ url('/') }}';

        $(function () {
            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true
            });

            $('.date').datepicker({
                format : 'dd/mm/yyyy',
                orientation: "bottom left"
            });

            $('#country').on('change', function(e) {
                e.preventDefault();
                var area = $('#area').val();
                var country = $('#country').val();

                if(area == country) {
                    $('.area').remove();
                }
            });

            $('#area').on('change', function(e) {
                e.preventDefault();
                var area = $('#area').val();
                var country = $('#country').val();

                if(area == country) {
                    $('.area').remove();
                }
            });
        })
    </script>
    <script src="{{ asset('js/customer.js') }}"></script>
@endsection
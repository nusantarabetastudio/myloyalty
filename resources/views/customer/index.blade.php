@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if(Session::has('notif_error'))
                <div class="alert alert-danger">
                    {{ Session::get('notif_error') }}
                </div>
            @endif
            <br>
            <form method="GET" action="{{ route('customer.search') }}">
                <div class="row row-lg">
                    <div class="col-sm-6">
                        <a href="{{ route('customer.create') }}" class="btn btn-primary"><i class="icon md-plus" aria-hidden="true"></i> Add Customer</a>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="col-xs-2">
                         Search by:
                        </div>
                         <div class="col-xs-4">
                             <select class="form-control form-group" name="searchby">


                                 <option
                                     @php
                                        if($searchby=='phone') echo 'selected';
                                     @endphp
                                     value="phone"> Phone Number</option>
                                 <option
                                     @php
                                        if($searchby=='name') echo 'selected';
                                     @endphp
                                     value="name"> Cust Name</option>
                                 <option
                                     @php
                                        if($searchby=='barcode') echo 'selected';
                                     @endphp
                                     value="barcode"> Cust Barcode</option>
                                 <option
                                     @php
                                        if($searchby=='idcard') echo 'selected';
                                     @endphp
                                     value="idcard"> ID Cards</option>
                                 <option
                                     @php
                                        if($searchby=='all') echo 'selected';
                                     @endphp
                                     value="all"> All</option>
                             </select>
                        </div>
                        <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Type keywords..." name="s" value="{{ $search }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary buttonSubmit" type="submit">Search</button>
                            </span>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
            <br><br>
            <table class="table table-hover dataTable table-striped width-full" id="d-customer">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Mobile Number</th>
                        <th>Barcode Number</th>
                        <th>ID Number</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $customer)
                        <tr>
                            <td>{!! str_replace($search,"<span style='background-color: #eeff41'>$search</span>",$customer->CUST_NAME) !!}</td>
                            <td>{!! str_replace($search,"<span style='background-color: #eeff41'>$search</span>",$customer->PHN_NUMBER) !!}</td>
                            <td>{!! str_replace($search,"<span style='background-color: #eeff41'>$search</span>",$customer->CUST_BARCODE) !!}</td>
                            <td>{!! str_replace($search,"<span style='background-color: #eeff41'>$search</span>",$customer->CUST_IDCARDS) !!}</td>
                            @if ($isCs)
                                <td>
                                    <a href="{{ url("customer/". $customer->CUST_RECID ."/detail") }}"><button type="button" class="btn btn-success btn-xs">View</button></a>
                                </td>
                            @else
                                <td>
                                    <a href="{{ url('customer/'.$customer->CUST_RECID .'/edit') }}" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>
                                    <a href="{{ url("customer/". $customer->CUST_RECID ."/detail") }}"><button type="button" class="btn btn-success btn-xs">View</button></a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $customers->appends(request()->query())->links() !!}
        </div>
    </div>

@endsection

@section('scripts')

@endsection
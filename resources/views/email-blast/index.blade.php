@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/fonts/font-awesome/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/summernote/summernote.css') }}">
@endsection
@section('content')
<form class="form-horizontal" method="post" action="{{ route('blast.email.store')}}">
{{ csrf_field() }}
	<div clas="row row-lg">
		<div class="col-lg-12">
			<div class="nav-tabs-horizontal">
				<div class="tab-pane active" row="tabpanel">
					<div class="panel">
						@if (Session::has('notif_success'))
							<div class="alert alert-success">
								{{ Session::get('notif_success') }}
							</div>
						@endif
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-3 control-label text-left">Choose Customer Group</label>
								<div class="col-sm-7">
									<select name="cust-group" id="cust-group" class="form-control select2" required>
										<option value="" selected>Choose One</option>
										@foreach($customerGroups as $customerGroup)
										<option value="{{ $customerGroup->id }}" data-type="{{ $customerGroup->id }}">{{ $customerGroup->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-sm-2">
									<a class="btn btn-primary view-cust">View Customer</a>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<h2>Customer List</h2>
						            <div class="row">
						                <div class="col-lg-6">
						                    <div class="dataTables_length">
						                    <label>Show
						                        <select class="form-control limit">
						                            <option data-limit="10">10</option>
						                            <option data-limit="25">25</option>
						                            <option data-limit="50">50</option>
						                            <option data-limit="100">100</option>
						                        </select>
						                        entries</label>
						                    </div>
						                </div>
						                <div class="col-lg-6">
						                    <div class="dataTables_filter">
						                        <label>
						                            Search:
						                            <input type="search" class="form-control search-product">
						                        </label>
						                    </div>
						                </div>
						            </div>
								    <div class="sk-folding-cube loading-wrapper-log" style="display:none;">
								        <div class="sk-cube1 sk-cube"></div>
								        <div class="sk-cube2 sk-cube"></div>
								        <div class="sk-cube4 sk-cube"></div>
								        <div class="sk-cube3 sk-cube"></div>
								    </div>
									<table class="table table-hover dataTable table-striped width-full">
	                                    <thead>
		                                    <tr>
	                                            <th>Name</th>
	                                            <th>Email</th>
	                                            <th>Mobile Number</th>
	                                            <th>Barcode Number</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody id="emailblast-container">
	                                    	<tr id="emailblast-no-found" class="item">
	                                        	<td colspan="5" id="dataTables_empty">No matching records found</td>
	                                        </tr>
	                                    	<tr id="emailblast-template" class="iTemplate">
	                                    		<td class="coloumn-name"></td>
	                                    		<td class="coloumn-email"></td>
	                                    		<td class="coloumn-mobile-number"></td>
	                                    		<td class="coloumn-burcode-number"></td>
	                                    	</tr>
	                                    </tbody>
                                    </table>
						            <div class="row mart20">
						                <div class="col-sm-12 text-right pagination-wrapper">
						                    <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="">
						                        Previous
						                    </button>
						                    Page 
						                    <span class="current-page" data-page="">
						                        
						                    </span>
						                     / 
						                    <span class="total-page"></span>
						                    <button class="btn btn-primary next-btn pagination-btn" data-page="">
						                        Next
						                    </button>
						                </div>
						            </div>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="panel panel-detail">
								<div class="panel-body">
									<h2>Email Template</h2>
									<div class="form-group">
										<div class="col-md-12">
											<label class="control-label">Subject Email:</label>
											<input type="text"  class="form-control" name="subject_email" required placeholder="Subject Email">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label class="control-label">Email Body:</label>
											<textarea  data-plugin="summernote" name="email_body"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<label class="control-label">Plain Text Email (Optional):</label>
											<textarea class="form-control" rows="5" name="plaint_text_email"  placeholder="Plain Text Email"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<button class="btn btn-primary">SEND</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
	<script src="{{ asset('assets/global/vendor/animsition/animsition.js') }}"></script>
	<script src="{{ asset('assets/global/vendor/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('assets/global/js/components/summernote.js') }}"></script>
	<script src="{{ asset('assets/examples/js/forms/editor-summernote.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('.pagination-btn').click(function() {
	        $('.current-page').data('page', $(this).data('page'));
	        getData();
	    });
	    $('.limit').change(function() {
	        getData(true);
	    });
        $('.search-product').keyup(function(e) {
            if(e.keyCode == 13) {
                getData(true);
            }
        });
        $('.view-cust').click(function(){
        	getData(true);
        });
        function getData(resetPage = false) {
        	var custgroup = $('#cust-group option:selected').data('type');
	        var limit = $('.limit').find('option:selected').data('limit');
	        var page = $('.current-page').data('page');
	        if (resetPage) page = 1;
	        var search = $('.search-product').val();
	        var pageUrl = '{{ url('customer/customer-group') }}/' + custgroup + '/customers?limit=' + limit + '&page=' + page +'&s='+ search;
	        if (pageUrl) {
	        	$('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                $.ajax({
                    url: pageUrl,
                    success: function(data) {
                        $('.loading-wrapper').hide().closest('table').css('opacity', 1);
                        var template = $('#emailblast-template');
                        var items = data.DATA;
                        var clone;
                        $('.item').remove();
                        for ($i = 0; $i < items.length; $i++) {
                            var item = items[$i];
							clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
							$('.coloumn-name', clone).text(item.CUST_NAME);
							$('.coloumn-email', clone).text((item.email) ? item.email.EML_EMAIL : '-');
							$('.coloumn-mobile-number', clone).text((item.phone) ? item.phone.PHN_NUMBER : '-');
							$('.coloumn-burcode-number', clone).text(item.CUST_BARCODE);
                            template.before(clone);
                        }
                        $('.current-page').text(data.paginator.current_page);
                        $('.previous-btn').data("page", data.paginator.previous_page);
                        $('.next-btn').data("page", data.paginator.next_page);
                        $('.total-page').text(data.paginator.total_pages);

                        $('.pagination-btn').removeClass('disabled');
                        if (!data.paginator.previous_page)
                            $('.previous-btn').addClass('disabled');
                        if (!data.paginator.next_page)
                            $('.next-btn').addClass('disabled');
                    }
                }).complete(function() {
                    $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                });
	        }
        }
	});
	$(function () {
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
	})
</script>
@endsection
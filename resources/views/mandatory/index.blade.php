@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/examples/css/forms/masks.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">

@endsection
@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="nav-tabs-horizontal">
                <div class="tab-content padding-top-10">
                    <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
							@if( !isset($mandatory))
							<a href="javascript:void(0)" id="new-mandatory-general" class="btn btn-primary">New Mandatory General</a>
							<br><br>
							<div class="col-lg-12">
								<table class="table table-hover table-striped dataTable no-footer" id="datatable-table" >
									<thead>
										<tr>
											<th>Description</th>
											<th>Field Name</th>
											<th>Table</th>
											<th>Application</th>
											<th>Visible</th>
											<th>Required</th>
											<th>Readonly</th>
											<th>Unique</th>
											<th>Active</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							@endif
							@if (isset($mandatory))
								<a href="{{ route('mandatory.general.index') }}" class="btn btn-danger waves-effect waves-light">
									<i class="icon md-chevron-left" aria-hidden="true"></i> Back
								</a>
							@endif
								<div class="panel panel-detail mart20" id="form-input" style="display: {{ isset($mandatory) ? 'block' : 'none' }} ;">
									<div class="panel-body">
		                                @if(isset($mandatory))
		                                    <form class="form-horizontal" id="form" action="{{ route('mandatory.general.update', $mandatory->id) }}" method="POST">
		                                        {!! method_field('PUT') !!}
		                                @else
		                                    <form class="form-horizontal" id="form" action="{{ route('mandatory.general.store') }}" method="POST">
		                                @endif
											{{ csrf_field() }}
											<div class="form-group">
												<div class="col-md-4">
													<label class="control-label">Description</label>
													<input type="text" name="description" class="form-control" value="{{ (isset($mandatory->description)) ? $mandatory->description : '' }}">
												</div>
												<div class="col-md-4">
													<label class="control-label">Field Name</label>
													<select name="field_name" class="form-control select2" id="fieldName" required>
														<option value="">Choose One</option>
														@foreach ($columns as $column)
															<?php 
															$columnValue = explode('.', $column); 
															$columnValue = $columnValue[1];
															?>
															<option value="{{ $columnValue }}" {{ (isset($mandatory) && $mandatory->field_name == $columnValue) ? 'selected="selected"' : ''}}>{{ $column }}</option>
														@endforeach
													</select>
												</div>
												<div class="col-md-4">
													<label class="control-label">Table</label>
													<input type="text" name="table_name" id="table" class="form-control" readonly="readonly" value="{{ (isset($mandatory->table_name)) ? $mandatory->table_name : '' }}">
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-4">
													<label class="control-label">Role Name</label>
													<select name="role_name" class="form-control select2" id="roleName" required>
														<option value="">Choose One</option>
														@foreach ($roles as $role)
															<option value="{{ $role }}" {{ (isset($mandatory) && $mandatory->role_name == $role) ? 'selected="selected"' : ''}}>{{ $role }}</option>
														@endforeach
													</select>
												</div>
												<div class="col-md-4">
													<label class="control-label">Data Type</label>
													<select name="data_type_id" class="form-control select2" id="dataType" required>
														<option value="">Choose One</option>
														@foreach ($dataTypes as $dataTypeId => $dataTypeName)
															<option value="{{ $dataTypeId }}" {{ (isset($mandatory) && $mandatory->data_type_id == $dataTypeId) ? 'selected="selected"' : ''}}>{{ $dataTypeName }}</option>
														@endforeach
													</select>
												</div>
												<div class="col-md-4">
													<label class="control-label">Default</label>
													<input type="text" class="form-control" name="default" value="{{ (isset($mandatory->default_value)) ? $mandatory->default_value : '' }}">
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-4">
													<label class="control-label">Minimum</label>
													<input type="text" class="form-control" name="minimum" value="{{ (isset($mandatory->minimum)) ? $mandatory->minimum : '' }}">
												</div>
												<div class="col-md-4">
													<label class="control-label">Maximum</label>
													<input type="text" class="form-control" name="maximum" value="{{ (isset($mandatory->maximum)) ? $mandatory->maximum : '' }}">
												</div>
												<div class="col-md-4">
													<div class="checkbox-custom checkbox-primary">
														<input type="checkbox" name="is_visible" {{ isset($mandatory) && $mandatory->is_visible == 1 ? 'checked' :'' }} />
														<label for="active">Visible </label>
													</div>
												</div>
												<div class="col-md-4">
													<div class="checkbox-custom checkbox-primary">
														<input type="checkbox" name="is_required" {{ isset($mandatory) && $mandatory->is_required == 1 ? 'checked' :'' }} />
														<label for="active">Required </label>
													</div>
													<div class="checkbox-custom checkbox-primary">
														<input type="checkbox" name="is_read_only" {{ isset($mandatory) && $mandatory->is_read_only == 1 ? 'checked' :'' }} />
														<label for="active">Readonly </label>
													</div>
													<div class="checkbox-custom checkbox-primary">
														<input type="checkbox" name="is_unique" {{ isset($mandatory) && $mandatory->is_unique == 1 ? 'checked' :'' }} />
														<label for="Unique">Unique </label>
													</div>
													<div class="checkbox-custom checkbox-primary">
														<input type="checkbox" name="active" {{ (isset($mandatory) && $mandatory->active == 1) || !isset($mandatory) ? 'checked' : '' }} />
														<label for="active">Active </label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-12">
													<button type="submit" class="btn btn-primary waves-effect waves-light">{{ isset($interest) ? 'Update' : 'Submit' }}</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-sm" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="{{ url('mandatory/') }}" method="post" role="form">
                    {!! csrf_field() !!}
                    {!! method_field('DELETE') !!}
                    <input type="hidden" name="id" id="mandatory_modal_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Information !</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/formatter-js/jquery.formatter.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
	<script type="text/javascript">
		var oTable;
		$(function(){
			var start;
			oTable = $('#datatable-table').DataTable({
				processing: true,
				serverSide: true,
                ajax: {
                    method : 'POST',
                    url    : '{{ url('mandatory/general/data') }}',
                    headers: {
                        'X-CSRF-TOKEN' : '{{csrf_token()}}'
                    }
                },
				columns :[
				{ data: 'description', name: 'description'},
				{ data: 'field_name', name: 'field_name'},
				{ data: 'table_name', name: 'table_name'},
				{ data: 'role_name', name: 'role_name'},
				{ data: 'is_visible', name: 'is_visible', searchable: false, orderable: false},
				{ data: 'is_required', name: 'is_required', searchable: false, orderable: false},
				{ data: 'is_read_only', name: 'is_read_only', searchable: false, orderable: false},
				{ data: 'is_unique', name: 'is_unique', searchable: false, orderable: false},
				{ data: 'active', name: 'active', searchable: false, orderable: false},
				{data: 'action', name: 'action', searchable: false, orderable: false},
				],
			});
		});
		$('#fieldName').change(function() {
			var value = $('option:selected', this).text();
			value = value.split(".");
			value = value[0];

			$('#table').val(value);
		});
		$('#new-mandatory-general').on('click', function(e) {
			$('#form-input').toggle();
			jQuery("html, body").animate({
                scrollTop: $('#form-input').offset().top - 100 // 66 for sticky bar height
            }, "slow");
		});
        $('.select2').select2({
            'placeholder' : 'Choose One',
            'allowClear' : true,
            'width' : '100%'
        });
        $('#delete-modal').on('show.bs.modal', function(event){
            $('#mandatory_modal_id').val($(event.relatedTarget).data('id'));
        });
	</script>    
@endsection
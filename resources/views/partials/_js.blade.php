<!-- Core  -->
<script src="{{ asset('assets/global/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('assets/global/vendor/bootstrap/bootstrap.js') }}"></script>
<script src="{{ asset('assets/global/vendor/animsition/animsition.js') }}"></script>
<script src="{{ asset('assets/global/vendor/asscroll/jquery-asScroll.js') }}"></script>
<script src="{{ asset('assets/global/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('assets/global/vendor/asscrollable/jquery.asScrollable.all.js') }}"></script>
<script src="{{ asset('assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>
<script src="{{ asset('assets/global/vendor/waves/waves.js') }}"></script>
<!-- Plugins -->
<script src="{{ asset('assets/global/vendor/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/intro-js/intro.js') }}"></script>
<script src="{{ asset('assets/global/vendor/screenfull/screenfull.js') }}"></script>
<script src="{{ asset('assets/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
<script src="{{ asset('assets/global/vendor/chartist-js/chartist.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/jvectormap/jquery-jvectormap.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('assets/global/vendor/matchheight/jquery.matchHeight-min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
<script src="{{ asset('assets/global/vendor/isotope/isotope.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<!-- Scripts -->
<script src="{{ asset('assets/global/js/core.js') }}"></script>
<script src="{{ asset('assets/js/site.js') }}"></script>
<script src="{{ asset('assets/js/sections/menu.js') }}"></script>
<script src="{{ asset('assets/js/sections/menubar.js') }}"></script>
<script src="{{ asset('assets/js/sections/gridmenu.js') }}"></script>
<script src="{{ asset('assets/js/sections/sidebar.js') }}"></script>
<script src="{{ asset('assets/global/js/configs/config-colors.js') }}"></script>
<script src="{{ asset('assets/js/configs/config-tour.js') }}"></script>
<script src="{{ asset('assets/global/js/components/asscrollable.js') }}"></script>
<script src="{{ asset('assets/global/js/components/animsition.js') }}"></script>
<script src="{{ asset('assets/global/js/components/slidepanel.js') }}"></script>
<script src="{{ asset('assets/global/js/components/switchery.js') }}"></script>
<script src="{{ asset('assets/global/js/components/tabs.js') }}"></script>
<script src="{{ asset('assets/global/js/components/matchheight.js') }}"></script>
<script src="{{ asset('assets/global/js/components/jvectormap.js') }}"></script>
<script src="{{ asset('assets/global/js/components/peity.js') }}"></script>
<script src="{{ asset('assets/examples/js/pages/gallery.js') }}"></script>
 <script src="{{ asset('assets/global/vendor/filament-tablesaw/tablesaw.js') }}"></script>
<script src="{{ asset('assets/global/vendor/filament-tablesaw/tablesaw-init.js') }}"></script>
<!--<script src="{{ asset('assets/examples/js/dashboard/v1.js') }}"></script>-->
<script>
    $( ".alert" ).fadeOut( 10800, "linear");
</script>
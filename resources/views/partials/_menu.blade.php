<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="{{ asset('assets/images/LogoSBL.png') }}" title="MYLOYALTY">

        </div>
        <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
                data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>
    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                <li class="dropdown">
                    <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                       data-animation="scale-up" role="button">
                        @if(Session::has('user'))
                         <b>{!! Session::get('user')->USER_FIRST_NAME . ' ' . Session::get('user')->USER_LAST_NAME !!}</b> as <b>{{ Session::get('role_data')->ROLE_NAME }} :: {{Session::get('data')->STORE->name}} :: </b> &nbsp;&nbsp;
                        @else
                            Welcome <b>Guest</b>&nbsp;&nbsp;
                        @endif
			  <span class="avatar">
                <img src="{{ url('assets/global/portraits/5.jpg') }}" alt="...">
              </span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a href="{{ route('profile') }}" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
                        </li>
                        <li role="presentation">
                            <a href="{{ route('change-password') }}" role="menuitem"><i class="icon md-key" aria-hidden="true"></i> Change Password</a>
                        </li>
                        <li class="divider" role="presentation"></li>
                        <li role="presentation">
                            <a href="{{ url('/logout') }}" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->
    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu">
                    @if(Session::has('menu_data'))
                        @foreach(Session::get('menu_data') as $menu)
                            <li class="site-menu-item @if($menu->child_menu !== []) has-sub @elseif(Request::is($menu->MENU_ROUTES)) active @endif @if(Request::is($menu->MENU_ROUTES.'/*')) active open @endif">
                                <a href="{{ $menu->child_menu !== [] ? 'javascript:void(0)' : url($menu->MENU_ROUTES) }}">
                                    <i class="site-menu-icon {{ isset($menu->MENU_ICON) ? $menu->MENU_ICON : 'md-google-pages' }}" aria-hidden="true"></i>
                                    <span class="site-menu-title">{{ $menu->MENU_NAME }}</span>
                                    @if($menu->child_menu !== [])
                                        <span class="site-menu-arrow"></span>
                                    @endif
                                </a>
                                @if(isset($menu->child_menu) && $menu->child_menu !== [])
                                    <ul class="site-menu-sub">
                                        @foreach($menu->child_menu as $child)
                                            <li class="site-menu-item @if( $child->child_menu !== []) has-sub @endif @if(Request::is($child->MENU_ROUTES . '/*') || Request::is($child->MENU_ROUTES) ) active open @endif">
                                                <a class="animsition-link" href="{{ $child->child_menu !== [] ? 'javascript:void(0)' : url($child->MENU_ROUTES) }}">
                                                    <span class="site-menu-title">{{ $child->MENU_NAME }}</span>
                                                    @if($child->child_menu !== [])
                                                        <span class="site-menu-arrow"></span>
                                                    @endif
                                                </a>
                                                @if(isset($child->child_menu) && $child->child_menu !== [])
                                                    <ul class="site-menu-sub">
                                                    @foreach($child->child_menu as $grandChild)
                                                        <li class="site-menu-item @if(Request::is($grandChild->MENU_ROUTES)) active @endif">
                                                            <a href="{{ url($grandChild->MENU_ROUTES) }}">
                                                                <span class="site-menu-title">{{ $grandChild->MENU_NAME }}</span>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
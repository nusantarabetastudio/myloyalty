<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="">
<meta name="author" content="">
<title>MYLOYALTY</title>
<link rel="apple-touch-icon" href="{{ asset('assets/images/apple-touch-icon.png') }}">
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">
<!-- Stylesheets -->
<link rel="stylesheet" href="{{ asset('assets/global/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/css/bootstrap-extend.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/site.min.css') }}">
<!-- Custom Stylesheets -->
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<!-- Plugins -->
<link rel="stylesheet" href="{{ asset('assets/global/vendor/animsition/animsition.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/asscrollable/asScrollable.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/switchery/switchery.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/intro-js/introjs.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/slidepanel/slidePanel.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/flag-icon-css/flag-icon.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/waves/waves.css') }}">
<!--<link rel="stylesheet" href="assets/global/vendor/chartist-js/chartist.css">
<link rel="stylesheet" href="assets/global/vendor/jvectormap/jquery-jvectormap.css') }}">
<link rel="stylesheet" href="assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">-->

<link rel="stylesheet" href="{{ asset('assets/examples/css/pages/login-v3.css') }}">

<link rel="stylesheet" href="{{ asset('assets/global/vendor/magnific-popup/magnific-popup.css') }}"> <!-- POP UP GALLERY-->
<link rel="stylesheet" href="{{ asset('assets/global/vendor/filament-tablesaw/tablesaw.min.css?v2.2.0') }}"> <!-- POP UP GALLERY-->
<!--<link rel="stylesheet" href="assets/examples/css/dashboard/v1.css">-->

<!-- datatables -->
<link rel="stylesheet" href="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/examples/css/tables/datatable.css') }}">

<!-- Fonts -->
<link rel="stylesheet" href="{{ asset('assets/global/fonts/font-awesome/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/fonts/web-icons/web-icons.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/fonts/material-design/material-design.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/fonts/glyphicons/glyphicons.min.css') }}">
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
<!--[if lt IE 9]>
<script src="{{ asset('assets/global/vendor/html5shiv/html5shiv.min.js') }}"></script>
<![endif]-->
<!--[if lt IE 10]>
<script src="{{ asset('assets/global/vendor/media-match/media.match.min.js') }}"></script>
<script src="{{ asset('assets/global/vendor/respond/respond.min.js') }}"></script>
<![endif]-->
<!-- Scripts -->
<script src="{{ asset('assets/global/vendor/modernizr/modernizr.js') }}"></script>
<script src="{{ asset('assets/global/vendor/breakpoints/breakpoints.js') }}"></script>
<script>
    Breakpoints();
</script>
@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
@endsection
@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
            @endif
            @if(Session::has('error_msg'))
                <div class="alert alert-danger">
                    {{ Session::get('error_msg') }}
                </div>
            @endif
            @if(count($errors) > 0 )
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row row-lg">
                <div class="col-lg-12">
                    <form class="form-horizontal">
                    {{ csrf_field() }}
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Product Name</label>
                                <div class="col-sm-7">
                                    <input type="text" name="description" value="{{ $product->description or old('description') }}"  class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Category :</label>
                                <div class="col-sm-7">
                                    <select name="categoryId" class="form-control input-sm select2" id="categoryId" disabled>
                                        <option value="">Choose One</option>
                                        @foreach($categorys as $category)
                                            @if(isset($product) && $product->product_redeem_category_id == $category->LOK_RECID)
                                                <option value="{{ $category->LOK_RECID}}" selected>{{$category->LOK_DESCRIPTION}}</option>
                                            @else
                                                <option value="{{ $category->LOK_RECID}}" >{{$category->LOK_DESCRIPTION}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Applicable From :</label>
                                <div class="col-sm-7">
                                    <input type="text" name="fromDate" value="{{ $product->from_date or old('from_date') }}" class="form-control input-sm date" required disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">To :</label>
                                <div class="col-sm-7">
                                    <input type="text" name="toDate" value="{{ $product->to_date or old('to_date') }}" class="form-control input-sm date" required disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Quantity :</label>
                                <div class="col-sm-7">
                                    <input type="number" name="qty" value="{{ $product->qty or old('qty') }}" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Unit Measurement :</label>
                                <div class="col-sm-7">
                                    <select name="unitId" class="form-control input-sm select2" id="unitId" required disabled>
                                        <option value="">Choose One</option>
                                        @foreach($units as $unit)
                                            @if(isset($product) && $product->unit_id == $unit->LOK_RECID)
                                                <option value="{{ $unit->LOK_RECID}}" selected>{{$unit->LOK_DESCRIPTION }}</option>
                                            @else
                                                <option value="{{ $unit->LOK_RECID}}" >{{$unit->LOK_DESCRIPTION }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Prices :</label>
                                <div class="col-sm-7">
                                    <input type="number" name="price" value="{{ $product->price or old('price') }}" id="price" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Point  :</label>
                                <div class="col-sm-7">
                                    <input type="number" name="point" value="{{ $product->point or old('point') }}" id="point" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                               <label class="col-sm-5 control-label text-left">Tenant :</label>
                                <div class="col-sm-7">
                                    <select name="tenantId" class="form-control input-sm select2" id="tenantId" required disabled>
                                        <option value="">Choose One</option>
                                        @foreach($tenants as $tenant)
                                            @if(isset($product) && $product->tenant_id == $tenant->TNT_RECID)
                                                <option value="{{ $tenant->TNT_RECID}}" selected>{{$tenant->TNT_DESC}}</option>
                                            @else
                                                <option value="{{ $tenant->TNT_RECID}}" >{{$tenant->TNT_DESC}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-2 col-sm-offset-5">
                                    <input type="checkbox" disabled name="active"  value="1" {{ (isset($product) && $product->active == 1) || !isset($product) ? 'checked' : '' }} {{ isset($product) ? '' : '' }}> Active
                                </div>
                                <div class="col-sm-4">
                                    <input type="hidden" name="" id="voucher-checkbox" value="{{$product->is_voucher}}">
                                    <input type="checkbox" name="is_voucher"  {{ (isset($product) && $product->is_voucher == 1) || !isset($product) ? 'checked' : '' }} {{ isset($product) ? '' : '' }} disabled> Voucher
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label text-left">Information :</label>
                                <div class="col-sm-10">
                                    <input type="hidden" id="id-iventory" value="{{ $idIventory }}">
                                    <input type="text" name="comment" value="{{ $product->comment or old('comment') }}" id="comment" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="col-sm-7">
                                    <div class="row">
                                        <input type="file" id="input-file-max-fs" name="image1" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $product->first_image_url or ''}}" disabled />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="col-sm-7">
                                    <div class="row">
                                        <input type="file" id="input-file-max-fs" name="image2" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $product->second_image_url or '' }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="panel bg-blue-600">
        <div class="panel-body" id="voucher-no-container" data-show="false">
            <div class="col-md-12">
                <form id="form" method="POST" action="{{ route('redeem.inv.product-redeem.store') }}" class="form-horizontal">
                {{ csrf_field() }}
                    <div class="form-group form-material-sm">
                        <div class="col-sm-6">
                            <label for="" class="control-label col-sm-3 text-left">Inventory QTY</label>
                            <div class="col-sm-9">
                                <input type="number" name="inventory_qty" class="form-control" id="inventory-qty">
                                <input type="hidden" name="id" class="form-control" value="{{$product->id}}">
                                <input type="hidden" name="is_voucher" class="form-control" value="{{$product->is_voucher}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-material-sm iTemplate" id="multiple-serial-no">
                        <div class="col-sm-6{{ $product->is_voucher == 0 ? ' iTemplate' : '' }}">
                            <label for="" class="col-sm-3 control-label text-left">Serial No From</label>
                            <div class="col-sm-9">
                                <input type="text" name="serial_no_from" id="serial_no_from" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6 iTemplate serial-no-to-wrapper">
                            <label for="" class="col-sm-3 control-label text-left">Serial No To</label>
                            <div class="col-sm-9">
                                <input type="text" name="serial_no_to" id="serial_no_to" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5">
                            <a class="btn btn-default" id="submit-btn">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="dataTables_length">
                        <label>
                            Show
                            <select class="form-control limit">
                                <option data-limit="10">10</option>
                                <option data-limit="25">25</option>
                                <option data-limit="50">50</option>
                                <option data-limit="100">100</option>
                            </select>
                            entries
                        </label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="dataTables_filter">
                        <label>
                            Search:
                            <input type="search" class="form-control search-product">
                        </label>
                    </div>
                </div>
            </div>
            <div class="sk-folding-cube loading-wrapper" style="display:none;">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
            <table class="tablesaw table-striped  dataTable table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Posting Date</th>
                        @if($product->is_voucher)
                        <th>Serial No.</th>
                        @endif
                        <th>Posting By</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($logs as $log)
                <tr class="item">
                    <td>{{$log->added_at}}</td>
                    @if($product->is_voucher)
                    <td>{{$log->serial_no}}</td>
                    @endif
                    <td>{{$log->added_by}}</td>
                    <td>{{$log->qty}}</td>
                </tr>
                @endforeach
                <tr class="iTemplate" id="item-template">
                    <td class="item-postingDate"></td>
                    @if($product->is_voucher)
                    <td class="item-serialNo"></td>
                    @endif
                    <td class="item-postingBy"></td>
                    <td class="item-qty"></td>
                </tr>
                </tbody>
            </table>
            <div class="row mart20">
                <div class="col-sm-12 text-right pagination-wrapper">
                    <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginator->previous_page }}">
                        Previous
                    </button>
                    Page
                    <span class="current-page" data-page="{{ $paginator->current_page }}">
                        {{ $paginator->current_page }}
                    </span>
                     /
                    <span class="total-page">{{ $paginator->total_pages }}</span>
                    <button class="btn btn-primary next-btn pagination-btn{{ (!$paginator->next_page) ? ' disabled' : '' }}" data-page="{{ $paginator->next_page }}">
                        Next
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
<script>
    $(document).ready(function() {
        var vcheckbox = $("#voucher-checkbox").val();

        $('#submit-btn').click(function(e) {
            var qty = $('#inventory-qty').val();
            if (qty >= 1) {
                $('#form').submit();
            } else {
                alert('Qty must be at least 1!');
            }

            return false;
        });

        if (vcheckbox >= 1) {
            $("#voucher-no-container").removeClass('iTemplate');
            $("#single-serial-no").removeClass('iTemplate');
            $('#inventory-qty').on('change' ,function() {
                var inventoryQty = $('#inventory-qty').val();
                if(inventoryQty > 1){
                    $('.serial-no-to-wrapper').removeClass('iTemplate');
                }else{
                    $('.serial-no-to-wrapper').addClass('iTemplate');
                }
                generateLastSerialNo();
            });
        } else {
            $("#voucher-no-container").removeClass('iTemplate');
        }

        $('#serial_no_from, #inventory-qty').keyup(function() {
            generateLastSerialNo();
        });

        function generateLastSerialNo() {
            var serial_no_from = $('#serial_no_from').val();
            var inventoryqty = $('#inventory-qty').val();
            if ($('#inventory-qty').val() > 1 && $('#serial_no_from').val() > 1) {
                var serial = parseInt(serial_no_from) + (parseInt(inventoryqty) - 1);
                $('#serial_no_to').val(serial);
            }
        }

        $('.pagination-btn').click(function() {
            $('.current-page').data('page', $(this).data('page'));
            getData();
        });
        $('.limit').change(function() {
            getData(true);
        });
        $('.search-product').keyup(function(e) {
            if(e.keyCode == 13) {
                getData(true);
            }
        });
        function getData(resetPage = false) {
            var idIventory = $('#id-iventory').val();
            var limit = $('.limit').find('option:selected').data('limit');
            var page = $('.current-page').data('page');

            if (resetPage) page = 1;
            var search = $('.search-product').val();
            var pageUrl = '{{ route('redeem.inv.product-redeem.stock') }}?limit=' + limit + '&page=' + page +'&s='+ search + '&idIventory=' + idIventory;
            if (pageUrl) {
                $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                $.ajax({
                    url: pageUrl,
                    success: function(data) {
                        $('.loading-wrapper').hide().closest('table').css('opacity', 1);
                        var template = $('#item-template');
                        var items = data.DATA;
                        var clone;

                        $('.item').remove();
                        for ($i = 0; $i < items.length; $i++) {
                            var item = items[$i];
                            if(item.is_voucher == 0){
                                var isVoucher ="Product";
                            }
                            else {
                                var isVoucher = "Voucher";
                            }
                            if(item.active >=1)
                            {
                                var active = '<i class="icon md-check">'
                            }else {
                                var active = '<i class="icon md-close">'
                            }
                            clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
                            $('.item-postingDate', clone).text(item.added_at);
                            $('.item-serialNo', clone).text(item.serial_no);
                            $('.item-postingBy', clone).text(item.added_by);
                            $('.item-qty', clone).text(item.qty);
                            template.before(clone);
                        }
                        $('.current-page').text(data.paginator.current_page);
                        $('.previous-btn').data("page", data.paginator.previous_page);
                        $('.next-btn').data("page", data.paginator.next_page);
                        $('.total-page').text(data.paginator.total_pages);

                        $('.pagination-btn').removeClass('disabled');
                        if (!data.paginator.previous_page)
                            $('.previous-btn').addClass('disabled');
                        if (!data.paginator.next_page)
                            $('.next-btn').addClass('disabled');
                    }
                }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
            }
        }
    });
</script>
@endsection

@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">

@endsection
@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="row row-lg">
                <div class="col-lg-12">
                    <form class="form-horizontal">
                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Description :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Category :</label>
                                <div class="col-sm-7">
                                    <select class="form-control input-sm" disabled="">
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Applicable From :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">To :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Quantity :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Unit Measurement :</label>
                                <div class="col-sm-7">
                                    <select class="form-control input-sm" disabled="">
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Prices :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Category :</label>
                                <div class="col-sm-7">
                                    <input type="number" class="form-control" disabled="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-6">
                                <label class="col-sm-5 control-label text-left">Point  :</label>
                                <div class="col-sm-7">
                                    <select class="form-control input-sm" disabled="">
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-2 col-sm-offset-3">
                                <input type="checkbox" disabled=""> Active
                            </div>
                            <div class="col-sm-4">
                                <input type="checkbox" id="serial-number-checkbox"> Serial Number
                            </div>
                        </div>

                        <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label text-left">Information :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="col-sm-7">
                                    <div class="row">
                                        <input type="file" id="input-file-max-fs" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ url('assets/global/portraits/5.jpg') }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="col-sm-7">
                                    <div class="row">
                                        <input type="file" id="input-file-max-fs" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ url('assets/global/portraits/5.jpg') }}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="panel bg-blue-600">
        <div class="panel-body iTemplate" id="serial-no-container" data-show="false">
            <div class="col-md-12">
                <form class="form-horizontal">
                    <div class="form-group form-material-sm">
                        <div class="col-sm-6">
                            <label for="" class="control-label col-sm-3 text-left">Add Quantity</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="inventory-qty" value="" min="0">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover dataTable table-striped width-full" id="datatables-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Posting Date</th>
                    <th>Serial No.</th>
                    <th>Posting By</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
<script>
$(document).ready(function(){
    $("#serial-number-checkbox").click(function(){
        var show = $("#serial-no-container").data('show');
        
        if (!show) {
            // buka box birunya
            $("#serial-no-container").removeClass('iTemplate');
        } else {
            $("#serial-no-container").addClass('iTemplate');
        }

        $("#serial-no-container").data('show', !show);
    });
});
</script>
    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#datatables-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('master.inventory.product-redeem.create.data') }}',
                lengthMenu: [[5, 25, 100, -1], ['5', '25', '100', 'All']],
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'date', name: 'date'},
                    { data: 'serial_no', name: 'serial_no'},
                    { data: 'by', name: 'by'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ],
                "fnDrawCallback": function ( oSettings ) {
                    start = oSettings._iDisplayStart + 1;
                    /* Need to redo the counters if filtered or sorted */
                    if ( oSettings.bSorted || oSettings.bFiltered )
                    {
                        for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
                        {
                            $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+start );
                        }
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0 ] }
                ],
                "aaSorting": [[ 1, 'asc' ]]
            });
        })
    </script>

@endsection

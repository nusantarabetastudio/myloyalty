@extends('layouts.loyalty')
@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('msg'))
                <div class="alert alert-success">
                    {{ Session::get('msg') }}
                </div>
            @endif
            @if(Session::has('error_msg'))
                <div class="alert alert-danger">
                    {{ Session::get('error_msg') }}
                </div>
            @endif
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="dataTables_length">
                        <label>
                            Show
                            <select class="form-control limit">
                                <option data-limit="10">10</option>
                                <option data-limit="25">25</option>
                                <option data-limit="50">50</option>
                                <option data-limit="100">100</option>
                            </select>
                            entries
                        </label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="dataTables_filter">
                        <label>
                            Search:
                            <input type="search" class="form-control search">
                        </label>
                    </div>
                </div>
            </div>
            <div class="sk-folding-cube loading-wrapper" style="display:none;">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
            <table class="tablesaw table-striped  dataTable table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Applicable From</th>
                        <th>To</th>
                        <th>Store</th>
                        <th>Price</th>
                        <th>point</th>
                        <th>Stok</th>
                        <th>Type</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $data)
                    <tr class="item">
                        <td>{{$data->description}}</td>
                        <td>{{$data->category ? $data->category->LOK_DESCRIPTION : '-'}}</td>
                        <td>{{$data->from_date}}</td>
                        <td>{{$data->to_date}}</td>
                        <td>{{$data->tenant ? $data->tenant->TNT_DESC : '-'}}</td>
                        <td>{{$data->price}}</td>
                        <td>{{$data->point}}</td>
                        <td>{{$data->qty}}</td>
                        <td>@if($data->is_voucher>=1)
                                Voucher
                            @else
                                Product
                            @endif
                        </td>
                        <td>@if($data->active>=1)
                                <i class="icon md-check">
                            @else
                                <i class="icon md-close">
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('redeem.inv.product-redeem.addstock', $data->id) }}" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Add Stock</button></a>
                        </td>
                    </tr>
                @endforeach
                    <tr class="iTemplate" id="item-template">
                        <td class="item-description"></td>
                        <td class="item-lokDescription"></td>
                        <td class="item-fromDate"></td>
                        <td class="item-toDate"></td>
                        <td class="item-tntDesc"></td>
                        <td class="item-price"></td>
                        <td class="item-point"></td>
                        <td class="item-qty"></td>
                        <td class="item-isVoucher"></td>
                        <td class="item-active"></td>
                        <td class="item-action"></td>
                    </tr>
                </tbody>
            </table>
            <div class="row mart20">
                <div class="col-sm-12 text-right pagination-wrapper">
                    <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginator->previous_page }}">
                        Previous
                    </button>
                    Page
                    <span class="current-page">
                        {{ $paginator->current_page }}
                    </span>
                     /
                    <span class="total-page">{{ $paginator->total_pages }}</span>
                    <button class="btn btn-primary next-btn pagination-btn{{ (!$paginator->next_page) ? ' disabled' : '' }}" data-page="{{ $paginator->next_page }}">
                        Next
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" id="result"><i class="loading iTemplate"></li></div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.pagination-btn').click(function() {
                $('.current-page').data('page', $(this).data('page'));
                getData();
            });
            $('.limit').change(function() {
                var limit = $(this).find('option:selected').data('limit');
                getData(true);
            });
            $('.search').keyup(function(e) {
                if(e.keyCode == 13) {
                getData(true);
                }
            });
            function getData(resetPage = false) {
                var limit = $('.limit').find('option:selected').data('limit');
                var page = $('.current-page').data('page');
                var search = $('.search').val();
                if (resetPage) page = 1;
                var pageUrl = '{{ route('redeem.inv.product-redeem.data') }}?limit=' + limit + '&page=' + page + '&s=' + search ;
                if (pageUrl) {
                    $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: pageUrl,
                        success: function(data) {
                            $('.loading-wrapper').hide().closest('table').css('opacity', 1);
                            var template = $('#item-template');
                            var items = data.DATA;
                            var clone;

                            $('.item').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                if(item.is_voucher == 0){
                                    var isVoucher ="Product";
                                }
                                else {
                                    var isVoucher = "Voucher";
                                }
                                if(item.active >=1)
                                {
                                    var active = '<i class="icon md-check">'
                                }else {
                                    var active = '<i class="icon md-close">'
                                }
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
                                $('.item-description', clone).text(item.description);
                                $('.item-lokDescription', clone).text(item.category ? item.category.LOK_DESCRIPTION : '-');
                                $('.item-fromDate', clone).text(item.from_date);
                                $('.item-toDate', clone).text(item.to_date);
                                $('.item-tntDesc', clone).text(item.tenant ? item.tenant.TNT_DESC : '-');
                                $('.item-price', clone).text(item.price);
                                $('.item-point', clone).text(item.point);
                                $('.item-qty', clone).text(item.qty);
                                $('.item-isVoucher', clone).text(isVoucher);
                                $('.item-active', clone).html(active);
                                $('.item-action', clone).html('<a href="product-redeem/'+ item.id +'/addstock" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Add Stock</button></a>');
                                template.before(clone);
                            }
                            $('.current-page').text(data.paginator.current_page);
                            $('.previous-btn').data("page", data.paginator.previous_page );
                            $('.next-btn').data("page", data.paginator.next_page);
                            $('.total-page').text(data.paginator.total_pages);

                            $('.pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
        });
    </script>
@endsection
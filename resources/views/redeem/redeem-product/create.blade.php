@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/dropify/dropify.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection
@section('content')
    @if(Session::has('error_msg'))
        <div class="alert alert-danger">
            {{ Session::get('error_msg') }}
        </div>
    @endif
    @if(count($errors) >0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method="POST" action="{{ isset($product) ? route('redeem.product.update', $product->id) : route('redeem.product.store') }}" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ isset($product) ? method_field('PUT') : ''}}
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Product Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="description" value="{{ $product->description or old('description') }}"  class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Subtitle</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="subtitle" value="{{ $product->subtitle or old('subtitle') }}"  class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Category :</label>
                                    <div class="col-sm-7">
                                        <select name="categoryId" class="form-control input-sm select2" id="categoryId" required>
                                            <option value="">Choose One</option>
                                            @foreach($categorys as $category)
                                                @if(isset($product) && $product->product_redeem_category_id == $category->LOK_RECID)
                                                    <option value="{{ $category->LOK_RECID}}" selected>{{$category->LOK_DESCRIPTION}}</option>
                                                @else
                                                    <option value="{{ $category->LOK_RECID}}" >{{$category->LOK_DESCRIPTION}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Applicable From :</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="fromDate" value="{{ $product->from_date or old('from_date') }}" class="form-control input-sm date" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">To :</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="toDate" value="{{ $product->to_date or old('to_date') }}" class="form-control input-sm date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Quantity :</label>
                                    <div class="col-sm-7">
                                        @if (isset($product))
                                            <input type="number" name="qty" value="{{ old('qty', $product->qty) }}" class="form-control">
                                        @else
                                            <input type="number" name="qty" value="{{ old('qty') }}" class="form-control" disabled>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Unit Measurement :</label>
                                    <div class="col-sm-7">
                                        <select name="unitId" class="form-control input-sm select2" id="unitId" required>
                                            <option value="">Choose One</option>
                                            @foreach($units as $unit)
                                                @if(isset($product) && $product->unit_id == $unit->LOK_RECID)
                                                    <option value="{{ $unit->LOK_RECID}}" selected>{{$unit->LOK_DESCRIPTION }}</option>
                                                @else
                                                    <option value="{{ $unit->LOK_RECID}}" >{{$unit->LOK_DESCRIPTION }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Prices :</label>
                                    <div class="col-sm-7">
                                        <input type="number" name="price" value="{{ $product->price or old('price') }}" id="price" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-sm-5 control-label text-left">Point  :</label>
                                    <div class="col-sm-7">
                                        <input type="number" name="point" value="{{ $product->point or old('point') }}" id="point" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-6">
                                    <div class="col-sm-7">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="col-sm-2 col-sm-offset-5">
                                        <input type="checkbox" name="active"  value="1" {{ (isset($product) && $product->active == 1) || !isset($product) ? 'checked' : '' }} {{ isset($product) ? '' : '' }}> Active
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="checkbox" name="voucher" id="serial-number-checkbox" {{ (isset($product) && $product->is_voucher == 1) || !isset($product) ? 'checked' : '' }} {{ isset($product) ? '' : '' }}> Voucher
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-12">
                                    <label class="col-sm-2 control-label text-left">Information :</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="comment" value="{{ $product->comment or old('comment') }}" id="comment" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-12">
                                    <label class="col-sm-2 control-label text-left">Informasi :</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="comment_ind" value="{{ $product->comment_ind or old('comment_ind') }}" id="comment_ind" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <div class="col-sm-7">
                                        <div class="row">
                                            <input type="file" id="input-file-max-fs" name="image1" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $product->first_image_url or ''}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-7">
                                        <div class="row">
                                            <input type="file" id="input-file-max-fs" name="image2" data-plugin="dropify" data-height="165px" data-max-file-size="1M" data-default-file="{{ $product->second_image_url or '' }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if (!isset($product))
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="nav-tabs-horizontal nav-tabs-inverse nav-tabs-animate">
                                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a data-toggle="tab" href="#tabTenant" aria-controls="tabTenant" role="tab" aria-expanded="false">
                                            Tenant
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content padding-top-20">
                                        <div class="tab-pane active animation-fade" id="tabTenant" role="tabpanel">
                                            <div class="row mar0">
                                                <button type="button" class="btn btn-primary select-all-tenant-btn pull-right marlr5">Select All</button>
                                                <button type="button" class="btn btn-danger unselect-all-tenant-btn pull-right marlr5">Unselect All</button>
                                            </div>
                                            <br/>
                                            <table class="table table-hover dataTable table-striped width-full" id="tenant-datatable">
                                            <thead>
                                                <tr>
                                                    <th>Tenant</th>
                                                    <th>Actived</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($tenants as $tenant)
                                                <tr>
                                                    <td>{{ $tenant->TNT_DESC }}</td>
                                                    <td><input type="checkbox"
                                                    @if(isset($product) && $product->tenant_id == $tenant->TNT_RECID) checked
                                                    @endif
                                                     id="inputBasicOn" name="{{ (isset($product)) ? 'tenantId' : 'tenantId[]' }}" class="tenant-switch" value="{{ $tenant->TNT_RECID}}" data-plugin="switchery"></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <input type="hidden" name="tenantId" value="{{ $product->tenant_id }}">
            @endif
        <button type="submit" id="submitBtn" class="btn btn-success waves-effect waves-light">{{isset($product) ? 'Update' : 'Submit'}}</button>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/components/dropify.min.js') }}"></script>
<script>
$(function () {
    $('.date').datepicker({
        format : 'yyyy/mm/dd',
        orientation: "bottom left"
    });
    $('.select2').select2({
        'placeholder' : 'Choose One',
        'allowClear' : true,
        'width' : '100%'
    });
    $('.select-all-tenant-btn').click(function() {
        $('.tenant-switch').prop('checked', false);
        $('.tenant-switch').click();
    });
    $('.unselect-all-tenant-btn').click(function() {
        $('.tenant-switch').prop('checked', true);
        $('.tenant-switch').click();
    });
});
</script>
@endsection
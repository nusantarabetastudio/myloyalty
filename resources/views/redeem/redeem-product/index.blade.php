@extends('layouts.loyalty')
@section('content')
    <div class="panel redem-product">
        <div class="panel-body">
            @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
            @endif
            @if(Session::has('error_msg'))
                <div class="alert alert-danger">
                    {{ Session::get('error_msg') }}
                </div>
            @endif
            @if($roleId == $admin)
                <div class="row">
                    <a href="{{ route('redeem.product.create') }}" class="btn btn-primary">Add Product Redeem</a>
                </div>
            @endif
                <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="dataTables_length">
                    <label>Show
                        <select class="form-control limit">
                            <option data-limit="10">10</option>
                            <option data-limit="25">25</option>
                            <option data-limit="50">50</option>
                            <option data-limit="100">100</option>
                        </select>
                        entries</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="dataTables_filter">
                        <label>
                            Search:
                            <input type="search" class="form-control search-product">
                        </label>
                    </div>
                </div>
            </div>
            <div class="sk-folding-cube loading-wrapper" style="display:none;">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
            <table class="tablesaw table-striped dataTable table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Subtitle</th>
                        <th>Category</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Store</th>
                        <th>Price</th>
                        <th>point</th>
                        <th>Stok</th>
                        <th>Type</th>
                        <th>Active</th>
                        @if($roleId == $admin)
                        <th>Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $data)
                    <tr class="item">
                        <td>{{$data->product_code or '-'}}</td>
                        <td>{{$data->description}}</td>
                        <td>{{$data->subtitle}}</td>
                        <td>{{$data->category ? $data->category->LOK_DESCRIPTION : '-'}}</td>
                        <td>{{$data->from_date}}</td>
                        <td>{{$data->to_date}}</td>
                        <td>{{$data->tenant->TNT_DESC}}</td>
                        <td>{{$data->price}}</td>
                        <td>{{$data->point}}</td>
                        <td>{{$data->qty}}</td>
                        <td>@if($data->is_voucher=='1')  Voucher @else Product @endif
                        </td>
                        <td>
                            <?php if($data->active>=1){?><i class="icon md-check"><?php }else{ ?> <i class="icon md-close"> <?php } ?>
                        </td>
                        @if($roleId == $admin)
                            <td>
                                <a href="{{ route('redeem.product.edit', $data->id) }}" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>
                                <button type="button" class="btn btn-danger btn-xs" data-menu-id="{{ $data->id }}"  data-toggle="modal" data-target="#modalDelete">Delete</button>
                            </td>
                        @endif
                    </tr>
                @endforeach
                <tr class="iTemplate" id="item-template">
                    <td class="item-code"></td>
                    <td class="item-description"></td>
                    <td class="item-subtitle"></td>
                    <td class="item-category"></td>
                    <td class="item-from-date"></td>
                    <td class="item-to-date"></td>
                    <td class="item-store"></td>
                    <td class="item-price"></td>
                    <td class="item-point"></td>
                    <td class="item-stock"></td>
                    <td class="item-type"></td>
                    <td class="item-active"></td>
                    @if($roleId == $admin)
                        <td class="item-action">
                    @endif
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="row mart20">
                <div class="col-sm-12 text-right pagination-wrapper">
                    <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginator->previous_page }}">
                        Previous
                    </button>
                    Page 
                    <span class="current-page" data-page="{{ $paginator->current_page }}">
                        {{ $paginator->current_page }}
                    </span>
                     / 
                    <span class="total-page">{{ $paginator->total_pages }}</span>
                    <button class="btn btn-primary next-btn pagination-btn{{ (!$paginator->next_page) ? ' disabled' : '' }}" data-page="{{ $paginator->next_page }}">
                        Next
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-3d-flip-vertical" id="modalDelete" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <form action="{{ route('redeem.product.destroy') }}" method="POST">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="productId" id="menu_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Confirm..</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.pagination-btn').click(function() {
                $('.current-page').data('page', $(this).data('page'));
                getData();
            });
            $('.limit').change(function() {
                getData(true);
            });
            $('.search-product').keyup(function(e) {
                if(e.keyCode == 13) {
                    getData(true);
                }
            });
            function getData(resetPage = false) {
                var limit = $('.limit').find('option:selected').data('limit');
                var page = $('.current-page').data('page');
                
                if (resetPage) page = 1;
                var search = $('.search-product').val();
                var pageUrl = '{{ route('redeem.product.data') }}?limit=' + limit + '&page=' + page +'&s='+ search;
                if (pageUrl) {
                    $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: pageUrl,
                        success: function(data) {
                            $('.loading-wrapper').hide().closest('table').css('opacity', 1);
                            var template = $('#item-template');
                            var items = data.DATA;
                            var clone;

                            $('.item').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                if(item.is_voucher == 0){
                                    var isVoucher ="Product";
                                }
                                else {
                                    var isVoucher = "Voucher";
                                }
                                if(item.active >=1)
                                {
                                    var active = '<i class="icon md-check">'
                                }else {
                                    var active = '<i class="icon md-close">'
                                }
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
                                $('.item-code', clone).text(item.product_code ? item.product_code : '-');
                                $('.item-description', clone).text(item.description);
                                $('.item-subtitle', clone).text(item.subtitle);
                                $('.item-category', clone).text(item.category ? item.category.LOK_DESCRIPTION : '-');
                                $('.item-from-date', clone).text(item.from_date);
                                $('.item-to-date', clone).text(item.to_date);
                                $('.item-store', clone).text(item.tenant ? item.tenant.TNT_DESC : '-');
                                $('.item-price', clone).text(item.price);
                                $('.item-point', clone).text(item.point);
                                $('.item-stock', clone).text(item.qty);
                                $('.item-type', clone).text(isVoucher);
                                $('.item-active', clone).html(active);
                                $('.item-action', clone).html('<a href="product/'+ item.id +'/edit" class="item-edit" style="text-decoration:none"><button type="button" class="btn btn-primary btn-xs">Edit</button></a><button type="button" class="btn btn-danger btn-xs" data-menu-id="'+ item.id +'"  data-toggle="modal" data-target="#modalDelete">Delete</button>');
                                template.before(clone);
                            }
                            $('.current-page').text(data.paginator.current_page);
                            $('.previous-btn').data("page", data.paginator.previous_page);
                            $('.next-btn').data("page", data.paginator.next_page);
                            $('.total-page').text(data.paginator.total_pages);

                            $('.pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
        });
        var oTable;
        var val =''; // role id
        $(function(){
            oTable = $('#datatable-table').DataTable({
                processing: true
            });
            $('#modalDelete').on('show.bs.modal', function(event){
                $('#menu_id').val($(event.relatedTarget).data('menu-id'));
            });
        });
    </script>
@endsection
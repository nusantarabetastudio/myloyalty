@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('notif_success'))
                <div class="alert alert-success">
                    {{ Session::get('notif_success') }}
                </div>
            @endif
            @if(Session::has('notif_error'))
                <div class="alert alert-danger">
                    {{ Session::get('notif_error') }}
                </div>
            @endif
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="dataTables_length">
                        <label>
                            Show
                            <select class="form-control limit">
                            <option data-limit="10">10</option>
                            <option data-limit="25">25</option>
                            <option data-limit="50">50</option>
                            <option data-limit="100">100</option>
                            </select>
                            entries
                        </label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="dataTables_filter">
                        <label>
                            Search:
                            <input type="search" class="form-control search">
                        </label>
                    </div>
                </div>
            </div>
            <div class="sk-folding-cube loading-wrapper" style="display:none;">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
            <table class="tablesaw table-striped  dataTable table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Barcode Number</th>
                    <th>Card Type</th>
                    <th>Join Date</th>
                    <th>Active</th>
                    <th>Detail</th>
                </tr>
                </thead>
                <tbody>
                @if ($customers)
                @foreach($customers as $data)
                    <tr class="item">
                        <td>{{$data->CUST_NAME}}</td>
                        <td>{{$data->CUST_BARCODE}}</td>
                        <td>{{$data->card_type->CARD_DESC}}</td>
                        <td>{{$data->CUST_JOINDATE}}</td>
                        <td>@if($data->CUST_ACTIVE >=1)
                                <i class="icon md-check">
                            @else
                                <i class="icon md-close">
                            @endif
                        </td>
                        <td>
                        <a href="{{ route('redeem', $data->CUST_RECID) }}" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Redeem</button></a>
                        </td>
                    </tr>
                @endforeach
                    <tr class="iTemplate" id="item-template">
                        <td class="item-name"></td>
                        <td class="item-barcode"></td>
                        <td class="item-desc"></td>
                        <td class="item-joinDate"></td>
                        <td class="item-active"></td>
                        <td class="item-detail"></td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="row mart20">
                <div class="col-sm-12 text-right pagination-wrapper">
                    <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginator->previous_page }}">
                        Previous
                    </button>
                    Page
                    <span class="current-page" data-page="{{ $paginator->current_page }}">
                        {{ $paginator->current_page }}
                    </span>
                     /
                    <span class="total-page">{{ $paginator->total_pages }}</span>
                    <button class="btn btn-primary next-btn pagination-btn{{ (!$paginator->next_page) ? ' disabled' : '' }}" data-page="{{ $paginator->next_page }}">
                        Next
                    </button>
                </div>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Customer Editor</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmCustomer" name="frmCustomer" class="form-horizontal" novalidate="">
                                {{ csrf_field() }}
                                <div class="form-group error">
                                    <label for="inputTask" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="name" name="name" placeholder="Name" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="address" name="address" placeholder="Address" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                            <input type="hidden" id="customer_id" name="customer_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.pagination-btn').click(function() {
                $('.current-page').data('page', $(this).data('page'));
                getData();
            });
            $('.limit').change(function() {
                getData(true);
            });
            $('.search').keyup(function(e) {
                if(e.keyCode == 13) {
                    getData(true);
                }
            });
            function getData(resetPage = false) {
                var limit = $('.limit').find('option:selected').data('limit');
                var page = $('.current-page').data('page');
                var search = $('.search').val();
                var type = 'productRedeem';
                if (resetPage) page = 1;
                var dataUrl = '{{ route('redeem.transaction.customer.data') }}?pnum={{ $pnum }}&ptype={{ $ptype }}&limit=' + limit + '&page=' + page + '&s=' + search + '&type=' + type;
                if (dataUrl) {
                    $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: dataUrl,
                        success: function(data) {
                            var template = $('#item-template');
                            var items = data.DATA;
                            $('.item').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item');
                                $('.item-name', clone).text(item.CUST_NAME);
                                $('.item-barcode', clone).text(item.CUST_BARCODE);
                                $('.item-desc', clone).text(item.card_type ? item.card_type.CARD_DESC : '-');
                                $('.item-joinDate', clone).text(item.CUST_JOINDATE);
                                $('.item-active', clone).text(item.CUST_ACTIVE);
                                $('.item-detail', clone).html('<a href="customer/'+ item.CUST_RECID +'" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Redeem</button></a>');
                                template.before(clone);
                            }
                            $('.current-page').text(data.paginator.current_page).data('page', data.paginator.current_page);
                            $('.previous-btn').data("page", data.paginator.previous_page );
                            $('.next-btn').data("page", data.paginator.next_page);
                            $('.total-page').text(data.paginator.total_pages);
                            $('.pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
        });
    </script>
@endsection
@extends('layouts.loyalty')

@section('content')
	<div clas="row row-lg">
		<div class="col-lg-12">
			<div class="nav-tabs-horizontal">
				<div class="tab-pane active" row="tabpanel">
					<div class="panel">
						<div class="panel-body">
						@if (Session::has('notif_success'))
							<div class="alert alert-success">
								{{ Session::get('notif_success') }}
							</div>
						@endif
			            @if (count($errors) > 0)
			                <div class="alert alert-danger">
			                    <ul>
			                        @foreach ($errors->all() as $error)
			                            <li>{{ $error }}</li>
			                        @endforeach
			                    </ul>
			                </div>
			            @endif
							<form class="form-horizontal">
								<div class="form-group form-material-sm">
									<div class="col-sm-12">
										<h1 class="page-title">Validate Booking Redeem</h1>
									</div>
								</div>
								<div class="form-group form-material-sm">
									<div class="col-sm-9">
										<label class="control-label">Booking Code:</label>
										<input type="text" class="form-control booking-code" value="{{ old('booking_code') }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <a type="button" class="btn btn-primary submit-btn" data-toggle="modal" data-target="#view-detail-modal">Submit</a>
                                    </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="modal fade" id="view-detail-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.submit-btn').on('click', function() {
				var modalTarget = $(this).data("target") + ' .modal-content';
				var bookingCode = $('.booking-code').val();
				$(this).find('.modal-title').text('Booking Code: ' + bookingCode);
				$(modalTarget).load('view-book/detail-book/'+bookingCode);
			});
		});
	</script>
@endsection
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@else
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <div class="row">
    	<div class="col-md-3"><h4><strong>Booking Code</strong></h4></div>
    	<div class="col-md-5"><h4><strong>: {{ $redeemPost->booking_code }}</strong></h4></div>
    	<div class="col-md-4"></div>
    </div>
    <div class="row">
    	<div class="col-md-3"><h4><strong>Customer Name </strong></h4></div>
    	<div class="col-md-5"><h4><strong>: {{ $redeemPost->customer->CUST_NAME }}</strong></h4></div>
    	<div class="col-md-4"></div>
    </div>
    <div class="row">
    	<div class="col-md-3"><h4><strong>Customer Barcode </strong></h4></div>
    	<div class="col-md-5"><h4><strong>: {{ $redeemPost->customer->CUST_BARCODE }}</strong></h4></div>
    	<div class="col-md-4"></div>
    </div>
</div>
<form method="POST" action="{{ route('redeem.transaction.cekValidate') }}" class="form-horizontal">
	{{ csrf_field() }}
	<div class="modal-body">
		<div class="form-group">
			<input type="hidden" class="form-control" name="booking_code" value="{{ $redeemPost->booking_code }}">
			<table class="table table-bordered table-hover table-striped" cellspacing="0" id="transaction_detail">
				<thead>
					<tr>
						<th class="text-center">Qty</th>
						<th class="text-center">Product Name</th>
					</tr>
				</thead>
				<tbody>
				@foreach($redeemPost->details as $detail)
					<tr>
						<td align="center">{{ $detail->qty }}</td>
						<td align="center">{{ $detail->redeemProduct->description }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</form>
@endif
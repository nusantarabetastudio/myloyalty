@extends('layouts.loyalty')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection
@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="panel panel-detail">
                <div class="panel-body">
                    <form class="form-horizontal" action="#" method="POST">
                        @if(Session::has('notif_success'))
                            <div class="alert alert-success">
                                {{ Session::get('notif_success') }}
                            </div>
                        @endif
                        @if(Session::has('notif_error'))
                            <div class="alert alert-danger">
                                {{ Session::get('notif_error') }}
                            </div>
                        @endif
                        <div class="form-group form-material-sm">
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Barcode No:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ $customers->CUST_BARCODE }}">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <label class="col-sm-3 control-label text-left">Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ $customers->CUST_NAME }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Total Expense:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ $customers->CUST_EXPENSE_EARN }}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-8 control-label text-left">Total Earn Lucky Draw:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ $customers->point->total_lucky_draw_point }}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-7 control-label text-left">Point Balance:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ $customers->point->total_redeem_point }}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel panel-detail">
                <div class="panel-body">
                    <form class="form-horizontal" action="{{ route('redeem.transaction.store') }}" method="POST" id="form">
                        {{ csrf_field() }}
                        <div class="form-group form-material-sm">
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Store</label>
                                <div class="col-sm-7">
                                    <input type="hidden" class="form-control" name='pnum' value='{{ $customers->CUST_PNUM}}'>
                                    <input type="hidden" class="form-control" name="ptype" value="{{ $customers->CUST_PTYPE}}">
                                    <input type="hidden" class="form-control customerId" name="customerId" value="{{ $customers->CUST_RECID}}">
                                    <input type="hidden" class="form-control" name="tenantId" value="{{$storeId}}">
                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{$storeName}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-sm-5 control-label text-left">Posting Date :</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="" disabled="disabled" value="{{ $tgl->format('d m Y') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-md-4">
                                <label class="control-label">Product :</label>
                                <select name="" id="new-material-product" class="form-control input-md select2" required>
                                    <option value="" selected>Choose One</option>
                                    @foreach($products as $product)
                                            <option value="{{$product->id}}" data-type="{{ ($product->is_voucher == 0) ? 'product' : 'voucher'}}" data-point="{{ ($product->point)}}" data-stok="{{ ($product->qty)}}">{{$product->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3 iTemplate" id="qty-container">
                                <label class="control-label">Qty :</label>
                                    <input type="number" class="form-control" id="new-material-qty" name="">
                            </div>
                            <div class="col-md-3 iTemplate" id="serial-container">
                                <label class="control-label">Serial # :</label>
                                <input type="text" class="form-control" id="new-material-serial" name="" >
                            </div>
                            <div class="col-md-2 text-right">
                                <a class="btn btn-primary mart27" id="add-product-btn">Add Product </a>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                                <div class="col-md-4 iTemplate" id="stok-container">
                                    <label class="control-label">Stok :</label>
                                    <input type="text" class="form-control" id="new-material-stok" name="" disabled>
                                </div>
                                <div class="col-md-3 iTemplate" id="point-container">
                                    <label class="control-label">Point :</label>
                                    <input type="text" class="form-control" id="new-material-point" name="" disabled>
                                </div>
                            </div>
                            <div class="form-group form-material-sm">
                            <div class="col-sm-12">
                                <table class="table table-hover dataTable table-striped width-full" id="payment-table-container">
                                    <thead>
                                        <tr>
                                            <th>PRODUCT</th>
                                            <th>QTY</th>
                                            <th>POINT</th>
                                            <th>SUB TOTAL POINT</th>
                                            <th>SERIAL</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody id="material-container">
                                        <tr id="material-template" class="iTemplate">
                                            <th class="material-product"></th>
                                            <th class="material-qty">-</th>
                                            <th class="material-point">-</th>
                                            <th class="material-sub-point">-</th>
                                            <th class="material-serial">-</th>
                                            <th><a class="material-delete">Delete</a></th>
                                        </tr>
                                        <tr>
                                            <th class="bold">TOTAL</th>
                                            <th class="total-qty bold">0</th>
                                            <th class="bold"></th>
                                            <th class="grand-total-point bold" colspan="3">0</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group form-material-sm">
                            <div class="col-sm-2 pull-right">
                                <a class="btn btn-block btn-primary btn-submit">Submit</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <h1>LOG REDEEM</h1>
            <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a data-toggle="tab" href="#tabTransaction" aria-controls="tabTransaction"
                       role="tab" aria-expanded="false">
                        Transaction
                    </a>
                </li>
                <li role="presentation" class="">
                    <a data-toggle="tab" href="#tabRedeemProduct" aria-controls="tabRedeemProduct"
                       role="tab" aria-expanded="false">
                        Redeemed Product History
                    </a>
                </li>
            </ul>
            <div class="tab-content padding-top-20">
                @include('redeem.redeem-transaction.partials.transaction-log')
                @include('redeem.redeem-transaction.partials.redeemed-product-log')
            </div>
        </div>
    </div>
    <div class="modal fade" id="view-transaction-details" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
<div class="modal fade modal-3d-flip-vertical" id="modalVoid" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <form action="{{ route('redeem.transaction.void') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="redeemId" id="redeemId">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Confirm..</h4>
                </div>
                <div class="modal-body">
                    Are you sure to transaction redeem?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
    <script type="text/javascript">
        $(function () {
            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });
            $('#modalVoid').on('show.bs.modal', function(event){
                $('#redeemId').val($(event.relatedTarget).data('redeem-id'));
            });
        })
    </script>
    <script>
        var countProduct = 1;
        $(document).ready(function() {
            @if (Session::has('notif_success'))
                localStorage.removeItem("productIds");
                localStorage.removeItem("productNames");
                localStorage.removeItem("quantities");
                localStorage.removeItem("points");
                localStorage.removeItem("serialNos");
            @else
                var storedProductIds = JSON.parse(localStorage.getItem("productIds"));
                var storedProductNames = JSON.parse(localStorage.getItem("productNames"));
                var storedQuantities = JSON.parse(localStorage.getItem("quantities"));
                var storedPoints = JSON.parse(localStorage.getItem("points"));
                var storedSerialNos = JSON.parse(localStorage.getItem("serialNos"));
                if (storedProductIds == null || storedProductNames == null || storedQuantities == null || storedSerialNos == null) {
                    // Do Nothing
                } else {
                    for (var i = 0; i < storedProductIds.length; i++) {
                        addProduct(storedProductNames[i], storedProductIds[i], parseInt(storedQuantities[i]), parseInt(storedPoints[i]), storedSerialNos[i]);
                    }
                }
            @endif

            $('#new-material-product').change(function() {
                var product_containt = $(this).find('option:selected').data('type');
                var point_containt = $(this).find('option:selected').data('point');
                var stok_containt = $(this).find('option:selected').data('stok');
                if (product_containt == 'voucher') {
                    $("#serial-container").removeClass('iTemplate');
                    $('#new-material-qty').val(1).attr('disabled', 'disabled');
                    $("#qty-container").removeClass('iTemplate');
                    $('#add-product-btn').parent().removeClass('col-md-5').addClass('col-md-2');
                } else {
                    $("#serial-container").addClass('iTemplate');
                    $('#new-material-serial').val('').attr('value', '');
                    $("#qty-container").removeClass('iTemplate');
                    $('#new-material-qty').removeAttr('disabled').val(1);
                    $('#add-product-btn').parent().removeClass('col-md-2').addClass('col-md-5');
                }
                $("#stok-container").removeClass('iTemplate');
                $("#new-material-stok").val(stok_containt);
                $("#point-container").removeClass('iTemplate');
                $("#new-material-point").val(point_containt);
            });

            $('.btn-submit').click(function() {
                var productIds = [];
                var productNames = [];
                var quantities = [];
                var points = [];
                var serialNos = [];

                for (var i = 0; i < $('.add-item-productId').length; i++) {
                    productIds[i] = $('.add-item-productId:eq('+ i +')').val();
                    productNames[i] = $('.add-item-productName:eq('+ i +')').text();
                    quantities[i] = $('.add-item-qty:eq('+ i +')').val();
                    points[i] = $('.add-item-point:eq('+ i +')').val();
                    serialNos[i] = $('.add-item-serial:eq('+ i +')').val();
                }

                localStorage.setItem("productIds", JSON.stringify(productIds));
                localStorage.setItem("productNames", JSON.stringify(productNames));
                localStorage.setItem("quantities", JSON.stringify(quantities));
                localStorage.setItem("points", JSON.stringify(points));
                localStorage.setItem("serialNos", JSON.stringify(serialNos));

                $('#form').submit();
            });

            $('#add-product-btn').click(function(e) {
                var product_containt = $('#new-material-product option:selected').data('type');
                var productName = $('#new-material-product option:selected').text();
                var productId = $('#new-material-product').val();
                var qty = parseInt($('#new-material-qty').val());
                var point = parseInt($('#new-material-point').val());
                var serial = $('#new-material-serial').val();
                $('#new-material-serial').val('');
                var error = false;
                if (qty < 0) {
                    alert('Qty must be at least 1');
                    $('#new-material-qty').val(1);
                    error = true;
                    return false;
                }
                if (product_containt == 'voucher') {
                    if (serial == '') {
                        alert('Serial cannot be empty');
                        error = true;
                    }

                    $('.material-serial .serial').each(function() {
                        if (serial == $(this).text()) {
                            alert('Cannot add duplicate serial number in 1 transaction!');
                            error = true;
                            return false;
                        }
                    });
                }
                if (!error) addProduct(productName, productId, qty, point, serial);
            });
            $('#new-material-qty, #new-material-serial').keyup(function(e) {
                if (e.keyCode == 13) {
                    var product_containt = $('#new-material-product option:selected').data('type');
                    var productName = $('#new-material-product option:selected').text();
                    var productId = $('#new-material-product').val();
                    var qty = parseInt($('#new-material-qty').val());
                    var point = parseInt($('#new-material-point').val());
                    var serial = $('#new-material-serial').val();
                    $('#new-material-serial').val('');
                    if(product_containt == 'voucher' && serial == '') {
                        alert('Serial not be empty');
                    } else {
                    addProduct(productName, productId, qty, point, serial);
                    e.preventDefault();
                    return false;
                    }
                }
            });

            function addProduct(productName, productId, qty, point, serial) {
                if (qty) {
                    var template = $('#material-template');
                    var clone = template.clone().removeAttr('id').removeClass('iTemplate');
                    var totalQty = parseInt($('.total-qty').text());
                    var grandTotal = parseInt($('.grand-total-point').text());
                    $('.material-product', clone).html(
                        '<span class="product add-item-productName">'+ productName +'</span>'+
                        '<input type="hidden" class="add-item-productId" value="'+ productId +'" name="products[' + countProduct + '][id]">'
                    );
                    $('.material-qty', clone).html(
                        '<span class="qty">'+ qty +'</span>'+
                        '<input type="hidden" class="add-item-qty" value="'+qty+'" name="products['+ countProduct +'][qty]">');
                    $('.material-point', clone).html(
                        '<span class="point">'+ point +'</span>'+
                        '<input type="hidden" class="add-item-point" value="'+point+'" name="products['+ countProduct +'][point]">');
                    totalSubPoint = qty * point;
                    $(".material-sub-point", clone).html(
                        '<span class="totalSubPoint">' + totalSubPoint +'</span>');
                    $('.material-serial', clone).html(
                        '<span class="serial">'+ serial +'</span>'+
                        '<input type="hidden" class="add-item-serial" value="'+serial+'" name="products['+ countProduct +'][serialNo]">');

                    $('.material-delete', clone).click(function() {
                        var qty = parseInt($('.qty', clone).text());
                        var totalQty =parseInt($('.total-qty').text());
                        totalQty = totalQty - qty;
                        $('.total-qty').text(totalQty);

                        var totalSubPoint = parseInt($('.totalSubPoint', clone).text());
                        var totalPoint = parseInt($('.grand-total-point').text());
                        grandTotal = totalPoint - totalSubPoint;
                        $('.grand-total-point').text(grandTotal);
                        $(this).parent().parent().remove();
                    });

                    totalQty = qty + totalQty;
                    $(".total-qty").text(totalQty);

                    grandTotal = grandTotal + totalSubPoint;
                    $('.grand-total-point').text(grandTotal);

                    template.before(clone);
                    countProduct++;
                }
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabTransaction .transaction-pagination-btn').click(function() {
                $('.current-page').data('page', $(this).data('page'));
                getDataTransaction();

            });
            $('#tabTransaction .transaction-txt-box').change(function() {
                getDataTransaction(true);
            });
            $('.search-transaction').keyup(function(e) {
                if(e.keyCode == 13) {
                    getDataTransaction(true);
                }
            });
            function getDataTransaction(resetPage = false) {
                var limit = $('.transaction-txt-box').find('option:selected').data('limit-transaction');
                var page = $('.current-page').data('page');

                if (resetPage) page = 1;
                var search = $('.search-transaction').val();
                var type = 'logRedeem';
                var  redeemId = $('.customerId').val();
                var pageUrlTransaction = '{{ route('redeem.transaction.customer.data') }}?limit=' + limit + '&page=' + page + '&redeemId='+ redeemId +'&s='+ search + '&type=' + type;
                if (pageUrlTransaction) {
                    $('.loading-wrapper').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: pageUrlTransaction,
                        success: function(data) {
                            var template = $('#item-template');
                            var items = data.DATA.redeemTransactions;
                            var clone;
                            $('.item').remove();
                            $('.itemvoid').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                if (item.is_void == '1') {
                                    var itemvoid = ' itemvoid';
                                } else {
                                    var itemvoid = ' ';
                                }
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item'+ itemvoid);
                                $('.item-receiptNo', clone).text(item.receipt_no);
                                $('.item-postDate', clone).text(item.post_date);
                                $('.item-price', clone).text(item.price);
                                $('.item-point', clone).text(item.point);
                                $('.item-postedBy', clone).text(item.posted_by);
                                var defaultHref = $('.item-detail', clone).attr('href');
                                $('.item-detail', clone).attr('href', defaultHref + item.id);
                                if (item.posted_by != "BOOKING APP")
                                    $('.item-void', clone).data('redeem-id', item.id);
                                else 
                                    $('.item-void', clone).remove();
                                @if (in_array($roleId, [$admin, $superAdmin]))
                                    if (item.is_void == 1) {
                                        $('.item-void', clone).remove();
                                    } else {
                                        $('.item-void', clone).data('redeem-id', item.id);
                                    }
                                @elseif ($roleId = $supervisor)
                                    if (item.voidable == false) {
                                        $('.item-void', clone).remove();
                                    } else {
                                        $('.item-void', clone).data('redeem-id', item.id);
                                    }
                                @else
                                    $('.item-void', clone).remove();
                                @endif
                                template.before(clone);
                            }
                            $('.current-page').text(data.paginator.current_page).data('page', data.paginator.current_page);
                            $('.transaction-previous-btn').data("page", data.paginator.previous_page );
                            $('.transaction-next-btn').data("page", data.paginator.next_page);
                            $('.transaction-total-page').text(data.paginator.total_pages);
                            $('.transaction-pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.transaction-previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.transaction-next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
        });
        $(document).ready(function(){
            $('#tabRedeemProduct .pagination-btn').click(function() {
                $('.current-page-redeemProduct').data('page', $(this).data('page'));
                getDataProduct();
            });

            $('#tabRedeemProduct .product-txt-box').change(function() {
                getDataProduct(true);
            });
            $('.search-RedeemProduct').keyup(function(e) {
                if(e.keyCode == 13) {
                    getDataProduct(true);
                }
            });
            function getDataProduct(resetPage = false) {
                var limit = $('.product-txt-box').find('option:selected').data('limit-product');
                var page = $('.current-page-redeemProduct').data('page');

                if (resetPage) page = 1;
                var search = $('.search-RedeemProduct').val();
                var type = 'redeemProduchistory';
                var  redeemId = $('.customerId').val();
                var pageUrlProduct = '{{ route('redeem.transaction.customer.data') }}?limit=' + limit + '&page=' + page + '&redeemId='+ redeemId +'&s='+ search + '&type=' + type;
                if (pageUrlProduct) {
                    $('.loading-wrapper-log').show().siblings('table').css('opacity', 0.5);
                    $.ajax({
                        url: pageUrlProduct,
                        success: function(data) {
                            var template = $('#item-template-redeemProduct');
                            var items = data.DATA.redeemTransactionDetails;
                            var clone;
                            $('.item-redeemProduct').remove();
                            $('.itemvoid').remove();
                            for ($i = 0; $i < items.length; $i++) {
                                var item = items[$i];
                                if (item.is_void == '1') {
                                    var itemvoid = ' itemvoid';
                                } else {
                                    var itemvoid = ' ';
                                }
                                clone = template.clone().removeAttr('id').removeClass('iTemplate').addClass('item-redeemProduct'+ itemvoid);
                                $('.item-fromDate-redeemProduct', clone).text(item.post_date);
                                $('.item-Description-redeemProduct', clone).text(item.description);
                                $('.item-qty-redeemProduct', clone).text(item.qty);
                                $('.item-point-redeemProduct', clone).text(item.point);
                                $('.item-receipt_no-redeemProduct', clone).text(item.receipt_no);
                                $('.item-serialNo-redeemProduct', clone).text(item.serial_no);
                                template.before(clone);
                            }
                            $('.current-page-redeemProduct').text(data.paginator.current_page).data('page', data.paginator.current_page);
                            $('.previous-btn').data("page", data.paginator.previous_page );
                            $('.next-btn').data("page", data.paginator.next_page);
                            $('.total-page').text(data.paginator.total_pages);

                            $('.pagination-btn').removeClass('disabled');
                            if (!data.paginator.previous_page)
                                $('.previous-btn').addClass('disabled');
                            if (!data.paginator.next_page)
                                $('.next-btn').addClass('disabled');
                        }
                    }).complete(function() {
                        $('.loading-wrapper-log').hide().siblings('table').css('opacity', 1);
                    });
                }
            }
        });
    </script>
@endsection
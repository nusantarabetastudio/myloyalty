@extends('layouts.loyalty')
@section('content')
	<div clas="row row-lg">
		<div class="col-lg-12">
			<div class="nav-tabs-horizontal">
				<div class="tab-pane active" row="tabpanel">
					<div class="panel">
						<div class="panel-body">
						@if (Session::has('notif_success'))
							<div class="alert alert-success">
								{{ Session::get('notif_success') }}
							</div>
						@endif
			            @if (count($errors) > 0)
			                <div class="alert alert-danger">
			                    <ul>
			                        @foreach ($errors->all() as $error)
			                            <li>{{ $error }}</li>
			                        @endforeach
			                    </ul>
			                </div>
			            @endif
							<form method="POST" action="{{ route('redeem.transaction.book.validate') }}" class="form-horizontal">
								<div class="form-group form-material-sm">
									<div class="col-sm-12">
										<h1 class="page-title"></h1>
									</div>
								</div>
                                {{ csrf_field() }}
								<div class="form-group form-material-sm">
									<div class="col-sm-9">
										<label class="control-label">Booking Code:</label>
										<input type="text" class="form-control" name="booking_code" value="{{ old('booking_code') }}">
									</div>
								</div>
								<div class="form-group form-material-sm">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
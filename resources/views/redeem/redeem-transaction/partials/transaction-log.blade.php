<div class="tab-pane active animation-fade" id="tabTransaction" role="tabpanel">
    <div class="row">
        <div class="col-lg-6">
            <div class="dataTables_length">
                <label>
                    Show
                    <select class="form-control transaction-txt-box">
                        <option data-limit-transaction="10">10</option>
                        <option data-limit-transaction="25">25</option>
                        <option data-limit-transaction="50">50</option>
                        <option data-limit-transaction="100">100</option>
                    </select>
                    entries
                </label>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="dataTables_filter">
                <label>
                    Search:
                    <input type="search" class="form-control search-transaction">
                </label>
            </div>
        </div>
    </div>
    <div class="sk-folding-cube loading-wrapper" style="display:none;">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
    <table class="table table-hover dataTable table-striped width-full">
        <thead>
        <tr>
            <th>RECEIPT NO</th>
            <th>POST DATE</th>
            <th>TOTAL PRICE</th>
            <th>TOTAL POINT</th>
            <th>POSTED BY</th>
            <th>ACTION</th>
        </tr>
        </thead>
        @if(empty(!$transactions))
        <tbody>
        @foreach($transactions as $transaction)
        <tr class="item @if($transaction->is_void =='1') itemvoid @else '' @endif">
            <td>{{$transaction->receipt_no}}</td>
            <td>{{$transaction->post_date}}</td>
            <td>{{$transaction->price}}</td>
            <td>{{$transaction->point}}</td>
            <td>{{$transaction->posted_by}}</td>
            <td>
                <a href="{{ route('redeem.transaction.transaction.details', $transaction->id) }}" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#view-transaction-details">View Detail</a>
                @if ((($transaction->voidable && $roleId == $supervisor) || (in_array($roleId, [$admin, $superAdmin]) && !$transaction->is_void)) && $transaction->booking_code == null)
                    <button type="button" class="btn btn-danger btn-xs" data-redeem-id="{{ $transaction->id }}"  data-toggle="modal" data-target="#modalVoid">Void</button>
                @endif
            </td>
        </tr>
        @endforeach
        <tr class="iTemplate" id="item-template">
        <td class="item-receiptNo"></td>
        <td class="item-postDate"></td>
        <td class="item-price"></td>
        <td class="item-point"></td>
        <td class="item-postedBy"></td>
        <td class="item-action">
            <a href="{{ url('redeem/transaction/transaction') }}/" type="button" class="btn btn-primary btn-xs item-detail" data-toggle="modal" data-target="#view-transaction-details">View Detail</a>
            <button type="button" class="btn btn-danger btn-xs item-void" data-redeem-id=""  data-toggle="modal" data-target="#modalVoid">Void</button>
        </td>
        </tr>
        </tbody>
        @endif
    </table>
    <div class="row mart20">
        <div class="col-sm-12 text-right pagination-wrapper">
            <button class="btn btn-primary transaction-previous-btn transaction-pagination-btn disabled" data-page-url-transaction="{{ $paginatorRedeemTransaction->previous_page }}">
                Previous
            </button>
            Page
            <span class="current-page" data-page="{{ $paginatorRedeemTransaction->current_page }}">
                {{ $paginatorRedeemTransaction->current_page }}
            </span>
             /
            <span class="transaction-total-page">{{ $paginatorRedeemTransaction->total_pages }}</span>
            <button class="btn btn-primary transaction-next-btn transaction-pagination-btn{{ (!$paginatorRedeemTransaction->next_page) ? ' disabled' : '' }}" data-page="{{ $paginatorRedeemTransaction->next_page }}">
                Next
            </button>
        </div>
    </div>
</div>
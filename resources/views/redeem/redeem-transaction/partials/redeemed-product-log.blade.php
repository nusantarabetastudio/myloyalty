<div class="tab-pane animation-fade" id="tabRedeemProduct" role="tabpanel">
    <div class="row">
        <div class="col-lg-6">
            <div class="dataTables_length">
                <label>
                    Show
                    <select class="form-control product-txt-box">
                        <option data-limit-product="10">10</option>
                        <option data-limit-product="25">25</option>
                        <option data-limit-product="50">50</option>
                        <option data-limit-product="100">100</option>
                    </select>
                    entries
                </label>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="dataTables_filter">
                <label>
                    Search:
                    <input type="search" class="form-control search-RedeemProduct">
                </label>
            </div>
        </div>
    </div>
    <div class="sk-folding-cube loading-wrapper-log" style="display:none;">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
    <table class="table table-hover dataTable table-striped width-full">
        <thead>
            <tr>
                <th>DATE</th>
                <th>PRODUCT NAME</th>
                <th>QTY</th>
                <th>POINT</th>
                <th>Receipt No RECEIPT NO</th>
                <th>SERIAL NO</th>
            </tr>
        </thead>
        <tbody>
        @foreach($redeemProducts as $redeemProduct)
            <tr class="item-redeemProduct{{ ($redeemProduct->is_void =='1') ? ' itemvoid' : '' }}">
                <td>{{ $redeemProduct->post_date }}</td>
                <td>{{ $redeemProduct->description }}</td>
                <td>{{ $redeemProduct->qty }}</td>
                <td>{{ $redeemProduct->point }}</td>
                <td>{{ $redeemProduct->receipt_no }}</td>
                <td>{{ $redeemProduct->serial_no }}</td>
            </tr>
        @endforeach
            <tr class="iTemplate" id="item-template-redeemProduct">
                <td class="item-fromDate-redeemProduct"></td>
                <td class="item-Description-redeemProduct"></td>
                <td class="item-qty-redeemProduct"></td>
                <td class="item-point-redeemProduct"></td>
                <td class="item-receipt_no-redeemProduct"></td>
                <td class="item-serialNo-redeemProduct"></td>
            </tr>
        </tbody>
    </table>
    <div class="row mart20">
        <div class="col-sm-12 text-right pagination-wrapper">
            <button class="btn btn-primary previous-btn pagination-btn disabled" data-page="{{ $paginatorredeemProducts->previous_page }}">
                Previous
            </button>
            Page 
            <span class="current-page-redeemProduct">
                {{ $paginatorredeemProducts->current_page }}
            </span>
             / 
            <span class="total-page">{{ $paginatorredeemProducts->total_pages }}</span>
            <button class="btn btn-primary next-btn pagination-btn{{ (!$paginatorredeemProducts->next_page) ? ' disabled' : '' }}" data-page="{{ $paginatorredeemProducts->next_page }}">
                Next
            </button>
        </div>
    </div>
</div>
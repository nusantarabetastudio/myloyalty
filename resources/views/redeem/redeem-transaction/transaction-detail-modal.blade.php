<div class="modal-header text-danger">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">Transaction Details</h4>
</div>
<div class="modal-body">
    <table class="table table-hover dataTable table-striped width-full" id="payment-table-container">
        <thead>
            <tr>
                <th>Product</th>
                <th>Qty</th>
                <th>Point</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
        @foreach($transactions as $transaction)
            <tr>
                <th>{{$transaction->redeem_product->description}}</th>
                <th>{{$transaction->qty}}</th>
                <th>{{$transaction->point}}</th>
                <th>{{$transaction->price}}</th>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    @include('partials._head')
    <link rel="stylesheet" href="{{ asset('assets/examples/css/pages/register-v3.css') }}">
</head>
<body class="page-register-v3 layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Page -->
<div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
     data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
        <div class="panel">
            <div class="panel-body">
                <div class="brand">
                    <img class="brand-img" width="300" src="{{ asset('assets/images/LogoSBL.png') }}" alt="MYLOYALTY">
                </div>
                <br>
                @if( Session::has('msg') )
                    <div class="alert alert-success">
                        {{ Session::get('msg') }}
                    </div>
                @elseif( Session::has('error') )
                    <div class="alert alert-warning">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <form method="post" action="{{ url('/verify/password') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group form-material floating{{ $errors->has('resetCode') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" name="resetCode" value="{{ $resetCode or old('resetCode') }}" />
                        @if ($errors->has('resetCode'))
                            <span class="help-block">
                                <strong>{{ $errors->first('resetCode') }}</strong>
                            </span>
                        @endif
                        <label class="floating-label">Reset Code</label>
                    </div>
                    <div class="form-group form-material floating{{ $errors->has('username') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" name="username" value="{{ old('username') }}" />
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                        <label class="floating-label">Username</label>
                    </div>
                    <div class="form-group form-material floating{{ $errors->has('newPassword') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" name="newPassword" />
                        @if ($errors->has('newPassword'))
                            <span class="help-block">
                                <strong>{{ $errors->first('newPassword') }}</strong>
                            </span>
                        @endif
                        <label class="floating-label">New Password</label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg margin-top-40">Submit</button>
                </form>
                <p>Have account already? Please go to <a href="{{ url('/login') }}">Sign In</a></p>
            </div>
        </div>
        <footer class="page-copyright page-copyright-inverse">
            <p>© {{ date('Y') }}. All RIGHT RESERVED.</p>
        </footer>
    </div>
</div>
<!-- End Page -->
<!-- Core  -->
@include('partials._js')
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>
</body>
</html>
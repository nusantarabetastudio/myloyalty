<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    @include('partials._head')
    <link rel="stylesheet" href="{{ asset('assets/examples/css/pages/forgot-password.css') }}">

</head>
<body class="page-forgot-password layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Page -->
<div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
     data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
        <h2>Forgot Your Password ?</h2>
        <p>Input your registered username to reset your password</p>
        <form method="post" action="{{ route('forgot-password.process')  }}" role="form" autocomplete="off">
            {{ csrf_field() }}
            @if( Session::has('msg') )
                <div class="alert alert-success">
                    {{ Session::get('msg') }}
                </div>
            @elseif( Session::has('error') )
                <div class="alert alert-warning">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="form-group form-material floating{{ $errors->has('username') ? ' has-error' : '' }}">
                <input type="text" class="form-control empty" id="username" name="username">
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
                <label class="floating-label" for="inputEmail">Your Username</label>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Reset Your Password</button>
            </div>
        </form>
        <footer class="page-copyright">
            <p>© {{ date('Y') }}. All RIGHT RESERVED.</p>
        </footer>
    </div>
</div>
<!-- End Page -->
@include('partials._js')
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>
</body>
</html>
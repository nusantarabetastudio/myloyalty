<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
    <head>
        @include('partials._head')
    </head>
    <body class="page-login-v3 layout-full">
        <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
            data-animsition-out="fade-out">
            >
            <div class="page-content vertical-align-middle">
                <div class="panel">
                    <div class="panel-body">
                        @if(Session::has('msg'))
                        <div class="alert alert-warning">
                            {{ Session::get('msg') }}
                        </div>
                        @elseif(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                        @endif
                        <div class="brand">
                            <img class="brand-img" width="300" src="{{ asset('assets/images/LogoSBL.png') }}" alt="MyLotalty">
                        </div>
                        <form method="POST" action="{{ url('login') }}" autocomplete="off">
                            {{ csrf_field() }}
                            <div class="form-group form-material floating{{ $errors->has('username') ? ' has-error' : '' }}">
                                <input type="text" class="form-control" name="username" value="{{ old('username') }}" id="username" autofocus>
                                <label class="floating-label">Username</label>
                                @if ($errors->has('username'))
                                <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group form-material floating{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" class="form-control" name="password" />
                                <label class="floating-label">Password</label>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group clearfix">
                                <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg pull-left">
                                    <input type="checkbox" id="inputCheckbox" name="remember">
                                    <label for="inputCheckbox">Remember me</label>
                                    <a class="pull-right" href="{{ url('forgot-password') }}">Forgot password?</a>
                                </div>
                                {{-- <a class="pull-right" href="/forgot-password">Forgot password?</a>--}}
                            </div>
                            <button type="submit" class="btn btn-primary btn-block btn-lg margin-top-40">Sign in</button>
                        </form>
                        {{--
                        <p>Still no account? Please go to <a href="/register">Sign up</a></p>
                        --}}
                    </div>
                </div>
                <footer class="page-copyright page-copyright-inverse">
                    <p>© {{ date('Y') }}. All RIGHT RESERVED.</p>
                </footer>
            </div>
        </div>
        @include('partials._js')
        <script>
            (function(document, window, $) {
                'use strict';
                var Site = window.Site;
                $(document).ready(function() {
                    Site.run();
                });
            })(document, window, jQuery);
        </script>
    </body>
</html>
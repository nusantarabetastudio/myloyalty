@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <br>
            <div class="row">
                <div class="col-sm-6">
                     <div class="example-wrap">
               
            
                <div class="form-group text-center">
                  <div class="input-group input-group-file">
                      <form class="form-horizontal" id="form" action="{{ url('earning/proccess') }}" method="POST" enctype="multipart/form-data">
                           {{ csrf_field() }}
                           <div class="row">
                               <div class="col-lg-6">
                                    <h1 class="example-title text-primary text-left">CSV Transaction File Upload</h1>
                               </div>
                               <div class="col-lg-6">
                                   <input type="file" name="csv_file">  <br/>
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-lg-6">
                                    <h1 class="example-title text-primary text-left">CSV Payment File Upload</h1>
                               </div>
                               <div class="col-lg-6">
                                   <input type="file" name="payment_file">  <br/>
                               </div>
                           </div>
                         
                          <p class="red-900">{!!$errors->first('csv_file')!!}</p>
                          <p class="red-900">{!!$errors->first('payment_file')!!}</p>
                            @if(Session::has('error'))
                            <p class="red-300 md-border-all">{!! Session::get('error') !!}</p>
                            @endif
                         <button type="submit" class="btn btn-block btn-success">Earning Proccess</button>
                      </form> 
                  </div>
                </div>
                <div class="form-group text-center">
                  <div class="input-group input-group-file">
                           
                  </div>
                </div>
                
              </div>
                </div>
            </div>

            <!-- Modal (Pop up when detail button clicked) -->
           
        </div>
    </div>
@endsection

@section('scripts')

@endsection
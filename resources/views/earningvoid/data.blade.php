@extends('layouts.loyalty')

@section('content')
    <div class="panel">
        <div class="panel-body">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <br>
            <div class="row">
                <div class="col-sm-12">
                     <div class="example-wrap">
                <h1 class="example-title">CSV File Upload</h1>
            
                <div class="form-group text-center">
                  <div class="input-group input-group-file">
                      <form class="form-horizontal" id="form" action="{{ url('earning/proccess') }}" method="POST" enctype="multipart/form-data">
                           {{ csrf_field() }}
                          <input type="file" name="csv_file">  <br/>
                          <p class="errors">{!!$errors->first('csv_file')!!}</p>
                            @if(Session::has('error'))
                            <p class="errors">{!! Session::get('error') !!}</p>
                            @endif
                         <button type="submit" class="btn btn-block btn-success">Earning Proccess</button>
                      </form> 
                  </div>
                </div>
                <div class="form-group text-center">
                  <div class="input-group input-group-file">
                           
                  </div>
                </div>
                
              </div>
                </div>
                
                 <div class="row">
                     <div class="col-sm-12">
@if(!empty($earning))                          
           <table class="tablesaw table-striped table-bordered table-hover" data-tablesaw-mode="swipe"
              data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap
              data-tablesaw-mode-switch>
               
               <thead>
                <tr>
                  <th></th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="1">Store Number </th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="2">tgl_transaksi</th>
                 
                  <th data-tablesaw-sortable-col data-tablesaw-priority="4">Waktu transaksi
</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="5">Terminal ID
</th>
                  <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Nomor member
</th>
 <th data-tablesaw-sortable-col data-tablesaw-priority="3">Nomor receipt
</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="6">Nomor artikel
</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="7">Total harga sebelum diskon (amount*qty)

</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="8">sign, + for sales, - for return/void


</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="9">Quantity barang, satuan harus dibagi 1000



</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="10">Merchandise category



</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="11">PPH / non PPH



</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="12">Total diskon



</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="13">Total setelah diskon



</th>
                  <th data-tablesaw-sortable-col data-tablesaw-priority="14">Index artikel



</th>
                 




                </tr>
              </thead>
              <tbody aria-relevant="all" aria-live="polite">
              @foreach($earning as $row)
                <tr class="odd">
                  <td></td>
                  <td>
                   {{$row[1- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[2- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[3- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[4- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[5- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[6- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[7- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[8- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[9- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[10- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[11- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[12- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[13- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[14- env('LOCALARRAY', 0)]}}
                  </td>
                  <td>
                   {{$row[15- env('LOCALARRAY', 0)]}}
                  </td>
                
                  
                  
                </tr>
               @endforeach 
              </tbody>
            </table>
          @else
            <p class="errors text-danger">Error! No Data Uploaded</p>
            
          @endif
          
          
                        
                     </div> 
                 </div>
                <br/>
                <br/>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <a href="{{url('/earning/result')}}" class="btn btn-block btn-primary">Earning Result</a>
                    </div>
                    
                </div>
                
            </div>

            <!-- Modal (Pop up when detail button clicked) -->
           
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

    <script type="text/javascript">
        var oTable;
        $(function () {
            oTable = $('#d-customer').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url('customer/data') }}',
                columns : [
                    { data: 'id', name: 'id', searchable: false, orderable: false},
                    { data: 'CUST_NAME', name: 'CUST_NAME'},
                    { data: 'mobile_number', name: 'mobile_number'},
                    { data: 'CUST_BARCODE', name: 'CUST_BARCODE'},
                    { data: 'CUST_IDTYPE', name: 'CUST_IDTYPE'},
                    { data: 'CUST_IDCARDS', name: 'CUST_IDCARDS'},
                    { data: 'action', name: 'action', searchable: false, orderable: false}
                ]
            });
        })
    </script>

@endsection
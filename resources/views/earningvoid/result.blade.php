@extends('layouts.loyalty')

@section('content')
<div class="panel">
    <div class="panel-body">
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <br>
        <div class="row">
            <div class="row">
            <div class="col-lg-12 text-right">
                    <!--<a href="{{url('helper/delete-earning-post')}}" class="btn btn-lg btn-danger">Clear Data Earning</a>-->
            </div>
                <div class="col-sm-12">
                    @if(!empty($post))                          
                    <table class="tablesaw table-striped table-bordered table-hover" data-tablesaw-mode="swipe"
                           data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap
                           data-tablesaw-mode-switch>
                        <thead>
                            <tr>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="1">Customer </th>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="2">Barcode</th>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="3">Date
                                </th>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="4">Time
                                </th>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="4">Receipt Number
                                </th>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="5">AMOUNT
                                </th>

                                <th data-tablesaw-sortable-col data-tablesaw-priority="6">POINT REGULAR
                                </th>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="7">POINT REWARD

                                </th>
                                <th data-tablesaw-sortable-col data-tablesaw-priority="8">POINT BONUS
                                </th>
                            </tr>
                        </thead>
                        <tbody aria-relevant="all" aria-live="polite">
                            @foreach($post as $row)
                            <tr class="odd">
                                <td>
                                    @php
                                        $customer = App\Models\Customer::where('CUST_BARCODE',$row->POS_BARCODE)->first();
                                    @endphp
                                    {{(isset($customer->CUST_NAME)?$customer->CUST_NAME:'-')}}
                                </td>
                                <td>
                                    {{$row->POS_BARCODE}}
                                </td>
                                <td>
                                    {{$row->POS_POST_DATE}}
                                </td>
                                <td>
                                    {{$row->POS_POST_TIME}}
                                </td>
                                <td>
                                    {{$row->POS_RECEIPT_NO}}
                                </td>
                                <td>
                                    <b>  {{number_format($row->POS_AMOUNT,2,',','.')}} </b>
                                </td>
                                <td>
                                    <b>  {{$row->POS_POINT_REGULAR}} </b>
                                </td>
                                <td>
                                    <b>  {{$row->POS_POINT_REWARD}} </b>
                                </td>
                                <td>
                                    <b>  {{$row->POS_POINT_BONUS}} </b>
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                    @else
                    <p class="errors text-danger">Error! No Data </p>
                    @endif
                    {{ $post->links() }}
                </div> 
            </div>
        </div>

        <!-- Modal (Pop up when detail button clicked) -->

    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('assets/global/js/components/datatables.js') }}"></script>
<script src="{{ asset('assets/global/vendor/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js') }}"></script>
<script src="{{ asset('assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/global/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
<script src="{{ asset('assets/global/vendor/datatables-tabletools/dataTables.tableTools.js') }}"></script>

<script type="text/javascript">
var oTable;
$(function () {
    oTable = $('#d-customer').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('customer / data') }}',
        columns: [
            {data: 'id', name: 'id', searchable: false, orderable: false},
            {data: 'CUST_NAME', name: 'CUST_NAME'},
            {data: 'mobile_number', name: 'mobile_number'},
            {data: 'CUST_BARCODE', name: 'CUST_BARCODE'},
            {data: 'CUST_IDTYPE', name: 'CUST_IDTYPE'},
            {data: 'CUST_IDCARDS', name: 'CUST_IDCARDS'},
            {data: 'action', name: 'action', searchable: false, orderable: false}
        ]
    });
})
</script>

@endsection
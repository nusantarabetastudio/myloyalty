@extends('layouts.loyalty')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/vendor/select2/select2.css') }}">
@endsection

@section('content')
    <div class="panel">
        <div class="panel-body">
            <div class="col-sm-6">
                @if(Session::has('msg'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ Session::get('msg') }}</li>
                        </ul>
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning">
                        <ul>
                            <li>{{ Session::get('error') }}</li>
                        </ul>
                    </div>
                @elseif (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('profile.update') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <input type="hidden" name="username" value="{{ Session::get('user')->USER_USERNAME }}">
                    <input type="hidden" name="roleId" value="{{ Session::get('user')->USER_ROLE_RECID }}">
                    <div class="form-group">
                        <label for="" class="control-label">First Name</label>
                        <input type="text" class="form-control" name="firstName" value="{{ $user->USER_FIRST_NAME }}">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Last Name</label>
                        <input type="text" class="form-control" name="lastName" value="{{ $user->USER_LAST_NAME }}">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ $user->USER_EMAIL }}">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Phone</label>
                        <input type="text" class="form-control" name="phone" value="{{ $user->USER_PHONE }}">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Address</label>
                        <input type="text" class="form-control" name="address" value="{{ $user->USER_ADDRESS }}">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Gender</label>
                        <select name="gender" id="" class="form-control select2">
                            <option value=""></option>
                            <option value="M" {{ $user->USER_GENDER == 'M' ? 'selected' :'' }}>Male</option>
                            <option value="F" {{ $user->USER_GENDER == 'F' ? 'selected' : '' }}>Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Update Profile</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/global/vendor/select2/select2.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            $('.select2').select2({
                'placeholder' : 'Choose One',
                'allowClear' : true,
                'width' : '100%'
            });
        });
    </script>
@endsection
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    @include('partials._head')
    @yield('css')
</head>
<body class="dashboard">
    @include('partials._menu')

    <!-- Page -->
    <div class="page animation">
        <div class="page-content">
        <!-- Panel Inline Form -->
            @if(isset($breadcrumbs))
                <ol class="breadcrumb">
                    <li><a href="{{ $parent_link }}">{{ isset($parent) ? $parent : '' }}</a></li>
                    <li class="active">{{ $breadcrumbs }}</li>
                </ol>
            @else

            @endif
        @yield('content')
        <!-- End Panel Inline Form -->
        </div>
    </div>
<!-- End Page -->
    <!-- Footer -->
    <footer class="site-footer">
        <div class="site-footer-legal">&copy; {{ date('Y') }}</div>
        {{--<div class="site-footer-right">
            Created By <a href="http://nusantarabetastudio.com" target="_blank">NusantaraBetaStudio</a>
        </div>--}}
    </footer>
@include('partials._js')

<script>
    $(document).ready(function() {
        jQuery.fn.preventDoubleSubmission = function() {
            $(this).on('submit',function(e){
                var $form = $(this);
                if ($form.data('submitted') === true) {
                    e.preventDefault();
                } else {
                    $form.data('submitted', true);
                }
            });

            return this;
        };
        $('form').preventDoubleSubmission();
    });
</script>
@yield('scripts')
</body>
</html>
<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Customers')->insert([
            'cust_pnum' => 1111,
            'cust_ptype' => 111,
            'cust_code' => 241,
            'cust_barcode' => 343,
            'cust_store' => GUID(),
            'cust_name' => 'DInan Riqal Firdaus',
            'cust_dob' => Carbon::now(),
            'cust_gender' => 1
        ]);
    }
}

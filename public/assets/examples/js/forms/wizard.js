/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
  });

  // Example Wizard Tabs
  // -------------------
  (function() {
    var defaults = $.components.getDefaults("wizard");
    var options = $.extend(true, {}, defaults, {
      step: '> .nav > li > a',
      onBeforeShow: function(step) {
        step.$element.tab('show');
      },
      classes: {
        step: {
          //done: 'color-done',
          error: 'color-error'
        }
      },
      onFinish: function() {
		/*profile*/
		var data = $('form').serialize();
		$.ajax({
		  type: "POST",
		  url: "http://localhost/myloyalty/public/customer",
		  data: data,
		  success: "berhasil",
		  dataType: "json"
		});
		alert('finish');
      },
      onNext: function(){
		  //next
      },
      buttonsAppendTo: '.tab-content'
    });

    $("#customer_new").wizard(options);
  })();

})(document, window, jQuery);

$(document).ready(function() {
    //display modal form for task editing
    $(document).on('click', '.open-modal', function () {
        var cust_id = $(this).val();
        console.log('called');
        $.get(url + '/' + cust_id, function (data) {
            //success data
            //console.log(data);
            $('#customer_id').val(data.id);
            $('#name').val(data.name);
            $('#address').val(data.address);
            $('#btn-save').val("update");

            $('#myModal').modal('show');
        })
    });

    //display modal form for creating new task
    $('#btn-add').on('click', function () {
        $('#btn-save').val("add");
        $('#frmCustomer').trigger("reset");
        $('#myModal').modal('show');
    });

    //delete task and remove it from list
    $(document).on("click", '.delete-task', function () {
        var customer_id = $(this).val();

        $.ajax({

            type: "DELETE",
            url: url + '/' + customer_id,
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
            success: function (data) {
                //console.log(data);

                $("#customer" + customer_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    //create new task / update existing task
    $("#btn-save").on("click", function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        e.preventDefault();

        var formData = $('#form-customer').serialize();

        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();

        var type = "POST"; //for creating new resource
        var customer_id = $('#customer_id').val();
        var my_url = url;

        if (state == "update") {
            type = "PUT"; //for updating existing resource
            my_url += '/' + customer_id;
        }

        //console.log(formData);

        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                //console.log(data);
                //
                // var customer = '<tr id="customer' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + data.address + '</td><td>' + data.created_at + '</td>';
                // customer += '<td><button class="btn btn-warning btn-xs btn-detail open-modal" value="' + data.id + '">Edit</button>';
                // customer += '<button class="btn btn-danger btn-xs btn-delete delete-task" value="' + data.id + '">Delete</button></td></tr>';
                console.log(data);


                if (state == "add") { //if user added a new record
                    $('#customer-list').append(customer);
                } else { //if user updated an existing record

                    $("#customer" + customer_id).replaceWith(customer);
                }

                $('#frmCustomer').trigger("reset");

                $('#myModal').modal('hide')
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});
<?php
namespace App\Traits;

use App\Models\Counter;
use Faker\Provider\Uuid;

trait CanCreateNewProptypeCounter
{
	public function addNewProptypeCounter($pnum, $ptype) {
        $proptypeCounter = new Counter();
        $proptypeCounter->id = strtoupper(Uuid::uuid());
        $proptypeCounter->pnum = $pnum;
        $proptypeCounter->ptype = $ptype;
        $proptypeCounter->customer_barcode_count = 0;
        $proptypeCounter->post_count = 0;
        $proptypeCounter->redeem_post_count = 0;
        $proptypeCounter->lucky_draw_count = 0;
        $proptypeCounter->active = 0;
        $proptypeCounter->update = 0;
        $proptypeCounter->save();

        return $proptypeCounter;
    }
}
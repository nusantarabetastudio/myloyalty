<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\EarningController as EarningController;
use App\Models\Preference;
use Excel,
    File,
    Config,
    Storage,
    Log;
use Faker\Provider\Uuid;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use League\Csv\Reader;
use Session;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Mail;
use App\Models\Admin\PostTemp;
use App\Models\Admin\PostTempPayment;
use App\Models\Admin\PostTemp_unclean;
use App\Models\Admin\PostTempPayment_unclean;

class EarningCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'earning:proses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command Will be Run Earning Point From CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function proses() {
        ini_set('memory_limit', '8069M');
        \Log::useFiles(storage_path() . '/logs/cron-earning-' . date('Y-m-d') . '.log');

        if (!app('hash')->check('true', Preference::where('key', 'hash')->first()->value)) {
            $this->line('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');
            \Log::info('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');
            return true;
        }

        $session = new Session();
        $earning = new EarningController($session);

        //  $directory = env('PATH_CSV', './../resources/uploads/store/');
        $ftp = Storage::disk('ftp');
        $local = Storage::disk('local');
        $ftpOriginalSales = 'MEMBER/OUT2/ORIGINAL/SALES';
        $ftpOriginalPayment = 'MEMBER/OUT2/ORIGINAL/PAYMENT';
        $ftpSourceFileSales = 'MEMBER/OUT2/SALES';
        $ftpSourceFilePayment = 'MEMBER/OUT2/PAYMENT';
        $ftpOkPath = 'MEMBER/OUT2/OK';
        $ftpFailPath = 'MEMBER/OUT2/FAIL';
        $localPath = 'uploads' . DIRECTORY_SEPARATOR . 'earning' . DIRECTORY_SEPARATOR;

        $testLocalMode = FALSE;

        if (!$testLocalMode) {
            //sales
            $this->writeLog('Get CSV Sales ......');
            $getFiles = $ftp->files($ftpSourceFileSales);
            $ftpCsv = [];
            //filter csv files
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            foreach ($ftpCsv as $row) {
                $this->writeLog('Moving Sales File ' . $row . ' from FTP to [' . $localPath . ']');
                $inputStream = $ftp->getDriver()->readStream($row);
                $transCSV = explode('/', $row);
                if ($local->put($transCSV[3], $inputStream))
                    $this->writeLog('Moving Sales file ' . $row . ' Completed!');
            }

            //payment
            $this->writeLog('Get CSV Payment ......');
            $getFiles = $ftp->files($ftpSourceFilePayment);
            $ftpCsv = [];
            //filter csv files
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            foreach ($ftpCsv as $row) {
                $this->writeLog('Moving Payment File ' . $row . ' from FTP to [' . $localPath . ']');
                $inputStream = $ftp->getDriver()->readStream($row);
                $transCSV = explode('/', $row);
                if ($local->put($transCSV[3], $inputStream))
                    $this->writeLog('Moving Payment file ' . $row . ' Completed!');
            }
        }

        $this->writeLog('---------------------------------------------------');
        $this->writeLog('EARNING POINT START AT '.date('d-m-Y H:i:s'));
        $this->writeLog('---------------------------------------------------');
        $directory = resource_path($localPath);
        $files = File::files($directory);
        $csv_file = [];
        $unmatchfiles = [
            'sales' => [],
            'payment' => [],
        ];

        session()->set("logsEarning", array());


        foreach ($files as $file) {


            $fileinfo = pathinfo($file);
            $filename = $fileinfo['filename'];
            $type_csv = explode('_', $filename);
            $array_file = [];
            if ($type_csv[2] == 'salesmember') {
                $found = false;
                foreach ($files as $file_payment) {
                    $fileinfo_payment = pathinfo($file_payment);
                    $filename_payment = $fileinfo_payment['filename'];
                    $type_csv_payment = explode('_', $filename_payment);
                    if ($type_csv_payment[2] == 'paymember' AND $type_csv[0] == $type_csv_payment[0] AND $type_csv[1] == $type_csv_payment[1] AND $type_csv[3] == $type_csv_payment[3]) {
                        $array_file['salesmember'] = $filename;
                        $array_file['paymember'] = $filename_payment;
                        $csv_file[] = $array_file;
                        $this->writeLog("File : " . $filename.'.csv' . ' & ' . $filename_payment.'.csv' . ' Match');
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $this->writeErrorLog("File : " . $filename.'.csv doesn\'t have match paymember csv');
                    array_push($unmatchfiles['sales'], $fileinfo['basename']);
                    try {
                        $local->delete('unclean/sales/'.$filename.'.csv');
						//$local->move($filename.'.csv','unclean/sales/'.$filename.'_'.date('ymdHis').'.csv');
                        $this->writeLog("File : " . $filename.'.csv' . ' move to unclean folder sales');
                    } catch (\Exception $e) {
                        $this->writeErrorLog('Failed Move '.$filename.'.csv'.' To unclean Folder With Error : ' . $e->getMessage());
                    }
                }
            }

            if ($type_csv[2] == 'paymember') {
                $found = false;
                foreach ($files as $file_salesmember) {
                    $fileinfo_salesmember = pathinfo($file_salesmember);
                    $filename_salesmember = $fileinfo_salesmember['filename'];
                    $type_csv_salesmember = explode('_', $filename_salesmember);
                    if ($type_csv_salesmember[2] == 'salesmember' AND $type_csv[0] == $type_csv_salesmember[0] AND $type_csv[1] == $type_csv_salesmember[1] AND $type_csv[3] == $type_csv_salesmember[3]) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $this->writeErrorLog("File : " . $filename.'.csv doesn\'t have match salesmember csv');
                    array_push($unmatchfiles['payment'], $fileinfo['basename']);
                    try {
                       $local->delete('unclean/payment/'.$filename.'.csv');
					   //$local->move($filename.'.csv','unclean/payment/'.$filename.'_'.date('ymdHis').'.csv');
                       $this->writeLog("File : " . $filename.'.csv' . ' move to unclean folder payment');
                    } catch (\Exception $e) {
                        $this->writeErrorLog('Failed Move '.$filename.'.csv'.' To unclean Folder With Error : ' . $e->getMessage());
                    }
                }
            }
        }

        DB::table('POSTTEMP')->delete();
        DB::table('POSTTEMPPAYMENT')->delete();
        DB::table('POSTTEMP_unclean')->delete();
        DB::table('POSTTEMPPAYMENT_unclean')->delete();

        foreach ($csv_file as $row) {
            $row_salesmember = $row['salesmember'];
            $this->writeLog('Begin ' . $row_salesmember . ' Process');
            $load_ress = Reader::createFromPath(resource_path($localPath . $row_salesmember . '.csv'));
            try {
                $earning->prosesTempSales($load_ress->fetch(), $row_salesmember);
                $this->writeLog('Finish ' . $row_salesmember . ' Process');
            } catch (\Exception $e) {
                $this->writeErrorLog('Failed Move to TempSales on CSV ' . $row_salesmember . ' with Error : ' . $e->getMessage() . '.');
            }


            $row_payment = $row['paymember'];
            $this->writeLog('Begin ' . $row_payment . ' Process');
            $load_ress_payment = Reader::createFromPath(resource_path($localPath . $row_payment . '.csv'));

            try {
                $earning->prosesTempPayment($load_ress_payment->fetch(), $row_payment);
                $this->writeLog('Finish ' . $row_payment . ' Process');
            } catch (\Exception $e) {
                $this->writeErrorLog('Failed Move to TempPayment on CSV ' . $row_payment . ' with Error : ' . $e->getMessage() . '.');
            }


            $this->writeLog("File : " . $row_salesmember . ' & ' . $row_payment . ' Earning Proses');

            try {
                $earning->earningPointPost(array(), array(), $row_salesmember, $row_payment, TRUE);
                $this->writeLog("\n----- Earning Point {$row_salesmember} & {$row_payment}  Finish ----- \n");
            } catch (\Exception $e) {
                $this->writeLog("\n Earning Point {$row_salesmember} & {$row_payment}  Failed With Error : {$e->getMessage()} \n");
            }
        }

        $this->writeLog('---------------------------------------------------');
        $this->writeLog('EARNING POINT FINISH AT '.date('d-m-Y H:i:s'));
        $this->writeLog('---------------------------------------------------');

        if(!$testLocalMode) {
            //Create Clean CSV sales;
            $this->writeLog('Create CSV Sales Clean');
            $this->createCleanCSVSales();
            $this->writeLog('Moving Clean Sales CSV to FTP..');

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 100) {
                try {
                    $files = $this->getFileFrom($local, 'clean/sales');
                    foreach ($files as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'clean/sales] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);
                        if ($ftp->put($ftpOkPath . '/SALES/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving Sales file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Sales Clean Failed with Error : ' . $e->getMessage());
                    $this->writeLog('Deleting ' . $ftpOkPath . '/SALES/' . $transCSV[2] . ' on FTP');
                    $ftp->delete($ftpOkPath . '/SALES/' . $transCSV[2]);
                    $this->writeLog($ftpOkPath . '/SALES/' . $transCSV[2] . ' has been deleted. Retrying...');
                    $attempt++;
                }
            }

            //Create Clean CSV payment;
            $this->writeLog('Create CSV Payment Clean');
            $this->createCleanCSVPayment();
            $this->writeLog('Moving Clean Payment CSV to FTP..');

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 100) {
                try {
                    $files = $this->getFileFrom($local, 'clean/payment');
                    foreach ($files as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'clean/payment] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);
                        if ($ftp->put($ftpOkPath . '/PAYMENT/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving Payment file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Payment Clean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $this->writeLog('Deleting ' . $ftpOkPath . '/PAYMENT/' . $transCSV[2] . ' on FTP');
                    $ftp->delete($ftpOkPath . '/PAYMENT/' . $transCSV[2]);
                    $this->writeLog($ftpOkPath . '/PAYMENT/' . $transCSV[2] . ' has been deleted. Retrying...');
                    $attempt++;
                }
            }

            //Create UnClean CSV sales;
            $this->writeLog('Create CSV Sales Unclean');
            $this->createUncleanCSVSales();
            $this->writeLog('Moving Unclean Sales CSV to FTP..');

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 100) {
                try {
                    $files = $this->getFileFrom($local, 'unclean/sales');
                    foreach ($files as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'unclean/sales] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);
                        if ($ftp->put($ftpFailPath . '/SALES/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Sales Unclean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $this->writeLog('Deleting ' . $ftpFailPath . '/SALES/' . $transCSV[2] . ' on FTP');
                    $ftp->delete($ftpFailPath . '/SALES/' . $transCSV[2]);
                    $this->writeLog($ftpFailPath . '/SALES/' . $transCSV[2] . ' has been deleted. Retrying...');
                    $attempt++;
                }
            }

            //Create UnClean CSV payment;
            $this->writeLog('Create CSV Unclean Payment');
            $this->createUncleanCSVPayment();
            $this->writeLog('Moving Unclean Payment CSV to FTP..');

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 100) {
                try {
                    $files = $this->getFileFrom($local, 'unclean/payment');
                    foreach ($files as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'unclean/payment] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);

                        if ($ftp->put($ftpFailPath . '/PAYMENT/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Payment Unclean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $this->writeLog('Deleting ' . $ftpFailPath . '/PAYMENT/' . $transCSV[2] . ' on FTP');
                    $ftp->delete($ftpFailPath . '/PAYMENT/' . $transCSV[2]);
                    $this->writeLog($ftpFailPath . '/PAYMENT/' . $transCSV[2] . ' has been deleted. Retrying...');
                    $attempt++;
                }
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 100) {
                try {
                    $this->writeLog('Moving FTP CSV Sales to Original Folder');
                    $getFiles = $ftp->files($ftpSourceFileSales);
                    foreach ($getFiles as $row) {
                        $fileInfo = pathinfo($row);
                        if (in_array($fileInfo['basename'], $unmatchfiles['sales'])) {
                            $this->writeLog('File Sales : ' . $fileInfo['basename'] .' doesn\'t have a matched payment csv. Abort Moving.');
                            continue;
                        }
                        if ($ftp->move($ftpSourceFileSales . '/' . $fileInfo['basename'], $ftpOriginalSales . '/' . $fileInfo['basename'])) {
                            $this->writeLog('Success Moving CSV '.$fileInfo['basename']);
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Original Sales Failed with Error : ' . $e->getMessage() . '. Retrying / Overwriting...');
                    $this->writeLog('Deleting ' . $ftpOriginalSales . '/' . $fileInfo['basename'] . ' on FTP');
                    $ftp->delete($ftpOriginalSales . '/' . $fileInfo['basename']);
                    $this->writeLog($ftpOriginalSales . '/' . $fileInfo['basename'] . ' has been deleted. Retrying...');
                    $attempt++;
                }
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 100) {
                try {
                    $this->writeLog('Moving FTP CSV Payment to Original Folder');
                    $getFiles = $ftp->files($ftpSourceFilePayment);
                    foreach ($getFiles as $row) {
                        $fileInfo = pathinfo($row);
                        if (in_array($fileInfo['basename'], $unmatchfiles['payment'])) {
                            $this->writeLog('File Payment : ' . $fileInfo['basename'] .' doesn\'t have a matched sales csv. Abort Moving.');
                            continue;
                        }
                        if( $ftp->move($ftpSourceFilePayment . '/' . $fileInfo['basename'], $ftpOriginalPayment . '/' . $fileInfo['basename'])) {
                            $this->writeLog('Success Moving CSV '.$fileInfo['basename']);
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV original Payment Failed with Error : ' . $e->getMessage() . '. Retrying / Overwriting...');
                    $this->writeLog('Deleting ' . $ftpOriginalPayment . '/' . $fileInfo['basename'] . ' on FTP');
                    $ftp->delete($ftpOriginalPayment . '/' . $fileInfo['basename']);
                    $this->writeLog($ftpOriginalPayment . '/' . $fileInfo['basename'] . ' has been deleted. Retrying...');
                    $attempt++;
                }
            }

            DB::table('POSTTEMP')->delete();
            DB::table('POSTTEMPPAYMENT')->delete();
            DB::table('POSTTEMP_unclean')->delete();
            DB::table('POSTTEMPPAYMENT_unclean')->delete();

            $this->writeLog('Delete Local Original CSV Sales & Payment');
            $getFiles = $local->files('/');
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
               $local->delete($fileInfo['basename']);
            }

            $this->writeLog('Delete Local Clean CSV Sales');
            $path = 'clean/sales/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

            $this->writeLog('Delete Local Clean CSV Payment');
            $path = 'clean/payment/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

            $this->writeLog('Delete Local Unclean CSV Sales');
            $path = 'unclean/sales/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

            $this->writeLog('Delete Local Unclean CSV Payment');
            $path = 'unclean/payment/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

        }

        $earning->notification();
    }

    public function getFileFrom($driver, $path)
    {
        $files = [];

        foreach ($driver->files($path) as $row) {
            $fileInfo = pathinfo($row);
            if ($fileInfo['extension'] == 'csv') {
                $files[] = $row;
            }
        }

        return $files;
    }

    public function writeLog($message) {
        $this->line('[' . date('d-m-Y H:i:s') . '] [Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);
        Log::info('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);

        return true;
    }

    public function writeErrorLog($message) {
        $this->error('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::error('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);
        session()->push('logsEarning', $message);
        return true;
    }

    private function createCleanCSVSales() {

        $files = PostTemp::select('FILE')->groupBy('FILE')->get();

        foreach($files as $row) {
            $handle = fopen('resources/uploads/earning/clean/sales/' . $row->FILE . '.csv', 'a');

            $csv = PostTemp::where('FILE',$row->FILE)->get();

            foreach ($csv as $row_csv) {
                 $data = [];
                 $data[] = $row_csv->STOREID;
                 $data[] = $row_csv->RCPDATE;
                 $data[] = $row_csv->RCPNBR;
                 $data[] = $row_csv->RCPTIME;
                 $data[] = $row_csv->TERMID;
                 $data[] = $row_csv->CARDID;
                 $data[] = $row_csv->INTCODE;
                 $data[] = $row_csv->ITEMTOTAL;
                 $data[] = $row_csv->ITEMTOTSIGN;
                 $data[] = $row_csv->ITEMQTY;
                 $data[] = $row_csv->DEPTCODE;
                 $data[] = $row_csv->VATCODE;
                 $data[] = $row_csv->DISCOUNT;
                 $data[] = $row_csv->TOTALAFTERDISC;
                 $data[] = $row_csv->ITMIDX;
                 $data[] = $row_csv->VOID_ITEM;
                 $data[] = $row_csv->POINT_BONUS;
                 $data[] = $row_csv->BIN;
                 $data[] = $row_csv->SPLIT_BONUS;
                 $data[] = $row_csv->MULTIPLE;
                //print_r($data); echo PHP_EOL;
                fputcsv($handle, $data);
            }
            fclose($handle);
        }
      }

     private function createUncleanCSVSales() {

         $files = PostTemp_unclean::select('FILE')->groupBy('FILE')->get();

        foreach($files as $row) {
            $handle = fopen('resources/uploads/earning/unclean/sales/' . $row->FILE . '.csv', 'a');

            $csv = PostTemp_unclean::where('FILE',$row->FILE)->get();

            foreach ($csv as $row_csv) {
                 $data = [];
                 $data[] = $row_csv->STOREID;
                 $data[] = $row_csv->RCPDATE;
                 $data[] = $row_csv->RCPNBR;
                 $data[] = $row_csv->RCPTIME;
                 $data[] = $row_csv->TERMID;
                 $data[] = $row_csv->CARDID;
                 $data[] = $row_csv->INTCODE;
                 $data[] = $row_csv->ITEMTOTAL;
                 $data[] = $row_csv->ITEMTOTSIGN;
                 $data[] = $row_csv->ITEMQTY;
                 $data[] = $row_csv->DEPTCODE;
                 $data[] = $row_csv->VATCODE;
                 $data[] = $row_csv->DISCOUNT;
                 $data[] = $row_csv->TOTALAFTERDISC;
                 $data[] = $row_csv->ITMIDX;
                 $data[] = $row_csv->VOID_ITEM;
                 $data[] = $row_csv->POINT_BONUS;
                 $data[] = $row_csv->BIN;
                 $data[] = $row_csv->SPLIT_BONUS;
                 $data[] = $row_csv->MULTIPLE;
                 $data[] = $row_csv->DESC;
               // print_r($data); echo PHP_EOL;
                fputcsv($handle, $data);
            }
            fclose($handle);
        }
    }

    private function createCleanCSVPayment() {

        $files = PostTempPayment::select('FILE')->groupBy('FILE')->get();

        foreach($files as $row) {
            $handle = fopen('resources/uploads/earning/clean/payment/' . $row->FILE . '.csv', 'a');

            $csv = PostTempPayment::where('FILE',$row->FILE)->get();

            foreach ($csv as $row_csv) {
                 $data = [];
                 $data[] = $row_csv->STOREID;
                 $data[] = $row_csv->RCPDATE;
                 $data[] = $row_csv->RCPTIME;
                 $data[] = $row_csv->TERMID;
                 $data[] = $row_csv->RCPNBR;
                 $data[] = $row_csv->CARDID;
                 $data[] = $row_csv->TENDNBR;
                 $data[] = $row_csv->ITEMTOTAL;
                 $data[] = $row_csv->BIN;
                 $data[] = $row_csv->POINT_BONUS;
                 $data[] = $row_csv->MULTIPLE;
                //print_r($data); echo PHP_EOL;
                fputcsv($handle, $data);
            }
            fclose($handle);
        }
    }

    private function createUncleanCSVPayment() {

       $files = PostTempPayment_unclean::select('FILE')->groupBy('FILE')->get();

        foreach($files as $row) {
            $handle = fopen('resources/uploads/earning/unclean/payment/' . $row->FILE . '.csv', 'a');

            $csv = PostTempPayment_unclean::where('FILE',$row->FILE)->get();

            foreach ($csv as $row_csv) {
                 $data = [];
                 $data[] = $row_csv->STOREID;
                 $data[] = $row_csv->RCPDATE;
                 $data[] = $row_csv->RCPTIME;
                 $data[] = $row_csv->TERMID;
                 $data[] = $row_csv->RCPNBR;
                 $data[] = $row_csv->CARDID;
                 $data[] = $row_csv->TENDNBR;
                 $data[] = $row_csv->ITEMTOTAL;
                 $data[] = $row_csv->BIN;
                 $data[] = $row_csv->POINT_BONUS;
                 $data[] = $row_csv->MULTIPLE;
                 $data[] = $row_csv->DESC;
               // print_r($data); echo PHP_EOL;
                fputcsv($handle, $data);
            }
            fclose($handle);
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->proses();
        $this->info("\n ------ \n Earning Already Proccess!");
    }

}

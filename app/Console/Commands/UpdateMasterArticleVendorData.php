<?php

namespace App\Console\Commands;
use App\Models\Master\Product;
use App\Models\Master\ProductVendor;
use App\Models\Master\Vendor;
use App\Models\Preference;
use CSV, File, Config, Storage, Log;
use Carbon\Carbon;
use Faker\Provider\Uuid;
use Illuminate\Console\Command;

class UpdateMasterArticleVendorData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-master:article-vendor {date : Date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update master Article Vendor data from CSV';

    private $delimiter = '|';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $masterDate = Carbon::parse($this->argument('date'));

        $this->writeLog('================================================================');
        $this->writeLog('Running ' . $this->signature . ' command..');
        $this->writeLog('================================================================');

        if (! app('hash')->check('true', Preference::where('key', 'hash')->first()->value)) {
            $this->writeErrorLog('Command ' . $this->signature . ': failed. Loyalty Program Expired!');
            return true;
        }

        $ftp = Storage::disk('ftp');
        $local = Storage::disk('uploads');
        $originalFileDirectory = 'master/original/';
        $originalFilePath = storage_path('app/uploads/' . $originalFileDirectory);
        $filename = 'CRM_MAPARTVEN_' . $masterDate->format('Ymd') . '.csv';
        $ftpSourceFile = 'MEMBER/MASTER/' . $filename;
        $ftpOkPath = 'MEMBER/MASTER/OK/' . $filename;
        $ftpFailPath = 'MEMBER/MASTER/FAIL/' . $filename;

        $this->writeLog('Searching for file ' . $filename . ' on FTP');
        if ($ftp->exists($ftpSourceFile)) {
            $this->writeLog('File ' . $filename . ' Found on FTP Server');
        } else {
            $this->writeErrorLog('File ' . $filename . ' not found on FTP Server!');
            return true;
        }

        $this->writeLog('Moving File ' . $filename . ' from FTP to [' . $originalFilePath . $filename . ']');
        $inputStream = $ftp->getDriver()->readStream($ftpSourceFile);
        if ($local->put($originalFileDirectory . $filename, $inputStream)) $this->writeLog('Moving file ' . $filename . ' Completed!');

        if (!File::exists($originalFilePath . $filename)) {
            $this->writeErrorLog('File [' . $originalFilePath . $filename . '] not found');
            return true;
        }

        $this->writeLog($this->description . ' begin..');
        $this->process($originalFilePath, $filename);
        $this->writeLog($this->description . ' complete!');

        $masterPath = storage_path('app/uploads/master/');
        clearstatcache();
        $cleanPath = $masterPath . 'ok/' . $filename;
        if (filesize($cleanPath) > 5) {
            $this->writeLog('Moving Ok File to FTP Server');
            $ftp->put($ftpOkPath, fopen($cleanPath, 'r+'));
        }
        $uncleanPath = $masterPath . 'fail/' . $filename;
        if (filesize($uncleanPath) > 5) {
            $this->writeLog('Moving Fail File to FTP Server');
            $ftp->put($ftpFailPath, fopen($uncleanPath, 'r+'));
        }
        $this->writeLog('Moving file to FTP Server completed!');

        $this->writeLog('================================================================');
        $this->writeLog('Command ' . $this->signature . ' has been processed successfully');
        $this->writeLog('================================================================');
        $this->writeLog('');
    }

    public function process($originalFilePath, $filename)
    {
        $masterPath = storage_path('app/uploads/master/');
        $cleanCsv = fopen($masterPath . 'ok/' . $filename, 'w+');
        $uncleanCsv = fopen($masterPath . 'fail/' . $filename, 'w+');

        CSV::load($originalFilePath . $filename)->setDelimiter($this->delimiter)->chunk(500, function($results) use($cleanCsv, $uncleanCsv) {
            foreach($results as $row) {
                try {
                    $datePost = $row[0];
                    $productCode = $row[1];
                    $vendorCode = $row[2];
                    $status = $row[3];

                    if (empty($productCode) || empty($vendorCode) || empty($status)) {
                        $this->insertTo($uncleanCsv, $row, 'Some field are empty');
                        continue;
                    }

                    if ($status == 'D') {
                        $product = Product::where('PRO_CODE', $productCode)->first();
                        $vendor = Vendor::where('VDR_CODE', $vendorCode)->first();
                        if (!$product) {
                            $this->insertTo($uncleanCsv, $row, 'Product Not Found on DB');
                            continue;
                        }
                        if (!$vendor) {
                            $this->insertTo($uncleanCsv, $row, 'Vendor Not Found on DB');
                            continue;
                        }

                        $productID = $product->PRO_RECID;
                        $vendorID = $vendor->VDR_RECID;

                        $existingProductVendor = ProductVendor::where('PROV_VDR_RECID', $vendorID)
                                                    ->where('PROV_PRO_RECID', $productID)
                                                    ->delete();
                        $this->insertTo($cleanCsv, $row);
                        continue;
                    }

                    $product = Product::where('PRO_CODE', $productCode)->first();
                    $vendor = Vendor::where('VDR_CODE', $vendorCode)->first();
                    if (!$product) {
                        $this->insertTo($uncleanCsv, $row, 'Product Not Found on DB');
                        continue;
                    }
                    if (!$vendor) {
                        $this->insertTo($uncleanCsv, $row, 'Vendor Not Found on DB');
                        continue;
                    }

                    $productID = $product->PRO_RECID;
                    $vendorID = $vendor->VDR_RECID;

                    $existingProductVendor = ProductVendor::where('PROV_VDR_RECID', $vendorID)
                                                ->where('PROV_PRO_RECID', $productID)
                                                ->first();

                    if (!$existingProductVendor) {
                        $productVendor = new ProductVendor;
                        $productVendor->PROV_RECID = Uuid::uuid();
                        $productVendor->PROV_VDR_RECID = $vendorID;
                        $productVendor->PROV_PRO_RECID = $productID;
                        $productVendor->save();
                    }
                    $this->insertTo($cleanCsv, $row);
                } catch (\Exception $e) {
                    $this->writeErrorLog('****************************************************************');
                    $this->writeErrorLog('ERROR on row: ' . implode($row, $this->delimiter));
                    $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
                    $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
                    $this->writeErrorLog('****************************************************************');
                    $this->writeErrorLog('');
                    $this->insertTo($uncleanCsv, $row, 'Unknown Error! MESSAGE: ' . $e->getMessage() . ' - FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
                }
            }
        });
        fclose($cleanCsv);
        fclose($uncleanCsv);

        return true;
    }
    
    public function insertTo($csv, array $rows, $uncleanError = null)
    {
        if ($uncleanError) {
            fputs($csv, implode($rows, $this->delimiter) . $this->delimiter . $uncleanError . "\n");
        } else {
            fputs($csv, implode($rows, $this->delimiter) . "\n");
        }

        return true;
    }

    public function writeLog($message)
    {
        $this->line('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::info($message);

        return true;
    }

    public function writeErrorLog($message)
    {
        $this->error('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::error($message);

        return true;
    }
}

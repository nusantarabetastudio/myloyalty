<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Log, Exception;

class UpdateAllMasterData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-master:all {date? : Date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update All Master Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $masterDate = $this->argument('date') ? Carbon::parse($this->argument('date'))->format('Y-m-d') 
                                              : Carbon::yesterday()->format('Y-m-d');
        Log::useFiles(storage_path().'/logs/cron-master-' . $masterDate . '.log');

        try {
            $this->call('update-master:brand', ['date' => $masterDate]);
        } catch(Exception $e) {
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('update-master:brand ERROR! Exception Message');
            $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
            $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('');
        }

        try {
            $this->call('update-master:merchandise', ['date' => $masterDate]);
        } catch(Exception $e) {
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('update-master:merchandise ERROR! Exception Message');
            $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
            $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('');
        }

        try {
            $this->call('update-master:vendor', ['date' => $masterDate]);
        } catch(Exception $e) {
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('update-master:merchandise ERROR! Exception Message');
            $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
            $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('');
        }

        try {
            $this->call('update-master:article', ['date' => $masterDate]);
        } catch(Exception $e) {
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('update-master:merchandise ERROR! Exception Message');
            $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
            $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('');
        }

        try {
            $this->call('update-master:article-vendor', ['date' => $masterDate]);
        } catch(Exception $e) {
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('update-master:merchandise ERROR! Exception Message');
            $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
            $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
            $this->writeErrorLog('****************************************************************');
            $this->writeErrorLog('');
        }
    }

    public function writeErrorLog($message)
    {
        $this->error('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::error($message);

        return true;
    }
}

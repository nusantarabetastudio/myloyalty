<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\ExpiredPointController as ExpiredPointController;
use App\Models\Admin\ZExpired as ZExpired;
use App\Models\Preference;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Session;

class PointExpiredCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pointexpired:proses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command Will be Run Point Expired Proses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function proses()
    {
        if (! app('hash')->check('true', Preference::where('key', 'hash')->first()->value)) {
            $this->line('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');
            \Log::info('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');
            return true;
        }


        echo "\n ----- \n Cek Datetime " . date('d/M/Y H:i:s');
        $setExpired = ZExpired::first();
        $start = strtotime($setExpired['START_ON']);
        $date = strtotime(date('Y-m-d'));
        $end = 0;


        echo "\n ----- \n ENDS {$setExpired['ENDS']} " . date('d/M/Y H:i:s');

        if ($setExpired['ENDS'] == 0) {
            $end = strtotime(date('Y-m-d',strtotime("+10 year",$date)));
            echo "\n ----- \n ENDS date :  $end  " . date('d/M/Y H:i:s');

        } elseif ($setExpired['ENDS'] == 1) {
            $end = strtotime("+{$setExpired['ENDS_AFTER']} month", $start);
        } elseif ($setExpired['ENDS'] == 2) {
            $end = strtotime($setExpired['ENDS_ON']);
        }

        echo "\n ----- \n date dibandingkan end $date < $end " . date('d/M/Y H:i:s');

        if ($date < $end) {


            echo "\n ----- \n REPEAT {$setExpired['REPEAT']} " . date('d/M/Y H:i:s');

            if ($setExpired['REPEAT'] == 0) {
                // jika monthly
                if ($start == $date) {
                    // set jika tanggal mulai expired = tanggal saat ini
                    $this->runExpired();
                }
                for ($i = 12; $i >= $setExpired['EVERY']; $i--) {
                    if (strtotime("+{$i} month", $start) == $date) {
                        // jika repeat
                        echo $setExpired['START_ON'];
                        $this->runExpired();
                    }

                }
            } elseif ($setExpired['REPEAT'] == 1) {
                // jika yearly
                if ($start == $date) {
                    // set jika tanggal mulai expired = tanggal saat ini
                    $this->runExpired();
              
                }
                for ($i = 1; $i <= $setExpired['EVERY']; $i++) {
                    if (strtotime("+{$i} year", $start) == $date) {
                        // jika repeat
                        $this->runExpired();
          
                    }
                }
            }
        }
    }

    private function runExpired()
    {
        $session = new Session();
        $pe = new ExpiredPointController($session);
        $pe->prosesPECLI();
        echo "\n ----- \n Point Expired  Run On " . date('d/M/Y H:i:s');
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->proses();
        $this->info('Point Expired Already Proccess!');
    }
}

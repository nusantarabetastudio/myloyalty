<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\EarningController as EarningController;
use App\Models\Preference;
use Excel,
    File,
    Config,
    Storage,
    Log;
use Faker\Provider\Uuid;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use League\Csv\Reader;
use Session;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Mail;

class EarningCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'earning:proses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command Will be Run Earning Point From CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function proses() {
        ini_set('memory_limit', '8069M');
        \Log::useFiles(storage_path() . '/logs/cron-earning-' . date('Y-m-d') . '.log');

        if (!app('hash')->check('true', Preference::where('key', 'hash')->first()->value)) {
            $this->line('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');
            \Log::info('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');
            return true;
        }

        $session = new Session();
        $earning = new EarningController($session);

        //  $directory = env('PATH_CSV', './../resources/uploads/store/');
        $ftp = Storage::disk('ftp');
        $local = Storage::disk('local');
        $ftpOriginalSales = 'MEMBER/OUT2/ORIGINAL/SALES';
        $ftpOriginalPayment = 'MEMBER/OUT2/ORIGINAL/PAYMENT';
        $ftpSourceFileSales = 'MEMBER/OUT2/SALES';
        $ftpSourceFilePayment = 'MEMBER/OUT2/PAYMENT';
        $ftpOkPath = 'MEMBER/OUT2/OK';
        $ftpFailPath = 'MEMBER/OUT2/FAIL';
        $localPath = 'uploads' . DIRECTORY_SEPARATOR . 'earning' . DIRECTORY_SEPARATOR;

        $testLocalMode = FALSE;

        if (!$testLocalMode) {
            //sales
            $this->writeLog('Get CSV Sales ......');
            $getFiles = $ftp->files($ftpSourceFileSales);
            $ftpCsv = [];
            //filter csv files
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            foreach ($ftpCsv as $row) {
                $this->writeLog('Moving Sales File ' . $row . ' from FTP to [' . $localPath . ']');
                $inputStream = $ftp->getDriver()->readStream($row);
                $transCSV = explode('/', $row);
                if ($local->put($transCSV[3], $inputStream))
                    $this->writeLog('Moving Sales file ' . $row . ' Completed!');
            }

            //payment
            $this->writeLog('Get CSV Payment ......');
            $getFiles = $ftp->files($ftpSourceFilePayment);
            $ftpCsv = [];
            //filter csv files
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            foreach ($ftpCsv as $row) {
                $this->writeLog('Moving Payment File ' . $row . ' from FTP to [' . $localPath . ']');
                $inputStream = $ftp->getDriver()->readStream($row);
                $transCSV = explode('/', $row);
                if ($local->put($transCSV[3], $inputStream))
                    $this->writeLog('Moving Payment file ' . $row . ' Completed!');
            }
        }

        $this->writeLog('Earning Point Proccess');
        $this->writeLog('=====================');
        $directory = resource_path($localPath);
        $files = File::files($directory);
        $csv_file = [];

        session()->set("logsEarning", array());


        foreach ($files as $file) {
            $fileinfo = pathinfo($file);
            $filename = $fileinfo['filename'];
            $type_csv = explode('_', $filename);
            $array_file = [];
            if ($type_csv[2] == 'salesmember') {
                $found = false;
                foreach ($files as $file_payment) {
                    $fileinfo_payment = pathinfo($file_payment);
                    $filename_payment = $fileinfo_payment['filename'];
                    $type_csv_payment = explode('_', $filename_payment);
                    if ($type_csv_payment[2] == 'paymember' AND $type_csv[0] == $type_csv_payment[0] AND $type_csv[1] == $type_csv_payment[1] AND $type_csv[3] == $type_csv_payment[3]) {
                        $array_file['salesmember'] = $filename;
                        $array_file['paymember'] = $filename_payment;
                        $csv_file[] = $array_file;
                        $this->writeLog("File : " . $filename.'.csv' . ' & ' . $filename_payment.'.csv' . ' Match');
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $this->writeErrorLog("File : " . $filename.'.csv' . ' dosn\'t have match paymember csv');
                    try {
                       $local->move($filename.'.csv','unclean/sales/'.$filename.'.csv');
                        $this->writeLog("File : " . $filename.'.csv' . ' move to unclean folder sales');
                    } catch (\Exception $e) {
                        $this->writeErrorLog('Failed Move '.$filename.'.csv'.' To unclean Folder With Error : ' . $e->getMessage());
                    }
                }
            }

            if ($type_csv[2] == 'paymember') {
                $found = false;
                foreach ($files as $file_salesmember) {
                    $fileinfo_salesmember = pathinfo($file_salesmember);
                    $filename_salesmember = $fileinfo_salesmember['filename'];
                    $type_csv_salesmember = explode('_', $filename_salesmember);
                    if ($type_csv_salesmember[2] == 'salesmember' AND $type_csv[0] == $type_csv_salesmember[0] AND $type_csv[1] == $type_csv_salesmember[1] AND $type_csv[3] == $type_csv_salesmember[3]) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $this->writeLog("File : " . $filename.'.csv' . ' dosn\'t have match salesmember csv');
                    try {
                       $local->move($filename.'.csv','unclean/payment/'.$filename.'.csv');
                       $this->writeLog("File : " . $filename.'.csv' . ' move to unclean folder payment');
                    } catch (\Exception $e) {
                        $this->writeErrorLog('Failed Move '.$filename.'.csv'.' To unclean Folder With Error : ' . $e->getMessage());
                    }
                }
            }
        }

        DB::table('POSTTEMP')->delete();
        DB::table('POSTTEMPPAYMENT')->delete();

        foreach ($csv_file as $row) {
            $row_salesmember = $row['salesmember'];
            $this->writeLog('Begin ' . $row_salesmember . ' Process');
            $load_ress = Reader::createFromPath(resource_path($localPath . $row_salesmember . '.csv'));
            try {
                $earning->prosesTempSales($load_ress->fetch(), $row_salesmember);
                $this->writeLog('Finish ' . $row_salesmember . ' Process');
            } catch (\Exception $e) {
                $this->writeErrorLog('Failed Move to TempSales on CSV ' . $row_salesmember . ' with Error : ' . $e->getMessage() . '.');
            }


            $row_payment = $row['paymember'];
            $this->writeLog('Begin ' . $row_payment . ' Process');
            $load_ress_payment = Reader::createFromPath(resource_path($localPath . $row_payment . '.csv'));

            try {
                $earning->prosesTempPayment($load_ress_payment->fetch(), $row_payment);
                $this->writeLog('Finish ' . $row_payment . ' Process');
            } catch (\Exception $e) {
                $this->writeErrorLog('Failed Move to TempPayment on CSV ' . $row_payment . ' with Error : ' . $e->getMessage() . '.');
            }


            $this->writeLog("File : " . $row_salesmember . ' & ' . $row_payment . ' Earning Proses');
            
             $earning->earningPointPost(array(), array(), $row_salesmember, $row_payment, TRUE);
            
            try {
               
                $this->writeLog("\n----- Earning Point {$row_salesmember} & {$row_payment}  Finish ----- \n");
            } catch (\Exception $e) {
                $this->writeLog("\n Earning Point {$row_salesmember} & {$row_payment}  Failed With Error : {$e->getMessage()} \n");
            }
        }

        if(!$testLocalMode) {
            $this->writeLog('Moving Clean Sales CSV to FTP..');
            
            //Create Clean CSV sales;
            
            $getFiles = $local->files('clean/sales');
            $ftpCsv = [];

            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 10) {
                try {
                    foreach ($ftpCsv as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'clean/sales] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);
                        if ($ftp->put($ftpOkPath . '/SALES/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving Sales file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Sales Clean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $attempt++;
                }
            }

            $this->writeLog('Moving Clean Payment CSV to FTP..');
            
            //Create Clean CSV payment;
            
            $getFiles = $local->files('clean/payment');
            $ftpCsv = [];
            //filter csv files
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 10) {
                try {
                    foreach ($ftpCsv as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'clean/payment] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);
                        if ($ftp->put($ftpOkPath . '/PAYMENT/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving Payment file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Payment Clean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $attempt++;
                }
            }

            $this->writeLog('Moving Unclean Sales CSV to FTP..');
            
            //Create UnClean CSV sales;
            
            $getFiles = $local->files('unclean/sales');
            $ftpCsv = [];
            //filter csv files
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 10) {
                try {
                    foreach ($ftpCsv as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'unclean/sales] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);
                        if ($ftp->put($ftpFailPath . '/SALES/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Sales Unclean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $attempt++;
                }
            }

            $this->writeLog('Moving Unclean Payment CSV to FTP..');
            
            //Create UnClean CSV payment;
            
            $getFiles = $local->files('unclean/payment');
            $ftpCsv = [];
            //filter csv files
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                if ($fileInfo['extension'] == 'csv') {
                    $ftpCsv[] = $row;
                }
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 10) {
                try {
                    foreach ($ftpCsv as $row) {
                        $this->writeLog('Moving File ' . $row . ' from [' . $localPath . 'unclean/payment] to FTP  ');
                        $inputStream = $local->getDriver()->readStream($row);
                        $transCSV = explode('/', $row);

                        if ($ftp->put($ftpFailPath . '/PAYMENT/' . $transCSV[2], $inputStream)) {
                            $this->writeLog('Moving file ' . $row . ' Completed!');
                        }
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Payment Unclean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $attempt++;
                }
            }

            $this->writeLog('Delete Local Original CSV Sales & Payment');
            $getFiles = $local->files('/');
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                $local->delete($fileInfo['basename']);
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 10) {
                try {
                    $this->writeLog('Moving FTP CSV Sales to Original Folder');
                    $getFiles = $ftp->files($ftpSourceFileSales);
                    foreach ($getFiles as $row) {
                        $fileInfo = pathinfo($row);
                        $ftp->move($ftpSourceFileSales . '/' . $fileInfo['basename'], $ftpOriginalSales . '/' . $fileInfo['basename']);
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Payment Unclean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $attempt++;
                }
            }

            $success = false;
            $attempt = 0;
            while (!$success && $attempt < 10) {
                try {
                    $this->writeLog('Moving FTP CSV Payment to Original Folder');
                    $getFiles = $ftp->files($ftpSourceFilePayment);
                    foreach ($getFiles as $row) {
                        $fileInfo = pathinfo($row);
                        $ftp->move($ftpSourceFilePayment . '/' . $fileInfo['basename'], $ftpOriginalPayment . '/' . $fileInfo['basename']);
                    }
                    $success = true;
                } catch (\Exception $e) {
                    $this->writeErrorLog('Send CSV Payment Unclean Failed with Error : ' . $e->getMessage() . '. Retrying...');
                    $attempt++;
                }
            }

            $this->writeLog('Delete Local Clean CSV Sales');
            $path = 'clean/sales/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

            $this->writeLog('Delete Local Clean CSV Payment');
            $path = 'clean/payment/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

            $this->writeLog('Delete Local Unclean CSV Sales');
            $path = 'unclean/sales/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

            $this->writeLog('Delete Local Unclean CSV Payment');
            $path = 'unclean/payment/';
            $getFiles = $local->files($path);
            foreach ($getFiles as $row) {
                $fileInfo = pathinfo($row);
                try {
                    $local->delete($path . $fileInfo['basename']);
                } catch (\Exception $e) {
                    $this->writeErrorLog('Delete file failed with error : ' . $e->getMessage());
                }
            }

        }

        $earning->notification();
    }

    public function writeLog($message) {
        $this->line('[' . date('d-m-Y H:i:s') . '] [Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);
        Log::info('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);

        return true;
    }

    public function writeErrorLog($message) {
        $this->error('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::error('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);
        session()->push('logsEarning', $message);
        return true;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->proses();
        $this->info("\n ------ \n Earning Already Proccess!");
    }

}



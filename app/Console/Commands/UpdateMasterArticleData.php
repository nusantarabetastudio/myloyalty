<?php

namespace App\Console\Commands;

use App\Models\Master\Brand;
use App\Models\Master\Lookup;
use App\Models\Master\Product;
use App\Models\Preference;
use CSV, File, Config, Storage, Log;
use Carbon\Carbon;
use Faker\Provider\Uuid;
use Illuminate\Console\Command;

class UpdateMasterArticleData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-master:article {date : Date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update master article data from CSV';

    private $delimiter = '|';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $masterDate = Carbon::parse($this->argument('date'));

        $this->writeLog('================================================================');
        $this->writeLog('Running ' . $this->signature . ' command..');
        $this->writeLog('================================================================');

        if (! app('hash')->check('true', Preference::where('key', 'hash')->first()->value)) {
            $this->writeErrorLog('Command ' . $this->signature . ': failed. Loyalty Program Expired!');
            return true;
        }

        $ftp = Storage::disk('ftp');
        $local = Storage::disk('uploads');
        $originalFileDirectory = 'master/original/';
        $originalFilePath = storage_path('app/uploads/' . $originalFileDirectory);
        $filename = 'CRM_MASART_' . $masterDate->format('Ymd') . '.csv';
        $ftpSourceFile = 'MEMBER/MASTER/' . $filename;
        $ftpOkPath = 'MEMBER/MASTER/OK/' . $filename;
        $ftpFailPath = 'MEMBER/MASTER/FAIL/' . $filename;

        $this->writeLog('Searching for file ' . $filename . ' on FTP');
        if ($ftp->exists($ftpSourceFile)) {
            $this->writeLog('File ' . $filename . ' Found on FTP Server');
        } else {
            $this->writeErrorLog('File ' . $filename . ' not found on FTP Server!');
            return true;
        }

        $this->writeLog('Moving File ' . $filename . ' from FTP to [' . $originalFilePath . $filename . ']');
        $inputStream = $ftp->getDriver()->readStream($ftpSourceFile);
        if ($local->put($originalFileDirectory . $filename, $inputStream)) $this->writeLog('Moving file ' . $filename . ' Completed!');

        if (!File::exists($originalFilePath . $filename)) {
            $this->writeErrorLog('File [' . $originalFilePath . $filename . '] not found');
            return true;
        }

        $this->writeLog($this->description . ' begin..');
        $this->process($originalFilePath, $filename);
        $this->writeLog($this->description . ' complete!');

        $masterPath = storage_path('app/uploads/master/');
        clearstatcache();
        $cleanPath = $masterPath . 'ok/' . $filename;
        if (filesize($cleanPath) > 5) {
            $this->writeLog('Moving Ok File to FTP Server');
            $ftp->put($ftpOkPath, fopen($cleanPath, 'r+'));
        }
        $uncleanPath = $masterPath . 'fail/' . $filename;
        if (filesize($uncleanPath) > 5) {
            $this->writeLog('Moving Fail File to FTP Server');
            $ftp->put($ftpFailPath, fopen($uncleanPath, 'r+'));
        }
        $this->writeLog('Moving file to FTP Server completed!');

        $this->writeLog('================================================================');
        $this->writeLog('Command ' . $this->signature . ' has been processed successfully');
        $this->writeLog('================================================================');
        $this->writeLog('');
    }

    public function process($originalFilePath, $filename)
    {
        $masterPath = storage_path('app/uploads/master/');
        $cleanCsv = fopen($masterPath . 'ok/' . $filename, 'w+');
        $uncleanCsv = fopen($masterPath . 'fail/' . $filename, 'w+');

        $units = Lookup::where('LOK_CODE', 'UNIT')->get();
        CSV::load($originalFilePath . $filename)->setDelimiter($this->delimiter)->chunk(500, function($results) use(&$units, $uncleanCsv, $cleanCsv) {
            foreach ($results as $row) {
                try {
                    $productCode = $row[0];
                    $productName = $row[1];
                    $unitName = $row[2];
                    $brandCode = $row[3];
                    $brandName = $row[4];
                    $merchandiseCode = $row[5];
                    $merchandiseName = $row[6];
                    $status = $row[7];

                    if (!empty($status) && !empty($productCode) && $status == 'D') {
                        $existingArticle = Product::where('PRO_CODE', $productCode)->delete();
                        $this->insertTo($cleanCsv, $row);
                        continue;
                    }

                    if (empty($productCode) || empty($productName) || empty($unitName) || empty($merchandiseCode) || empty($status)) {
                        $this->insertTo($uncleanCsv, $row, 'Some field are empty');
                        continue;
                    }

                    if (!is_numeric($merchandiseCode)) {
                        $this->insertTo($uncleanCsv, $row, 'Merchandise Code must be numeric');
                        continue;
                    }

                    $unit = $units->where('LOK_DESCRIPTION', $unitName)->first();
                    if (empty($unit)) {
                        $unit = new Lookup;
                        $unit->LOK_RECID = Uuid::uuid();
                        $unit->LOK_PNUM = env('PNUM');
                        $unit->LOK_PTYPE = env('PTYPE');
                        $unit->LOK_CODE = 'UNIT';
                        $unit->LOK_DESCRIPTION = $unitName;
                        $unit->LOK_ACTIVE = 1;
                        $unit->LOK_UPDATE = 1;
                        $unit->save();

                        $units->push($unit);
                    }
                    $unitId = $unit->LOK_RECID;

                    if (in_array($merchandiseCode, ['1X1', '2X1', '3X1', '4X1', '5X1'])) {
                        $merchandiseCode = '1X1';
                    }

                    if (!empty($brandCode)) {
                        $brand = Brand::where('code', $brandCode)->first();
                        $brandId = $brand ? $brand->id : null;
                    } else {
                        $brandId = null;
                    }

                    $existingArticle = Product::where('PRO_CODE', $productCode)->first();
                    if (empty($existingArticle)) {
                        $article = new Product;
                        $article->PRO_RECID = Uuid::uuid();
                        $article->PRO_PNUM = env('PNUM');
                        $article->PRO_PTYPE = env('PTYPE');
                        $article->PRO_MERCHANDISE = $merchandiseCode;
                        $article->PRO_CODE = $productCode;
                        $article->PRO_DESCR = $productName;
                        $article->PRO_UNIT = $unitId;
                        $article->brand_id = $brandId;
                        $article->save();
                    } else {
                        if (
                            $existingArticle->PRO_DESCR != $productName ||
                            $existingArticle->PRO_MERCHANDISE != $merchandiseCode ||
                            $existingArticle->PRO_UNIT != $unitId ||
                            $existingArticle->brand_id != $brandId
                        ) {
                            $existingArticle->PRO_MERCHANDISE = $merchandiseCode;
                            $existingArticle->PRO_DESCR = $productName;
                            $existingArticle->PRO_UNIT = $unitId;
                            $existingArticle->brand_id = $brandId;
                            $existingArticle->save();
                        }
                    }

                    $this->insertTo($cleanCsv, $row);
                } catch (\Exception $e) {
                    $this->writeErrorLog('****************************************************************');
                    $this->writeErrorLog('ERROR on row: ' . implode($row, $this->delimiter));
                    $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
                    $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
                    $this->writeErrorLog('****************************************************************');
                    $this->writeErrorLog('');
                    $this->insertTo($uncleanCsv, $row, 'Unknown Error! MESSAGE: ' . $e->getMessage() . ' - FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
                }
            }
        });
        fclose($cleanCsv);
        fclose($uncleanCsv);

        return true;
    }

    public function insertTo($csv, array $rows, $uncleanError = null)
    {
        if ($uncleanError) {
            fputs($csv, implode($rows, $this->delimiter) . $this->delimiter . $uncleanError . "\n");
        } else {
            fputs($csv, implode($rows, $this->delimiter) . "\n");
        }

        return true;
    }

    public function writeLog($message)
    {
        $this->line('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::info($message);

        return true;
    }

    public function writeErrorLog($message)
    {
        $this->error('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::error($message);

        return true;
    }
}

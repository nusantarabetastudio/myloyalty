<?php

namespace App\Console\Commands;

use App\Models\Customer;
use App\Models\Master\CardType;
use App\Models\Master\Lookup;
use App\Models\Master\Tenant;
use App\Models\Preference;
use App\Transformers\CustomerCodeTransformer;
use App\Transformers\Serializer\CustomSerializer;
use Carbon\Carbon;
use Excel, Log, DB, Config, File, Storage;
use Illuminate\Console\Command;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class BackupTxtCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup-txt:customer {date? : Date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup Customer to TXT with | delimited Format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->showHeader = false;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->argument('date') ? Carbon::parse($this->argument('date'))
                                        : Carbon::yesterday();
        Log::useFiles(storage_path().'/logs/backup-customer-' . $date->format('Y-m-d') . '.log');

        $this->writeLog('================================================================');
        $this->writeLog('Running ' . $this->signature . ' command..');
        $this->writeLog('================================================================');

        if (! app('hash')->check('true', Preference::where('key', 'hash')->first()->value)) {
            $this->line('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');
            Log::error('Command ' . $this->signature . ': failed. Loyalty Program Expired! Please contact Loyalto team.');

            return true;
        }

        $self = $this;
        $destinationPath = storage_path('app/uploads/backup/');
        Config::set('excel.csv.delimiter', '|');

        $filename = $date->format('Ymd') . '_Customer';
        Excel::create($filename, function($excel) use($self, $date) {
            $self->writeLog("Backing up customer data [" . $date->format('d M Y') . "]..");
            $excel->setTitle('Customer Backup');

            $excel->sheet(0, function($sheet) use($self, $date) {
                $customerCount = Customer::where(function($qry) use($date) {
                    $qry->whereHas('exchanges', function($qry) use($date) {
                        return $qry->where('Customer_Exchange.EXC_DATE', $date->format('Y-m-d'));
                    })
                    ->orWhereHas('logs', function($qry) use($date) {
                        return $qry->whereDate('customer_logs.date', '=', $date->format('Y-m-d'));
                    });
                })->count();
                $self->writeLog($customerCount . ' customer has changed');

                $customers = Customer::with(['exchanges' => function($qry) use($date) {
                    $qry->where('EXC_DATE', $date)->orderBy('EXC_DATE')->orderBy('EXC_TIME');
                }, 'rdPoint', 'addressKtp', 'homePhone', 'mobilePhone', 'businessPhone', 'email'])
                ->where(function($qry) use($date) {
                    $qry->whereHas('exchanges', function($qry) use($date) {
                        return $qry->where('Customer_Exchange.EXC_DATE', $date->format('Y-m-d'));
                    })
                    ->orWhereHas('logs', function($qry) use($date) {
                        return $qry->whereDate('customer_logs.date', '=', $date->format('Y-m-d'));
                    });
                })
                ->chunk(20000, function ($customers) use($self, $sheet, $date) {
                    $self->writeLog('Processing 20k record');
                    $customers = $self->transformData($customers, new CustomerCodeTransformer ($date->format('Y-m-d')));

                    $sheet->fromArray($customers, null, 'A1', false, $self->showHeader);
                });
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Store';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up store data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Tenant::all();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Member_Type';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up member type data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'MEMT')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_ID_Type';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up id type data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'TPID')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Marital';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up marital data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'MART')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Nationality';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up nationality data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'NATL')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Religion';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up religion data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'RELI')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Blood';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up blood data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'BLOD')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Occupation';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up occupation data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'OCCP')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Gender';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up gender data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'GEN')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Country';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up country data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'COTR')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Province';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up province data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'PROV')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_City';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up city data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'CITY')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Area';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up area data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = Lookup::where('LOK_CODE', 'AREA')->get();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $filename = $date->format('Ymd') . '_Card_Type';
        Excel::create($filename, function($excel) use($self) {
            $self->writeLog('Backing up card type data..');
            $excel->sheet(0, function($sheet) use($self) {
                $data = CardType::all();
                $sheet->fromArray($data, null, 'A1', false, $self->showHeader);
            });
        })->store('csv', $destinationPath);
        $this->changeToTxt($destinationPath, $filename);

        $this->writeLog('Backup Customer (TXT) Complete!');

        $this->moveBackupToFtp($destinationPath);

        $this->writeLog('================================================================');
        $this->writeLog('Command ' . $this->signature . ' has been processed successfully');
        $this->writeLog('================================================================');
        $this->writeLog('');
    }

    public function moveBackupToFtp($sourcePath)
    {
        $date = $this->argument('date') ? Carbon::parse($this->argument('date')) : Carbon::yesterday();

        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Customer.txt', false);
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Store.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Member_Type.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_ID_Type.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Marital.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Nationality.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Religion.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Blood.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Occupation.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Gender.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Country.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Province.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_City.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Area.txt');
        $this->moveToFtp($sourcePath, $date->format('Ymd') . '_Card_Type.txt');
        $this->writeLog('Moving file to FTP Server completed!');
    }

    public function moveToFtp($sourcePath, $filename, $deleteFile = true)
    {
        clearstatcache();
        $ftp = Storage::disk('ftp');
        $ftpDestinationPath = 'MEMBER/INB2/' . $filename;
        $filePath = $sourcePath . $filename;

        if (filesize($filePath) > 5) {
            $this->writeLog('Moving [' . $filePath . '] to FTP Server');
            $ftp->put($ftpDestinationPath, fopen($filePath, 'r+'));
        }

        if ($deleteFile) {
            File::delete($filePath);
        }

        return;
    }

    private function changeToTxt($destinationPath, $filename)
    {
        File::move($destinationPath . $filename . '.csv', $destinationPath . $filename . '.txt');

        return $this;
    }

    private function transformData($items, $transformerClass) {
        $fractal = new Manager;
        $fractal->setSerializer(new CustomSerializer());

        $resource = new Collection($items, $transformerClass);

        $transformedItems = $fractal->createData($resource)->toArray();

        return $transformedItems;
    }

    public function writeLog($message)
    {
        $this->line('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::info($message);

        return true;
    }

    public function writeErrorLog($message)
    {
        $this->error('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::error($message);

        return true;
    }
}

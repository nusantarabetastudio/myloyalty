<?php

namespace App\Console\Commands;

use App\Models\Master\Lookup;
use App\Models\Master\Merchandise;
use App\Models\Preference;
use CSV, File, Config, Storage, Log;
use Carbon\Carbon;
use Faker\Provider\Uuid;
use Illuminate\Console\Command;

class UpdateMasterMerchandiseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-master:merchandise {date : Date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update master marchandise data from CSV';

    private $delimiter = '|';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $masterDate = Carbon::parse($this->argument('date'));

        $this->writeLog('================================================================');
        $this->writeLog('Running ' . $this->signature . ' command..');
        $this->writeLog('================================================================');

        if (! app('hash')->check('true', Preference::where('key', 'hash')->first()->value)) {
            $this->writeErrorLog('Command ' . $this->signature . ': failed. Loyalty Program Expired!');
            return true;
        }

        $ftp = Storage::disk('ftp');
        $local = Storage::disk('uploads');
        $originalFileDirectory = 'master/original/';
        $originalFilePath = storage_path('app/uploads/' . $originalFileDirectory);
        $filename = 'CRM_MASMC_' . $masterDate->format('Ymd') . '.csv';
        $ftpSourceFile = 'MEMBER/MASTER/' . $filename;
        $ftpOkPath = 'MEMBER/MASTER/OK/' . $filename;
        $ftpFailPath = 'MEMBER/MASTER/FAIL/' . $filename;

        $this->writeLog('Searching for file ' . $filename . ' on FTP');
        if ($ftp->exists($ftpSourceFile)) {
            $this->writeLog('File ' . $filename . ' Found on FTP Server');
        } else {
            $this->writeErrorLog('File ' . $filename . ' not found on FTP Server!');
            return true;
        }

        $this->writeLog('Moving File ' . $filename . ' from FTP to [' . $originalFilePath . $filename . ']');
        $inputStream = $ftp->getDriver()->readStream($ftpSourceFile);
        if ($local->put($originalFileDirectory . $filename, $inputStream)) $this->writeLog('Moving file ' . $filename . ' Completed!');

        if (!File::exists($originalFilePath . $filename)) {
            $this->writeErrorLog('File [' . $originalFilePath . $filename . '] not found');
            return true;
        }

        $this->writeLog($this->description . ' begin..');
        $this->process($originalFilePath, $filename);
        $this->writeLog($this->description . ' complete!');

        $masterPath = storage_path('app/uploads/master/');
        clearstatcache();
        $cleanPath = $masterPath . 'ok/' . $filename;
        if (filesize($cleanPath) > 5) {
            $this->writeLog('Moving Ok File to FTP Server');
            $ftp->put($ftpOkPath, fopen($cleanPath, 'r+'));
        }
        $uncleanPath = $masterPath . 'fail/' . $filename;
        if (filesize($uncleanPath) > 5) {
            $this->writeLog('Moving Fail File to FTP Server');
            $ftp->put($ftpFailPath, fopen($uncleanPath, 'r+'));
        }
        $this->writeLog('Moving file to FTP Server completed!');

        $this->writeLog('================================================================');
        $this->writeLog('Command ' . $this->signature . ' has been processed successfully');
        $this->writeLog('================================================================');
        $this->writeLog('');
    }

    public function process($originalFilePath, $filename)
    {
        $masterPath = storage_path('app/uploads/master/');
        $cleanCsv = fopen($masterPath . 'ok/' . $filename, 'w+');
        $uncleanCsv = fopen($masterPath . 'fail/' . $filename, 'w+');

        CSV::load($originalFilePath . $filename)->setDelimiter($this->delimiter)->chunk(500, function($results) use($cleanCsv, $uncleanCsv) {
            foreach($results as $row) {
                try {
                    $merchandiseCode = $row[0];
                    $merchandiseType = $row[1];
                    $merchandiseName = $row[2];
                    $merchandiseStatus = $row[3];

                    if (empty($merchandiseCode) || empty($merchandiseType) || empty($merchandiseName) || empty($merchandiseStatus)) {
                        $this->insertTo($uncleanCsv, $row, 'Some field are empty');
                        continue;
                    }

                    if (!is_numeric($merchandiseCode)) {
                        $this->insertTo($uncleanCsv, $row, 'Merchandise Code must be numeric');
                        continue;
                    }

                    if ($merchandiseType == 1) {
                        $merchandiseType = 'DEPT';
                    } elseif ($merchandiseType == 2) {
                        $merchandiseType = 'DIVI';
                    } elseif ($merchandiseType == 3) {
                        $merchandiseType = 'CATG';
                    } elseif ($merchandiseType == 4) {
                        $merchandiseType = 'SUBC';
                    } elseif ($merchandiseType == 5) {
                        $merchandiseType = 'SEGM';
                    }

                    if (!empty($merchandiseStatus) && !empty($merchandiseCode) && !empty($merchandiseType) && $merchandiseStatus == 'D') {
                        $existingMcLookup = Lookup::where('LOK_MAPPING', $merchandiseCode)
                                                ->where('LOK_CODE', $merchandiseType)
                                                ->delete();

                        if ($merchandiseType == 'SEGM') {
                            Merchandise::where('MERC_CODE', $merchandiseCode)->delete();
                        }
                        $this->insertTo($cleanCsv, $row);
                        continue;
                    }

                    if (in_array($merchandiseCode, ['1X1', '2X1', '3X1', '4X1', '5X1'])) {
                        $merchandiseCode = '1X1';
                    }

                    $existingMcLookup = Lookup::where('LOK_MAPPING', $merchandiseCode)
                                                ->where('LOK_CODE', $merchandiseType)
                                                ->first();

                    if (empty($existingMcLookup)) {
                        $strlenLokMapping = strlen($merchandiseCode);
                        if ($strlenLokMapping == 2) {
                            $parent = substr($merchandiseCode, 0, 1);
                        } elseif ($strlenLokMapping == 1) {
                            $parent = null;
                        } else {
                            $parent = substr($merchandiseCode, 0, $strlenLokMapping - 2);
                        }

                        $mcLookup = new Lookup;
                        $mcLookup->LOK_RECID = Uuid::uuid();
                        $mcLookup->LOK_PNUM = env('PNUM');
                        $mcLookup->LOK_PTYPE = env('PTYPE');
                        $mcLookup->LOK_MAPPING = $merchandiseCode;
                        $mcLookup->LOK_MAPPING_PARENT = $parent;
                        $mcLookup->LOK_CODE = $merchandiseType;
                        $mcLookup->LOK_DESCRIPTION = $merchandiseName;
                        $mcLookup->save();

                        if ($merchandiseType == 'SEGM') {
                            $existingMc = Merchandise::where('MERC_CODE', $merchandiseCode)->first();
                            if (empty($existingMc)) {
                                $segmentCode = $merchandiseCode;
                                $segment = Lookup::where('LOK_MAPPING', $segmentCode)->where('LOK_CODE', 'SEGM')->first();
                                $subcategoryCode = substr($segmentCode, 0, 6);
                                $subcategory = Lookup::where('LOK_MAPPING', $subcategoryCode)->where('LOK_CODE', 'SUBC')->first();
                                $categoryCode = substr($segmentCode, 0, 4);
                                $category = Lookup::where('LOK_MAPPING', $categoryCode)->where('LOK_CODE', 'CATG')->first();
                                $divisionCode = substr($segmentCode, 0, 2);
                                $division = Lookup::where('LOK_MAPPING', $divisionCode)->where('LOK_CODE', 'DIVI')->first();
                                $departmentCode = substr($segmentCode, 0, 1);
                                $department = Lookup::where('LOK_MAPPING', $departmentCode)->where('LOK_CODE', 'DEPT')->first();

                                if (empty($segment) || empty($subcategory) || empty($category) || empty($division) || empty($department)) {
                                    $this->insertTo($uncleanCsv, $row, 'Segment not found or cannot find parent code');
                                    continue;
                                }

                                $merchandise = new Merchandise;
                                $merchandise->MERC_RECID = Uuid::uuid();
                                $merchandise->MERC_CODE = $merchandiseCode;
                                $merchandise->MERC_SEGMENT = $segment->LOK_RECID;
                                $merchandise->MERC_SUBCATEGORY = $subcategory->LOK_RECID;
                                $merchandise->MERC_CATEGORY = $category->LOK_RECID;
                                $merchandise->MERC_DIVISION = $division->LOK_RECID;
                                $merchandise->MERC_DEPARTMENT = $department->LOK_RECID;
                                $merchandise->MERC_ACTIVE = 1;
                                $merchandise->MERC_UPDATE = 1;
                                $merchandise->save();
                            }
                        }
                    } else {
                        if ($existingMcLookup->LOK_DESCRIPTION != $merchandiseName) {
                            $existingMcLookup->LOK_DESCRIPTION = $merchandiseName;
                            $existingMcLookup->save();
                        }
                    }
                    $this->insertTo($cleanCsv, $row);
                } catch (\Exception $e) {
                    $this->writeErrorLog('****************************************************************');
                    $this->writeErrorLog('ERROR on row: ' . implode($row, $this->delimiter));
                    $this->writeErrorLog('MESSAGE: ' . $e->getMessage());
                    $this->writeErrorLog('FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
                    $this->writeErrorLog('****************************************************************');
                    $this->writeErrorLog('');
                    $this->insertTo($uncleanCsv, $row, 'Unknown Error! MESSAGE: ' . $e->getMessage() . ' - FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine());
                }
            }
        });
        fclose($cleanCsv);
        fclose($uncleanCsv);

        return true;
    }
    
    public function insertTo($csv, array $rows, $uncleanError = null)
    {
        if ($uncleanError) {
            fputs($csv, implode($rows, $this->delimiter) . $this->delimiter . $uncleanError . "\n");
        } else {
            fputs($csv, implode($rows, $this->delimiter) . "\n");
        }

        return true;
    }

    public function writeLog($message)
    {
        $this->line('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::info($message);

        return true;
    }

    public function writeErrorLog($message)
    {
        $this->error('[' . date('d-m-Y H:i:s') . '] : ' . $message);
        Log::error($message);

        return true;
    }
}

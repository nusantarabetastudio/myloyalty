<?php
namespace App\Console\Commands;

use Carbon\Carbon;
use Faker\Provider\Uuid;
use App\Models\Admin\RedeemPost;
use App\Models\Admin\RedeemProduct;
use Illuminate\Console\Command;
use App\Models\RedeemProductLog;
use DB;

class CheckBookingRedeemExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redeem:check-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command Will be Run Point Expired Proses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->comment('--- Process Begins ---');
        $this->line('');
        $now = Carbon::now();
        $redeemPosts = RedeemPost::with(['details'])->where('expired_in', '<=', $now)->get();
        foreach($redeemPosts as $redeemPost) {
            DB::beginTransaction();
            try {
                $redeemPost->booking_code = null;
                $redeemPost->expired_in = null;
                $redeemPost->is_void = 1;
                $redeemPost->posted_by = 'BOOKING EXPIRED';
                $redeemPost->save();

                $post                       = new RedeemPost();
                $post->id                   = strtoupper(Uuid::uuid());
                $post->pnum                 = $redeemPost->pnum;
                $post->ptype                = $redeemPost->ptype;
                $post->customer_id          = $redeemPost->customer_id;
                $post->tenant_id            = $redeemPost->tenant_id;
                $post->post_date            = date('Y-m-d');
                $post->post_time            = date('H:i:s');
                $post->price                = -$redeemPost->price;
                $post->point                = -$redeemPost->point;
                $post->payment_id           = $redeemPost->payment_id;
                $post->posted_by            = 'BOOKING EXPIRED';
                $post->is_void              = 1;
                $post->void_date            = date('Y-m-d H:i:s');
                $post->void_redeem_post_id  = $redeemPost->id;
                $post->receipt_no           = $redeemPost->receipt_no .'-V';
                $post->update               = 1;
                $post->save();

                foreach($redeemPost->details as $detail) {
                    $transactionQty = $detail->qty;
                    $redeemProduct = RedeemProduct::find($detail->redeem_product_id);
                    if ($redeemProduct) {
                        $redeemProduct->qty += $transactionQty;
                        $redeemProduct->save();

                        $redeemProductLog = new RedeemProductLog;
                        $redeemProductLog->redeem_product_id = $redeemProduct->id;
                        $redeemProductLog->qty = $transactionQty;
                        $redeemProductLog->added_by = 'BOOKING EXPIRED';
                        $redeemProductLog->redeem_post_id = $redeemPost->id;
                        $redeemProductLog->serial_no = $detail->serial_no;
                        $redeemProductLog->added_at = Carbon::now();
                        $redeemProductLog->save();
                    }
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();

                $this->error('Booking Expired Failed! Please try again!');
            }
            $this->comment('Processed Booking Expired #' . $redeemPost->id .' complete');
            $this->line('');
        }
        $this->comment('--- Process Completed ---');
        return true;
    }
}
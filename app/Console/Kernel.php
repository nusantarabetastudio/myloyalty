<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\BackupTxtCustomer::class,
        Commands\UpdateMasterBrandData::class,
        Commands\UpdateMasterVendorData::class,
        Commands\UpdateMasterArticleData::class,
        Commands\UpdateMasterMerchandiseData::class,
        Commands\UpdateMasterArticleVendorData::class,
        Commands\UpdateAllMasterData::class,
        Commands\PointExpiredCommand::class,
        Commands\EarningCommand::class,
        Commands\CheckBookingRedeemExpiredCommand::class,

        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('pointexpired:proses')->dailyAt('00:01');
//        $schedule->command('earning:proses')->dailyAt('11:11');
//        $schedule->command('backup-txt:customer')->dailyAt('00:01');
//        $schedule->command('update-master:all')->dailyAt('00:01');
//        $schedule->command('redeem:check-expired')->everyMinute();
    }
}

<?php

namespace App\Providers;

use URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->environment() === 'production') {
            URL::forceSchema('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // \DB::listen(function ($query) { \Log::info("[$query->time ms] " . $query->sql, $query->bindings); });

        if (defined('PDO::DBLIB_ATTR_STRINGIFY_UNIQUEIDENTIFIER')) {
            $pdo = \DB::connection('sqlsrv')->getPdo();
            $pdo->setAttribute(\PDO::DBLIB_ATTR_STRINGIFY_UNIQUEIDENTIFIER, true);
        }
    }
}

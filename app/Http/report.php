<?php

//Customer Redemption Summary
Route::get('customer-redemption-summary/filter', 'Report\CustomerRedemptionSummaryController@showFilter')->name('report.customer-redemption-summary.filter');
Route::post('customer-redemption-summary', 'Report\CustomerRedemptionSummaryController@displayPdfReport')->name('report.customer-redemption-summary.display-pdf');
Route::post('customer-redemption-summary/download-pdf', 'Report\CustomerRedemptionSummaryController@downloadPdfReport')->name('report.customer-redemption-summary.download-pdf');
Route::post('customer-redemption-summary/download-excel', 'Report\CustomerRedemptionSummaryController@downloadExcelReport')->name('report.customer-redemption-summary.download-excel');

//Customer redemption Detail
Route::get('customer-redemption-detail/filter', 'Report\CustomerRedemptionDetailController@showFilter')->name('report.customer-redemption-detail.filter');
Route::post('customer-redemption-detail', 'Report\CustomerRedemptionDetailController@displayPdfReport')->name('report.customer-redemption-detail.display-pdf');
Route::post('customer-redemption-detail/download-pdf', 'Report\CustomerRedemptionDetailController@downloadPdfReport')->name('report.customer-redemption-detail.download-pdf');
Route::post('customer-redemption-detail/download-excel', 'Report\CustomerRedemptionDetailController@downloadExcelReport')->name('report.customer-redemption-detail.download-excel');

//Frequent transaction by Customer
Route::get('frequency-transaction-customer/filter', 'Report\FrequencyTransactionCustomerController@showFilter')->name('report.frequency-transaction-customer.filter');
Route::post('frequency-transaction-customer', 'Report\FrequencyTransactionCustomerController@displayPdfReport')->name('report.frequency-transaction-customer.display-pdf');
Route::post('frequency-transaction-customer/download-pdf', 'Report\FrequencyTransactionCustomerController@downloadPdfReport')->name('report.frequency-transaction-customer.download-pdf');
Route::post('frequency-transaction-customer/download-excel', 'Report\FrequencyTransactionCustomerController@downloadExcelReport')->name('report.frequency-transaction-customer.download-excel');

//Frequent transaction by Store
Route::get('frequency-transaction-store/filter', 'Report\FrequencyTransactionStoreController@showFilter')->name('report.frequency-transaction-store.filter');
Route::post('frequency-transaction-store', 'Report\FrequencyTransactionStoreController@displayPdfReport')->name('report.frequency-transaction-store.display-pdf');
Route::post('frequency-transaction-store/download-pdf', 'Report\FrequencyTransactionStoreController@downloadPdfReport')->name('report.frequency-transaction-store.download-pdf');
Route::post('frequency-transaction-store/download-excel', 'Report\FrequencyTransactionStoreController@downloadExcelReport')->name('report.frequency-transaction-store.download-excel');

//Frequent Redeem by Store
Route::get('frequency-redeem-store/filter', 'Report\FrequencyRedeemStoreController@showFilter')->name('report.frequency-redeem-store.filter');
Route::post('frequency-redeem-store', 'Report\FrequencyRedeemStoreController@displayPdfReport')->name('report.frequency-redeem-store.display-pdf');
Route::post('frequency-redeem-store/download-pdf', 'Report\FrequencyRedeemStoreController@downloadPdfReport')->name('report.frequency-redeem-store.download-pdf');
Route::post('frequency-redeem-store/download-excel', 'Report\FrequencyRedeemStoreController@downloadExcelReport')->name('report.frequency-redeem-store.download-excel');

// Top Customer Report
Route::get('top-customer/filter', 'Report\TopCustomerController@showFilter')->name('report.top-customer.filter');
Route::post('top-customer/display-pdf', 'Report\TopCustomerController@displayPdfReport')->name('report.top-customer.display-pdf');
Route::post('top-customer/download-pdf', 'Report\TopCustomerController@downloadPdfReport')->name('report.top-customer.download-pdf');
Route::post('top-customer/download-excel', 'Report\TopCustomerController@downloadExcelReport')->name('report.top-customer.download-excel');

//Point Balance Customer
Route::get('point-balance-customer/filter', 'Report\PointBalanceCustomerController@showFilter')->name('report.point-balance-customer.filter');
Route::post('point-balance-customer', 'Report\PointBalanceCustomerController@displayPdfReport')->name('report.point-balance-customer.display-pdf');
Route::post('point-balance-customer/download-pdf', 'Report\PointBalanceCustomerController@downloadPdfReport')->name('report.point-balance-customer.download-pdf');
Route::post('point-balance-customer/download-excel', 'Report\PointBalanceCustomerController@downloadExcelReport')->name('report.point-balance-customer.download-excel');
Route::post('point-balance-customer/download-csv', 'Report\PointBalanceCustomerController@downloadCSVReport')->name('report.point-balance-customer.download-csv');

//Customer Earning By Store Summary
Route::get('customer-earning-summary/filter', 'Report\CustomerEarningSummaryController@showFilter')->name('report.customer-earning-summary.filter');
Route::post('customer-earning-summary', 'Report\CustomerEarningSummaryController@displayPdfReport')->name('report.customer-earning-summary.display-pdf');
Route::post('customer-earning-summary/download-pdf', 'Report\CustomerEarningSummaryController@downloadPdfReport')->name('report.customer-earning-summary.download-pdf');
Route::post('customer-earning-summary/download-excel', 'Report\CustomerEarningSummaryController@downloadExcelReport')->name('report.customer-earning-summary.download-excel');

//Customer Earning
Route::get('customer-earning/filter', 'Report\CustomerEarningController@showFilter')->name('report.customer-earning.filter');
Route::post('customer-earning', 'Report\CustomerEarningController@displayPdfReport')->name('report.customer-earning.display-pdf');
Route::post('customer-earning/download-pdf', 'Report\CustomerEarningController@downloadPdfReport')->name('report.customer-earning.download-pdf');
Route::post('customer-earning/download-excel', 'Report\CustomerEarningController@downloadExcelReport')->name('report.customer-earning.download-excel');
Route::post('customer-earning/download-csv', 'Report\CustomerEarningController@downloadCSVReport')->name('report.customer-earning.download-csv');

//Customer Earning By Store Detail
Route::get('customer-earning-detail/filter', 'Report\CustomerEarningDetailController@showFilter')->name('report.customer-earning-detail.filter');
Route::post('customer-earning-detail', 'Report\CustomerEarningDetailController@displayPdfReport')->name('report.customer-earning-detail.display-pdf');
Route::post('customer-earning-detail/download-pdf', 'Report\CustomerEarningDetailController@downloadPdfReport')->name('report.customer-earning-detail.download-pdf');
Route::post('customer-earning-detail/download-excel', 'Report\CustomerEarningDetailController@downloadExcelReport')->name('report.customer-earning-detail.download-excel');

// Customer List Report
Route::get('customer-list/filter', 'Report\CustomerListController@showFilter')->name('report.customer-list.filter');
Route::post('customer-list', 'Report\CustomerListController@displayPdfReport')->name('report.customer-list.display-pdf');
Route::post('customer-list/download-pdf', 'Report\CustomerListController@downloadPdfReport')->name('report.customer-list.download-pdf');
Route::post('customer-list/download-excel', 'Report\CustomerListController@downloadExcelReport')->name('report.customer-list.download-excel');
Route::post('customer-list/download-csv', 'Report\CustomerListController@downloadCSVReport')->name('report.customer-list.download-csv');

// Card Exchange List Report
Route::get('card-exchange-list/filter', 'Report\CardExchangeListController@showFilter')->name('report.card-exchange-list.filter');
Route::post('card-exchange-list', 'Report\CardExchangeListController@displayPdfReport')->name('report.card-exchange-list.display-pdf');
Route::post('card-exchange-list/download-pdf', 'Report\CardExchangeListController@downloadPdfReport')->name('report.card-exchange-list.download-pdf');
Route::post('card-exchange-list/download-excel', 'Report\CardExchangeListController@downloadExcelReport')->name('report.card-exchange-list.download-excel');
Route::post('card-exchange-list/download-csv', 'Report\CardExchangeListController@downloadCSVReport')->name('report.card-exchange-list.download-csv');

//Report Point Expired
Route::get('point-expired/filter', 'Report\PointExpiredController@showFilter')->name('report.point-expired.filter');
Route::post('point-expired', 'Report\PointExpiredController@displayPdfReport')->name('report.point-expired.display-pdf');
Route::post('point-expired/download-pdf', 'Report\PointExpiredController@downloadPdfReport')->name('report.point-expired.download-pdf');
Route::post('point-expired/download-excel', 'Report\PointExpiredController@downloadExcelReport')->name('report.point-expired.download-excel');
<?php
Route::get('/', function () {
    return redirect('customer/search');
});


// Auth Section
Route::get('login', 'AuthController@login');
Route::post('login', 'AuthController@postLogin');
Route::get('logout', 'AuthController@logout');
Route::get('forgot-password', 'AuthController@getForgotPassword');
Route::post('forgot-password', 'AuthController@postForgotPassword')->name('forgot-password.process');
Route::get('verify/code', 'AuthController@getVerifyCode');
Route::post('verify/code', 'AuthController@postVerifyCode');
Route::get('verify/password', 'AuthController@getPasswordResetWithCode');
Route::post('verify/password', 'AuthController@postPasswordResetWithCode');

Route::group(['middleware' => 'auth'], function() {
    Route::get('profile', 'HomeController@profile')->name('profile');
    Route::put('profile', 'HomeController@updateProfile')->name('profile.update');
    // Change password section
    Route::get('change-password', 'HomeController@changePassword')->name('change-password');
    Route::post('change-password/{username}', 'HomeController@changePasswordUpdate')->name('change-password.update');
    //Expired Point
    Route::get('expired-point', 'Admin\ExpiredPointController@index')->name('expired-point.edit');
    Route::put('expired-point', 'Admin\ExpiredPointController@update')->name('expired-point.update');
    Route::post('expired-point-rollback', 'Admin\ExpiredPointController@rollback')->name('expired-point.rollback');
    Route::post('expired-point-proses', 'Admin\ExpiredPointController@prosesPE')->name('expired-point.prosesPE');

    // Instant Gift Section
    Route::resource('instant-gift', 'Admin\InstantGiftController', ['except' => 'show', 'destroy']);
    Route::post('instant-gift/destroy', 'Admin\InstantGiftController@destroy');
    Route::post('instant-gift/data', 'Admin\InstantGiftController@data');

    // Config section
    Route::group(['prefix' => 'config'], function () {
        // User
        Route::resource('user', 'Admin\UserManagementController', ['except' => 'show', 'destroy']);
        Route::post('user/destroy', 'Admin\UserManagementController@destroy');
        Route::get('user/data', 'Admin\UserManagementController@data');
        // Role
        Route::get('role/data', 'Admin\RolesManagementController@data');
        Route::resource('role', 'Admin\RolesManagementController', ['except' => 'destroy']);
        Route::delete('role/destroy', 'Admin\RolesManagementController@destroy');
        // Menu
        Route::get('menu/data', 'Admin\MenuManagementController@data');
        Route::resource('menu', 'Admin\MenuManagementController', ['except' => 'show', 'destroy']);
        Route::post('menu/destroy', 'Admin\MenuManagementController@destroy');
        //Setting
        Route::get('general-preference', 'Admin\PreferenceController@edit')->name('config.general-preference.edit');
        Route::put('general-preference/update' , 'Admin\PreferenceController@update')->name('config.general-preference.update');
        //Expired Point
        Route::get('expired-point', 'Admin\ExpiredPointController@index')->name('config.expired-point.edit');
        Route::put('expired-point', 'Admin\ExpiredPointController@update')->name('config.expired-point.update');
        // Email Template
        Route::resource('email-template', 'Admin\EmailTemplateController', ['except' => 'show', 'destroy']);
        // Sms Template
        Route::resource('sms-templete', 'Admin\SmsTemplateController', ['except' => 'show', 'destroy']);
    });
    // Pointo
    Route::group(['prefix' => 'pointo'], function () {

        // Pointo External Tenant
        Route::resource('external-tenant', 'Pointo\ExternalTenantController', ['except' => 'show', 'destroy']);
        Route::post('external-tenant/destroy', 'Pointo\ExternalTenantController@destroy')->name('pointo.external-tenant.destroy');
        Route::post('external-tenant/data', 'Pointo\ExternalTenantController@getData')->name('pointo.external-tenant.data');

        //Pointo Card Link
        Route::resource('internal-card-link', 'Pointo\InternalCardLinkController', ['except' => 'show', 'destroy']);
        Route::post('internal-card-link/destroy', 'Pointo\InternalCardLinkController@destroy')->name('pointo.internal-card-link.destroy');
        Route::post('internal-card-link/data', 'Pointo\InternalCardLinkController@getData')->name('pointo.internal-card-link.data');

        // Pointo Api Setting
        Route::get('api-setting', 'Pointo\ApiSettingController@index');
        Route::put('api-setting/update' , 'Pointo\ApiSettingController@update')->name('pointo.api-setting.update');
    });
    // customer section
    Route::group(['prefix' => 'customer'], function(){
        Route::get('/search', 'CustomerController@index')->name('customer.search');
        Route::get('/mass-delete', 'CustomerController@massDelete')->name('customer.mass-delete');
        Route::post('/process-mass-delete', 'CustomerController@processMassDelete')->name('customer.process-mass-delete');
        Route::get('/total', 'CustomerController@total')->name('customer.total');
        Route::get('/dataCustomer', 'CustomerController@getDataCustomer')->name('customer.dataCustomer');
        Route::get('/dataCustomerCard', 'CustomerController@getDataCustomercard')->name('customer.dataCustomercard');
        Route::get('/create', 'CustomerController@create')->name('customer.create');
        Route::post('/', 'CustomerController@store')->name('customer.store');

        //export data excel

        Route::get('/exportbystore', 'CustomerController@exportByStore')->name('customer.exportbystore');
        Route::get('/exportByExchange', 'CustomerController@getDataCustomercard')->name('customer.exportByExchange');
        Route::get('/exportBynewCustomer', 'CustomerController@getDataCustomer')->name('customer.exportBynewCustomer');

        Route::get('{customer_id}/edit', 'CustomerController@edit');
        Route::put('{customer_id}', 'CustomerController@update');
        Route::get('{customer_id}/detail', 'CustomerController@detail')->name('customer.detail');
        Route::get('{customer_id}/address', 'CustomerController@getAddresses');
        Route::get('{customer_id}/phone', 'CustomerController@getPhones');
        Route::get('{customer_id}/email', 'CustomerController@getEmails');
        Route::get('{customer_id}/interest', 'CustomerController@getInterests');
        Route::get('{customer_id}/exchange', 'CustomerController@customerExchange')->name('customer.detail.card-exchange');
        Route::post('{customer_id}/exchange/data', 'CustomerController@getCustomerexchange')->name('customer.detail.card-exchange.data');
        Route::get('{customer_id}/log', 'CustomerController@getCustomerLogs')->name('customer.detail.logs');
        Route::get('{customer_id}/points', 'CustomerController@getCustomerTransactionHistory')->name('customer.detail.point-history');
        Route::get('transaction/{id}/receipt', 'CustomerController@showTransactionDetailReceipt')->name('customer.detail.view-receipt');
        Route::get('transaction/{id}/receipt/{receipt_no}/product', 'CustomerController@showTransactionDetailProduct')->name('customer.detail.view-product');
        Route::get('/customerTransaction', 'CustomerController@getCustomerTransaction')->name('customer.customerTransaction');

        //Address
        Route::get('{customer_id}/address/{address_id}/edit', 'CustomerController@editAddress');
        Route::post('address', 'CustomerController@storeAddress')->name('customer.address.store');
        Route::put('address/{address_id}', 'CustomerController@updateAddress')->name('customer.address.update');
        Route::delete('address/destroy', 'CustomerController@deleteAddress')->name('customer.address.delete');
        //Phone
        Route::get('{customer_id}/phone/{phone_id}/edit', 'CustomerController@editPhone');
        Route::post('phone', 'CustomerController@storePhone')->name('customer.phone.store');
        Route::put('phone/{phone_id}', 'CustomerController@updatePhone')->name('customer.phone.update');
        Route::delete('phone/destroy', 'CustomerController@deletePhone')->name('customer.phone.delete');
        //Email
        Route::get('{customer_id}/email/{email_id}/edit', 'CustomerController@editEmail');
        Route::post('email', 'CustomerController@storeEmail')->name('customer.email.store');
        Route::put('email/{email_id}', 'CustomerController@updateEmail')->name('customer.email.update');
        Route::delete('email/destroy', 'CustomerController@deleteEmail')->name('customer.email.delete');
        //Interest
        Route::get('{customer_id}/interest/{interest_id}/edit', 'CustomerController@editInterest');
        Route::post('interest', 'CustomerController@storeInterest')->name('customer.interest.store');
        Route::put('interest/{interest_id}', 'CustomerController@updateInterest')->name('customer.interest.update');
        Route::delete('interest/destroy', 'CustomerController@deleteInterest')->name('customer.interest.delete');

        //Card Exchange
        Route::get('card-exchanges/search', 'CustomerController@getCardExchanges')->name('cardExchange.search');
        Route::get('card-exchanges', 'CustomerController@getCardExchanges');
        Route::get('card-exchange/{id}/edit', 'CustomerController@editCardExchange');
        Route::post('card-exchange/{id}', 'CustomerController@updateCardExchange')->name('customer.card-exchange.update');

        // Datatables
        Route::get('data', 'CustomerController@getData');
        Route::post('{customer_id}/address/data', 'CustomerController@getAddressData');
        Route::get('{customer_id}/phone/data', 'CustomerController@getPhoneData');
        Route::get('{customer_id}/email/data', 'CustomerController@getEmailData');
        Route::get('{customer_id}/interest/data', 'CustomerController@getInterestData');
        Route::get('{customer_id}/exchange/data', 'CustomerController@getExchangeData')->name('customer.detail.exchange.data');
        Route::post('card-exchanges/data', 'CustomerController@getCardExchangeData')->name('customer.card-exchanges.data');
        // Customer Group
        Route::resource('customer-group', 'CustomerGroupController', ['except' => 'show', 'destroy']);
        Route::post('customer-group/destroy', 'CustomerGroupController@destroy')->name('customer.customer-group.destroy');
        Route::post('customer-group/data', 'CustomerGroupController@getData')->name('customer.customer-group.data');
        Route::put('customer-group/{id}', 'CustomerGroupController@update')->name('customer.customer-group.update');
        Route::get('customer-group/{customerGroup}/customers', 'CustomerGroupController@getCustomerData')->name('customer.customer-group.customer-data');
    });

    Route::group(['prefix' => 'blast'], function() {
        // Customer Email Blast
        Route::get('email', 'EmailBlastController@index')->name('blast.email.index');
        Route::post('email-blast/store', 'EmailBlastController@store')->name('blast.email.store');
    });

    // transaction section.
    Route::group(['prefix' => 'transaction'], function(){
        // Search Customer Data
        Route::get('posting', 'TransactionController@getCustomerToPosting')->name('transaction.posting.customer');
        Route::get('void', 'TransactionController@getCustomerToVoid')->name('transaction.void.customer');
        Route::get('adjustment', 'TransactionController@getCustomerToAdjust')->name('transaction.adjustment.customer');
        Route::get('testing', 'TransactionController@getCustomerToTesting')->name('transaction.testing.customer');
        Route::get('manual', 'TransactionController@getCustomerToManual')->name('transaction.manual.customer');
        Route::get('manipulate', 'TransactionController@getCustomerToManipulate');
        // Search Transaction
        Route::get('{customer_id}/posting', 'TransactionController@getTransactionToPosting')->name('transaction.posting.detail');
        Route::post('{customer_id}/posting', 'TransactionController@postTransactionToPosting')->name('transaction.posting.process');
        Route::get('{customer_id}/void', 'TransactionController@getTransactionToVoid')->name('transaction.void.detail');
        Route::post('{customer_id}/void', 'TransactionController@postTransactionVoidProcess')->name('transaction.void.process');
        Route::get('{customer_id}/adjustment', 'TransactionController@getTransactionToAdjust')->name('transaction.adjustment.detail');
        Route::get('{customer_id}/manual', 'TransactionController@getTransactionToManual')->name('transaction.manual.detail');
        Route::post('manual', 'TransactionController@postTransactionManualProcess')->name('transaction.manual.process');
        Route::get('{customer_id}/testing', 'TransactionController@getTransactionToTesting')->name('transaction.testing.detail');
        Route::get('{customer_id}/manipulate', 'TransactionController@showManipulatePointForm')->name('transaction.manipulate.detail');
        Route::post('testing', 'TransactionController@postTransactionTestProcess')->name('transaction.testing.process');
        //Process
        Route::put('adjustment', 'TransactionController@postTransactionAdjustment')->name('transaction.adjustment.process');
        Route::post('{customer}/manipulate', 'TransactionController@manipulatePoint')->name('transaction.manipulate.process');

        // Datatables
        Route::get('posting-customer/data', 'TransactionController@getPostingCustomerData')->name('transaction.posting.customer.data');
        Route::get('void-customer/data', 'TransactionController@getVoidCustomerData')->name('transaction.void.customer.data');
        Route::get('adjustment-customer/data', 'TransactionController@getAdjustmentCustomerData')->name('transaction.adjustment.customer.data');
        Route::get('manual-customer/data', 'TransactionController@getManualCustomerData')->name('transaction.manual.customer.data');
        Route::get('testing-customer/data', 'TransactionController@getTestingCustomerData')->name('transaction.testing.customer.data');
        Route::get('manipulate-customer/data', 'TransactionController@getManipulateData')->name('transaction.manipulate.customer.data');
        Route::post('adjustment-transaction/{customerId}/data', 'TransactionController@getAdjustmentTransactionData')->name('transaction.adjustment.transaction.data');

        Route::post('void-transaction/{barcode}/data', 'TransactionController@getVoidTransactionData')->name('transaction.void.transaction.data');
        Route::post('posting-master-transaction/{barcode}/data', 'TransactionController@getPostingTransactionMasterData')->name('transaction.posting.transaction.master.data');
        Route::post('posting-detail-transaction/{barcode}/data', 'TransactionController@getPostingTransactionDetailData')->name('transaction.posting.transaction.detail.data');
        Route::post('posting-transaction-posting/data', 'TransactionController@getPostingTransactionPostingData')->name('transaction.posting.transaction.posting.data');

    });
    // Mandatory section
    Route::group(['prefix' => 'mandatory'], function(){
        Route::get('general', 'MandatoryController@generalIndex')->name('mandatory.general.index');
        Route::post('general', 'MandatoryController@generalStore')->name('mandatory.general.store');
        Route::get('general/{mandatoryId}/edit', 'MandatoryController@generalEdit')->name('mandatory.general.edit');
        Route::put('general/{mandatoryId}', 'MandatoryController@generalUpdate')->name('mandatory.general.update');

        Route::post('general/data', 'MandatoryController@getGeneralData')->name('mandatory.general.data');
        Route::delete('/', 'MandatoryController@destroy')->name('mandatory.destroy');

        Route::get('property', 'MandatoryController@propertyIndex')->name('mandatory.property.index');
        Route::post('property', 'MandatoryController@propertyStore')->name('mandatory.property.store');
        Route::get('property/{mandatoryId}/edit', 'MandatoryController@propertyEdit')->name('mandatory.general.edit');
        Route::get('property/data', 'MandatoryController@getpropertyData')->name('mandatory.property.data');
        Route::put('property/{mandatoryId}', 'MandatoryController@propertyUpdate')->name('mandatory.property.update');
    });
    // Redeem point formula section
    Route::group(['prefix' => 'redeem-point-formula'], function() {
        // Tebus Murah
        Route::resource('tebus-murah', 'RedeemPointFormula\TebusMurahController', ['except' => ['show', 'destroy']]);
        Route::post('tebus-murah/destroy', 'RedeemPointFormula\TebusMurahController@destroy')->name('redeem-point-formula.tebus-murah.destroy');

        // Tebus Gratis
        Route::resource('tebus-gratis', 'RedeemPointFormula\TebusGratisController', ['except' => ['show', 'destroy']]);
        Route::post('tebus-gratis/destroy', 'RedeemPointFormula\TebusGratisController@destroy')->name('redeem-point-formula.tebus-gratis.destroy');

        // Datatables
        Route::get('tebus-murah/data', 'RedeemPointFormula\TebusMurahController@getData')->name('redeem-point-formula.tebus-murah.data');
        Route::get('tebus-gratis/data', 'RedeemPointFormula\TebusGratisController@getData')->name('redeem-point-formula.tebus-gratis.data');
    });
    // master data section
    Route::group(['prefix' => 'master'], function() {
        Route::resource('vendor', 'Master\VendorController', ['except' =>'show', 'destroy']);
        Route::post('vendor/destroy', 'Master\VendorController@destroy');
        Route::get('vendor/data', 'Master\VendorController@getData');
        Route::get('vendor/{Id}/view', 'Master\VendorController@view');

        // Master data Brand
        Route::resource('brandData', 'Master\brandController', ['except' =>'show', 'destroy']);
        Route::post('brandData/data', 'Master\brandController@getData');
        Route::get('brandData/{id}/view', 'Master\brandController@view');

        Route::resource('department', 'Master\DepartmentController', [ 'except' => 'show', 'destroy']);
        Route::post('department/destroy', 'Master\DepartmentController@destroy');
        Route::post('department/data', 'Master\DepartmentController@getData');
        Route::get('department/{id}/edit', 'Master\DepartmentController@edit');

        Route::resource('division', 'Master\DivisionController', [ 'except' => 'show', 'destroy']);
        Route::post('division/destroy', 'Master\DivisionController@destroy');
        Route::post('division/data', 'Master\DivisionController@getData');

        Route::resource('category', 'Master\CategoryController', [ 'except' => 'show', 'destroy']);
        Route::post('category/destroy', 'Master\CategoryController@destroy');
        Route::post('category/data', 'Master\CategoryController@getData');

        Route::resource('subcategory', 'Master\SubcategoryController', [ 'except' => 'show', 'destroy']);
        Route::post('subcategory/destroy', 'Master\SubcategoryController@destroy');
        Route::post('subcategory/data', 'Master\SubcategoryController@getData');

        Route::resource('segment', 'Master\SegmentController', [ 'except' => 'show', 'destroy']);
        Route::post('segment/destroy', 'Master\SegmentController@destroy');
        Route::post('segment/data', 'Master\SegmentController@getData');

        Route::resource('country', 'Master\CountryController', [ 'except' => 'show', 'destroy']);
        Route::post('country/destroy', 'Master\CountryController@destroy');
        Route::get('country/data', 'Master\CountryController@getData');

        Route::resource('province', 'Master\ProvinceController', [ 'except' => 'show', 'destroy']);
        Route::post('province/destroy', 'Master\ProvinceController@destroy');
        Route::get('province/data', 'Master\ProvinceController@getData');

        Route::resource('city', 'Master\CityController', [ 'except' => 'show', 'destroy']);
        Route::post('city/destroy', 'Master\CityController@destroy');
        Route::get('city/data', 'Master\CityController@getData');

        Route::resource('area', 'Master\AreaController', [ 'except' => 'show', 'destroy']);
        Route::post('area/destroy', 'Master\AreaController@destroy');
        Route::get('area/data', 'Master\AreaController@getData');

        Route::resource('nationality', 'Master\NationalityController', [ 'except' => 'show', 'destroy']);
        Route::post('nationality/destroy', 'Master\NationalityController@destroy');
        Route::get('nationality/data', 'Master\NationalityController@getData');

        Route::resource('card-type/exchange', 'Master\CardTypeExchangeController', [ 'except' => 'show', 'destroy']);
        Route::post('card-type/exchange/destroy', 'Master\CardTypeExchangeController@destroy');
        Route::get('card-type/exchange/data', 'Master\CardTypeExchangeController@getData');

        Route::resource('card-type', 'Master\CardTypeController', [ 'except' => 'show', 'destroy']);
        Route::post('card-type/destroy', 'Master\CardTypeController@destroy');
        Route::get('card-type/data', 'Master\CardTypeController@getData');

        Route::resource('payment-method', 'Master\PaymentMethodController', [ 'except' => 'show', 'destroy']);
        Route::post('payment-method/destroy', 'Master\PaymentMethodController@destroy');
        Route::get('payment-method/data', 'Master\PaymentMethodController@getData');

        Route::resource('bank-card-type', 'Master\BankCardTypeController', [ 'except' => 'show', 'destroy']);
        Route::post('bank-card-type/destroy', 'Master\BankCardTypeController@destroy');
        Route::get('bank-card-type/data', 'Master\BankCardTypeController@getData');

        Route::resource('id-type', 'Master\IDTypeController', [ 'except' => 'show', 'destroy']);
        Route::post('id-type/destroy', 'Master\IDTypeController@destroy');
        Route::get('id-type/data', 'Master\IDTypeController@getData');

        Route::resource('marital', 'Master\MaritalController', [ 'except' => 'show', 'destroy']);
        Route::post('marital/destroy', 'Master\MaritalController@destroy');
        Route::get('marital/data', 'Master\MaritalController@getData');

        Route::resource('occupation', 'Master\OccupationController', [ 'except' => 'show', 'destroy']);
        Route::post('occupation/destroy', 'Master\OccupationController@destroy');
        Route::get('occupation/data', 'Master\OccupationController@getData');

        Route::resource('religion', 'Master\ReligionController', [ 'except' => 'show', 'destroy']);
        Route::post('religion/destroy', 'Master\ReligionController@destroy');
        Route::get('religion/data', 'Master\ReligionController@getData');

        Route::resource('blood', 'Master\BloodController', [ 'except' => 'show', 'destroy']);
        Route::post('blood/destroy', 'Master\BloodController@destroy');
        Route::get('blood/data', 'Master\BloodController@getData');

        Route::resource('interest', 'Master\InterestController', [ 'except' => 'show', 'destroy']);
        Route::post('interest/destroy', 'Master\InterestController@destroy');
        Route::get('interest/data', 'Master\InterestController@getData');

        Route::resource('measurement', 'Master\MeasurementController', [ 'except' => 'show', 'destroy']);
        Route::post('measurement/destroy', 'Master\MeasurementController@destroy');
        Route::get('measurement/data', 'Master\MeasurementController@getData');

        Route::resource('store', 'Master\StoreController', [ 'except' => 'show', 'destroy']);
        Route::post('store/destroy', 'Master\StoreController@destroy');
        Route::get('store/data', 'Master\StoreController@getData');
        Route::get('store/{id}/view', 'Master\StoreController@getView');

        Route::resource('store-category', 'Master\StoreCategoryController', [ 'except' => 'show', 'destroy']);
        Route::post('store-category/destroy', 'Master\StoreCategoryController@destroy');
        Route::get('store-category/data', 'Master\StoreCategoryController@getData');

        Route::resource('event', 'Master\EventController', [ 'except' => 'show', 'destroy']);
        Route::post('event/destroy', 'Master\EventController@destroy');
        Route::post('event/data', 'Master\EventController@getData');

        Route::resource('event-category', 'Master\EventCategoryController', [ 'except' => 'show', 'destroy']);
        Route::post('event-category/destroy', 'Master\EventCategoryController@destroy');
        Route::get('event-category/data', 'Master\EventCategoryController@getData');

        Route::resource('promotion', 'Master\PromotionController', [ 'except' => 'show', 'destroy']);
        Route::post('promotion/destroy', 'Master\PromotionController@destroy');
        Route::get('promotion/data', 'Master\PromotionController@getData');

        Route::resource('member-type', 'Master\MemberTypeController', [ 'except' => 'show', 'destroy']);
        Route::post('member-type/destroy', 'Master\MemberTypeController@destroy');
        Route::get('member-type/data', 'Master\MemberTypeController@getData');

        Route::resource('product', 'Master\ProductController', [ 'only' => 'index']);
        Route::post('product/destroy', 'Master\ProductController@destroy');
        Route::post('product/data', 'Master\ProductController@getData');
    });
    // Point formula
    Route::group(['prefix' => 'formula'], function(){
        // Regular
        Route::post('regular/data', 'Admin\RegularPointController@data');
        Route::resource('regular', 'Admin\RegularPointController', ['except' => 'show', 'destroy']);
        Route::delete('regular/destroy', 'Admin\RegularPointController@destroy');
        Route::get('regular/view/{id}', 'Admin\RegularPointController@view')->name('formula.regular.view');
        Route::post('regular/updateStatus', 'Admin\RegularPointController@updateStatus')->name('formula.regular.updateStatus');
        // Reward
        Route::post('reward/data', 'Admin\RewardPointController@data');
        Route::resource('reward', 'Admin\RewardPointController', ['except' => 'show', 'destroy' ]);
        Route::delete('reward/destroy', 'Admin\RewardPointController@destroy');
        Route::get('reward/view/{id}', 'Admin\RewardPointController@view')->name('formula.reward.view');
        Route::post('reward/updateStatus', 'Admin\RewardPointController@updateStatus')->name('formula.reward.updateStatus');
        // Bonus
        Route::post('bonus/data', 'Admin\BonusPointController@data');
        Route::resource('bonus', 'Admin\BonusPointController', ['except' => 'show', 'destroy' ]);
        Route::delete('bonus/destroy', 'Admin\BonusPointController@destroy');
        Route::get('bonus/view/{id}', 'Admin\BonusPointController@view')->name('formula.bonus.view');
        Route::post('bonus/updateStatus', 'Admin\BonusPointController@updateStatus')->name('formula.bonus.updateStatus');
    });
    // Earning
    Route::group(['prefix' => 'earning'], function(){
        Route::get('upload', 'Admin\EarningController@index');
        Route::get('result', 'Admin\EarningController@result');
        Route::post('proccess', 'Admin\EarningController@proccess');
        Route::get('filter', 'Admin\FilterEarningController@index');
    });
    // redeem product section
    Route::group(['prefix' => 'redeem'], function() {
        Route::resource('product', 'Redeem\RedeemProductController');
        Route::get('/', 'Redeem\RedeemProductController@getRedeemData')->name('redeem.product.data');
        Route::delete('/', 'Redeem\RedeemProductController@destroy')->name("redeem.product.destroy");
    });
    // inventory redeem product
    Route::group(['prefix' => 'redeem/inv'], function () {
        Route::resource('product-redeem', 'Redeem\InventoryProductRedeemController');
        Route::post('product-redeem/destroy', 'Redeem\InventoryProductRedeemController@destroy');
        Route::get('product-redeem/{Id}/addstock', 'Redeem\InventoryProductRedeemController@addStock')->name('redeem.inv.product-redeem.addstock');
        Route::get('/', 'Redeem\InventoryProductRedeemController@getData')->name('redeem.inv.product-redeem.data');
        Route::get('stock/data', 'Redeem\InventoryProductRedeemController@getStockData')->name('redeem.inv.product-redeem.stock');
    });
    // redeem Transaction
    Route::group(['prefix' => 'redeem/transaction'], function() {
        Route::get('/customer', 'Redeem\RedeemTransactionController@index')->name('redeem.transaction.customer');
        Route::get('/customer/data', 'Redeem\RedeemTransactionController@getCustomerData')->name('redeem.transaction.customer.data');
        Route::get('/customer/{customerId}', 'Redeem\RedeemTransactionController@redeem')->name('redeem');
        Route::get('/transaction/{transactionId}', 'Redeem\RedeemTransactionController@getTransactionDetails')->name('redeem.transaction.transaction.details');
        Route::post('/store', 'Redeem\RedeemTransactionController@store')->name('redeem.transaction.store');
        Route::post('/void', 'Redeem\RedeemTransactionController@void')->name('redeem.transaction.void');
        Route::get('/view-book', 'Redeem\RedeemTransactionController@viewBook')->name('redeem.transaction.view-book');
        Route::get('/view-book/detail-book/{bookCode}', 'Redeem\RedeemTransactionController@detailBook')->name('redeem.transaction.detail-book');
        Route::post('/cekValidate', 'Redeem\RedeemTransactionController@cekValidate')->name('redeem.transaction.cekValidate');
    });
    // API
    Route::group(['prefix' => 'api/v1'], function() {
        Route::get('country/{country_id}/province', 'Api\v1\LocationController@getProvinces')->name('api.location.province');
        Route::get('province/{province_id}/city', 'Api\v1\LocationController@getCities')->name('api.location.city');
        Route::get('city/{cityId}/area', 'Api\v1\LocationController@getAreas')->name('api.location.area');
        Route::get('pos/{pos_id}', 'Api\v1\PostController@getPost')->name('api.pos');

        Route::get('product', 'Api\v1\ProductController@index')->name('api.product.index');

        Route::group(['as' => 'api.master.'], function() {

            Route::get('department', 'Api\v1\MasterDataController@department')->name('department');
            Route::get('division', 'Api\v1\MasterDataController@division')->name('division');
            Route::get('category', 'Api\v1\MasterDataController@category')->name('category');
            Route::get('subCategory', 'Api\v1\MasterDataController@subCategory')->name('subcategory');
            Route::get('segment', 'Api\v1\MasterDataController@segment')->name('segment');
            Route::get('brand', 'Api\v1\MasterDataController@brand')->name('brand');
            Route::get('vendor', 'Api\v1\MasterDataController@vendor')->name('vendor');

        });
    });
    // user logs
    Route::group(['prefix' => 'user-logs'], function() {
        Route::get('/', 'UserLogsController@index');

        Route::get('/data', 'UserLogsController@getUserLogData')->name('user-logs.data');
    });
    // Helper
    Route::get('helper/delete-post-temp', 'HomeController@deletePostTemp');
    Route::get('helper/delete-earning-post', 'HomeController@deleteEarningPost');
    Route::get('helper/delete-testing-post-temp', 'HomeController@deleteTestingPostTemp')->name('destroy.testing.post.temp');

    Route::group(['prefix' => 'upload'], function () {
        Route::get('vendor', 'MasterUploadController@uploadVendor');
        Route::post('vendor', 'MasterUploadController@storeVendor');
    });
});


<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Preference;
use App\Models\UserLog;
use Illuminate\Http\Request;
use Session;
use Faker\Provider\Uuid;

class UserLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $UserLog = new UserLog();
        $UserLog->id = Uuid::uuid();
        $UserLog->user_id = Session::get('user')->USER_RECID;
        $UserLog->route = $request->path();
        $UserLog->IP = $request->ip();
        $UserLog->save();
        
        return $next($request);
    }
}

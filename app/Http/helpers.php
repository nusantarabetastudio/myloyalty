<?php

use Illuminate\Support\Facades\Storage;

function dateFormat($string)
{
    $date = explode('/', $string);
    if (!isset($date[2]) || !isset($date[1])) {
        return null;
    }
    $new_date = $date[2].'-'.$date[1].'-'.$date[0];

    return date('Y-m-d',strtotime($new_date));
}

function checkCode($code = 0)
{
    if($code < 10) {
        return '0'.++$code;
    } elseif($code > 9 && $code < 100) {
        return ++$code;
    }
    return $code;
}

function checkCode2($code = 0)
{
    if($code < 10) {
        return '00'.++$code;
    } elseif( $code > 9 && $code < 100) {
        return '0'.++$code;
    }
    return $code;
}

function getDepartmentCode($code)
{
    return substr($code, 0, 1);
}

function getDivisionCode($code)
{
    return substr($code, 0, 2);
}

function getCategoryCode($code)
{
    return substr($code, 0, 4);
}

function getSubCategoryCode($code)
{
    return substr($code, 0, 6);
}

function getDepartment($code)
{
    return substr($code, 0, 2);
}

function getDivision($code)
{
    return substr($code, 2, 3);
}

function getCategory($code)
{
    return substr($code, 5, 3);
}

function getSubCategory($code)
{
    return substr($code, 8, 3);
}

function getSegment($code)
{
    return substr($code, 11, 3);
}

function prefix($str, $length)
{
    return str_pad($str, $length, '0', STR_PAD_LEFT);
}

function thousandSeparator($number)
{
    return number_format($number, 0, '.', ',');
}

function isClosure($t)
{
    return is_object($t) && ($t instanceof Closure);
}

function isEmpty($string)
{
    return empty(trim($string));
}

function user()
{
    return Session::get('user');
}

function convertByte($size)
{
    $unit=array('b','kb','mb','gb','tb','pb');

    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function generateApiToken()
{
    return str_random(25) . time() . str_random(25);
}

function uploadToS3($image, $path)
{
    $imagePath = $path . '/' . strtoupper(str_random(10)) . '-' . time() . '.' . $image->getClientOriginalExtension();
    $streamData = fopen($image->getPathname(), 'rb');
    $imageUri = Storage::disk('s3')->put($imagePath, $streamData, 'public');

    return Storage::disk('s3')->url($imagePath);
}

function deleteImageFile($urlFile)
{
    if ($urlFile) {
        list($urlApp, $pathFile) = explode(url(''), $urlFile);
        $file = public_path() . $pathFile;
        if (File::isFile($file)) {
            File::delete($file);
            return true;
        }
    }

    return false;
}

function generateLeadingZero($string, $digit) {
    return sprintf("%0" . $digit . "d", $string);
}

function validationError($message = 'Validation Error!')
{
    while (DB::transactionLevel() > 0) {
        DB::rollBack();
    }

    return redirect()->back()->withInput(request()->except('_token'))->withErrors($message);
}
<?php

namespace App\Http\Controllers\Master;

use App\Models\Master\CardType;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Storage, Image;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class CardTypeController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;


    public function __construct()
    {
        //$this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        $this->pnum = Session::get('pnum');
        $this->ptype = Session::get('ptype');
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master Card Type';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

        return view('master.card-type.index', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'CARD_DESC' => 'required',
            'CARD_IMAGE' => 'image'
        ];
        $messages = [
            'CARD_DESC.required' => 'Description field is required.',
            'CARD_IMAGE.image' => 'Upload file must be image.'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', 'CARD_ACTIVE', 'CARD_IMAGE']);
        $cardType = new CardType();
        $cardType->CARD_RECID = Uuid::uuid();
        $cardType->CARD_PNUM = $this->pnum;
        $cardType->CARD_PTYPE = $this->ptype;
        $cardType->CARD_DESC = strtoupper($request->input('CARD_DESC'));
        $cardType->fill($post_data);
        $cardType->CARD_ACTIVE = 1;
        if($request->hasFile('CARD_IMAGE') ){
            $cardType->CARD_IMAGE = uploadToS3($request->file('CARD_IMAGE'), 'uploads/card_type');
        }
        $cardType->save();
        $request->session()->flash('success', 'Data has been saved');
        return redirect('master/card-type');

    }

    public function edit($id)
    {
        $data['card_type'] = CardType::findOrFail($id);
        return view('master.card-type.edit', $data);
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'CARD_DESC' => 'required',
            'CARD_IMAGE' => 'image'
        ];
        $messages = [
            'CARD_DESC.required' => 'Description field is required.',
            'CARD_IMAGE.image' => 'Upload file must be image.'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', 'CARD_ACTIVE', 'CARD_IMAGE']);
        $cardType = CardType::findOrFail($id);
        $cardType->fill($post_data);
        $cardType->CARD_DESC = strtoupper($request->input('CARD_DESC'));
        $request->input('CARD_ACTIVE') != null ? $cardType->CARD_ACTIVE = 1 : $cardType->CARD_ACTIVE = 0;
        if($request->hasFile('CARD_IMAGE') ){
            $cardType->CARD_IMAGE = uploadToS3($request->file('CARD_IMAGE'), 'uploads/card_type');
        }
        $cardType->save();
        $request->session()->flash('success', 'Data has been updated');
        return redirect('master/card-type');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $cardType = CardType::find($request->input('id'));
        if($cardType->CARD_IMAGE !== null){
            Storage::delete($cardType->CARD_IMAGE);
        }
        $cardType->delete();

        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/card-type');
    }

    public function getData()
    {
        $data = CardType::where('CARD_PNUM', Session::get('pnum'))->where('CARD_PTYPE', Session::get('ptype'))->get();

        $datatables = app('datatables')->of($data)
            ->addColumn('action', function($lookup) {
                $edit = '<a href="'.url('master/card-type/'.$lookup->CARD_RECID).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$lookup->CARD_RECID.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            });
        return $datatables->make(true);
    }

    function upload(Request $request, $parameter_name ,$save_name, $folder, $x = 500, $y = 500){
        $file = $request->file($parameter_name);
        $imageFileName = $save_name.'-'. rand(0,999).'-'.date('d-m-Y').'-.'.$file->getClientOriginalExtension();
        $destinationPath = $folder;
        $file->move($destinationPath, $imageFileName);
        Image::make( sprintf( $destinationPath .'/%s', $imageFileName) )->resize($x, $y)->save();
        return $imageFileName;
    }
}

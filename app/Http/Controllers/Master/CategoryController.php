<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Master\Lookup;
use App\Models\Master\Merchandise;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Category';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['code'] = Merchandise::where('MERC_CATEGORY', '<>', null)
            ->where('MERC_SUBCATEGORY', null)
            ->count();
        $data['departments'] = Merchandise::select(['MERC_CODE', 'MERC_DEPARTMENT'])
            ->where('MERC_DEPARTMENT', '<>', null)
            ->where('MERC_DIVISION', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['divisions'] = Merchandise::select(['MERC_CODE', 'MERC_DIVISION'])
            ->where('MERC_DIVISION', '<>' , null)
            ->where('MERC_CATEGORY', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        return view('master.category.index', $data);
    }

    public function create()
    {
        //;
    }

    public function store(Request $request)
    {
        $rules = [
            'MERC_CATEGORY' => 'required'
        ];
        $messages = [
            'MERC_CATEGORY.required' => 'Category field is required'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except('_token', 'MERC_CODE', 'MERC_DIVISION_CODE', 'MERC_DEPARTMENT_CODE', 'MERC_ACTIVE');
        $merc = new Merchandise();
        $merc->MERC_RECID = Uuid::uuid();

        if($request->input('MERC_ACTIVE') != null ) {
            $merc->MERC_ACTIVE = 1;
        } else {
            $merc->MERC_ACTIVE = 0;
        }
        $merc->MERC_CODE = getDepartment($request->input('MERC_DEPARTMENT_CODE')) . getDivision($request->input('MERC_DIVISION_CODE')) . $request->input('MERC_CODE');
        $merc->fill($post_data);
        $merc->save();

        $request->session()->flash('success', 'Data has been saved');
        return redirect('master/category');

    }

    public function edit($id)
    {
        $data['merc'] = Merchandise::findOrFail($id);
        $data['code'] = Merchandise::where('MERC_CATEGORY', '<>' , null)
            ->where('MERC_SUBCATEGORY', null)
            ->count();
        $data['departments'] = Merchandise::select(['MERC_CODE', 'MERC_DEPARTMENT'])
            ->where('MERC_DEPARTMENT', '<>', null)
            ->where('MERC_DIVISION', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['divisions'] = Merchandise::select(['MERC_CODE', 'MERC_DIVISION'])
            ->where('MERC_DIVISION', '<>', null)
            ->where('MERC_CATEGORY', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        return view('master.category.edit', $data);
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'MERC_CATEGORY' => 'required'
        ];
        $messages = [
            'MERC_CATEGORY.required' => 'Category field is required'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', '_method', 'MERC_DEPARTMENT_CODE', 'MERC_DIVISION_CODE']);
        $merchandise = Merchandise::findOrFail($id);
        $merchandise->fill($post_data);
        $code = getDepartment($request->input('MERC_DEPARTMENT_CODE')) . getDivision($request->input('MERC_DIVISION_CODE')) . $request->input('MERC_CODE');
        if($merchandise->MERC_CODE !== $code) {
            $merchandise->MERC_CODE = $code;
        }
        if($request->input('MERC_ACTIVE') == 'on') {
            $merchandise->MERC_ACTIVE = 1;
        } else {
            $merchandise->MERC_ACTIVE = 0;
        }
        $merchandise->save();

        $request->session()->flash('success', 'Data has been updated');
        return redirect('master/category');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        Merchandise::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/category');
    }

    public function getData()
    {
        $category = Lookup::with(['division.department'])->where('LOK_CODE', 'CATG');

        $datatables = app('datatables')->of($category);
        return $datatables->make(true);
    }
}

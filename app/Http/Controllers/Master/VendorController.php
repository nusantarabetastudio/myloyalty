<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Master\Vendor;
use Faker\Provider\Uuid;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;


class VendorController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct()
    {
        $this->parent = 'Master Data';
        $this->parent_link = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit', 'view']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Vendor';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        return view('master.vendor.index', $data);
    }

    public function create()
    {
        return view('master.vendor.create');
    }

    public function store(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'VDR_NAME' => 'required'
        ]);

        $messages = Validator::make($request->all(), [
            'VDR_NAME.required' => 'Vendor Name field is required'
        ]);
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', 'VDR_ACTIVE']);
        $vendor = new Vendor();
        $vendor->VDR_RECID = Uuid::uuid();
        $vendor->fill($post_data);
        $vendor->VDR_ACTIVE =1;
        $vendor->VDR_UPDATE =0;
//            if($request->input('VDR_ACTIVE') == 'on') {
//                $vendor->VDR_ACTIVE = 1;
//            } else {
//                $vendor->VDR_ACTIVE = 0;
//            }
        $vendor->save();

        $request->session()->flash('success', 'Data has been saved.');
        return redirect('master/vendor');

    }

    public function edit($id)
    {
        $vendor = Vendor::findOrFail($id);
        $data['vendor'] = $vendor;
        return view('master.vendor.edit', $data);
    }

    public function view($id)
    {
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'vendor/'.$id.'/product?page=1&limit=10000';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['products'] = $contents->DATA->products;
            $data['vendors'] = $contents->DATA->vendor;
             // dd($data['vendors']);
            return view('master.vendor.view', $data);
        }
        else {
            return redirect()->back();
        }
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = Validator::make($request->all(), [
            'VDR_NAME' => 'required'
        ]);

        $messages = Validator::make($request->all(), [
            'VDR_NAME.required' => 'Vendor Name field is required'
        ]);
        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', 'VDR_ACTIVE']);
        $vendor = Vendor::findOrFail($id);
        $vendor->fill($post_data);
        $vendor->VDR_ACTIVE =1;
        $vendor->VDR_UPDATE =1;
        /*if($request->input('VDR_ACTIVE') == 'on') {
            $vendor->VDR_ACTIVE = 1;
        } else {
            $vendor->VDR_ACTIVE = 0;
        }*/
        $vendor->save();
        $request->session()->flash('success', 'Data has been updated.');
        return redirect('master/vendor');

    }

    public function destroy(Request $request)
    {
        Vendor::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/vendor');
    }

    public function getData()
    {
        $vendor = Vendor::all();

        $datatables = app('datatables')->of($vendor)
            ->addColumn('action', function($vendor) {
                $view = '<a href="'.url('master/vendor/'.$vendor->VDR_RECID).'/view" style="text-decoration:none"> <button type="button" class="btn btn-success btn-xs">View Product</button></a>';

                return $view;
            });

        return $datatables->make(true);
    }
}

<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Lookup;
use App\Models\Master\Merchandise;
use Datatables, Validator;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Department';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['code'] = Merchandise::select('MERC_DEPARTMENT')
            ->where('MERC_DEPARTMENT', '<>', null)
            ->where('MERC_DIVISION', null)
            ->count();
        return view('master.department.index', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'MERC_DEPARTMENT' => 'required'
        ];
        $messages = [
            'MERC_DEPARTMENT.required' => 'Department field is required'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', 'MERC_ACTIVE']);
        $merc = new Merchandise();
        $merc->MERC_RECID = Uuid::uuid();
        if($request->input('MERC_ACTIVE') != null ) {
            $merc->MERC_ACTIVE = 1;
        } else {
            $merc->MERC_ACTIVE = 0;
        }
        $merc->fill($post_data);
        $merc->save();
        $request->session()->flash('success', 'Data has been saved');
        return redirect('master/department');

    }

    public function edit($id)
    {
        $data['merc'] = Merchandise::findOrFail($id);
        return view('master.department.edit', $data);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'MERC_DEPARTMENT' => 'required'
        ];
        $messages = [
            'MERC_DEPARTMENT.required' => 'Department field is required'
        ];
        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', 'MERC_ACTIVE']);
        $merchandise = Merchandise::findOrFail($id);
        $merchandise->fill($post_data);
        if($request->input('MERC_ACTIVE') == 'on') {
            $merchandise->MERC_ACTIVE = 1;
        } else {
            $merchandise->MERC_ACTIVE = 0;
        }
        $merchandise->save();
        $request->session()->flash('success', 'Data has been updated');
        return redirect('master/department');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        Merchandise::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/department');
    }

    public function getData()
    {
        $department = Lookup::where('LOK_CODE', 'DEPT');

        $datatables = app('datatables')->of($department)
            ->addColumn('action', function($department) {
                $edit = '<a href="'.url('master/department/'.$department->LOK_RECID).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$department->LOK_RECID.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            });
        return $datatables->make(true);
    }
}

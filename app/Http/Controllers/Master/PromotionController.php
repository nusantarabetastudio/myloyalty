<?php

namespace App\Http\Controllers\Master;

use App\Models\Master\Lookup;
use App\Models\Master\Promotion;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage, Image;
use Faker\Provider\Uuid;

class PromotionController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master Promotion';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

        return view('master.promotion.index', $data);
    }

    public function create()
    {
        $data['breadcrumbs'] = 'Master Promotion';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['payment_methods'] = Lookup::where('LOK_CODE', 'PAYM')->get();
        $data['stores'] = Tenant::where('TNT_ACTIVE', 1)->get();

        return view('master.promotion.create', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'PRM_DESC' => 'required',
            'PRM_IMAGE_1' => 'image',
            'PRM_IMAGE_2' => 'image',
            'PRM_TENANT' => 'required'
        ];
        $messages = [
            'PRM_DESC.required' => 'Description field is required',
            'PRM_IMAGE_1.image' => 'Uploaded file must be an image',
            'PRM_IMAGE_2.image' => 'Uploaded file must be an image'
        ];
        $this->validate($request, $rules, $messages);

        // $post_data = $request->except(['_token', 'PRM_ACTIVE', 'PRM_IMAGE_1', 'PRM_IMAGE_2']);
        $promotion = new Promotion();
        $promotion->PRM_RECID = Uuid::uuid();
        $promotion->PRM_PNUM = $this->pnum;
        $promotion->PRM_PTYPE = $this->ptype;
        $promotion->PRM_DESC = $request->PRM_DESC;
        $promotion->PRM_SUBTITLE = $request->PRM_SUBTITLE;
        $promotion->PRM_TENANT = $request->PRM_TENANT;
        $promotion->PRM_FRDATE = $request->PRM_FRDATE;
        $promotion->PRM_TODATE = $request->PRM_TODATE;
        $promotion->PRM_TEXT = $request->PRM_TEXT;
        $promotion->PRM_FOOT_NOTE = $request->PRM_FOOT_NOTE;
        $promotion->PRM_TEXT_IND = $request->PRM_TEXT_IND;
        $promotion->PRM_UPDATE = 0;
        $promotion->PRM_PAYMENT = $request->PRM_PAYMENT ? : null;
        $promotion->PRM_ACTIVE = $request->input('PRM_ACTIVE') ? 1 : 0;
        if ($request->hasFile('PRM_IMAGE_1')) {
            $promotion->PRM_IMAGE_1 = uploadToS3($request->file('PRM_IMAGE_1'), 'uploads/promotions');
        }
        if ($request->hasFile('PRM_IMAGE_2')) {
            $promotion->PRM_IMAGE_2 = uploadToS3($request->file('PRM_IMAGE_2'), 'uploads/promotions');
        }
        $promotion->save();

        return redirect('master/promotion')->with('success', 'Data has been saved');
    }

    public function edit($id)
    {
        $data['breadcrumbs'] = 'Master Promotion';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['promotion'] = Promotion::findOrFail($id);
        $data['stores'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['payment_methods'] = Lookup::where('LOK_CODE', 'PAYM')->get();
        return view('master.promotion.create', $data);

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'PRM_DESC' => 'required',
            'PRM_IMAGE_1' => 'image',
            'PRM_IMAGE_2' => 'image',
            'PRM_TENANT' => 'required'
        ];

        $messages = [
            'PRM_DESC.required' => 'Description field is required',
            'PRM_IMAGE_1.image' => 'Uploaded file must be an image',
            'PRM_IMAGE_2.image' => 'Uploaded file must be an image'
        ];

        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', 'PRM_ACTIVE', 'PRM_IMAGE_1', 'PRM_IMAGE_2']);
        $promotion = Promotion::findOrFail($id);
        $promotion->PRM_DESC = $request->PRM_DESC;
        $promotion->PRM_SUBTITLE = $request->PRM_SUBTITLE;
        $promotion->PRM_TENANT = $request->PRM_TENANT;
        $promotion->PRM_FRDATE = $request->PRM_FRDATE;
        $promotion->PRM_TODATE = $request->PRM_TODATE;
        $promotion->PRM_TEXT = $request->PRM_TEXT;
        $promotion->PRM_FOOT_NOTE = $request->PRM_FOOT_NOTE;
        $promotion->PRM_TEXT_IND = $request->PRM_TEXT_IND;
        $promotion->PRM_PAYMENT = $request->PRM_PAYMENT ? : null;
        $promotion->PRM_ACTIVE = $request->input('PRM_ACTIVE') ? 1 : 0;
        if ($request->hasFile('PRM_IMAGE_1')) {
            $promotion->PRM_IMAGE_1 = uploadToS3($request->file('PRM_IMAGE_1'), 'uploads/promotions');
        }
        if ($request->hasFile('PRM_IMAGE_2')) {
            $promotion->PRM_IMAGE_2 = uploadToS3($request->file('PRM_IMAGE_2'), 'uploads/promotions');
        }
        $promotion->save();

        return redirect('master/promotion')->with('success', 'Data has been updated');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $promotion = Promotion::find($request->input('id'));
        if($promotion->PRM_IMAGE_1 !== null){
            Storage::delete($promotion->PRM_IMAGE_1);
        }
        if($promotion->PRM_IMAGE_2 !== null){
            Storage::delete($promotion->PRM_IMAGE_2);
        }
        $promotion->delete();
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/promotion');
    }

    public function getData()
    {
        $promotions = Promotion::all();

        $datatables = app('datatables')->of($promotions)
            ->addColumn('action', function($promotion) {
                $edit = '<a href="'.url('master/promotion/'.$promotion->PRM_RECID ).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'. $promotion->PRM_RECID .'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                return $edit . '&nbsp;' . $delete;
            })
            ->addColumn('STORE', function($promotion) {
                if($promotion->PRM_TENANT !== null) {
                    return ucfirst($promotion->tenant->TNT_DESC);
                }
                return null;
            })
            ->editColumn('PRM_FRDATE', function ($promotion) {
                return $promotion->PRM_FRDATE->format('d/m/Y');
            })
            ->editColumn('PRM_TODATE', function ($promotion) {
                return $promotion->PRM_TODATE->format('d/m/Y');
            });

        return $datatables->make(true);
    }
}

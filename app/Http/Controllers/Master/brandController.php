<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Faker\Provider\Uuid;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Master\Brand;
use App\Models\Master\Product;

class brandController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct()
    {
        $this->parent = 'Master Data';
        $this->parent_link = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit', 'view']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Brand';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        return view('master.brand.index', $data);
    }

    public function getData()
    {
        $brand = Brand::all();

        $datatables = app('datatables')->of($brand)
            ->addColumn('action', function($brand) {
                $view = '<a href="'.url('master/brandData/'.$brand->id).'/view" style="text-decoration:none"> <button type="button" class="btn btn-success btn-xs">View Product</button></a>';

                return $view;
            });

        return $datatables->make(true);
    }

    public function view($id)
    {
    	
    	$data['products'] = Product::where('brand_id', $id)->get();
        $data['brand'] = Brand::where('id',$id)->first();
    	return view('master.brand.view', $data);
    }
}

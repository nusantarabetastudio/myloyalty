<?php

namespace App\Http\Controllers\Master;

use App\Models\Master\Lookup;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Faker\Provider\Uuid;
use Image, Storage, File;
use App\Models\Master\Event;
use Carbon\Carbon;

class EventController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master Event';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

        return view('master.event.index', $data);
    }

    public function create()
    {
        $data['breadcrumbs'] = 'Master Event';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['categories'] = Lookup::where('LOK_CODE', 'EVNC')->where('LOK_ACTIVE', 1)->get();
        return view('master.event.create', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'EVT_DESC' => 'required',
            'EVT_FRDATE' => 'required',
            'EVT_TODATE' => 'required',
            'EVT_IMAGE_1' => 'image',
            'EVT_IMAGE_2' => 'image'
        ];
        $messages = [
            'EVT_DESC' => 'Description field is required.',
            'EVT_FRDATE' => 'From Date field is required.',
            'EVT_TODATE' => 'To Date field is required.',
            'EVT_IMAGE_1' => 'Uploaded file must be image.',
            'EVT_IMAGE_2' => 'Uploaded file must be image.'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except([
            '_token', 'EVT_ACTIVE', 'EVT_FRDATE', 'EVT_TODATE', 'EVT_FRTIME_H', 'EVT_FRTIME_M',
            'EVT_FRTIME_S', 'EVT_TOTIME_H', 'EVT_TOTIME_M', 'EVT_TOTIME_S', 'EVT_IMAGE_1', 'EVT_IMAGE_2'
        ]);
        $FR_TIME_H = $request->input('EVT_FRTIME_H');
        $FR_TIME_M = $request->input('EVT_FRTIME_M');
        $FR_TIME_S = $request->input('EVT_FRTIME_S');
        $TO_TIME_H = $request->input('EVT_TOTIME_H');
        $TO_TIME_M = $request->input('EVT_TOTIME_M');
        $TO_TIME_S = $request->input('EVT_TOTIME_S');

        $event = new Event();
        $event->EVT_RECID = Uuid::uuid();
        $event->EVT_PNUM = $this->pnum;
        $event->EVT_PTYPE = $this->ptype;
        $event->fill($post_data);
        if($request->input('EVT_ACTIVE')== 'on'){
            $event->EVT_ACTIVE = 1;
        } else {
            $event->EVT_ACTIVE = 0;
        }
        $event->EVT_FRDATE = dateFormat($request->input('EVT_FRDATE'));
        $event->EVT_TODATE = dateFormat($request->input('EVT_TODATE'));
        $event->EVT_FRTIME = Carbon::createFromTime($FR_TIME_H,$FR_TIME_M,$FR_TIME_S);
        $event->EVT_TOTIME = Carbon::createFromTime($TO_TIME_H,$TO_TIME_M,$TO_TIME_S);

        if($request->hasFile('EVT_IMAGE_1')){
            $event->EVT_IMAGE_1 = uploadToS3($request->file('EVT_IMAGE_1'), 'uploads/events');

        }

        if($request->hasFile('EVT_IMAGE_2')){
            $event->EVT_IMAGE_2 = uploadToS3($request->file('EVT_IMAGE_2'), 'uploads/events');
        }
        $event->save();
        $request->session()->flash('success', 'Data has been saved');
        return redirect('master/event');
    }

    public function edit($id)
    {
        $data['event'] = Event::findOrFail($id);
        $data['breadcrumbs'] = 'Master Event';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['categories'] = Lookup::where('LOK_CODE', 'EVNC')->where('LOK_ACTIVE', 1)->get();
        return view('master.event.create', $data);
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'EVT_DESC' => 'required',
            'EVT_VENUE' => 'required',
            'EVT_FRDATE' => 'required',
            'EVT_TODATE' => 'required',
            'EVT_IMAGE_1' => 'image',
            'EVT_IMAGE_2' => 'image'
        ];
        $messages = [
            'EVT_DESC' => 'Description field is required.',
            'EVT_VENUE' => 'Venue field is required.',
            'EVT_FRDATE' => 'From Date field is required.',
            'EVT_TODATE' => 'To Date field is required.',
            'EVT_IMAGE_1' => 'Uploaded file must be image.',
            'EVT_IMAGE_2' => 'Uploaded file must be image.'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except([
            '_token', 'EVT_ACTIVE', 'EVT_FRDATE', 'EVT_TODATE', 'EVT_FRTIME_H', 'EVT_FRTIME_M',
            'EVT_FRTIME_S', 'EVT_TOTIME_H', 'EVT_TOTIME_M', 'EVT_TOTIME_S', 'EVT_IMAGE_1', 'EVT_IMAGE_2'
        ]);
        $FR_TIME_H = $request->input('EVT_FRTIME_H');
        $FR_TIME_M = $request->input('EVT_FRTIME_M');
        $FR_TIME_S = $request->input('EVT_FRTIME_S');
        $TO_TIME_H = $request->input('EVT_TOTIME_H');
        $TO_TIME_M = $request->input('EVT_TOTIME_M');
        $TO_TIME_S = $request->input('EVT_TOTIME_S');

        $event = Event::find($id);
        $event->fill($post_data);
        if($request->input('EVT_ACTIVE')== 'on'){
            $event->EVT_ACTIVE = 1;
        } else {
            $event->EVT_ACTIVE = 0;
        }
        $event->EVT_FRDATE = dateFormat($request->input('EVT_FRDATE'));
        $event->EVT_TODATE = dateFormat($request->input('EVT_TODATE'));
        $event->EVT_FRTIME = Carbon::createFromTime($FR_TIME_H,$FR_TIME_M,$FR_TIME_S);
        $event->EVT_TOTIME = Carbon::createFromTime($TO_TIME_H,$TO_TIME_M,$TO_TIME_S);

        if($request->hasFile('EVT_IMAGE_1')){
            Storage::delete($event->EVT_IMAGE_1);
            $event->EVT_IMAGE_1 = $this->upload($request, 'EVT_IMAGE_1', 'event', 'uploads/events');
        }

        if($request->hasFile('EVT_IMAGE_2')){
            Storage::delete( $event->EVT_IMAGE_2);
            $event->EVT_IMAGE_2 = $this->upload($request, 'EVT_IMAGE_2', 'event', 'uploads/events');
        }
        $event->save();
        $request->session()->flash('success', 'Data has been updated');
        return redirect('master/event');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $event = Event::find($request->input('id'));
        if($event->EVT_IMAGE_1 !== null) {
            Storage::delete($event->EVT_IMAGE_1);
        }
        if($event->EVT_IMAGE_2 !== null) {
            Storage::delete($event->EVT_IMAGE_2);
        }
        $event->delete();
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/event');
    }

    public function getData()
    {
        $events = Event::select(['EVT_RECID', 'EVT_DESC', 'EVT_VENUE', 'EVT_FRDATE', 'EVT_TODATE', 'EVT_FRTIME', 'EVT_TOTIME', 'EVT_ACTIVE']);

        $datatables = app('datatables')->of($events)
            ->addColumn('action', function($event) {
                $edit = '<a href="'.url('master/event/'.$event->EVT_RECID).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$event->EVT_RECID.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                return $edit . '&nbsp;' . $delete;
            })
            ->editColumn('EVT_FRDATE', function($event){
                return $event->EVT_FRDATE->format('d/m/Y');
            })
            ->editColumn('EVT_TODATE', function($event){
                return $event->EVT_TODATE->format('d/m/Y');
            })
            ->editColumn('EVT_FRTIME', function($event){
                return substr($event->EVT_FRTIME,0,8);
            })
            ->editColumn('EVT_TOTIME', function($event){
                return substr($event->EVT_TOTIME,0,8);
            });
        return $datatables->make(true);
    }

    function upload(Request $request, $parameter_name ,$save_name, $folder, $x = 500, $y = 500){
        $file = $request->file($parameter_name);
        $imageFileName = $save_name.'-'. rand(0,999).'-'.date('d-m-Y').'-.'.$file->getClientOriginalExtension();
        $destinationPath = $folder;
        $file->move($destinationPath, $imageFileName);
        Image::make( sprintf( $destinationPath .'/%s', $imageFileName) )->resize($x, $y)->save();
        return $imageFileName;
    }
}

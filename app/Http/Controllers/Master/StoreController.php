<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Faker\Provider\Uuid;
use Session, Storage, Image;
use App\Models\Master\Tenant;
use App\Models\Master\Lookup;
use App\Models\Preference;

class StoreController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->parent = 'Master Data';
        $this->admin = Preference::where('key', 'admin')->first()->value;
        $this->superadmin = preference::where('key', 'superadmin')->first()->value;
        $this->parent_link = '';
        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit', 'getView']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master Store';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['superadmin'] = $this->superadmin ;
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        return view('master.store.index', $data);
    }

    public function create()
    {
        $data['breadcrumbs'] = 'Master Store';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['tenant_categories'] = Lookup::where('LOK_CODE', 'TENC')->get();
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();
        $data['areas'] = Lookup::where('LOK_CODE', 'AREA')->get();

        return view('master.store.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'TNT_CODE'  => 'required',
            'TNT_DESC'   => 'required',
            'TNT_CATEGORY' => 'required',
            'TNT_IMAGE_URL_1' => 'image|max:1048',
            'TNT_IMAGE_URL_2' => 'image|max:1048',
        ]);

        $store = new Tenant();
        $store->TNT_RECID = Uuid::uuid();
        $store->TNT_PNUM = $this->pnum;
        $store->TNT_PTYPE = $this->ptype;
        $store->TNT_CODE = $request->TNT_CODE;
        $store->TNT_DESC = $request->TNT_DESC;
        $store->TNT_PHONE = $request->TNT_PHONE;
        $store->TNT_CONTACT = $request->TNT_CONTACT;
        $store->TNT_EMAIL = $request->TNT_EMAIL;
        $store->TNT_CATEGORY = $request->TNT_CATEGORY;
        $store->TNT_UNIT = $request->TNT_UNIT;
        $store->TNT_LATITUDE = $request->TNT_LATITUDE ?: 0;
        $store->TNT_LONGITUDE = $request->TNT_LONGITUDE ?: 0;
        $store->TNT_UPDATE = 0;
        $store->TNT_COUNTRY = $request->TNT_COUNTRY ?: null;
        $store->TNT_PROVINCE = $request->TNT_PROVINCE ?: null;
        $store->TNT_CITY = $request->TNT_CITY ?: null;
        $store->TNT_AREA = $request->TNT_AREA ?: null;
        $store->TNT_ACTIVE = $request->input('TNT_ACTIVE') ? 1 : 0;
        if($request->hasFile('TNT_IMAGE_URL_1')){
            $store->TNT_IMAGE_URL_1 = uploadToS3($request->file('TNT_IMAGE_URL_1'), 'uploads/stores');
        }
        if($request->hasFile('TNT_IMAGE_URL_2')){
            $store->TNT_IMAGE_URL_2 = uploadToS3($request->file('TNT_IMAGE_URL_2'), 'uploads/stores');
        }
        $store->save();
        $request->session()->flash('success', 'Data has been saved.');
        return redirect('master/store');

    }

    public function edit($id)
    {
        $data['breadcrumbs'] = 'Master Store';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['store'] = Tenant::find($id);
        $data['tenant_categories'] = Lookup::where('LOK_CODE', 'TENC')->get();
        $data['provinces'] = Lookup::where('LOK_CODE', 'PROV')->where('LOK_CODEMST', $data['store']->TNT_COUNTRY)->get();
        $data['cities'] = Lookup::where('LOK_CODE', 'CITY')->where('LOK_CODEMST', $data['store']->TNT_PROVINCE)->get();
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();
        $data['areas'] = Lookup::where('LOK_CODE', 'AREA')->get();

        return view('master.store.create', $data);

    }

    public function getView($id)
    {
        $data['breadcrumbs'] = 'Master Store';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['store'] = Tenant::find($id);
        $data['tenant_categories'] = Lookup::where('LOK_CODE', 'TENC')->get();
        $data['provinces'] = Lookup::where('LOK_CODE', 'PROV')->where('LOK_CODEMST', $data['store']->TNT_COUNTRY)->get();
        $data['cities'] = Lookup::where('LOK_CODE', 'CITY')->where('LOK_CODEMST', $data['store']->TNT_PROVINCE)->get();
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();
        $data['areas'] = Lookup::where('LOK_CODE', 'AREA')->get();

        return view('master.store.viewstore', $data);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'TNT_DESC' => 'required',
            'TNT_IMAGE_URL_1' => 'image',
            'TNT_IMAGE_URL_2' => 'image',
            'TNT_LONGITUDE' => 'numeric|digits_between:1,15',
            'TNT_LATITUDE' => 'numeric|digits_between:1,15'
        ];

        $messages = [
            'TNT_DESC.required' => 'Description field is required.',
            'TNT_IMAGE_URL_1.image' => 'Uploaded file must be image.',
            'TNT_IMAGE_URL_2.image' => 'Uploaded file must be image.'
        ];
        $this->validate($request, $rules, $messages);

        $store = Tenant::findOrFail($id);
        $store->TNT_CODE = $request->TNT_CODE;
        $store->TNT_DESC = $request->TNT_DESC;
        $store->TNT_PHONE = $request->TNT_PHONE;
        $store->TNT_CONTACT = $request->TNT_CONTACT;
        $store->TNT_EMAIL = $request->TNT_EMAIL;
        $store->TNT_CATEGORY = $request->TNT_CATEGORY;
        $store->TNT_UNIT = $request->TNT_UNIT;
        $store->TNT_LATITUDE = $request->TNT_LATITUDE ?: 0;
        $store->TNT_LONGITUDE = $request->TNT_LONGITUDE ?: 0;
        $store->TNT_COUNTRY = $request->TNT_COUNTRY;
        $store->TNT_PROVINCE = $request->TNT_PROVINCE;
        $store->TNT_CITY = $request->TNT_CITY;
        $store->TNT_AREA = $request->TNT_AREA;
        $store->TNT_ACTIVE = $request->input('TNT_ACTIVE') ? 1 : 0;
        if($request->hasFile('TNT_IMAGE_URL_1')){
            $store->TNT_IMAGE_URL_1 = uploadToS3($request->file('TNT_IMAGE_URL_1'), 'uploads/stores');
        }
        if($request->hasFile('TNT_IMAGE_URL_2')){
            $store->TNT_IMAGE_URL_2 = uploadToS3($request->file('TNT_IMAGE_URL_2'), 'uploads/stores');
        }

        $store->save();
        $request->session()->flash('success', 'Data has been updated.');
        return redirect('master/store');
    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $tenant = Tenant::find($request->input('id'));
        if($tenant->TNT_IMAGE_URL_1 !== null){
            Storage::delete($tenant->TNT_IMAGE_URL_1);
        }
        if($tenant->TNT_IMAGE_URL_2 !== null){
            Storage::delete($tenant->TNT_IMAGE_URL_2);
        }
        $tenant->delete();
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/store');
    }

    public function getData()
    {
        $roleId = Session::get('role_data')->ROLE_RECID;
        $store = Tenant::where('TNT_PNUM', '!=', '0101');

        if($this->superadmin == $roleId) {
        $datatables = app('datatables')->of($store)
            ->addColumn('action', function($store) {
                $edit = '<a href="'.url('master/store/'.$store->TNT_RECID ).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'. $store->TNT_RECID .'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                $view = '<a href="'.url('master/store/'.$store->TNT_RECID ).'/view" style="text-decoration:none"> <button type="button" class="btn btn-success btn-xs">View</button></a>';
                return $edit . '&nbsp;' . $delete . '&nbsp;' .$view;
            });
            return $datatables->make(true);
        } else {
        $datatables = app('datatables')->of($store)
            ->addColumn('action', function($store) {
                $edit = '<a href="'.url('master/store/'.$store->TNT_RECID ).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $view = '<a href="'.url('master/store/'.$store->TNT_RECID ).'/view" style="text-decoration:none"> <button type="button" class="btn btn-success btn-xs">View</button></a>';

                return $edit . $view;
            });
            return $datatables->make(true);
        }
    }


    function upload(Request $request, $parameter_name ,$save_name, $folder, $x = 500, $y = 500){
        $file = $request->file($parameter_name);
        $imageFileName = $save_name.'-'. rand(0,999).'-'.date('d-m-Y').'-.'.$file->getClientOriginalExtension();
        $destinationPath = $folder;
        $file->move($destinationPath, $imageFileName);
        Image::make( sprintf( $destinationPath .'/%s', $imageFileName) )->resize($x, $y)->save();
        return $imageFileName;
    }
}

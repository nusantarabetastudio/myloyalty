<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Master\Lookup;
use App\Models\Master\Merchandise;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;

class SegmentController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Segment';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['code'] = Merchandise::where('MERC_SEGMENT', '<>', null)
            ->count();
        $data['departments'] = Merchandise::select(['MERC_CODE', 'MERC_DEPARTMENT'])
            ->where('MERC_DEPARTMENT', '<>', null)
            ->where('MERC_DIVISION', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['divisions'] = Merchandise::select(['MERC_CODE', 'MERC_DIVISION'])
            ->where('MERC_DIVISION', '<>' , null)
            ->where('MERC_CATEGORY', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['categories'] = Merchandise::select(['MERC_CODE', 'MERC_CATEGORY'])
            ->where('MERC_CATEGORY', '<>', null)
            ->where('MERC_SUBCATEGORY', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['subcategories'] = Merchandise::select(['MERC_CODE', 'MERC_SUBCATEGORY'])
            ->where('MERC_SUBCATEGORY', '<>', null)
            ->where('MERC_SEGMENT', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        
        return view('master.segment.index', $data);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $rules = [
            'MERC_SUBCATEGORY' => 'required'
        ];

        $messages = [
            'MERC_SUBCATEGORY.required' => 'Sub Category field is required'
        ];

        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', 'MERC_CODE', 'MERC_DIVISION_CODE', 'MERC_DEPARTMENT_CODE', 'MERC_CATEGORY_CODE', 'MERC_SUBCATEGORY_CODE', 'MERC_ACTIVE']);
        $merc = new Merchandise();
        $merc->MERC_RECID = Uuid::uuid();
        if($request->input('MERC_ACTIVE') != null ) {
            $merc->MERC_ACTIVE = 1;
        } else {
            $merc->MERC_ACTIVE = 0;
        }
        $merc->MERC_CODE = getDepartment($request->input('MERC_DEPARTMENT_CODE')) . getDivision($request->input('MERC_DIVISION_CODE')) . getCategory($request->input('MERC_CATEGORY_CODE')). getSubCategory($request->input('MERC_SUBCATEGORY_CODE')) . $request->input('MERC_CODE');
        $merc->fill($post_data);
        $merc->save();

        $request->session()->flash('success', 'Data has been saved.');
        return redirect('master/segment');
    }

    public function edit($id)
    {
        $data['breadcrumbs'] = 'Segment';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['merc'] = Merchandise::findOrFail($id);
        $data['departments'] = Merchandise::select(['MERC_CODE', 'MERC_DEPARTMENT'])
            ->where('MERC_DEPARTMENT', '<>', null)
            ->where('MERC_DIVISION', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['divisions'] = Merchandise::select(['MERC_CODE', 'MERC_DIVISION'])
            ->where('MERC_DIVISION', '<>', null)
            ->where('MERC_CATEGORY', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['categories'] = Merchandise::select(['MERC_CODE', 'MERC_CATEGORY'])
            ->where('MERC_CATEGORY', '<>', null)
            ->where('MERC_SUBCATEGORY', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        $data['subcategories'] = Merchandise::select(['MERC_CODE', 'MERC_SUBCATEGORY'])
            ->where('MERC_SUBCATEGORY', '<>', null)
            ->where('MERC_SEGMENT', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        return view('master.segment.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'MERC_DEPARTMENT' => 'required',
            'MERC_DIVISION' => 'required',
            'MERC_CATEGORY' => 'required',
            'MERC_SUBCATEGORY' => 'required',
            'MERC_SEGMENT' => 'required'
        ];
        $messages = [
            'MERC_DEPARTMENT' => 'Department field is required.',
            'MERC_DIVISION' => 'Division field is required.',
            'MERC_CATEGORY' => 'Category field is required',
            'MERC_SUBCATEGORY' => 'Sub Category field is required.',
            'MERC_SEGMENT' => 'Segment field is required.'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', '_method', 'MERC_DEPARTMENT_CODE', 'MERC_DIVISION_CODE', 'MERC_CATEGORY_CODE', 'MERC_SUBCATEGORY_CODE']);
        $merchandise = Merchandise::findOrFail($id);
        $merchandise->fill($post_data);
        $code = getDepartment($request->input('MERC_DEPARTMENT_CODE')) . getDivision($request->input('MERC_DIVISION_CODE')). getCategory($request->input('MERC_CATEGORY_CODE')) . getSubCategory($request->input('MERC_SUBCATEGORY_CODE')) . $request->input('MERC_CODE');
        if($merchandise->MERC_CODE !== $code) {
            $merchandise->MERC_CODE = $code;
        }
        if($request->input('MERC_ACTIVE') == 'on') {
            $merchandise->MERC_ACTIVE = 1;
        } else {
            $merchandise->MERC_ACTIVE = 0;
        }
        $merchandise->save();
        $request->session()->flash('success', 'Data has been updated');
        return redirect('master/segment');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        Merchandise::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/segment');
    }

    public function getData()
    {
        $segments = Lookup::with(['subCategory.category.division.department'])->where('LOK_CODE', 'SEGM');

        $datatables = app('datatables')->of($segments);

        return $datatables->orderColumn('name', 'email $1')->make(true);
    }
}

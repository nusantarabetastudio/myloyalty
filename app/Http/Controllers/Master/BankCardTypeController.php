<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Master\Bank;
use App\Models\Master\BankCardType;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;

class BankCardTypeController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
        $this->parent = 'Master Data';
        $this->parent_link = '';
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master BIN';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['banks'] = Bank::orderBy('name')->get();

        return view('master.bank-card-type.index', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'code' => 'required',
            'bank_id' => 'required'
        ];
        $messages = [
            'name.required' => 'Name field is required',
            'code.required' => 'Code field is required',
            'bank_id.required' => 'Bank Code field is required'
        ];
        $this->validate($request, $rules, $messages);

        $input = $request->except('_token');
        $bankCardType = new BankCardType();
        $bankCardType->id = Uuid::uuid();
        $bankCardType->name = $input['name'];
        $bankCardType->code = $input['code'];
        $bankCardType->bank_id = $input['bank_id'];
        $bankCardType->save();

        $request->session()->flash('success', 'Data has been saved.');
        return redirect('master/bank-card-type');

    }

    public function edit($id)
    {
        $bankCardType = BankCardType::findOrFail($id);
        $banks = Bank::orderBy('name')->get();
        return view('master.bank-card-type.edit', compact('bankCardType', 'banks'));
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'code' => 'required',
            'bank_id' => 'required'
        ];
        $messages = [
            'name.required' => 'Name field is required',
            'code.required' => 'Code field is required',
            'bank_id.required' => 'Bank Code field is required'
        ];
        $this->validate($request, $rules, $messages);

        $input = $request->except(['_token', '_method']);
        $bankCardType = BankCardType::findOrFail($id);
        $bankCardType->name = $input['name'];
        $bankCardType->code = $input['code'];
        $bankCardType->bank_id = $input['bank_id'];
        $bankCardType->save();

        $request->session()->flash('success', 'Data has been updated.');
        return redirect('master/bank-card-type');

    }

    public function destroy(Request $request)
    {
        BankCardType::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/bank-card-type');
    }

    public function getData()
    {
        $bankCardType = BankCardType::query();

        $datatables = app('datatables')->of($bankCardType)
            ->addColumn('action', function($bankCardType) {
                $edit = '<a href="'.url('master/bank-card-type/'.$bankCardType->id).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$bankCardType->id.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            });
        return $datatables->make(true);

    }
}

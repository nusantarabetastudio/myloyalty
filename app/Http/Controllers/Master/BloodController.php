<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Master\Lookup;
use Faker\Provider\Uuid;

class BloodController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        $this->code = 'BLOD';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master Blood';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

        return view('master.blood.index', $data);
    }

    public function store(Request $request)
    {
        $lookup = Lookup::where('LOK_CODE', 'BLOD')->get();
        $blod = Lookup::where('LOK_CODE', 'BLOD')->where('LOK_MAPPING', '!=', '999')->orderby('LOK_MAPPING', 'DESC')->first()->LOK_MAPPING;
         $LOK_MAPPING = $blod + 1;
        $rules = [
            'LOK_DESCRIPTION' => 'required'
        ];
        $messages = [
            'LOK_DESCRIPTION.required' => 'Description field is required'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', 'LOK_ACTIVE']);
        $lookup = new Lookup();
        $lookup->LOK_RECID = Uuid::uuid();
        $lookup->LOK_PNUM = $this->pnum;
        $lookup->LOK_PTYPE = $this->ptype;
        $lookup->LOK_CODE = $this->code;
        $lookup->LOK_MAPPING = $LOK_MAPPING;
        $lookup->LOK_DESCRIPTION = strtoupper($post_data['LOK_DESCRIPTION']);
        if($request->input('LOK_ACTIVE') != null ) {
            $lookup->LOK_ACTIVE = 1;
        } else {
            $lookup->LOK_ACTIVE = 0;
        }
        $lookup->save();
        $request->session()->flash('success', 'Data has been saved');
        return redirect('master/blood');

    }

    public function edit($id)
    {
        $data['lookup'] = Lookup::findOrFail($id);
        return view('master.blood.edit', $data);
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'LOK_DESCRIPTION' => 'required'
        ];
        $messages = [
            'LOK_DESCRIPTION.required' => 'Description field is required'
        ];
        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', 'LOK_ACTIVE']);
        $lookup = Lookup::find($id);
        $lookup->fill($post_data);
        if($request->input('LOK_ACTIVE') != null ) {
            $lookup->LOK_ACTIVE = 1;
        } else {
            $lookup->LOK_ACTIVE = 0;
        }
        $request->session()->flash('success', 'Data has been Updated');
        return redirect('master/blood');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        Lookup::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/blood');
    }

    public function getData()
    {
        $lookup = Lookup::where('LOK_CODE', 'BLOD')->get();
        $datatables = app('datatables')->of($lookup)
            ->addColumn('action', function($lookup) {
                $edit = '<a href="'.url('master/blood/'.$lookup->LOK_RECID).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$lookup->LOK_RECID.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                return $edit . '&nbsp;' . $delete;
            });
        return $datatables->make(true);
    }
}

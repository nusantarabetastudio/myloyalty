<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Master\Lookup;
use App\Models\Master\Merchandise;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;


class DivisionController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Division';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['code'] = Merchandise::where('MERC_DIVISION')
            ->where('MERC_DIVISION', '<>', null)
            ->where('MERC_CATEGORY', null)
            ->count();
        $data['departments'] = Merchandise::select('MERC_CODE', 'MERC_DEPARTMENT')
            ->where('MERC_DEPARTMENT', '<>', null)
            ->where('MERC_DIVISION', null)
            ->where('MERC_ACTIVE', 1)
            ->get();
        return view('master.division.index', $data);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $rules = [
            'MERC_DEPARTMENT' => 'required'
        ];
        $messages = [
            'MERC_DEPARTMENT.required' => 'Department field is required required'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', 'MERC_ACTIVE', 'MERC_CODE', 'MERC_DEPARTMENT_CODE']);
        $merc = new Merchandise();
        $merc->MERC_RECID = Uuid::uuid();
        $merc->fill($post_data);
        if($request->input('MERC_ACTIVE') != null ) {
            $merc->MERC_ACTIVE = 1;
        } else {
            $merc->MERC_ACTIVE = 0;
        }
        $merc->MERC_CODE = getDepartment($request->input('MERC_DEPARTMENT_CODE')) . $request->input('MERC_CODE');
        $merc->save();
        $request->session()->flash('success', 'Successfully');
        return redirect('master/division');

    }

    public function edit($id)
    {
        $data['merc'] = Merchandise::findOrFail($id);
        $data['code'] = Merchandise::select('MERC_DIVISION')
            ->where('MERC_DIVISION', '<>' , null)
            ->where('MERC_CATEGORY', null)
            ->count();
        $data['departments'] = Merchandise::select('MERC_CODE', 'MERC_DEPARTMENT')
            ->where('MERC_DEPARTMENT', '<>', null)
            ->where('MERC_ACTIVE', 1)
            ->where('MERC_DIVISION', null)
            ->get();
        return view('master.division.edit', $data);
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'MERC_DEPARTMENT' => 'required'
        ];

        $messages = [
            'MERC_DEPARTMENT.required' => 'Department field is required required'
        ];
        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', '_method', 'MERC_CODE', 'MERC_ACTIVE' , 'MERC_DEPARTMENT_CODE']);
        $merchandise = Merchandise::findOrFail($id);
        $merchandise->fill($post_data);
        $code = getDepartment($request->input('MERC_DEPARTMENT_CODE')) . $request->input('MERC_CODE');
        if($merchandise->MERC_CODE !== $code) {
            $merchandise->MERC_CODE = $code;
        }
        if($request->input('MERC_ACTIVE') == 'on') {
            $merchandise->MERC_ACTIVE = 1;
        } else {
            $merchandise->MERC_ACTIVE = 0;
        }
        $merchandise->save();
        $request->session()->flash('success', 'Update Success');
        return redirect('master/division');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        Merchandise::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/division');
    }

    public function getData()
    {
        $division = Lookup::with(['department'])->where('LOK_CODE', 'DIVI');

        $datatables = app('datatables')->of($division)
            ->addColumn('action', function($division) {
                $edit = '<a href="'.url('master/division/'.$division->LOK_RECID).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$division->LOK_RECID.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            });
        return $datatables->make(true);
    }
}

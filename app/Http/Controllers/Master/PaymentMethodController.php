<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Master\PaymentMethod;
use Faker\Provider\Uuid;

class PaymentMethodController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
        $this->parent = 'Master Data';
        $this->parent_link = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master Tender';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

        return view('master.payment-method.index', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];
        $messages = [
            'name.required' => 'Payment Method Name field is required'
        ];
        $this->validate($request, $rules, $messages);

        $input = $request->except('_token');
        $lastCode = PaymentMethod::take(1)->orderBy('code', 'DESC')->first()->code;
        $paymentMethod = new PaymentMethod();
        $paymentMethod->id = Uuid::uuid();
        $paymentMethod->code = $lastCode + 1;
        $paymentMethod->name = $input['name'];
        $paymentMethod->save();

        $request->session()->flash('success', 'Data has been saved.');
        return redirect('master/payment-method');

    }

    public function edit($id)
    {
        $paymentMethod = PaymentMethod::findOrFail($id);
        return view('master.payment-method.edit', compact('paymentMethod'));
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required'
        ];
        $messages = [
            'name.required' => 'Payment Method Name field is required'
        ];
        $this->validate($request, $rules, $messages);

        $input = $request->except(['_token', '_method']);
        $paymentMethod = PaymentMethod::findOrFail($id);
        $paymentMethod->name = $input['name'];
        $paymentMethod->save();
        $request->session()->flash('success', 'Data has been updated.');
        return redirect('master/payment-method');

    }

    public function destroy(Request $request)
    {
        PaymentMethod::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/payment-method');
    }

    public function getData()
    {
        $paymentMethod = PaymentMethod::query();

        $datatables = app('datatables')->of($paymentMethod)
            ->addColumn('action', function($paymentMethod) {
                $edit = '<a href="'.url('master/payment-method/'.$paymentMethod->id).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$paymentMethod->id.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            });
        return $datatables->make(true);

    }
}

<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Master\Lookup;
use Faker\Provider\Uuid;

class ProvinceController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->parent = 'Master Data';
        $this->parent_link = '';
        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        $this->code = 'PROV';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Province';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();

        return view('master.province.index', $data);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $countries = Lookup::where('LOK_CODE', 'PROV')->where('LOK_MAPPING', '!=', '999')
        ->orderby('LOK_MAPPING', 'DESC')->first()->LOK_MAPPING;
        $countries = $countries + 1;
        $LOK_MAPPING = prefix($countries, 3);
        $rules = [
            'LOK_DESCRIPTION' => 'required'
        ];

        $messages = [
            'LOK_DESCRIPTION.required' => 'Description field is required'
        ];

        $this->validate($request, $rules, $messages);
        $lookup = new Lookup();
        $lookup->LOK_RECID = Uuid::uuid();
        $lookup->LOK_PNUM = $this->pnum;
        $lookup->LOK_PTYPE = $this->ptype;
        $lookup->LOK_CODE = $this->code;
        $lookup->LOK_CODEMST = $request->input('country_id');
        $lookup->LOK_MAPPING = $LOK_MAPPING;
        if($request->input('LOK_ACTIVE') == 'on'){
            $lookup->LOK_ACTIVE = 1;
        } else {
            $lookup->LOK_ACTIVE = 0;
        }
        $lookup->LOK_DESCRIPTION = strtoupper($request->input('LOK_DESCRIPTION'));
        $lookup->save();

        $request->session()->flash('success', 'Data has been saved.');
        return redirect('master/province');

    }

    public function edit($id)
    {
        $data['lookup'] = Lookup::findOrFail($id);
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();

        return view('master.province.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'LOK_DESCRIPTION' => 'required',
            'country_id' => 'required'
        ];

        $messages = [
            'LOK_DESCRIPTION.required' => 'Description field is required'
        ];

        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', 'LOK_ACTIVE', 'country_id']);
        $lookup = Lookup::findOrFail($id);
        $lookup->fill($post_data);
        $lookup->LOK_CODEMST = $request->input('country_id');
        if($request->input('LOK_ACTIVE') == 'on'){
            $lookup->LOK_ACTIVE = 1;
        } else {
            $lookup->LOK_ACTIVE = 0;
        }
        $lookup->save();
        $request->session()->flash('success', 'Data has been updated.');
        return redirect('master/province');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        Lookup::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/province');
    }

    public function getData()
    {
        $lookup = Lookup::where('LOK_CODE', 'PROV')->orderBy('LOK_DESCRIPTION')->get();

        $datatables = app('datatables')->of($lookup)
            ->addColumn('action', function($lookup) {
                $edit = '<a href="'.url('master/province/'.$lookup->LOK_RECID).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$lookup->LOK_RECID.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            });
        return $datatables->make(true);
    }
}

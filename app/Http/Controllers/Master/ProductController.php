<?php

namespace App\Http\Controllers\Master;

use App\Models\Master\Lookup;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Master\Product;
use Faker\Provider\Uuid;
use App\Models\Master\Merchandise;

class ProductController extends Controller
{
    private $pnum;
    private $ptype;
    private $parent;
    private $parent_link;

    public function __construct()
    {
//        $this->middleware('auth');
        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        $this->parent = 'Master Data';
        $this->parent_link = '';
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Master Articles';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

        return view('master.product.index', $data);
    }

    public function create()
    {
        $data['breadcrumbs'] = 'Master Articles';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['merchandises'] = Merchandise::select('MERC_RECID', 'MERC_CODE')->get();
        $data['units'] = Lookup::where('LOK_CODE', 'UNIT')->get();

        return view('master.product.create', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'PRO_CODE' =>'required',
            'PRO_DESCR'=> 'required',
            'PRO_UNIT' => 'required'
        ];
        $messages = [
            'PRO_CODE.required' => 'Product Code field is required.',
            'PRO_DESCR.required' => 'Product Description field is required.',
            'PRO_UNIT.required' => 'Product Unit field is required.',
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['_token', 'PRO_ACTIVE']);
        $product = new Product();
        $product->PRO_RECID = Uuid::uuid();
        $product->PRO_PNUM = $this->pnum;
        $product->PRO_PTYPE = $this->ptype;
        if($request->input('PRO_ACTIVE') != null ) {
            $product->PRO_ACTIVE = 1;
        } else {
            $product->PRO_ACTIVE = 0;
        }
        $product->fill($post_data);
        $product->save();

        $request->session()->flash('success', 'Data has been saved');
        return redirect('master/product');

    }

    public function edit($id)
    {
        $data['breadcrumbs'] = 'Master Articles';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['product'] = Product::findOrFail($id);
        $data['units'] = Lookup::where('LOK_CODE', 'UNIT')->get();
        $data['merchandises'] = Merchandise::select('MERC_CODE')->get();
        return view('master.product.create', $data);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'PRO_CODE' =>'required',
            'PRO_DESCR'=> 'required',
            'PRO_UNIT' => 'required'
        ];
        $messages = [
            'PRO_CODE.required' => 'Product Code field is required.',
            'PRO_DESCR.required' => 'Product Description field is required.',
            'PRO_UNIT.required' => 'Product Unit field is required.',
        ];

        $this->validate($request, $rules, $messages);
        $post_data = $request->except(['_token', 'PRO_ACTIVE']);
        $product = Product::findOrFail($id);
        $product->fill($post_data);
        if($request->input('PRO_ACTIVE') == 'on') {
            $product->PRO_ACTIVE = 1;
        } else {
            $product->PRO_ACTIVE = 0;
        }
        $product->save();

        $request->session()->flash('success', 'Data has been updated.');
        return redirect('master/product');

    }

    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        Product::destroy($request->input('id'));
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('master/product');
    }

    public function getData()
    {
        $products = Product::has('merchandise')
                    ->with(['merchandise', 'merchandise.segment', 'unit', 'brand']);
        return app('datatables')->of($products)
            ->addColumn('PRO_CODE', function($products) {
                if($products->PRO_CODE == null){
                    return '-';
                }
                return $products->PRO_CODE;
            })
            ->addColumn('PRO_DESCR', function($products) {
                if($products->PRO_DESCR == null){
                    return '-';
                }
                return $products->PRO_DESCR;
            })

            ->addColumn('MERC_CODE', function($products) {
                if($products->merchandise->MERC_CODE == null){
                    return '-';
                }
                return $products->merchandise->MERC_CODE;
            })

            ->addColumn('LOK_DESCRIPTION', function($products) {
                if($products->merchandise->segment->LOK_DESCRIPTION !== null){
                    
                    return $products->merchandise->segment->LOK_DESCRIPTION;
                }
                return '-';                
            })

            ->addColumn('unit', function($products) {
                if($products->unit == null){
                    return '-';
                }
                return $products->unit->LOK_DESCRIPTION;
            })

            /*->addColumn('action', function($product) {
                $edit = '<a href="'.url('master/product/'.$product->PRO_RECID).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$product->PRO_RECID.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            })*/
            ->make(true);
    }
}

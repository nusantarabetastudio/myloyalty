<?php

namespace App\Http\Controllers\Redeem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Preference;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Session;

class InventoryProductRedeemController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
        $this->parent = 'Inventory Product Redeem';
        $this->parent_link = '';
        $this->admin = Preference::where('key', 'admin')->first()->value;
        $this->superAdmin = Preference::where('key', 'superadmin')->first()->value;
        $this->cs = Preference::where('key', 'cs')->first()->value;
        $this->supervisor = Preference::where('key', 'supervisor')->first()->value;

        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        // $this->middleware('user-log', ['only' => ['index', 'addStock']]);
    }

    public function index()
    {
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $storeid = Session::get('data')->STORE->id;
        $data['breadcrumbs'] = 'Product Redeem';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        $data['cs'] = $this->cs;
        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        } else {
            $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/'.$storeid.'/product?csAccess=true';
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $data['uri'], [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['products'] = $contents->DATA;
            $data['paginator'] = $contents->paginator;
            
            return view('redeem.inventory-redeem-product.index', $data);
        }
        else {
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        $user = Session::get('user');
        $this->validate($request, [
            'inventory_qty' => 'required'
        ]);

        $data = $request->all();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product/'. $request->id .'/add-stock';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'qty' => $data['inventory_qty'],
                'userUsername' => $user->USER_USERNAME
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            return redirect()->back()->with('success_msg', $msg);
        }
        elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            return redirect()->back()->with('error_msg', $msg);
        }
        else {
            return redirect()->back()->with('error_msg', 'Error Occured!');
        }
    }

    public function addStock($id)
    {
        $data['breadcrumbs'] = 'Add Stock';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $uricategory = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/redeem-product-category';
        $uriproduct = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product/'.$id;
        $uriproductList = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        $uriunit = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/unit';
        $uritenant = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/tenant';
        $urilogs = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product/'.$id.'/logs?limit=10&page=1';
        $client = new Client(['http_errors' => false]);
        $responsetenant = $client->request('GET', $uritenant, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $responseunit = $client->request('GET', $uriunit, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $productListclient = new Client(['http_errors' => false]);
        $responsecategory = $client->request('GET', $uricategory, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $response = $client->request('GET', $uriproduct, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $productListresponse = $client->request('GET', $uriproductList, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $logsresponse = $client->request('GET', $urilogs, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200 && $productListresponse->getStatusCode() == 200 && $responsecategory->getStatusCode() == 200 && $responseunit->getStatusCode() == 200 && $responsetenant->getStatusCode() == 200 && $logsresponse->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentList = json_decode($productListresponse->getBody()->getContents());
            $contentcategory = json_decode($responsecategory->getBody()->getContents());
            $contentunits = json_decode($responseunit->getBody()->getContents());
            $contenttenant = json_decode($responsetenant->getBody()->getContents());
            $contentlogs = json_decode($logsresponse->getBody()->getContents());
            $data['product'] = $contents->DATA;
            $data['products'] = $contents->DATA;
            $data['categorys'] = $contentcategory->DATA;
            $data['units'] = $contentunits->DATA;
            $data['tenants'] = $contenttenant->DATA;
            $data['logs'] = $contentlogs->DATA;
            $data['paginator'] = $contentlogs->paginator;
            $data['idIventory'] = $id;

        	return view('redeem.inventory-redeem-product.create', $data);
        }
        elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        }
    }

    public function getData(Request $request) {
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);
        $search = $request->get('s', '');
        $type = $request->get('type', '');
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        $storeid = Session::get('data')->STORE->id;
        if (in_array($data['roleId'], [$this->admin, $this->superAdmin])) {
            $productRedeemUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        } else {
            $productRedeemUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/'.$storeid.'/product?csAccess=true&page=' . $page . '&limit=' . $limit . '&s=' . $search;
        }

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $productRedeemUri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() == 200) {
            return response()->json($contents);
        } else { // Error 500
            return response()->json('Error occured!');
        }
    }

    public function getStockData(Request $request)
    {
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);
        $search = $request->get('s', '');
        $type = $request->get('type', '');
        $idIventory = $request->get('idIventory', '');

        $productRedeemUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product/'.$idIventory.'/logs?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $productRedeemUri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() == 200) {
            return response()->json($contents);
        } else { // Error 500
            return response()->json('Error occured!');
        }
    }
}
<?php

namespace App\Http\Controllers\Redeem;
use App\Http\Controllers\Controller;
use App\Models\Admin\Post;
use App\Models\Customer;
use App\Models\CustomerPoint;
use App\Models\Preference;
use App\Models\Master\Tenant;
use Carbon\Carbon;
use App\Models\Admin\RedeemPost;
use App\Models\RedeemProductLog;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Illuminate\Http\Request;
use Session, DB, Datatables;


class RedeemTransactionController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct()
    {
        $this->parent = 'Redeem';
        $this->admin = Preference::where('key', 'admin')->first()->value;
        $this->superAdmin = Preference::where('key', 'superadmin')->first()->value;
        $this->cs = Preference::where('key', 'cs')->first()->value;
        $this->supervisor = Preference::where('key', 'supervisor')->first()->value;
        $this->parent_link = '';
        $this->menu_id = '';
        // $this->middleware('user-log', ['only' => ['index', 'redeem', 'getTransactionDetails']]);
    }

    public function index()
    {
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $data['storeId'] = Session::get('data')->STORE->id;
        $data['pnum'] = Session::get('pnum');
        $data['ptype'] = Session::get('ptype');
        $data['breadcrumbs'] = 'Product Redeem';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        $data['cs'] = $this->cs;
        $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer?active=1&pnum=' . $data['pnum'] . '&ptype=' . $data['ptype'];
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $data['uri'], [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        if($response->getStatusCode() == 200) {
            $data['customers'] = $contents->DATA;
            $data['paginator'] = $contents->paginator;
            return view('redeem.redeem-transaction.customer-list', $data);
        } else if ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $msg = $contents->MSG;

            return redirect()->route('redeem.transaction.customer')->with('notif_error', $msg);
        } else { // Error 500
            $msg = isset($contents->MSG) ? $contents->MSG : 'ERROR OCCURED!';
            return redirect()->route('redeem.transaction.customer')->with('notif_error', $msg);
        }
    }

    public function getCustomerData(Request $request)
    {
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);
        $pnum = $request->get('pnum');
        $ptype = $request->get('ptype');
        $search = $request->get('s', '');
        $type = $request->get('type', '');
        $redeemId = $request->get('redeemId' , '');

        if ($type == 'productRedeem') {
            $customerUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer?pnum=' . $pnum . '&ptype=' . $ptype . '&active=1&page=' . $page . '&limit=' . $limit . '&s=' . $search;
        }
        if ($type == 'logRedeem') {
            $customerUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/customer/' .$redeemId. '/transaction?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        }
        if ($type == 'redeemProduchistory') {
            $customerUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/customer/' .$redeemId. '/transaction/detail?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $customerUri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() == 200) {
            return response()->json($contents);
        } else { // Error 500
            return response()->json('Error occured!');
        }
    }

    public function redeem($customerId)
    {
        $data['breadcrumbs'] = 'Product Redeem';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $storeId = Session::get('data')->STORE->id;
        $data['superAdmin'] = $this->superAdmin;
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product?limit=1000&page=1&availableProductOnly=true&defaultStoreOnly=true';
        } else {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/' . $storeId . '/product?limit=1000&availableProductOnly=true';
        }
        $uricustomer = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/customer/'.$customerId.'/transaction?limit=10';
        $uriRedeemProduct = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/'). 'cs/redeem/customer/'.$customerId.'/transaction/detail?limit=1000';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $responsecustomer = $client->request('GET', $uricustomer, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $responseRedeemProduct = $client->request('GET', $uriRedeemProduct, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        $contentcustomer = json_decode($responsecustomer->getBody()->getContents());
        $contentRedeemProduct = json_decode($responseRedeemProduct->getBody()->getContents());
        if ($response->getStatusCode() == 200 && $responsecustomer->getStatusCode() == 200 && $responseRedeemProduct->getStatusCode() == 200) {
            $data['user'] = Session::get('data');
            if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
                $data['storeId'] = Tenant::where('TNT_DEFAULT', 1)->first()->TNT_RECID;
                $data['storeName'] =Tenant::where('TNT_DEFAULT', 1)->first()->TNT_DESC;
            } else {
                $data['storeId'] = $data['user']->STORE->id;
                $data['storeName'] = $data['user']->STORE->name;
            }
            $data['tgl'] = Carbon::now();
            $data['products'] = $contents->DATA;
            $data['customers'] = $contentcustomer->DATA->customer;
            $data['transactions'] = $contentcustomer->DATA->redeemTransactions;
            $data['paginatorRedeemTransaction'] = $contentcustomer->paginator;
            $data['redeemProducts'] = $contentRedeemProduct->DATA->redeemTransactionDetails;
            $data['paginatorredeemProducts'] = $contentRedeemProduct->paginator;
            $data['cs'] = $this->cs;
            $data['supervisor'] = $this->supervisor;
            $data['admin'] = $this->admin;
            $data['superAdmin'] = $this->superAdmin;
            $data['customerId'] = $customerId;

            return view('redeem.redeem-transaction.transaction-list', $data);
        } else if ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $msg = $contents->MSG;

            return redirect()->back()->with('notif_error', $msg);
        } else if ($responsecustomer->getStatusCode() == 500) {
            $msg = isset($contentcustomer->MSG) ? $contentcustomer->MSG : 'ERROR OCCURED!';

            return redirect()->back()->with('notif_error', $msg);
        }
    }

    public function void(Request $request)
    {
        $storeId = Session::get('data');
        $data = $request->except(['_token', 'username']);
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/'.$data['redeemId'].'/void';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'multipart' => [
                [
                    'name'      => 'pnum',
                    'contents'  => $storeId->PNUM
                ],
                [
                    'name'      => 'ptype',
                    'contents'  => $storeId->PTYPE
                ],
                [
                    'name'      => 'tenantId',
                    'contents'  => $storeId->STORE->id
                ],
                [
                    'name'      => 'userUsername',
                    'contents'  => $storeId->USER_DATA->USER_USERNAME
                ]
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        if($response->getStatusCode() == 200){
            $msg = $contents->MSG;
            return redirect()->back()->with('notif_success', $msg);
        }else if($response->getStatusCode() == 422 || $response->getStatusCode() == 404){
            $msg = $contents->MSG;
            return redirect()->back()->with('notif_error', $msg);
        } else {
            $msg = isset($contents->MSG) ? $contents->MSG : 'ERROR OCCURED!';

            return redirect()->back()->with('notif_error', $msg);
        }
    }

    public function viewBook()
    {
        return view('redeem.redeem-transaction.view-book');
    }

    public function detailBook($bookCode)
    {
        $redeemPost = RedeemPost::with(['customer', 'details.redeemProduct'])->where('booking_code', $bookCode)->first();
        if (!$redeemPost) return view('redeem.redeem-transaction.detail-book')->withErrors('booking redeem with code #'. $bookCode . ' not found');
         return view('redeem.redeem-transaction.detail-book', compact('redeemPost'));
    }

    public function cekValidate(Request $request)
    {
        $tenant = Session::get('data')->STORE->id;
        $user = Session::get('user')->USER_USERNAME;
        $redeemPost = RedeemPost::with(['details.redeemProduct', 'tenant'])->where('booking_code', $request->booking_code)->first();

        if (!$redeemPost) return redirect()->back()->withErrors('Booking redeem with code #'. $request->booking_code . ' not found');

        if ($redeemPost->tenant_id != $tenant) return redirect()->back()->withErrors('Cannot validate booking redeem on tenant #'. $redeemPost->tenant->TNT_DESC);

        $point = $redeemPost->point;
        $redeemPostId= $redeemPost->id;

        DB::beginTransaction();
        try {
            $redeemPost->posted_by = $user;
            $redeemPost->booking_code = null;
            $redeemPost->expired_in = null;
            $redeemPost->save();

            $redeemProductLogs = RedeemProductLog::where('redeem_post_id', $redeemPostId)->get();
            foreach($redeemProductLogs as $redeemProductLog) {
                $redeemProductLog->added_by = $user;
                $redeemProductLog->save();
            }

            $customerPoint = CustomerPoint::where('customer_id',$redeemPost->customer_id)->redeemPoint()->first();
            $customerPoint->point = $customerPoint->point - $point;
            $customerPoint->save();
        } catch (ValidationException $e) {
            DB::rollBack();

            throw new ValidationException($e->validator, $e->getResponse());
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->withErrors('Booking Redeem Failed! Please try again!');
        }

        DB::commit();
        return redirect()->back()->with('notif_success', 'Data has been saved');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'products[1][id]' => '',
            'products[1][qty]' => '']);
        $user = Session::get('user');
        $data = $request->all();
        $data['userUsername'] = $user->USER_USERNAME;
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/process';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        $contents = json_decode($response->getBody()->getContents());
        if($response->getStatusCode() == 200) {
            $msg = $contents->MSG;
            return redirect()->back()->with('notif_success', $msg);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $msg = $contents->MSG;
            return redirect()->back()->with('notif_error', $msg);
        } else {
            $msg = isset($contents->MSG) ? $contents->MSG : 'ERROR OCCURED!';

            return redirect()->back()->with('notif_error', $msg);
        }
    }

    public function getTransactionDetails($transactionId) {
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/transaction/'.$transactionId.'/detail';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        if($response->getStatusCode() == 200) {
            $transactions= $contents->DATA;

            return view('redeem.redeem-transaction.transaction-detail-modal', compact('transactions'));
        }
        else if ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $msg = $contents->MSG;

            return redirect()->back()->with('notif_error', $msg);
        } else {
            $msg = isset($contents->MSG) ? $contents->MSG : 'ERROR OCCURED!';

            return redirect()->back()->with('notif_error', $msg);
        }
    }
}

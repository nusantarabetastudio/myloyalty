<?php
namespace App\Http\Controllers\Redeem;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Preference;
use Carbon\Carbon;
use File, DB;
use App\Models\Counter;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use App\Models\Master\Tenant;
use App\Models\Admin\RedeemProduct;
use Faker\Provider\Uuid;
use App\Models\RedeemProductLog;
use Session;

class RedeemProductController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
        $this->parent = 'Product Redeem';
        $this->parent_link = '';
        $this->admin = Preference::where('key', 'admin')->first()->value;
        $this->superAdmin = Preference::where('key' , 'superadmin')->first()->value;
        $this->pnum = \Session::get('pnum');
        $this->ptype = \Session::get('ptype');
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $storeid = Session::get('data')->STORE->id;
        $data['breadcrumbs'] = 'Product Redeem';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        } else {
            $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/'.$storeid.'/product';
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $data['uri'], [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['products'] = $contents->DATA;
            $data['paginator'] = $contents->paginator;

            return view('redeem.redeem-product.index', $data);

        } else if ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;

            return redirect()->route('redeem.transaction.customer')->with('notif_error', $msg);
        } else {
            return redirect()->route('redeem.transaction.customer')->with('notif_error', 'Error Occured!');
        }
    }

    public function create()
    {
        $data['breadcrumbs'] = 'Product Redeem Create';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $uriunit = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/unit';
        $client = new Client(['http_errors' => false]);
        $responseunit = $client->request('GET', $uriunit, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($responseunit->getStatusCode() == 200) {
            $contentunits = json_decode($responseunit->getBody()->getContents());
            $data['units'] = $contentunits->DATA;
        }
        $uricategory = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/redeem-product-category';
        $responsecategory = $client->request('GET', $uricategory, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($responsecategory->getStatusCode() == 200) {
            $contentcategory = json_decode($responsecategory->getBody()->getContents());
            $data['categorys'] = $contentcategory->DATA;
        }
        $uritenant = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/tenant';
        $responsetenant = $client->request('GET', $uritenant, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($responsetenant->getStatusCode() == 200) {
            $contenttenant = json_decode($responsetenant->getBody()->getContents());
            $data['tenants'] = $contenttenant->DATA;
        }
        return view('redeem.redeem-product.create', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
            'price'       => 'required|numeric|min:0',
            'point'       => 'required|integer|min:0'
        ]);
        $counter = Counter::where('pnum', $this->pnum)->where('ptype', $this->ptype)->first();
        $redeemProductCount = $counter->redeem_product_count;

        if ($redeemProductCount >= 9999) {
            return redirect()->back()->withErrors('Max Redeem Product Code reached (9999)');
        }

        if (!$request->tenantId) {
            return redirect()->back()->withErrors('The Tenant field is required');
        }

        $isLast = false;
        $i = 0;
        DB::beginTransaction();
        try {
            foreach($request->tenantId as $tenantId) {
                $tenant = Tenant::find($tenantId);
                if (isEmpty($tenant)) return redirect()->back()->withErrors('Tenant with ID #' . $tenantId . ' not found.');
                if ($i+1 == count($request->tenantId)) $isLast = true;
                $redeemProduct = new RedeemProduct();
                $redeemProduct->id = Uuid::uuid();
                $redeemProduct->description = $request->description;
                $redeemProduct->subtitle = $request->subtitle;
                $redeemProduct->price = $request->price;
                $redeemProduct->point = $request->point;
                $redeemProduct->from_date = Carbon::parse($request->fromDate);
                $redeemProduct->to_date = Carbon::parse($request->toDate);
                if($request->hasFile('image1')){
                    $redeemProduct->first_image_url = uploadToS3($request->file('image1'), 'uploads/product_redeem');
                }
                if($request->hasFile('image2')){
                    $redeemProduct->second_image_url = uploadToS3($request->file('image2'), 'uploads/product_redeem');
                }
                $redeemProduct->qty = 0;
                $redeemProduct->tenant_id = $tenantId;
                $redeemProduct->unit_id = $request->unitId;
                $redeemProduct->product_redeem_category_id = $request->categoryId;
                $redeemProduct->comment = ($request->comment) ? $request->comment : null;
                $redeemProduct->comment_ind = ($request->comment_ind) ? $request->comment_ind : null;
                $redeemProduct->is_voucher = ($request->voucher) ? 1 : 0;
                if ($redeemProduct->is_voucher != 0) {
                    $redeemProduct->product_code = 'V' . generateLeadingZero($redeemProductCount + 1, 4);
                } else {
                    $redeemProduct->product_code = 'P' . generateLeadingZero($redeemProductCount + 1, 4);
                }
                $redeemProduct->active = ($request->active) ? 1 : 0;
                $redeemProduct->update = 1;
                $redeemProduct->save();
            }
            $counter->redeem_product_count += 1;
            $counter->save();
        } catch (ValidationException $e) {
            DB::rollBack();

            throw new ValidationException($e->validator, $e->getResponse());
        } catch (Exception $e) {
            DB::rollBack();

            return validationError('Add Product Failed! Please try again!');
        }
        DB::commit();

        return redirect()->route('redeem.product.index')->with('success_msg', 'Redeem Product has been stored successfully!');
    }

    public function update(Request $request, $id)
    {
        $redeemProduct = RedeemProduct::find($id);
        if (!$redeemProduct) return redirect()->back()->withErrors('Data with ID#' . $id . ' not Found.');
        DB::beginTransaction();
        try {
            $oldQty = $redeemProduct->qty;
            $newQty = $request->qty;
            $redeemProduct->description = $request->description;
            $redeemProduct->subtitle = $request->subtitle;
            $redeemProduct->price = $request->price;
            $redeemProduct->point = $request->point;
            $redeemProduct->from_date = Carbon::parse($request->fromDate);
            $redeemProduct->to_date = Carbon::parse($request->toDate);
            if($request->hasFile('image1')){
                $redeemProduct->first_image_url = uploadToS3($request->file('image1'), 'uploads/product_redeem');
            }
            if($request->hasFile('image2')){
                $redeemProduct->second_image_url = uploadToS3($request->file('image2'), 'uploads/product_redeem');
            }
            $redeemProduct->qty = $newQty;
            $redeemProduct->tenant_id = $request->tenantId;
            $redeemProduct->unit_id = $request->unitId;
            $redeemProduct->product_redeem_category_id = $request->categoryId;
            $redeemProduct->comment = ($request->comment) ? $request->comment : null;
            $redeemProduct->comment_ind = ($request->comment_ind) ? $request->comment_ind : null;
            $redeemProduct->is_voucher = ($request->voucher) ? 1 : 0;
            $redeemProduct->active = ($request->active) ? 1 : 0;
            $redeemProduct->update = 1;
            $redeemProduct->save();

            if ($oldQty != $newQty) {
                $log = new RedeemProductLog;
                $log->qty = $newQty - $oldQty;
                $log->redeem_product_id = $id;
                $log->added_by = user()->USER_USERNAME;
                $log->added_at = date('Y-m-d H:i:s');
                $log->save();
            }
        } catch (ValidationException $e) {
            DB::rollBack();

            throw new ValidationException($e->validator, $e->getResponse());
        } catch (Exception $e) {
            DB::rollBack();

            return validationError('Add Product Failed! Please try again!');
        }
        DB::commit();

        return redirect()->route('redeem.product.index')->with('success_msg', 'Redeem Product has been updated successfully!');
    }

    public function edit($id)
    {
        $data['breadcrumbs'] = 'Product Redeem Edit';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $uricategory = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/redeem-product-category';
        $uriproduct = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product/'.$id;
        $uriproductList = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        $uriunit = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/unit';
        $uritenant = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'master/tenant';
        $client = new Client(['http_errors' => false]);
        $responsetenant = $client->request('GET', $uritenant, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $responseunit = $client->request('GET', $uriunit, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $productListclient = new Client(['http_errors' => false]);
        $responsecategory = $client->request('GET', $uricategory, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $response = $client->request('GET', $uriproduct, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $productListresponse = $client->request('GET', $uriproductList, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200 && $productListresponse->getStatusCode() == 200 && $responsecategory->getStatusCode() == 200 && $responseunit->getStatusCode() == 200 && $responsetenant->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentList = json_decode($productListresponse->getBody()->getContents());
            $contentcategory = json_decode($responsecategory->getBody()->getContents());
            $contentunits = json_decode($responseunit->getBody()->getContents());
            $contenttenant = json_decode($responsetenant->getBody()->getContents());
            $data['product'] = $contents->DATA;
            $data['products'] = $contents->DATA;
            $data['categorys'] = $contentcategory->DATA;
            $data['units'] = $contentunits->DATA;
            $data['tenants'] = $contenttenant->DATA;
            return view('redeem.redeem-product.create', $data);
        }
        else {
            return redirect()->back();
        }

    }

    public function destroy(Request $request)
    {
        $data = $request->except(['_token', 'username']);
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product/'.$data['productId'];
        $client = new Client([
            'http_errors' => false
        ]);
        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if ($response->getStatusCode() == 200) {
            return redirect()->back()->with('success_msg', 'Data has been deleted');
        } else {
            return redirect()->back()->with('error_msg', 'Cannot delete data!');
        }
    }

    public function getRedeemData(Request $request)
    {
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);
        $search = $request->get('s', '');
        $type = $request->get('type', '');
        $productRedeemUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $productRedeemUri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() == 200) {
            return response()->json($contents);
        } else { // Error 500
            return response()->json('Error occured!');
        }
    }

}

<?php

namespace App\Http\Controllers;

use DB;
use Image;
use Storage;
use Carbon\Carbon;
use Excel, File, CSV;
use App\Http\Requests;
use GuzzleHttp\Client;
use App\Models\Counter;
use App\Models\Customer;
use Faker\Provider\Uuid;
use App\Models\Admin\Post;
use App\Models\Preference;
use App\Models\CustomerLog;
use App\Models\CardExchange;
use App\Models\CustomerAddr;
use Illuminate\Http\Request;
use App\Models\CustomerEmail;
use App\Models\CustomerPhone;
use App\Models\CustomerPoint;
use App\Models\Master\Lookup;
use App\Models\Master\Tenant;
use App\Models\CustomerPicture;
use App\Models\Master\CardType;
use App\Models\Admin\PostDetail;
use App\Models\CustomerExchange;
use App\Models\CustomerInterest;
use Validator, Datatables, Session;
use App\Traits\CanCreateNewProptypeCounter;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

class CustomerController extends Controller
{
    use CanCreateNewProptypeCounter;

    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct(Session $session)
    {
        $this->parent = 'Customers';
        $this->parent_link = '';
        $this->menu_id = '';
        $this->cs = Preference::where('key', 'cs')->first()->value;
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit', 'detail', 'getAddresses', 'editAddress', 'getPhones', 'editPhone', 'getEmails', 'editEmail', 'getInterests', 'editInterest', 'getCardExchanges', 'editCardExchange', 'getCustomerLogs', 'getCustomerTransactionHistory', 'total']]);
    }

    public function index(Request $request) {
        $data['breadcrumbs'] = 'Search';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        $data['search'] = $request->get('s');
        $data['searchby'] = $request->get('searchby');

        $data['isCs'] = Session::get('role_data')->ROLE_RECID == $this->cs;
        $data['customers'] = Customer::selectRaw('CUST_RECID, CUST_NAME, CUST_BARCODE, PHN_NUMBER, CUST_IDCARDS')
                ->leftJoin('customers_phone', function ($join) {
                    $join->on('customers_phone.PHN_CUST_RECID', '=', 'customers.CUST_RECID')
                    ->where('PHN_TYPE', '=', 1);
                })
                ->where('CUST_PNUM', $this->pnum)
                ->where('CUST_PTYPE', $this->ptype)
                ->when($request->has('s'), function($qry) use($data) {
                    $search = $data['search'];
                    $searchby = $data['searchby'];

                    $replacedSearch = str_replace(' ', '%', $search);

                    if ($searchby == 'phone') {
                        return $qry->where(function($qry) use($search, $replacedSearch) {
                                    $qry->where('PHN_NUMBER', 'LIKE', "%{$search}%");
                                });
                    } elseif ($searchby == 'name') {
                        return $qry->where(function($qry) use($search, $replacedSearch) {
                                    $qry->where('CUST_NAME', 'LIKE', "%{$replacedSearch}%");
                                });
                    } elseif ($searchby == 'barcode') {
                        return $qry->where(function($qry) use($search, $replacedSearch) {
                                    $qry->where('CUST_BARCODE', 'LIKE', "%{$search}%");
                                });
                    } elseif ($searchby == 'idcard') {
                        return $qry->where(function($qry) use($search, $replacedSearch) {
                                    $qry->where('CUST_IDCARDS', 'LIKE', "%{$search}%");
                                });
                    } else {
                        return $qry->where(function($qry) use($search, $replacedSearch) {
                                    $qry->where('CUST_NAME', 'LIKE', "%{$replacedSearch}%")
                                    ->orWhere('CUST_BARCODE', 'LIKE', "%{$search}%")
                                    ->orWhere('PHN_NUMBER', 'LIKE', "%{$search}%")
                                    ->orWhere('CUST_IDCARDS', 'LIKE', "%{$search}%");
                                });
                    }
                })
                ->orderBy('CUST_BARCODE', 'DESC')
                ->simplePaginate(10);

        return view('customer.index', $data);
    }

    public function showTransactionDetailReceipt($id)
    {
        $earnings = PostDetail::selectRaw('PSD_DOC_NO, SUM(PSD_AMOUNT) as total_amount')->where('PSD_POS_RECID', $id)->groupBy('PSD_DOC_NO')->get();

        return view('customer.points_history_view', compact('id', 'earnings'));
    }

    public function showTransactionDetailProduct($id, $receiptNo)
    {
        $earnings = PostDetail::with(['article'])->where('PSD_POS_RECID', $id)->where('PSD_DOC_NO', $receiptNo)->get();

        return view('customer.points_history_product_list', compact('receiptNo', 'earnings'));
    }

    public function create()
    {
        $data['id_types'] = Lookup::where('LOK_CODE', 'TPID')->get();
        $data['marital_statuses'] = Lookup::where('LOK_CODE', 'MART')->get();
        $data['interests'] = Lookup::where('LOK_CODE', 'INTR')->get();
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();
        $data['areas'] = Lookup::where('LOK_CODE', 'AREA')->get();
        $data['nationalities'] = Lookup::where('LOK_CODE', 'NATL')->get();
        $data['bloods'] = Lookup::where('LOK_CODE', 'BLOD')->get();
        $data['religions'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['occupations'] = Lookup::where('LOK_CODE', 'OCCP')->get();
        $data['mem_types'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['cardTypes'] = CardType::where('CARD_ACTIVE', 1)->where('CARD_PNUM', Session::get('pnum'))->where('CARD_PTYPE', Session::get('ptype'))->get();
        $data['genders'] = Lookup::where('LOK_CODE', 'GEN')->get();

        return view('customer.create', $data);
    }

    public function store(Request $requests) {
        $rules = [
            'CUST_NAME'=> 'required',
            'CUST_IDTYPE' => 'required',
            'CUST_BARCODE' => 'required|digits:10|unique:Customers,CUST_BARCODE',
            'CUST_IDCARDS' => 'required',
            'CUST_CARDTYPE' => 'required',
            'EML_EMAIL' => 'required',
            'PHN_MOBILE' => 'required'
        ];
        $messages = [
            'CUST_NAME.required' => 'Customer Name field is required',
            'CUST_BARCODE.required' => 'Barcode field is required'

        ];
        $this->validate($requests,$rules,$messages);
        $customerIdNumber = Customer::where('CUST_IDCARDS', $requests->CUST_IDCARDS)
                            ->first();
        if ($customerIdNumber) return redirect()->back()->withErrors('ID number has been used by another customer');
        DB::beginTransaction();
        try {
            if (CustomerExchange::where('EXC_OLDCARD', $requests->CUST_BARCODE)->first()) return validationError('Barcode is already taken.');
            $userPnum = Session::get('pnum');
            $userPtype = Session::get('ptype');
            $proptypeCounter = Counter::where('pnum', $userPnum)->where('ptype', $userPtype)->first();
            $proptypeCounter->customer_barcode_count += 1;
            $proptypeCounter->save();
            $customerCode = $proptypeCounter->customer_barcode_count;
            $customer = new Customer();
            $customer->CUST_RECID = strtoupper(Uuid::uuid());
            $customer->CUST_CODE = $userPnum . $userPtype . prefix($customerCode, 10);
            $customer->CUST_STORE = Session::get('data')->STORE->id;
            $customer->CUST_MEMTYPE = '00065099-F571-4FF6-9258-866726445731'; // Regular Customer
            $customer->CUST_PNUM = $userPnum;
            $customer->CUST_PTYPE = $userPtype;
            $customer->CUST_BARCODE = $requests->CUST_BARCODE;
            $customer->CUST_NAME = $requests->CUST_NAME;
            $customer->CUST_IDTYPE = $requests->CUST_IDTYPE ? $requests->CUST_IDTYPE : null;
            $customer->CUST_IDCARDS = $requests->CUST_IDCARDS ? $requests->CUST_IDCARDS: null;
            $customer->CUST_GENDER = $requests->CUST_GENDER ? $requests->CUST_GENDER : null;
            $customer->CUST_MARITAL = $requests->CUST_MARITAL ? $requests->CUST_MARITAL : null;
            $customer->CUST_POD = $requests->CUST_POD ? $requests->CUST_POD: null;
            $customer->CUST_NATIONALITY = $requests->CUST_NATIONALITY ? $requests->CUST_NATIONALITY : null;
            $customer->CUST_BLOOD = $requests->CUST_BLOOD ? $requests->CUST_BLOOD : null;
            $customer->CUST_RELIGION = $requests->CUST_RELIGION ? $requests->CUST_RELIGION: null;
            $customer->CUST_OCCUPATION = $requests->CUST_OCCUPATION  ? $requests->CUST_OCCUPATION : null;
            $customer->CUST_CARDTYPE = $requests->CUST_CARDTYPE ? $requests->CUST_CARDTYPE: null;
            if ($requests->hasFile('profile_picture')) {
                $customer->CUST_PROFILEPICTURE = uploadToS3($requests->file('profile_picture'), 'uploads/profile_pictures');
            }
            // On Create New Cust, Always active
            $customer->CUST_ACTIVE = 1;
            $customer->CUST_DOB = $requests->input('CUST_DOB') ? dateFormat($requests->input('CUST_DOB')) : null;
            $customer->CUST_JOINDATE = Carbon::now();
            $customer->CUST_SYSTEMDATE = Carbon::now();
            $customer->CUST_EXPIREDATE = Carbon::now()->addYear();
            $customer->CUST_EXTENDATE = Carbon::now()->addYear();
            do {
                $apiToken = generateApiToken();
                $hasDuplicate = Customer::where('api_token', $apiToken)->first();
            } while ($hasDuplicate);
            $customer->api_token = $apiToken;
            $customer->registered_via = 'Backend';
            $customer->save();

            $phone_mobile = new CustomerPhone();
            $phone_mobile->PHN_RECID = Uuid::uuid();
            $phone_mobile->PHN_CUST_RECID = $customer->CUST_RECID;
            $phone_mobile->PHN_TYPE = 1;
            $phone_mobile->PHN_NUMBER = ltrim($requests->input('PHN_MOBILE'), '0');
            $phone_mobile->PHN_ACTIVE = 1;
            $phone_mobile->save();

            $phone_home = new CustomerPhone();
            $phone_home->PHN_RECID = Uuid::uuid();
            $phone_home->PHN_CUST_RECID = $customer->CUST_RECID;
            $phone_home->PHN_TYPE = 7;
            $phone_home->PHN_NUMBER = $requests->input('PHN_HOME');
            $phone_home->PHN_ACTIVE = 1;
            $phone_home->save();

            $phone_office = new CustomerPhone();
            $phone_office->PHN_RECID = Uuid::uuid();
            $phone_office->PHN_CUST_RECID = $customer->CUST_RECID;
            $phone_office->PHN_TYPE = 4;
            $phone_office->PHN_NUMBER = $requests->input('PHN_OFFICE');
            $phone_office->PHN_ACTIVE = 1;
            $phone_office->save();

            if ($requests['ADDR_ADDRESS']) {
                $address = new CustomerAddr();
                $post_address = $requests->only(['ADDR_ADDRESS', 'ADDR_COUNTRY', 'ADDR_POSTAL']);
                $addressData = $requests->only(['ADDR_PROVINCE', 'ADDR_CITY', 'ADDR_AREA']);
                $address->ADDR_PROVINCE = !empty($addressData['ADDR_PROVINCE']) ? $addressData['ADDR_PROVINCE'] : null;
                $address->ADDR_CITY = !empty($addressData['ADDR_CITY']) ? $addressData['ADDR_CITY'] : null;
                $address->ADDR_AREA = !empty($addressData['ADDR_AREA']) ? $addressData['ADDR_AREA'] : null;
                $address->ADDR_RECID = Uuid::uuid();
                $address->ADDR_CUST_RECID = $customer->CUST_RECID;
                $address->ADDR_TYPE =1 ; // 1 = KTP
                $address->fill($post_address);
                $address->save();
            }

            $email = new CustomerEmail();
            $post_email = $requests->input('EML_EMAIL');
            $email->EML_RECID = Uuid::uuid();
            $email->EML_EMAIL = (empty($post_email)) ? 'y@y.com' : $post_email;
            $email->EML_CUST_RECID = $customer->CUST_RECID;
            $email->EML_TYPE = 1;
            $email->save();

            if ($requests['interests'])
                foreach ($requests['interests'] as $interest) {
                    $customerInterest = new CustomerInterest();
                    $customerInterest->INT_RECID = Uuid::uuid();
                    $customerInterest->INT_CUST_RECID = $customer->CUST_RECID;
                    $customerInterest->INT_CATCODE = $interest;
                    $customerInterest->INT_ACTIVE = 1;
                    $customerInterest->INT_UPDATE = 1;
                    $customerInterest->save();
                }

            $registrationRewardPoint = Preference::where('key', 'registration_earn_reward')->first();
            $registrationRewardPoint = $registrationRewardPoint ? $registrationRewardPoint->value : 0;

            $this->addCustomerPoint($registrationRewardPoint, $customer);

            $customerExchange = new CustomerExchange;
            $customerExchange->EXC_RECID = Uuid::uuid();
            $customerExchange->EXC_CUST_RECID = $customer->CUST_RECID;
            $customerExchange->EXC_OLDCARD = NULL;
            $customerExchange->EXC_NEWCARD = $customer->CUST_BARCODE;
            $customerExchange->EXC_OLDCARDTYPE = NULL;
            $customerExchange->EXC_NEWCARDTYPE = $customer->CUST_CARDTYPE;
            $customerExchange->EXC_DATE = date('Y-m-d');
            $customerExchange->EXC_TIME = date('H:i:s');
            $customerExchange->EXC_USER = user()->USER_USERNAME;
            $customerExchange->EXC_TYPE = 7;
            $customerExchange->EXC_PNUM = Session::get('pnum');
            $customerExchange->EXC_PTYPE = Session::get('ptype');
            $customerExchange->EXC_TNT_RECID = $customer->CUST_STORE;
            $customerExchange->save();
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->withErrors('Add Customer Failed! Please try again!');
        }
        DB::commit();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$customer->CUST_RECID.'/log';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'storeId' => Session::get('data')->STORE->id,
                'type' => 'Register',
                'oldCard' => '',
                'newCard' => $requests->CUST_BARCODE,
                'userUsername' => user()->USER_USERNAME
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        $roleId = Session::get('role_data')->ROLE_RECID;
        if ($roleId == $this->cs) {
            $requests->session()->flash('success', 'Success on Storing a New Customer');
            return redirect()->route('customer.detail', $customer->CUST_RECID)->with('success', 'Success on Storing a New Customer');
        } else {
            //return redirect(action('CustomerController@edit', $customer->CUST_RECID));
            $requests->session()->flash('success', 'Success on Storing a New Customer');
            return redirect()->route('customer.detail', $customer->CUST_RECID)->with('success', 'Success on Storing a New Customer');
        }
    }

    private function addCustomerPoint($registrationRewardPoint, $customer) {
        $pnum = Session::get('pnum');
        $ptype = Session::get('ptype');

        $point = new CustomerPoint;
        $point->id = Uuid::uuid();
        $point->customer_id = $customer->CUST_RECID;
        $point->pnum = Session::get('pnum');
        $point->ptype = Session::get('ptype');
        $point->code = 2;
        $point->point = $registrationRewardPoint;
        $point->save();

        $post = new Post;
        $post->POS_RECID = Uuid::uuid();
        $post->POS_CUST_RECID = $customer->CUST_RECID;
        $post->POS_PNUM = $pnum;
        $post->POS_PTYPE = $ptype;
        $post->POS_STORE = $customer->CUST_STORE;
        $post->POS_BARCODE = $customer->CUST_BARCODE;
        $post->POS_POST_DATE = date('Y-m-d');
        $post->POS_POST_TIME = date('H:i:s');
        $post->POS_STATION_ID = '-';
        $post->POS_SHIFT_ID = '-';
        $post->POS_DOC_NO = '-';
        $post->POS_CASHIER_ID = '-';
        $post->POS_AMOUNT = 0;
        $post->POS_POINT_LD = 0;
        $post->POS_POINT_REWARD = $registrationRewardPoint;
        $post->POS_POINT_REGULAR = 0;
        $post->POS_POINT_BONUS = 0;
        $post->POS_RECEIPT_TYPE = 2;
        $post->POS_USERBY = 'REWARD';
        $post->POS_UPDATE = 1;
        $post->POS_TYPE = 3; // Reward on Registration Type
        $post->POS_VOID = 0;
        $postCount = $this->getEarningCount($pnum, $ptype);
        $post->POS_RECEIPT_NO = 'EA/' . date('y') . '/' . prefix($postCount, 7);
        $post->save();
    }

    private function getEarningCount($pnum, $ptype) {
        $proptypeCounter = Counter::where('pnum', $pnum)->where('ptype', $ptype)->first();
        if (empty(trim($proptypeCounter))) {
            $proptypeCounter = $this->addNewProptypeCounter($pnum, $ptype);
        }
        $proptypeCounter->post_count += 1;
        $proptypeCounter->save();

        return $proptypeCounter->post_count;
    }

    public function edit($id) {
        $roleId = Session::get('role_data')->ROLE_RECID;
        if ($roleId == $this->cs) {
            return redirect()->route('customer.detail', $id);
        }

        $data['breadcrumbs'] = 'Customer Edit';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;

        $data['id_types'] = Lookup::where('LOK_CODE', 'TPID')->get();
        $data['marital_statuses'] = Lookup::where('LOK_CODE', 'MART')->get();
        $data['nationalities'] = Lookup::where('LOK_CODE', 'NATL')->get();
        $data['bloods'] = Lookup::where('LOK_CODE', 'BLOD')->get();
        $data['religions'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['occupations'] = Lookup::where('LOK_CODE', 'OCCP')->get();
        $data['mem_types'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['cardTypes'] = CardType::where('CARD_ACTIVE', 1)->where('CARD_PNUM', Session::get('pnum'))->where('CARD_PTYPE', Session::get('ptype'))->get();
        $data['genders'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['customer'] = Customer::findOrFail($id);
        return view('customer.create', $data);
    }

    public function detail($id)
    {
        $data['breadcrumbs'] = 'Customer Detail';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;

        $data['id_types'] = Lookup::where('LOK_CODE', 'TPID')->get();
        $data['marital_statuses'] = Lookup::where('LOK_CODE', 'MART')->get();
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();
        $data['provinces'] = Lookup::where('LOK_CODE', 'PROV')->get();
        $data['cities'] = Lookup::where('LOK_CODE', 'CITY')->get();
        $data['areas'] = Lookup::where('LOK_CODE', 'AREA')->get();
        $data['nationalities'] = Lookup::where('LOK_CODE', 'NATL')->get();
        $data['bloods'] = Lookup::where('LOK_CODE', 'BLOD')->get();
        $data['religions'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['occupations'] = Lookup::where('LOK_CODE', 'OCCP')->get();
        $data['mem_types'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['card_types'] = Lookup::where('LOK_CODE', 'CTEX')->get();
        $data['genders'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['customer'] = Customer::with('store')->findOrFail($id);

        return view('customer.detail', $data);
    }

    public function update(Request $requests, $id) {
        DB::beginTransaction();
        try {
            $customers = Customer::where('CUST_RECID', $id)->first();

            $rules = [
                'CUST_NAME'=> 'required',
                'CUST_IDCARDS' => 'required',
            ];
            $messages = [
                'CUST_NAME.required' => 'Customer Name field is required'
            ];

            $this->validate($requests,$rules,$messages);

            $customerIdNumber = Customer::where('CUST_IDCARDS', $requests->CUST_IDCARDS)
                                ->where('CUST_RECID', '!=', $id)
                                ->first();
            if($customerIdNumber) return redirect()->back()->withErrors('ID number has been used by another customer');

            $customer = Customer::find($id);
            $oldCustomerActive = $customer->CUST_ACTIVE;
            $oldCustomerName = $customer->CUST_NAME;
            $oldCustomerIdCard = $customer->CUST_IDCARDS;
            $customer->CUST_NAME = $requests->CUST_NAME;
            $customer->CUST_IDTYPE = $requests->CUST_IDTYPE;
            $requests->CUST_IDCARDS ? $customer->CUST_IDCARDS = $requests->CUST_IDCARDS :'';
            $requests->CUST_GENDER ? $customer->CUST_GENDER = $requests->CUST_GENDER : '';
            $requests->CUST_MARITAL ? $customer->CUST_MARITAL = $requests->CUST_MARITAL : '';
            $requests->CUST_POD ? $customer->CUST_POD = $requests->CUST_POD : '';
            $requests->CUST_NATIONALITY ? $customer->CUST_NATIONALITY = $requests->CUST_NATIONALITY : '';
            $requests->CUST_BLOOD ? $customer->CUST_BLOOD = $requests->CUST_BLOOD : '';
            $requests->CUST_RELIGION ? $customer->CUST_RELIGION = $requests->CUST_RELIGION :'';
            $requests->CUST_OCCUPATION ? $customer->CUST_OCCUPATION = $requests->CUST_OCCUPATION : '';
            $requests->CUST_CARDTYPE ? $customer->CUST_CARDTYPE = $requests->CUST_CARDTYPE : '';
            $requests->CUST_DOB ? $customer->CUST_DOB = dateFormat($requests->input('CUST_DOB')) : '';
            if ($requests->hasFile('profile_picture')) {
                $customer->CUST_PROFILEPICTURE = uploadToS3($requests->file('profile_picture'), 'uploads/profile_pictures');
            }
            if($requests->input('CUST_ACTIVE')  == 1) {
                $customer->CUST_ACTIVE = 1;
            } else {
                $customer->CUST_ACTIVE = 0;
            }
            $customer->save();

            if ($requests->CUST_ACTIVE != $oldCustomerActive) {
                $customerLog = new CustomerLog;
                $customerLog->id = strtoupper(Uuid::uuid());
                $customerLog->customer_id = $customer->CUST_RECID;
                $customerLog->store_id = Session::get('data')->STORE->id;
                $customerLog->old_data = $oldCustomerActive ? 'Active' : 'Inactive';
                $customerLog->new_data = $requests->CUST_ACTIVE ? 'Active' : 'Inactive';
                $customerLog->type = 'Edit Active';
                $customerLog->date = date('Y-m-d H:i:s');
                $customerLog->user_username = user()->USER_USERNAME;
                $customerLog->save();
            }

            /*$phone_mobile = CustomerPhone::where('PHN_CUST_RECID', $id)->where('PHN_TYPE', 1)->first();
            $phone_mobile->PHN_NUMBER = $requests->input('PHN_MOBILE');
            $phone_mobile->save();

            $phone_mobile = CustomerPhone::where('PHN_CUST_RECID', $id)->where('PHN_TYPE', 7)->first();
            $phone_mobile->PHN_NUMBER = $requests->input('PHN_HOME');
            $phone_mobile->save();

            $phone_mobile = CustomerPhone::where('PHN_CUST_RECID', $id)->where('PHN_TYPE', 4)->first();
            $phone_mobile->PHN_NUMBER = $requests->input('PHN_OFFICE');
            $phone_mobile->save();

            $post_address = $requests->only(['ADDR_ADDRESS', 'ADDR_CITY', 'ADDR_PROVINCE', 'ADDR_COUNTRY', 'ADDR_AREA', 'ADDR_POSTAL']);
            $address = CustomerAddr::where('ADDR_CUST_RECID', $id)->first();
            $address->fill($post_address);
            $address->save();

            $post_email = $requests->only('EML_EMAIL');
            $email = CustomerEmail::where('EML_CUST_RECID', $id)->where('EML_TYPE', 1)->first();
            $email->fill($post_email);
            $email->save();*/
            DB::commit();
        } catch (ValidationException $e) {
            DB::rollBack();

            throw new ValidationException($e->validator, $e->getResponse());
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors('Add Customer Email Failed! Please try again!');
        }
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$id.'/log';
        $client = new Client(['http_errors' => false]);
        if ($customers->CUST_NAME !== $requests->input('CUST_NAME')) {
            $response = $client->request('POST', $uri, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
                ],
                'form_params' => [
                    'storeId'  => Session::get('data')->STORE->id,
                    'type'  => 'Edit Name',
                    'oldData' => $oldCustomerName,
                    'newData' => $requests->input('CUST_NAME'),
                    'userUsername' => user()->USER_USERNAME
                ]
            ]);
            $contents = json_decode($response->getBody()->getContents());
        }
        if (!isEmpty($requests->input('CUST_IDCARDS')) && $customers->CUST_IDCARDS !== $requests->input('CUST_IDCARDS')) {
            $response = $client->request('POST', $uri, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
                ],
                'form_params' => [
                    'storeId'  => Session::get('data')->STORE->id,
                    'type'  => 'Edit ID Card',
                    'oldData' => $oldCustomerIdCard,
                    'newData' => $requests->input('CUST_IDCARDS'),
                    'userUsername' => user()->USER_USERNAME
                ]
            ]);
            $contents = json_decode($response->getBody()->getContents());
        }
        $requests->session()->flash('success', 'Update Success');
        return redirect()->back();
    }

    public function getData() {
        $customer = Customer::with(['mobilePhone', 'idType'])
                        ->where('CUST_PNUM', Session::get('pnum'))
                        ->where('CUST_PTYPE', Session::get('ptype'))
                        ->select('Customers.*');

        return Datatables::of($customer)
            ->addColumn('id', function() {
                return null;
            } )
            ->addColumn('action', function($customer) {
                $roleId = Session::get('role_data')->ROLE_RECID;
                if($roleId == $this->cs){
                    $edit='';
                } else {
                $edit = '<a href="'.url('customer/'.$customer->CUST_RECID) .'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                }
                $more = '<a href="'. url("customer/". $customer->CUST_RECID ."/detail") .'"><button type="button" class="btn btn-success btn-xs">View</button></a>';

                return $edit . '&nbsp;' . $more;
            })
            ->make(true);
    }

    public function getAddresses($customer_id)
    {
        $data['customer'] = Customer::find($customer_id);
        $data['customer_id'] = $customer_id;
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();
        $data['provinces'] = Lookup::where('LOK_CODE', 'PROV')->get();
        $data['areas'] = Lookup::where('LOK_CODE', 'AREA')->get();
        return view('customer.address_list', $data);
    }

    public function editAddress($customer_id, $id)
    {
        $data['customer'] = Customer::find($customer_id);
        $data['address'] = CustomerAddr::findOrFail($id);
        $data['countries'] = Lookup::where('LOK_CODE', 'COTR')->get();
        $data['provinces'] = Lookup::where('LOK_CODE', 'PROV')->where('LOK_CODEMST', $data['address']->ADDR_COUNTRY)->get();
        $data['cities'] = Lookup::where('LOK_CODE', 'CITY')->where('LOK_CODEMST', $data['address']->ADDR_PROVINCE)->get();
        $data['areas'] = Lookup::where('LOK_CODE', 'AREA')->get();
        return view('customer.address_list', $data);
    }

    public function storeAddress(Request $request)
    {
        $rules = [
            'ADDR_ADDRESS' => 'required'
        ];

        $messages = [
            'ADDR_ADDRESS.required' => 'Address field is required'
        ];

        $this->validate($request, $rules, $messages);

        $post_data = $request->except(['token', 'ADDR_ACTIVE', 'CUST_RECID']);
        $address = new CustomerAddr();
        $address->ADDR_RECID = Uuid::uuid();
        $address->ADDR_CUST_RECID = $request->input('CUST_RECID');
        $address->fill($post_data);
        $addressData = $request->only(['ADDR_PROVINCE', 'ADDR_CITY', 'ADDR_AREA']);
        $address->ADDR_PROVINCE = !empty($addressData['ADDR_PROVINCE']) ? $addressData['ADDR_PROVINCE'] : null;
        $address->ADDR_CITY = !empty($addressData['ADDR_CITY']) ? $addressData['ADDR_CITY'] : null;
        $address->ADDR_AREA = !empty($addressData['ADDR_AREA']) ? $addressData['ADDR_AREA'] : null;
        $address->ADDR_ACTIVE = 1;
        $address->save();

        $request->session()->flash('success', 'Data has been Saved');
        return redirect()->back();
    }

    public function updateAddress(Request $request, $id)
    {
        $rules = [
            'ADDR_ADDRESS' => 'required'
        ];
        $messages = [
            'ADDR_ADDRESS.required' => 'Address field is required'
        ];

        $this->validate($request, $rules, $messages);
        $customer_id = $request->input('CUST_RECID');
        $post_data = $request->except(['token', 'ADDR_ACTIVE', 'CUST_RECID']);
        $address = CustomerAddr::find($id);
        $address->fill($post_data);
        $addressData = $request->only(['ADDR_PROVINCE', 'ADDR_CITY', 'ADDR_AREA']);
        $address->ADDR_PROVINCE = !empty($addressData['ADDR_PROVINCE']) ? $addressData['ADDR_PROVINCE'] : null;
        $address->ADDR_CITY = !empty($addressData['ADDR_CITY']) ? $addressData['ADDR_CITY'] : null;
        $address->ADDR_AREA = !empty($addressData['ADDR_AREA']) ? $addressData['ADDR_AREA'] : null;
        $address->ADDR_RECID = Uuid::uuid();
        $address->ADDR_ACTIVE = 1;
        $address->save();

        $request->session()->flash('success', 'Data has been Updated');

        return redirect('customer/'.$customer_id.'/address');

    }

    public function deleteAddress(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $customer_id = $request->input('customer_id');
        $customer = CustomerAddr::find($request->input('id'));
        $type = $request->input('type');
        if ($type == 1 || $type == 2 || $type == 3) {
            $addressGroup = [1,2,3];
        }
        $addressCount = CustomerAddr::where('ADDR_CUST_RECID', $customer_id)->whereIn('ADDR_TYPE', $addressGroup)->count();
        if($addressCount > 1){
        $customer->delete();
        $request->session()->flash('success', 'Data has been deleted');
        }else{
        $request->session()->flash('error', 'Cannot delete data!');
        }
        return redirect('customer/'.$request->input('customer_id').'/address');

    }

    public function getAddressData($customer_id)
    {
        $address = Customer::find($customer_id)->addresses;
        return Datatables::of($address)
            ->editColumn('ADDR_CITY', function($address) {
                $res = Lookup::where('LOK_RECID', $address->ADDR_CITY)->first();
                if (empty($res)) return '-';
                return $res->LOK_DESCRIPTION;
            })
            ->editColumn('ADDR_PROVINCE', function($address) {
                $res = Lookup::where('LOK_RECID', $address->ADDR_PROVINCE)->first();
                if (empty($res)) return '-';
                return $res->LOK_DESCRIPTION;
            })
            ->editColumn('ADDR_COUNTRY', function($address) {
                $res = Lookup::where('LOK_RECID', $address->ADDR_COUNTRY)->first();
                if (empty($res)) return '-';
                return $res->LOK_DESCRIPTION;
            })
            ->editColumn('ADDR_AREA', function($address) {
                $res = Lookup::where('LOK_RECID', $address->ADDR_AREA)->first();
                if (empty($res)) return '-';
                return $res->LOK_DESCRIPTION;
            })
            ->addColumn('action', function($address) use($customer_id){
                $url = url('customer/'.$customer_id.'/address');
                $edit = '<a href="'. $url .'/'.$address->ADDR_RECID .'/edit' .'" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>';
             /*   $delete = '<a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row waves-effect waves-classic" data-id="'.$address->ADDR_RECID.'" data-name="'.$address->ADDR_ADDRESS.'" data-customer-id="'.$customer_id.'" data-original-title="Remove" data-toggle="modal" data-type="' . $address->ADDR_TYPE . '" data-target="#delete-modal"><i class="icon md-delete" aria-hidden="true"></i></a>';
             */
                return $edit ;
            })
            ->make(true);
    }

    public function getPhones($customer_id)
    {
        $data['customer'] = Customer::find($customer_id);
        return view('customer.phone_list', $data);
    }

    public function editPhone($customer_id, $id)
    {
        $data['customer'] = Customer::find($customer_id);
        $data['phone'] = CustomerPhone::findOrFail($id);
        return view('customer.phone_list', $data);
    }

    public function storePhone(Request $request)
    {
        $rules = [
            'PHN_TYPE' => 'required',
            'PHN_NUMBER' => 'required'
        ];
        $messages = [
            'PHN_TYPE.required' => 'Phone type is required',
            'PHN_NUMBER.required' => 'Phone number is required'
        ];
        $this->validate($request, $rules, $messages);


        $post_data = $request->except(['token', 'PHN_ACTIVE', 'CUST_RECID', 'PHN_NUMBER']);
        $phone = new CustomerPhone();
        $phone->PHN_RECID = Uuid::uuid();
        $phone->PHN_CUST_RECID = $request->input('CUST_RECID');
        if($request->input('PHN_TYPE') == 1) {
            $phone->PHN_NUMBER = ltrim($request->input('PHN_NUMBER'), '0');
        } else {
            $phone->PHN_NUMBER = $request->input('PHN_NUMBER');
        }
        $phone->fill($post_data);
        $phone->PHN_ACTIVE = 1;
        $phone->save();

        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$request->input('CUST_RECID').'/log';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'storeId'  => Session::get('data')->STORE->id,
                'type'     => 'Add Phone',
                'newData'  => $phone->PHN_NUMBER,
                'userUsername' => user()->USER_USERNAME
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        $request->session()->flash('success', 'Data has been Saved');

        return redirect()->back();

    }

    public function updatePhone(Request $request, $id)
    {
        $rules = [
            'PHN_TYPE' => 'required',
            'PHN_NUMBER' => 'required'
        ];

        $messages = [
            'PHN_TYPE.required' => 'Phone type is required',
            'PHN_NUMBER.required' => 'Phone number is required'
        ];

        $this->validate($request, $rules, $messages);

        $customer_id = $request->input('CUST_RECID');
        $post_data = $request->except(['token', 'PHN_ACTIVE', 'CUST_RECID']);
        $phone = CustomerPhone::find($id);
        $oldPhone = $phone->PHN_NUMBER;
        $phone->fill($post_data);
        if($request->input('PHN_TYPE') == 1) {
            $phone->PHN_NUMBER = ltrim($request->input('PHN_NUMBER'), '0');
        } else {
            $phone->PHN_NUMBER = $request->input('PHN_NUMBER');
        }
        $phone->PHN_ACTIVE = 1;
        $phone->save();

        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$request->input('CUST_RECID').'/log';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'storeId'  => Session::get('data')->STORE->id,
                'type'     => 'Edit Phone',
                'oldData' => $oldPhone,
                'newData' => $phone->PHN_NUMBER,
                'userUsername' => user()->USER_USERNAME
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        $request->session()->flash('success', 'Data has been Updated');

        return redirect('customer/'.$customer_id.'/phone');
    }

    public function deletePhone(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $customerId = $request->input('customer_id');
        $type = $request->input('type');
        $phone = CustomerPhone::find($request->input('id'));
        $phone->delete();
        $request->session()->flash('success', 'Data has been deleted');
        if ($type == 1) {
            $request->session()->flash('error', 'Cannot delete data!');
        }

        return redirect('customer/'.$customerId.'/phone');
    }

    public function getPhoneData($customer_id)
    {
        $phones = Customer::find($customer_id)->phones;
        return Datatables::of($phones)
            ->editColumn('PHN_TYPE', function($phones){
                if($phones->PHN_TYPE == 1){
                    return 'Mobile 1';
                } elseif($phones->PHN_TYPE == 2){
                    return 'Mobile 2';
                } elseif($phones->PHN_TYPE == 3){
                    return 'Mobile 3';
                } elseif($phones->PHN_TYPE == 4){
                    return 'Business 1';
                } elseif($phones->PHN_TYPE == 5){
                    return 'Business 2';
                } elseif($phones->PHN_TYPE == 6){
                    return 'Business 3';
                } elseif($phones->PHN_TYPE == 7){
                    return 'Home 1';
                } elseif($phones->PHN_TYPE == 8){
                    return 'Home 2';
                } elseif($phones->PHN_TYPE == 9){
                    return 'Home 3';
                } elseif($phones->PHN_TYPE == 10){
                    return 'Company 1';
                } elseif($phones->PHN_TYPE == 11){
                    return 'Company 2';
                } elseif($phones->PHN_TYPE == 12){
                    return 'Company 3';
                } elseif($phones->PHN_TYPE == 13){
                    return 'Facsimile 1';
                } elseif($phones->PHN_TYPE == 14){
                    return 'Facsimile 2';
                } elseif($phones->PHN_TYPE == 15){
                    return 'Facsimile 3';
                }
                return null;
            })
            ->addColumn('action', function($phones) use($customer_id){
                $url = url('customer/'.$customer_id.'/phone');
                $edit = '<a href="'. $url .'/'.$phones->PHN_RECID .'/edit' .'" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>';
                if ($phones->PHN_TYPE == 1) {
                    $delete = '';
                } else {
                    $delete = '<a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row waves-effect waves-classic" data-id="'.$phones->PHN_RECID.'" data-name="'.$phones->PHN_NUMBER.'" data-customer-id="'.$customer_id.'" data-original-title="Remove" data-toggle="modal" data-type="' . $phones->PHN_TYPE . '" data-target="#delete-modal"><i class="icon md-delete" aria-hidden="true"></i></a>';
                }
                return $edit . $delete ;
            })
            ->make(true);
    }

    public function getEmails($customer_id)
    {
        $data['customer'] = Customer::find($customer_id);
        return view('customer.email_list', $data);

    }

    public function editEmail($customer_id, $id)
    {
        $data['customer'] = Customer::find($customer_id);
        $data['email'] = CustomerEmail::findOrFail($id);
        return view('customer.email_list', $data);
    }

    public function storeEmail(Request $request)
    {
        $rules = [
            'EML_EMAIL' => 'required',
            'EML_TYPE' => 'required'
        ];
        $messages = [
            'EML_EMAIL.required' => 'Email Address field is required',
            'EML_TYPE.required' => 'Type field is required'
        ];
        $this->validate($request, $rules, $messages);

        $email = $request->input('EML_EMAIL');

        $post_data = $request->except(['token', 'EML_ACTIVE', 'CUST_RECID']);
        $email = new CustomerEmail();
        $email->EML_RECID = Uuid::uuid();
        $email->EML_CUST_RECID = $request->input('CUST_RECID');
        $email->fill($post_data);
        $email->EML_ACTIVE = 1;
        $email->save();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$request->input('CUST_RECID').'/log';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'storeId'  => Session::get('data')->STORE->id,
                'type'  => 'Add Email',
                'newData' => $request->input('EML_EMAIL'),
                'userUsername' => user()->USER_USERNAME
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        $request->session()->flash('success', 'Data has been Saved');
        return redirect()->back();

    }

    public function updateEmail(Request $request, $id)
    {
        $rules = [
            'EML_TYPE' => 'required',
            'EML_EMAIL' => 'required'
        ];
        $messages = [
            'EML_TYPE.required' => 'Type field is required required',
            'EML_EMAIL.required' => 'Email Address field is required'
        ];
        $this->validate($request, $rules, $messages);

        $email = $request->input('EML_EMAIL');

        $customer_id = $request->input('CUST_RECID');
        $post_data = $request->except(['token', 'EML_ACTIVE', 'CUST_RECID']);
        $email = CustomerEmail::find($id);
        $oldEmail = $email->EML_EMAIL;
        $email->fill($post_data);
        $email->EML_ACTIVE = 1;
        $email->save();

        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$request->input('CUST_RECID').'/log';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'storeId'  => Session::get('data')->STORE->id,
                'type'  => 'Edit Email',
                'oldData' => $oldEmail,
                'newData' => $request->EML_EMAIL,
                'userUsername' => user()->USER_USERNAME
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        $request->session()->flash('success', 'Data has been Updated');
        return redirect('customer/'.$customer_id.'/email');

    }

    public function deleteEmail(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $customer_id = $request->input('customer_id');
        $email = CustomerEmail::find($request->input('id'));
        $type = $request->input('type');
        if ($type == 1 || $type == 2 || $type == 3) {
            $emailGroup = [1,2,3];
        }
        $emailCount = CustomerEmail::where('EML_CUST_RECID', $customer_id)->whereIn('EML_TYPE', $emailGroup)->count();
        if($emailCount > 1){
        $email->delete();
        $request->session()->flash('success', 'Data has been deleted');
        } else {
         $request->session()->flash('error', 'Cannot delete data!');
        }
        return redirect('customer/'.$customer_id.'/email');

    }

    public function getEmailData($customer_id)
    {
        $email = Customer::find($customer_id)->emails;
        return Datatables::of($email)
            ->editColumn('EML_TYPE', function($email){
                if($email->EML_TYPE == 1){
                    return 'Primary Email';
                } elseif($email->EML_TYPE == 2){
                    return 'Secondary Email';
                } elseif($email->EML_TYPE == 3){
                    return 'Tertiary Email';
                }
                return null;
            })
            ->addColumn('action', function($email) use ($customer_id){
                $url = url('customer/'.$customer_id.'/email');
                $edit = '<a href="'. $url .'/'.$email->EML_RECID .'/edit' .'" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>';
                if ($email->EML_TYPE == 1)
                {
                    $delete = '';
                }else{
                    $delete = '<a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row waves-effect waves-classic" data-id="'.$email->EML_RECID.'" data-name="'.$email->PHN_NUMBER.'" data-customer-id="'.$customer_id.'" data-original-title="Remove" data-toggle="modal" data-type="' . $email->EML_TYPE . '" data-target="#delete-modal"><i class="icon md-delete" aria-hidden="true"></i></a>';
                }
                return $edit . $delete ;
            })
            ->make(true);
    }

    public function getInterests($customer_id)
    {
        $data['customer'] = Customer::find($customer_id);
        $data['master_interests'] = Lookup::where('LOK_CODE', 'INTR')->get();
        return view('customer.interest_list', $data);
    }

    public function editInterest($customer_id, $id)
    {
        $data['customer'] = Customer::find($customer_id);
        $data['interest'] = CustomerInterest::findOrFail($id);
        return view('customer.interest_list', $data);
    }

    public function storeInterest(Request $request)
    {
        $rules = [
            'CUST_RECID' => 'required',
            'INT_CATCODE' => 'required'
        ];
        $this->validate($request, $rules);

        $post_data = $request->except(['token', 'INT_ACTIVE', 'CUST_RECID']);
        $interest = new CustomerInterest();
        $interest->INT_RECID = Uuid::uuid();
        $interest->INT_CUST_RECID = $request->input('CUST_RECID');
        $interest->fill($post_data);
        if( $request->input('INT_ACTIVE') == 'on') {
            $interest->INT_ACTIVE = 1;
        } else {
            $interest->INT_ACTIVE = 0;
        }
        $interest->save();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$request->input('CUST_RECID').'/log';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'storeId' => Session::get('data')->STORE->id,
                'type' => 'Add Interest',
                'userUsername' => user()->USER_USERNAME
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        $request->session()->flash('success', 'Data has been Saved');
        return redirect()->back();

    }

    public function updateInterest(Request $request, $id)
    {
        $rules = [
            'CUST_RECID' => 'required',
            'INT_CATCODE' => 'required'
        ];
        $messages = [

        ];
        $this->validate($request, $rules, $messages);

        $customer_id = $request->input('CUST_RECID');
        $post_data = $request->except(['token', 'INT_ACTIVE', 'CUST_RECID']);
        $interest = CustomerInterest::find($id);
        $interest->fill($post_data);
        if( $request->input('INT_ACTIVE') == 'on') {
            $interest->INT_ACTIVE = 1;
        } else {
            $interest->INT_ACTIVE = 0;
        }
        $interest->save();
        $request->session()->flash('success', 'Data has been Updated');
        return redirect('customer/'.$customer_id.'/interest');

    }

    public function deleteInterest(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        $customer_id = $request->input('customer_id');
        $email = CustomerInterest::find($request->input('id'));
        $email->delete();
        $request->session()->flash('success', 'Data has been deleted');
        return redirect('customer/'.$customer_id.'/interest');

    }

    public function getInterestData($customer_id)
    {
        $interest = Customer::find($customer_id)->interests;
        return Datatables::of($interest)
            ->editColumn('INT_CATCODE', function($interest) {
                $res = Lookup::where('LOK_RECID', $interest->INT_CATCODE)->first();

                return $res->LOK_DESCRIPTION;
            })
            ->addColumn('action', function($interest) use ($customer_id){
                $delete = '<a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row waves-effect waves-classic" data-id="'.$interest->INT_RECID.'" data-customer-id="'.$customer_id.'" data-original-title="Remove" data-toggle="modal" data-target="#delete-modal"><i class="icon md-delete" aria-hidden="true"></i></a>';
                return $delete;
            })
            ->make(true);
    }

    public function getCardExchanges(Request $request)
    {
        $data['breadcrumbs'] = 'Card Exchange';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['search'] = $request->get('s');

        $data['customers'] = Customer::selectRaw('CUST_RECID, CUST_NAME, CUST_BARCODE, PHN_NUMBER, CUST_IDCARDS')
                                ->leftJoin('customers_phone', 'customers_phone.PHN_CUST_RECID', '=', 'customers.CUST_RECID')
                                ->where('PHN_TYPE', 1)
                                ->where('CUST_PNUM', $this->pnum)
                                ->where('CUST_PTYPE', $this->ptype)
                                ->where('CUST_ACTIVE', 1)
                                ->when($request->has('s'), function($qry) use($data) {
                                    $search = $data['search'];
                                    $replacedSearch = str_replace(' ', '%', $search);

                                    return $qry->where(function($qry) use($search, $replacedSearch) {
                                        $qry->where('CUST_NAME', 'LIKE', "%{$replacedSearch}%")
                                            ->orWhere('CUST_BARCODE', 'LIKE', "%{$search}%")
                                            ->orWhere('PHN_NUMBER', 'LIKE', "%{$search}%")
                                            ->orWhere('CUST_IDCARDS', 'LIKE', "%{$search}%");
                                    });
                                })
                                ->orderBy('CUST_BARCODE', 'DESC')
                                ->simplePaginate(10);

        return view('customer.card_exchange_list', $data);
    }

    public function editCardExchange($id)
    {
        $data['breadcrumbs'] = 'Card Exchange';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['customer'] = Customer::where('CUST_ACTIVE', 1)->findOrFail($id);
        $data['cardTypes'] = CardType::where('CARD_ACTIVE', 1)->where('CARD_PNUM', Session::get('pnum'))->where('CARD_PTYPE', Session::get('ptype'))->get();

        return view('customer.card_exchange', $data);
    }

    public function updateCardExchange(Request $request, $customer_id)
    {
        $messages = [
            'EXC_NEWCARD.required' => 'New card field is required',
            'EXC_NEWCARDTYPE.required' => 'New card type field is required',
            'EXC_TYPE.required' => 'Change Type is required'
        ];

        $rules = [
            'EXC_NEWCARD' => 'required',
            'EXC_NEWCARDTYPE' => 'required',
            'EXC_TYPE' => 'required'
        ];
        $this->validate($request, $rules ,$messages );

        $newBarcode = $request->input('EXC_NEWCARD');
        $customer = Customer::where('CUST_ACTIVE', 1)->findOrFail($customer_id);
        if ($customer->CUST_BARCODE == $newBarcode) {
            return redirect()->back()->withErrors('New barcode can\'t be same as current barcode');
        }
        if (Customer::where('CUST_BARCODE', $newBarcode)->where('CUST_RECID', '!=', $customer_id)->count() > 0) {
            return redirect()->back()->withErrors('Barcode already bound to another customer!');
        }
        if (CardExchange::where('EXC_OLDCARD',$request->EXC_NEWCARD)->first()) {
            return redirect()->back()->withErrors('Barcode already bound to another card exchange');
        }

        $card_exchange = new CardExchange();
        $card_exchange->EXC_RECID = Uuid::uuid();
        $card_exchange->EXC_CUST_RECID = $customer_id;
        $card_exchange->EXC_OLDCARD = $request->EXC_OLDCARD;
        $card_exchange->EXC_NEWCARD = $request->EXC_NEWCARD;
        $card_exchange->EXC_OLDCARDTYPE = $request->EXC_OLDCARDTYPE ? $request->EXC_OLDCARDTYPE : NULL;
        $card_exchange->EXC_NEWCARDTYPE = $request->EXC_NEWCARDTYPE;
        $card_exchange->EXC_TYPE = $request->EXC_TYPE;
        $card_exchange->EXC_NOTES = $request->EXC_NOTES;
        $card_exchange->EXC_DATE = Carbon::now()->format('Y-m-d');
        $card_exchange->EXC_TIME = Carbon::now()->format('H:i:s');
        $card_exchange->EXC_USER = user()->USER_USERNAME;
        $card_exchange->EXC_TNT_RECID = Session::get('data')->STORE->id;
        $card_exchange->save();

        $customer->CUST_BARCODE = $request->input('EXC_NEWCARD');
        $customer->CUST_CARDTYPE = $request->input('EXC_NEWCARDTYPE');
        // $customer->CUST_ALIASNAME = $request->input('CUST_ALIASNAME');
        $customer->save();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$customer_id.'/log';

        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'storeId' => Session::get('data')->STORE->id,
                'type' => 'Exchange Card',
                'oldData' => $request->input('EXC_OLDCARD'),
                'newData' => $request->input('EXC_NEWCARD'),
                'userUsername' => user()->USER_USERNAME
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        $request->session()->flash('success', 'Data has been Updated');
        return redirect('customer/card-exchanges');

    }

    public function getCardExchangeData()
    {
        $customer = Customer::select(['CUST_RECID', 'CUST_NAME', 'CUST_BARCODE', 'CUST_IDCARDS', 'CUST_IDTYPE'])->with(['mobilePhone','idType']);

        return Datatables::of($customer)
            ->addColumn('action', function($customer) {
                $more = '<a href="'. url("customer/card-exchange/". $customer->CUST_RECID) .'/edit"><button type="button" class="btn btn-success btn-xs">Change Card</button></a>';
                return $more;
            })
            ->editColumn('CUST_IDTYPE', function($customer) {
                $res = Lookup::where('LOK_RECID', $customer->CUST_IDTYPE)->first();
                if ($res) {
                    return $res->LOK_DESCRIPTION;
                } else {
                    return '-';
                }
            })
            ->addColumn('mobilePhone', function($customer) {
                if($customer->mobilePhone){
                    return $customer->mobilePhone->PHN_NUMBER;

                } else {
                    return '-';
                }
            })
            ->addColumn('cust_name', function($customer) {
                if($customer->CUST_NAME){
                    return $customer->CUST_NAME;
                } else {
                    return '-';
                }
            })
            ->addColumn('cust_cards', function($customer) {
                if($customer->CUST_IDCARDS){
                    return $customer->CUST_IDCARDS;
                } else {
                    return '-';
                }
            })
            ->addColumn('idType', function($customer) {
                if($customer->idType){
                    return $customer->idType->LOK_DESCRIPTION;
                } else {
                    return '-';
                }
            })
            ->make(true);
    }

    public function getCustomerLogs($customer_id) {
        $data['breadcrumbs'] = 'Customer Exchange History';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['customer'] = Customer::findOrFail($customer_id);
        $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$customer_id.'/log';
        $data['customerId'] = $customer_id;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $data['uri'], [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['exchange'] = $contents->DATA;
            $data['paginator'] = $contents->paginator;
            return view('customer.log_history', $data);
        }
        elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        }
    }

    public function customerExchange($customer_id)
    {
        $data['breadcrumbs'] = 'Customer Exchange History';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['customer'] = Customer::findOrFail($customer_id);

        return view('customer.exchange_history', $data);
    }

    public function getCustomerexchange($customer_id)
    {
        $exchange = CustomerExchange::where('EXC_CUST_RECID', $customer_id)
                                ->with(['store']);

        return Datatables::of($exchange)
            ->addColumn('store', function($exchange) {
                return $exchange->store ? $exchange->store->TNT_DESC : '-';
            })
            ->editColumn('EXC_TIME', function($exchange) {
                return  Carbon::parse($exchange->EXC_TIME)->format('H:i:s');
            })
            ->addColumn('type', function($exchange) {
                if($exchange->EXC_TYPE =='1'){
                    return 'Upgrade';

                } elseif($exchange->EXC_TYPE =='2') {
                    return 'Downgrade';
                } elseif($exchange->EXC_TYPE =='3') {
                    return 'Replacement';
                } elseif($exchange->EXC_TYPE =='4') {
                    return 'Lost';
                } elseif($exchange->EXC_TYPE =='5') {
                    return 'Extend';
                } elseif($exchange->EXC_TYPE =='6') {
                    return 'Migrasi';
                } elseif($exchange->EXC_TYPE =='7') {
                    return 'New Registration';
                }else {
                    return 'Edit profile or Other';
                }
            })
            ->make(true);
    }

    public function getCustomerTransactionHistory($customer_id) {
        $data['breadcrumbs'] = 'Customer Points History';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['customer'] = Customer::findOrFail($customer_id);
        $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/'.$customer_id.'/transaction/history';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $data['uri'], [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['pointHistory'] = $contents->DATA;
            $data['paginator'] = $contents->paginator;
            $data['customerId'] = $customer_id;

            return view('customer.points_history', $data);
        }
        elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        }
    }

    public function getCustomerTransaction(Request $request) {

        $idcustomer = $request->get('idcustomer', '');
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);
        $search = $request->get('s', '');

        if($request->value == 'points'){
            $customerUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/' .$idcustomer. '/transaction/history?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        }else {
            $customerUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/' .$idcustomer. '/log?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        }

        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $customerUri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() == 200) {
            return response()->json($contents);
        } else { // Error 500
            return response()->json('Error occured!');
        }
    }

    public function getExchangeData($customer_id){
        $card_exchange = CardExchange::where('EXC_CUST_RECID', $customer_id)->get();

        $datatables = app('datatables')->of($card_exchange)
            ->editColumn('EXC_DATE', function($card) {
                return $card->EXC_DATE->format('d/m/Y');
            })
            ->editColumn('EXC_TIME', function ($card) {
                return substr($card->EXC_TIME, 0, 8);
            })
            ->editColumn('EXC_TYPE', function($card) {
                if($card->EXC_TYPE == 6) {
                    return 'Replacement';
                } else {
                    return 'Not Found';
                }
            });

        return $datatables->make(true);
    }

    public function upload(Request $request, $parameter_name ,$save_name, $folder, $width = 300, $height = 300){
        $file = $request->file($parameter_name);
        $imageFileName = $save_name.'-'. rand(0,999).'-'.date('d-m-Y').'.'.$file->getClientOriginalExtension();
        $destinationPath = $folder;
        $file->move($destinationPath, $imageFileName);
        Image::make( sprintf( $destinationPath .'/%s', $imageFileName) )->resize($width, $height)->save();
        return $imageFileName;
    }

    public function total() {

        $customersByStore = Customer::select([
            DB::raw('COUNT(Customers.CUST_RECID) AS total_customer_bystore'),
            'Z_Tenant.TNT_RECID',
            'Z_Tenant.TNT_DESC'
            ])
            ->rightJoin('Z_Tenant', 'Customers.CUST_STORE', '=', 'Z_Tenant.TNT_RECID')
            ->groupBy('Z_Tenant.TNT_DESC', 'Z_Tenant.TNT_RECID', 'Customers.CUST_STORE')
            ->get();

        $customer = Customer::select([
            DB::raw('COUNT(Customers.CUST_STORE) AS total')])->first();

        $tenants = Tenant::select(['TNT_DESC'])
                    ->where('TNT_PNUM', $this->pnum)
                    ->where('TNT_PTYPE', $this->ptype)
                    ->get();
        $totalCustomers = app('db')->select(app('db')->raw("
        select t.*,
        sum(Total) over (order by Total desc) as [numberCustomer]
        from (
        SELECT
        MONTH(Cust_joindate) AS [month],
        COUNT(MONTH(Cust_joindate)) AS [Total]
        FROM
        Customers
                WHERE YEAR(Cust_joindate) = '".Carbon::now()->year."'
        GROUP BY
        MONTH(Cust_joindate)
        ) t
        "));
        $totalCustomers = $this->mapTotalCustomers($totalCustomers);

        return view('customer.total_customer', compact('customersByStore', 'customer', 'totalCustomers', 'tenants'));
    }

    private function mapTotalCustomers($totalCustomers)
    {
        $month = [];
        $total = [];
        $numberCustomer = [];
        foreach ($totalCustomers as $totalCustomer) {
            array_push($total, $totalCustomer->Total);
            array_push($numberCustomer, $totalCustomer->numberCustomer);
            if ($totalCustomer->month == 1) {
                $mont = "'Jan'";
            } elseif ($totalCustomer->month == 2) {
                $mont = "'Feb'";
            }elseif ($totalCustomer->month == 3) {
                $mont = "'Marc'";
            }elseif ($totalCustomer->month == 4) {
                $mont = "'Apr'";
            }elseif ($totalCustomer->month == 5) {
                $mont = "'Mei'";
            }elseif ($totalCustomer->month == 6) {
                $mont = "'Jun'";
            }elseif ($totalCustomer->month == 7) {
                $mont = "'Jul'";
            }elseif ($totalCustomer->month == 8) {
                $mont = "'Agus'";
            }elseif ($totalCustomer->month == 9) {
                $mont = "'Sept'";
            } elseif ($totalCustomer->month == 10) {
                 $mont = "'Okt'";
            } elseif ($totalCustomer->month == 11) {
                $mont = "'Nov'";
            } elseif ($totalCustomer->month == 12) {
                $mont = "'Des'";
            }
            array_push($month, $mont);
        }
        return [
            'month' => $month,
            'total' => $total,
            'numberCustomer' => $numberCustomer
        ];
    }

    public function getDataCustomer(Request $request) {
        $fromData = $request->input('fromData');
        $toDate = $request->input('toDate');
        $newCust = $request->input('newCust');
        $customerByAmonth = Customer::select([
            'Z_Tenant.TNT_DESC as STORENAME',
            DB::raw('COUNT(Customers.CUST_RECID) AS CUSTOMER')
            ])
            ->rightJoin('Z_Tenant', 'Customers.CUST_STORE', '=', 'Z_Tenant.TNT_RECID')
            ->whereBetween('Customers.CUST_JOINDATE' , array($fromData, $toDate))
            ->groupBy('Z_Tenant.TNT_DESC', 'Z_Tenant.TNT_RECID', 'Customers.CUST_STORE')
            ->get();
            if ($newCust == 'newCust') {
                return response()->json($customerByAmonth);
            } else {
                $filename = 'Customer_' . date('Y-m-d');
                Excel::create($filename, function($file) use($customerByAmonth) {
                    $file->sheet('Customer', function($sheet) use($customerByAmonth) {
                        $sheet->fromArray($customerByAmonth);
                    });
                })->download('xls');
                return redirect()->back();
            }

    }

    public function getDataCustomercard(Request $request) {
        $fromData = $request->input('fromDatecard');
        $toDate = $request->input('toDatecard');
        $card  = $request->input('card');
        $customersByCardEx = app('db')->select(app('db')->raw("
            SELECT
                        Count(Customer_Exchange.EXC_RECID) AS [totalcutomer],
                        dbo.Z_Tenant.TNT_DESC AS [Store]
            FROM
            dbo.Customer_Exchange
            INNER JOIN dbo.Z_Tenant ON dbo.Customer_Exchange.EXC_TNT_RECID = dbo.Z_Tenant.TNT_RECID
            WHERE Customer_Exchange.EXC_DATE  BETWEEN  '".$fromData."' AND '".$toDate."' AND Customer_Exchange.EXC_TYPE NOT IN('7')
            GROUP BY dbo.Z_Tenant.TNT_DESC
            "));
            if ($card == 'card') {
                return response()->json($customersByCardEx);
            } else {
                $stores=[];
                foreach ($customersByCardEx as $data) {
                    array_push($stores, [$data->Store, $data->totalcutomer]);
                }
                $filename = 'Customer_' . date('Y-m-d');
                Excel::create($filename, function($file) use($stores) {
                    $file->sheet('Customer', function($sheet) use($stores) {
                        $sheet->fromArray($stores);
                    });
                })->download('xls');
                return redirect()->back();
            }
    }

    public function exportByStore() {

        $customersByStore = Customer::select([
            'Z_Tenant.TNT_DESC AS STORENAME',
            DB::raw('COUNT(Customers.CUST_RECID) AS CUSTOMER')
            ])
            ->rightJoin('Z_Tenant', 'Customers.CUST_STORE', '=', 'Z_Tenant.TNT_RECID')
            ->groupBy('Z_Tenant.TNT_DESC', 'Z_Tenant.TNT_RECID', 'Customers.CUST_STORE')
            ->get();
        $filename = 'Customer_' . date('Y-m-d');
        Excel::create($filename, function($file) use($customersByStore) {
            $file->sheet('Customer', function($sheet) use($customersByStore) {
                $sheet->fromArray($customersByStore);
            });
        })->download('xls');
        return true;
    }

    public function massDelete() {
        $data['breadcrumbs'] = 'Delete';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;

        return view('customer.delete', $data);
    }

    public function processMassDelete(Request $request) {
        $this->validate($request, [
            'csv_file' => 'required|file|mimes:csv,txt'
        ]);
        DB::beginTransaction();
        try {
            $csv = $request->file('csv_file');
            $pathToCsv = $csv->getPathName();

            CSV::load($pathToCsv)->chunk(5000, function($results) {
                foreach ($results as $row) {
                    $barcode = $row[0];
                    $customer = Customer::where('CUST_BARCODE', $barcode)->first();
                    if (!$customer) continue;

                    $customerPhones = CustomerPhone::where('PHN_CUST_RECID', $customer->CUST_RECID)->delete();
                    $customerAddrs = CustomerAddr::where('ADDR_CUST_RECID', $customer->CUST_RECID)->delete();
                    $customerEmails = CustomerEmail::where('EML_CUST_RECID', $customer->CUST_RECID)->delete();
                    $customerInterests = CustomerInterest::where('INT_CUST_RECID', $customer->CUST_RECID)->delete();
                    $customerExchanges = CustomerExchange::where('EXC_CUST_RECID', $customer->CUST_RECID)->delete();

                    $customer->delete();
                }
            });
            DB::commit();
            $request->session()->flash('success', 'Data has been deleted');

            return redirect()->back();
        } catch (ValidationException $e) {
            DB::rollBack();

            throw new ValidationException($e->validator, $e->getResponse());
        } catch (Exception $e) {
            DB::rollBack();

            return redirect()->back()->withErrors('Delete Customer Failed! Please try again!');
        }
    }
}

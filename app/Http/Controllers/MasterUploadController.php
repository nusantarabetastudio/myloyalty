<?php

namespace App\Http\Controllers;

use App\Models\Master\Vendor;
use Excel, File;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;

class MasterUploadController extends Controller
{
    public function uploadVendor() {
    	$name = 'vendor';

    	return view('update-master', compact('name'));
    }

    public function storeVendor(Request $request) {
        $rules = [
            'csv_file' => 'required|mimes:csv,txt'
        ];
        $this->validate($request, $rules);

        $destinationPath = resource_path('uploads/store/');
        $extension = $request->file('csv_file')->getClientOriginalExtension();

        if ($extension == 'csv') {
            $filename = 'vendor_' . date('d-m-Y') . '_' . time() . '.' . $extension;
            $request->file('csv_file')->move($destinationPath, $filename);
            $count = 0;
            $excelData = Excel::filter('chunk')->load($destinationPath . $filename)->chunk(500, function($result) use($count) {
	        	// Do stuff with 500 data & continue fetch another data after this function has been executed
	        	foreach ($result as $row) {
		        	$vendor = new Vendor;
		        	$vendor->VDR_RECID = Uuid::uuid();
		        	$vendor->VDR_PNUM = \Session::get('pnum');
		        	$vendor->VDR_PTYPE = \Session::get('ptype');
		        	$vendor->VDR_CODE = $row[1];
		        	$vendor->VDR_NAME = $row[2];
		        	$vendor->save();

		        	$count++;
	        	}
	        });
	        File::delete($destinationPath . $filename);
	        return redirect()->back()->with('notif_success', 'Import Vendor Master Complete');
        } else {
            return redirect()->back()->with('notif_error', 'Please Upload CSV file extension');
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\FormulaEarnItems;
use App\Models\Admin\FormulaEarnTenant;
use App\Models\Master\Lookup;
use App\Models\Admin\FormulaEarn;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Faker\Provider\Uuid;
use Datatables;
use Session;

class RewardPointController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    /**
     * RewardPointController constructor.
     * @param $session
     */
    public function __construct(Session $session) {
        $this->parent = 'Point Formula';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        $this->menu_id = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index() {
        $data['breadcrumbs'] = 'Reward Point';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        return view('pointformula.rewardpoint.index', $data);
    }

    public function create() {
        $data['breadcrumbs'] = 'Reward Point Create';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        $data['tenants'] = Tenant::select(['TNT_RECID', 'TNT_PNUM', 'TNT_PTYPE', 'TNT_CODE', 'TNT_DESC'])->get()->toJson();
        $data['tenantsArray'] = Tenant::select(['TNT_RECID', 'TNT_PNUM', 'TNT_PTYPE', 'TNT_CODE', 'TNT_DESC'])
                                        ->where('TNT_DESC', 'NOT LIKE', '%POINTO - HAGENDAASZ%')
                                        ->orderBy('TNT_CODE', 'asc')
                                        ->get();
        // dd($data['tenantsArray']->toArray());
        $data['cards'] = Lookup::where('LOK_CODE', 'MEMT')->get();

        return view('pointformula.rewardpoint.create', $data);
    }

    public function store(Request $request) {
        $rules = [
            'ERN_DESC' => 'required',
            'ERN_POINT' => 'numeric',
        ];

        $messages = [
            'ERN_DESC.required' => 'Description field is required',
            'ERN_POINT.numeric' => 'Point field must be numeric',
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->only(['ERN_DESC', 'ERN_TYPE', 'ERN_FORMULA_DTL']);
        $earnPoint = new FormulaEarn();
        $earnPoint->ERN_RECID = strtoupper(Uuid::uuid());
        $earnPoint->fill($post_data);
        $earnPoint->ERN_PNUM = $this->pnum;
        $earnPoint->ERN_PTYPE = $this->ptype;
        $earnPoint->ERN_ACTIVE = 1;
        $earnPoint->ERN_FORMULA = 2; // Reward Point
        $earnPoint->ERN_VALUE = 0; // by Amount
        $earnPoint->ERN_FRDATE = dateFormat($request->input('fromDate'));
        $earnPoint->ERN_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $earnPoint->ERN_TODATE = dateFormat($request->input('toDate'));
        $earnPoint->ERN_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s

        if($request->input('ERN_FIXED') == 'on') {
            $earnPoint->ERN_FIXED = 1;
            $earnPoint->ERN_POINT = $request->input('ERN_POINT');
            $earnPoint->ERN_MULTIPLE_VALUE = 0;
            $earnPoint->ERN_MULTIPLE = 0;
        } else {
            $earnPoint->ERN_FIXED = 0;
            $earnPoint->ERN_MULTIPLE = 1;
            $earnPoint->ERN_MULTIPLE_VALUE = $request->input('ERN_MULTIPLE_VALUE');
            $earnPoint->ERN_POINT = 0;
        }

        // Maximum amount is checked
        if($request->input('ERN_ACTIVE_MAX') == 'on') {
            $earnPoint->ERN_ACTIVE_MAX = 1;
            $earnPoint->ERN_MAX_AMOUNT = $request->input('ERN_MAX_AMOUNT');
            $earnPoint->ERN_MIN_AMOUNT = $request->input('ERN_MIN_AMOUNT');
        } else {
            $earnPoint->ERN_ACTIVE_MAX = 0;
            $earnPoint->ERN_MAX_AMOUNT = 0;
            $earnPoint->ERN_MIN_AMOUNT = 0;
        }

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $earnPoint->ERN_SUNDAY = 1;
            } else {
                $earnPoint->ERN_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $earnPoint->ERN_MONDAY = 1;
            } else {
                $earnPoint->ERN_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $earnPoint->ERN_TUESDAY = 1;
            } else {
                $earnPoint->ERN_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $earnPoint->ERN_WEDNESDAY = 1;
            } else {
                $earnPoint->ERN_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $earnPoint->ERN_THURSDAY = 1;
            } else {
                $earnPoint->ERN_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $earnPoint->ERN_FRIDAY = 1;
            } else {
                $earnPoint->ERN_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $earnPoint->ERN_SATURDAY = 1;
            } else {
                $earnPoint->ERN_SATURDAY = 0;
            }
        }

        $earnPoint->save(); // Save Object Earn Point

            
        if($request->input('ERN_FORMULA_DTL') == 8) {
            foreach($request->input('TENT_TNT_RECID') as $row_tenant) 
            {            
                $formulaStore = new FormulaEarnTenant();
                $formulaStore->TENT_RECID = strtoupper(Uuid::uuid());
                $formulaStore->TENT_ERN_RECID = $earnPoint->ERN_RECID;
                $formulaStore->TENT_TNT_RECID = $row_tenant;
                $formulaStore->TENT_ACTIVE = 1;
                $formulaStore->save();
            }
        }

        $request->session()->flash('success', 'Data has been saved.');

        return redirect('formula/reward');
    }

    public function edit($id) {
        $data['breadcrumbs'] = 'Reward Point Edit';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        $data['cards'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['tenants'] = Tenant::select(['TNT_RECID', 'TNT_PNUM', 'TNT_PTYPE', 'TNT_CODE', 'TNT_DESC'])->get()->toJson();
        $data['tenantsArray'] = Tenant::select(['TNT_RECID', 'TNT_PNUM', 'TNT_PTYPE', 'TNT_CODE', 'TNT_DESC'])->get();
        $data['formulaEarn'] = FormulaEarn::find($id);

        return view('pointformula.rewardpoint.create', $data);

    }

    public function update(Request $request, $id) {
        $rules = [
            'ERN_DESC' => 'required',
            'ERN_POINT' => 'numeric',
        ];

        $messages = [
            'ERN_DESC.required' => 'Description field is required',
            'ERN_POINT.numeric' => 'Point field must be numeric',
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->only(['ERN_DESC', 'ERN_TYPE', 'ERN_FORMULA_DTL']);
        $earnPoint = FormulaEarn::find($id);
        $earnPoint->fill($post_data);
        $earnPoint->ERN_FRDATE = dateFormat($request->input('fromDate'));
        $earnPoint->ERN_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $earnPoint->ERN_TODATE = dateFormat($request->input('toDate'));
        $earnPoint->ERN_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s
        // If Active has checked
        $earnPoint->ERN_ACTIVE = $request->has('ERN_ACTIVE') && $request->input('ERN_ACTIVE') == 'on' ? 1 : 0;

        if($request->input('ERN_FIXED') == 'on') {
            $earnPoint->ERN_FIXED = 1;
            $earnPoint->ERN_POINT = $request->input('ERN_POINT');
            $earnPoint->ERN_MULTIPLE_VALUE = 0;
            $earnPoint->ERN_MULTIPLE = 0;
        } else {
            $earnPoint->ERN_FIXED = 0;
            $earnPoint->ERN_MULTIPLE = 1;
            $earnPoint->ERN_MULTIPLE_VALUE = $request->input('ERN_MULTIPLE_VALUE');
            $earnPoint->ERN_POINT = 0;
        }

        // Maximum amount is checked
        if($request->input('ERN_ACTIVE_MAX') == 'on') {
            $earnPoint->ERN_ACTIVE_MAX = 1;
            $earnPoint->ERN_MAX_AMOUNT = $request->input('ERN_MAX_AMOUNT');
            $earnPoint->ERN_MIN_AMOUNT = $request->input('ERN_MIN_AMOUNT');
        } else {
            $earnPoint->ERN_ACTIVE_MAX = 0;
            $earnPoint->ERN_MAX_AMOUNT = 0;
            $earnPoint->ERN_MIN_AMOUNT = 0;
        }

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $earnPoint->ERN_SUNDAY = 1;
            } else {
                $earnPoint->ERN_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $earnPoint->ERN_MONDAY = 1;
            } else {
                $earnPoint->ERN_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $earnPoint->ERN_TUESDAY = 1;
            } else {
                $earnPoint->ERN_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $earnPoint->ERN_WEDNESDAY = 1;
            } else {
                $earnPoint->ERN_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $earnPoint->ERN_THURSDAY = 1;
            } else {
                $earnPoint->ERN_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $earnPoint->ERN_FRIDAY = 1;
            } else {
                $earnPoint->ERN_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $earnPoint->ERN_SATURDAY = 1;
            } else {
                $earnPoint->ERN_SATURDAY = 0;
            }
        }

        $earnPoint->save(); // Save Object Earn Point
        $earnPoint->tenant()->delete();

        if($request->input('ERN_FORMULA_DTL') == 8) {
            if($request->has('TENT_TNT_RECID')) {
                foreach ($request->input('TENT_TNT_RECID') as $row) {
                    $formulaStore = new FormulaEarnTenant();
                    $formulaStore->TENT_RECID = strtoupper(Uuid::uuid());
                    $formulaStore->TENT_ERN_RECID = $earnPoint->ERN_RECID;
                    $formulaStore->TENT_TNT_RECID = $row;
                    $formulaStore->TENT_ACTIVE = 1;
                    $formulaStore->save();
                }
            }
        }

        $request->session()->flash('success', 'Data has been updated.');

        return redirect('formula/reward');
    }
    public function updateStatus(Request $request)
    {
        $id= $request->input('id');
        $FormulaEarn = FormulaEarn::where('ERN_RECID', $id)->first();
        if($FormulaEarn->ERN_ACTIVE == '1')
        {
            $FormulaEarn->ERN_ACTIVE = '0';
            $FormulaEarn->save();
            $request->session()->flash('success', 'Data has been active.');
        } else{
            $FormulaEarn->ERN_ACTIVE = '1';
            $FormulaEarn->save();
        }
            return redirect('formula/reward');
    }
    public function view($id)
    {
         $tenant = Tenant::select([
            'TNT_CODE',
            'TNT_DESC',
            'ERN_DESC'
            ])
            ->join('FormulaEarn_Tenant', 'TNT_RECID', '=', 'TENT_TNT_RECID')
            ->join('FormulaEarn', 'ERN_RECID', '=', 'TENT_ERN_RECID')
            ->where('TENT_ERN_RECID', $id)->get();
        return view('pointformula.regularpoint.view', compact('tenant'));
    }

    public function destroy(Request $request) {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        if($request->input('id') !== null) {
            $data = FormulaEarn::find($request->input('id'));
            $data->tenant()->delete();
            $data->delete();
            $request->session()->flash('success', 'Data has been deleted.');
            return redirect('formula/reward');

        } else {
            $request->session()->flash('error', 'Data hasn\'t been deleted.');

            return redirect('formula/reward');
        }
    }

    public function data(){
        $formulaEarn = FormulaEarn::reward()->orderBy('ERN_ACTIVE', 'asc')->get();

        $datatables = app('datatables')->of($formulaEarn)
            ->addColumn('action', function($point) {
                $edit = '<a href="' . url('formula/reward/' . $point->ERN_RECID) . '/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$point->ERN_RECID.'" data-name="'.$point->ERN_DESC.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                $view = '<a href="' . route('formula.reward.view', $point->ERN_RECID).'" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#view-transaction-details">View Tenant</a>';
                return $edit . '&nbsp;' . $delete .'&nbsp;' . $view;
            })
            ->editColumn('ERN_FRDATE', function($point) {
                return $point->ERN_FRDATE->format('d/m/y');
            })
            ->editColumn('ERN_FORMULA_TYPE', function ($point) {
                if($point->ERN_FORMULA_TYPE !== null) {
                    if($point->ERN_FORMULA_TYPE == 0) {
                        return 'Point Earn';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 1 ) {
                        return 'Point Earn Bonus By Product';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 2 ) {
                        return 'Point Earn Bonus By Supplier';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 3 ) {
                        return 'Department';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 4 ) {
                        return 'Division';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 5 ) {
                        return 'Category';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 6 ) {
                        return 'Sub Category';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 7 ) {
                        return 'Segment';
                    }
                }
                return 'Unknown';
            })
            ->editColumn('ERN_TODATE', function($point) {
                return $point->ERN_TODATE->format('d/m/y');
            })
            ->addColumn('tenant', function($regular_point) {
                if($regular_point->tenant()->count() > 0) {

                    $tenants = Tenant::all()->count();

                    if($tenants == $regular_point->tenant()->count()) {

                        return 'All Store';

                    } else {
                        $desc = [];
                        foreach($regular_point->tenant()->get()->toArray() as $row_tenant){

                            $desc[] = '<span class="md-circle-o"> </span> ' . $row_tenant['tenant']['TNT_DESC'].  '<br>';

                        }
                        return $desc;
                    }

                }
                return '-';
            })
            ->editColumn('ERN_ACTIVE', function ($regular_point) {
                if($regular_point->ERN_ACTIVE == 1) {
                    return '<span class="text-success md-check" data-toggle="modal" data-target="#active-modal" data-id="'.$regular_point->ERN_RECID.'" data-name="'.$regular_point->ERN_DESC.'"></span>';
                } else {
                    return '<span class="text-danger md-close" data-toggle="modal" data-target="#active-modal" data-id="'.$regular_point->ERN_RECID.'" data-name="'.$regular_point->ERN_DESC.'"></span>';
                }
            });

        return $datatables->make(true);
    }
}

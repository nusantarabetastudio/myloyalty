<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Session;

class MenuManagementController extends Controller
{
    public function __construct()
    {
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $proptype = Session::get('user');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID.'/menu';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());

            $data['menus'] = $contents->DATA;
            return view('management.menu.index', $data);
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        }
        return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Redirect
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $proptype = Session::get('user');
        $data = $request->all();
        if( empty( $data['parent'] )) unset( $data['parent'] );
        $data['icon'] = $request->has('icon') && ($request->input('icon') !== "") ? $request->input('icon') : null;
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/menu';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if ($response->getStatusCode() == 201) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/menu');
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Redirect
     */
    public function edit($id)
    {
        $proptype = Session::get('user');
        $uriMenu = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID. '/menu/'.$id;
        $uriMenuList = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID.'/menu';
        $client = new Client(['http_errors' => false]);
        $menuListClient = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uriMenu, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $menuListResponse = $menuListClient->request('GET', $uriMenuList, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200 && $menuListResponse->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentList = json_decode($menuListResponse->getBody()->getContents());
            $arr['menu'] = $contents->DATA;
            $arr['menus'] = $contentList->DATA;

            return view('management.menu.index', $arr);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Redirect
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $data = $request->all();
        $proptype = Session::get('user');
        $data['active'] = $request->has('active') ? 1 : 0;
        $data['icon'] = $request->has('icon') && ($request->input('icon') !== "") ? $request->input('icon') : null;
        if( empty( $data['parent'] )) unset( $data['parent'] );

        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/menu/' . $id;
        $client = new Client([
            'http_errors' => false
        ]);
        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/menu');
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        }
        return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return Redirect
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            '_method' => 'required'
        ]);
        $proptype = Session::get('user');
        $data = $request->except(['_token', 'username']);
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/menu/'. $request->input('menuId');
        $client = new Client([
            'http_errors' => false
        ]);
        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/menu');

        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\FormulaEarn;
use App\Models\Admin\FormulaEarnBlood;
use App\Models\Admin\FormulaEarnCardType;
use App\Models\Admin\FormulaEarnCategory;
use App\Models\Admin\FormulaEarnDepartment;
use App\Models\Admin\FormulaEarnDivision;
use App\Models\Admin\FormulaEarnGender;
use App\Models\Admin\FormulaEarnItems;
use App\Models\Admin\FormulaEarnMember;
use App\Models\Admin\FormulaEarnPayment;
use App\Models\Admin\FormulaEarnReligion;
use App\Models\Admin\FormulaEarnSegment;
use App\Models\Admin\FormulaEarnSubCategory;
use App\Models\Admin\FormulaEarnTenant;
use App\Models\Admin\FormulaEarnBrand;
use App\Models\Admin\FormulaEarnVendor;
use App\Models\Admin\Post;
use App\Models\Admin\PostDetail;
use App\Models\Admin\PostTemp;
use App\Models\Admin\PostTempPayment;
use App\Models\Admin\PostTemp_unclean;
use App\Models\Admin\PostTempPayment_unclean;
use App\Models\Admin\ZTotalPoint;
use App\Models\Customer;
use App\Models\Master\Merchandise;
use App\Models\Master\Tenant;
use App\Models\Counter;
use Carbon\Carbon;
use Excel;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Master\Product;
use App\Models\Master\Vendor;
use App\Models\Master\Brand;
use App\Models\Master\PaymentMethod;
use App\Models\Master\Lookup;
use App\Models\Preference;
use Log;
use Exception;
use Mail;
use App\Models\PointRandom;

class EarningController extends Controller {

    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    /**
     * RewardPointController constructor.
     * @param $session
     */
    public function __construct(Session $session) {
        $this->parent = 'Earning';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : env('PNUM');
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : env('PTYPE');
        $this->menu_id = '';
    }

    public function index() {
        $data['breadcrumbs'] = 'Upload by CSV';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        return view('earningvoid.index', $data);
    }

    public function result() {
        $data['breadcrumbs'] = 'Earning Result';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;

        $data['post'] = DB::table('post')->orderBy('POS_POST_DATE', 'DESC')
                        ->orderBy('POS_POST_TIME', 'DESC')->where('POS_BARCODE', '!=', '')->paginate(50);

        return view('earningvoid.result', $data);
    }

    public function proccess(Request $request) {
        // setting up rules
        $rules = [
            'csv_file' => 'required|mimes:csv,txt',
            'payment_file' => 'mimes:csv,txt',
        ];
        // Validate
        $this->validate($request, $rules);

        $destinationPath = 'uploads'; // upload path
        $extension = $request->file('csv_file')->getClientOriginalExtension();
        $extension_payment = $request->file('payment_file')->getClientOriginalExtension();

        if ($extension == 'csv' and $extension_payment = 'csv') {

            $fileName = date('d-m-Y') . '_' . rand(11111, 99999) . '_transaction.' . $extension; // renameing
            $fileName_payment = date('d-m-Y') . '_' . rand(11111, 99999) . '_payment.' . $extension_payment; // renameing

            $request->file('csv_file')->move($destinationPath, $fileName); // uploading file to given path
            // sending back with message

            $request->file('payment_file')->move($destinationPath, $fileName_payment); // uploading file to given path
            // sending back with message

            $ress = Excel::load($destinationPath . '/' . $fileName, function ($reader) {
                        $reader->all();
                    })->get();

            $ress_payment = Excel::load($destinationPath . '/' . $fileName_payment, function ($reader) {
                        $reader->all();
                    })->get();

            $this->earningPointPost($ress, $ress_payment, $fileName, $fileName_payment);

            Session::flash('success', 'Earning Point Proccess Success');
            $data = array();
            $data['earning'] = $ress;

            return view('earningvoid.data', $data);
        } else {
            Session::flash('error', 'Please Upload CSV file extension for Transaction and Payment');
            return view('earningvoid.index');
        }
    }

    public function earningPointPost($data = array(), $data_payment = array(), $fileName = null, $fileName_payment = null, $command = false) {


        $DOC_NO = 'docs.' . date('Y-m-d H:i:s');

        if (!empty($data_payment)) {
            if (!$command) {
                DB::table('POSTTEMPPAYMENT')->delete();
                $this->writeLog('1. proses temppayment',FALSE);
                $this->prosesTempPayment($data_payment, $fileName_payment);
            }
        }
        if (!empty($data)) {
            if (!$command) {
                DB::table('POSTTEMP')->delete();
                $this->writeLog('2. proses tempsales',FALSE);
                $this->prosesTempSales($data, $fileName);
            }
        }

        //for test performance Query
        $this->writeLog('Query 1',FALSE);
        $newErn = PostTemp::select('CARDID', DB::raw('SUM(TOTALAFTERDISC) as total'), DB::raw('SUM(POINT_BONUS) as total_point_bonus'), DB::raw('SUM(SPLIT_BONUS) as total_split_bonus'))
                ->where('FILE',$fileName)
  			    ->groupBy('CARDID')
                ->groupBy('FILE')->get();


        $no = 1;
        foreach ($newErn as $row) {

            $this->writeLog('Query 2 ke '.$no,FALSE);
            $bonus_combine_fix = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                    ->where('CARDID', $row->CARDID)
					->where('FILE',$fileName)
                    ->where('MULTIPLE', 0)
                    ->orderBy('point', 'DESC')
                    ->groupBy('INTCODE')
                    ->groupBy('FILE')
                    ->first();

                    $this->writeLog('Query 3 ke '.$no,FALSE);
            $bonus_combine_multiple = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                    ->where('CARDID', $row->CARDID)
					->where('FILE',$fileName)
                    ->where('MULTIPLE', 1)
                    ->orderBy('point', 'DESC')
                    ->groupBy('INTCODE')
                    ->groupBy('FILE')
                    ->first();

                    $this->writeLog('Query 4 ke '.$no,FALSE);
            $bonus_payment_fix = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                    ->where('CARDID', $row->CARDID)
					->where('FILE',$fileName_payment)
                    ->where('MULTIPLE', 0)
                    ->groupBy('TENDNBR')
                    ->groupBy('FILE')
                    ->orderBy('point', 'DESC')
                    ->first();

                    $this->writeLog('Query 5 ke '.$no,FALSE);
            $bonus_payment_multiple = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                    ->where('CARDID', $row->CARDID)
					->where('FILE',$fileName_payment)
                    ->where('MULTIPLE', 1)
                    ->groupBy('TENDNBR')
                    ->groupBy('FILE')
                    ->orderBy('point', 'DESC')
                    ->first();


            $bonus_payment = (isset($bonus_payment_fix->point) ? $bonus_payment_fix->point : 0) + (isset($bonus_payment_multiple->point) ? $bonus_payment_multiple->point : 0);


            $bonus_combine_fix_point = (isset($bonus_combine_fix->point) ? $bonus_combine_fix->point : 0);
            $bonus_combine_fix_split = (isset($bonus_combine_fix->split) ? $bonus_combine_fix->split : 0);
            $bonus_payment_point = (isset($bonus_payment) ? $bonus_payment : 0);

            $bonus_combine_multiple_point = (isset($bonus_combine_multiple->point) ? $bonus_combine_multiple->point : 0);
            $bonus_combine_multiple_split = (isset($bonus_combine_multiple->split) ? $bonus_combine_multiple->split : 0);

            $point = $bonus_combine_fix_point + $bonus_combine_multiple_point + $bonus_payment_point;
            $split = $bonus_combine_fix_split + $bonus_combine_multiple_split;

            $totalAmount = (isset($bonus_combine_multiple->total) ? $bonus_combine_multiple->total : 0) + (isset($bonus_combine_fix->total) ? $bonus_combine_fix->total : 0);

            $this->writeLog('Query 6 ke '.$no,FALSE);
            $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
            $minimum_transaction = $preference['value'];




            $sumErn = $row->total;

            $this->writeLog('Query 7 ke '.$no,FALSE);
            $customer = Customer::where('CUST_BARCODE', $row->CARDID)->first();

            $this->writeLog('Query 8 ke '.$no,FALSE);
            $postTemp = PostTemp::where('CARDID', $row->CARDID)->where('FILE', $fileName)->first();  // tambah where FILE = .....

            $this->writeLog('Query 9 ke '.$no,FALSE);
            $postTempAll = PostTemp::where('CARDID', $row->CARDID)->where('FILE', $fileName)->get(); // tambah where FILE = .....



            try {
                $this->writeLog('Proses Bonus ke '.$no,FALSE);
                $bonus_grouping = $this->_checkBonusPointGroup($postTemp, $customer, $sumErn, $fileName);
                //$bonus_grouping = array('point' => 0, 'split' => 0, 'multiple' => 0, 'pf' => 0, 'group' => 0);
            } catch (\Exception $e) {

                $bonus_grouping = array('point' => 0, 'split' => 0, 'multiple' => 0, 'pf' => 0, 'group' => 0);

                $this->writeLog(" Check Bonus Point Failed, with message : {$e->getMessage()}", env('DEBUG_EARNING_LOG', false));
            }




            $this->writeLog('Proses LD ke '.$no,FALSE);
            $ld_grouping = $this->_checkLDPointGroup($postTemp, $customer, $sumErn);

            $split_grouping = $bonus_grouping['split'];

            $sumErn_split = $sumErn - $split - $split_grouping;


            $this->writeLog('Query 10 ke '.$no,FALSE);
            $temp_paymentMethod = PostTempPayment::where('TENDNBR', $row->TENDNBR)->where('FILE', $fileName_payment)->first(); // tambah where FILE = .....

            $this->writeLog('Proses Regular 1 ke '.$no,FALSE);
            $ress_point_regular = $this->_checkRegularPoint($postTemp, $sumErn_split, $customer, $temp_paymentMethod);
            $this->writeLog('Proses Regular 2 ke '.$no,FALSE);
            $ress_point_regular_nonsplit = $this->_checkRegularPoint($postTemp, $sumErn, $customer, $temp_paymentMethod);

            if (env('DEBUG_BONUS', false)) {
                echo'\n ress point ' . $ress_point_regular['point'];
            }

            $point_regular = ($ress_point_regular['point'] == NULL) ? 0 : $ress_point_regular['point'];
            $point_regular_nonsplit = ($ress_point_regular_nonsplit['point'] == NULL) ? 0 : $ress_point_regular_nonsplit['point'];



            $ld = 0;
            $this->writeLog('Proses Reward ke '.$no,FALSE);
            $get_reward = $this->_checkRewardPoint($postTemp, $customer, $sumErn);
            $point_reward = $get_reward['point_fix'] + ($get_reward['point_multiple'] * $point_regular_nonsplit);

            if (isset($customer->CUST_RECID) and isset($postTemp->STOREID)) {

                $this->writeLog('Query 11 ke '.$no,FALSE);
                $tenant = Tenant::where('TNT_CODE', $postTemp->STOREID)->first();
                //$product = Product::where('PRO_CODE', $postTemp->INTCODE)->first();
                //$vendors = $product->vendors();
                //  foreach($product->vendors as $val_vendor) { dd($val_vendor);  }
                //$vendor = $vendors[0]; // hanya diambil yang pertama
                // Assign to $row->rcpdate
                //  $postTemp->RCPDATE = date('Y-m-d', strtotime(str_replace('-', '', $postTemp->RCPDATE)));

                if (!env('DEBUG_BONUS', FALSE)) {
                    DB::beginTransaction();
                    try {

                        $this->writeLog('Query 12 ke '.$no,FALSE);
                        $counter = Counter::select('post_count')->where('pnum', $this->pnum)->where('ptype', $this->ptype);
                        $getCounter = $counter->first();
                        $RECEIPT_NO = $getCounter->post_count + 1;


                        $counter->update(['post_count' => $RECEIPT_NO]);
                        $this->writeLog('Proses Insert Post Ke '.$no,FALSE);
                        $post = new Post();
                        $post->POS_RECID = Uuid::uuid();
                        $post->POS_CUST_RECID = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_PNUM = $this->pnum;
                        $post->POS_PTYPE = $this->ptype;
                        $post->POS_STORE = $tenant->TNT_RECID;
                        $post->POS_BARCODE = $row->CARDID;
                        $post->POS_POST_DATE = $postTemp->RCPDATE;
                        $post->POS_POST_TIME = $postTemp->RCPTIME;
                        $post->POS_STATION_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                        $post->POS_SHIFT_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                        $post->POS_DOC_NO = $postTemp->RCPNBR;
                        $post->POS_CASHIER_ID = $postTemp->TERMID;
                        $post->POS_AMOUNT = $sumErn;
                        $post->POS_POINT_LD = $ld;
                        $post->POS_POINT_REGULAR = $point_regular;
                        $post->POS_POINT_REWARD = $point_reward;
                        $post->POS_POINT_BONUS = $point + $bonus_grouping['point'];
                        $post->POS_RECEIPT_TYPE = '1';
                        $post->POS_USERBY = (isset(Session::get('data')->USER_DATA->USER_RECID)) ? Session::get('data')->USER_DATA->USER_RECID : 'By System';
                        $post->POS_CREATEBY = '-';
                        $post->POS_UPDATE = 1;
                        $post->POS_TYPE = 0;
                        $post->POS_TRANSF_FROM = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_TRANSF_TO = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_RECEIPT_NO = 'EA/' . date('y', strtotime($postTemp->RCPDATE)) . '/' . sprintf("%'.07d".PHP_EOL, $RECEIPT_NO);
                        $post->TENDNBR = (isset($postTemp->payment->TENDNBR)) ? $postTemp->payment->TENDNBR : 0;
                        $post->BIN = (isset($postTemp->payment->BIN)) ? $postTemp->payment->BIN : 0;
                        $post->POS_VOID = 0;
                        $post->save();
                        $this->writeLog("-- POST {$row->CARDID} Saved --", env('DEBUG_EARNING_LOG', false));

                        $this->writeLog(" [" . $post->POS_POST_DATE . "|" . $post->POS_POST_TIME . "] Store : " . $post->POS_STORE . " Customer : " . $post->POS_BARCODE, env('DEBUG_EARNING_LOG', false));
                        $this->writeLog(" | Regular Point : " . $post->POS_POINT_REGULAR . " | Reward Point : " . $post->POS_POINT_REWARD . " | Bonus Point : " . $post->POS_POINT_BONUS . " | ", env('DEBUG_EARNING_LOG', false));
                        $this->writeLog(" ======================= ", env('DEBUG_EARNING_LOG', false));

                        $this->writeLog('Query 13 ke '.$no,FALSE);
                        $totalPoint = ZTotalPoint::where('TTL_CUST_RECID', $customer->CUST_RECID)->redeemPoint()->first();
                        $resultPoint = $point_regular + $point + $bonus_grouping['point'] + $point_reward;
                        if ($totalPoint) {
                            $totalPoint->TTL_POINT = $totalPoint->TTL_POINT + $resultPoint;
                            $totalPoint->save();
                            $this->writeLog("-- Total Point {$row->CARDID} Saved --", env('DEBUG_EARNING_LOG', false));
                        } else {
                            $totalPoint = new ZTotalPoint;
                            $totalPoint->TTL_RECID = Uuid::uuid();
                            $totalPoint->TTL_PNUM = $this->pnum;
                            $totalPoint->TTL_PTYPE = $this->ptype;
                            $totalPoint->TTL_CUST_RECID = $customer->CUST_RECID;
                            $totalPoint->TTL_CODE = 2;
                            $totalPoint->TTL_POINT = $resultPoint;
                            $totalPoint->save();
                            $this->writeLog("-- Total Point {$row->CARDID} Saved --", env('DEBUG_EARNING_LOG', false));
                        }

                        $customer_earn = $customer->CUST_EXPENSE_EARN;
                        $customer->CUST_EXPENSE_EARN = $customer_earn + $sumErn;
                        $customer->save(); //save new CUST_EXPENSE_EARN

                        try {
                            foreach ($postTempAll as $row_postTemp) {

                                $this->writeLog('Query 14 ke '.$no,FALSE);
                                $product = Product::where('PRO_CODE', $row_postTemp->INTCODE)->first();

                                $this->writeLog('Proses Save Post Detail ke '.$no,FALSE);
                                $postDetail = new PostDetail();
                                $postDetail->PSD_RECID = Uuid::uuid();
                                $postDetail->PSD_POS_RECID = $post->POS_RECID;
                                $postDetail->PSD_STORE = $tenant->TNT_RECID;
                                $postDetail->PSD_FORMULA_ID = $product->PRO_RECID;  // Hanya menyimpan Formula dari Bonus Point Formula by Product / Merchan.
                                $postDetail->PSD_STATION_ID = $post->POS_STATION_ID;
                                $postDetail->PSD_SHIFT_ID = $post->POS_SHIFT_ID;
                                $postDetail->PSD_DOC_NO = $post->POS_DOC_NO;
                                $postDetail->PSD_TRAN_DATE = $post->POS_POST_DATE;
                                $postDetail->PSD_TRAN_TIME = $post->POS_POST_TIME;
                                $postDetail->PSD_TYPE = 0;  // ??
                                $postDetail->PSD_SUBTYPE = 0;  // ??
                                $postDetail->PSD_VENDOR = $product->PRO_RECID;
                                $postDetail->PSD_ARTICLE = $product->PRO_RECID;
                                $postDetail->PSD_QTY = $row_postTemp->ITEMQTY;  // harus SUM QTY ??
                                $postDetail->PSD_AMOUNT = $row_postTemp->TOTALAFTERDISC;
                                $postDetail->PSD_POINT = $post->POS_POINT_BONUS;  //
                                $postDetail->PSD_PAYMENT = $product->PRO_RECID;  // harus menyimpan payment method recid
                                // $postDetail->PSD_BANK = $payment;  // harus menyimpan payment method recid
                                $postDetail->POS_VOID = 0;  //
                                $postDetail->PSD_UPDATE = 0;  //

                                $postDetail->save();

                                //$this->writeLog("-- Article {$row_postTemp->INTCODE} Saved In Post Detail --", env('DEBUG_EARNING_LOG', false));
                            }
                        } catch (\Exception $e) {
                            $this->writeErrorLog("Error Save to Post Detail with Message : {$e->getMessage()}");
                        }

                        // jika didapatkan lucky draw
                        if ($ld_grouping['point'] > 0) {

                            // looping sesuai jumlah LD yang didapat
                            foreach ($ld_grouping['point'] as $row) {

                                $this->writeLog('Query 15 ke '.$no,FALSE);
                                $getInc = PointRandom::select('RAND_RECID')->count();
                                $this->writeLog('Proses Get LD Number '.$no,FALSE);
                                $kode = $this->_getLDNumber($getInc, $tenant->TNT_RECID, $post->POS_DOC_NO);

                                $this->writeLog('Proses Save LD '.$no,FALSE);
                                $pointRandom = new PointRandom();
                                $pointRandom->RAND_RECID = Uuid::uuid();
                                $pointRandom->RAND_CUST_RECID = $customer->CUST_RECID;
                                $pointRandom->RAND_POS_RECID = $post->POS_RECID;
                                $pointRandom->RAND_TYPE = 1;
                                $pointRandom->RAND_POINT_1 = $kode;   //  incremental (4 digit) ,kode store (4 digit), receipt no (5)
                                $pointRandom->RAND_STS = 0;
                                $pointRandom->RAND_WIN = 0;
                                $pointRandom->save();
                            }
                        }


                        DB::commit();
                        $this->writeLog("-- Post Detail Saved --".PHP_EOL, env('DEBUG_EARNING_LOG', false));
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $this->writeErrorLog("Error Save to DB with Message : {$e->getMessage()}");
                    }
                }
            }

            $no++;
        }

        if (env('DEBUG_BONUS', FALSE)) {
            // exit;
        }

//        DB::table('POSTTEMP')->delete();
//        DB::table('POSTTEMPPAYMENT')->delete();
//        DB::table('POSTTEMP_unclean')->delete();
//        DB::table('POSTTEMPPAYMENT_unclean')->delete();
    }

    public function _getLDNumber($getInc, $kode_store, $rcp_number) {

        $getIncPlus = $getInc + 1;
        $inc = str_pad($getIncPlus, 10, "0", STR_PAD_LEFT);

        $kode = $inc;
        $checkInc = PointRandom::select('RAND_POINT_1')->where('RAND_POINT_1', $kode)->count();

        if ($checkInc > 0) {
            $this->_getLDNumber($inc, $kode_store, $rcp_number);
        } else {
            return $kode;
        }
    }

    public function _checkRegularPoint($row, $sumErn, $customer, $temp_paymentMethod = null) {



        $point = 0;
        $pf = array();
         $return_point = 0;

         if($row!=NULL and isset($row->RCPDATE)) {

            $this->writeLog('Query Regular Point Ke 1 ',FALSE);
        $formula = FormulaEarn::where('ERN_TYPE', 0)
                ->where('ERN_FORMULA', 0) // regular point
                ->where('ERN_FORMULA_DTL', 0)
                ->where('ERN_ACTIVE', 1) // active
                ->where('ERN_VALUE', 0)
                ->where('ERN_FRDATE', '<=', $row->RCPDATE) // from date
                ->where('ERN_TODATE', '>=', $row->RCPDATE); // to date
        // to datetime
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

        $formula_check = $formula->get();


        $formula_load = array();

        //check batas waktu (jam, menit, second) jika formula aktif ditanggal awal dan tanggal ahir
        foreach ($formula_check as $row_check) {
            if (!empty($row_check) && $row_check->ERN_AMOUNT != 0) { // jika dapet point
                if ($row->RCPDATE == $row_check->ERN_FRDATE or $row->RCPDATE == $row_check->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                    $this->writeLog('Query Regular Point Ke 2 ',FALSE);
                    $formula_time = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME) // from datetime
                            ->where('ERN_TOTIME', '>=', $row->RCPTIME);
                    $row_check_time = $formula_time->first();
                    if (count($row_check_time) > 0) {
                        $formula_load[] = $row_check;
                    }
                } else {
                    $formula_load[] = $row_check;
                }
            }
        }



        $best_point = array(); // untuk mencari best point

        if (!empty($formula_load)) {


            foreach ($formula_load as $formula_ress) {

                if (isset($formula_ress->ERN_AMOUNT)) {

                    $temp1 = (float) $sumErn;
                    $temp2 = (float) $formula_ress->ERN_AMOUNT;

                    if ($formula_ress->ERN_MULTIPLE_BASIC == 1) { // jika multiple basic ON
                        $pointamount = floor(@($temp1 / $temp2));
                        // ex : 95.000/50.000 = 1
                        $point = $pointamount * $formula_ress->ERN_POINT;
                        //jika normal point
                    } else {
                        if ($sumErn >= $formula_ress->ERN_AMOUNT) {
                            $point = 1 * $formula_ress->ERN_POINT;
                        } else {
                            $point = 0;
                        }
                    }


                    // Min Max Amount
                    if ($formula_ress->ERN_ACTIVE_MAX == 1) {
                        if ($sumErn < $formula_ress->ERN_MIN_AMOUNT) { //jika Belanja Kurang Dari Minimum Amount
                            $point = $point;
                        }

                        if ($sumErn >= $formula_ress->ERN_MIN_AMOUNT && $sumErn <= $formula_ress->ERN_MAX_AMOUNT) {
                            //jika belanja diantara rentang Min Max
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {
                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;
                            }
                        }

                        if ($sumErn > $formula_ress->ERN_MAX_AMOUNT && $formula_ress->ERN_MAX_AMOUNT != 0) { //jika Belanja Lebih Dari Max Amount
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {

                                $moreErn = $sumErn - $formula_ress->ERN_MAX_AMOUNT;
                                $moreAmount = floor(@($moreErn / $formula_ress->ERN_AMOUNT));
                                $pointamount = floor(@($formula_ress->ERN_MAX_AMOUNT / $formula_ress->ERN_AMOUNT));
                                $morePoint = $moreAmount * $formula_ress->ERN_POINT;
                                $point_inmax = ($pointamount * $formula_ress->ERN_POINT) * $formula_ress->ERN_MULTIPLE_VALUE;
                                $point = $point_inmax + $morePoint;
                                //point < Max dikalikan multipler dan yang > max ($morePoint) diberikan normal point kemudian keduanya dijumlahkan
                                //                        echo 'Total Belanja = ' . $sumErn . ''.PHP_EOL;
                                //                        echo 'Max = ' . $formula_ress->ERN_MAX_AMOUNT . ''.PHP_EOL;
                                //                        echo 'Min = ' . $formula_ress->ERN_MIN_AMOUNT . ''.PHP_EOL;
                                //                        echo 'Sisa Total Belanja = ' . $moreAmount . ''.PHP_EOL;
                                //                        echo 'Point Dalam Max = ' . '(' . $pointamount . '*' . $formula_ress->ERN_POINT . ') *' . $formula_ress->ERN_MULTIPLE_VALUE . ' = ' . $point_inmax . ''.PHP_EOL;
                                //                        echo 'Point Lebih = ' . $moreAmount . ' * ' . $formula_ress->ERN_POINT . ' = ' . $morePoint . ''.PHP_EOL;
                                //                        echo 'Total Point = ' . $point;
                                //                        exit;
                            }
                        }
                    }

                    if ($formula_ress->ERN_ACTIVE_MAXAMOUNT == 1) { // jika MAX AMOUNT active
                        if ($formula_ress->ERN_ACTIVE_REST == 1) { // jika REST AMOUNT active
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $temp3 = $sumErn - $formula_ress->ERN_MAXAMOUNT_VALUE; //selisih lebih

                                $pointamount = floor(@($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2));
                                $point = $pointamount * $formula_ress->ERN_POINT;

                                $pointamountress = floor(@($temp3 / $formula_ress->ERN_AMOUNT_REST));
                                $pointress = $pointamountress * $formula_ress->ERN_POINT_REST;

                                $point = $point + $pointress; //point normal + point ress
                            }
                        } elseif ($formula_ress->ERN_ACTIVE_REST == 0) { // jika REST AMOUNT inactive
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $pointamount = floor(@($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2));
                                $point = $pointamount * $formula_ress->ERN_POINT;
                            }
                        }
                    }


                    if (isset($customer->CUST_GENDER)) {
                        switch ($customer->CUST_GENDER) {
                            case 'M':
                                $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                                break;
                            case 'F':
                                $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                                break;
                        } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
                    }

                    if ($customer->CUST_GENDER == null) {
                        $this->writeLog('Query Regular Point Ke 3 ',FALSE);
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'GEN')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_GENDER = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_BLOOD == null) {
                        $this->writeLog('Query Regular Point Ke 4 ',FALSE);
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'BLOD')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_BLOOD = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_RELIGION == null) {
                        $this->writeLog('Query Regular Point Ke 5 ',FALSE);
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'RELI')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_RELIGION = $lookUp->LOK_RECID; //other
                    }

                    $this->writeLog('Query Regular Point Ke 6 ',FALSE);
                    $formulaMember = FormulaEarnMember::where('MEM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('MEM_LOK_RECID', '=', $customer->CUST_MEMTYPE)->get();
                    $this->writeLog('Query Regular Point Ke 7 ',FALSE);
                    $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
                    $this->writeLog('Query Regular Point Ke 8 ',FALSE);
                    $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
                    $this->writeLog('Query Regular Point Ke 9 ',FALSE);
                    $formulaCardType = FormulaEarnCardType::where('CDTY_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CDTY_LOK_RECID', '=', $customer->CUST_CARDTYPE)->get();
                    //card type di point formula diubah refer ke z_cardtype bukan ke z_lookup kembali

                    if ($temp_paymentMethod != null) {
                        $this->writeLog('Query Regular Point Ke 10 ',FALSE);
                        $payment_method_recid = PaymentMethod::where('code', $temp_paymentMethod)->first();
                        $this->writeLog('Query Regular Point Ke 11 ',FALSE);
                        $formulaCardPayment = FormulaEarnPayment::where('PAY_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('PAY_LOK_RECID', '=', $payment_method_recid['id'])->get();
                        if (count($formulaCardPayment) < 1) {
                            $point = 0; //gender tidak ditemukan
                            echo 'payment tidak ditemukan '.PHP_EOL;
                        }
                    }

                    //formula ern store belum dibuat model dan table nya
                    $this->writeLog('Query Regular Point Ke 12 ',FALSE);
                    $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();
                    $this->writeLog('Query Regular Point Ke 13 ',FALSE);
                    $tenant = Tenant::where('TNT_CODE', $row->STOREID)->first();
                    $this->writeLog('Query Regular Point Ke 14 ',FALSE);
                    $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                    ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                    ->where('TENT_ACTIVE', 1)->get();

                    echo ''.PHP_EOL;
                    if (count($formulaTenant) < 1) {
                        $point = 0; //memtype tidak ditemukan
                        echo 'store tidak ditemukan '.PHP_EOL;
                    }

                    if (count($formulaMember) < 1) {
                        $point = 0; //memtype tidak ditemukan
                        echo 'member type tidak ditemukan '.PHP_EOL;
                    }
                    if (count($formulaGender) < 1) {
                        $point = 0; //gender tidak ditemukan
                        echo 'gender tidak ditemukan '.PHP_EOL;
                    }
                    if (count($formulaReligion) < 1) {
                        $point = 0; //gender tidak ditemukan
                        echo 'religion tidak ditemukan '.PHP_EOL;
                    }
                    if (count($formulaCardType) < 1) {
                        $point = 0; //Card Type tidak ditemukan
                        echo 'card type tidak ditemukan '.PHP_EOL;
                    }
                    if (count($formulaBlood) < 1) {
                        $point = 0; //gender tidak ditemukan
                        echo 'blood tidak ditemukan '.PHP_EOL;
                    }


                    if (isset($point) && $point < 0) {
                        $point = 0;
                    }

                    if ($point == null) {
                        $point = 0;
                    }




                    $best_point[] = $this->_filterTransaction($row->CARDID, $formula_ress->ERN_RECID, $row->STOREID, $point,$row->RCPDATE);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                    $pf[] = $pf_founded;
                }
            }
        }



        $return_point = 0;

        if (count($best_point) != 0) {
            $return_point = max($best_point);
        }
        // echo '\n return point = '.$return_point.' PF : '.$pf;
         }
        return ['point' => $return_point, 'pf' => $pf];
    }

    public function _checkBonusPoint($row) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();

        $pf = array();
        $TOTALAFTERDISC = $row[14 - env('LOCALARRAY', 0)];
        $ITEMQTY = $row[10 - env('LOCALARRAY', 0)];



        $group['brand_qty'] = 0;
        $group['brand_amount'] = 0;
        $group['vendor_qty'] = 0;
        $group['vendor_amount'] = 0;
        $group['dept_qty'] = 0;
        $group['dept_amount'] = 0;
        $group['div_qty'] = 0;
        $group['div_amount'] = 0;
        $group['cat_qty'] = 0;
        $group['cat_amount'] = 0;
        $group['subcat_qty'] = 0;
        $group['subcat_amount'] = 0;
        $group['subcat_qty'] = 0;
        $group['subcat_amount'] = 0;
        $group['segment_qty'] = 0;
        $group['segment_amount'] = 0;
        $group['vendor_qty'] = 0;
        $group['vendor_amount'] = 0;



        $bonus_type = [
            9 => 'All Product'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point


        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {
            $this->writeLog('Query Bonus Point Ke 1 ',FALSE);
            $formula = FormulaEarn::
                    where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_TODATE', '>=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_FORMULA', 1);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();

            if (!empty($formula_load)) {
                foreach ($formula_load as $formula_ress) {
                    $point = 0;
                    $check_time = TRUE;

                    if ($row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_FRDATE OR $row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir
                        $this->writeLog('Query Bonus Point Ke 2 ',FALSE);
                        $formula = $formula->where('ERN_FRTIME', '<=', $row[3 - env('LOCALARRAY', 0)])  // from datetime
                                ->where('ERN_TOTIME', '>=', $row[3 - env('LOCALARRAY', 0)])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                    }

                    if (isset($row[7 - env('LOCALARRAY', 0)]) && $row[7 - env('LOCALARRAY', 0)] != '') {

                        if ($key_bonus_type == 1 AND $check_time == TRUE) { // jika produk
                            //Product
                            $this->writeLog('Query Bonus Point Ke 3 ',FALSE);
                            $product = Product::select('PRO_CODE', 'PRO_RECID')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $this->writeLog('Query Bonus Point Ke 4 ',FALSE);
                            $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('ITM_PRODUCT', '=', $product['PRO_RECID'])->get();
                        } elseif ($key_bonus_type == 2 and $check_time == true) { // jika brand
                            //Brand
                            $this->writeLog('Query Bonus Point Ke 5 ',FALSE);
                            $product = Product::select('PRO_CODE', 'PRO_DESCR', 'PRO_RECID', 'brand_id')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $this->writeLog('Query Bonus Point Ke 6 ',FALSE);
                            $brands = Brand::select('id', 'code', 'name')->where('id', $product['brand_id'])->first();
                            $this->writeLog('Query Bonus Point Ke 7 ',FALSE);
                            $formulaItem = FormulaEarnBrand::where('BRAND_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BRAND_BRAND_RECID', '=', $brands['id'])->get();

                            if (count($formulaItem) > 0) {
                                $group['brand_qty'] = $group['brand_qty'] + $ITEMQTY;
                                $group['brand_amount'] = $group['brand_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['brand_qty'] = 0;
                                $group['brand_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                            //Department
                            $this->writeLog('Query Bonus Point Ke 8 ',FALSE);
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $this->writeLog('Query Bonus Point Ke 9 ',FALSE);
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $this->writeLog('Query Bonus Point Ke 10 ',FALSE);
                            $formulaItem = FormulaEarnDepartment::where('DEPT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DEPT_DEPARTMENT', '=', $merchant->department->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {
                                $group['dept_qty'] = $group['dept_qty'] + $ITEMQTY;
                                $group['dept_amount'] = $group['dept_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['dept_qty'] = 0;
                                $group['dept_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                            //Division
                            $this->writeLog('Query Bonus Point Ke 11 ',FALSE);
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $this->writeLog('Query Bonus Point Ke 12 ',FALSE);
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $this->writeLog('Query Bonus Point Ke 13 ',FALSE);
                            $formulaItem = FormulaEarnDivision::where('DIV_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DIV_DIVISION', '=', $merchant->division->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['div_qty'] = $group['div_qty'] + $ITEMQTY;
                                $group['div_amount'] = $group['div_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['div_qty'] = 0;
                                $group['div_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                            //Category
                            $this->writeLog('Query Bonus Point Ke 14 ',FALSE);
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $this->writeLog('Query Bonus Point Ke 15 ',FALSE);
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $this->writeLog('Query Bonus Point Ke 16 ',FALSE);
                            $formulaItem = FormulaEarnCategory::where('CAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CAT_CATEGORY', '=', $merchant->category->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {
                                $group['cat_qty'] = $group['cat_qty'] + $ITEMQTY;
                                $group['cat_amount'] = $group['cat_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['cat_qty'] = 0;
                                $group['cat_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                            //Departement
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnSubCategory::where('SCAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SCAT_SUBCATEGORY', '=', $merchant->subcategory->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['subcat_qty'] = $group['subcat_qty'] + $ITEMQTY;
                                $group['subcat_amount'] = $group['subcat_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['subcat_qty'] = 0;
                                $group['subcat_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                            //Departement
                            $this->writeLog('Query Bonus Point Ke 17 ',FALSE);
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $this->writeLog('Query Bonus Point Ke 18 ',FALSE);
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $this->writeLog('Query Bonus Point Ke 19 ',FALSE);
                            $formulaItem = FormulaEarnSegment::where('SEG_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SEG_SEGMENT', '=', $merchant->segment->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['segment_qty'] = $group['segment_qty'] + $ITEMQTY;
                                $group['segment_amount'] = $group['segment_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['segment_qty'] = 0;
                                $group['segment_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                            $this->writeLog('Query Bonus Point Ke 20 ',FALSE);
                            $product = Product::select('PRO_CODE', 'PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = $product->vendors()->get();
                            $countVendor = array();
                            foreach ($merchant as $row_vendor) {
                                $formulaVendor = FormulaEarnVendor::where('VND_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('VND_VENDOR_RECID', '=', $row_vendor['PROV_VDR_RECID'])->get();
                                if (count($formulaVendor) > 0) {
                                    $countVendor[] = $row_vendor['PROV_VDR_RECID'];
                                }
                            }

                            $formulaItem = $countVendor;

                            if (count($formulaItem) > 0) {
                                $group['vendor_qty'] = $group['vendor_qty'] + $ITEMQTY;
                                $group['vendor_amount'] = $group['vendor_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['vendor_qty'] = 0;
                                $group['vendor_amount'] = 0;
                            }
                        }

                        if ($key_bonus_type == 9 AND $check_time == TRUE) {

                            $formulaItem = array(0 => 'All Product', 1 => 'All Product');
                        }
                    }


                    if (count($formulaItem) > 0) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount '.PHP_EOL : 'by quantity '.PHP_EOL;
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . ''.PHP_EOL . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = $TOTALAFTERDISC - $sisa_bagi; // jika terdapat sisa bagi,, akan ditampung di split
                                    $best_point_multiple[] = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;
                                        if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
                                        $best_point_fix[] = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);
                                    }
                                }
                            }
                        }





                        if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                            // $row[10 - env('LOCALARRAY', 0)] = $row[10 - env('LOCALARRAY', 0)] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;

                                $best_point_fix[] = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);
                            }

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . ''.PHP_EOL . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                            }
                        }
                    }


                    $point = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }

                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = max($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;

                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }

        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }

    public function _checkBonusPayment($row) {

        $pf = array();
        $point = 0;
        // $split = 0;
        $last_poin = 0;
        $bonus_type = [
            0 => 'Payment'
        ];

        $multiple = 0;

        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {
            $this->writeLog('Query Bonus Payment Point Ke 1 ',FALSE);
            $cek = FormulaEarn::where('ERN_FORMULA', 1)
                    ->where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_TODATE', '>=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_FORMULA_TYPE', $key_bonus_type)->first();

            if(count($cek) >= 1) {
                $this->writeLog('Query Bonus Payment Point Ke 2 ',FALSE);
            $formula = FormulaEarn::where('ERN_FORMULA', 1)
                    ->where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_TODATE', '>=', $row[2 - env('LOCALARRAY', 0)]);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();
            echo ":: CEK PAYMENT BONUS ::".PHP_EOL;
        if (!empty($formula_load)) {

                foreach ($formula_load as $formula_ress) {
                    echo ":: PROSES PAYMENT BONUS ::".PHP_EOL;
                    //$point = 0;
                    $check_time = true;

                    if ($row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_FRDATE or $row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                        $this->writeLog('Query Bonus Payment Point Ke 4 ',FALSE);
                        $formula = $formula->where('ERN_FRTIME', '<=', $row[3 - env('LOCALARRAY', 0)]) // from datetime
                                ->where('ERN_TOTIME', '>=', $row[3 - env('LOCALARRAY', 0)])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula > first())) ? true : false;
                    }

                    if ($key_bonus_type == 0 and $check_time == true) { // jika payment
                        $this->writeLog('Query Bonus Payment Point Ke 5 ',FALSE);
                        $payment_method = PaymentMethod::where('code', 0)->first();
                        $formulaItem = FormulaEarnPayment::where('PAY_ERN_RECID', $formula_ress->ERN_RECID)->where('PAY_LOK_RECID', $payment_method['id'])->get();
                    }

                    if (count($formulaItem) > 0) {

                        $multiple = $formula_ress->ERN_MULTIPLE;

                        if ($formula_ress->ERN_VALUE == 0) { // By Amount
                            $pointamount = floor(@($row[8 - env('LOCALARRAY', 0)] / $formula_ress->ERN_AMOUNT));
                            $point = $pointamount * $formula_ress->ERN_POINT;
                        }

                        if (isset($formula_ress->ERN_MULTIPLE)) {
                            if ($formula_ress->ERN_MULTIPLE == 1) {
                                if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                    $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                }

                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE; // Multiple
                            } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                if (isset($pointamount)) {
                                    $point = @($point / $pointamount); // Fix Point
                                }
                            }
                        }
                    } else {
                        $point = 0;
                    }

                    $this->writeLog('Query Bonus Payment Point Ke 6 ',FALSE);
                    $customer = Customer::where('CUST_BARCODE', $row[6 - env('LOCALARRAY', 0)])->first();

                    if (isset($customer->CUST_GENDER)) {
                        switch ($customer->CUST_GENDER) {
                            case 'M':
                                $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                                break;
                            case 'F':
                                $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                                break;
                        } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
                    }

                    if ($customer->CUST_GENDER == null) {
                        $this->writeLog('Query Bonus Payment Point Ke 7 ',FALSE);
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'GEN')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_GENDER = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_BLOOD == null) {
                        $this->writeLog('Query Bonus Payment Point Ke 8 ',FALSE);
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'BLOD')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_BLOOD = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_RELIGION == null) {
                        $this->writeLog('Query Bonus Payment Point Ke 9 ',FALSE);
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'RELI')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_RELIGION = $lookUp->LOK_RECID; //other
                    }

                    $this->writeLog('Query Bonus Payment Point Ke 10 ',FALSE);
                    $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
                    $this->writeLog('Query Bonus Payment Point Ke 11 ',FALSE);
                    $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
                    $this->writeLog('Query Bonus Payment Point Ke 12 ',FALSE);
                    $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();

                    $this->writeLog('Query Bonus Payment Point Ke 13 ',FALSE);
                    $tenant = Tenant::where('TNT_CODE', $row[1 - env('LOCALARRAY', 0)])->first();
                    $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                    ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                    ->where('TENT_ACTIVE', 1)->get();

                    if (count($formulaTenant) < 1) {
                        $point = 0; //memtype tidak ditemukan
                        // echo 'member type tidak ditemukan '.PHP_EOL;
                    }

                    if (count($formulaGender) < 1) {
                        $point = 0;
                        //echo 'gender tidak ditemukan'; exit;
                    }
                    if (count($formulaReligion) < 1) {
                        $point = 0;
                        //  echo 'religion tidak ditemukan '.PHP_EOL;
                    }
                    if (count($formulaBlood) < 1) {
                        $point = 0;
                        //  echo 'blood tidak ditemukan '.PHP_EOL;
                    }
                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                    $pf[] = $pf_founded;
                    // echo $point.''.PHP_EOL;
                    $last_poin = $last_poin + $point;
                }

                if ($last_poin < 0) {
                    $last_poin = 0;
                }
            } }
        }

        $return = array('point' => $last_poin, 'multiple' => $multiple, 'pf' => $pf);



        return $return;
    }

    public function _checkBonusPointGroup($row, $customer, $sumErn, $fileName = null) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();


        $pf = array();


        $bonus_type = [
            1 => 'Point Earn by Product',
            2 => 'Point Earn Bonus by Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendors',
            9 => 'All Product'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point

        $this->writeLog('Query Bonus Point Group Ke 1 ',FALSE);
        $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
        $minimum_transaction = $preference['value'];


        if($row!=NULL) {
        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $this->writeLog('Query Bonus Point Group Ke 2 ',FALSE);
            $formula = FormulaEarn::where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row['RCPDATE'])
                    ->where('ERN_TODATE', '>=', $row['RCPDATE'])
                    ->where('ERN_FORMULA', 1)->where('ERN_FORMULA_TYPE', $key_bonus_type);

            $formula_load = $formula->get();

            if (!empty($formula_load)) {
                $no = 0;
                foreach ($formula_load as $formula_ress) {
                    $no++;
                    $point = 0;
                    $check_time = TRUE;



                    if ($row['RCPDATE'] == $formula_ress->ERN_FRDATE OR $row['RCPDATE'] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir

                        $this->writeLog('Query Bonus Point Group Ke 3 ',FALSE);
                        $formula_check = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME'])  // from datetime
                                ->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula_check->first()) ) ? TRUE : FALSE;
                    }


                    if ($key_bonus_type == 1 AND $check_time == TRUE) { // jika produk
                        //Product
                        $ERN_RECID = $formula_ress->ERN_RECID;
                        $this->writeLog('Query Bonus Point Group Ke 4 ',FALSE);
                        $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->get();


                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_product) {
                            //query terberat sebelumnya disini
                            $count = DB::select("select SUM(ITEMQTY) as qty,
                            SUM(TOTALAFTERDISC) as total
                            from POSTTEMP
                            WHERE CARDID = '{$row->CARDID}' AND [FILE] = '{$fileName}'
                            AND INTCODE IN (select PRO_CODE from Z_Product_Item where PRO_RECID = '{$formulaItem_product->ITM_PRODUCT}') ");

                            /*
                            $this->writeLog("select SUM(ITEMQTY) as qty,
                            SUM(TOTALAFTERDISC) as total
                            from POSTTEMP
                            WHERE CARDID = '{$row->CARDID}' AND [FILE] = '{$fileName}'
                            AND INTCODE IN (select PRO_CODE from Z_Product_Item where PRO_RECID = '{$formulaItem_product->ITM_PRODUCT}') ",FALSE);
*/
                            // Mendapatkan product di table POSTEMP
                            // tambah where FILE = .....
                            $count = $count[0];
                            $this->writeLog('Query Bonus Point Group Ke 6 ',FALSE);
                            $rowGroup[] = array(
                                'product_name' => '',
                                'qty' => $count->qty,
                                'total' => $count->total,
                                'post' => []);
                        }


                    } elseif ($key_bonus_type == 2 and $check_time == true) { // jika brand
                        //Brand
                        $ERN_RECID = $formula_ress->ERN_RECID;

                        $formulaItem = $formula_ress->brand;

                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_brand) {

                            $this->writeLog('Query Bonus Point Group Ke 7 ',FALSE);
                            $formulaItem_product = Product::select('*')->where('brand_id', $formulaItem_brand->brand->id)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // Mendapatkan product dari Brand terkait di table POSTEMP
                            // tambah where FILE = .....
                            $this->writeLog('Query Bonus Point Group Ke 8 ',FALSE);
                            $rowGroup[] = array('brand' => $formulaItem_brand->brand->id,
                                'brand_name' => $formulaItem_brand->brand->name,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->get());
                        }
                    } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                        //Department
                        $formulaItem = $formula_ress->department;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_department) {
                            $this->writeLog('Query Bonus Point Group Ke 9 ',FALSE);
                            $merchant = Merchandise::where('MERC_DEPARTMENT', $formulaItem_department->DEPT_DEPARTMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query Bonus Point Group Ke 10 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query Bonus Point Group Ke 11 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_department->department->LOK_RECID,
                                'merchand_name' => $formulaItem_department->department->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->get());
                        }
                    } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                        //Division
                        $formulaItem = $formula_ress->division;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_division) {
                            $this->writeLog('Query Bonus Point Group Ke 12 ',FALSE);
                            $merchant = Merchandise::where('MERC_DIVISION', $formulaItem_division->DIV_DIVISION)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query Bonus Point Group Ke 13 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query Bonus Point Group Ke 14 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_division->division->LOK_RECID,
                                'merchand_name' => $formulaItem_division->division->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->get());
                        }
                    } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                        $formulaItem = $formula_ress->category;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_category) {
                            $this->writeLog('Query Bonus Point Group Ke 15 ',FALSE);
                            $merchant = Merchandise::where('MERC_CATEGORY', $formulaItem_category->CAT_CATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query Bonus Point Group Ke 16 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query Bonus Point Group Ke 17 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_category->category->LOK_RECID,
                                'merchand_name' => $formulaItem_category->category->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                        $formulaItem = $formula_ress->subcategory;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_subcategory) {
                            $this->writeLog('Query Bonus Point Group Ke 18 ',FALSE);
                            $merchant = Merchandise::where('MERC_SUBCATEGORY', $formulaItem_subcategory->SUBCAT_SUBCATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query Bonus Point Group Ke 19 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_subcategory->subcategory->LOK_RECID,
                                'merchand_name' => $formulaItem_subcategory->subcategory->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                        $formulaItem = $formula_ress->segment;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_segment) {
                            $this->writeLog('Query Bonus Point Group Ke 20 ',FALSE);
                            $merchant = Merchandise::where('MERC_SEGMENT', $formulaItem_segment->SEG_SEGMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query Bonus Point Group Ke 21 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query Bonus Point Group Ke 22 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_segment->segment->LOK_RECID,
                                'merchand_name' => $formulaItem_segment->segment->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                        $formulaItem = $formula_ress->vendor;
                        // dd($formulaItem );
                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_vendor) {
                            $this->writeLog('Query Bonus Point Group Ke 23 ',FALSE);
                            $vendor = Vendor::where('VDR_RECID', $formulaItem_vendor->VND_VENDOR_RECID)->first(); //dd($vendor);
                            $formulaItem_product = $vendor->products;

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query Bonus Point Group Ke 24 ',FALSE);
                            $rowGroup[] = array('vendor' => $formulaItem_vendor->vendor->VDR_RECID,
                                'vendor_name' => $formulaItem_vendor->vendor->VDR_NAME,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    }
                    elseif ($key_bonus_type == 9 AND $check_time == TRUE) { // jika all product
                         $getProduct = array();
						  $getProduct[] = 'All Product';
                                                  $rowGroup = array();

			  $rowGroup[] = array('product name' => 'All',
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->where('FILE',$fileName)->get());

                    }



                    if (!isset($rowGroup['total'])) {
                        $rowGroup['total'] = 0;
                    }
                    if (!isset($rowGroup['qty'])) {
                        $rowGroup['qty'] = 0;
                    }


                    // ...
                    $TOTALAFTERDISC = array_sum(array_column($rowGroup, 'total'));
                    $ITEMQTY = array_sum(array_column($rowGroup, 'qty'));

                    $ITEMPOST = array_sum(array_column($rowGroup, 'post'));



                    if ($TOTALAFTERDISC > 0 and $ITEMQTY > 0) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount '.PHP_EOL : 'by quantity '.PHP_EOL;
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (TRUE) {
                                echo 'Product : '.PHP_EOL;
                                //print_r($getProduct);
                                echo ''.PHP_EOL;
                                echo $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = $TOTALAFTERDISC - $sisa_bagi; // jika terdapat sisa bagi,, akan ditampung di split
                                    if ($TOTALAFTERDISC < $minimum_transaction) {
                                        $point = 0;
                                    }
                                    $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;
                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
                                        elseif($TOTALAFTERDISC < $formula_ress->ERN_AMOUNT) {
                                            $point = 0;
                                        }


                                        if ($TOTALAFTERDISC < $minimum_transaction) {
                                            $point = 0;
                                        }
                                        $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                    }
                                }
                            }
                        }





                        if ($formula_ress->ERN_VALUE == 1) {       // By Quantity
                            // $row['ITEMQTY'] = $row['ITEMQTY'] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;
                                if ($TOTALAFTERDISC < $minimum_transaction) {
                                    $point = 0;
                                }
                                $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                            }

                            if (TRUE) {
                                echo 'Product : '.PHP_EOL;
                                print_r($getProduct);
                                echo ''.PHP_EOL;
                                echo $point . ' = ' . $formula_ress->ERN_POINT . ' <br/> by QTY<hr/>';
                            }
                        }
                    }

                    echo " Total Amount PF BONUS ke " . $no . " : " . $TOTALAFTERDISC . ' '.PHP_EOL;
                    echo " With Minimum trans : " . $minimum_transaction . ' '.PHP_EOL;
                    echo "-----------------------------------------------------------".PHP_EOL;


                    $point = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }



                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = array_sum($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;



                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }

        }
        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }

    public function _checkLDPointGroup($row, $customer, $sumErn) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();


        $pf = array();


        $bonus_type = [
            2 => 'Point Earn LD by Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendors'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point

if($row['RCPDATE']!=NULL) {
        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {
            $this->writeLog('Query LD Group Ke 1 ',FALSE);
            $formula = FormulaEarn::where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row['RCPDATE'])
                    ->where('ERN_TODATE', '>=', $row['RCPDATE'])
                    ->where('ERN_FORMULA', 3);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();



            if (!empty($formula_load)) {
                foreach ($formula_load as $formula_ress) {
                    $point = 0;
                    $check_time = TRUE;

                    if ($row['RCPDATE'] == $formula_ress->ERN_FRDATE OR $row['RCPDATE'] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir
                        $this->writeLog('Query LD Group Ke 2 ',FALSE);
                        $formula = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME'])  // from datetime
                                ->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                    }



                    if ($key_bonus_type == 2 and $check_time == true) { // jika brand
                        //Brand
                        $ERN_RECID = $formula_ress->ERN_RECID;

                        $formulaItem = $formula_ress->brand;

                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_brand) {
                            $this->writeLog('Query LD Group Ke 3 ',FALSE);
                            $formulaItem_product = Product::select('*')->where('brand_id', $formulaItem_brand->brand->id)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            // Mendapatkan product dari Brand terkait di table POSTEMP
                            $this->writeLog('Query LD Group Ke 4 ',FALSE);
                            $rowGroup[] = array('brand' => $formulaItem_brand->brand->id,
                                'brand_name' => $formulaItem_brand->brand->name,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                        //Department
                        $formulaItem = $formula_ress->department;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_department) {
                            $this->writeLog('Query LD Group Ke 5 ',FALSE);
                            $merchant = Merchandise::where('MERC_DEPARTMENT', $formulaItem_department->DEPT_DEPARTMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query LD Group Ke 6 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query LD Group Ke 7 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_department->department->LOK_RECID,
                                'merchand_name' => $formulaItem_department->department->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                        //Division
                        $formulaItem = $formula_ress->division;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_division) {
                            $this->writeLog('Query LD Group Ke 8 ',FALSE);
                            $merchant = Merchandise::where('MERC_DIVISION', $formulaItem_division->DIV_DIVISION)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query LD Group Ke 9 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query LD Group Ke 10 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_division->division->LOK_RECID,
                                'merchand_name' => $formulaItem_division->division->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                        $formulaItem = $formula_ress->category;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_category) {
                            $this->writeLog('Query LD Group Ke 11 ',FALSE);
                            $merchant = Merchandise::where('MERC_CATEGORY', $formulaItem_category->CAT_CATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query LD Group Ke 12 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_category->category->LOK_RECID,
                                'merchand_name' => $formulaItem_category->category->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                        $formulaItem = $formula_ress->subcategory;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_subcategory) {
                            $this->writeLog('Query LD Group Ke 13 ',FALSE);
                            $merchant = Merchandise::where('MERC_SUBCATEGORY', $formulaItem_subcategory->SUBCAT_SUBCATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query LD Group Ke 14 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query LD Group Ke 15 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_subcategory->subcategory->LOK_RECID,
                                'merchand_name' => $formulaItem_subcategory->subcategory->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                        $formulaItem = $formula_ress->segment;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_segment) {
                            $this->writeLog('Query LD Group Ke 16 ',FALSE);
                            $merchant = Merchandise::where('MERC_SEGMENT', $formulaItem_segment->SEG_SEGMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }


                            $this->writeLog('Query LD Group Ke 17 ',FALSE);
                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query LD Group Ke 18 ',FALSE);
                            $rowGroup[] = array('merchand' => $formulaItem_segment->segment->LOK_RECID,
                                'merchand_name' => $formulaItem_segment->segment->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                        $formulaItem = $formula_ress->vendor;
                        // dd($formulaItem );
                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_vendor) {

                            $this->writeLog('Query LD Group Ke 19 ',FALSE);
                            $vendor = Vendor::where('VDR_RECID', $formulaItem_vendor->VND_VENDOR_RECID)->first(); //dd($vendor);
                            $formulaItem_product = $vendor->products;

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $this->writeLog('Query LD Group Ke 20 ',FALSE);
                            $rowGroup[] = array('vendor' => $formulaItem_vendor->vendor->VDR_RECID,
                                'vendor_name' => $formulaItem_vendor->vendor->VDR_NAME,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->where('FILE',$fileName)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    }



                    if (!isset($rowGroup['total'])) {
                        $rowGroup['total'] = 0;
                    }
                    if (!isset($rowGroup['qty'])) {
                        $rowGroup['qty'] = 0;
                    }


                    // ...
                    $TOTALAFTERDISC = array_sum(array_column($rowGroup, 'total'));
                    $ITEMQTY = array_sum(array_column($rowGroup, 'qty'));

                    $ITEMPOST = array_sum(array_column($rowGroup, 'post'));




                    if ($TOTALAFTERDISC > 0 and $ITEMQTY > 0) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount '.PHP_EOL : 'by quantity '.PHP_EOL;
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . ''.PHP_EOL . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = 0;
                                    $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;
                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
                                        $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                    }
                                }
                            }
                        }





                        if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                            // $row['ITEMQTY'] = $row['ITEMQTY'] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;

                                $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                            }

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . ''.PHP_EOL . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                            }
                        }
                    }


                    $point = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }



                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = array_sum($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;



                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }



}

$return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);
   $this->writeLog('LD Result : ',FALSE);
   $this->writeLog('Point LD CUST '.$row->CARDID.' : '.$last_poin,FALSE);

return $return;
    }

    public function _checkRewardPoint($row, $customer, $sumErn) {
        $point = 0;
        $fixed = 0;
        $pf = array();
        $this->writeLog('Query Reward Point ke 1 ',FALSE);
        $preference = Preference::select('value')->where('key', 'minimum_transaction_reward')->first();
        $minimum_transaction = $preference['value'];
        $max_best_point_fix = 0;
        $max_best_point_multiple = 0;

        if($row!=NULL and isset($row->RCPDATE)) {

            $this->writeLog('Query Reward Point ke 2 ',FALSE);
        $formula = FormulaEarn::where('ERN_FORMULA', 2)
                ->where('ERN_ACTIVE', 1)
                ->where('ERN_VALUE', 0)
                ->where('ERN_FRDATE', '<=', $row->RCPDATE)
                ->where('ERN_TODATE', '>=', $row->RCPDATE);  //ketika belanja masuk priode
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

        $formula_check = $formula->get();


        $formula_load = array();

        //check batas waktu (jam, menit, second) jika formula aktif ditanggal awal dan tanggal ahir
        foreach ($formula_check as $row_check) {

            if ($row->RCPDATE == $row_check->ERN_FRDATE or $row->RCPDATE == $row_check->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                $row_check_time = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME) // from datetime
                                ->where('ERN_TOTIME', '>=', $row->RCPTIME)->first();
                if (count($row_check_time) > 0) {
                    $formula_load[] = $row_check;
                }
            } else {
                $formula_load[] = $row_check;
            }
        }

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point

        if (!empty($formula_load)) {


            foreach ($formula_load as $formula_ress) {

                $point = 0;

                if (isset($formula_ress->ERN_FIXED)) {

                    $fixed = $formula_ress->ERN_FIXED;

                    if ($formula_ress->ERN_FORMULA_DTL == 1) {
                        //Member Get Member Reward
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 2) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);
                        //Birthday Reward
                        if ($cust_dob[1] == $rcpdate[1]) { // kondisi Reward on same Month
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 5) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);

                        $bd = $rcpdate[0] . '-' . $cust_dob[1] . '-' . $cust_dob[2];

                        $cust_trans = strtotime($row->RCPDATE); // tanggal transaksi
                        $cust_bd_weekly_start = strtotime($bd); // tanggal awal periode Birthday
                        $cust_bd_weekly_end = strtotime($bd . " +7 days"); // tanggal akhir periode Birthday

                        if ($cust_trans >= $cust_bd_weekly_start and $cust_trans <= $cust_bd_weekly_end) { // kondisi Reward on same Weekly
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 6) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);
                        //Birthday Reward

                        if ($cust_dob[1] == $rcpdate[1] and $cust_dob[2] == $rcpdate[2]) { // kondisi Reward on same Daily
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 3) {
                        //Butler Service Reward
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 4) {
                        // Reward New Registration dan Update Profile on Web Site
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 7) {
                        // Company Anniversary
                        $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 8) {
                        // Store Anniversary

                        $tenant = Tenant::where('TNT_CODE', $row->STOREID)->first();

                        $found = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                        ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                        ->where('TENT_ACTIVE', 1)->get();

                        if (count($found) > 0) {
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    }

                    // Min Max Amount
                    if ($formula_ress->ERN_ACTIVE_MAX == 1) {

                        if ($sumErn < $formula_ress->ERN_MIN_AMOUNT) { //jika Belanja Kurang Dari Minimum Amount
                            $point = 0;
                        }

                        if ($sumErn > $formula_ress->ERN_MAX_AMOUNT) { //jika Belanja Lebih Dari Minimum Amount
                            $point = 0;
                        }
                    }

                    if ($sumErn < $minimum_transaction) {  // jika dibawah minimum transaction
                        $point = 0;
                    }
                }

                if ($point < 0) {
                    $point = 0;
                }

                if ($formula_ress->ERN_FIXED == 1) {
                    $best_point_fix[] = $this->_filterTransaction($row->CARDID, $formula_ress->ERN_RECID, $row->STOREID, $point,$row->RCPDATE);
                } else {
                    $best_point_multiple[] = $this->_filterTransaction($row->CARDID, $formula_ress->ERN_RECID, $row->STOREID, $point,$row->RCPDATE);
                }

                $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                $pf[] = $pf_founded;
            }
        }



        if (count($best_point_fix) != 0) {
            $max_best_point_fix = max($best_point_fix);
        } else {
            $max_best_point_fix = 0;
        }

        if (count($best_point_multiple) != 0) {
            $max_best_point_multiple = max($best_point_multiple);
        } else {
            $max_best_point_multiple = 0;
        }

        }

        $ress = array('point_fix' => $max_best_point_fix, 'point_multiple' => $max_best_point_multiple, 'fixed' => $fixed, 'pf' => $pf);

        return $ress;
    }

    public function _availableDays($date, $id) {
        if ($id !== null) {

            $formulaEarn = FormulaEarn::find($id); // Find Formula ID

            $day = Carbon::createFromFormat('Y-m-d H:i:s', $date); // Parse to Carbon object
            //$day = Carbon::now(); // Parse to Carbon object


            //print_r($day->isWednesday()); exit;

            if ($formulaEarn->ERN_SUNDAY == 1 && $day->isSunday()) {
                echo PHP_EOL.'Its Sunday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_MONDAY == 1 && $day->isMonday()) {
                echo PHP_EOL.'Its Monday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_TUESDAY == 1 && $day->isTuesday()) {
                echo PHP_EOL.'Its Tuesday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_WEDNESDAY == 1 && $day->isWednesday()) {
                echo PHP_EOL.'Its Wednesday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_THURSDAY == 1 && $day->isThursday()) {
                echo PHP_EOL.'Its Thursday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_FRIDAY == 1 && $day->isFriday()) {
                echo PHP_EOL.'Its Friday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_SATURDAY == 1 && $day->isSaturday()) {
                echo PHP_EOL.'Its Saturday'.PHP_EOL;
                return true;
            } else {
                echo PHP_EOL.'Its Not Avaible Day'.PHP_EOL;
                return false;
            }
        }
        return false;
    }

    private function _checkCustomer($code) {
        $this->writeLog('Query Check Customer ',FALSE);
        $found = Customer::where('CUST_BARCODE', $code)->where('CUST_ACTIVE', 1)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function _checkStore($code) {
        $this->writeLog('Query Check Tenant ',FALSE);
        $found = Tenant::where('TNT_CODE', $code)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function _checkProduct($code) {
        $this->writeLog('Query Check Produk ',FALSE);
        $found = Product::where('PRO_CODE', $code)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function _checkPaymentMethod($code) {
        $this->writeLog('Query Check Payment Method ',FALSE);
        $found = PaymentMethod::where('code', $code)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function createUncleanCSVSales($sales, $fileName) {
        $handle = fopen('resources/uploads/earning/unclean/sales/' . $fileName . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($sales as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function createUncleanCSVPayment($payment, $fileName_payment) {
        $handle = fopen('resources/uploads/earning/unclean/payment/' . $fileName_payment . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($payment as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function createCleanCSVSales($sales, $fileName) {
        $handle = fopen('resources/uploads/earning/clean/sales/' . $fileName . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($sales as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function createCleanCSVPayment($payment, $fileName_payment) {
        $handle = fopen('resources/uploads/earning/clean/payment/' . $fileName_payment . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($payment as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function _logsPF($post_id, $pf, $type) {
        $this->writeLog('Query Logs PF ',FALSE);
        foreach ($pf as $row) {
            DB::beginTransaction();
            try {
                $PostPF = new PostPF();
                $PostPF->POSTF_RECID = Uuid::uuid();
                $PostPF->POSTF_POST_RECID = $post_id;
                $PostPF->POSTF_ERN_RECID = $row['id'];
                $PostPF->POSTF_POINT = $row['point'];
                $PostPF->POSTF_TYPE = $type;
                $PostPF->save();
                DB::commit();
                $this->writeLog("-- Logs PF Saved --", env('DEBUG_EARNING_LOG', false));
            } catch (\Exception $e) {
                DB::rollBack();
                $this->writeErrorLog("Error Save PostPF with Message : {$e->getMessage()}");
            }
        }
    }

    private function _logsPFBonusProses($post_id, $pf, $type, $point) {

        foreach ($pf as $row) {
            DB::beginTransaction;

            try {
                $PostPF = new PostPF();
                $PostPF->POSTF_RECID = Uuid::uuid();
                $PostPF->POSTF_POST_RECID = $post_id;
                $PostPF->POSTF_ERN_RECID = $row['id'];
                $PostPF->POSTF_POINT = $row['point'];
                $PostPF->POSTF_TYPE = $type;
                $PostPF->save();
                DB::commit();
                $this->writeLog("-- Logs PF Bonus Saved --", env('DEBUG_EARNING_LOG', false));
            } catch (\Exception $e) {
                DB::rollBack();
                $this->writeErrorLog("Error Save PostPF Bonus with Message : {$e->getMessage()}");
            }
        }
    }

    private function _logsPFBonus($postBonus, $postPayment, $post_id) {
        $this->writeLog('Query Logs PF Bonus ',FALSE);

        $PostPF = PostPF::whereIn('POSTF_POST_RECID', $postBonus);
        $PostPF->update(['POSTF_POST_RECID' => $post_id]);

        $PostPFPay = PostPF::whereIn('POSTF_POST_RECID', $postPayment);
        $PostPFPay->update(['POSTF_POST_RECID' => $post_id]);
    }

    public function _filterTransaction($CARDID, $ERN_RECID, $STOREID, $point,$date) {
        $this->writeLog('Filter Transaction ',FALSE);
        $customer = Customer::where('CUST_BARCODE', $CARDID)->where('CUST_ACTIVE',1)->first();

        if(isset($customer->CUST_RECID)) {

        if (isset($customer->CUST_GENDER)) {
            switch ($customer->CUST_GENDER) {
                case 'M':
                    $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                    break;
                case 'F':
                    $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                    break;
            } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
        }

        if ($customer->CUST_GENDER == null) {
            $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'GEN')->where('LOK_DESCRIPTION', 'OTHERS')->first();
            $customer->CUST_GENDER = $lookUp->LOK_RECID; //other
        }
        if ($customer->CUST_BLOOD == null) {
            $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'BLOD')->where('LOK_DESCRIPTION', 'OTHERS')->first();
            $customer->CUST_BLOOD = $lookUp->LOK_RECID; //other
        }
        if ($customer->CUST_RELIGION == null) {
            $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'RELI')->where('LOK_DESCRIPTION', 'OTHERS')->first();
            $customer->CUST_RELIGION = $lookUp->LOK_RECID; //other
        }

        $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
        $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
        $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();

        $tenant = Tenant::where('TNT_CODE', $STOREID)->first();
        $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                        ->where('TENT_ERN_RECID', $ERN_RECID)
                        ->where('TENT_ACTIVE', 1)->get();

        if (count($formulaTenant) < 1) {
            $point = 0; //memtype tidak ditemukan
            echo 'store tidak mendapatkan promo '.PHP_EOL;
        }

        if (count($formulaGender) < 1) {
            $point = 0;
            echo 'gender tidak mendapatkan promo'.PHP_EOL;
        }
        if (count($formulaReligion) < 1) {
            $point = 0;
            echo 'religion tidak mendapatkan promo '.PHP_EOL;
        }
        if (count($formulaBlood) < 1) {
            $point = 0;
              echo 'blood tidak mendapatkan promo '.PHP_EOL;
        }

        if($this->_availableDays($date,$ERN_RECID)==false) {
            $point = 0;
              echo 'day not avaible '.PHP_EOL;
        }

         }
        else {
            $point = 0;
              echo 'Customer Tidak Aktif '.PHP_EOL;
        }

        return $point;
    }

    public function prosesTempPayment($data_payment, $fileName_payment) {
        $this->writeLog('Proses temp payment ',FALSE);
         ini_set('memory_limit', '8069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        ini_set('SQL_SERVER_MAX_PARAMS', 99999);

        $data_clean = [];
        $data_unclean = [];

        $proses_payment_bonus = FALSE;

         foreach ($data_payment as $key => $row_payment) {
             echo "CEK DATE FOR PF BONUS PAYMENT".PHP_EOL;
             $date_trans = $row_payment[2 - env('LOCALARRAY', 0)];
             break;
         }


        $cek = FormulaEarn::where('ERN_FORMULA', 1)
                    ->where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $date_trans)
                    ->where('ERN_TODATE', '>=', $date_trans)
                    ->where('ERN_FORMULA_TYPE', 0)->first();

        if(count($cek) >= 1) {
             $proses_payment_bonus = TRUE;
        }


        foreach ($data_payment as $key => $row_payment) {
            $no = $key + 1;
            // Convert all format datetime to timestamp
            $rcpdate = Carbon::createFromTimestamp(strtotime($row_payment[2 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcpdate
            $row_payment[2 - env('LOCALARRAY', 0)] = date('Y-m-d', strtotime($rcpdate->toDateString()));
            // Convert all format time to timestamp
            $rcptime = Carbon::createFromTimestamp(strtotime($row_payment[3 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcptime
            $row_payment[3 - env('LOCALARRAY', 0)] = date('H:i:s', strtotime($rcptime->toTimeString()));


//
//            $checkPayment = $this->_checkPaymentMethod($row_payment[7 - env('LOCALARRAY', 0)]);
//            $checkCustomer = $this->_checkCustomer($row_payment[6 - env('LOCALARRAY', 0)]);
//            $checkStore = $this->_checkStore($row_payment[1 - env('LOCALARRAY', 0)]);

            $calcBonus = 0;
            $point_bonus = 0;
            $multiple = 0;

            if($proses_payment_bonus) {
                $calcBonus = $this->_checkBonusPayment($row_payment);
                $point_bonus = $calcBonus['point'];
                $multiple = $calcBonus['multiple'];
            }

            $checkPayment = TRUE;
            $checkCustomer = TRUE;
            $checkStore = TRUE;



            if ($checkPayment == TRUE && $checkCustomer == TRUE && $checkStore == TRUE) {
                try {
                    $postTempPayment = new PostTempPayment();
                    $postTempPayment->POSTTEMPPAYMENT_RECID = Uuid::uuid();
                    $postTempPayment->STOREID = $row_payment[1 - env('LOCALARRAY', 0)]; // on windows CSV array start from 1
                    $postTempPayment->RCPDATE = $row_payment[2 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPTIME = $row_payment[3 - env('LOCALARRAY', 0)];
                    $postTempPayment->TERMID = $row_payment[4 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPNBR = $row_payment[5 - env('LOCALARRAY', 0)];
                    $postTempPayment->CARDID = $row_payment[6 - env('LOCALARRAY', 0)];
                    $postTempPayment->TENDNBR = $row_payment[7 - env('LOCALARRAY', 0)];
                    $postTempPayment->ITEMTOTAL = $row_payment[8 - env('LOCALARRAY', 0)];
                    $postTempPayment->BIN = (!empty($row_payment[9 - env('LOCALARRAY', 0)])) ? $row_payment[9 - env('LOCALARRAY', 0)] : '0';
                    $postTempPayment->POINT_BONUS = $point_bonus;
                    $postTempPayment->MULTIPLE = $multiple;
                    $postTempPayment->FILE = $fileName_payment;
                    //insert file name
                    //$postTempPayment->save();
                     $data_clean[] = $postTempPayment->attributesToArray();

                    $row_payment[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row_payment[2 - env('LOCALARRAY', 0)]);
                    $row_payment[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row_payment[3 - env('LOCALARRAY', 0)]);

                    $this->writeLog("$fileName_payment :: Log Line $no : OK", env('DEBUG_EARNING_LOG', false));
                    //$this->createCleanCSVPayment($row_payment, $fileName_payment);
                } catch (\Exception $e) {
                    $this->writeLog("$fileName_payment :: Log Line $no : Error With Message : {$e->getMessage()}");
                    $row_payment[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row_payment[2 - env('LOCALARRAY', 0)]);
                    $row_payment[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row_payment[3 - env('LOCALARRAY', 0)]);
                    $row_payment[10 - env('LOCALARRAY', 0)] = ' ';
                    $row_payment[11 - env('LOCALARRAY', 0)] = " *** Undefined Error With Message : {$e->getMessage()}";
                    //$this->createUncleanCSVPayment($row_payment, $fileName_payment);
                    //Insert Unclean Postemp Payment

                    $postTempPayment = new PostTempPayment_unclean();
                    $postTempPayment->POSTTEMPPAYMENT_RECID = Uuid::uuid();
                    $postTempPayment->STOREID = $row_payment[1 - env('LOCALARRAY', 0)]; // on windows CSV array start from 1
                    $postTempPayment->RCPDATE = $row_payment[2 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPTIME = $row_payment[3 - env('LOCALARRAY', 0)];
                    $postTempPayment->TERMID = $row_payment[4 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPNBR = $row_payment[5 - env('LOCALARRAY', 0)];
                    $postTempPayment->CARDID = $row_payment[6 - env('LOCALARRAY', 0)];
                    $postTempPayment->TENDNBR = $row_payment[7 - env('LOCALARRAY', 0)];
                    $postTempPayment->ITEMTOTAL = $row_payment[8 - env('LOCALARRAY', 0)];
                    $postTempPayment->BIN = (!empty($row_payment[9 - env('LOCALARRAY', 0)])) ? $row_payment[9 - env('LOCALARRAY', 0)] : '0';
                    $postTempPayment->POINT_BONUS = 0;
                    $postTempPayment->MULTIPLE = 0;
                    $postTempPayment->DESC = $row_payment[11 - env('LOCALARRAY', 0)];
                    $postTempPayment->FILE = $fileName_payment;
                    //insert file name
                    //$postTempPayment->save();
                    $data_unclean[] = $postTempPayment->attributesToArray();
                }
            } else {
                $csvLog = "Log Line $no : ";
                if (!$checkPayment) {
                    $msg = " Payment Method " . $row_payment[7 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkCustomer) {
                    $msg = " Customer " . $row_payment[6 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkStore) {
                    $msg = " Store " . $row_payment[1 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }

                $this->writeLog($fileName_payment . " " . $csvLog, env('DEBUG_EARNING_LOG', false));

                $row_payment[10 - env('LOCALARRAY', 0)] = ' ';
                $row_payment[11 - env('LOCALARRAY', 0)] = ' *** ' . $csvLog;
                //$this->createUncleanCSVPayment($row_payment, $fileName_payment);
                //Insert Unclean Postemp Payment
                $postTempPayment = new PostTempPayment_unclean();
                $postTempPayment->POSTTEMPPAYMENT_RECID = Uuid::uuid();
                $postTempPayment->STOREID = $row_payment[1 - env('LOCALARRAY', 0)]; // on windows CSV array start from 1
                $postTempPayment->RCPDATE = $row_payment[2 - env('LOCALARRAY', 0)];
                $postTempPayment->RCPTIME = $row_payment[3 - env('LOCALARRAY', 0)];
                $postTempPayment->TERMID = $row_payment[4 - env('LOCALARRAY', 0)];
                $postTempPayment->RCPNBR = $row_payment[5 - env('LOCALARRAY', 0)];
                $postTempPayment->CARDID = $row_payment[6 - env('LOCALARRAY', 0)];
                $postTempPayment->TENDNBR = $row_payment[7 - env('LOCALARRAY', 0)];
                $postTempPayment->ITEMTOTAL = $row_payment[8 - env('LOCALARRAY', 0)];
                $postTempPayment->BIN = (!empty($row_payment[9 - env('LOCALARRAY', 0)])) ? $row_payment[9 - env('LOCALARRAY', 0)] : '0';
                $postTempPayment->POINT_BONUS = 0;
                $postTempPayment->MULTIPLE = 0;
                $postTempPayment->DESC = $row_payment[11 - env('LOCALARRAY', 0)];
                $postTempPayment->FILE = $fileName_payment;
                //insert file name
                //$postTempPayment->save();
                $data_unclean[] = $postTempPayment->attributesToArray();
            }
        }

        $count_clean = count($data_clean);
        $count_unclean = count($data_unclean);


        $this->writeLog("$fileName_payment :: Save Clean Data {$count_clean} record ", env('DEBUG_EARNING_LOG', false));

        $collection = collect($data_clean);
        $chunks = $collection->chunk(104);

        foreach($chunks->toArray() as $row) {
            PostTempPayment::insert($row);
        }


        $this->writeLog("$fileName_payment :: Save Unclean Data {$count_unclean} record", env('DEBUG_EARNING_LOG', false));

        $collection = collect($data_unclean);
        $chunks = $collection->chunk(104);

        foreach($chunks->toArray() as $row) {
            PostTempPayment_unclean::insert($row);
        }


         DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; insert into POSTTEMPPAYMENT_unclean (POSTTEMPPAYMENT_RECID,STOREID,RCPDATE,RCPTIME
,TERMID,RCPNBR,CARDID,TENDNBR,ITEMTOTAL,BIN,POINT_BONUS,MULTIPLE,[FILE],[DESC])
 (select a.*, concat('Customer ', a.CARDID, ' Not Found') as 'DESC' from POSTTEMPPAYMENT a  left join Customers b ON a.CARDID = b.CUST_BARCODE
where b.CUST_BARCODE is null OR b.CUST_BARCODE = ''); select TOP 1 POSTTEMPPAYMENT_RECID from POSTTEMPPAYMENT_unclean;");

       DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; delete from POSTTEMPPAYMENT where POSTTEMPPAYMENT_RECID in (select POSTTEMPPAYMENT_RECID from POSTTEMPPAYMENT a  left join Customers b ON a.CARDID = b.CUST_BARCODE
where b.CUST_BARCODE is null OR b.CUST_BARCODE = ''); select TOP 1 POSTTEMPPAYMENT_RECID from POSTTEMPPAYMENT_unclean;");

                DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; insert into POSTTEMPPAYMENT_unclean (POSTTEMPPAYMENT_RECID,STOREID,RCPDATE,RCPTIME
,TERMID,RCPNBR,CARDID,TENDNBR,ITEMTOTAL,BIN,POINT_BONUS,MULTIPLE,[FILE],[DESC])
 (select a.*, concat('Product ', a.STOREID, ' Not Found') as 'DESC' from POSTTEMPPAYMENT a  left join Z_Tenant b ON a.STOREID = b.TNT_CODE
where b.TNT_CODE is null OR b.TNT_CODE = ''); select TOP 1 POSTTEMPPAYMENT_RECID from POSTTEMPPAYMENT_unclean;");

       DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; delete from POSTTEMPPAYMENT where POSTTEMPPAYMENT_RECID in (select POSTTEMPPAYMENT_RECID from POSTTEMPPAYMENT a  left join Z_Tenant b ON a.STOREID = b.TNT_CODE
where b.TNT_CODE is null OR b.TNT_CODE = ''); select TOP 1 POSTTEMPPAYMENT_RECID from POSTTEMPPAYMENT_unclean;");


    }

    public function prosesTempSales($data, $fileName) {
        $this->writeLog('Proses temp sales ',FALSE);
        ini_set('memory_limit', '8069M');
        ini_set('client_buffer_max_kb_size', 998069);
        ini_set('sqlsrv.ClientBufferMaxKBSize', 998069);
        ini_set('max_execution_time', 0);
        ini_set('SQL_SERVER_MAX_PARAMS', 99999);

        $data_clean = [];
        $data_unclean = [];

        foreach ($data as $key => $row) {
            $no = $key + 1;
            // Convert all format datetime to timestamp
            $rcpdate = Carbon::createFromTimestamp(strtotime($row[2 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcpdate
            $row[2 - env('LOCALARRAY', 0)] = date('Y-m-d', strtotime($rcpdate->toDateString()));

            // Convert all format time to timestamp
            $rcptime = Carbon::createFromTimestamp(strtotime($row[3 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcptime
            $row[3 - env('LOCALARRAY', 0)] = date('H:i:s', strtotime($rcptime->toTimeString()));

            try {
//               $calcBonus = $this->_checkBonusPoint($row);
//			   //back to single grouping for product bonus point formula
//
//                $point_bonus = $calcBonus['point'];
//                $split_bonus = $calcBonus['split'];
//                $multiple = $calcBonus['multiple'];
                $point_bonus = 0;
                $split_bonus = 0;
                $multiple = 0;
            } catch (\Exception $e) {
                $point_bonus = 0;
                $split_bonus = 0;
                $multiple = 0;
                $this->writeLog(" Check Bonus Point Failed, with message : {$e->getMessage()}", env('DEBUG_EARNING_LOG', false));
            }

//            $checkProduct = $this->_checkProduct($row[7 - env('LOCALARRAY', 0)]);
//            $checkCustomer = $this->_checkCustomer($row[5 - env('LOCALARRAY', 0)]);
//            $checkStore = $this->_checkStore($row[1 - env('LOCALARRAY', 0)]);
            // proses check unclean di ubah dengan cara query select join ketika seluruh data selesai CSV di insert

            $checkProduct = TRUE;
            $checkCustomer = TRUE;
            $checkStore = TRUE;

            if ($checkProduct == TRUE && $checkCustomer == TRUE && $checkStore == TRUE) {
                try {
                    $postTemp = new PostTemp();
                    $postTemp->POSTTEMP_RECID = Uuid::uuid();
                    $postTemp->STOREID = $row[1 - env('LOCALARRAY', 0)];
                    $postTemp->RCPDATE = $row[2 - env('LOCALARRAY', 0)];
                    $postTemp->RCPTIME = $row[3 - env('LOCALARRAY', 0)];
                    $postTemp->TERMID = $row[4 - env('LOCALARRAY', 0)];
                    $postTemp->CARDID = $row[5 - env('LOCALARRAY', 0)];
                    $postTemp->RCPNBR = $row[6 - env('LOCALARRAY', 0)];
                    $postTemp->INTCODE = $row[7 - env('LOCALARRAY', 0)];
                    $postTemp->ITEMTOTAL = filter_var(str_replace('-','',$row[8 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->ITEMTOTSIGN = $row[9 - env('LOCALARRAY', 0)];
                    $postTemp->ITEMQTY = filter_var(str_replace('-','',$row[10 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->DEPTCODE = $row[11 - env('LOCALARRAY', 0)];
                    $postTemp->VATCODE = $row[12 - env('LOCALARRAY', 0)];
                    $postTemp->DISCOUNT = filter_var(str_replace('-','',$row[13 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->TOTALAFTERDISC = filter_var(str_replace('-','',$row[14 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->ITMIDX = (!empty($row[15 - env('LOCALARRAY', 0)])) ? $row[15 - env('LOCALARRAY', 0)] : 0;
                    $postTemp->POINT_BONUS = $point_bonus;
                    $postTemp->SPLIT_BONUS = $split_bonus;
                    $postTemp->MULTIPLE = $multiple;
                    $postTemp->FILE = $fileName;
                    //insert file name
                    //$postTemp->save();
                    $data_clean[] = $postTemp->attributesToArray();

                    $row[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row[2 - env('LOCALARRAY', 0)]);
                    $row[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row[3 - env('LOCALARRAY', 0)]);

                    $this->writeLog("$fileName :: Log Line $no : OK", env('DEBUG_EARNING_LOG', false));
                    //$this->createCleanCSVSales($row, $fileName);
                } catch (\Exception $e) {
                    $this->writeLog("$fileName :: Log Line $no : Error With Message : {$e->getMessage()}");
                    $row[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row[2 - env('LOCALARRAY', 0)]);
                    $row[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row[3 - env('LOCALARRAY', 0)]);
                    $row[16 - env('LOCALARRAY', 0)] = ' ';
                    $row[17 - env('LOCALARRAY', 0)] = " *** Undefined Error With Message : {$e->getMessage()}";
                    //$this->createUncleanCSVSales($row, $fileName);
                    //Insert Unclean Postemp
                    $postTemp = new PostTemp_unclean();
                    $postTemp->POSTTEMP_RECID = Uuid::uuid();
                    $postTemp->STOREID = $row[1 - env('LOCALARRAY', 0)];
                    $postTemp->RCPDATE = $row[2 - env('LOCALARRAY', 0)];
                    $postTemp->RCPTIME = $row[3 - env('LOCALARRAY', 0)];
                    $postTemp->TERMID = $row[4 - env('LOCALARRAY', 0)];
                    $postTemp->CARDID = $row[5 - env('LOCALARRAY', 0)];
                    $postTemp->RCPNBR = $row[6 - env('LOCALARRAY', 0)];
                    $postTemp->INTCODE = $row[7 - env('LOCALARRAY', 0)];
                    $postTemp->ITEMTOTAL = filter_var(str_replace('-','',$row[8 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->ITEMTOTSIGN = $row[9 - env('LOCALARRAY', 0)];
                    $postTemp->ITEMQTY = filter_var(str_replace('-','',$row[10 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->DEPTCODE = $row[11 - env('LOCALARRAY', 0)];
                    $postTemp->VATCODE = $row[12 - env('LOCALARRAY', 0)];
                    $postTemp->DISCOUNT = filter_var(str_replace('-','',$row[13 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->TOTALAFTERDISC = filter_var(str_replace('-','',$row[14 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                    $postTemp->ITMIDX = (!empty($row[15 - env('LOCALARRAY', 0)])) ? $row[15 - env('LOCALARRAY', 0)] : 0;
                    $postTemp->POINT_BONUS = 0;
                    $postTemp->SPLIT_BONUS = 0;
                    $postTemp->MULTIPLE = 0;
                    $postTemp->DESC = $row[17 - env('LOCALARRAY', 0)];
                    $postTemp->FILE = $fileName;
                    //insert file name
                    //$postTemp->save();
                    $data_unclean[] = $postTemp->attributesToArray();
                }
            } else {
                $csvLog = "Log Line $no : ";
                if (!$checkProduct) {
                    $msg = " | Product " . $row[7 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkCustomer) {
                    $msg = " | Customer " . $row[5 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkStore) {
                    $msg = " | Store " . $row[1 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }

                $this->writeLog($fileName . " :: " . $csvLog, env('DEBUG_EARNING_LOG', false));

                $row[16 - env('LOCALARRAY', 0)] = ' ';
                $row[17 - env('LOCALARRAY', 0)] = ' *** ' . $csvLog;
                //$this->createUncleanCSVSales($row, $fileName);
                //Insert Unclean Postemp

                $postTemp = new PostTemp_unclean();
                $postTemp->POSTTEMP_RECID = Uuid::uuid();
                $postTemp->STOREID = $row[1 - env('LOCALARRAY', 0)];
                $postTemp->RCPDATE = $row[2 - env('LOCALARRAY', 0)];
                $postTemp->RCPTIME = $row[3 - env('LOCALARRAY', 0)];
                $postTemp->TERMID = $row[4 - env('LOCALARRAY', 0)];
                $postTemp->CARDID = $row[5 - env('LOCALARRAY', 0)];
                $postTemp->RCPNBR = $row[6 - env('LOCALARRAY', 0)];
                $postTemp->INTCODE = $row[7 - env('LOCALARRAY', 0)];
                $postTemp->ITEMTOTAL = filter_var(str_replace('-','',$row[8 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                $postTemp->ITEMTOTSIGN = $row[9 - env('LOCALARRAY', 0)];
                $postTemp->ITEMQTY = filter_var(str_replace('-','',$row[10 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                $postTemp->DEPTCODE = $row[11 - env('LOCALARRAY', 0)];
                $postTemp->VATCODE = $row[12 - env('LOCALARRAY', 0)];
                $postTemp->DISCOUNT = filter_var(str_replace('-','',$row[13 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                $postTemp->TOTALAFTERDISC = filter_var(str_replace('-','',$row[14 - env('LOCALARRAY', 0)]),FILTER_SANITIZE_NUMBER_INT);
                $postTemp->ITMIDX = (!empty($row[15 - env('LOCALARRAY', 0)])) ? $row[15 - env('LOCALARRAY', 0)] : 0;
                $postTemp->POINT_BONUS = 0;
                $postTemp->SPLIT_BONUS = 0;
                $postTemp->MULTIPLE = 0;
                $postTemp->DESC = $row[17 - env('LOCALARRAY', 0)];
                $postTemp->FILE = $fileName;
                //insert file name
                //$postTemp->save();
                $data_unclean[] = $postTemp->attributesToArray();
            }
            $no++;
        }

        $collection = collect($data_clean);
        $chunks = $collection->chunk(104);

        foreach($chunks->toArray() as $row) {
         $count_clean = count($row);
         $this->writeLog("$fileName :: Save Partial Clean Data {$count_clean} record ", env('DEBUG_EARNING_LOG', false));
          PostTemp::insert($row);
        }


          $this->writeLog("$fileName :: Save Unclean Data  ", env('DEBUG_EARNING_LOG', false));

       DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; insert into POSTTEMP_unclean (POSTTEMP_RECID,STOREID,RCPDATE,RCPNBR,RCPTIME,
TERMID,CARDID,INTCODE,ITEMTOTAL,ITEMTOTSIGN,ITEMQTY,DEPTCODE,VATCODE,DISCOUNT,TOTALAFTERDISC,ITMIDX,VOID_ITEM,POINT_BONUS,BIN,SPLIT_BONUS,MULTIPLE,[FILE],[DESC])
 (select a.*, concat('Customer ', a.CARDID, ' Not Found') as 'DESC' from POSTTEMP a  left join Customers b ON a.CARDID = b.CUST_BARCODE
where b.CUST_BARCODE is null OR b.CUST_BARCODE = ''); select TOP 1 POSTTEMP_RECID from POSTTEMP_unclean;");

       DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; delete from POSTTEMP where POSTTEMP_RECID in (select POSTTEMP_RECID from POSTTEMP a  left join Customers b ON a.CARDID = b.CUST_BARCODE
where b.CUST_BARCODE is null OR b.CUST_BARCODE = ''); select TOP 1 POSTTEMP_RECID from POSTTEMP_unclean;");

       DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; insert into POSTTEMP_unclean (POSTTEMP_RECID,STOREID,RCPDATE,RCPNBR,RCPTIME,
TERMID,CARDID,INTCODE,ITEMTOTAL,ITEMTOTSIGN,ITEMQTY,DEPTCODE,VATCODE,DISCOUNT,TOTALAFTERDISC,ITMIDX,VOID_ITEM,POINT_BONUS,BIN,SPLIT_BONUS,MULTIPLE,[FILE],[DESC])
 (select a.*, concat('Product ', a.INTCODE, ' Not Found') as 'DESC' from POSTTEMP a  left join Z_Product_Item b ON a.INTCODE = b.PRO_CODE
where b.PRO_CODE is null OR b.PRO_CODE = ''); select TOP 1 POSTTEMP_RECID from POSTTEMP_unclean;");

       DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; delete from POSTTEMP where POSTTEMP_RECID in (select a.POSTTEMP_RECID from POSTTEMP a  left join Z_Product_Item b ON a.INTCODE = b.PRO_CODE
where b.PRO_CODE is null OR b.PRO_CODE = ''); select TOP 1 POSTTEMP_RECID from POSTTEMP_unclean;");

        DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; insert into POSTTEMP_unclean (POSTTEMP_RECID,STOREID,RCPDATE,RCPNBR,RCPTIME,
TERMID,CARDID,INTCODE,ITEMTOTAL,ITEMTOTSIGN,ITEMQTY,DEPTCODE,VATCODE,DISCOUNT,TOTALAFTERDISC,ITMIDX,VOID_ITEM,POINT_BONUS,BIN,SPLIT_BONUS,MULTIPLE,[FILE],[DESC])
 (select a.*, concat('Store ', a.STOREID, ' Not Found') as 'DESC' from POSTTEMP a  left join Z_Tenant b ON a.STOREID = b.TNT_CODE
where b.TNT_CODE is null OR b.TNT_CODE = ''); select TOP 1 POSTTEMP_RECID from POSTTEMP_unclean;");

       DB::select("SET ANSI_NULLS ON; SET ANSI_WARNINGS ON; SET NOCOUNT ON; delete from POSTTEMP where POSTTEMP_RECID in (select a.POSTTEMP_RECID from POSTTEMP a  left join Z_Tenant b ON a.STOREID = b.TNT_CODE
where b.TNT_CODE is null OR b.TNT_CODE = ''); select TOP 1 POSTTEMP_RECID from POSTTEMP_unclean;");

    }

    public function writeLog($message, $writeToLog = true) {
        echo '[' . date('d-m-Y H:i:s') . '] [Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message . "".PHP_EOL;
        if ($writeToLog)
            Log::info('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);

        return true;
    }

    public function writeErrorLog($message, $writeToLog = true) {
        echo '[' . date('d-m-Y H:i:s') . '] : ' . $message . "".PHP_EOL;
        if ($writeToLog)
            Log::error('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);
        session()->push('logsEarning', $message);
        return true;
    }

    public function notification() {
        try {
            $foundError = session('logsEarning');
            $data = ['error' => $foundError, 'count' => count($foundError)];
            if ($data['count'] > 0) {
                $this->writeLog("\n Sending Error Email Notification");

                Mail::send('mail-confirm-earning', $data, function ($mail) {
                    $mail->to(env('EMAIL_ERROR_TO', 'info@loyalto.id'), 'Administrator MyLoyalty');
                    $mail->subject('Earning Proccess Error Notification');
                });
            }
        } catch (\Exception $e) {
            $this->writeErrorLog('Send Mail Fail with error  : ' . $e->getMessage());
        }
    }

}

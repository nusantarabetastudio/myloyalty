<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\FormulaEarnBlood;
use App\Models\Admin\FormulaEarnCardType;
use App\Models\Admin\FormulaEarnMember;
use App\Models\Admin\FormulaEarnPayment;
use App\Models\Admin\FormulaEarnReligion;
use App\Models\Admin\FormulaEarnTenant;
use App\Models\Master\CardType;
use App\Models\Master\PaymentMethod;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\FormulaEarn;
use App\Models\Master\Lookup;
use Faker\Provider\Uuid;
use App\Models\Admin\FormulaEarnGender;
use Datatables, Session;

class RegularPointController extends Controller {

    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct(Session $session) {
        $this->parent = 'Point Formula';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        $this->menu_id = '';
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index() {
        $data['breadcrumbs'] = 'Regular Point';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        
        return view('pointformula.regularpoint.index', $data);
    }

    public function create() {
        $data['breadcrumbs'] = 'Regular Point Create';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        
        $data['payments'] = PaymentMethod::all();
        $data['member_type'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['gender'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['religion'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['card_type'] = CardType::all();
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['cards'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['blood_type'] = Lookup::where('LOK_CODE', 'BLOD')->get();

        return view('pointformula.regularpoint.create', $data);
    }

    public function store(Request $request) {
        $rules = [
            'ERN_DESC' => 'required',
            'ERN_AMOUNT_REST' => 'numeric',
            'ERN_AMOUNT' => 'numeric',
            'ERN_POINT' => 'numeric',
            'ERN_POINT_REST' => 'numeric',
            'ERN_MAX_AMOUNT' => 'numeric',
            'ERN_MIN_AMOUNT' => 'numeric'
        ];
        $messages = [
            'ERN_DESC.required' => 'Description field is required',
            'ERN_AMOUNT.numeric' => 'Amount field must be numeric',
            'ERN_AMOUNT_REST.numeric' => 'Rest Amount field must be numeric',
            'ERN_MAX_AMOUNT.numeric' => 'Maximum Amount field must be numeric',
            'ERN_MIN_AMOUNT.numeric' => 'Minimum Amount field must be numeric',
            'ERN_POINT.numeric' => 'Point field must be numeric',
            'ERN_POINT_REST.numeric' => 'Rest Point field must be numeric'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->only(['ERN_DESC', 'ERN_TYPE', 'ERN_AMOUNT', 'ERN_POINT']);
        $earnPoint = new FormulaEarn();
        $earnPoint->ERN_RECID = Uuid::uuid();
        $earnPoint->fill($post_data);
        $earnPoint->ERN_PNUM = $this->pnum;
        $earnPoint->ERN_PTYPE = $this->ptype;
        $earnPoint->ERN_VALUE = 0;
        $earnPoint->ERN_ACTIVE = 1;
        $earnPoint->ERN_MULTIPLE_BASIC = $request->has('ERN_MULTIPLE_BASIC') && $request->input('ERN_MULTIPLE_BASIC') == 'on' ? 1 : 0;
        $earnPoint->ERN_TYPE = 0;
        $earnPoint->ERN_FORMULA = 0;
        $earnPoint->ERN_FRDATE = dateFormat($request->input('fromDate'));
        $earnPoint->ERN_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $earnPoint->ERN_TODATE = dateFormat($request->input('toDate'));
        $earnPoint->ERN_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s
        $earnPoint->ERN_ACTIVE_MAXAMOUNT = $request->has('ERN_ACTIVE_MAXAMOUNT') ? 1 : 0;
        // MAXAMOUNT
        if($request->has('ERN_ACTIVE_MAXAMOUNT') && $request->input('ERN_ACTIVE_MAXAMOUNT') == 'on') {
            $earnPoint->ERN_MAXAMOUNT_VALUE = $request->input('ERN_MAXAMOUNT_VALUE');
        } else {
            $earnPoint->ERN_MAXAMOUNT_VALUE = 0;
        }
        // If Rest Amount has checked.
        if($request->has('ERN_ACTIVE_REST') && $request->input('ERN_ACTIVE_REST') == 'on') {
            $earnPoint->ERN_ACTIVE_REST = 1;
            $earnPoint->ERN_POINT_REST = $request->has('ERN_POINT_REST') ? $request->input('ERN_POINT_REST') : 0;
            $earnPoint->ERN_AMOUNT_REST = $request->has('ERN_AMOUNT_REST') ? $request->input('ERN_AMOUNT_REST') : 0;
            if($request->input('ERN_MULTIPLE_REST') == 'on') {
                $earnPoint->ERN_MULTIPLE_REST = 1;
            } else {
                $earnPoint->ERN_MULTIPLE_REST = 0;
            }
        } else {
            $earnPoint->ERN_ACTIVE_REST = 0;
            $earnPoint->ERN_POINT_REST = 0;
            $earnPoint->ERN_AMOUNT_REST = 0;
            $earnPoint->ERN_MULTIPLE_REST = 0;
        }
        // Max amount is checked
        if($request->input('ERN_ACTIVE_MAX') == 'on') {
            $earnPoint->ERN_ACTIVE_MAX = 1;
            $earnPoint->ERN_MAX_AMOUNT = $request->input('ERN_MAX_AMOUNT');
            $earnPoint->ERN_MIN_AMOUNT = $request->input('ERN_MIN_AMOUNT');
            if($request->input('ERN_MULTIPLE') == 'on') {
                $earnPoint->ERN_MULTIPLE = 1;
                $earnPoint->ERN_MULTIPLE_VALUE = $request->input('ERN_MULTIPLE_VALUE');
            } else {
                $earnPoint->ERN_MULTIPLE = 0;
                $earnPoint->ERN_MULTIPLE_VALUE = 0;
            }
        } else {
            $earnPoint->ERN_ACTIVE_MAX = 0;
            $earnPoint->ERN_MAX_AMOUNT = 0;
            $earnPoint->ERN_MIN_AMOUNT = 0;
            $earnPoint->ERN_MULTIPLE = 0;
            $earnPoint->ERN_MULTIPLE_VALUE = 0;

        }

        $earnPoint->ERN_REFERER = $request->has('ERN_REFERER') && ($request->input('ERN_REFERER') == 'on') ? 1 : 0;
        $earnPoint->ERN_PARENT = $request->has('ERN_PARENT') && ($request->input('ERN_PARENT') == 'on') ? 1 : 0;

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $earnPoint->ERN_SUNDAY = 1;
            } else {
                $earnPoint->ERN_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $earnPoint->ERN_MONDAY = 1;
            } else {
                $earnPoint->ERN_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $earnPoint->ERN_TUESDAY = 1;
            } else {
                $earnPoint->ERN_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $earnPoint->ERN_WEDNESDAY = 1;
            } else {
                $earnPoint->ERN_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $earnPoint->ERN_THURSDAY = 1;
            } else {
                $earnPoint->ERN_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $earnPoint->ERN_FRIDAY = 1;
            } else {
                $earnPoint->ERN_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $earnPoint->ERN_SATURDAY = 1;
            } else {
                $earnPoint->ERN_SATURDAY = 0;
            }
        }
        $earnPoint->save(); // Save Object Earn Point
        // PAYMENT
        $PAY_LOK_RECID = $request->input('PAY_LOK_RECID');
        if($request->has('PAY_LOK_RECID')) {
            foreach($PAY_LOK_RECID as $row) {
                $earnPayment = new FormulaEarnPayment();
                $earnPayment->PAY_RECID = Uuid::uuid();
                $earnPayment->PAY_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->PAY_LOK_RECID = $row;
                $earnPayment->PAY_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // MEMBER TYPE
        $MEM_LOK_RECID = $request->input('MEM_LOK_RECID');
        if($request->has('MEM_LOK_RECID')) {
            foreach($MEM_LOK_RECID as $row) {
                $earnMemberType = new FormulaEarnMember();
                $earnMemberType->MEM_RECID = Uuid::uuid();
                $earnMemberType->MEM_ERN_RECID = $earnPoint->ERN_RECID;
                $earnMemberType->MEM_LOK_RECID = $row;
                $earnMemberType->MEM_ACTIVE = 1;
                $earnMemberType->save();
            }
        }
        // GENDER
        $GEN_LOK_RECID = $request->input('GEN_LOK_RECID');
        if($request->has('GEN_LOK_RECID')) {
            foreach($GEN_LOK_RECID as $row) {
                $earnGender = new FormulaEarnGender();
                $earnGender->GEN_RECID = Uuid::uuid();
                $earnGender->GEN_ERN_RECID = $earnPoint->ERN_RECID;
                $earnGender->GEN_LOK_RECID = $row;
                $earnGender->GEN_ACTIVE = 1;
                $earnGender->save();
            }
        }
        // REGLIGION
        $RELI_LOK_RECID = $request->input('RELI_LOK_RECID');
        if($request->has('RELI_LOK_RECID')) {
            foreach($RELI_LOK_RECID as $row) {
                $earnReligion = new FormulaEarnReligion();
                $earnReligion->RELI_RECID = Uuid::uuid();
                $earnReligion->RELI_ERN_RECID = $earnPoint->ERN_RECID;
                $earnReligion->RELI_LOK_RECID = $row;
                $earnReligion->RELI_ACTIVE = 1;
                $earnReligion->save();
            }
        }
        // STORE
        $TENT_TNT_RECID = $request->input('TENT_TNT_RECID');
        if($request->has('TENT_TNT_RECID')) {
            foreach($TENT_TNT_RECID as $row) {
                $earnStore = new FormulaEarnTenant();
                $earnStore->TENT_RECID = Uuid::uuid();
                $earnStore->TENT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnStore->TENT_TNT_RECID = $row;
                $earnStore->TENT_ACTIVE = 1;
                $earnStore->TENT_UPDATE = 1;
                $earnStore->save();
            }
        }
        // CARD TYPE
        $CDTY_LOK_RECID = $request->input('CDTY_LOK_RECID');
        if($request->has('CDTY_LOK_RECID')) {
            foreach($CDTY_LOK_RECID as $row) {
                $earnCardType = new FormulaEarnCardType();
                $earnCardType->CDTY_RECID = Uuid::uuid();
                $earnCardType->CDTY_ERN_RECID = $earnPoint->ERN_RECID;
                $earnCardType->CDTY_LOK_RECID = $row;
                $earnCardType->CDTY_ACTIVE = 1;
                $earnCardType->save();
            }
        }
        // BLOOD TYPE
        $BLOD_LOK_RECID = $request->input('BLOD_LOK_RECID');
        if($request->has('BLOD_LOK_RECID')) {
            foreach($BLOD_LOK_RECID as $row) {
                $earnBlood = new FormulaEarnBlood();
                $earnBlood->BLOD_RECID = Uuid::uuid();
                $earnBlood->BLOD_ERN_RECID = $earnPoint->ERN_RECID;
                $earnBlood->BLOD_LOK_RECID = $row;
                $earnBlood->BLOD_ACTIVE = 1;
                $earnBlood->save();
            }
        }

        $request->session()->flash('success', 'Data has been saved.');

        return redirect('formula/regular');

    }

    public function edit($id) {
        $data['formulaEarn'] = FormulaEarn::findOrFail($id);
        $data['breadcrumbs'] = 'Regular Point Edit';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;

        $data['payments'] = PaymentMethod::all();
        $data['member_type'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['gender'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['religion'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['card_type'] = CardType::all();
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['cards'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['blood_type'] = Lookup::where('LOK_CODE', 'BLOD')->get();
        return view('pointformula.regularpoint.create', $data);
    }

    public function updateStatus(Request $request)
    {
        $id= $request->input('id');
        $FormulaEarn = FormulaEarn::where('ERN_RECID', $id)->first();
        if($FormulaEarn->ERN_ACTIVE == '1')
        {
            $FormulaEarn->ERN_ACTIVE = '0';
            $FormulaEarn->save();
            $request->session()->flash('success', 'Data has been active.');
        } else{
            $FormulaEarn->ERN_ACTIVE = '1';
            $FormulaEarn->save();
        }
            return redirect('formula/regular');
    }
    public function update(Request $request, $id) {
        
     
        
        $rules = [
            'ERN_DESC' => 'required',
            'ERN_AMOUNT_REST' => 'numeric',
            'ERN_AMOUNT' => 'numeric',
            'ERN_POINT' => 'numeric',
            'ERN_POINT_REST' => 'numeric',
            'ERN_MAX_AMOUNT' => 'numeric',
            'ERN_MIN_AMOUNT' => 'numeric'
        ];
        $messages = [
            'ERN_DESC.required' => 'Description field is required',
            'ERN_AMOUNT.numeric' => 'Amount field must be numeric',
            'ERN_AMOUNT_REST.numeric' => 'Rest Amount field must be numeric',
            'ERN_MAX_AMOUNT.numeric' => 'Maximum Amount field must be numeric',
            'ERN_MIN_AMOUNT.numeric' => 'Minimum Amount field must be numeric',
            'ERN_POINT.numeric' => 'Point field must be numeric',
            'ERN_POINT_REST.numeric' => 'Rest Point field must be numeric'
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->only(['ERN_DESC', 'ERN_CARDS', 'ERN_TYPE', 'ERN_AMOUNT', 'ERN_POINT']);
        $earnPoint = FormulaEarn::find($id);
        $earnPoint->fill($post_data);
        $earnPoint->ERN_VALUE = 0;
        $earnPoint->ERN_ACTIVE = $request->has('ERN_ACTIVE') && $request->input('ERN_ACTIVE') == 'on' ? 1 : 0;
        $earnPoint->ERN_MULTIPLE_BASIC = $request->has('ERN_MULTIPLE_BASIC') && $request->input('ERN_MULTIPLE_BASIC') == 'on' ? 1 : 0;
        $earnPoint->ERN_FORMULA = 0;
        $earnPoint->ERN_FRDATE = dateFormat($request->input('fromDate'));
        $earnPoint->ERN_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $earnPoint->ERN_TODATE = dateFormat($request->input('toDate'));
        $earnPoint->ERN_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s
        $earnPoint->ERN_ACTIVE_MAXAMOUNT = $request->has('ERN_ACTIVE_MAXAMOUNT') ? 1 : 0;
        // MAXAMOUNT
        if($request->has('ERN_ACTIVE_MAXAMOUNT') && $request->input('ERN_ACTIVE_MAXAMOUNT') == 'on') {
            $earnPoint->ERN_MAXAMOUNT_VALUE = $request->input('ERN_MAXAMOUNT_VALUE');
        } else {
            $earnPoint->ERN_MAXAMOUNT_VALUE = 0;
        }
        // If Rest Amount has checked.
        if($request->has('ERN_ACTIVE_REST') && $request->input('ERN_ACTIVE_REST') == 'on') {
            $earnPoint->ERN_ACTIVE_REST = 1;
            $earnPoint->ERN_POINT_REST = $request->has('ERN_POINT_REST') ? $request->input('ERN_POINT_REST') : 0;
            $earnPoint->ERN_AMOUNT_REST = $request->has('ERN_AMOUNT_REST') ? $request->input('ERN_AMOUNT_REST') : 0;
            if($request->input('ERN_MULTIPLE_REST') == 'on') {
                $earnPoint->ERN_MULTIPLE_REST = 1;
            } else {
                $earnPoint->ERN_MULTIPLE_REST = 0;
            }
        } else {
            $earnPoint->ERN_ACTIVE_REST = 0;
            $earnPoint->ERN_POINT_REST = 0;
            $earnPoint->ERN_AMOUNT_REST = 0;
            $earnPoint->ERN_MULTIPLE = 0;
            $earnPoint->ERN_MULTIPLE_VALUE = 0;
        }
        // Max amount is checked
        if($request->input('ERN_ACTIVE_MAX') == 'on') {
            $earnPoint->ERN_ACTIVE_MAX = 1;
            $earnPoint->ERN_MAX_AMOUNT = $request->input('ERN_MAX_AMOUNT');
            $earnPoint->ERN_MIN_AMOUNT = $request->input('ERN_MIN_AMOUNT');
            if($request->input('ERN_MULTIPLE') == 'on') {
                $earnPoint->ERN_MULTIPLE = 1;
                $earnPoint->ERN_MULTIPLE_VALUE = $request->input('ERN_MULTIPLE_VALUE');
            } else {
                $earnPoint->ERN_MULTIPLE = 0;
                $earnPoint->ERN_MULTIPLE_VALUE = 0;
            }
        } else {
            $earnPoint->ERN_ACTIVE_MAX = 0;
            $earnPoint->ERN_MAX_AMOUNT = 0;
            $earnPoint->ERN_MIN_AMOUNT = 0;
            $earnPoint->ERN_MULTIPLE = 0;
            $earnPoint->ERN_MULTIPLE_VALUE = 0;

        }
        $earnPoint->ERN_REFERER = $request->has('ERN_REFERER') && ($request->input('ERN_REFERER') == 'on') ? 1 : 0;
        $earnPoint->ERN_PARENT = $request->has('ERN_PARENT') && ($request->input('ERN_PARENT') == 'on') ? 1 : 0;

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $earnPoint->ERN_SUNDAY = 1;
            } else {
                $earnPoint->ERN_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $earnPoint->ERN_MONDAY = 1;
            } else {
                $earnPoint->ERN_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $earnPoint->ERN_TUESDAY = 1;
            } else {
                $earnPoint->ERN_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $earnPoint->ERN_WEDNESDAY = 1;
            } else {
                $earnPoint->ERN_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $earnPoint->ERN_THURSDAY = 1;
            } else {
                $earnPoint->ERN_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $earnPoint->ERN_FRIDAY = 1;
            } else {
                $earnPoint->ERN_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $earnPoint->ERN_SATURDAY = 1;
            } else {
                $earnPoint->ERN_SATURDAY = 0;
            }
        }
        $earnPoint->save(); // Save Object Earn Point

        # Before update, we must delete Relational with Formula Earning
        $earnPoint->payment()->delete();
        $earnPoint->member()->delete();
        $earnPoint->gender()->delete();
        $earnPoint->religion()->delete();
        $earnPoint->cardType()->delete();
        $earnPoint->blood()->delete();
        $earnPoint->tenant()->delete();

        # And update again.
        $PAY_LOK_RECID = $request->input('PAY_LOK_RECID');
        if($request->has('PAY_LOK_RECID')) {
            foreach($PAY_LOK_RECID as $row) {
                $earnPayment = new FormulaEarnPayment();
                $earnPayment->PAY_RECID = Uuid::uuid();
                $earnPayment->PAY_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->PAY_LOK_RECID = $row;
                $earnPayment->PAY_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // MEMBER TYPE
        $MEM_LOK_RECID = $request->input('MEM_LOK_RECID');
        if($request->has('MEM_LOK_RECID')) {
            foreach($MEM_LOK_RECID as $row) {
                $earnMemberType = new FormulaEarnMember();
                $earnMemberType->MEM_RECID = Uuid::uuid();
                $earnMemberType->MEM_ERN_RECID = $earnPoint->ERN_RECID;
                $earnMemberType->MEM_LOK_RECID = $row;
                $earnMemberType->MEM_ACTIVE = 1;
                $earnMemberType->save();
            }
        }
        // GENDER
        $GEN_LOK_RECID = $request->input('GEN_LOK_RECID');
        if($request->has('GEN_LOK_RECID')) {
            foreach($GEN_LOK_RECID as $row) {
                $earnGender = new FormulaEarnGender();
                $earnGender->GEN_RECID = Uuid::uuid();
                $earnGender->GEN_ERN_RECID = $earnPoint->ERN_RECID;
                $earnGender->GEN_LOK_RECID = $row;
                $earnGender->GEN_ACTIVE = 1;
                $earnGender->save();
            }
        }
        // REGLIGION
        $RELI_LOK_RECID = $request->input('RELI_LOK_RECID');
        if($request->has('RELI_LOK_RECID')) {
            foreach($RELI_LOK_RECID as $row) {
                $earnReligion = new FormulaEarnReligion();
                $earnReligion->RELI_RECID = Uuid::uuid();
                $earnReligion->RELI_ERN_RECID = $earnPoint->ERN_RECID;
                $earnReligion->RELI_LOK_RECID = $row;
                $earnReligion->RELI_ACTIVE = 1;
                $earnReligion->save();
            }
        }
        // CARD TYPE
        $CDTY_LOK_RECID = $request->input('CDTY_LOK_RECID');
        if($request->has('CDTY_LOK_RECID')) {
            foreach($CDTY_LOK_RECID as $row) {
                $earnCardType = new FormulaEarnCardType();
                $earnCardType->CDTY_RECID = Uuid::uuid();
                $earnCardType->CDTY_ERN_RECID = $earnPoint->ERN_RECID;
                $earnCardType->CDTY_LOK_RECID = $row;
                $earnCardType->CDTY_ACTIVE = 1;
                $earnCardType->save();
            }
        }
        // BLOOD TYPE
        $BLOD_LOK_RECID = $request->input('BLOD_LOK_RECID');
        if($request->has('BLOD_LOK_RECID')) {
            foreach($BLOD_LOK_RECID as $row) {
                $earnBlood = new FormulaEarnBlood();
                $earnBlood->BLOD_RECID = Uuid::uuid();
                $earnBlood->BLOD_ERN_RECID = $earnPoint->ERN_RECID;
                $earnBlood->BLOD_LOK_RECID = $row;
                $earnBlood->BLOD_ACTIVE = 1;
                $earnBlood->save();
            }
        }
        // STORE
        $TENT_TNT_RECID = $request->input('TENT_TNT_RECID');
        if($request->has('TENT_TNT_RECID')) {
            foreach($TENT_TNT_RECID as $row) {
                $earnStore = new FormulaEarnTenant();
                $earnStore->TENT_RECID = Uuid::uuid();
                $earnStore->TENT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnStore->TENT_TNT_RECID = $row;
                $earnStore->TENT_ACTIVE = 1;
                $earnStore->TENT_UPDATE = 1;
                $earnStore->save();
            }
        }

        $request->session()->flash('success', 'Data has been updated.');
        return redirect('formula/regular');        
    }

    public function view($id)
    {

         $tenant = Tenant::select([
            'TNT_CODE',
            'TNT_DESC',
            'ERN_DESC'
            ])
            ->join('FormulaEarn_Tenant', 'TNT_RECID', '=', 'TENT_TNT_RECID')
            ->join('FormulaEarn', 'ERN_RECID', '=', 'TENT_ERN_RECID')
            ->where('TENT_ERN_RECID', $id)->get();
        return view('pointformula.regularpoint.view', compact('tenant'));
    }

    public function destroy(Request $request) {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        if($request->input('id') !== null) {
            $data = FormulaEarn::find($request->input('id'));
            $data->payment()->delete();
            $data->member()->delete();
            $data->gender()->delete();
            $data->religion()->delete();
            $data->cardType()->delete();
            $data->blood()->delete();
            $data->tenant()->delete();
            $data->delete();

            $request->session()->flash('success', 'Data has been deleted.');
            return redirect('formula/regular');

        } else {
            $request->session()->flash('error', 'Data hasn\'t been deleted.');

            return redirect('formula/regular');
        }
    }

    public function data() {
        $regularpoint = FormulaEarn::regular()->orderBy('ERN_ACTIVE', 'desc')->get();
        return Datatables::of($regularpoint)
            ->addColumn('action', function($regularpoint) {
                $edit = '<a href="' . url('formula/regular/' . $regularpoint->ERN_RECID) . '/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$regularpoint->ERN_RECID.'" data-name="'.$regularpoint->ERN_DESC.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                $view = '<a href="' . route('formula.regular.view', $regularpoint->ERN_RECID).'" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#view-transaction-details">View Tenant</a>';
                return $edit . '&nbsp;' . $delete .'&nbsp;' . $view;
            })
            ->editColumn('ERN_FRDATE', function($point) {
                return $point->ERN_FRDATE->format('d/m/y');
            })
            ->editColumn('ERN_TODATE', function($point) {
                return $point->ERN_TODATE->format('d/m/y');
            })
            ->addColumn('id', function($regularpoint) {
                for ($i = 1; $i <= $regularpoint->count(); $i++) {
                    return $i;
                }
                return $i;
            })
            ->addColumn('tenant', function($regularpoint) {
                if($regularpoint->tenant()->count() > 0) {

                    $tenants = Tenant::all()->count();

                    if($tenants == $regularpoint->tenant()->count()) {

                        return 'All Store';

                    } else {
                        $desc = [];
                        foreach($regularpoint->tenant()->get()->toArray() as $row_tenant){

                            $desc[] = '<span class="md-circle-o"> </span> ' . $row_tenant['tenant']['TNT_DESC'].  '<br>';

                        }
                        return $desc;
                    }

                }
                return '-';
            })
            ->editColumn('ERN_ACTIVE', function ($regular_point) {
                if($regular_point->ERN_ACTIVE == 1) {
                    return '<span class="text-success md-check" data-toggle="modal" data-target="#active-modal" data-id="'.$regular_point->ERN_RECID.'" data-name="'.$regular_point->ERN_DESC.'"></span>';
                } else {
                    return '<span class="text-danger md-close" data-toggle="modal" data-target="#active-modal" data-id="'.$regular_point->ERN_RECID.'" data-name="'.$regular_point->ERN_DESC.'"></span>';
                }
            })
            ->make(true);
    }

}

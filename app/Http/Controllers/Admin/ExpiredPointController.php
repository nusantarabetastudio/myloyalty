<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\PointExpired;
use App\Models\Admin\ZExpired;
use App\Models\Admin\ZTotalPoint;
use Illuminate\Http\Request;
use Session;
use DB;
use Faker\Provider\Uuid;
use App\Models\Admin\PostPE;

class ExpiredPointController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct(Session $session)
    {
        $this->parent = 'Config';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
    }

    public function index()
    {
        $data['repeats'] = [0 => 'Monthly', 1 => 'Yearly'];
        $data['numbers'] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $data['expired_data'] = ZExpired::all();
        $data['last_on'] = PostPE::orderby('PE_DATETIME', 'DESC')->first();
        $data['point_expired'] = ZExpired::all()->first();

        return view('expired', $data);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'REPEAT' => 'required',
            'EVERY' => 'required',
        ]);

        $input = $request->except('id', '_token', '_method', 'START_ON');

        $expiredPoint = ZExpired::find($request->input('id'));
        $expiredPoint->PNUM = $this->pnum;
        $expiredPoint->PTYPE = $this->ptype;
        $expiredPoint->fill($input);
        $expiredPoint->START_ON = dateFormat($request->input('START_ON'));
        $expiredPoint->save();

        $request->session()->flash('notif_success', 'Data updated successfully.');

        return redirect(route('expired-point.edit'));
    }

    public function prosesPE(Request $request) {

       $this->prosesPECLI();

        if(isset($request->manual)) {
            $request->session()->flash('notif_success', 'POINT EXPIRED ALREADY PROCCESS.');
            return redirect(route('expired-point.edit'));
        }

    }


    public function prosesPECLI() {


        DB::statement("insert into POST_PE_History select * from POST_PE");

        PostPE::truncate();

        $getPEFormula = ZExpired::first();


        $id = Uuid::uuid();
        $date = date('Y-m-d H:i:s');

        DB::statement("insert into POST_PE (POS_PE_RECID, PE_RECID, POINT_BEFORE_PE, CUST_RECID, PE_DATETIME)
                        select NEWID(), '{$getPEFormula->EXPIRED_RECID}', TTL_POINT,
                               TTL_CUST_RECID, '{$date}' from Z_TotalPoint where TTL_CODE = 2");


        DB::statement("update A set A.TTL_POINT = 0
                       from Z_TotalPoint A inner join POST_PE B on B.CUST_RECID = A.TTL_CUST_RECID");
    }


    public function rollBack(Request $request) {

        $date = date('Y-m-d H:i:s');
        DB::statement("update A set A.TTL_POINT = B.POINT_BEFORE_PE
                       from Z_TotalPoint A inner join POST_PE B on B.CUST_RECID = A.TTL_CUST_RECID");

        DB::statement("update POST_PE set ROLLBACK_DATETIME = '{$date}' ");

        $request->session()->flash('notif_success', 'ROLLBACK POINT EXPIRED SUCCESSFULL.');
        return redirect(route('expired-point.edit'));
    }

}

<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Master\Role;
use App\Models\Preference;
use App\Models\client;
use Illuminate\Http\Request;
use Session;

class PreferenceController extends Controller {
	private $parent;
	private $parent_link;

	public function __construct() {
		$this->parent = 'Config';
		$this->parent_link = '';
		// $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
	}

	public function edit() {
		$breadcrumbs = 'General Setting';
		$parent = $this->parent;
		$parent_link = $this->parent_link;
		$completedProfileReward = Preference::where('key', 'completed_profile_reward')->first()->value;
		$registrationEarnReward = Preference::where('key', 'registration_earn_reward')->first()->value;
		$smtp_host = Preference::where('key', 'smtp_host')->first()->value;
		$smtp_port = Preference::where('key', 'smtp_port')->first()->value;
		$smtp_encryption = Preference::where('key', 'smtp_encryption')->first()->value;
		$smtp_username = Preference::where('key', 'smtp_username')->first()->value;
		$smtp_password = Preference::where('key', 'smtp_password')->first()->value;
		$proptype = Session::get('data')->PRT_RECID;
		$roles = Role::where('ROLE_PRT_RECID', $proptype)->get();
		$superadmin = Preference::where('key', 'superadmin')->first()->value;
		$admin = Preference::where('key', 'admin')->first()->value;
		$supervisor = Preference::where('key', 'supervisor')->first()->value;
        $cs = Preference::where('key', 'cs')->first()->value;
		$expiredDate = str_replace('YYYY-', '', Preference::where('key', 'expired_date')->first()->value);
		$client = client::where('CLN_NO', substr(Session::get('pnum'), 0, 2))->first();
		$minimum_transaction_bonus = Preference::where('key', 'minimum_transaction_bonus')->first()->value;
		$minimum_transaction_reward = Preference::where('key', 'minimum_transaction_reward')->first()->value;
		return view('management.preference.edit', compact(
			'breadcrumbs', 'parent', 'parent_link', 'completedProfileReward','roles', 'registrationEarnReward', 'admin', 'supervisor', 'cs', 'superadmin', 'client', 'smtp_host', 'smtp_port', 'smtp_encryption', 'smtp_username', 'smtp_password', 'minimum_transaction_bonus', 'minimum_transaction_reward', 'expiredDate'
		));
	}

	public function update(Request $request) {

        $rules = [
            'registration_earn_reward' => 'required',
            'admin' => 'required',
            'supervisor' => 'required',
            'cs' => 'required',
            'expired_date' => 'required|date_format:m-d'
        ];

        $this->validate($request, $rules);

        $active = (isset($request->active)) ? 1 : 0;
        $preference = Preference::where('key','registration_earn_reward')->first();
		$preference->value = $request->input('registration_earn_reward');
		$preference->save();
		$preference = Preference::where('key','completed_profile_reward')->first();
		$preference->value = $request->input('completed_profile_reward');
		$preference->save();
        $preference = Preference::where('key','admin')->first();
		$preference->value = $request->input('admin');
		$preference->save();
        $preference = Preference::where('key','supervisor')->first();
		$preference->value = $request->input('supervisor');
		$preference->save();
        $preference = Preference::where('key','cs')->first();
		$preference->value = $request->input('cs');
		$preference->save();
        $preference = Preference::where('key','smtp_host')->first();
		$preference->value = $request->input('smtp_host');
		$preference->save();
        $preference = Preference::where('key','smtp_port')->first();
		$preference->value = $request->input('smtp_port');
		$preference->save();
		$preference = Preference::where('key','minimum_transaction_bonus')->first();
		$preference->value = $request->input('minimum_transaction_bonus');
		$preference->save();
		$preference = Preference::where('key','minimum_transaction_reward')->first();
		$preference->value = $request->input('minimum_transaction_reward');
		$preference->save();
        $preference = Preference::where('key','smtp_encryption')->first();
		$preference->value = $request->input('smtp_encryption');
		$preference->save();
        $preference = Preference::where('key','smtp_username')->first();
		$preference->value = $request->input('smtp_username');
		$preference->save();
        $preference = Preference::where('key','smtp_password')->first();
		$preference->value = $request->input('smtp_password');
		$preference->save();
        Preference::where('key','expired_date')->update(['value' => 'YYYY-' . $request->input('expired_date')]);
		$client = client::where('CLN_NO', '70')->first();
		$client->CLN_PV = $request->input('point_value');
		$client->CLN_PV_ACTIVE = $active;
		$client->save();

		return redirect()->route('config.general-preference.edit')->with('notif_success', 'Update Success');
	}
}
?>
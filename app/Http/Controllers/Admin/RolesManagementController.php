<?php

namespace App\Http\Controllers\Admin;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class RolesManagementController extends Controller
{
    private $allMenuId = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        if(!Session::has('user')) {
            return redirect('login');
        }
        $proptype = Session::get('user');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID.'/role';
        $uriMenuList = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID. '/menu';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $responseMenu = $client->request('GET', $uriMenuList, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200 || $responseMenu->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentsMenus = json_decode($responseMenu->getBody()->getContents());

            $data['roles'] = $contents->DATA;
            $data['menus'] = $contentsMenus->DATA;
            return view('management.role.index', $data);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        if(!Session::has('user')){
            return redirect('login');
        }
        $proptype = Session::get('user');
        $data = $request->all();

        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/role';
        $client = new Client([
            'http_errors' => false
        ]);
        $bodyParams = [
            'name' => $data['name'],
            'menuIds' => $data['menuIds']
        ];

        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $bodyParams
        ]);

        if ($response->getStatusCode() == 201) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/role');
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        }

        return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!Session::has('user')) {
            return redirect('login');
        }
        $proptype = Session::get('user');
        $uriRole = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID. '/role/'.$id;
        $uriMenuList = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID.'/menu';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uriRole, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $menuListResponse = $client->request('GET', $uriMenuList, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200 && $menuListResponse->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentList = json_decode($menuListResponse->getBody()->getContents());
            $arr['role'] = $contents->DATA;
            $arr['menus'] = $contentList->DATA;
            $this->recurseMenu($arr['role']->menus);
            $arr['getMenuChild'] = $this->allMenuId;

            return view('management.role.show', $arr);
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Session::has('user')) {
            return redirect('login');
        }
        $proptype = Session::get('user');
        $uriRole = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID. '/role/'.$id;
        $uriMenuList = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID.'/menu';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uriRole, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $menuListResponse = $client->request('GET', $uriMenuList, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200 && $menuListResponse->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentList = json_decode($menuListResponse->getBody()->getContents());
            $arr['role'] = $contents->DATA;
            $arr['menus'] = $contentList->DATA;
            $this->recurseMenu($arr['role']->menus);
            $arr['currentMenus'] = $this->allMenuId;

            return view('management.role.index', $arr);
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    function recurseMenu($menus) {
        foreach($menus as $menu) {
            $this->allMenuId[$menu->MENU_RECID] = $menu->MENU_NAME;
            if (!empty($menu->child_menu)) {
                $this->recurseMenu($menu->child_menu);
            }
        }

        return true;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $proptype = Session::get('user');
        $data = $request->all();
        $data['active'] = $request->has('active') ? 1 : 0;
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/role/' . $id;
        $client = new Client([
            'http_errors' => false
        ]);
        $bodyParams = [
            'name' => $data['name'],
            'active' => $data['active'],
            'menuIds' => $data['menuIds'],
            '_method' => 'PUT'
        ];

        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $bodyParams
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/role');
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        }
        return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
    }

    public function destroy(Request $request)
    {
        $this->validate($request, [
            'roleId' => 'required'
        ]);
        $data = $request->except('_token');
        $proptype = Session::get('user');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/role/'. $data['roleId'];
        $client = new Client([
            'http_errors' => false
        ]);
        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/role');
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    public function data() {
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/' . Session::get('user')->USER_PRT_RECID . '/role';
        $client = new Client([
            'http_errors' => false
        ]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);

        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());

            $data = $contents->DATA;

            return response()->json([
                'code' => 200,
                'data'=> $data
            ]);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $contents = json_decode($response->getBody()->getContents());
            $data = $contents->DATA;
            return $data;
        }
        return response()->json([
                'code' => $response->getStatusCode(),
                'data' => 'Something went wrong'
        ]);
    }
}

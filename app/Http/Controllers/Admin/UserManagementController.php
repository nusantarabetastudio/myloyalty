<?php

namespace App\Http\Controllers\Admin;

use App\Models\Master\Tenant;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class UserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $proptype = Session::get('user');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID.'/user';
        $roleUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID .'/role';
        $client = new Client(['http_errors' => false]);
        $responseRole = $client->request('GET', $roleUri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200 && $responseRole->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentsRole = json_decode($responseRole->getBody()->getContents());
            $data['users'] = $contents->DATA->USER_DATA;
            $data['roles'] = $contentsRole->DATA;
            $data['tenants'] = Tenant::select('TNT_RECID as id', 'TNT_DESC as description', 'TNT_CODE as code')->get();
            return view('management.user.index', $data);
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('management.user.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proptype = Session::get('user');
        $data = $request->except('_token');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/user';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);

        if ($response->getStatusCode() == 201) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/user');
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;

            return redirect()->back()->with('notif_error', $msg);;
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return null;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $username
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        $proptype = Session::get('user');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID. '/user/'.$username;
        $roleUri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'.$proptype->USER_PRT_RECID .'/role';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $responseRole = $client->request('GET', $roleUri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200 && $responseRole->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $contentsRole = json_decode($responseRole->getBody()->getContents());
            $data = $contents->DATA;
            $arr['user'] = $data->USER_DATA;
            $arr['roles'] = $contentsRole->DATA;
            $arr['tenants'] = Tenant::select('TNT_RECID as id', 'TNT_DESC as description', 'TNT_CODE as code')->get();

            return view('management.user.index', $arr);
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'roleId' => 'required'
        ]);
        $proptype = Session::get('user');
        $data = $request->except(['_token']);
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/user/' . $id;
        $client = new Client([
            'http_errors' => false
        ]);
        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/user');

        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }

    public function destroy(Request $request)
    {
        $this->validate($request, [
            '_method' => 'required'
        ]);
        $proptype = Session::get('user');
        $data = $request->except(['_token', 'username']);
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/user/'. $request->input('username');
        $client = new Client([
            'http_errors' => false
        ]);
        $response =  $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('config/user');
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {

            return redirect()->back();
        } else {
            return redirect()->back()->with('notif_error', '500 Error. Something went wrong!');
        }
    }
}

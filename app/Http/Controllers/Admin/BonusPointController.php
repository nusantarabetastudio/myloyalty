<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\FormulaEarnBlood;
use App\Models\Admin\FormulaEarnBrand;
use App\Models\Admin\FormulaEarnCardType;
use App\Models\Admin\FormulaEarnDepartment;
use App\Models\Admin\FormulaEarnDivision;
use App\Models\Admin\FormulaEarnCategory;
use App\Models\Admin\FormulaEarnSubCategory;
use App\Models\Admin\FormulaEarnSegment;
use App\Models\Admin\FormulaEarnMember;
use App\Models\Admin\FormulaEarnPayment;
use App\Models\Admin\FormulaEarnReligion;
use App\Models\Admin\FormulaEarnGender;
use App\Models\Admin\FormulaEarnSupplier;
use App\Models\Admin\FormulaEarnItems;
use App\Models\Admin\FormulaEarn;
use App\Models\Admin\FormulaEarnTenant;
use App\Models\Admin\FormulaEarnVendor;
use App\Models\Master\PaymentMethod;
use App\Models\Master\Tenant;
use App\Models\Master\Product;
use App\Models\Master\Lookup;
use App\Models\Master\Vendor;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Faker\Provider\Uuid;
use Datatables, CSV;
use Session;

class BonusPointController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct(Session $session) {
        //$this->middleware('auth');
        $this->parent = 'Point Formula';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        $this->menu_id = '';
        $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index() {
        $data['breadcrumbs'] = 'Bonus Point';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        return view('pointformula.bonuspoint.index', $data);
    }

    public function create() {
        $data['breadcrumbs'] = 'Bonus Point Create';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        $data['payments'] = PaymentMethod::all();
        $data['member_type'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['gender'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['religion'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['card_type'] = Lookup::where('LOK_CODE', 'CTEX')->get();
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['cards'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['blood_type'] = Lookup::where('LOK_CODE', 'BLOD')->get();
       
        
        $data['department'] = Lookup::where('LOK_CODE', 'DEPT')->get();
        $data['division'] = Lookup::where('LOK_CODE', 'DIVI')->get();
        $data['category'] = Lookup::where('LOK_CODE', 'CATG')->get();
        $data['subcategory'] = Lookup::where('LOK_CODE', 'SUBC')->get();
        $data['segment'] = Lookup::where('LOK_CODE', 'SEGM')->get();
        $data['tennant'] = Lookup::where('LOK_CODE', 'TENC')->get(); 
        $data['formulaEarnProducts'] = FormulaEarnItems::all(); 
        $data['formulaType'] = [
            0 => 'Point Earn Bonus By Payment',
            1 => 'Point Earn Bonus By Product',
            2 => 'Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendor',
            9 => 'All Product'
        ];

		 
        return view('pointformula.bonuspoint.create', $data);
    }

    public function store(Request $request) {
        $rules = [
            'ERN_DESC' => 'required',
            'ERN_AMOUNT' => 'numeric',
            'ERN_QTY' => 'numeric',
            'ERN_POINT' => 'numeric',
            'fromDate' => 'date_format:d/m/Y',
            'toDate' => 'date_format:d/m/Y'
        ];
        $messages = [
            'ERN_DESC.required' => 'Description field is required',
            'ERN_AMOUNT.numeric' => 'Amount field must be numeric',
            'ERN_QTY.numeric' => 'Quantity field must be numeric',
            'ERN_POINT.numeric' => 'Point field must be numeric',
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->only(['ERN_DESC', 'ERN_AMOUNT', 'ERN_TYPE', 'ERN_FORMULA_TYPE']);
        $earnPoint = new FormulaEarn();
        $earnPoint->ERN_RECID = Uuid::uuid();
        $earnPoint->fill($post_data);
        $earnPoint->ERN_PNUM = $this->pnum;
        $earnPoint->ERN_PTYPE = $this->ptype;
        $earnPoint->ERN_CARDS = '00065099-F571-4FF6-9258-866726445731';
        $earnPoint->ERN_ACTIVE = 1;
        $earnPoint->ERN_FORMULA = $request->input('ERN_FORMULA');
        $earnPoint->ERN_FRDATE = dateFormat($request->input('fromDate'));
        $earnPoint->ERN_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $earnPoint->ERN_TODATE = dateFormat($request->input('toDate'));
        $earnPoint->ERN_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s
        // Value Type
        $earnPoint->ERN_VALUE = ($request->input('ERN_VALUE') == 1) ? 1 : 0;
        // check quantity
        if($request->input('ERN_QTY') == '' || $request->input('ERN_QTY') == null ) {
            $earnPoint->ERN_QTY = 0;
        } else {
            $earnPoint->ERN_QTY = $request->input('ERN_QTY');
            $earnPoint->ERN_AMOUNT = 0;
        }
        // Check Amount
        if($request->input('ERN_AMOUNT') == '' || $request->input('ERN_AMOUNT') == null) {
            $earnPoint->ERN_AMOUNT = 0;
        } else {
            $earnPoint->ERN_QTY = $request->input('ERN_QTY');
        }
        // Check Point
        if($request->input('ERN_POINT') == '' || $request->input('ERN_POINT') == null ) {
            $earnPoint->ERN_POINT = 0;
        } else {
            $earnPoint->ERN_POINT = $request->input('ERN_POINT');
        }
        // If Multiple Has checked.
        $request->input('ERN_MULTIPLE') == 'on' && $request->has('ERN_MULTIPLE') ? $earnPoint->ERN_MULTIPLE = 1 : $earnPoint->ERN_MULTIPLE = 0;
        // Multiple Value
        if($request->input('ERN_MULTIPLE') == 'on') {
            $earnPoint->ERN_MULTIPLE_VALUE = $request->input('ERN_MULTIPLE_VALUE');
        } else {
            $earnPoint->ERN_MULTIPLE_VALUE = 0;
        }

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $earnPoint->ERN_SUNDAY = 1;
            } else {
                $earnPoint->ERN_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $earnPoint->ERN_MONDAY = 1;
            } else {
                $earnPoint->ERN_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $earnPoint->ERN_TUESDAY = 1;
            } else {
                $earnPoint->ERN_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $earnPoint->ERN_WEDNESDAY = 1;
            } else {
                $earnPoint->ERN_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $earnPoint->ERN_THURSDAY = 1;
            } else {
                $earnPoint->ERN_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $earnPoint->ERN_FRIDAY = 1;
            } else {
                $earnPoint->ERN_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $earnPoint->ERN_SATURDAY = 1;
            } else {
                $earnPoint->ERN_SATURDAY = 0;
            }
        }


        $earnPoint->save(); // Save Object Earn Point
        // PAYMENT
        $PAY_LOK_RECID = $request->input('PAY_LOK_RECID');
        if($request->has('PAY_LOK_RECID')) {
            foreach($PAY_LOK_RECID as $row) {
                $earnPayment = new FormulaEarnPayment();
                $earnPayment->PAY_RECID = Uuid::uuid();
                $earnPayment->PAY_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->PAY_LOK_RECID = $row;
                $earnPayment->PAY_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // MEMBER TYPE
        $MEM_LOK_RECID = $request->input('MEM_LOK_RECID');
        if($request->has('MEM_LOK_RECID')) {
            foreach($MEM_LOK_RECID as $row) {
                $earnMemberType = new FormulaEarnMember();
                $earnMemberType->MEM_RECID = Uuid::uuid();
                $earnMemberType->MEM_ERN_RECID = $earnPoint->ERN_RECID;
                $earnMemberType->MEM_LOK_RECID = $row;
                $earnMemberType->MEM_ACTIVE = 1;
                $earnMemberType->save();
            }
        }
        // GENDER
        $GEN_LOK_RECID = $request->input('GEN_LOK_RECID');
        if($request->has('GEN_LOK_RECID')) {
            foreach($GEN_LOK_RECID as $row) {
                $earnGender = new FormulaEarnGender();
                $earnGender->GEN_RECID = Uuid::uuid();
                $earnGender->GEN_ERN_RECID = $earnPoint->ERN_RECID;
                $earnGender->GEN_LOK_RECID = $row;
                $earnGender->GEN_ACTIVE = 1;
                $earnGender->save();
            }
        }
        // RELIGION
        $RELI_LOK_RECID = $request->input('RELI_LOK_RECID');
        if($request->has('RELI_LOK_RECID')) {
            foreach($RELI_LOK_RECID as $row) {
                $earnReligion = new FormulaEarnReligion();
                $earnReligion->RELI_RECID = Uuid::uuid();
                $earnReligion->RELI_ERN_RECID = $earnPoint->ERN_RECID;
                $earnReligion->RELI_LOK_RECID = $row;
                $earnReligion->RELI_ACTIVE = 1;
                $earnReligion->save();
            }
        }
        // CARD TYPE
        $CDTY_LOK_RECID = $request->input('CDTY_LOK_RECID');
        if($request->has('CDTY_LOK_RECID')) {
            foreach($CDTY_LOK_RECID as $row) {
                $earnCardType = new FormulaEarnCardType();
                $earnCardType->CDTY_RECID = Uuid::uuid();
                $earnCardType->CDTY_ERN_RECID = $earnPoint->ERN_RECID;
                $earnCardType->CDTY_LOK_RECID = $row;
                $earnCardType->CDTY_ACTIVE = 1;
                $earnCardType->save();
            }
        }
        // BLOOD TYPE
        $BLOD_LOK_RECID = $request->input('BLOD_LOK_RECID');
        if($request->has('BLOD_LOK_RECID')) {
            foreach($BLOD_LOK_RECID as $row) {
                $earnBlood = new FormulaEarnBlood();
                $earnBlood->BLOD_RECID = Uuid::uuid();
                $earnBlood->BLOD_ERN_RECID = $earnPoint->ERN_RECID;
                $earnBlood->BLOD_LOK_RECID = $row;
                $earnBlood->BLOD_ACTIVE = 1;
                $earnBlood->save();
            }
        }
       
        
          // DEPARTMENT
        $DEPARTMENT = $request->input('DEPT_DEPARTMENT');
        if($request->has('DEPT_DEPARTMENT')) {
            foreach($DEPARTMENT as $row) {
                $earnPayment = new FormulaEarnDepartment();
                $earnPayment->DEPT_RECID = Uuid::uuid();
                $earnPayment->DEPT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->DEPT_DEPARTMENT = $row;
                $earnPayment->DEPT_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // DIVISION
        $DIV_DIVISION = $request->input('DIV_DIVISION');
        if($request->has('DIV_DIVISION')) {
            foreach($DIV_DIVISION as $row) {
                $earnPayment = new FormulaEarnDivision();
                $earnPayment->DIV_RECID = Uuid::uuid();
                $earnPayment->DIV_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->DIV_DIVISION = $row;
                $earnPayment->DIV_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // CATEGORY
        $CATEGORY = $request->input('CAT_CATEGORY');
        if($request->has('CAT_CATEGORY')) {
            foreach($CATEGORY as $row) {
                $earnPayment = new FormulaEarnCategory();
                $earnPayment->CAT_RECID = Uuid::uuid();
                $earnPayment->CAT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->CAT_CATEGORY = $row;
                $earnPayment->CAT_ACTIVE = 1;
                $earnPayment->save();
            }
        }

        // SUBCATEGORY
        $SUBCATEGORY = $request->input('SCAT_SUBCATEGORY');
        if($request->has('SCAT_SUBCATEGORY')) {
            foreach($SUBCATEGORY as $row) {
                $earnPayment = new FormulaEarnSubCategory();
                $earnPayment->SCAT_RECID = Uuid::uuid();
                $earnPayment->SCAT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->SCAT_SUBCATEGORY = $row;
                $earnPayment->SCAT_ACTIVE = 1;
                $earnPayment->save();
            }
        }

        // SEGMENT
        $SEGMENT = $request->input('SEG_SEGMENT');
        if($request->has('SEG_SEGMENT')) {
            foreach($SEGMENT as $row) {
                $earnPayment = new FormulaEarnSegment();
                $earnPayment->SEG_RECID = Uuid::uuid();
                $earnPayment->SEG_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->SEG_SEGMENT = $row;
                $earnPayment->SEG_ACTIVE = 1;
                $earnPayment->save();
            }
        }

        // Point Earn Bonus by Product
        $product = $request->input('ITM_PRODUCT');
        $productItemSupplier = $request->has('ITM_SUPPLIER') ? $request->input('ITM_SUPPLIER') : null;
        $ERN_RECID = $earnPoint->ERN_RECID;
        $csv = $request->file('csv_file');
        if($csv) {
            $pathToCsv = $csv->getPathName();
            CSV::load($pathToCsv)->chunk(5000, function($results) use($productItemSupplier, $ERN_RECID) {
                foreach ($results as $row) {
                    $earnProduct = new FormulaEarnItems();
                    $earnProduct->ITM_RECID = strtoupper(Uuid::uuid());
                    $earnProduct->ITM_ERN_RECID = $ERN_RECID;
                    $earnProduct->ITM_PRODUCT = $row[0];
                    $earnProduct->ITM_SUPPLIER = $productItemSupplier;
                    $earnProduct->ITM_ACTIVE = 1;
                    $earnProduct->save();
                }
            });
        }

        if($request->has('ITM_PRODUCT') && !$csv) {
            foreach ($product as $row) {
                $earnProduct = new FormulaEarnItems();
                $earnProduct->ITM_RECID = strtoupper(Uuid::uuid());
                $earnProduct->ITM_ERN_RECID = $earnPoint->ERN_RECID;
                $earnProduct->ITM_PRODUCT = $row;
                $earnProduct->ITM_SUPPLIER = $productItemSupplier;
                $earnProduct->ITM_ACTIVE = 1;
                $earnProduct->save();
            }
        }

        // Point Earn Bonus by Supplier
        $supplier = $request->input('SPL_SUPPLIER');
        if($request->has('SPL_SUPPLIER')) {
            foreach ($supplier as $row) {
                $earnSupplier = new FormulaEarnSupplier();
                $earnSupplier->SPL_RECID = strtoupper(Uuid::uuid());
                $earnSupplier->SPL_ERN_RECID = $earnPoint->ERN_RECID;
                $earnSupplier->SPL_SUPPLIER = $row;
                $earnSupplier->SPL_ACTIVE = 1;
                $earnSupplier->save();
            }
        }

        // Point Earn Bonus by Vendor
        $vendor = $request->input('VND_VENDOR_RECID');
        if($request->has('VND_VENDOR_RECID')) {
            foreach ($vendor as $row) {
                $earnVendor = new FormulaEarnVendor();
                $earnVendor->VND_RECID = strtoupper(Uuid::uuid());
                $earnVendor->VND_ERN_RECID = $earnPoint->ERN_RECID;
                $earnVendor->VND_VENDOR_RECID = $row;
                $earnVendor->VND_ACTIVE = 1;
                $earnVendor->save();
            }
            
        }
        // STORE
        $TENT_TNT_RECID = $request->input('TENT_TNT_RECID');
        if($request->has('TENT_TNT_RECID')) {
            foreach($TENT_TNT_RECID as $row) {
                $earnStore = new FormulaEarnTenant();
                $earnStore->TENT_RECID = Uuid::uuid();
                $earnStore->TENT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnStore->TENT_TNT_RECID = $row;
                $earnStore->TENT_ACTIVE = 1;
                $earnStore->TENT_UPDATE = 1;
                $earnStore->save();
            }
        }

        // brand.
        $brand = $request->input('BRAND_BRAND_RECID');
        if($request->has('BRAND_BRAND_RECID')) {
            foreach ($brand as $row) {
                $earnBrand = new FormulaEarnBrand();
                $earnBrand->BRAND_RECID = strtoupper(Uuid::uuid());
                $earnBrand->BRAND_ERN_RECID = $earnPoint->ERN_RECID;
                $earnBrand->BRAND_BRAND_RECID = $row;
                $earnBrand->BRAND_ACTIVE = 1;
                $earnBrand->save();
            }
        }

        $request->session()->flash('success', 'Data has been saved.');

        return redirect('formula/bonus');
    }

    public function edit($id) {
        $data['formulaEarn'] = FormulaEarn::find($id);  
        $data['breadcrumbs'] = 'Bonus Point Edit';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        // FormulaEarn Product Get
        $formulaEarnProduct = $data['formulaEarn']->product !== null ? $data['formulaEarn']->product : null;
        // Parse to json
        $data['formulaEarnProduct'] = $formulaEarnProduct->toArray();
        // to check existing product on another formulaEarn
        // get all FormulaEarn_Items
        $data['formulaEarnAll'] = FormulaEarn::all();

        $data['payments'] = PaymentMethod::all();
        $data['member_type'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['gender'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['religion'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['card_type'] = Lookup::where('LOK_CODE', 'CTEX')->get();
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['cards'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['blood_type'] = Lookup::where('LOK_CODE', 'BLOD')->get();

        $data['department'] = Lookup::where('LOK_CODE', 'DEPT')->get();
        $data['division'] = Lookup::where('LOK_CODE', 'DIVI')->get();
        $data['category'] = Lookup::where('LOK_CODE', 'CATG')->get();
        $data['subcategory'] = Lookup::where('LOK_CODE', 'SUBC')->get();
        $data['segment'] = Lookup::where('LOK_CODE', 'SEGM')->get();
        $data['formulaEarnProducts'] = FormulaEarnItems::all();

         $data['formulaType'] = [
            0 => 'Point Earn Bonus By Payment',
            1 => 'Point Earn Bonus By Product',
            2 => 'Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendor',
            9 => 'All Product'
        ];

        return view('pointformula.bonuspoint.create', $data);
    }

    public function update(Request $request, $id) {
        $rules = [
            'ERN_DESC' => 'required',
            'ERN_AMOUNT' => 'numeric',
            'ERN_QTY' => 'numeric',
            'ERN_POINT' => 'numeric',
            'fromDate' => 'date_format:d/m/Y',
            'toDate' => 'date_format:d/m/Y'
        ];
        $messages = [
            'ERN_DESC.required' => 'Description field is required',
            'ERN_AMOUNT.numeric' => 'Amount field must be numeric',
            'ERN_QTY.numeric' => 'Quantity field must be numeric',
            'ERN_POINT.numeric' => 'Point field must be numeric',
        ];
        $this->validate($request, $rules, $messages);

        $post_data = $request->only(['ERN_DESC', 'ERN_AMOUNT', 'ERN_TYPE', 'ERN_FORMULA_TYPE']);
        $earnPoint = FormulaEarn::find($id);
        $earnPoint->fill($post_data);
        if($request->input('ERN_ACTIVE') == 'on') {
            $earnPoint->ERN_ACTIVE = 1;
        } else {
            $earnPoint->ERN_ACTIVE = 0;
        }
        $earnPoint->ERN_FORMULA = $request->input('ERN_FORMULA');
        $earnPoint->ERN_VALUE = ($request->input('ERN_VALUE') == 1) ? 1 : 0;
        $earnPoint->ERN_FRDATE = dateFormat($request->input('fromDate'));
        $earnPoint->ERN_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $earnPoint->ERN_TODATE = dateFormat($request->input('toDate'));
        $earnPoint->ERN_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s
        // check quantity
        if($request->input('ERN_QTY') == '' || $request->input('ERN_QTY') == null ) {
            $earnPoint->ERN_QTY = 0;
        } else {
            $earnPoint->ERN_QTY = $request->input('ERN_QTY');
            $earnPoint->ERN_AMOUNT = 0;
        }
        // Check Amount
        if($request->input('ERN_AMOUNT') == '' || $request->input('ERN_AMOUNT') == null) {
            $earnPoint->ERN_AMOUNT = 0;
        } else {
            $earnPoint->ERN_QTY = $request->input('ERN_QTY');
            $earnPoint->ERN_AMOUNT = $request->input('ERN_AMOUNT');
        }
        // Check Point
        if($request->input('ERN_POINT') == '' || $request->input('ERN_POINT') == null ) {
            $earnPoint->ERN_POINT = 0;
        } else {
            $earnPoint->ERN_POINT = $request->input('ERN_POINT');
        }
        // If Multiple Has checked.
        $request->input('ERN_MULTIPLE') == 'on' && $request->has('ERN_MULTIPLE') ? $earnPoint->ERN_MULTIPLE = 1 : $earnPoint->ERN_MULTIPLE = 0;
        // Multiple Value
        if($request->input('ERN_MULTIPLE') == 'on') {
            $earnPoint->ERN_MULTIPLE_VALUE = $request->input('ERN_MULTIPLE_VALUE');
        } else {
            $earnPoint->ERN_MULTIPLE_VALUE = 0;
        }

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $earnPoint->ERN_SUNDAY = 1;
            } else {
                $earnPoint->ERN_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $earnPoint->ERN_MONDAY = 1;
            } else {
                $earnPoint->ERN_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $earnPoint->ERN_TUESDAY = 1;
            } else {
                $earnPoint->ERN_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $earnPoint->ERN_WEDNESDAY = 1;
            } else {
                $earnPoint->ERN_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $earnPoint->ERN_THURSDAY = 1;
            } else {
                $earnPoint->ERN_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $earnPoint->ERN_FRIDAY = 1;
            } else {
                $earnPoint->ERN_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $earnPoint->ERN_SATURDAY = 1;
            } else {
                $earnPoint->ERN_SATURDAY = 0;
            }
        }
        $earnPoint->save(); // Save Object Earn Point
        $earnPoint->payment()->delete();
        $earnPoint->gender()->delete();
        $earnPoint->blood()->delete();
        $earnPoint->religion()->delete();
        $earnPoint->tenant()->delete();
        $earnPoint->department()->delete();
        $earnPoint->division()->delete();
        $earnPoint->category()->delete();
        $earnPoint->subcategory()->delete();
        $earnPoint->segment()->delete();
        $earnPoint->supplier()->delete();
        $earnPoint->product()->delete();
        $earnPoint->brand()->delete();
        $earnPoint->vendor()->delete();

        // DEPARTMENT
        $DEPARTMENT = $request->input('DEPT_DEPARTMENT');
        if($request->has('DEPT_DEPARTMENT')) {
            foreach($DEPARTMENT as $row) {
                $earnPayment = new FormulaEarnDepartment();
                $earnPayment->DEPT_RECID = Uuid::uuid();
                $earnPayment->DEPT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->DEPT_DEPARTMENT = $row;
                $earnPayment->DEPT_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // DIVISION
        $DIVISION = $request->input('DIV_DIVISION');
        if($request->has('DIV_DIVISION')) {
            foreach($DIVISION as $row) {
                $earnPayment = new FormulaEarnDivision();
                $earnPayment->DIV_RECID = Uuid::uuid();
                $earnPayment->DIV_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->DIV_DIVISION = $row;
                $earnPayment->DIV_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // CATEGORY
        $CATEGORY = $request->input('CAT_CATEGORY');
        if($request->has('CAT_CATEGORY')) {
            foreach($CATEGORY as $row) {
                $earnPayment = new FormulaEarnCategory();
                $earnPayment->CAT_RECID = Uuid::uuid();
                $earnPayment->CAT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->CAT_CATEGORY = $row;
                $earnPayment->CAT_ACTIVE = 1;
                $earnPayment->save();
            }
        }

        // SUBCATEGORY
        $SUBCATEGORY = $request->input('SCAT_SUBCATEGORY');
        if($request->has('SCAT_SUBCATEGORY')) {
            foreach($SUBCATEGORY as $row) {
                $earnPayment = new FormulaEarnSubCategory();
                $earnPayment->SCAT_RECID = Uuid::uuid();
                $earnPayment->SCAT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->SCAT_SUBCATEGORY = $row;
                $earnPayment->SCAT_ACTIVE = 1;
                $earnPayment->save();
            }
        }

        // SEGMENT
        $SEGMENT = $request->input('SEG_SEGMENT');
        if($request->has('SEG_SEGMENT')) {
            foreach($SEGMENT as $row) {
                $earnPayment = new FormulaEarnSegment();
                $earnPayment->SEG_RECID = Uuid::uuid();
                $earnPayment->SEG_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->SEG_SEGMENT = $row;
                $earnPayment->SEG_ACTIVE = 1;
                $earnPayment->save();
            }
        }

        // PAYMENT
        $PAY_LOK_RECID = $request->input('PAY_LOK_RECID');
        if($request->has('PAY_LOK_RECID')) {
            foreach($PAY_LOK_RECID as $row) {
                $earnPayment = new FormulaEarnPayment();
                $earnPayment->PAY_RECID = Uuid::uuid();
                $earnPayment->PAY_ERN_RECID = $earnPoint->ERN_RECID;
                $earnPayment->PAY_LOK_RECID = $row;
                $earnPayment->PAY_ACTIVE = 1;
                $earnPayment->save();
            }
        }
        // GENDER
        $GEN_LOK_RECID = $request->input('GEN_LOK_RECID');
        if($request->has('GEN_LOK_RECID')) {
            foreach($GEN_LOK_RECID as $row) {
                $earnGender = new FormulaEarnGender();
                $earnGender->GEN_RECID = Uuid::uuid();
                $earnGender->GEN_ERN_RECID = $earnPoint->ERN_RECID;
                $earnGender->GEN_LOK_RECID = $row;
                $earnGender->GEN_ACTIVE = 1;
                $earnGender->save();
            }
        }
        // RELIGION
        $RELI_LOK_RECID = $request->input('RELI_LOK_RECID');
        if($request->has('RELI_LOK_RECID')) {
            foreach($RELI_LOK_RECID as $row) {
                $earnReligion = new FormulaEarnReligion();
                $earnReligion->RELI_RECID = Uuid::uuid();
                $earnReligion->RELI_ERN_RECID = $earnPoint->ERN_RECID;
                $earnReligion->RELI_LOK_RECID = $row;
                $earnReligion->RELI_ACTIVE = 1;
                $earnReligion->save();
            }
        }
        // BLOOD TYPE
        $BLOD_LOK_RECID = $request->input('BLOD_LOK_RECID');
        if($request->has('BLOD_LOK_RECID')) {
            foreach($BLOD_LOK_RECID as $row) {
                $earnBlood = new FormulaEarnBlood();
                $earnBlood->BLOD_RECID = Uuid::uuid();
                $earnBlood->BLOD_ERN_RECID = $earnPoint->ERN_RECID;
                $earnBlood->BLOD_LOK_RECID = $row;
                $earnBlood->BLOD_ACTIVE = 1;
                $earnBlood->save();
            }
        }

        // STORE
        $TENT_TNT_RECID = $request->input('TENT_TNT_RECID');
        if($request->has('TENT_TNT_RECID')) {
            foreach($TENT_TNT_RECID as $row) {
                $earnStore = new FormulaEarnTenant();
                $earnStore->TENT_RECID = Uuid::uuid();
                $earnStore->TENT_ERN_RECID = $earnPoint->ERN_RECID;
                $earnStore->TENT_TNT_RECID = $row;
                $earnStore->TENT_ACTIVE = 1;
                $earnStore->TENT_UPDATE = 1;
                $earnStore->save();
            }
        }
        // Point Earn Bonus by Vendor
        $vendor = $request->input('VND_VENDOR_RECID');
        if($request->has('VND_VENDOR_RECID')) {
            foreach ($vendor as $row) {
                $earnVendor = new FormulaEarnVendor();
                $earnVendor->VND_RECID = strtoupper(Uuid::uuid());
                $earnVendor->VND_ERN_RECID = $earnPoint->ERN_RECID;
                $earnVendor->VND_VENDOR_RECID = $row;
                $earnVendor->VND_ACTIVE = 1;
                $earnVendor->save();
            
            }
        }


        // Point Earn Bonus by Product
        $product = $request->input('ITM_PRODUCT');
        $ERN_RECID = $earnPoint->ERN_RECID;
        $csv = $request->file('csv_file');
        if($csv) {
            $pathToCsv = $csv->getPathName();
            CSV::load($pathToCsv)->chunk(5000, function($results) use($ERN_RECID) {
                foreach ($results as $row) {
                        $earnProduct = new FormulaEarnItems();
                        $earnProduct->ITM_RECID = strtoupper(Uuid::uuid());
                        $earnProduct->ITM_ERN_RECID = $ERN_RECID;
                        $earnProduct->ITM_PRODUCT = $row[0];
                        $earnProduct->ITM_ACTIVE = 1;
                        $earnProduct->save();
                }
            });
        }
        if($request->has('ITM_PRODUCT') && !$csv) {
            foreach ($product as $row) {
                $earnProduct = new FormulaEarnItems();
                $earnProduct->ITM_RECID = strtoupper(Uuid::uuid());
                $earnProduct->ITM_ERN_RECID = $earnPoint->ERN_RECID;
                $earnProduct->ITM_PRODUCT = $row;
                $earnProduct->ITM_ACTIVE = 1;
                $earnProduct->save();
            }
        }

        // Point Earn Bonus by Supplier
        $supplier = $request->input('SPL_SUPPLIER');
        if($request->has('SPL_SUPPLIER')) {
            foreach ($supplier as $row) {
                $earnSupplier = new FormulaEarnSupplier();
                $earnSupplier->SPL_RECID = strtoupper(Uuid::uuid());
                $earnSupplier->SPL_ERN_RECID = $earnPoint->ERN_RECID;
                $earnSupplier->SPL_SUPPLIER = $row;
                $earnSupplier->SPL_ACTIVE = 1;
                $earnSupplier->save();
            }
        }

        // Brand.
        $brand = $request->input('BRAND_BRAND_RECID');
        if($request->has('BRAND_BRAND_RECID')) {
            foreach ($brand as $row) {
                $earnBrand = new FormulaEarnBrand();
                $earnBrand->BRAND_RECID = strtoupper(Uuid::uuid());
                $earnBrand->BRAND_ERN_RECID = $earnPoint->ERN_RECID;
                $earnBrand->BRAND_BRAND_RECID = $row;
                $earnBrand->BRAND_ACTIVE = 1;
                $earnBrand->save();
            }
        }

        $request->session()->flash('success', 'Data has been saved.');

        return redirect('formula/bonus');
    }

    public function destroy(Request $request) {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        if($request->input('id') !== null) {
            $data = FormulaEarn::findOrFail($request->input('id'));
            $data->payment()->delete();
            $data->gender()->delete();
            $data->blood()->delete();
            $data->tenant()->delete();
            $data->religion()->delete();
            $data->department()->delete();
            $data->division()->delete();
            $data->category()->delete();
            $data->subcategory()->delete();
            $data->segment()->delete();
            $data->supplier()->delete();
            $data->product()->delete();
            $data->brand()->delete();
            $data->vendor()->delete();
            $data->delete();

            $request->session()->flash('success', 'Data has been deleted.');
            return redirect('formula/bonus');

        } else {
            $request->session()->flash('error', 'Data hasn\'t been deleted.');
            return redirect('formula/bonus');
        }
    }

    public function view($id)
    {
         $tenant = Tenant::select([
            'TNT_CODE',
            'TNT_DESC',
            'ERN_DESC'
            ])
            ->join('FormulaEarn_Tenant', 'TNT_RECID', '=', 'TENT_TNT_RECID')
            ->join('FormulaEarn', 'ERN_RECID', '=', 'TENT_ERN_RECID')
            ->where('TENT_ERN_RECID', $id)->get();
        return view('pointformula.regularpoint.view', compact('tenant'));
    }

    public function updateStatus(Request $request)
    {
        $id= $request->input('id');
        $FormulaEarn = FormulaEarn::where('ERN_RECID', $id)->first();
        if($FormulaEarn->ERN_ACTIVE == '1')
        {
            $FormulaEarn->ERN_ACTIVE = '0';
            $FormulaEarn->save();
            $request->session()->flash('success', 'Data has been active.');
        } else{
            $FormulaEarn->ERN_ACTIVE = '1';
            $FormulaEarn->save();
        }
            return redirect('formula/bonus');
    }

    public function data(){
        $formulaEarn = FormulaEarn::bonus();

        $datatables = app('datatables')->of($formulaEarn)
            ->addColumn('action', function($point) {
                $edit = '<a href="' . url('formula/bonus/' . $point->ERN_RECID) . '/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$point->ERN_RECID.'" data-name="'.$point->ERN_DESC.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                $view = '<a href="' . route('formula.bonus.view', $point->ERN_RECID).'" type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#view-transaction-details">View Tenant</a>';
                return $edit . '&nbsp;' . $delete .'&nbsp;' . $view;
            })
            ->editColumn('ERN_FRDATE', function($point) {
                return $point->ERN_FRDATE->format('d/m/y');
            })
            ->editColumn('ERN_FORMULA_TYPE', function ($point) {
                if($point->ERN_FORMULA_TYPE !== null) {
                    if($point->ERN_FORMULA_TYPE == 0) {
                        return 'Point Earn';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 1 ) {
                        return 'Point Earn Bonus By Product';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 2 ) {
                        return 'Brand';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 3 ) {
                        return 'Department';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 4 ) {
                        return 'Division';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 5 ) {
                        return 'Category';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 6 ) {
                        return 'Sub Category';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 7 ) {
                        return 'Segment';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 8 ) {
                        return 'Vendor';
                    }
                    elseif( $point->ERN_FORMULA_TYPE == 9 ) {
                        return 'All Product';
                    }
                }
                return 'Unknown';
            })
            ->editColumn('ERN_TODATE', function($point) {
                return $point->ERN_TODATE->format('d/m/y');
            })
            ->addColumn('tenant', function($regular_point) {
                if($regular_point->tenant()->count() > 0) {

                    $tenants = Tenant::all()->count();

                    if($tenants == $regular_point->tenant()->count()) {

                        return 'All Store';

                    } else {
                        $desc = [];
                        foreach($regular_point->tenant()->get()->toArray() as $row_tenant){

                            $desc[] = '<span class="md-circle-o"> </span> ' . $row_tenant['tenant']['TNT_DESC'].  '<br>';

                        }
                        return $desc;
                    }

                }
                return '-';
            })
            ->editColumn('ERN_ACTIVE', function ($regular_point) {
                if($regular_point->ERN_ACTIVE == 1) {
                    return '<span class="text-success md-check" data-toggle="modal" data-target="#active-modal" data-id="'.$regular_point->ERN_RECID.'" data-name="'.$regular_point->ERN_DESC.'"></span>';
                } else {
                    return '<span class="text-danger md-close" data-toggle="modal" data-target="#active-modal" data-id="'.$regular_point->ERN_RECID.'" data-name="'.$regular_point->ERN_DESC.'"></span>';
                }
            });

        return $datatables->make(true);
    }


    public function getProductData() {
        $data = Product::select(['PRO_RECID', 'PRO_DESCR'])->get();

        $datatables = app('datatables')->of($data);

        return $datatables->make(true);
    }
}

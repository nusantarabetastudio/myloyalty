<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\FormulaEarn;
use App\Models\Admin\FormulaEarnBlood;
use App\Models\Admin\FormulaEarnCardType;
use App\Models\Admin\FormulaEarnCategory;
use App\Models\Admin\FormulaEarnDepartment;
use App\Models\Admin\FormulaEarnDivision;
use App\Models\Admin\FormulaEarnGender;
use App\Models\Admin\FormulaEarnItems;
use App\Models\Admin\FormulaEarnMember;
use App\Models\Admin\FormulaEarnPayment;
use App\Models\Admin\FormulaEarnReligion;
use App\Models\Admin\FormulaEarnSegment;
use App\Models\Admin\FormulaEarnSubCategory;
use App\Models\Admin\FormulaEarnTenant;
use App\Models\Admin\FormulaEarnBrand;
use App\Models\Admin\FormulaEarnVendor;
use App\Models\Admin\Post;
use App\Models\Admin\PostDetail;
use App\Models\Admin\PostTemp;
use App\Models\Admin\PostTempPayment;
use App\Models\Admin\ZTotalPoint;
use App\Models\Customer;
use App\Models\Master\Merchandise;
use App\Models\Master\Tenant;
use App\Models\Counter;
use Carbon\Carbon;
use Excel;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\Master\Product;
use App\Models\Master\Vendor;
use App\Models\Master\Brand;
use App\Models\Master\PaymentMethod;
use App\Models\Master\Lookup;
use App\Models\Preference;
use Log;
use Exception;
use Mail;
use App\Models\PointRandom;

class EarningController extends Controller {

    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    /**
     * RewardPointController constructor.
     * @param $session
     */
    public function __construct(Session $session) {
        $this->parent = 'Earning';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : env('PNUM');
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : env('PTYPE');
        $this->menu_id = '';
    }

    public function index() {
        $data['breadcrumbs'] = 'Upload by CSV';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;
        return view('earningvoid.index', $data);
    }

    public function result() {
        $data['breadcrumbs'] = 'Earning Result';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['menu_id'] = $this->menu_id;

        $data['post'] = DB::table('post')->orderBy('POS_POST_DATE', 'DESC')
                        ->orderBy('POS_POST_TIME', 'DESC')->where('POS_BARCODE', '!=', '')->paginate(50);

        return view('earningvoid.result', $data);
    }

    public function proccess(Request $request) {
        // setting up rules
        $rules = [
            'csv_file' => 'required|mimes:csv,txt',
            'payment_file' => 'mimes:csv,txt',
        ];
        // Validate
        $this->validate($request, $rules);

        $destinationPath = 'uploads'; // upload path
        $extension = $request->file('csv_file')->getClientOriginalExtension();
        $extension_payment = $request->file('payment_file')->getClientOriginalExtension();

        if ($extension == 'csv' and $extension_payment = 'csv') {

            $fileName = date('d-m-Y') . '_' . rand(11111, 99999) . '_transaction.' . $extension; // renameing
            $fileName_payment = date('d-m-Y') . '_' . rand(11111, 99999) . '_payment.' . $extension_payment; // renameing

            $request->file('csv_file')->move($destinationPath, $fileName); // uploading file to given path
            // sending back with message

            $request->file('payment_file')->move($destinationPath, $fileName_payment); // uploading file to given path
            // sending back with message

            $ress = Excel::load($destinationPath . '/' . $fileName, function ($reader) {
                        $reader->all();
                    })->get();

            $ress_payment = Excel::load($destinationPath . '/' . $fileName_payment, function ($reader) {
                        $reader->all();
                    })->get();

            $this->earningPointPost($ress, $ress_payment, $fileName, $fileName_payment);

            Session::flash('success', 'Earning Point Proccess Success');
            $data = array();
            $data['earning'] = $ress;

            return view('earningvoid.data', $data);
        } else {
            Session::flash('error', 'Please Upload CSV file extension for Transaction and Payment');
            return view('earningvoid.index');
        }
    }

    public function earningPointPost($data = array(), $data_payment = array(), $fileName = null, $fileName_payment = null, $command = false) {


        $DOC_NO = 'docs.' . date('Y-m-d H:i:s');

        if (!empty($data_payment)) {
            if (!$command) {
                DB::table('POSTTEMPPAYMENT')->delete();
                $this->prosesTempPayment($data_payment, $fileName_payment);
            }
        }
        if (!empty($data)) {
            if (!$command) {
                DB::table('POSTTEMP')->delete();
                $this->prosesTempSales($data, $fileName);
            }
        }

        $newErn = PostTemp::select('CARDID', DB::raw('SUM(TOTALAFTERDISC) as total'), DB::raw('SUM(POINT_BONUS) as total_point_bonus'), DB::raw('SUM(SPLIT_BONUS) as total_split_bonus'))
                ->groupBy('CARDID');



        foreach ($newErn->get() as $row) {

            $bonus_combine_fix = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                    ->where('CARDID', $row->CARDID)
                    ->where('MULTIPLE', 0)
                    ->orderBy('point', 'DESC')
                    ->groupBy('INTCODE')
                    ->first();

            $bonus_combine_multiple = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                    ->where('CARDID', $row->CARDID)
                    ->where('MULTIPLE', 1)
                    ->orderBy('point', 'DESC')
                    ->groupBy('INTCODE')
                    ->first();


            $bonus_payment_fix = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                    ->where('CARDID', $row->CARDID)
                    ->where('MULTIPLE', 0)
                    ->groupBy('TENDNBR')
                    ->orderBy('point', 'DESC')
                    ->first();

            $bonus_payment_multiple = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                    ->where('CARDID', $row->CARDID)
                    ->where('MULTIPLE', 1)
                    ->groupBy('TENDNBR')
                    ->orderBy('point', 'DESC')
                    ->first();


            $bonus_payment = (isset($bonus_payment_fix->point) ? $bonus_payment_fix->point : 0) + (isset($bonus_payment_multiple->point) ? $bonus_payment_multiple->point : 0);


            $bonus_combine_fix_point = (isset($bonus_combine_fix->point) ? $bonus_combine_fix->point : 0);
            $bonus_combine_fix_split = (isset($bonus_combine_fix->split) ? $bonus_combine_fix->split : 0);
            $bonus_payment_point = (isset($bonus_payment) ? $bonus_payment : 0);

            $bonus_combine_multiple_point = (isset($bonus_combine_multiple->point) ? $bonus_combine_multiple->point : 0);
            $bonus_combine_multiple_split = (isset($bonus_combine_multiple->split) ? $bonus_combine_multiple->split : 0);

            $point = $bonus_combine_fix_point + $bonus_combine_multiple_point + $bonus_payment_point;
            $split = $bonus_combine_fix_split + $bonus_combine_multiple_split;

            $totalAmount = (isset($bonus_combine_multiple->total) ? $bonus_combine_multiple->total : 0) + (isset($bonus_combine_fix->total) ? $bonus_combine_fix->total : 0);
            $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
            $minimum_transaction = $preference['value'];




            $sumErn = $row->total;

            $customer = Customer::where('CUST_BARCODE', $row->CARDID)->first();
            $postTemp = PostTemp::where('CARDID', $row->CARDID)->first();  // tambah where FILE = .....
            $postTempAll = PostTemp::where('CARDID', $row->CARDID)->get(); // tambah where FILE = .....
            
			
            
            try{
                 $bonus_grouping = $this->_checkBonusPointGroup($postTemp, $customer, $sumErn);
            }
             catch (\Exception $e) {
                
                 $bonus_grouping = array('point' => 0, 'split' => 0, 'multiple' => 0, 'pf' => 0, 'group' => 0);
                 
                $this->writeLog(" Check Bonus Point Failed, with message : {$e->getMessage()}", env('DEBUG_EARNING_LOG', false));
            }
            
            echo PHP_EOL;
            echo "--------------Bonus Point Cek-----------------".PHP_EOL;
            print_r($bonus_grouping);
            echo "-------------------------------".PHP_EOL;
            echo PHP_EOL;
            
            
            
            
            $ld_grouping = $this->_checkLDPointGroup($postTemp, $customer, $sumErn);

            $split_grouping = $bonus_grouping['split'];

            $sumErn_split = $sumErn - $split - $split_grouping;


            $temp_paymentMethod = PostTempPayment::where('TENDNBR', $row->TENDNBR)->first(); // tambah where FILE = .....

            $ress_point_regular = $this->_checkRegularPoint($postTemp, $sumErn_split, $customer, $temp_paymentMethod);
            $ress_point_regular_nonsplit = $this->_checkRegularPoint($postTemp, $sumErn, $customer, $temp_paymentMethod);

            if(env('DEBUG_BONUS',false)) { echo'\n ress point '. $ress_point_regular['point']; }

            $point_regular = ($ress_point_regular['point'] == NULL) ? 0 : $ress_point_regular['point'];
            $point_regular_nonsplit = ($ress_point_regular_nonsplit['point'] == NULL) ? 0 : $ress_point_regular_nonsplit['point'];

           

            $ld = 0;
            $get_reward = $this->_checkRewardPoint($postTemp, $customer, $sumErn);
            $point_reward = $get_reward['point_fix'] + ($get_reward['point_multiple'] * $point_regular_nonsplit);

            if (isset($customer->CUST_RECID)) {

                $tenant = Tenant::where('TNT_CODE', $postTemp->STOREID)->first();
                //$product = Product::where('PRO_CODE', $postTemp->INTCODE)->first();
                //$vendors = $product->vendors();
                //  foreach($product->vendors as $val_vendor) { dd($val_vendor);  }
                //$vendor = $vendors[0]; // hanya diambil yang pertama
                // Assign to $row->rcpdate
                //  $postTemp->RCPDATE = date('Y-m-d', strtotime(str_replace('-', '', $postTemp->RCPDATE)));

                if (!env('DEBUG_BONUS', FALSE)) {
                    DB::beginTransaction();
                    try {

                        $counter = Counter::select('post_count')->where('pnum', $this->pnum)->where('ptype', $this->ptype);
                        $getCounter = $counter->first();
                        $RECEIPT_NO = $getCounter->post_count + 1;


                        $counter->update(['post_count' => $RECEIPT_NO]);

                        $post = new Post();
                        $post->POS_RECID = Uuid::uuid();
                        $post->POS_CUST_RECID = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_PNUM = $this->pnum;
                        $post->POS_PTYPE = $this->ptype;
                        $post->POS_STORE = $tenant->TNT_RECID;
                        $post->POS_BARCODE = $row->CARDID;
                        $post->POS_POST_DATE = $postTemp->RCPDATE;
                        $post->POS_POST_TIME = $postTemp->RCPTIME;
                        $post->POS_STATION_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                        $post->POS_SHIFT_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                        $post->POS_DOC_NO = $postTemp->RCPNBR;
                        $post->POS_CASHIER_ID = $postTemp->TERMID;
                        $post->POS_AMOUNT = $sumErn;
                        $post->POS_POINT_LD = $ld;
                        $post->POS_POINT_REGULAR = $point_regular;
                        $post->POS_POINT_REWARD = $point_reward;
                        $post->POS_POINT_BONUS = $point + $bonus_grouping['point'];
                        $post->POS_RECEIPT_TYPE = '1';
                        $post->POS_USERBY = (isset(Session::get('data')->USER_DATA->USER_RECID)) ? Session::get('data')->USER_DATA->USER_RECID : 'By System';
                        $post->POS_CREATEBY = '-';
                        $post->POS_UPDATE = 1;
                        $post->POS_TYPE = 0;
                        $post->POS_TRANSF_FROM = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_TRANSF_TO = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                        $post->POS_RECEIPT_NO = 'EA/' . date('y', strtotime($postTemp->RCPDATE)) . '/' . sprintf("%'.07d\n", $RECEIPT_NO);
                        $post->TENDNBR = (isset($postTemp->payment->TENDNBR)) ? $postTemp->payment->TENDNBR : 0;
                        $post->BIN = (isset($postTemp->payment->BIN)) ? $postTemp->payment->BIN : 0;
                        $post->POS_VOID = 0;
                        $post->save();
                        $this->writeLog("-- POST {$row->CARDID} Saved --", env('DEBUG_EARNING_LOG', false));

                        $this->writeLog(" [" . $post->POS_POST_DATE . "|" . $post->POS_POST_TIME . "] Store : " . $post->POS_STORE . " Customer : " . $post->POS_BARCODE, env('DEBUG_EARNING_LOG', false));
                        $this->writeLog(" Total Amount : ".$sumErn, env('DEBUG_EARNING_LOG', false));
                        $this->writeLog(" | Regular Point : " . $post->POS_POINT_REGULAR . " | Reward Point : " . $post->POS_POINT_REWARD . " | Bonus Point : " . $post->POS_POINT_BONUS . " | ", env('DEBUG_EARNING_LOG', false));
                        $this->writeLog(" ======================= ", env('DEBUG_EARNING_LOG', false));

                        $totalPoint = ZTotalPoint::where('TTL_CUST_RECID', $customer->CUST_RECID)->redeemPoint()->first();
                        $resultPoint = $point_regular + $point + $bonus_grouping['point'] + $point_reward;
                        if ($totalPoint) {
                            $totalPoint->TTL_POINT = $totalPoint->TTL_POINT + $resultPoint;
                            $totalPoint->save();
                            $this->writeLog("-- Total Point {$row->CARDID} Saved --", env('DEBUG_EARNING_LOG', false));
                        } else {
                            $totalPoint = new ZTotalPoint;
                            $totalPoint->TTL_RECID = Uuid::uuid();
                            $totalPoint->TTL_PNUM = $this->pnum;
                            $totalPoint->TTL_PTYPE = $this->ptype;
                            $totalPoint->TTL_CUST_RECID = $customer->CUST_RECID;
                            $totalPoint->TTL_CODE = 2;
                            $totalPoint->TTL_POINT = $resultPoint;
                            $totalPoint->save();
                            $this->writeLog("-- Total Point {$row->CARDID} Saved --", env('DEBUG_EARNING_LOG', false));
                        }
                        
                        $customer_earn = $customer->CUST_EXPENSE_EARN;
                        $customer->CUST_EXPENSE_EARN = $customer_earn+$sumErn;
                        $customer->save(); //save new CUST_EXPENSE_EARN
                       
                        try {
                        foreach($postTempAll as $row_postTemp) { 
                        
                        $product = Product::where('PRO_CODE',$row_postTemp->INTCODE)->first();    
                            
                        $postDetail = new PostDetail();
                        $postDetail->PSD_RECID = Uuid::uuid();
                        $postDetail->PSD_POS_RECID = $post->POS_RECID;
                        $postDetail->PSD_STORE = $tenant->TNT_RECID;
                        $postDetail->PSD_FORMULA_ID = $product->PRO_RECID;  // Hanya menyimpan Formula dari Bonus Point Formula by Product / Merchan.
                        $postDetail->PSD_STATION_ID = $post->POS_STATION_ID;
                        $postDetail->PSD_SHIFT_ID = $post->POS_SHIFT_ID;
                        $postDetail->PSD_DOC_NO = $post->POS_DOC_NO;
                        $postDetail->PSD_TRAN_DATE = $post->POS_POST_DATE;
                        $postDetail->PSD_TRAN_TIME = $post->POS_POST_TIME;
                        $postDetail->PSD_TYPE = 0;  // ??
                        $postDetail->PSD_SUBTYPE = 0;  // ??
                        $postDetail->PSD_VENDOR = $product->PRO_RECID;
                        $postDetail->PSD_ARTICLE = $product->PRO_RECID;
                        $postDetail->PSD_QTY = $row_postTemp->ITEMQTY;  // harus SUM QTY ??
                        $postDetail->PSD_AMOUNT = $row_postTemp->TOTALAFTERDISC;
                        $postDetail->PSD_POINT = $post->POS_POINT_BONUS;  //
                        $postDetail->PSD_PAYMENT = $product->PRO_RECID;  // harus menyimpan payment method recid
                        // $postDetail->PSD_BANK = $payment;  // harus menyimpan payment method recid
                        $postDetail->POS_VOID = 0;  //
                        $postDetail->PSD_UPDATE = 0;  //

                        $postDetail->save(); 
                        
                         $this->writeLog("-- Article {$row_postTemp->INTCODE} Saved In Post Detail --", env('DEBUG_EARNING_LOG', false));
                        } }
                        catch (\Exception $e) {
                          $this->writeErrorLog("Error Save to Post Detail with Message : {$e->getMessage()}");
                         }

                        // jika didapatkan lucky draw
                        if($ld_grouping['point']>0) {

                            // looping sesuai jumlah LD yang didapat
                            foreach($ld_grouping['point'] as $row) {

                                $getInc = PointRandom::select('RAND_RECID')->count();
                                $kode = $this->_getLDNumber($getInc,$tenant->TNT_RECID,$post->POS_DOC_NO);

                                $pointRandom = new PointRandom();
                                $pointRandom->RAND_RECID = Uuid::uuid();
                                $pointRandom->RAND_CUST_RECID = $customer->CUST_RECID;
                                $pointRandom->RAND_POS_RECID = $post->POS_RECID;
                                $pointRandom->RAND_TYPE = 1;
                                $pointRandom->RAND_POINT_1 = $kode;   //  incremental (4 digit) ,kode store (4 digit), receipt no (5)
                                $pointRandom->RAND_STS = 0;
                                $pointRandom->RAND_WIN = 0;
                                $pointRandom->save();
                            }

                        }


                        DB::commit();
                        $this->writeLog("-- Post Detail Saved --\n", env('DEBUG_EARNING_LOG', false));
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $this->writeErrorLog("Error Save to DB with Message : {$e->getMessage()}");
                    }
                }
            }
        }

        if (env('DEBUG_BONUS', FALSE)) {
           // exit;
        }

        DB::table('POSTTEMP')->delete();
        DB::table('POSTTEMPPAYMENT')->delete();
    }

    public function _getLDNumber($getInc,$kode_store,$rcp_number) {

        $getIncPlus = $getInc+1;
        $inc = str_pad($getIncPlus, 4, "0", STR_PAD_LEFT);

        $kode = $inc.$kode_store.$rcp_number;
        $checkInc = PointRandom::select('RAND_POINT_1')->where('RAND_POINT_1',$kode)->count();

        if($checkInc>0) {
            $this->_getLDNumber($inc,$kode_store,$rcp_number);
        }
        else {
            return $kode;
        }


    }

    public function _checkRegularPoint($row, $sumErn, $customer, $temp_paymentMethod = null) {
        $point = 0;
        $pf = array();
        $formula = FormulaEarn::where('ERN_TYPE', 0)
                ->where('ERN_FORMULA', 0) // regular point
                ->where('ERN_FORMULA_DTL', 0)
                ->where('ERN_ACTIVE', 1) // active
                ->where('ERN_VALUE', 0)
                ->where('ERN_FRDATE', '<=', $row->RCPDATE) // from date
                ->where('ERN_TODATE', '>=', $row->RCPDATE); // to date
        // to datetime
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

        $formula_check = $formula->get();


        $formula_load = array();

        //check batas waktu (jam, menit, second) jika formula aktif ditanggal awal dan tanggal ahir
        foreach ($formula_check as $row_check) {
            if (!empty($row_check) && $row_check->ERN_AMOUNT != 0) { // jika dapet point
                if ($row->RCPDATE == $row_check->ERN_FRDATE or $row->RCPDATE == $row_check->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                    $formula_time = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME) // from datetime
                            ->where('ERN_TOTIME', '>=', $row->RCPTIME);
                    $row_check_time = $formula_time->first();
                    if (count($row_check_time) > 0) {
                        $formula_load[] = $row_check;
                    }
                } else {
                    $formula_load[] = $row_check;
                }
            }
        }



        $best_point = array(); // untuk mencari best point

        if (!empty($formula_load)) {


            foreach ($formula_load as $formula_ress) {

                if (isset($formula_ress->ERN_AMOUNT)) {

                    $temp1 = (float) $sumErn;
                    $temp2 = (float) $formula_ress->ERN_AMOUNT;

                    if ($formula_ress->ERN_MULTIPLE_BASIC == 1) { // jika multiple basic ON
                        $pointamount = floor(@($temp1 / $temp2));
                        // ex : 95.000/50.000 = 1
                        $point = $pointamount * $formula_ress->ERN_POINT;
                        //jika normal point
                    } else {
                        if ($sumErn >= $formula_ress->ERN_AMOUNT) {
                            $point = 1 * $formula_ress->ERN_POINT;
                        } else {
                            $point = 0;
                        }
                    }


                    // Min Max Amount
                    if ($formula_ress->ERN_ACTIVE_MAX == 1) {
                        if ($sumErn < $formula_ress->ERN_MIN_AMOUNT) { //jika Belanja Kurang Dari Minimum Amount
                            $point = $point;
                        }

                        if ($sumErn >= $formula_ress->ERN_MIN_AMOUNT && $sumErn <= $formula_ress->ERN_MAX_AMOUNT) {
                            //jika belanja diantara rentang Min Max
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {
                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;
                            }
                        }

                        if ($sumErn > $formula_ress->ERN_MAX_AMOUNT && $formula_ress->ERN_MAX_AMOUNT != 0) { //jika Belanja Lebih Dari Max Amount
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {

                                $moreErn = $sumErn - $formula_ress->ERN_MAX_AMOUNT;
                                $moreAmount = floor(@($moreErn / $formula_ress->ERN_AMOUNT));
                                $pointamount = floor(@($formula_ress->ERN_MAX_AMOUNT / $formula_ress->ERN_AMOUNT));
                                $morePoint = $moreAmount * $formula_ress->ERN_POINT;
                                $point_inmax = ($pointamount * $formula_ress->ERN_POINT) * $formula_ress->ERN_MULTIPLE_VALUE;
                                $point = $point_inmax + $morePoint;
                                //point < Max dikalikan multipler dan yang > max ($morePoint) diberikan normal point kemudian keduanya dijumlahkan
                                //                        echo 'Total Belanja = ' . $sumErn . '<br/>';
                                //                        echo 'Max = ' . $formula_ress->ERN_MAX_AMOUNT . '<br/>';
                                //                        echo 'Min = ' . $formula_ress->ERN_MIN_AMOUNT . '<br/>';
                                //                        echo 'Sisa Total Belanja = ' . $moreAmount . '<br/>';
                                //                        echo 'Point Dalam Max = ' . '(' . $pointamount . '*' . $formula_ress->ERN_POINT . ') *' . $formula_ress->ERN_MULTIPLE_VALUE . ' = ' . $point_inmax . '<br/>';
                                //                        echo 'Point Lebih = ' . $moreAmount . ' * ' . $formula_ress->ERN_POINT . ' = ' . $morePoint . '<br/>';
                                //                        echo 'Total Point = ' . $point;
                                //                        exit;
                            }
                        }
                    }

                    if ($formula_ress->ERN_ACTIVE_MAXAMOUNT == 1) { // jika MAX AMOUNT active
                        if ($formula_ress->ERN_ACTIVE_REST == 1) { // jika REST AMOUNT active
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $temp3 = $sumErn - $formula_ress->ERN_MAXAMOUNT_VALUE; //selisih lebih

                                $pointamount = floor(@($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2));
                                $point = $pointamount * $formula_ress->ERN_POINT;

                                $pointamountress = floor(@($temp3 / $formula_ress->ERN_AMOUNT_REST));
                                $pointress = $pointamountress * $formula_ress->ERN_POINT_REST;

                                $point = $point + $pointress; //point normal + point ress
                            }
                        } elseif ($formula_ress->ERN_ACTIVE_REST == 0) { // jika REST AMOUNT inactive
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $pointamount = floor(@($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2));
                                $point = $pointamount * $formula_ress->ERN_POINT;
                            }
                        }
                    }


                    if (isset($customer->CUST_GENDER)) {
                        switch ($customer->CUST_GENDER) {
                            case 'M':
                                $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                                break;
                            case 'F':
                                $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                                break;
                        } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
                    }

                    if ($customer->CUST_GENDER == null) {
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'GEN')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_GENDER = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_BLOOD == null) {
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'BLOD')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_BLOOD = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_RELIGION == null) {
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'RELI')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_RELIGION = $lookUp->LOK_RECID; //other
                    }

                    $formulaMember = FormulaEarnMember::where('MEM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('MEM_LOK_RECID', '=', $customer->CUST_MEMTYPE)->get();
                    $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
                    $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
                    $formulaCardType = FormulaEarnCardType::where('CDTY_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CDTY_LOK_RECID', '=', $customer->CUST_CARDTYPE)->get();
                    //card type di point formula diubah refer ke z_cardtype bukan ke z_lookup kembali

                    if ($temp_paymentMethod != null) {
                        $payment_method_recid = PaymentMethod::where('code', $temp_paymentMethod)->first();
                        $formulaCardPayment = FormulaEarnPayment::where('PAY_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('PAY_LOK_RECID', '=', $payment_method_recid['id'])->get();
                        if (count($formulaCardPayment) < 1) {
                            $point = 0; //gender tidak ditemukan
                            echo 'payment tidak ditemukan \n';
                        }
                    }

                    //formula ern store belum dibuat model dan table nya

                    $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();
                    $tenant = Tenant::where('TNT_CODE', $row->STOREID)->first();
                    $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                    ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                    ->where('TENT_ACTIVE', 1)->get();

                    echo '\n';
                    if (count($formulaTenant) < 1) {
                        $point = 0; //memtype tidak ditemukan
                         echo 'store tidak ditemukan \n';
                    }

                    if (count($formulaMember) < 1) {
                        $point = 0; //memtype tidak ditemukan
                        echo 'member type tidak ditemukan \n';
                    }
                    if (count($formulaGender) < 1) {
                        $point = 0; //gender tidak ditemukan
                        echo 'gender tidak ditemukan \n';
                    }
                    if (count($formulaReligion) < 1) {
                        $point = 0; //gender tidak ditemukan
                         echo 'religion tidak ditemukan \n';
                    }
                    if (count($formulaCardType) < 1) {
                        $point = 0; //Card Type tidak ditemukan
                        echo 'card type tidak ditemukan \n';
                    }
                    if (count($formulaBlood) < 1) {
                        $point = 0; //gender tidak ditemukan
                         echo 'blood tidak ditemukan \n';
                    }


                    if (isset($point) && $point < 0) {
                        $point = 0;
                    }

                    if ($point == null) {
                        $point = 0;
                    }
                    echo PHP_EOL.' point';
                    echo PHP_EOL.' ------------- '.$point.'';

                    $best_point[] = $point;

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                    $pf[] = $pf_founded;
                }
            }
        }



        $return_point = 0;

        if (count($best_point) != 0) {
            $return_point = max($best_point);
        }
             // echo '\n return point = '.$return_point.' PF : '.$pf;
               echo PHP_EOL.' ------------- ';
        return ['point' => $return_point, 'pf' => $pf];
    }

    public function _checkBonusPoint($row) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();

        $pf = array();
        $TOTALAFTERDISC = $row[14 - env('LOCALARRAY', 0)];
        $ITEMQTY = $row[10 - env('LOCALARRAY', 0)];



        $group['brand_qty'] = 0;
        $group['brand_amount'] = 0;
        $group['vendor_qty'] = 0;
        $group['vendor_amount'] = 0;
        $group['dept_qty'] = 0;
        $group['dept_amount'] = 0;
        $group['div_qty'] = 0;
        $group['div_amount'] = 0;
        $group['cat_qty'] = 0;
        $group['cat_amount'] = 0;
        $group['subcat_qty'] = 0;
        $group['subcat_amount'] = 0;
        $group['subcat_qty'] = 0;
        $group['subcat_amount'] = 0;
        $group['segment_qty'] = 0;
        $group['segment_amount'] = 0;
        $group['vendor_qty'] = 0;
        $group['vendor_amount'] = 0;



        $bonus_type = [
            9 => 'All Product'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point
		
		 $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
                     $minimum_transaction = $preference['value'];

        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::
                    where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_TODATE', '>=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_FORMULA', 1);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();

            if (!empty($formula_load)) {
                foreach ($formula_load as $formula_ress) {
                    $point = 0;
                    $check_time = TRUE;

                    if ($row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_FRDATE OR $row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row[3 - env('LOCALARRAY', 0)])  // from datetime
                                ->where('ERN_TOTIME', '>=', $row[3 - env('LOCALARRAY', 0)])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                    }

                    if (isset($row[7 - env('LOCALARRAY', 0)]) && $row[7 - env('LOCALARRAY', 0)] != '') {

                        if ($key_bonus_type == 1 AND $check_time == TRUE) { // jika produk
                            //Product
                            $product = Product::select('PRO_CODE', 'PRO_RECID')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('ITM_PRODUCT', '=', $product['PRO_RECID'])->get();
                        } elseif ($key_bonus_type == 2 and $check_time == true) { // jika brand
                            //Brand
                            $product = Product::select('PRO_CODE', 'PRO_DESCR', 'PRO_RECID', 'brand_id')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $brands = Brand::select('id', 'code', 'name')->where('id', $product['brand_id'])->first();
                            $formulaItem = FormulaEarnBrand::where('BRAND_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BRAND_BRAND_RECID', '=', $brands['id'])->get();

                            if (count($formulaItem) > 0) {
                                $group['brand_qty'] = $group['brand_qty'] + $ITEMQTY;
                                $group['brand_amount'] = $group['brand_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['brand_qty'] = 0;
                                $group['brand_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                            //Department
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnDepartment::where('DEPT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DEPT_DEPARTMENT', '=', $merchant->department->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {
                                $group['dept_qty'] = $group['dept_qty'] + $ITEMQTY;
                                $group['dept_amount'] = $group['dept_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['dept_qty'] = 0;
                                $group['dept_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                            //Division
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnDivision::where('DIV_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DIV_DIVISION', '=', $merchant->division->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['div_qty'] = $group['div_qty'] + $ITEMQTY;
                                $group['div_amount'] = $group['div_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['div_qty'] = 0;
                                $group['div_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                            //Category
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnCategory::where('CAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CAT_CATEGORY', '=', $merchant->category->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {
                                $group['cat_qty'] = $group['cat_qty'] + $ITEMQTY;
                                $group['cat_amount'] = $group['cat_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['cat_qty'] = 0;
                                $group['cat_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                            //Departement
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnSubCategory::where('SCAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SCAT_SUBCATEGORY', '=', $merchant->subcategory->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['subcat_qty'] = $group['subcat_qty'] + $ITEMQTY;
                                $group['subcat_amount'] = $group['subcat_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['subcat_qty'] = 0;
                                $group['subcat_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                            //Departement
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnSegment::where('SEG_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SEG_SEGMENT', '=', $merchant->segment->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['segment_qty'] = $group['segment_qty'] + $ITEMQTY;
                                $group['segment_amount'] = $group['segment_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['segment_qty'] = 0;
                                $group['segment_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                            $product = Product::select('PRO_CODE', 'PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row[7 - env('LOCALARRAY', 0)])->first();
                            $merchant = $product->vendors()->get();
                            $countVendor = array();
                            foreach ($merchant as $row_vendor) {
                                $formulaVendor = FormulaEarnVendor::where('VND_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('VND_VENDOR_RECID', '=', $row_vendor['PROV_VDR_RECID'])->get();
                                if (count($formulaVendor) > 0) {
                                    $countVendor[] = $row_vendor['PROV_VDR_RECID'];
                                }
                            }

                            $formulaItem = $countVendor;

                            if (count($formulaItem) > 0) {
                                $group['vendor_qty'] = $group['vendor_qty'] + $ITEMQTY;
                                $group['vendor_amount'] = $group['vendor_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['vendor_qty'] = 0;
                                $group['vendor_amount'] = 0;
                            }
                        }

                        if ($key_bonus_type == 9 AND $check_time == TRUE) {

                            $formulaItem = array(0 => 'All Product', 1 => 'All Product');
                        }
                    }


                    if (count($formulaItem) > 0) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount <br/>' : 'by quantity <br/>';
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = $TOTALAFTERDISC - $sisa_bagi; // jika terdapat sisa bagi,, akan ditampung di split
                                    $best_point_multiple[] = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                          $ITEMQTY_POINT = $ITEMQTY;
                                        
                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
                                        elseif($TOTALAFTERDISC < $formula_ress->ERN_AMOUNT) {
                                            $point = 0;
                                        }
                                        if($TOTALAFTERDISC<$minimum_transaction) {
                                                     $point = 0;
                                            }
										
										
                                        $best_point_fix[] = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);
                                    }
                                }
                            }
                        }





                        if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                            // $row[10 - env('LOCALARRAY', 0)] = $row[10 - env('LOCALARRAY', 0)] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;

                                $best_point_fix[] = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);
                            }

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                            }
                        }
                    }


                    $point = $this->_filterTransaction($row[5 - env('LOCALARRAY', 0)], $formula_ress->ERN_RECID, $row[1 - env('LOCALARRAY', 0)], $point,$row[2 - env('LOCALARRAY', 0)]);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }

                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = max($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;

                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }

        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }

    public function _checkBonusPayment($row) {

        $pf = array();
        $point = 0;
        // $split = 0;
        $last_poin = 0;
        $bonus_type = [
            0 => 'Payment'
        ];

        $multiple = 0;

        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA', 1)
                    ->where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row[2 - env('LOCALARRAY', 0)])
                    ->where('ERN_TODATE', '>=', $row[2 - env('LOCALARRAY', 0)]);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();

            if (!empty($formula_load)) {

                foreach ($formula_load as $formula_ress) {
                    //$point = 0;
                    $check_time = true;

                    if ($row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_FRDATE or $row[2 - env('LOCALARRAY', 0)] == $formula_ress->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row[3 - env('LOCALARRAY', 0)]) // from datetime
                                ->where('ERN_TOTIME', '>=', $row[3 - env('LOCALARRAY', 0)])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula > first())) ? true : false;
                    }

                    if ($key_bonus_type == 0 and $check_time == true) { // jika payment
                        $payment_method = PaymentMethod::where('code', 0)->first();
                        $formulaItem = FormulaEarnPayment::where('PAY_ERN_RECID', $formula_ress->ERN_RECID)->where('PAY_LOK_RECID', $payment_method['id'])->get();
                    }

                    if (count($formulaItem) > 0) {

                        $multiple = $formula_ress->ERN_MULTIPLE;

                        if ($formula_ress->ERN_VALUE == 0) { // By Amount
                            $pointamount = floor(@($row[8 - env('LOCALARRAY', 0)] / $formula_ress->ERN_AMOUNT));
                            $point = $pointamount * $formula_ress->ERN_POINT;
                        }

                         if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = $TOTALAFTERDISC - $sisa_bagi; // jika terdapat sisa bagi,, akan ditampung di split
                                    if($TOTALAFTERDISC<$minimum_transaction) {
                                                     $point = 0;
                                            }
                                    $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;
                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
                                        if($TOTALAFTERDISC<$minimum_transaction) {
                                                     $point = 0;
                                            }
                                        $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                    }
                                }
                            }
                    } else {
                        $point = 0;
                    }


                    $customer = Customer::where('CUST_BARCODE', $row[6 - env('LOCALARRAY', 0)])->first();

                    if (isset($customer->CUST_GENDER)) {
                        switch ($customer->CUST_GENDER) {
                            case 'M':
                                $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                                break;
                            case 'F':
                                $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                                break;
                        } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
                    }

                    if ($customer->CUST_GENDER == null) {
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'GEN')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_GENDER = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_BLOOD == null) {
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'BLOD')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_BLOOD = $lookUp->LOK_RECID; //other
                    }
                    if ($customer->CUST_RELIGION == null) {
                        $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'RELI')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                        $customer->CUST_RELIGION = $lookUp->LOK_RECID; //other
                    }

                    $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
                    $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
                    $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();

                    $tenant = Tenant::where('TNT_CODE', $row[1 - env('LOCALARRAY', 0)])->first();
                    $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                    ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                    ->where('TENT_ACTIVE', 1)->get();

                    if (count($formulaTenant) < 1) {
                        $point = 0; //memtype tidak ditemukan
                        // echo 'member type tidak ditemukan <br/>';
                    }

                    if (count($formulaGender) < 1) {
                        $point = 0;
                        //echo 'gender tidak ditemukan'; exit;
                    }
                    if (count($formulaReligion) < 1) {
                        $point = 0;
                        //  echo 'religion tidak ditemukan <br/>';
                    }
                    if (count($formulaBlood) < 1) {
                        $point = 0;
                        //  echo 'blood tidak ditemukan <br/>';
                    }
                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                    $pf[] = $pf_founded;
                    // echo $point.'<br/>';
                    $last_poin = $last_poin + $point;
                }

                if ($last_poin < 0) {
                    $last_poin = 0;
                }
            }
        }

        $return = array('point' => $last_poin, 'multiple' => $multiple, 'pf' => $pf);



        return $return;
    }

    public function _checkBonusPointGroup($row, $customer, $sumErn) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();


        $pf = array();


        $bonus_type = [
            1 => 'Point Earn by Product',
            2 => 'Point Earn Bonus by Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendors',
			9 => 'All Products'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point
        
                     $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
                     $minimum_transaction = $preference['value'];
        
        if($row!=NULL) {             
        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row['RCPDATE'])
                    ->where('ERN_TODATE', '>=', $row['RCPDATE'])
                    ->where('ERN_FORMULA', 1);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();



            if (!empty($formula_load)) {
                $no = 0;
                foreach ($formula_load as $formula_ress) {
                    $no++;
                    $point = 0;
                    $check_time = TRUE;

                    if ($row['RCPDATE'] == $formula_ress->ERN_FRDATE OR $row['RCPDATE'] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir
                        $formula_check = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME'])  // from datetime
                                ->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula_check->first()) ) ? TRUE : FALSE;
                    }


                    if ($key_bonus_type == 1 AND $check_time == TRUE) { // jika produk
                            //Product
                       
                        $ERN_RECID = $formula_ress->ERN_RECID;
                         $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->get();
                  

                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_product) {

                            $formulaItem_product = Product::select('*')->where('PRO_RECID', $formulaItem_product->ITM_PRODUCT)->get();
                        
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // Mendapatkan product di table POSTEMP
                            // tambah where FILE = .....
                            $rowGroup[] = array(
                                'product_name' => '',
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            
                        } 
                        
                     
                            
                    }
                    elseif ($key_bonus_type == 2 and $check_time == true) { // jika brand
                        //Brand
                        $ERN_RECID = $formula_ress->ERN_RECID;

                        $formulaItem = $formula_ress->brand;

                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_brand) {

                            $formulaItem_product = Product::select('*')->where('brand_id', $formulaItem_brand->brand->id)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // Mendapatkan product dari Brand terkait di table POSTEMP
                            // tambah where FILE = .....
                            $rowGroup[] = array('brand' => $formulaItem_brand->brand->id,
                                'brand_name' => $formulaItem_brand->brand->name,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                        //Department
                        $formulaItem = $formula_ress->department;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_department) {
                            $merchant = Merchandise::where('MERC_DEPARTMENT', $formulaItem_department->DEPT_DEPARTMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_department->department->LOK_RECID,
                                'merchand_name' => $formulaItem_department->department->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                        //Division
                        $formulaItem = $formula_ress->division;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_division) {
                            $merchant = Merchandise::where('MERC_DIVISION', $formulaItem_division->DIV_DIVISION)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_division->division->LOK_RECID,
                                'merchand_name' => $formulaItem_division->division->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                        $formulaItem = $formula_ress->category;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_category) {
                            $merchant = Merchandise::where('MERC_CATEGORY', $formulaItem_category->CAT_CATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_category->category->LOK_RECID,
                                'merchand_name' => $formulaItem_category->category->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                        $formulaItem = $formula_ress->subcategory;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_subcategory) {
                            $merchant = Merchandise::where('MERC_SUBCATEGORY', $formulaItem_subcategory->SUBCAT_SUBCATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_subcategory->subcategory->LOK_RECID,
                                'merchand_name' => $formulaItem_subcategory->subcategory->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                        $formulaItem = $formula_ress->segment;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_segment) {
                            $merchant = Merchandise::where('MERC_SEGMENT', $formulaItem_segment->SEG_SEGMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_segment->segment->LOK_RECID,
                                'merchand_name' => $formulaItem_segment->segment->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                        $formulaItem = $formula_ress->vendor;
                        // dd($formulaItem );
                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_vendor) {

                            $vendor = Vendor::where('VDR_RECID', $formulaItem_vendor->VND_VENDOR_RECID)->first(); //dd($vendor);
                            $formulaItem_product = $vendor->products;

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('vendor' => $formulaItem_vendor->vendor->VDR_RECID,
                                'vendor_name' => $formulaItem_vendor->vendor->VDR_NAME,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    }  elseif ($key_bonus_type == 9 AND $check_time == TRUE) { // jika all product
                         $getProduct = array();
						  $getProduct[] = 'All Product'; 
                                                  $rowGroup = array();
						
						$rowGroup[] = array('product name' => 'All',
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->get());
						
                    }



                    if (!isset($rowGroup['total'])) {
                        $rowGroup['total'] = 0;
                    }
                    if (!isset($rowGroup['qty'])) {
                        $rowGroup['qty'] = 0;
                    }

                     
                    // ...
                    $TOTALAFTERDISC = array_sum(array_column($rowGroup, 'total'));
                    $ITEMQTY = array_sum(array_column($rowGroup, 'qty'));
					
					 $this->writeLog(" Total After Disc : ".$TOTALAFTERDISC.$sumErn, env('DEBUG_EARNING_LOG', false));
					 $this->writeLog(" Total QTY Disc : ".$ITEMQTY.$sumErn, env('DEBUG_EARNING_LOG', false));

                    $ITEMPOST = array_sum(array_column($rowGroup, 'post'));

                    echo "Total Amount PF ke ".$no." : ".$TOTALAFTERDISC;


                    if ($TOTALAFTERDISC > 0 and $ITEMQTY > 0) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount <br/>' : 'by quantity <br/>';
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (TRUE) {
                                echo 'Product : \n';
                                //print_r($getProduct);
                                echo '\n';
                                echo  $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = $TOTALAFTERDISC - $sisa_bagi; // jika terdapat sisa bagi,, akan ditampung di split
                                    if($TOTALAFTERDISC<$minimum_transaction) {
                                                     $point = 0;
                                            }
                                    $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;
                                        
                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
                                        elseif($TOTALAFTERDISC < $formula_ress->ERN_AMOUNT) {
                                            $point = 0;
                                        }
                                        if($TOTALAFTERDISC<$minimum_transaction) {
                                                     $point = 0;
                                            }
                                        $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                    }
                                }
                            }
                            
                            
                             if (TRUE) {
                                  echo ''.PHP_EOL;
                               echo 'Product : '.PHP_EOL;
                                print_r($getProduct);
                                echo ''.PHP_EOL;
                                echo 'Minimum Amount = '.$formula_ress->ERN_AMOUNT;
                                echo ''.PHP_EOL;
                                echo  'Total Amount = '.$TOTALAFTERDISC.' Get Point Bonus = '.$point . '   <br/> by Amount<hr/>';
                            }
                            
                        }





                        if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                            // $row['ITEMQTY'] = $row['ITEMQTY'] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;
                                if($TOTALAFTERDISC<$minimum_transaction) {
                                                     $point = 0;
                                            }
                                $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                            }

                            if (TRUE) {
                               echo 'Product : \n';
                                print_r($getProduct);
                                echo '\n';
                                echo  $point . ' = ' .  $formula_ress->ERN_POINT . ' <br/> by QTY<hr/>';
                            }
                        }
                    } 

                    echo PHP_EOL." Total Amount PF ke ".$no." : ".$TOTALAFTERDISC.' ';
                    echo PHP_EOL." With Minimum trans : ".$minimum_transaction.' ';
					 echo PHP_EOL;

                    $point = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }



                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = array_sum($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;



                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }  }
       
		

        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }


     public function _checkLDPointGroup($row, $customer, $sumErn) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();


        $pf = array();


        $bonus_type = [
            2 => 'Point Earn LD by Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendors'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point


        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row['RCPDATE'])
                    ->where('ERN_TODATE', '>=', $row['RCPDATE'])
                    ->where('ERN_FORMULA', 3);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();



            if (!empty($formula_load)) {
                foreach ($formula_load as $formula_ress) {
                    $point = 0;
                    $check_time = TRUE;

                    if ($row['RCPDATE'] == $formula_ress->ERN_FRDATE OR $row['RCPDATE'] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME'])  // from datetime
                                ->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                    }



                    if ($key_bonus_type == 2 and $check_time == true) { // jika brand
                        //Brand
                        $ERN_RECID = $formula_ress->ERN_RECID;

                        $formulaItem = $formula_ress->brand;

                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_brand) {

                            $formulaItem_product = Product::select('*')->where('brand_id', $formulaItem_brand->brand->id)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            // Mendapatkan product dari Brand terkait di table POSTEMP
                            $rowGroup[] = array('brand' => $formulaItem_brand->brand->id,
                                'brand_name' => $formulaItem_brand->brand->name,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                        //Department
                        $formulaItem = $formula_ress->department;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_department) {
                            $merchant = Merchandise::where('MERC_DEPARTMENT', $formulaItem_department->DEPT_DEPARTMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_department->department->LOK_RECID,
                                'merchand_name' => $formulaItem_department->department->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                        //Division
                        $formulaItem = $formula_ress->division;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_division) {
                            $merchant = Merchandise::where('MERC_DIVISION', $formulaItem_division->DIV_DIVISION)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_division->division->LOK_RECID,
                                'merchand_name' => $formulaItem_division->division->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                        $formulaItem = $formula_ress->category;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_category) {
                            $merchant = Merchandise::where('MERC_CATEGORY', $formulaItem_category->CAT_CATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_category->category->LOK_RECID,
                                'merchand_name' => $formulaItem_category->category->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                        $formulaItem = $formula_ress->subcategory;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_subcategory) {
                            $merchant = Merchandise::where('MERC_SUBCATEGORY', $formulaItem_subcategory->SUBCAT_SUBCATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_subcategory->subcategory->LOK_RECID,
                                'merchand_name' => $formulaItem_subcategory->subcategory->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                        $formulaItem = $formula_ress->segment;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_segment) {
                            $merchant = Merchandise::where('MERC_SEGMENT', $formulaItem_segment->SEG_SEGMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('merchand' => $formulaItem_segment->segment->LOK_RECID,
                                'merchand_name' => $formulaItem_segment->segment->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                        $formulaItem = $formula_ress->vendor;
                        // dd($formulaItem );
                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_vendor) {

                            $vendor = Vendor::where('VDR_RECID', $formulaItem_vendor->VND_VENDOR_RECID)->first(); //dd($vendor);
                            $formulaItem_product = $vendor->products;

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // tambah where FILE = .....
                            $rowGroup[] = array('vendor' => $formulaItem_vendor->vendor->VDR_RECID,
                                'vendor_name' => $formulaItem_vendor->vendor->VDR_NAME,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    }



                    if (!isset($rowGroup['total'])) {
                        $rowGroup['total'] = 0;
                    }
                    if (!isset($rowGroup['qty'])) {
                        $rowGroup['qty'] = 0;
                    }


                    // ...
                    $TOTALAFTERDISC = array_sum(array_column($rowGroup, 'total'));
                    $ITEMQTY = array_sum(array_column($rowGroup, 'qty'));

                    $ITEMPOST = array_sum(array_column($rowGroup, 'post'));




                    if ($TOTALAFTERDISC > 0 and $ITEMQTY > 0) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount <br/>' : 'by quantity <br/>';
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = 0;
                                    $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;
                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
										elseif($TOTALAFTERDISC < $formula_ress->ERN_AMOUNT) {
                                            $point = 0;
                                        }
                                            
                                        
                                        if ($TOTALAFTERDISC < $minimum_transaction) {
                                            $point = 0;
                                        }
                                        $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                                    }
                                }
                            }
                        }





                        if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                            // $row['ITEMQTY'] = $row['ITEMQTY'] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;

                                $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);
                            }

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                            }
                        }
                    }


                    $point = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point,$row['RCPDATE']);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }



                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = array_sum($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;



                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }



        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }


    public function _checkRewardPoint($row, $customer, $sumErn) {
        $point = 0;
        $fixed = 0;
        $pf = array();
        $preference = Preference::select('value')->where('key', 'minimum_transaction_reward')->first();
        $minimum_transaction = $preference['value'];

        $formula = FormulaEarn::where('ERN_FORMULA', 2)
                ->where('ERN_ACTIVE', 1)
                ->where('ERN_VALUE', 0)
                ->where('ERN_FRDATE', '<=', $row->RCPDATE)
                ->where('ERN_TODATE', '>=', $row->RCPDATE);  //ketika belanja masuk priode
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

        $formula_check = $formula->get();


        $formula_load = array();

        //check batas waktu (jam, menit, second) jika formula aktif ditanggal awal dan tanggal ahir
        foreach ($formula_check as $row_check) {

            if ($row->RCPDATE == $row_check->ERN_FRDATE or $row->RCPDATE == $row_check->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                $row_check_time = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME) // from datetime
                                ->where('ERN_TOTIME', '>=', $row->RCPTIME)->first();
                if (count($row_check_time) > 0) {
                    $formula_load[] = $row_check;
                }
            } else {
                $formula_load[] = $row_check;
            }
        }

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point

        if (!empty($formula_load)) {


            foreach ($formula_load as $formula_ress) {

                $point = 0;

                if (isset($formula_ress->ERN_FIXED)) {

                    $fixed = $formula_ress->ERN_FIXED;

                    if ($formula_ress->ERN_FORMULA_DTL == 1) {
                        //Member Get Member Reward
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 2) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);
                        //Birthday Reward
                        if ($cust_dob[1] == $rcpdate[1]) { // kondisi Reward on same Month
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 5) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);

                        $bd = $rcpdate[0] . '-' . $cust_dob[1] . '-' . $cust_dob[2];

                        $cust_trans = strtotime($row->RCPDATE); // tanggal transaksi
                        $cust_bd_weekly_start = strtotime($bd); // tanggal awal periode Birthday
                        $cust_bd_weekly_end = strtotime($bd . " +7 days"); // tanggal akhir periode Birthday

                        if ($cust_trans >= $cust_bd_weekly_start and $cust_trans <= $cust_bd_weekly_end) { // kondisi Reward on same Weekly
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 6) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);
                        //Birthday Reward

                        if ($cust_dob[1] == $rcpdate[1] and $cust_dob[2] == $rcpdate[2]) { // kondisi Reward on same Daily
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 3) {
                        //Butler Service Reward
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 4) {
                        // Reward New Registration dan Update Profile on Web Site
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 7) {
                        // Company Anniversary
                        $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 8) {
                        // Store Anniversary

                        $tenant = Tenant::where('TNT_CODE', $row->STOREID)->first();

                        $found = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                        ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                        ->where('TENT_ACTIVE', 1)->get();

                        if (count($found) > 0) {
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    }

                    // Min Max Amount
                    if ($formula_ress->ERN_ACTIVE_MAX == 1) {

                        if ($sumErn < $formula_ress->ERN_MIN_AMOUNT) { //jika Belanja Kurang Dari Minimum Amount
                            $point = 0;
                        }

                        if ($sumErn > $formula_ress->ERN_MAX_AMOUNT) { //jika Belanja Lebih Dari Minimum Amount
                            $point = 0;
                        }
                    }

                    if ($sumErn < $minimum_transaction) {  // jika dibawah minimum transaction
                        $point = 0;
                    }
                }

                if ($point < 0) {
                    $point = 0;
                }

                if ($formula_ress->ERN_FIXED == 1) {
                    $best_point_fix[] = $point;
                } else {
                    $best_point_multiple[] = $point;
                }

                $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                $pf[] = $pf_founded;
            }
        }



        if (count($best_point_fix) != 0) {
            $max_best_point_fix = max($best_point_fix);
        } else {
            $max_best_point_fix = 0;
        }

        if (count($best_point_multiple) != 0) {
            $max_best_point_multiple = max($best_point_multiple);
        } else {
            $max_best_point_multiple = 0;
        }


        $ress = array('point_fix' => $max_best_point_fix, 'point_multiple' => $max_best_point_multiple, 'fixed' => $fixed, 'pf' => $pf);

        return $ress;
    }

        public function _availableDays($date, $id) {
        if ($id !== null) {

            $formulaEarn = FormulaEarn::find($id); // Find Formula ID

            $day = Carbon::createFromFormat('Y-m-d H:i:s', $date); // Parse to Carbon object
            //$day = Carbon::now(); // Parse to Carbon object

            
            //print_r($day->isWednesday()); exit;
            
            if ($formulaEarn->ERN_SUNDAY == 1 && $day->isSunday()) {
                echo PHP_EOL.'Its Sunday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_MONDAY == 1 && $day->isMonday()) {
                echo PHP_EOL.'Its Monday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_TUESDAY == 1 && $day->isTuesday()) {
                echo PHP_EOL.'Its Tuesday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_WEDNESDAY == 1 && $day->isWednesday()) {
                echo PHP_EOL.'Its Wednesday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_THURSDAY == 1 && $day->isThursday()) {
                echo PHP_EOL.'Its Thursday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_FRIDAY == 1 && $day->isFriday()) {
                echo PHP_EOL.'Its Friday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_SATURDAY == 1 && $day->isSaturday()) {
                echo PHP_EOL.'Its Saturday'.PHP_EOL;
                return true;
            } else {
                echo PHP_EOL.'Its Not Avaible Day'.PHP_EOL;
                return false;
            }
        }
        return false;
    }

    private function _checkCustomer($code) {
        $found = Customer::where('CUST_BARCODE', $code)->where('CUST_ACTIVE',1)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function _checkStore($code) {
        $found = Tenant::where('TNT_CODE', $code)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function _checkProduct($code) {
        $found = Product::where('PRO_CODE', $code)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function _checkPaymentMethod($code) {
        $found = PaymentMethod::where('code', $code)->first();

        return (count($found) >= 1) ? TRUE : FALSE;
    }

    private function createUncleanCSVSales($sales, $fileName) {
        $handle = fopen('resources/uploads/earning/unclean/sales/' . $fileName . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($sales as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function createUncleanCSVPayment($payment, $fileName_payment) {
        $handle = fopen('resources/uploads/earning/unclean/payment/' . $fileName_payment . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($payment as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function createCleanCSVSales($sales, $fileName) {
        $handle = fopen('resources/uploads/earning/clean/sales/' . $fileName . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($sales as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function createCleanCSVPayment($payment, $fileName_payment) {
        $handle = fopen('resources/uploads/earning/clean/payment/' . $fileName_payment . '.csv', 'a');
        $no = 0;
        $data = [];
        foreach ($payment as $row) {
            $data[$no] = $row;
            $no++;
        }
        fputcsv($handle, $data);
        fclose($handle);
    }

    private function _logsPF($post_id, $pf, $type) {

        foreach ($pf as $row) {
            DB::beginTransaction();
            try {
                $PostPF = new PostPF();
                $PostPF->POSTF_RECID = Uuid::uuid();
                $PostPF->POSTF_POST_RECID = $post_id;
                $PostPF->POSTF_ERN_RECID = $row['id'];
                $PostPF->POSTF_POINT = $row['point'];
                $PostPF->POSTF_TYPE = $type;
                $PostPF->save();
                DB::commit();
                $this->writeLog("-- Logs PF Saved --", env('DEBUG_EARNING_LOG', false));
            } catch (\Exception $e) {
                DB::rollBack();
                $this->writeErrorLog("Error Save PostPF with Message : {$e->getMessage()}");
            }
        }
    }

    private function _logsPFBonusProses($post_id, $pf, $type, $point) {

        foreach ($pf as $row) {
            DB::beginTransaction;

            try {
                $PostPF = new PostPF();
                $PostPF->POSTF_RECID = Uuid::uuid();
                $PostPF->POSTF_POST_RECID = $post_id;
                $PostPF->POSTF_ERN_RECID = $row['id'];
                $PostPF->POSTF_POINT = $row['point'];
                $PostPF->POSTF_TYPE = $type;
                $PostPF->save();
                DB::commit();
                $this->writeLog("-- Logs PF Bonus Saved --", env('DEBUG_EARNING_LOG', false));
            } catch (\Exception $e) {
                DB::rollBack();
                $this->writeErrorLog("Error Save PostPF Bonus with Message : {$e->getMessage()}");
            }
        }
    }

    private function _logsPFBonus($postBonus, $postPayment, $post_id) {


        $PostPF = PostPF::whereIn('POSTF_POST_RECID', $postBonus);
        $PostPF->update(['POSTF_POST_RECID' => $post_id]);

        $PostPFPay = PostPF::whereIn('POSTF_POST_RECID', $postPayment);
        $PostPFPay->update(['POSTF_POST_RECID' => $post_id]);
    }

    public function _filterTransaction($CARDID, $ERN_RECID, $STOREID, $point,$date) {

        $customer = Customer::where('CUST_BARCODE', $CARDID)->first();
		
		
		if(isset($customer->CUST_BARCODE)) {
			
        if (isset($customer->CUST_GENDER)) {
            switch ($customer->CUST_GENDER) {
                case 'M':
                    $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                    break;
                case 'F':
                    $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                    break;
            } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
        }

		
        if ($customer->CUST_GENDER == null) {
            $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'GEN')->where('LOK_DESCRIPTION', 'OTHERS')->first();
            $customer->CUST_GENDER = $lookUp->LOK_RECID; //other
        }
        if ($customer->CUST_BLOOD == null) {
            $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'BLOD')->where('LOK_DESCRIPTION', 'OTHERS')->first();
            $customer->CUST_BLOOD = $lookUp->LOK_RECID; //other
        }
        if ($customer->CUST_RELIGION == null) {
            $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'RELI')->where('LOK_DESCRIPTION', 'OTHERS')->first();
            $customer->CUST_RELIGION = $lookUp->LOK_RECID; //other
        }

        $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
        $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
        $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();

        $tenant = Tenant::where('TNT_CODE', $STOREID)->first();
        $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                        ->where('TENT_ERN_RECID', $ERN_RECID)
                        ->where('TENT_ACTIVE', 1)->get();

        if (count($formulaTenant) < 1) {
            $point = 0; //memtype tidak ditemukan
             echo 'store tidak ditemukan <br/>';
        }

        if (count($formulaGender) < 1) {
            $point = 0;
            //echo 'gender tidak ditemukan'; exit;
        }
        if (count($formulaReligion) < 1) {
            $point = 0;
            //  echo 'religion tidak ditemukan <br/>';
        }
        if (count($formulaBlood) < 1) {
            $point = 0;
            //  echo 'blood tidak ditemukan <br/>';
        }
      if($this->_availableDays($date,$ERN_RECID)==false) {
            $point = 0;
              echo 'day not avaible '.PHP_EOL;
        }
		
		}

        return $point;
    }

    public function prosesTempPayment($data_payment, $fileName_payment) {
        foreach ($data_payment as $key => $row_payment) {
            $no = $key + 1;
            // Convert all format datetime to timestamp
            $rcpdate = Carbon::createFromTimestamp(strtotime($row_payment[2 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcpdate
            $row_payment[2 - env('LOCALARRAY', 0)] = date('Y-m-d', strtotime($rcpdate->toDateString()));
            // Convert all format time to timestamp
            $rcptime = Carbon::createFromTimestamp(strtotime($row_payment[3 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcptime
            $row_payment[3 - env('LOCALARRAY', 0)] = date('H:i:s', strtotime($rcptime->toTimeString()));

            $calcBonus = $this->_checkBonusPayment($row_payment);

            $point_bonus = $calcBonus['point'];
            $multiple = $calcBonus['multiple'];

            $checkPayment = $this->_checkPaymentMethod($row_payment[7 - env('LOCALARRAY', 0)]);
            $checkCustomer = $this->_checkCustomer($row_payment[6 - env('LOCALARRAY', 0)]);
            $checkStore = $this->_checkStore($row_payment[1 - env('LOCALARRAY', 0)]);

            if ($checkPayment == TRUE && $checkCustomer == TRUE && $checkStore == TRUE) {
                try {
                    $postTempPayment = new PostTempPayment();
                    $postTempPayment->POSTTEMPPAYMENT_RECID = Uuid::uuid();
                    $postTempPayment->STOREID = $row_payment[1 - env('LOCALARRAY', 0)]; // on windows CSV array start from 1
                    $postTempPayment->RCPDATE = $row_payment[2 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPTIME = $row_payment[3 - env('LOCALARRAY', 0)];
                    $postTempPayment->TERMID = $row_payment[4 - env('LOCALARRAY', 0)];
                    $postTempPayment->RCPNBR = $row_payment[5 - env('LOCALARRAY', 0)];
                    $postTempPayment->CARDID = $row_payment[6 - env('LOCALARRAY', 0)];
                    $postTempPayment->TENDNBR = $row_payment[7 - env('LOCALARRAY', 0)];
                    $postTempPayment->ITEMTOTAL = $row_payment[8 - env('LOCALARRAY', 0)];
                    $postTempPayment->BIN = (!empty($row_payment[9 - env('LOCALARRAY', 0)])) ? $row_payment[9 - env('LOCALARRAY', 0)] : '0';
                    $postTempPayment->POINT_BONUS = $point_bonus;
                    $postTempPayment->MULTIPLE = $multiple;
                    //insert file name
                    $postTempPayment->save();

                    $row_payment[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row_payment[2 - env('LOCALARRAY', 0)]);
                    $row_payment[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row_payment[3 - env('LOCALARRAY', 0)]);

                    $this->writeLog("$fileName_payment :: Log Line $no : OK", env('DEBUG_EARNING_LOG', false));
                    //$this->createCleanCSVPayment($row_payment, $fileName_payment);
                } catch (\Exception $e) {
                    $this->writeLog("$fileName_payment :: Log Line $no : Error With Message : {$e->getMessage()}");
                    $row_payment[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row_payment[2 - env('LOCALARRAY', 0)]);
                    $row_payment[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row_payment[3 - env('LOCALARRAY', 0)]);
                    $row_payment[10 - env('LOCALARRAY', 0)] = ' ';
                    $row_payment[11 - env('LOCALARRAY', 0)] = " *** Undefined Error With Message : {$e->getMessage()}";
                    //$this->createUncleanCSVPayment($row_payment, $fileName_payment);
                    //Insert Unclean Postemp Payment
                }
            } else {
                $csvLog = "Log Line $no : ";
                if (!$checkPayment) {
                    $msg = " Payment Method " . $row_payment[7 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkCustomer) {
                    $msg = " Customer " . $row_payment[6 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkStore) {
                    $msg = " Store " . $row_payment[1 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }

                $this->writeLog($fileName_payment . " " . $csvLog, env('DEBUG_EARNING_LOG', false));

                $row_payment[10 - env('LOCALARRAY', 0)] = ' ';
                $row_payment[11 - env('LOCALARRAY', 0)] = ' *** ' . $csvLog;
                //$this->createUncleanCSVPayment($row_payment, $fileName_payment);
                //Insert Unclean Postemp Payment
            }
        }
    }

    public function prosesTempSales($data, $fileName) {
        foreach ($data as $key => $row) {
            $no = $key + 1;
            // Convert all format datetime to timestamp
            $rcpdate = Carbon::createFromTimestamp(strtotime($row[2 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcpdate
            $row[2 - env('LOCALARRAY', 0)] = date('Y-m-d', strtotime($rcpdate->toDateString()));

            // Convert all format time to timestamp
            $rcptime = Carbon::createFromTimestamp(strtotime($row[3 - env('LOCALARRAY', 0)]));
            // Assign to $row->rcptime
            $row[3 - env('LOCALARRAY', 0)] = date('H:i:s', strtotime($rcptime->toTimeString()));
            
            
			

          /*  
            try {
               
							   //back to single grouping for product bonus point formula
                $calcBonus = $this->_checkBonusPoint($row);
//			   //back to single grouping for product bonus point formula
//
                $point_bonus = $calcBonus['point'];
                $split_bonus = $calcBonus['split'];
                $multiple = $calcBonus['multiple'];
				
				 $this->writeLog(" Point : $point_bonus, Split : $split_bonus, Multiple : $split_bonus", env('DEBUG_EARNING_LOG', false));
               
            }
            catch (\Exception $e) {
                $point_bonus = 0;
                $split_bonus = 0;
                $multiple = 0;
                $this->writeLog(" Check Bonus Point Failed, with message : {$e->getMessage()}", env('DEBUG_EARNING_LOG', false));
            }  */
			
			
			    $point_bonus = 0;
                $split_bonus = 0;
                $multiple = 0;

            $checkProduct = $this->_checkProduct($row[7 - env('LOCALARRAY', 0)]);
            $checkCustomer = $this->_checkCustomer($row[5 - env('LOCALARRAY', 0)]);
            $checkStore = $this->_checkStore($row[1 - env('LOCALARRAY', 0)]);

            if ($checkProduct == TRUE && $checkCustomer == TRUE && $checkStore == TRUE) {
                try {
                    $postTemp = new PostTemp();
                    $postTemp->POSTTEMP_RECID = Uuid::uuid();
                    $postTemp->STOREID = $row[1 - env('LOCALARRAY', 0)];
                    $postTemp->RCPDATE = $row[2 - env('LOCALARRAY', 0)];
                    $postTemp->RCPTIME = $row[3 - env('LOCALARRAY', 0)];
                    $postTemp->TERMID = $row[4 - env('LOCALARRAY', 0)];
                    $postTemp->CARDID = $row[5 - env('LOCALARRAY', 0)];
                    $postTemp->RCPNBR = $row[6 - env('LOCALARRAY', 0)];
                    $postTemp->INTCODE = $row[7 - env('LOCALARRAY', 0)];
                    $postTemp->ITEMTOTAL = $row[8 - env('LOCALARRAY', 0)];
                    $postTemp->ITEMTOTSIGN = $row[9 - env('LOCALARRAY', 0)];
                    $postTemp->ITEMQTY = $row[10 - env('LOCALARRAY', 0)];
                    $postTemp->DEPTCODE = $row[11 - env('LOCALARRAY', 0)];
                    $postTemp->VATCODE = $row[12 - env('LOCALARRAY', 0)];
                    $postTemp->DISCOUNT = $row[13 - env('LOCALARRAY', 0)];
                    $postTemp->TOTALAFTERDISC = $row[14 - env('LOCALARRAY', 0)];
                    $postTemp->ITMIDX = (!empty($row[15 - env('LOCALARRAY', 0)])) ? $row[15 - env('LOCALARRAY', 0)] : 0;
                    $postTemp->POINT_BONUS = $point_bonus;
                    $postTemp->SPLIT_BONUS = $split_bonus;
                    $postTemp->MULTIPLE = $multiple;
                    //insert file name
                    $postTemp->save();

                    $row[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row[2 - env('LOCALARRAY', 0)]);
                    $row[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row[3 - env('LOCALARRAY', 0)]);

                    $this->writeLog("$fileName :: Log Line $no : OK", env('DEBUG_EARNING_LOG', false));
                    //$this->createCleanCSVSales($row, $fileName);
                } catch (\Exception $e) {
                    $this->writeLog("$fileName :: Log Line $no : Error With Message : {$e->getMessage()}");
                    $row[2 - env('LOCALARRAY', 0)] = str_replace('-', '', $row[2 - env('LOCALARRAY', 0)]);
                    $row[3 - env('LOCALARRAY', 0)] = str_replace(':', '', $row[3 - env('LOCALARRAY', 0)]);
                    $row[16 - env('LOCALARRAY', 0)] = ' ';
                    $row[17 - env('LOCALARRAY', 0)] = " *** Undefined Error With Message : {$e->getMessage()}";
                    //$this->createUncleanCSVSales($row, $fileName);
                    //Insert Unclean Postemp
                    
                }
            } else {
                $csvLog = "Log Line $no : ";
                if (!$checkProduct) {
                    $msg = " | Product " . $row[7 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkCustomer) {
                    $msg = " | Customer " . $row[5 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }
                if (!$checkStore) {
                    $msg = " | Store " . $row[1 - env('LOCALARRAY', 0)] . " Not Found";
                    $csvLog .= $msg;
                }

                $this->writeLog($fileName . " :: " . $csvLog, env('DEBUG_EARNING_LOG', false));

                $row[16 - env('LOCALARRAY', 0)] = ' ';
                $row[17 - env('LOCALARRAY', 0)] = ' *** ' . $csvLog;
                //$this->createUncleanCSVSales($row, $fileName);
                //Insert Unclean Postemp
            }
            $no++;
        }
    }

    public function writeLog($message, $writeToLog = true) {
        echo '[' . date('d-m-Y H:i:s') . '] [Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message . "\n";
        if ($writeToLog)
            Log::info('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);

        return true;
    }

    public function writeErrorLog($message, $writeToLog = true) {
        echo '[' . date('d-m-Y H:i:s') . '] : ' . $message . "\n";
        if ($writeToLog)
            Log::error('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);
        session()->push('logsEarning', $message);
        return true;
    }

    public function notification() {
        try {
            $foundError = session('logsEarning');
            $data = ['error' => $foundError, 'count' => count($foundError)];
            if ($data['count'] > 0) {
                $this->writeLog("\n Sending Error Email Notification");

                Mail::send('mail-confirm-earning', $data, function ($mail) {
                    $mail->to(env('EMAIL_ERROR_TO', 'info@loyalto.id'), 'Administrator MyLoyalty');
                    $mail->subject('Earning Proccess Error Notification');
                });
            }
        } catch (\Exception $e) {
            $this->writeErrorLog('Send Mail Fail with error  : ' . $e->getMessage());
        }
    }

}


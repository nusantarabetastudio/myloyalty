<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\InstantGift;
use App\Models\Master\Gift;
use App\Models\Master\GiftTenant;
use App\Models\Master\Tenant;
use App\Models\Preference;
use Faker\Provider\Uuid;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class InstantGiftController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct(Session $session)
    {
        $this->parent = 'Instant Gift';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        $this->admin = Preference::where('key', 'admin')->first()->value;
        $this->superAdmin = Preference::where('key' , 'superadmin')->first()->value;
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['breadcrumbs'] = 'Listing Instant Gift';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        return view('instant-gift.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['breadcrumbs'] = 'Instant Gift Formula Create';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['gift_types'] = ['Birthday', 'Amount'];
        $data['signs'] = ['Between', 'Equals', 'Greater Than', 'Less Than', 'Greater Than or Equal To', 'Less Than or Equal To'];
        $data['periods'] = ['Per day in period', 'Per Period', 'Unlimited Period'];
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        $store_id = Session::get('data')->STORE->id;
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        } else {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/' . $store_id . '/product?limit=100&availableProductOnly=true';
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['products'] = $contents->DATA;

            return view('instant-gift.create', $data);
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'GIFT_FROM' => 'required',
            'GIFT_TO' => 'required',
            'GIFT_FRAMOUNT' => 'numeric|required',
            'GIFT_TOAMOUNT' => 'numeric|required',
            'GIFT_PERIOD' => 'required',
            'GIFT_NUMOFGIFT' => 'required'
        ],[
            'GIFT_FROM.required' => 'Applicable From fields is required.',
            'GIFT_TO.required' => 'Applicable To fields is required.',
            'GIFT_FRAMOUNT.required' => 'From Amount fields is required.',
            'GIFT_TOAMOUNT.required' => 'To Amount fields is required.',
            'GIFT_PERIOD.required' => 'Periods fields is required.',
        ]);

        $inputData = $request->except(['_token', 'PRDR_PRODUCT', 'GIFT_TENANT', 'GIFT_TO', 'GIFT_FROM', 'GIFT_ACTIVE', 'GIFT_SEND_EMAIL', 'GIFT_SEND_SMS']);

        $instantGift = new Gift();
        $instantGift->GIFT_RECID = Uuid::uuid();
        $instantGift->fill($inputData);
        $instantGift->GIFT_PNUM = $this->pnum;
        $instantGift->GIFT_PTYPE = $this->ptype;
        $instantGift->GIFT_FROM = dateFormat($request->input('GIFT_FROM'));
        $instantGift->GIFT_TO = dateFormat($request->input('GIFT_TO'));
        $instantGift->GIFT_ACTIVE = 1;
        $instantGift->GIFT_SEND_EMAIL = $request->has('GIFT_SEND_EMAIL') && $request->input('GIFT_SEND_EMAIL') == 'on' ? 1 : 0;
        $instantGift->GIFT_SEND_SMS = $request->has('GIFT_SEND_SMS') && $request->input('GIFT_SEND_SMS') == 'on' ? 1 : 0;

        $instantGift->save();

        if($request->has('GIFT_TENANT')) {
            foreach ($request->input('GIFT_TENANT') as $tenant) {

                $giftTenant = new GiftTenant();
                $giftTenant->GIFT_CODE = Uuid::uuid();
                $giftTenant->GIFT_GIFT_RECID = $instantGift->GIFT_RECID;
                $giftTenant->GIFT_TENANT = $tenant;
                $giftTenant->save();

            }
        }

        $request->session()->flash('success', 'Data has been saved.');

        return redirect('instant-gift');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['breadcrumbs'] = 'Instant Gift Formula Create';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['gift_types'] = ['Birthday', 'Amount'];
        $data['signs'] = ['Between', 'Equals', 'Greater Than', 'Less Than', 'Greater Than or Equal To', 'Less Than or Equal To'];
        $data['periods'] = ['Per day in period', 'Per Period', 'Unlimited Period'];
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $data['instantGift'] = Gift::findOrFail($id);
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        $store_id = Session::get('data')->STORE->id;
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        } else {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/' . $store_id . '/product?limit=100&availableProductOnly=true';
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['products'] = $contents->DATA;

            return view('instant-gift.create', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'GIFT_FROM' => 'required',
            'GIFT_TO' => 'required',
            'GIFT_FRAMOUNT' => 'numeric|required',
            'GIFT_TOAMOUNT' => 'numeric|required',
            'GIFT_PERIOD' => 'required',
            'GIFT_NUMOFGIFT' => 'required'
        ]);

        $inputData = $request->except(['_token', 'PRDR_PRODUCT', 'GIFT_TENANT', 'GIFT_TO', 'GIFT_FROM', 'GIFT_ACTIVE', 'GIFT_SEND_EMAIL', 'GIFT_SEND_SMS']);

        $instantGift = Gift::findOrFail($id);
        $instantGift->fill($inputData);
        $instantGift->GIFT_FROM = dateFormat($request->input('GIFT_FROM'));
        $instantGift->GIFT_TO = dateFormat($request->input('GIFT_TO'));
        $instantGift->GIFT_ACTIVE = $request->has('GIFT_ACTIVE') && $request->input('GIFT_ACTIVE') == 'on' ? 1 : 0;
        $instantGift->GIFT_SEND_EMAIL = $request->has('GIFT_SEND_EMAIL') && $request->input('GIFT_SEND_EMAIL') == 'on' ? 1 : 0;
        $instantGift->GIFT_SEND_SMS = $request->has('GIFT_SEND_SMS') && $request->input('GIFT_SEND_SMS') == 'on' ? 1 : 0;
        $instantGift->save();

        $instantGift->tenants()->delete();

        if($request->has('GIFT_TENANT')) {
            foreach ($request->input('GIFT_TENANT') as $tenant) {

                $giftTenant = new GiftTenant();
                $giftTenant->GIFT_CODE = Uuid::uuid();
                $giftTenant->GIFT_GIFT_RECID = $instantGift->GIFT_RECID;
                $giftTenant->GIFT_TENANT = $tenant;
                $giftTenant->save();

            }
        }

        $request->session()->flash('success', 'Data has been updated.');

        return redirect('instant-gift');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        if($request->input('id') !== null) {
            $data = Gift::findOrFail($request->input('id'));
            $data->tenants()->delete();
            $data->delete();
            $request->session()->flash('success', 'Data has been deleted.');
            return redirect('instant-gift');

        } else {
            $request->session()->flash('error', 'Data hasn\'t been deleted.');

            return redirect('instant-gift');

        }
    }

    public function data(Request $request)
    {
        if($request->ajax()) {
            $instantGift = Gift::all();

            $datatables = app('datatables')->of($instantGift)
                ->addColumn('id', function () {
                    return null;
                })
                ->addColumn('action', function($data) {
                    $edit = '<a href="'. route('instant-gift.edit', $data->GIFT_RECID ) .'" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                    $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$data->GIFT_RECID.'" data-name="'.$data->GIFT_DESCRIPTION.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                    return $edit . ' ' . $delete ;
                })
                ->editColumn('GIFT_FROM', function ($data) {
                    if(!empty($data->GIFT_FROM)) {
                        return $data->GIFT_FROM->format('d/m/Y');
                    }
                    return '-';
                })
                ->editColumn('GIFT_TO', function ($data) {
                    if(!empty($data->GIFT_TO)) {
                        return $data->GIFT_TO->format('d/m/Y');
                    }
                    return '-';
                })
                ->editColumn('GIFT_ACTIVE', function ($instantGift) {
                    if($instantGift->GIFT_ACTIVE == 1) {
                        return '<span class="text-success md-check"></span>';
                    } else {
                        return '<span class="text-danger md-close"></span>';
                    }
                });

            return $datatables->make(true);
        }
        return abort('403', 'Method Not Allowed');
    }
}

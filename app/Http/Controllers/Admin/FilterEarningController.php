<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use Psy\Formatter\Formatter;


class FilterEarningController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {

        $file = public_path('filter.csv');
        $load = Excel::load($file)
            ->noHeading(false)
            ->get();

        $dataClean = $load->where('value', "TRUE");
        $dataUnclean = $load->where('value', "FALSE");

        $this->storeClean($dataClean);
        $this->storeUnclean($dataUnclean);

        return 'Successfully';
    }

    function storeClean($data = []) {
        $filename = Carbon::now()->format('Y-m-d-His').'-CLEAN';
        $file = Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('CLEAN', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->store('csv', public_path('/'));
        return $file;
    }

    function storeUnclean($data = []) {
        $filename = Carbon::now()->format('Y-m-d-His').'-UNCLEAN';
        $file = Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('CLEAN', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->store('csv', public_path('/'));
        return $file;
    }
}

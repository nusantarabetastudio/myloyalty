<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Models\Admin\Post;
use App\Models\Master\Product;
use Illuminate\Http\Request;

use App\Http\Requests;

class PostController extends ApiController {

    public function getPost($post_id) {
        $post = Post::findOrFail($post_id);
        return $this->respond($post);
    }
}
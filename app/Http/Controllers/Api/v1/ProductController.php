<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Models\Master\Product;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends ApiController {

    public function index(Request $request) {

        $query = $request->get('q');

        $products = Product::select(['PRO_RECID', 'PRO_CODE', 'PRO_DESCR'])->where('PRO_DESCR', 'like', "%{$query}%")->limit(10)->get();
        return response()->json($products);
    }
}
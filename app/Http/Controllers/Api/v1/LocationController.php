<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Master\Lookup;
use Illuminate\Http\Request;

class LocationController extends ApiController
{
    public function getProvinces($countryId)
    {
        $provinces = Lookup::where('LOK_CODE', 'PROV')->where('LOK_CODEMST', $countryId)->get();

        return $this->makeResponse(null, $provinces);
    }

    public function getCities($provinceId)
    {
        $cities = Lookup::where('LOK_CODE', 'CITY')->where('LOK_CODEMST', $provinceId)->get();

        return $this->makeResponse(null, $cities);
    }

    public function getAreas($cityId)
    {
        $areas = Lookup::where('LOK_CODE', 'AREA')->where('LOK_CODEMST', $cityId)->get();

        return $this->makeResponse(null, $areas);
    }
}

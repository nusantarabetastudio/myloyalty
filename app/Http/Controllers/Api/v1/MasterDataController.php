<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Models\Master\Brand;
use App\Models\Master\Lookup;
use App\Models\Master\Product;
use App\Models\Master\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;

class MasterDataController extends ApiController {

    public function department(Request $request) {

        if($request->ajax()) {
            $query = $request->get('q');

            $data = Lookup::select(['LOK_RECID', 'LOK_DESCRIPTION'])->where('LOK_CODE', 'DEPT')->where('LOK_DESCRIPTION', 'like', "%{$query}%")->get();

            return response()->json($data);
        }
        return abort('404', 'Method Not Allowed');
    }

    public function division(Request $request) {

        if($request->ajax()) {
            $query = $request->get('q');

            $data = Lookup::select(['LOK_RECID', 'LOK_DESCRIPTION'])->where('LOK_CODE', 'DIVI')->where('LOK_DESCRIPTION', 'like', "%{$query}%")->get();

            return response()->json($data);
        }
        return abort('404', 'Method Not Allowed');
    }

    public function category(Request $request) {

        if($request->ajax()) {
            $query = $request->get('q');

            $data = Lookup::select(['LOK_RECID', 'LOK_DESCRIPTION'])->where('LOK_CODE', 'CATG')->where('LOK_DESCRIPTION', 'like', "%{$query}%")->get();

            return response()->json($data);
        }
        return abort('404', 'Method Not Allowed');
    }

    public function subCategory(Request $request) {

        if($request->ajax()) {
            $query = $request->get('q');

            $data = Lookup::select(['LOK_RECID', 'LOK_DESCRIPTION'])->where('LOK_CODE', 'SUBC')->where('LOK_DESCRIPTION', 'like', "%{$query}%")->get();

            return response()->json($data);
        }
        return abort('404', 'Method Not Allowed');
    }

    public function segment(Request $request) {

        if($request->ajax()) {
            $query = $request->get('q');

            $data = Lookup::select(['LOK_RECID', 'LOK_DESCRIPTION'])->where('LOK_CODE', 'SEGM')->where('LOK_DESCRIPTION', 'like', "%{$query}%")->get();

            return response()->json($data);
        }
        return abort('404', 'Method Not Allowed');
    }

    public function brand(Request $request) {

        if($request->ajax()) {
            $query = $request->get('q');

            $data = Brand::select(['id', 'code', 'name'])->where('name', 'like', "%{$query}%")->get();

            return response()->json($data);
        }
        return abort('404', 'Method Not Allowed');
    }

    public function vendor(Request $request) {

        if($request->ajax()) {
            $query = $request->get('q');

            $data = Vendor::select(['VDR_RECID', 'VDR_NAME', 'VDR_CODE'])->where('VDR_NAME', 'like', "%{$query}%")->get();

            return response()->json($data);
        }
        return abort('404', 'Method Not Allowed');
    }
}
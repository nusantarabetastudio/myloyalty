<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Mandatory;
use App\Models\Master\Role;
use Carbon\Carbon;
use Datatables;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Session;

class MandatoryController extends Controller
{
    private $pnum;
    private $ptype;
    private $parent;
    private $parent_link;

    public function __construct(Session $session)
    {
        //$this->middleware('auth');
        $this->parent = 'Mandatory';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        // $this->middleware('user-log', ['only' => ['generalIndex', 'propertyIndex', 'generalEdit', 'propertyEdit', 'generalUpdate', 'propertyUpdate']]);
    }

    public function generalIndex() {
        $columns = [];
        $includedTables = ['Z_CardType', 'Z_Lookup'];
        foreach ($includedTables as $table) {
            $tableColumns = \Schema::getColumnListing($table);
            $tableColumns = array_map(function($val) use($table) {
                return $table . '.' . $val;
            } , $tableColumns);

            $columns = array_merge($columns, $tableColumns);
        }
        $roles = Role::lists('ROLE_NAME');

        $dataTypes = [
            '34' => 'image',
            '35' => 'text',
            '36' => 'guid',
            '40' => 'date',
            '41' => 'time',
            '42' => 'datetime2',
            '43' => 'datetime offset',
            '48' => 'tiny</',
            '52' => 'smallint',
            '56' => 'int',
            '58' => 'small datetime',
            '59' => 'real',
            '60' => 'money',
            '61' => 'datetime',
            '62' => 'float',
            '104' => 'bit',
            '106' => 'decimal',
            '108' => 'numeric',
            '122' => 'small money',
            '127' => 'bigint',
            '165' => 'varbinary',
            '167' => 'varchar',
            '173' => 'binary',
            '175' => 'char',
            '189' => 'timestamp',
            '231' => 'nvarchar',
            '239' => 'nchar',
            '241' => 'xml'
        ];

        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Mandatory General';

        return view ('mandatory.index', compact('parent','parent_link','breadcrumbs', 'columns', 'roles', 'dataTypes'));
    }
    public function propertyIndex() {
        $columns = [];
        $includedTables = ['Customers', 'Post','Postr', 'Z_Event', 'Z_Product_Item','Z_Promotion', 'Z_Tenant'];
        foreach ($includedTables as $table) {
            $tableColumns = \Schema::getColumnListing($table);
            $tableColumns = array_map(function($val) use($table) {
                return $table . '.' . $val;
            } , $tableColumns);

            $columns = array_merge($columns, $tableColumns);
        }
        $roles = Role::lists('ROLE_NAME');

        $dataTypes = [
            '34' => 'image',
            '35' => 'text',
            '36' => 'guid',
            '40' => 'date',
            '41' => 'time',
            '42' => 'datetime2',
            '43' => 'datetime offset',
            '48' => 'tiny</',
            '52' => 'smallint',
            '56' => 'int',
            '58' => 'small datetime',
            '59' => 'real',
            '60' => 'money',
            '61' => 'datetime',
            '62' => 'float',
            '104' => 'bit',
            '106' => 'decimal',
            '108' => 'numeric',
            '122' => 'small money',
            '127' => 'bigint',
            '165' => 'varbinary',
            '167' => 'varchar',
            '173' => 'binary',
            '175' => 'char',
            '189' => 'timestamp',
            '231' => 'nvarchar',
            '239' => 'nchar',
            '241' => 'xml'
        ];

        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Mandatory Property';

        return view ('mandatory.property', compact('parent','parent_link','breadcrumbs', 'columns', 'roles', 'dataTypes'));
    }    
    public function getGeneralData() {
        $mandatory = Mandatory::where('ptype', '00');

        return Datatables::of($mandatory)
            ->editColumn('is_visible', function($mandatory) {
                $class = ($mandatory->is_visible == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('is_required', function($mandatory) {
                $class = ($mandatory->is_required == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('is_read_only', function($mandatory) {
                $class = ($mandatory->is_read_only == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('is_unique', function($mandatory) {
                $class = ($mandatory->is_unique == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('active', function($mandatory) {
                $class = ($mandatory->active == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->addColumn('action', function($mandatory) {
                $edit = '<a href="'.url('mandatory/general/'.$mandatory->id).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$mandatory->id.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            })
            ->make(true);
    }
    public function getpropertyData() {
        $mandatory = Mandatory::where('ptype','!=', '00');
        
        return Datatables::of($mandatory)
            ->editColumn('is_visible', function($mandatory) {
                $class = ($mandatory->is_visible == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('is_required', function($mandatory) {
                $class = ($mandatory->is_required == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('is_read_only', function($mandatory) {
                $class = ($mandatory->is_read_only == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('is_unique', function($mandatory) {
                $class = ($mandatory->is_unique == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->editColumn('active', function($mandatory) {
                $class = ($mandatory->active == 1) ? 'md-check text-success' : 'md-close text-danger';
                return '<i class="bold icon ' . $class .'"></i>';
            })
            ->addColumn('action', function($mandatory) {
                $edit = '<a href="'.url('mandatory/property/'.$mandatory->id).'/edit" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$mandatory->id.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                return $edit . '&nbsp;' . $delete;
            })
            ->make(true);
    }

    public function generalStore(Request $request) {
        $input = $request->except(['_token']);
        $mandatory = new Mandatory;
        $mandatory->id = Uuid::uuid();
        $mandatory->pnum = $this->pnum;
        $mandatory->ptype = '00';
        $mandatory->field_name = $input['field_name'];
        $mandatory->description = $input['description'];
        $mandatory->table_name = $input['table_name'];
        $mandatory->role_name = $input['role_name'];
        $mandatory->data_type_id = $input['data_type_id'];

        $mandatory->is_visible = (isset($input['is_visible']) && $input['is_visible'] == 'on') ? 1 : 0;
        $mandatory->is_required = (isset($input['is_required']) && $input['is_required'] == 'on') ? 1 : 0;
        $mandatory->is_read_only = (isset($input['is_read_only']) && $input['is_read_only'] == 'on') ? 1 : 0;
        $mandatory->is_unique = (isset($input['is_unique']) && $input['is_unique'] == 'on') ? 1 : 0;
        $mandatory->active = (isset($input['active']) && $input['active'] == 'on') ? 1 : 0;

        if(isset($input['default_value']))
            $mandatory->default_value = $input['default_value'];
        if(isset($input['minimum']))
            $mandatory->minimum = $input['minimum'];
        if(isset($input['maximum']))
            $mandatory->maximum = $input['maximum'];
        $mandatory->save();

        return redirect()->back()->with('success', 'Data has been stored successfully');
    }
    public function propertyStore(Request $request) {
        $input = $request->except(['_token']);
        $mandatory = new Mandatory;
        $mandatory->id = Uuid::uuid();
        $mandatory->pnum = $this->pnum;
        $mandatory->ptype = $this->ptype;
        $mandatory->field_name = $input['field_name'];
        $mandatory->description = $input['description'];
        $mandatory->table_name = $input['table_name'];
        $mandatory->role_name = $input['role_name'];
        $mandatory->data_type_id = $input['data_type_id'];

        $mandatory->is_visible = (isset($input['is_visible']) && $input['is_visible'] == 'on') ? 1 : 0;
        $mandatory->is_required = (isset($input['is_required']) && $input['is_required'] == 'on') ? 1 : 0;
        $mandatory->is_read_only = (isset($input['is_read_only']) && $input['is_read_only'] == 'on') ? 1 : 0;
        $mandatory->is_unique = (isset($input['is_unique']) && $input['is_unique'] == 'on') ? 1 : 0;
        $mandatory->active = (isset($input['active']) && $input['active'] == 'on') ? 1 : 0;

        if(isset($input['default_value']))
            $mandatory->default_value = $input['default_value'];
        if(isset($input['minimum']))
            $mandatory->minimum = $input['minimum'];
        if(isset($input['maximum']))
            $mandatory->maximum = $input['maximum'];
        $mandatory->save();

        return redirect()->back()->with('success', 'Data has been stored successfully');
    }
    public function generalEdit($mandatoryId, Request $request) {
        $mandatory = Mandatory::find($mandatoryId);
        $columns = [];
        $includedTables = ['Z_CardType', 'Z_Lookup'];
        foreach ($includedTables as $table) {
            $tableColumns = \Schema::getColumnListing($table);
            $tableColumns = array_map(function($val) use($table) {
                return $table . '.' . $val;
            } , $tableColumns);

            $columns = array_merge($columns, $tableColumns);
        }

        $roles = Role::lists('ROLE_NAME');

        $dataTypes = [
            '34' => 'image',
            '35' => 'text',
            '36' => 'guid',
            '40' => 'date',
            '41' => 'time',
            '42' => 'datetime2',
            '43' => 'datetime offset',
            '48' => 'tiny</',
            '52' => 'smallint',
            '56' => 'int',
            '58' => 'small datetime',
            '59' => 'real',
            '60' => 'money',
            '61' => 'datetime',
            '62' => 'float',
            '104' => 'bit',
            '106' => 'decimal',
            '108' => 'numeric',
            '122' => 'small money',
            '127' => 'bigint',
            '165' => 'varbinary',
            '167' => 'varchar',
            '173' => 'binary',
            '175' => 'char',
            '189' => 'timestamp',
            '231' => 'nvarchar',
            '239' => 'nchar',
            '241' => 'xml'
        ];

        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Mandatory General';

        return view ('mandatory.index', compact('parent','parent_link','breadcrumbs', 'mandatory', 'columns', 'roles', 'dataTypes'));
    }
    public function propertyEdit($mandatoryId, Request $request) {
        $mandatory = Mandatory::find($mandatoryId);
        $columns = [];
        $includedTables = ['Customers', 'Post','Postr', 'Z_Event', 'Z_Product_Item','Z_Promotion', 'Z_Tenant'];
        foreach ($includedTables as $table) {
            $tableColumns = \Schema::getColumnListing($table);
            $tableColumns = array_map(function($val) use($table) {
                return $table . '.' . $val;
            } , $tableColumns);

            $columns = array_merge($columns, $tableColumns);
        }

        $roles = Role::lists('ROLE_NAME');

        $dataTypes = [
            '34' => 'image',
            '35' => 'text',
            '36' => 'guid',
            '40' => 'date',
            '41' => 'time',
            '42' => 'datetime2',
            '43' => 'datetime offset',
            '48' => 'tiny</',
            '52' => 'smallint',
            '56' => 'int',
            '58' => 'small datetime',
            '59' => 'real',
            '60' => 'money',
            '61' => 'datetime',
            '62' => 'float',
            '104' => 'bit',
            '106' => 'decimal',
            '108' => 'numeric',
            '122' => 'small money',
            '127' => 'bigint',
            '165' => 'varbinary',
            '167' => 'varchar',
            '173' => 'binary',
            '175' => 'char',
            '189' => 'timestamp',
            '231' => 'nvarchar',
            '239' => 'nchar',
            '241' => 'xml'
        ];

        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Mandatory Property';

        return view ('mandatory.property', compact('parent','parent_link','breadcrumbs', 'mandatory', 'columns', 'roles', 'dataTypes'));
    }

    public function generalUpdate($mandatoryId, Request $request) {
        $input = $request->except(['_token', 'method']);
        $mandatory = Mandatory::find($mandatoryId);
        $mandatory->pnum = $this->pnum;
        $mandatory->ptype = '00';
        $mandatory->field_name = $input['field_name'];
        $mandatory->description = $input['description'];
        $mandatory->table_name = $input['table_name'];
        $mandatory->role_name = $input['role_name'];
        $mandatory->data_type_id = $input['data_type_id'];

        $mandatory->is_visible = (isset($input['is_visible']) && $input['is_visible'] == 'on') ? 1 : 0;
        $mandatory->is_required = (isset($input['is_required']) && $input['is_required'] == 'on') ? 1 : 0;
        $mandatory->is_read_only = (isset($input['is_read_only']) && $input['is_read_only'] == 'on') ? 1 : 0;
        $mandatory->is_unique = (isset($input['is_unique']) && $input['is_unique'] == 'on') ? 1 : 0;
        $mandatory->active = (isset($input['active']) && $input['active'] == 'on') ? 1 : 0;

        if(isset($input['default_value']))
            $mandatory->default_value = $input['default_value'];
        if(isset($input['minimum']))
            $mandatory->minimum = $input['minimum'];
        if(isset($input['maximum']))
            $mandatory->maximum = $input['maximum'];
        $mandatory->save();

        return redirect()->route('mandatory.general.index')->with('success', 'Data has been Updated successfully');
    }

    public function propertyUpdate($mandatoryId, Request $request) {
        $input = $request->except(['_token', 'method']);
        $mandatory = Mandatory::find($mandatoryId);
        $mandatory->pnum = $this->pnum;
        $mandatory->ptype = $this->ptype;
        $mandatory->field_name = $input['field_name'];
        $mandatory->description = $input['description'];
        $mandatory->table_name = $input['table_name'];
        $mandatory->role_name = $input['role_name'];
        $mandatory->data_type_id = $input['data_type_id'];

        $mandatory->is_visible = (isset($input['is_visible']) && $input['is_visible'] == 'on') ? 1 : 0;
        $mandatory->is_required = (isset($input['is_required']) && $input['is_required'] == 'on') ? 1 : 0;
        $mandatory->is_read_only = (isset($input['is_read_only']) && $input['is_read_only'] == 'on') ? 1 : 0;
        $mandatory->is_unique = (isset($input['is_unique']) && $input['is_unique'] == 'on') ? 1 : 0;
        $mandatory->active = (isset($input['active']) && $input['active'] == 'on') ? 1 : 0;

        if(isset($input['default_value']))
            $mandatory->default_value = $input['default_value'];
        if(isset($input['minimum']))
            $mandatory->minimum = $input['minimum'];
        if(isset($input['maximum']))
            $mandatory->maximum = $input['maximum'];
        $mandatory->save();

        return redirect()->route('mandatory.property.index')->with('success', 'Data has been Updated successfully');
    }

    public function destroy(Request $request) {
        Mandatory::destroy($request->input('id'));

        return redirect()->back()->with('success', 'Data has been deleted!');
    }

}
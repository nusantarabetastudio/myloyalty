<?php

namespace App\Http\Controllers\RedeemPointFormula;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Admin\FormulaRedeem;
use App\Models\Admin\FormulaRedeemLog;
use App\Models\Admin\FormulaRedeemProduct;
use App\Models\Admin\FormulaRedeemTenant;
use App\Models\Master\Tenant;
use App\Models\Preference;
use Carbon\Carbon;
use Datatables;
use Faker\Provider\Uuid;
use GuzzleHttp\Client;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class TebusMurahController extends Controller
{
    private $parent;
    private $parent_link;

    public function __construct(Session $session)
    {
        $this->parent = 'Redeem Point Formula';
        $this->parent_link = route('redeem-point-formula.tebus-murah.index');
        $this->admin = Preference::where('key', 'admin')->first()->value;
        $this->superAdmin = Preference::where('key' , 'superadmin')->first()->value;
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = 'Tebus Murah';
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $type = 'tebusMurah';
        return view('redeem-point-formula.index', compact('breadcrumbs', 'parent', 'parent_link', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['form_action'] = route('redeem-point-formula.tebus-murah.store');
        $data['breadcrumbs'] = 'New Formula';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['type'] = 'tebus-murah';
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        $store_id = Session::get('data')->STORE->id;

        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        } else {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/' . $store_id . '/product?limit=100&availableProductOnly=true';
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['products'] = $contents->DATA;

            return view('redeem-point-formula.form', $data);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'RDM_DESC' => 'required',
            'fromDate' => 'date_format:d/m/Y',
            'toDate' => 'date_format:d/m/Y'
        ];
        $this->validate($request, $rules);

        $inputData = $request->only(['RDM_DESC', 'RDM_BY_QTY', 'RDM_BY_TRANS', 'RDM_AMOUNT']);

        $data = new FormulaRedeem();
        $data->RDM_RECID = Uuid::uuid();
        $data->fill($inputData);
        $data->RDM_PNUM = $this->pnum;
        $data->RDM_PTYPE = $this->ptype;
        $data->RDM_FORMULA = 1; // Tebus Murah
        $data->RDM_POINT = $request->has('RDM_POINT') ? $request->input('RDM_POINT') : 0;
        $data->RDM_ACTIVE = 1;
        $data->RDM_FRDATE = dateFormat($request->input('fromDate'));
        $data->RDM_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $data->RDM_TODATE = dateFormat($request->input('toDate'));
        $data->RDM_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s
        $data->RDM_VALUE = $request->has('RDM_VALUE') ? $request->has('RDM_VALUE') : 0;

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $data->RDM_SUNDAY = 1;
            } else {
                $data->RDM_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $data->RDM_MONDAY = 1;
            } else {
                $data->RDM_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $data->RDM_TUESDAY = 1;
            } else {
                $data->RDM_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $data->RDM_WEDNESDAY = 1;
            } else {
                $data->RDM_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $data->RDM_THURSDAY = 1;
            } else {
                $data->RDM_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $data->RDM_FRIDAY = 1;
            } else {
                $data->RDM_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $data->RDM_SATURDAY = 1;
            } else {
                $data->RDM_SATURDAY = 0;
            }
        }

        $data->save();

        if($request->has('RDT_TENANT')) {

            foreach($request->input('RDT_TENANT') as $tenant) {
                $r_tenant = new FormulaRedeemTenant();
                $r_tenant->RDT_RECID = Uuid::uuid();
                $r_tenant->RDT_RDM_RECID = $data->RDM_RECID;
                $r_tenant->RDT_TENANT = $tenant;
                $r_tenant->RDT_ACTIVE = 1;
                $r_tenant->save();

            }
        }

        if($request->has('PRDR_PRODUCT')) {

            foreach($request->input('PRDR_PRODUCT') as $product) {
                $r_product = new FormulaRedeemProduct();
                $r_product->PRDR_RECID = Uuid::uuid();
                $r_product->PRDR_RDM_RECID = $data->RDM_RECID;
                $r_product->PRDR_PRODUCT = $product;
                $r_product->PRDR_ACTIVE = 1;
                $r_product->save();

            }
        }

        $log = new FormulaRedeemLog();
        $log->RLOG_RECID = Uuid::uuid();
        $log->RLOG_RDM_RECID = $data->RDM_RECID;
        $log->RLOG_TYPE = 1; // Create
        $log->RLOG_POSTDATE = Carbon::now()->format('Y-m-d');
        $log->RLOG_USERID = Session::get('data')->USER_DATA->USER_USERNAME;
        $log->save();

        $request->session()->flash('success', 'Data has been created.');

        return redirect('redeem-point-formula/tebus-murah');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['form_action'] = route('redeem-point-formula.tebus-murah.update', $id);
        $data['breadcrumbs'] = 'Edit Formula';
        $data['parent'] = 'Tebus Murah';
        $data['parent_link'] = route('redeem-point-formula.tebus-murah.index');
        $data['type'] = 'tebus-murah';
        $data['store'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['formulaRedeem'] = FormulaRedeem::findOrFail($id);

        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        $store_id = Session::get('data')->STORE->id;

        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/product';
        } else {
            $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'cs/redeem/store/' . $store_id . '/product?limit=100&availableProductOnly=true';
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['products'] = $contents->DATA;

            return view('redeem-point-formula.form', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'RDM_DESC' => 'required',
            'fromDate' => 'date_format:d/m/Y',
            'toDate' => 'date_format:d/m/Y'
        ];

        $this->validate($request, $rules);

        $inputData = $request->only(['RDM_DESC', 'RDM_BY_QTY', 'RDM_BY_TRANS', 'RDM_AMOUNT']);

        $data = FormulaRedeem::findOrFail($id);

        $data->RDM_ACTIVE = $request->has('RDM_ACTIVE') && $request->input('RDM_ACTIVE') == 'on' ? 1 : 0;
        $data->fill($inputData);
        $data->RDM_POINT = $request->has('RDM_POINT') ? $request->input('RDM_POINT') : 0;
        $data->RDM_FRDATE = dateFormat($request->input('fromDate'));
        $data->RDM_FRTIME = date('H:i:s', strtotime($request->input('fromHour') . ':' . $request->input('fromMinute') . ':' . $request->input('fromSecond'))); // Output Format H:i:s
        $data->RDM_TODATE = dateFormat($request->input('toDate'));
        $data->RDM_TOTIME = date('H:i:s', strtotime($request->input('toHour') . ':' . $request->input('toMinute') . ':' . $request->input('toSecond'))); // Output Format H:i:s
        $data->RDM_VALUE = $request->has('RDM_VALUE') ? $request->has('RDM_VALUE') : 0;

        // Week
        if($request->has('week')) {
            if(isset($request->input('week')[0]) && $request->input('week')[0] ) {
                $data->RDM_SUNDAY = 1;
            } else {
                $data->RDM_SUNDAY = 0;
            }
            if(isset($request->input('week')[1]) && $request->input('week')[1] !== null ) {
                $data->RDM_MONDAY = 1;
            } else {
                $data->RDM_MONDAY = 0;
            }
            if(isset($request->input('week')[2]) && $request->input('week')[2] !== null ) {
                $data->RDM_TUESDAY = 1;
            } else {
                $data->RDM_TUESDAY = 0;
            }
            if(isset($request->input('week')[3]) && $request->input('week')[3] !== null ) {
                $data->RDM_WEDNESDAY = 1;
            } else {
                $data->RDM_WEDNESDAY = 0;
            }
            if(isset($request->input('week')[4]) && $request->input('week')[4] !== null ) {
                $data->RDM_THURSDAY = 1;
            } else {
                $data->RDM_THURSDAY = 0;
            }
            if(isset($request->input('week')[5]) && $request->input('week')[5] !== null ) {
                $data->RDM_FRIDAY = 1;
            } else {
                $data->RDM_FRIDAY = 0;
            }
            if(isset($request->input('week')[6]) && $request->input('week')[6] !== null ) {
                $data->RDM_SATURDAY = 1;
            } else {
                $data->RDM_SATURDAY = 0;
            }
        }

        $data->save();

        if($request->has('RDT_TENANT')) {

            $data->formulaTenants()->delete();

            foreach ($request->input('RDT_TENANT') as $tenant) {
                $r_tenant = new FormulaRedeemTenant();
                $r_tenant->RDT_RECID = Uuid::uuid();
                $r_tenant->RDT_RDM_RECID = $data->RDM_RECID;
                $r_tenant->RDT_TENANT = $tenant;
                $r_tenant->RDT_ACTIVE = 1;
                $r_tenant->save();

            }
        }

        if($request->has('PRDR_PRODUCT')) {

            $data->formulaProducts()->delete();

            foreach ($request->input('PRDR_PRODUCT') as $product) {
                $r_product = new FormulaRedeemProduct();
                $r_product->PRDR_RECID = Uuid::uuid();
                $r_product->PRDR_RDM_RECID = $data->RDM_RECID;
                $r_product->PRDR_PRODUCT = $product;
                $r_product->PRDR_ACTIVE = 1;
                $r_product->save();

            }
        }

        $log = new FormulaRedeemLog();
        $log->RLOG_RECID = Uuid::uuid();
        $log->RLOG_RDM_RECID = $data->RDM_RECID;
        $log->RLOG_TYPE = 2; // Update
        $log->RLOG_POSTDATE = Carbon::now()->format('Y-m-d');
        $log->RLOG_USERID = Session::get('data')->USER_DATA->USER_USERNAME;
        $log->save();

        $request->session()->flash('success', 'Data has been updated.');

        return redirect('redeem-point-formula/tebus-murah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if( $request->ajax() ) {
            return abort('404', 'Method Not Allowed');
        }
        if($request->input('id') !== null) {
            $data = FormulaRedeem::findOrFail($request->input('id'));
            $data->formulaTenants()->delete();
            $data->formulaProducts()->delete();
            $data->delete();
            $request->session()->flash('success', 'Data has been deleted.');
            return redirect('redeem-point-formula/tebus-murah');

        } else {
            $request->session()->flash('error', 'Data hasn\'t been deleted.');

            return redirect('redeem-point-formula/tebus-murah');

        }
    }

    public function getData() {

        $data = FormulaRedeem::murah();

        $datatables = app('datatables')->of($data)
            ->addColumn('id', null)
            ->addColumn('action', function($data) {
                $edit = '<a href="' . route('redeem-point-formula.tebus-murah.edit', $data->RDM_RECID).'" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="'.$data->RDM_RECID.'" data-name="'.$data->RDM_DESC.'" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                return $edit . '&nbsp;' . $delete;
            })
            ->addColumn('active', function ($data) {
                if($data->RDM_ACTIVE == 1) {
                    return '<span class="text-success md-check"></span>';
                } else {
                    return '<span class="text-danger md-close"></span>';
                }
            })
            ->addColumn('type', function ($data) {
                if($data->RDM_FORMULA == 1) {
                    return 'Tebus Murah';
                } elseif($data->RDM_FORMULA == 2) {
                    return 'Tebus Gratis';
                }
                return '-';
            })
            ->addColumn('from', function($data) {
                if($data->RDM_FRDATE) {
                    return $data->RDM_FRDATE->format('d/m/Y');
                }
                return '-';
            })
            ->addColumn('to', function($data) {
                if($data->RDM_TODATE) {
                    return $data->RDM_TODATE->format('d/m/Y');
                }
                return '-';
            });

        return $datatables->make(true);
    }
}

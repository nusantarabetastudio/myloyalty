<?php
namespace App\Http\Controllers\Pointo;

use App\Http\Controllers\Controller;
use App\Models\Master\Tenant;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Session, Datatables;

class ExternalTenantController extends Controller {

	private $parent;
	private $parent_link;

	public function __construct() {
		$this->parent = 'External Tenant List';
		$this->parent_link = route('pointo.external-tenant.index');
	}

	public function index() {

		$this->parent = 'Pointo';
        $data['breadcrumbs'] = 'External Tenant List';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

         return view('pointo.external-tenant.index', $data);
	}

	public function create() {
        $data['breadcrumbs'] = 'New External Tenant';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

       return view('pointo.external-tenant.form', $data);
	}

    public function store(Request $request)
    {
		$rules = [
		    'description' => 'required',
		    'card_name' => 'required',
		    'card_code' => 'required'
		];
		$this->validate($request, $rules);

        $input = $request->all();
        $clientNo = substr(Session::get('pnum'), 0, 2);

        $ExternalTenant = new Tenant;
        $ExternalTenant->TNT_RECID = Uuid::uuid();
        $ExternalTenant->TNT_PNUM = '0101';
        $ExternalTenant->TNT_PTYPE = $clientNo;
        $ExternalTenant->TNT_DESC = $input['description'];
        $ExternalTenant->TNT_TEXT = $input['card_name'];
        $ExternalTenant->TNT_UNIT = $input['card_code'];
        $ExternalTenant->TNT_ACTIVE = (isset($input['status']) && $input['status'] == 'on') ? 1 : 0 ;
        $ExternalTenant->save();

        return redirect()->route('pointo.external-tenant.index')->with('notif_success', 'Data has been saved!');
    }

    public function edit($id)
    {
	    $parent = $this->parent;
	    $parent_link = $this->parent_link;
	    $breadcrumbs = 'Update External Tenant';

	    $ExternalTenant = Tenant::findOrFail($id);

	    return view('pointo.external-tenant.form', compact('ExternalTenant', 'parent', 'parent_link', 'breadcrumbs'));
    }
   
   public function update(Request $request, $id)
   {
        $rules = [
		    'description' => 'required',
		    'card_name' => 'required',
		    'card_code' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $clientNo = substr(Session::get('pnum'), 0, 2);

        $ExternalTenant = Tenant::findOrFail($id);
        $ExternalTenant->TNT_DESC = $input['description'];
        $ExternalTenant->TNT_TEXT = $input['card_name'];
        $ExternalTenant->TNT_UNIT = $input['card_code'];
        $ExternalTenant->TNT_ACTIVE = (isset($input['status']) && $input['status'] == 'on') ? 1 : 0 ;
        $ExternalTenant->save();

        return redirect()->route('pointo.external-tenant.index')->with('notif_success', 'Data has been updated!');
   }

    public function destroy(Request $request)
    {
        Tenant::destroy($request->input('id'));
        
        return redirect()->route('pointo.external-tenant.index')->with('success', 'Data has been deleted!');
    }

	public function getData() {
		$clientNo = substr(Session::get('pnum'), 0, 2);
		$ExternalTenanList = Tenant::where('TNT_PNUM', '0101')->where('TNT_PTYPE', $clientNo)->get();

		return Datatables::of($ExternalTenanList)
            ->addColumn('status', function($ExternalTenanList) {
                if ($ExternalTenanList->TNT_ACTIVE == 1) {
                    return '<i class="md-check text-success"></i>';
                } else {
                    return '<i class="md-close text-danger"></i>';
                }
            })
			->addColumn('action', function($ExternalTenanList) {
				$edit = '<a href="' .route('pointo.external-tenant.edit', $ExternalTenanList->TNT_RECID). '" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
				 $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="' . $ExternalTenanList->TNT_RECID . '" data-toggle="modal" data-target="#delete-modal">Delete</button>';

				 return $edit. '&nbsp;' . $delete;
			})
			->make(true);

	}

}
?>
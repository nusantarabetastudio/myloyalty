<?php
namespace App\Http\Controllers\Pointo;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Customer;
use App\Models\Preference;
use Illuminate\Http\Request;
use Session;


class ApiSettingController extends Controller {

	private $parent;
	private $parent_link;

	public function __construct() {
		$this->parent = 'Pointo';
		$this->parent_link = '';
	}

	public function index() {
        $breadcrumbs = 'Api Setting';
        $parent = $this->parent;
        $parent_link = $this->parent_link;

        $customers = Customer::orderBy('CUST_CODE', 'DESC')->get();
        $client = Client::where('CLN_NO', substr(Session::get('pnum'), 0, 2))->first();

        $pointo = new \stdClass;
        $pointo->url = Preference::where('key', 'pointo_api_url')->first();
        $pointo->url = (!empty($pointo->url)) ? $pointo->url->value : '';
        $pointo->username = Preference::where('key', 'pointo_api_username')->first();
        $pointo->username = (!empty($pointo->username)) ? $pointo->username->value : '';
        $pointo->password = Preference::where('key', 'pointo_api_password')->first();
        $pointo->password = (!empty($pointo->password)) ? $pointo->password->value : '';
        $pointo->token = Preference::where('key', 'pointo_api_token')->first();
        $pointo->token = (!empty($pointo->token)) ? $pointo->token->value : '';

        return view('pointo.api-setting.index', compact('breadcrumbs', 'parent', 'parent_link', 'customers', 'client', 'pointo'));
	}

	public function update(Request $request) {
        $rules = [
            'api_url' => 'required',
            'user' => 'required',
            'password' => 'required',
            'token' => 'required',
            'customer' => 'required'
        ];
        $this->validate($request, $rules);

        $pointoUrl = Preference::where('key', 'pointo_api_url')->first();
        $pointoUrl->value = $request->input('api_url');
        $pointoUrl->save();
        $pointoUsername = Preference::where('key', 'pointo_api_username')->first();
        $pointoUsername->value = $request->input('user');
        $pointoUsername->save();
        $pointoPassword = Preference::where('key', 'pointo_api_password')->first();
        $pointoPassword->value = $request->input('password');
        $pointoPassword->save();
        $pointoToken = Preference::where('key', 'pointo_api_token')->first();
        $pointoToken->value = $request->input('token');
        $pointoToken->save();

        $client = Client::where('CLN_NO', substr(Session::get('pnum'), 0, 2))->first();
        $client->CLN_APEX_CUSTOMER = $request->input('customer');
        $client->save();

        return redirect()->back()->with('success', 'Update Success');
	}
}
?>
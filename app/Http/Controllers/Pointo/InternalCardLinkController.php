<?php

namespace App\Http\Controllers\Pointo;

use App\Http\Controllers\Controller;
use App\Models\Master\CardType;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Session, Datatables;

class InternalCardLinkController extends Controller {

	private $parent;
	private $parent_link;

	public function __construct()
	{
		$this->parent = 'Internal Card Link';
		$this->parent_link = route('pointo.internal-card-link.index');
	}

	public function index()
	{
        $data['breadcrumbs'] = 'Internal Card Link';
        $data['parent'] = 'Pointo';
        $data['parent_link'] = '';

        return view('pointo.internal-card-link.index', $data);
	}

	public function create()
	{
        $data['breadcrumbs'] = 'Create Internal Card';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['cardTypes'] = CardType::where('CARD_PNUM', Session::get('pnum'))
                                ->where('CARD_PTYPE', Session::get('ptype'))
                                ->where('CARD_ACTIVE', 1)
                                ->get();

    	return view('pointo.internal-card-link.form', $data);
	}

    public function store(Request $request)
    {
        $rules = [
            'card_code' => 'required',
            'card_type' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $clientNo = substr(Session::get('pnum'), 0, 2);

        $internalCardLink = new CardType;
        $internalCardLink->CARD_RECID = Uuid::uuid();
        $internalCardLink->CARD_PNUM = '0101';
        $internalCardLink->CARD_PTYPE = $clientNo;
        $internalCardLink->CARD_DESC = $input['card_code'];
        $internalCardLink->CARD_GROUP = $input['card_type'];
        $internalCardLink->CARD_ACTIVE = (isset($input['status']) && $input['status'] == 'on') ? 1 : 0;
        $internalCardLink->save();

        return redirect()->route('pointo.internal-card-link.index')->with('notif_success', 'Data has been saved!');
    }

    public function edit($id)
    {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Update Internal Card';

        $internalCardLink = CardType::findOrFail($id);
        $cardTypes = CardType::where('CARD_PNUM', Session::get('pnum'))
                                ->where('CARD_PTYPE', Session::get('ptype'))
                                ->where('CARD_ACTIVE', 1)
                                ->get();

        return view('pointo.internal-card-link.form', compact('internalCardLink', 'cardTypes', 'parent', 'parent_link', 'breadcrumbs'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'card_code' => 'required',
            'card_type' => 'required'
        ];
        $this->validate($request, $rules);

        $input = $request->all();
        $clientNo = substr(Session::get('pnum'), 0, 2);

        $internalCardLink = CardType::findOrFail($id);
        $internalCardLink->CARD_DESC = $input['card_code'];
        $internalCardLink->CARD_GROUP = $input['card_type'];
        $internalCardLink->CARD_ACTIVE = (isset($input['status']) && $input['status'] == 'on') ? 1 : 0;
        $internalCardLink->save();

        return redirect()->route('pointo.internal-card-link.index')->with('notif_success', 'Data has been updated!');
    }

    public function destroy(Request $request)
    {
        CardType::destroy($request->input('id'));
        
        return redirect()->route('pointo.internal-card-link.index')->with('success', 'Data has been deleted!');
    }

	public function getData()
	{
        $clientNo = substr(Session::get('pnum'), 0, 2);
        $internalCardLinks = CardType::with('parent')->where('CARD_PNUM', '0101')->where('CARD_PTYPE', $clientNo)->select('*');

        return Datatables::of($internalCardLinks)
                    ->addColumn('status', function($internalCardLinks) {
                        if ($internalCardLinks->CARD_ACTIVE == 1) {
                            return '<i class="md-check text-success"></i>';
                        } else {
                            return '<i class="md-close text-danger"></i>';
                        }
                    })
                    ->addColumn('action', function($internalCardLinks) {
                        $edit = '<a href="' . route('pointo.internal-card-link.edit', $internalCardLinks->CARD_RECID) . '" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                        $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="' . $internalCardLinks->CARD_RECID . '" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                        return $edit . '&nbsp;' . $delete;
                    })
                    ->make(true);
	}
}
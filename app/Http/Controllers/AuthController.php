<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Collection;
use Redirect;
use Session;

class AuthController extends Controller
{

    public function __construct()
    {
    }

    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'auth/login';
        $data = $request->only(['username', 'password']);
        $client = new Client(['http_errors' => false]);

        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if($response->getStatusCode() == 202) {
            $contents = json_decode($response->getBody()->getContents());
            $user = $contents->DATA->USER_DATA;
            $data = $contents->DATA;
            $role = $contents->DATA->ROLE_DATA;
            $menu = $contents->DATA->MENU_DATA;

            Session::set('data', $data);
            Session::set('pnum', $data->PNUM);
            Session::set('ptype', $data->PTYPE);
            Session::set('user', $user);
            Session::set('menu_data', $menu);
            Session::set('role_data', $role);

            $uriLogs = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'user/'.Session::get('user')->USER_USERNAME.'/logs';
            $clientLogs = new Client(['http_errors' => false]);
            $responseLogs = $clientLogs->request('POST', $uriLogs, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
                ],
                'form_params' => [
                    'IP' => $request->ip(),
                    'type' => 'Login'
                ]
            ]);
            $request->session()->flash('success_login', $contents->MSG);
            return redirect('customer/search');
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);
            return redirect('login');
        } else {
            return redirect('login')->with('msg', 'Something Went Wrong!');
        }
    }

    public function logout(Request $request){
        $uriLogs = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'user/'.Session::get('user')->USER_USERNAME.'/logs';
        $clientLogs = new Client(['http_errors' => false]);
        $responseLogs = $clientLogs->request('POST', $uriLogs, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'IP' => $request->ip(),
                'type' => 'Logout'
            ]
        ]);
        Session::flush();
        $request->session()->flash('success', 'Log Out Successful.');
        return redirect('login');
    }

    public function getForgotPassword()
    {
        return view('auth.passwords.email');
    }

    public function postForgotPassword(Request $request)
    {
        $this->validate($request, [
            'username' => 'required'
        ]);
        $data = $request->all();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/user/'. $data['username'] . '/forgot-password';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['msg'] = $contents->MSG;
            $request->session()->flash('msg', $data['msg']);

            return redirect('forgot-password');
        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $request->session()->flash('error', 'Username Not Found');

            return redirect('forgot-password');
        } else {
            return redirect('login');
        }
    }

    public function getVerifyCode(Request $request)
    {
        return view('auth.verify-code');
    }

    public function postVerifyCode(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'resetCode' => 'required'
        ]);
        $client = new Client(['http_errors' => false]);
        $data = $request->all();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/user/' .  $data['username'] . '/forgot-password/verify/' . $data['resetCode'];
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);

        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $request->session()->flash('msg', $contents->MSG);

            return redirect('verify/password');
        }
        elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $request->session()->flash('error', 'Your Code is Incorrect');
            return redirect('verify/code');
        }
        else {
            return redirect('login');
        }
    }

    public function getPasswordResetWithCode(Request $request)
    {
        return view('auth.passwords.reset');
    }

    public function postPasswordResetWithCode(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'resetCode' => 'required',
            'newPassword' => 'required'
        ]);
        $client = new Client(['http_errors' => false]);
        $data = $request->all();
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/user/' .  $data['username'] . '/change-password/' . $data['resetCode'];
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'newPassword' => $data['newPassword']
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $request->session()->flash('success', $contents->MSG);
            return redirect('login');

        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $request->session()->flash('error', 'Your Code is Incorrect');
            return redirect('verify/password');
        } else {
            return redirect('login');
        }
    }
}

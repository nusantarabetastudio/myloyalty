<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Master\Tenant;
use App\Models\Admin\Post;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use Session, DB;

class FrequencyTransactionStoreController extends Controller
{
    public function ShowFilter() {

		$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();
    	return view('report.filter.frequency-transaction-store', compact('tenants'));
    }

    public function getQuery($fromDate, $toDate, $fromBarcode, $toBarcode, $frequencyGreaterThan, $store, $sortBy)
    {
		$query = Post::select([
			'TNT_CODE',
			'CUST_BARCODE',
			'CUST_NAME',
			'POS_POST_DATE',
			DB::raw('SUM(POS_AMOUNT) AS amount'),
			DB::raw('SUM(POS_POINT_REGULAR + POS_POINT_BONUS + POS_POINT_REWARD) AS points'),
			DB::raw('COUNT(POS_RECID) AS frequency'),
			])
			->leftJoin('Z_Tenant', 'TNT_RECID', '=', 'POS_STORE')
			->leftJoin('Customers', 'CUST_RECID', '=', 'POS_CUST_RECID')
			->whereBetween('POS_POST_DATE', [$fromDate, $toDate])
			->whereBetween('CUST_BARCODE', [$fromBarcode, $toBarcode])
			->where('POS_PNUM', Session::get('pnum'))
			->where('POS_PTYPE', Session::get('ptype'))
			->groupBy(['TNT_CODE', 'CUST_BARCODE', 'CUST_NAME', 'POS_POST_DATE'])
			->having(DB::raw('COUNT(POS_RECID)'), '>', $frequencyGreaterThan)
			->orderBy('TNT_CODE');
		if ($store != 'All') {
			$query->where('TNT_CODE', $store);
		}
		if ($sortBy == 'Posting Date') {
			$query->orderBy('POS_POST_DATE', 'ASC');
		} else {
			$query->orderBy(DB::raw('count(POS_RECID)'), 'asc');
		}

		return $query;
    }

    public function getReportParams(Request $request)
    {
        $this->validate($request, [
            'fromDate' => 'required',
            'toDate' => 'required',
            'frombarcode' => 'required',
            'tobarcode' => 'required',
            'store' => 'required',
            'sortBy' => 'required'
        ]);

    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$fromBarcode =  $request->input('frombarcode');
    	$toBarcode = $request->input('tobarcode');
    	$store = $request->input('store');
    	$frequencyGreaterThan = $request->input('frequencyGreaterThan', 0);
    	$limit = $request->input('limit');
    	$sortBy = $request->input('sortBy');
    	$TNT_DESC = '';
    	
    	if($store == 'All'){
    		$store = 'All';
    	}else {
    		$query = Tenant::where('TNT_RECID', $store)->first();
    		$store = $query->TNT_CODE;
    		$TNT_DESC = $query->TNT_DESC;
    	}
    	$title = 'Transaction Frequency By Store';
    	$meta = [
    		'Store' => $store .'-'. $TNT_DESC, 
    		'Date' => $fromDate . ' To ' . $toDate,
    		'Sort By' => $sortBy,
    		'Barcode' => $fromBarcode . ' To ' . $toBarcode
    	];

    	$query = $this->getQuery($fromDate, $toDate, $fromBarcode, $toBarcode, $frequencyGreaterThan, $store, $sortBy);

		$columns = [
			'Store' => 'TNT_CODE',
			'Member ID' => 'CUST_BARCODE',
			'Customer Name' => 'CUST_NAME',
			'Date' => 'POS_POST_DATE',
			'Amount' => 'amount',
			'Point' => 'points',
			'Freq' => 'frequency'
		];

		return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
    	return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
					->editColumn('Date', [
						'displayAs' => function($result) {
							return $result->POS_POST_DATE->format('d M\'y');
						}
					])
					->editColumn('Amount', [
						'class' => 'right',
						'displayAs' => function($result) {
							return 'IDR ' . thousandSeparator($result->amount);
						}
					])
					->editColumn('Point', [
						'class' => 'right',
						'displayAs' => function($result) {
							return thousandSeparator($result->points);
						}
					])
					->editColumn('Freq', [
						'class' => 'right'
					])
					->limit($params['limit'])
					->groupBy('Store')
					->showTotal(['Amount' => 'idr', 'Point' => 'point', 'Freq' => 'point']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->download('Transaction Frequency By Store' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->download('Transaction Frequency By Store' . date('d M Y'));
    }

}

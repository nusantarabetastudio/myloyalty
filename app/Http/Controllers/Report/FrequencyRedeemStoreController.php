<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Admin\RedeemPost;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use Session, DB;

class FrequencyRedeemStoreController extends Controller
{
    public function ShowFilter()
    {
		$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))
						->where('TNT_PTYPE', Session::get('ptype'))
						->get();

    	return view('report.filter.frequency-redeem-store', compact('tenants'));
    }

    public function getQuery($fromDate, $toDate, $fromBarcode, $toBarcode, $frequencyGreaterThan, $store, $sortBy)
    {
    	$query = RedeemPost::select([
    							'TNT_CODE',
    							'c.CUST_BARCODE',
    							'c.CUST_NAME',
    							'post_date',
    							DB::raw('SUM(price) as total_price'),
    							DB::raw('SUM(point) as total_point'),
    							DB::raw('count(redeem_posts.id) as frequency'),
    						])
					    	->leftJoin('z_tenant as t', 't.TNT_RECID', '=', 'redeem_posts.tenant_id')
					    	->leftJoin('customers as c', 'c.CUST_RECID', '=', 'redeem_posts.customer_id')
    						->whereBetween('post_date', [$fromDate, $toDate])
    						->whereBetween('c.CUST_BARCODE', [$fromBarcode, $toBarcode])
					    	->where('pnum', Session::get('pnum'))
					    	->where('ptype', Session::get('ptype'))
					    	->groupBy(['TNT_CODE', 'c.CUST_BARCODE', 'c.CUST_NAME', 'post_date'])
					    	->having(DB::raw('count(redeem_posts.id)'), '>', $frequencyGreaterThan)
					    	->orderBy('t.TNT_CODE');

		if ($store != 'All') {
			$query->where('TNT_CODE', $store);
		}
		if ($sortBy == 'Posting Date') {
			$query->orderBy('POST_DATE', 'ASC');
		} else {
			$query->orderBy(DB::raw('count(redeem_posts.id)'), 'DESC');
		}

		return $query;
    }

    public function getReportParams(Request $request)
    {
		$this->validate($request, [
			'fromDate' => 'required',
			'toDate' => 'required',
			'fromBarcode' => 'required',
			'toBarcode' => 'required',
			'store' => 'required',
			'sortBy' => 'required'
		]);

    	$limit = $request->input('limit') ?: 2000;
    	$limit = $limit <= 2000 ? $limit : 2000;
    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$fromBarcode = $request->input('fromBarcode');
    	$toBarcode = $request->input('toBarcode');
    	$store = $request->input('store');
    	$sortBy = $request->input('sortBy');
    	$frequencyGreaterThan = $request->input('frequencyGreaterThan', 0);

    	$title = 'Frequency Redeem By Store';
    	$meta = [
    		'Store' => ($request->input('store') == 'All') ? $request->input('store') : Tenant::findOrFail($request->input('store'))->TNT_CODE,
    		'Date' => $fromDate . ' To ' . $toDate,
    		'Sort By' => $sortBy,
    		'Barcode' => $fromBarcode . ' To ' . $toBarcode,
    		'Frequency Greater than' => $frequencyGreaterThan
    	];

    	$query = $this->getQuery($fromDate, $toDate, $fromBarcode, $toBarcode, $frequencyGreaterThan, $store, $sortBy);

 		$columns = [
			'Store' => 'TNT_CODE',
			'Member ID' => 'CUST_BARCODE',
			'Customer Name' => 'CUST_NAME',
			'Date' => 'post_date',
			'Amount' => 'total_price',
			'Point' => 'total_point',
			'Frequency' => 'frequency'
		];

		return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
    	return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
			->editColumn('Date', [
				'displayAs' => function($result) {
					return $result->post_date->format('d M\'y');
				}
			])
			->editColumn('Amount', [
				'class' => 'right',
				'displayAs' => function($result) {
					return 'IDR ' . thousandSeparator($result->total_price);
				}
			])
			->editColumn('Point', [
				'class' => 'right',
				'displayAs' => function($result) {
					return thousandSeparator($result->total_point);
				}
			])
			->editColumn('Frequency', [
				'class' => 'right'
			])
			->limit($params['limit'])
			->groupBy('Store')
			->showTotal(['Amount' => 'idr', 'Point' => 'point', 'Frequency' => 'point']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->download('Frequency Redeem Store Filter ' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->download('Frequency Redeem Store Filter' . date('d M Y'));
    }
}

<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Master\Tenant;
use App\Models\Admin\Post;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;

class CustomerEarningDetailController extends Controller
{
    public function showFilter()
    {
    	$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();
    	return view('report.filter.customer-earning-detail', compact('tenants'));
    }

	public function getGroupingByBarcodeQuery($store, $fromDate, $toDate, $fromBarcode, $toBarcode)
	{
		$query = Post::leftJoin('z_tenant', 'post.POS_STORE', '=', 'z_tenant.TNT_RECID')
					->leftJoin('Customers', 'post.POS_CUST_RECID', '=', 'Customers.CUST_RECID')
					->whereBetween('Customers.CUST_BARCODE', [$fromBarcode, $toBarcode])
					->whereBetween('POS_POST_DATE', [$fromDate, $toDate])
					->orderBy('POS_POST_DATE', 'ASC')
					->orderBy('Customers.CUST_BARCODE', 'ASC');
		if ($store != 'All') {
			$query->where('POS_STORE', $store);
		}

		return $query;
	}

	public function getGroupingByStoreQuery($store, $fromDate, $toDate, $fromBarcode, $toBarcode)
	{
		$query = Post::leftJoin('z_tenant', 'post.POS_STORE', '=', 'z_tenant.TNT_RECID')
					->leftJoin('Customers', 'post.POS_CUST_RECID', '=', 'Customers.CUST_RECID')
					->whereBetween('Customers.CUST_BARCODE', [$fromBarcode, $toBarcode])
					->whereBetween('post.POS_POST_DATE', [$fromDate, $toDate])
					->orderBy('post.POS_POST_DATE', 'ASC')
					->orderBy('z_tenant.TNT_CODE', 'ASC');
		if ($store != 'All') {
			$query->where('POS_STORE', $store);
		}

		return $query;
	}

    public function getQuery($groupBy, $store, $fromDate, $toDate, $fromBarcode, $toBarcode)
    {
		if (strtolower($groupBy) == 'store') {
			$query = $this->getGroupingByStoreQuery($store, $fromDate, $toDate, $fromBarcode, $toBarcode);
		} elseif (strtolower($groupBy) == 'member id') {
			$query = $this->getGroupingByBarcodeQuery($store, $fromDate, $toDate, $fromBarcode, $toBarcode);
		}

		return $query;
    }

	public function getReportParams(Request $request)
	{
    	$this->validate($request, [
    		'fromDate' => 'required',
    		'toDate' => 'required',
    		'fromBarcode' => 'required',
    		'toBarcode' => 'required',
    		'store' => 'required',
    		'groupBy' => 'required'
    	]);

    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$fromBarcode = $request->input('fromBarcode');
    	$toBarcode = $request->input('toBarcode');
    	$store = $request->input('store');
    	$limit = $request->input('limit') ? : 2000;
    	$limit = $limit <= 2000 ? $limit : 2000;
		$groupBy = $request->input('groupBy');

		$title = 'Customer Earning by ' . $groupBy . ' - Detail';
		$meta = [
			'Date' => $fromDate . ' To ' . $toDate,
			'Store' => ($request->input('store') == 'All') ? $request->input('store') : Tenant::findOrFail($request->input('store'))->TNT_CODE,
			'Grouping By' => $groupBy
		];

		$query = $this->getQuery($groupBy, $store, $fromDate, $toDate, $fromBarcode, $toBarcode);

		$columns = [
			'Store' => 'TNT_CODE',
			'Member ID' => 'CUST_BARCODE',
			'Name' => 'CUST_NAME',
			'Loyal Data' => function($result){
				return $result->POS_POST_DATE->format('d M Y');
			},
			'POS Date' => function($result) {
				return $result->POS_POST_DATE->format('d M Y');
			},
			'Receipt No' => 'POS_RECEIPT_NO',
			'Total Receipt' => 'POS_AMOUNT',
			'Earning' => function($result) {
				return $result->POS_POINT_REGULAR + $result->POS_POINT_BONUS;
			},
			'Reward' => 'POS_POINT_REWARD',
			'Total' => function($result) {
				return $result->POS_POINT_REGULAR + $result->POS_POINT_BONUS + $result->POS_POINT_REWARD;
			},
			'Void' => 'POS_VOID',
			'User ID' => 'POS_CREATEBY'
		];

        return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params, $groupBy)
    {
		return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
					->editColumn('Total Receipt', [
						'class' => 'right',
						'displayAs'	=> function($result) {
							return 'IDR '. thousandSeparator($result->POS_AMOUNT);
						}
					])
					->editColumn('Earning', [
						'class' => 'right',
						'displayAs'	=> function($result) {
							return thousandSeparator($result->POS_POINT_REGULAR + $result->POS_POINT_BONUS);
						}
					])
					->editColumn('Reward', [
						'class' => 'right',
						'displayAs'	=> function($result) {
							return thousandSeparator($result->POS_POINT_REWARD);
						}
					])
					->editColumn('Total', [
						'class' => 'right',
						'displayAs'	=> function($result) {
							return thousandSeparator($result->POS_POINT_REGULAR + $result->POS_POINT_BONUS + $result->POS_POINT_REWARD);
						}
					])
					->groupBy(strtolower($groupBy) == 'store' ? 'Store' : 'Member ID')
					->showTotal([
						'Total Receipt' => 'idr', 
						'Earning' => 'point',
						'Reward' => 'point', 
						'Total' => 'point'
					])
					->setOrientation('landscape')
					->limit($params['limit']);
	}

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params, $request->input('groupBy'));

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params, $request->input('groupBy'));

        return $pdfReport->download('Customer Earning Detail ' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params, $request->input('groupBy'));

        return $excelReport->simple()->download('Customer Earning Detail ' . date('d M Y'));
    }
}

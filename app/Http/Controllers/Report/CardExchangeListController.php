<?php

namespace App\Http\Controllers\Report;

use DB, Session;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Master\Tenant;
use App\Models\CustomerExchange;
use App\Http\Controllers\Controller;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\CSVReport;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;

class CardExchangeListController extends Controller
{
	public function showFilter()
	{
		$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();

		return view('report.filter.card-exchange-list', compact('tenants'));
	}

    public function getQuery(Request $request)
    {
        $fromDate = Carbon::parse($request->fromDate);
        $toDate = Carbon::parse($request->toDate)->addDay(1)->subSecond();
        $fromBarcode = $request->fromBarcode;
        $toBarcode = $request->toBarcode;
        $type = $request->type;
        $store = $request->store;

    	switch ($type) {
			case 'New Register':
				$type = 7;
				break;
			case 'Migrate':
				$type = 6;
				break;
			case 'Replacement':
				$type = 3;
				break;
		}

        $query = CustomerExchange::selectRaw("
                    Z_Tenant.TNT_CODE AS TNT_CODE,
                    Customer_Exchange.EXC_DATE as EXC_DATE,
                    CASE WHEN EXC_TYPE = 1 THEN 'Upgrade'
                        WHEN EXC_TYPE = 2 THEN 'Downgrade'
                        WHEN EXC_TYPE = 3 THEN 'Replacement'
                        WHEN EXC_TYPE = 4 THEN 'Lost'
                        WHEN EXC_TYPE = 5 THEN 'Extend'
                        WHEN EXC_TYPE = 6 THEN 'Migration'
                        WHEN EXC_TYPE = 7 THEN 'New Register'
                        WHEN EXC_TYPE = 8 THEN 'Edit profile'
                        ELSE 'Others'
                    END as EXC_TYPE,
                    Customer_Exchange.EXC_OLDCARD,
                    Customer_Exchange.EXC_NEWCARD,
                    Customers.CUST_NAME,
                    Customer_Exchange.EXC_USER
                    ")
                    ->leftJoin('Z_Tenant', 'Z_Tenant.TNT_RECID', '=', 'Customer_Exchange.EXC_TNT_RECID')
                    ->leftJoin('Customers', 'Customers.CUST_RECID', '=', 'Customer_Exchange.EXC_CUST_RECID')
                    ->whereNotNull('EXC_TNT_RECID')
                    ->whereBetween('Customers.CUST_BARCODE', [$fromBarcode, $toBarcode])
                    ->whereBetween('Customer_Exchange.EXC_DATE', [$fromDate, $toDate])
                    ->when($store != 'All', function($qry) use($store) {
                        return $qry->where('Customer_Exchange.EXC_TNT_RECID', $store);
                    })
                    ->when($type != 'All', function($qry) use($type) {
                        return $qry->where('Customer_Exchange.EXC_TYPE', $type);
                    });

		return $query;
    }

    public function getReportParams(Request $request)
    {
    	$this->validate($request, [
    		'fromDate' => 'required',
    		'toDate' => 'required',
    		'fromBarcode' => 'required',
    		'toBarcode' => 'required',
    		'store' => 'required',
    		'type' => 'required',
    	]);

    	$limit = $request->input('limit');
    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$fromBarcode = $request->input('fromBarcode');
    	$toBarcode = $request->input('toBarcode');
    	$store = $request->input('store');
    	$type = $request->input('type');

    	$title = 'Card Exchange List';
    	$meta = [
    		'Date' => $fromDate . ' To ' . $toDate,
    		'Barcode' => $fromBarcode . ' To ' . $toBarcode,
    		'Store' => ($request->input('store') == 'All') ? $request->input('store') : Tenant::findOrFail($request->input('store'))->TNT_CODE,
    		'Type' => $type
    	];

    	$query = $this->getQuery($request);

    	$columns = [
            'Store' => 'TNT_CODE',
            'Date' => 'EXC_DATE',
            'Type' => 'EXC_TYPE',
            'Old Barcode' => 'EXC_OLDCARD',
            'New Barcode' => 'EXC_NEWCARD',
            'Name' => 'CUST_NAME',
            'User' => 'EXC_USER',
    	];

    	return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
		return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
					->limit($params['limit']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->download('Card Exchange ' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->withoutManipulation()->download('Card Exchange ' . date('d M Y'));
    }

    public function downloadCSVReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $csvReport = $this->generateReport(new CSVReport, $params);

        return $csvReport->showMeta()->withoutManipulation()->download('Card Exchange ' . date('d M Y'));
    }
}

<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\CSVReport;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;
use Carbon\Carbon;

class PointBalanceCustomerController extends Controller
{
    public function showFilter()
    {
        return view('report.filter.point-balance-customer');

    }

    public function getQuery($date, $fromBarcode, $toBarcode, $orderBy, $sortBy)
    {
        $pnum = Session::get('pnum');
        $ptype = Session::get('ptype');

        $query = Customer::from(DB::raw("
                    (
                    SELECT result.*, result.earning - result.redemption as balance
                    FROM
                        (
                        SELECT 
                            Customers.CUST_BARCODE,
                            Customers.CUST_NAME,
                            ISNULL(Sum(Post.POS_POINT_REWARD) + Sum(Post.POS_POINT_REGULAR) + SUM(Post.POS_POINT_BONUS),0) AS earning,
                            ISNULL((SELECT SUM(redeem_posts.point) FROM redeem_posts WHERE redeem_posts.customer_id = Customers.CUST_RECID AND redeem_posts.is_void = 0 AND redeem_posts.post_date <= '".$date."'),0) AS redemption
                        FROM Customers
                        LEFT JOIN Post
                            ON Customers.CUST_RECID = Post.POS_CUST_RECID AND POS_VOID = 0
                        WHERE
                            Post.POS_POST_DATE <='".$date."'
                        AND Customers.CUST_PNUM = '".$pnum."'
                        AND Customers.CUST_PTYPE = '".$ptype."'
                        GROUP BY
                            Customers.CUST_BARCODE,
                            Customers.CUST_NAME,
                            Customers.CUST_RECID
                        ) as result
                    ) as results
                "))
                ->orderBy('results.' . $orderBy, $sortBy == 'Descending' ? 'DESC' : 'ASC');

        return $query;
    }

    public function getReportParams(Request $request)
    {
        $this->validate($request, [
            'periode' => 'required',
            'fromBarcode' => 'required',
            'toBarcode' => 'required',
            'orderBy' => 'required',
            'sortBy' =>'required'
        ]);

        $date = $request->input('periode');
        $fromBarcode = $request->input('fromBarcode');
        $toBarcode = $request->input('toBarcode');
        $orderBy = $request->input('orderBy');
        $limit = $request->input('limit') ;
        $sortBy = $request->input('sortBy');

        $query = $this->getQuery($date, $fromBarcode, $toBarcode, $orderBy, $sortBy);

        $title = 'Point Balance Customer';
        $meta = [
            'As Of ' => $date,
            'Order By' => $orderBy,
            'Sort By' => $sortBy
        ];
                        
        $columns = [
            'Member ID' => 'CUST_BARCODE',
            'Name'  => 'CUST_NAME',
            'Total Earning' => 'earning',
            'Total Redem' => 'redemption',
            'Point Balance' => 'balance'
        ];

        return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
        return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
                    ->limit($params['limit']);
    }


    public function displayPdfReport(Request $request) 
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->stream();
    }

    public function downloadPdfReport(Request $request) 
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->download('Point Balance Customer  ' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request) 
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->withoutManipulation()->download('Point Balance Customer ' . date('d M Y'));
    }

    public function downloadCSVReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $csvReport = $this->generateReport(new CSVReport, $params);

        return $csvReport->showMeta()->withoutManipulation()->download('Point Balance Customer ' . date('d M Y'));
    }
}

<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;

class CustomerRedemptionSummaryController extends Controller
{
	public function showFilter()
	{
		$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();

		return view('report.filter.customer-redemption-summary', compact('tenants'));
	}

    public function getQueryByStore($fromDate, $toDate, $store, $fromBarcode, $toBarcode)
    {
    	$query = Tenant::select(['TNT_CODE as tenant_code', 'CUST_BARCODE as barcode', 'CUST_NAME as name', DB::raw("SUM(rp.point) as redeemed_points"), 'cp.point'])
					->leftJoin('redeem_posts as rp', 'rp.tenant_id', '=', 'z_tenant.TNT_RECID')
					->leftJoin('customers as c', 'c.CUST_RECID', '=', 'rp.customer_id')
					->leftJoin('customer_points as cp', 'cp.customer_id', '=', 'c.CUST_RECID')
					->whereNotNull('CUST_NAME')
					->where('cp.code', '=', 2)
					->where('TNT_PNUM', Session::get('pnum'))
					->where('TNT_PTYPE', Session::get('ptype'));
		if ($store != 'All') {
			$query->where('TNT_RECID', $store);
		}
		$query->whereBetween('rp.post_date', [$fromDate, $toDate])
			->whereBetween('c.cust_barcode', [$fromBarcode, $toBarcode])
			->groupBy(['TNT_CODE', 'CUST_BARCODE', 'CUST_NAME', 'cp.point', 'rp.post_date'])
			->orderBy('rp.post_date', 'ASC')
			->orderBy('TNT_CODE', 'ASC');

		return $query;
    }

    public function getQueryByBarcode($fromDate, $toDate, $store, $fromBarcode, $toBarcode)
    {
    	$query = Customer::select(['CUST_BARCODE as barcode', 'CUST_NAME as name', DB::raw("SUM(rp.point) as redeemed_points"), 'cp.point'])
					->leftJoin('redeem_posts as rp', 'customers.CUST_RECID', '=', 'rp.customer_id')
					->leftJoin('customer_points as cp', 'cp.customer_id', '=', 'customers.CUST_RECID')
					->whereNotNull('CUST_NAME')
					->where('cp.code', '=', 2);
		if ($store != 'All') {
			$query->where('rp.tenant_id', $store);
		}
		$query->whereBetween('rp.post_date', [$fromDate, $toDate])
			->whereBetween('customers.cust_barcode', [$fromBarcode, $toBarcode])
			->groupBy(['CUST_BARCODE', 'CUST_NAME', 'cp.point', 'rp.post_date'])
			->orderBy('rp.post_date', 'ASC')
			->orderBy('CUST_BARCODE', 'ASC');

		return $query;
    }

    public function getReportParams(Request $request)
    {
		$this->validate($request, [
			'fromDate' => 'required',
			'toDate' => 'required',
			'fromBarcode' => 'required',
			'toBarcode' => 'required',
			'store' => 'required',
			'groupBy' => 'required',
		]);

		$groupBy = $request->input('groupBy');
    	$limit = $request->input('limit') ;
    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$store = $request->input('store');
    	$fromBarcode = $request->input('fromBarcode');
    	$toBarcode = $request->input('toBarcode');
    	$title = 'Customer Redemption by ' . $groupBy . ' - Summary';
    	$meta = [
    		'Date' => $fromDate . ' To ' . $toDate,
    		'Barcode' => $fromBarcode . ' To ' . $toBarcode,
    		'Store' => ($request->input('store') == 'All') ? $request->input('store') : Tenant::findOrFail($request->input('store'))->TNT_CODE,
    		'Grouping By' => $groupBy
    	];
		if ($groupBy == 'Store') {
			$query = $this->getQueryByStore($fromDate, $toDate, $store, $fromBarcode, $toBarcode);
		} elseif ($groupBy == 'Member ID') {
			$query = $this->getQueryByBarcode($fromDate, $toDate, $store, $fromBarcode, $toBarcode);
		}
		

		$columns = [
			'Store' => 'tenant_code',
			'Member ID' => 'barcode',
			'Name' => 'name',
			'Redeemed Points' => 'redeemed_points',
			'Point Balance' => 'point'
		];

        return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
        return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
				->editColumn('Redeemed Points', [
					'class' => 'right', 
					'displayAs' => function($result) {
						return thousandSeparator($result->redeemed_points);
					}
				])
				->editColumn('Point Balance', [
					'class' => 'right', 
					'displayAs' => function($result) {
						return thousandSeparator($result->point);
					}
				])
				->showTotal(['Redeemed Points' => 'point', 'Point Balance' => 'point'])
				->limit($params['limit']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->download('Customer Redemtion Summary' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->simple()->download('Customer Redemtion Summary' . date('d M Y'));
    }

}

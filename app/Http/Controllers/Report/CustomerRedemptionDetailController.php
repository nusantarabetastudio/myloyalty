<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Admin\RedeemPostDetail;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;

class CustomerRedemptionDetailController extends Controller
{
    public function showFilter()
    {
    	$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();

    	return view('report.filter.customer-redemption-detail', compact('tenants'));
    }

    public function getQueryByStore($store, $fromDate, $toDate, $fromBarcode, $toBarcode)
    {
    	$query = RedeemPostDetail::select([
		    			'TNT_CODE', 'CUST_BARCODE', 'CUST_NAME', 'post_date', 'receipt_no', 'rpd.point', DB::raw('(SELECT point FROM customer_points WHERE code = 2 AND customer_id = c.CUST_RECID) as point_balance'), 'rpro.description', 'rpd.qty', 'rp.is_void', 'rp.posted_by'
		    		])
    				->from('redeem_post_details as rpd')
					->leftJoin('redeem_posts as rp', 'rp.id', '=', 'rpd.redeem_post_id')
					->leftJoin('customers as c', 'c.CUST_RECID', '=', 'rp.customer_id')
					->leftJoin('z_tenant as t', 't.TNT_RECID', '=', 'rp.tenant_id')
					->leftJoin('redeem_products as rpro', 'rpro.id', '=', 'rpd.redeem_product_id')
					->where('CUST_PNUM', Session::get('pnum'))
					->where('CUST_PTYPE', Session::get('ptype'))
					->where('TNT_PNUM', Session::get('pnum'))
					->where('TNT_PTYPE', Session::get('ptype'));
		if ($store != 'All') {
			$query->where('t.TNT_RECID', $store);
		}
		$query->whereBetween('rp.post_date', [$fromDate, $toDate])
			->whereBetween('c.CUST_BARCODE', [$fromBarcode, $toBarcode])
			->orderBy('rp.post_date', 'ASC')
			->orderBy('t.TNT_CODE', 'ASC');

		return $query;
    }

    public function getQueryByMemberId($store, $fromDate, $toDate, $fromBarcode, $toBarcode)
    {
    	$query = RedeemPostDetail::select([
		    			'TNT_CODE', 'CUST_BARCODE', 'CUST_NAME', 'post_date', 'receipt_no', 'rpd.point', DB::raw('(SELECT point FROM customer_points WHERE code = 2 AND customer_id = c.CUST_RECID) as point_balance'), 'rpro.description', 'rpd.qty', 'rp.is_void', 'rp.posted_by'
		    		])
    				->from('redeem_post_details as rpd')
					->leftJoin('redeem_posts as rp', 'rp.id', '=', 'rpd.redeem_post_id')
					->leftJoin('customers as c', 'c.CUST_RECID', '=', 'rp.customer_id')
					->leftJoin('z_tenant as t', 't.TNT_RECID', '=', 'rp.tenant_id')
					->leftJoin('redeem_products as rpro', 'rpro.id', '=', 'rpd.redeem_product_id')
					->where('CUST_PNUM', Session::get('pnum'))
					->where('CUST_PTYPE', Session::get('ptype'))
					->where('TNT_PNUM', Session::get('pnum'))
					->where('TNT_PTYPE', Session::get('ptype'));
		if ($store != 'All') {
			$query->where('t.TNT_RECID', $store);
		}
		$query->whereBetween('rp.post_date', [$fromDate, $toDate])
			->whereBetween('c.CUST_BARCODE', [$fromBarcode, $toBarcode])
			->orderBy('rp.post_date', 'ASC')
			->orderBy('c.CUST_BARCODE', 'ASC');

		return $query;
    }

    public function getReportParams(Request $request)
    {
		$this->validate($request, [
			'fromDate' => 'required',
			'toDate' => 'required',
			'fromBarcode' => 'required',
			'toBarcode' => 'required',
			'store' => 'required',
			'groupBy' => 'required',
		]);
		$groupBy= $request->input('groupBy');
    	$limit = $request->input('limit') ;
    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$store = $request->input('store');
    	$fromBarcode = $request->input('fromBarcode');
    	$toBarcode = $request->input('toBarcode');

    	$title = 'Customer Redemption by Store - Detail';
    	$meta = [
    		'Date' => $fromDate . ' To ' . $toDate,
    		'Barcode' => $fromBarcode . ' To ' . $toBarcode,
    		'Store' => ($request->input('store') == 'All') ? $request->input('store') : Tenant::findOrFail($request->input('store'))->TNT_CODE,
    		'Grouping By' => 'Store'
    	];

		if ($groupBy == 'Store') {
			$query = $this->getQueryByStore($store, $fromDate, $toDate, $fromBarcode, $toBarcode);
		} elseif ($groupBy == 'Member ID') {
			$query = $this->getQueryByMemberId($store, $fromDate, $toDate, $fromBarcode, $toBarcode);
		}

		$columns = [
			'Store' => 'TNT_CODE',
			'Member ID' => 'CUST_BARCODE',
			'Name' => 'CUST_NAME',
			'Post Date' => 'post_date',
			'Receipt No' => 'receipt_no',
			'Redeemed Point' => 'point',
			'Point Balance' => 'point_balance',
			'Redeem Product' => 'description',
			'Qty' => 'qty',
			'Void' => 'is_void',
			'User ID' => 'posted_by',
		];

		return compact('title', 'meta', 'query', 'columns', 'limit', 'groupBy');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
    	return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
					->editColumn('Redeemed Point', [
						'class' => 'right',
						'displayAs' => function($result) {
							return thousandSeparator($result->point);
						}
					])
					->editColumn('Point Balance', [
						'class' => 'right',
						'displayAs' => function($result) {
							return thousandSeparator($result->point_balance);
						}
					])
					->groupBy($params['groupBy'])
					->setOrientation('landscape')
					->showTotal(['Redeemed Point' => 'point', 'Point Balance' => 'point'])
					->limit($params['limit']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->download('Customer Redeem Detail' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->simple()->download('Customer Redeem Detail' . date('d M Y'));
    }

}

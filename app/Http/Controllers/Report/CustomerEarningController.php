<?php

namespace App\Http\Controllers\Report;

use PDF, DB, Session;
use App\Http\Requests;
use App\Models\Admin\Post;
use Illuminate\Http\Request;
use App\Models\Master\Tenant;
use App\Http\Controllers\Controller;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\CSVReport;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;

class CustomerEarningController extends Controller
{
    public function showFilter()
    {
    	$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();
    	return view('report.filter.customer-earning', compact('tenants'));
    }

    public function getQuery($fromBarcode, $toBarcode, $fromDate, $toDate, $store)
    {
		$query = Post::selectRaw("
                        TNT_CODE,
                        convert(NVARCHAR, POS_POST_DATE, 106) AS F_POS_POST_DATE,
                        CUST_BARCODE,
                        CUST_NAME,
                        CASE WHEN POS_TYPE = 3 THEN 'RWD' ELSE 'EARN' END AS EARNING_TYPE,
                        POS_AMOUNT,
                        POS_POINT_REGULAR,
                        POS_POINT_BONUS,
                        POS_POINT_REWARD,
                        POS_POINT_REWARD + POS_POINT_REGULAR + POS_POINT_BONUS as TOTAL_EARNING,
                        POS_VOID,
                        POS_CREATEBY
                    ")
                    ->leftJoin('z_tenant', 'post.POS_STORE', '=', 'z_tenant.TNT_RECID')
                    ->leftJoin('customers as c', 'c.CUST_RECID', '=', 'post.POS_CUST_RECID')
					->whereBetween('CUST_BARCODE', [$fromBarcode, $toBarcode])
					->whereBetween('POS_POST_DATE', [$fromDate, $toDate])
					->where('POS_PNUM', Session::get('pnum'))
					->where('POS_PTYPE', Session::get('ptype'))
                    ->orderBy('z_tenant.TNT_CODE', 'ASC')
					->orderBy('POS_POST_DATE', 'ASC')
                    ->when($store != 'All', function($qry) use($store) {
                        return $qry->where('POS_STORE', $store);
                    });

		return $query;
    }

    public function getReportParams(Request $request)
    {
    	$this->validate($request,[
    		'fromDate' => 'required',
    		'toDate' => 'required'
    	]);

    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$fromBarcode = $request->input('fromBarcode');
    	$toBarcode = $request->input('toBarcode');
    	$store = $request->input('store');
    	$limit = $request->input('limit');
    	$groupBy = $request->input('groupBy');

		$title = 'Customer Earning Report';
		$meta = [
			'Store' => ($request->input('store') == 'All') ? 'All' : Tenant::findOrFail($request->input('store'))->TNT_CODE,
			'Date ' => $fromDate . ' To ' . $toDate,
			'Barcode' => $fromBarcode . ' To ' . $toBarcode
		];

		$query = $this->getQuery($fromBarcode, $toBarcode, $fromDate, $toDate, $store);

		$columns = [
    		'Store' => 'TNT_CODE',
			'Date' => 'F_POS_POST_DATE',
			'Barcode' => 'CUST_BARCODE',
			'Name' => 'CUST_NAME',
			'Type'	=> 'EARNING_TYPE',
			'Total Receipt' => 'POS_AMOUNT',
			'Reguler' => 'POS_POINT_REGULAR',
			'Bonus' => 'POS_POINT_BONUS',
			'Reward' => 'POS_POINT_REWARD',
			'Total' => 'TOTAL_EARNING',
			'Void' => 'POS_VOID',
			'User' => 'POS_CREATEBY'
		];

		return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
        return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
					->showTotal([
						'Total Receipt' => 'idr',
						'Reguler' => 'point',
						'Bonus' => 'point',
						'Reward' => 'point',
						'Total' => 'point',
						'Void' => 'point'
					])
					->setOrientation('landscape')
					->limit($params['limit']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->download('Earning ' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->withoutManipulation()->download('Earning ' . date('d M Y'));
    }

    public function downloadCSVReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $csvReport = $this->generateReport(new CSVReport, $params);

        return $csvReport->showMeta()->withoutManipulation()->download('Earning ' . date('d M Y'));
    }

}


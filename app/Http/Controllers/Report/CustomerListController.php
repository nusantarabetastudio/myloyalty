<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\CSVReport;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;

class CustomerListController extends Controller
{
	public function showFilter()
	{
		$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();

		return view('report.filter.customer-list', compact('tenants'));
	}

    public function getQuery($fromDate, $toDate, $fromBarcode, $toBarcode, $active, $store)
    {
		$query = Customer::selectRaw("Z_Tenant.TNT_DESC,
									Z_Tenant.TNT_CODE,
									Customers.CUST_BARCODE,
									Customers.CUST_NAME,
									CONCAT('62', Customers_Phone.PHN_NUMBER) as PHN_NUMBER,
									FORMAT(Customers.CUST_DOB, 'dd-MM-yyyy') as birthday,
									Customers_Email.EML_EMAIL,
									Z_Lookup.LOK_DESCRIPTION,
									Customers_Addr.ADDR_ADDRESS,
									FORMAT(Customers.CUST_JOINDATE, 'dd-MM-yyyy') as created_date,
									(SELECT TOP 1 EXC_DATE FROM Customer_Exchange WHERE EXC_CUST_RECID = CUST_RECID AND EXC_TYPE != '7' ORDER BY EXC_DATE DESC) AS exchange_date,
									(SELECT TOP 1 EXC_USER FROM Customer_Exchange WHERE EXC_CUST_RECID = CUST_RECID) AS created_user,
									(SELECT TOP 1 EXC_USER FROM Customer_Exchange WHERE EXC_CUST_RECID = CUST_RECID AND EXC_TYPE != '7') AS migrate_user")
			    				->leftJoin('Z_Tenant', 'Customers.CUST_STORE', '=', 'Z_Tenant.TNT_RECID')
			    				->leftJoin('Customers_Phone', 'Customers_Phone.PHN_CUST_RECID', '=', 'Customers.CUST_RECID')
			    				->leftJoin('Customers_Email', 'Customers_Email.EML_CUST_RECID', '=', 'Customers.CUST_RECID')
			    				->leftJoin('Z_Lookup', 'Customers.CUST_GENDER', '=', 'Z_Lookup.LOK_RECID')
			    				->leftJoin('Customers_Addr', 'Customers_Addr.ADDR_CUST_RECID', '=', 'Customers.CUST_RECID')
			    				->where('Customers_Phone.PHN_TYPE', 1)
			    				->where('Customers_Email.EML_TYPE', 1)
			    				->whereBetween('Customers.CUST_JOINDATE', [$fromDate, $toDate])
								->whereBetween('Customers.CUST_BARCODE', [$fromBarcode, $toBarcode])
								->where('CUST_ACTIVE', ($active == 'Active') ? 1 : 0)
			    				->orderBy('Customers.CUST_STORE', 'ASC')
			    				->orderBy('Customers.CUST_RECID', 'ASC')
			    				->when($store != 'All', function($qry) use($store) {
			    					return $qry->where('CUST_STORE', $store);
			    				});
		return $query;
    }

    public function getReportParams(Request $request)
    {
		$this->validate($request, [
			'fromDate' => 'required',
			'toDate' => 'required',
			'fromBarcode' => 'required',
			'toBarcode' => 'required',
			'store' => 'required',
			'active' => 'required',
		]);

		$active = $request->input('active');
    	$limit = $request->input('limit');
    	$fromDate = $request->input('fromDate');
    	$toDate = $request->input('toDate');
    	$fromBarcode = $request->input('fromBarcode');
    	$toBarcode = $request->input('toBarcode');
    	$store = $request->input('store');
    	$title = $active . ' Customer List';
    	$meta = [
    		'Date' => $fromDate . ' To ' . $toDate,
    		'Barcode' => $fromBarcode . ' To ' . $toBarcode
    	];

    	$query = $this->getQuery($fromDate, $toDate, $fromBarcode, $toBarcode, $active, $store);

		$columns = [
			'Store'			=> 'TNT_DESC',
			'Store Code'	=> 'TNT_CODE',
			'Member ID' 	=> 'CUST_BARCODE',
			'Name' 			=> 'CUST_NAME',
			'Mobile Phone'	=> 'PHN_NUMBER',
			'Birthday'		=> 'birthday',
			'Email'			=> 'EML_EMAIL',
			'Sex'			=> 'LOK_DESCRIPTION',
			'Address'		=> 'ADDR_ADDRESS',
			'Created Date'	=> 'created_date',
			'Last Exchange' => 'exchange_date',
			'Created User'	=> 'created_user',
			'Migrate User'	=> 'migrate_user'
		];

        return compact('title', 'meta', 'query', 'columns', 'limit');
	}

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
		return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
					->setOrientation('landscape')
					->limit($params['limit']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->withoutManipulation()->download('Customer List ' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->withoutManipulation()->download('Customer List ' . date('d M Y'));
    }

    public function downloadCSVReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $csvReport = $this->generateReport(new CSVReport, $params);

        return $csvReport->showMeta()->withoutManipulation()->download('Customer List ' . date('d M Y'));
    }
}


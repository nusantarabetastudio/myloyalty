<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Master\Tenant;
use Illuminate\Http\Request;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;

class TopCustomerController extends Controller
{
    public function showFilter()
    {
    	return view('report.filter.top-customer');
    }

    public function getQuery($fromDate, $toDate, $orderBy, $sortBy)
    {
        return Customer::select(['result.*'])
                        ->from(DB::raw("
                            (SELECT
                                c.CUST_PNUM,
                                c.CUST_PTYPE,
                                c.CUST_BARCODE,
                                c.CUST_NAME,
                                Sum(e.POS_POINT_REWARD) + Sum(e.POS_POINT_REGULAR) + SUM(e.POS_POINT_BONUS) AS earning,
                                (
                                    SELECT SUM(rp.point) FROM redeem_posts rp WHERE rp.customer_id = c.CUST_RECID  AND POS_POST_DATE BETWEEN '".$fromDate."'    AND '".$toDate."' 
                                ) AS redemption,
                                (
                                    SELECT SUM(point) FROM customer_points 
                                    WHERE customer_id = c.CUST_RECID
                                ) AS balance
                            FROM
                                Customers AS c
                            LEFT JOIN Post AS e ON e.POS_CUST_RECID = c.CUST_RECID
                            WHERE
                                e.POS_POST_DATE BETWEEN '".$fromDate."' AND '".$toDate."'
                            AND
                                c.CUST_PNUM = '".Session::get('pnum')."'
                            AND
                                c.CUST_PTYPE = '".Session::get('ptype')."'
                            GROUP BY
                                c.CUST_PNUM,
                                c.CUST_PTYPE,
                                c.CUST_BARCODE,
                                c.CUST_NAME,
                                c.CUST_RECID,
                                e.POS_POST_DATE
                            ) as result
                        "))
                        ->orderBy('result.' . $orderBy, $sortBy == 'Descending' ? 'DESC' : 'ASC');
    }

    public function getReportParams(Request $request)
    {
        $this->validate($request, [
            'fromDate' => 'required',
            'toDate' => 'required',
            'orderBy' => 'required',
            'sortBy' => 'required'
        ]);

        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        $orderBy = $request->input('orderBy');
        $sortBy = $request->input('sortBy');
        $limit = $request->input('limit');

        $title = 'Top Customer';
        $meta = [
            'Filter Periode ' => $fromDate. '-'.$toDate,
            'Order By' => $orderBy,
            'Sort By' => $sortBy
        ];

        $query = $this->getQuery($fromDate, $toDate, $orderBy, $sortBy);

        $columns = [
            'Member ID' => 'CUST_BARCODE',
            'Name'  => 'CUST_NAME',
            'Total Earning' => 'earning',
            'Total Redeem' => 'redemption',
            'Point Balance' => 'balance'
        ];

        return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
        return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
                    ->limit($params['limit']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->download('Top Customer ' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->simple()->download('Top Customer ' . date('d M Y'));
    }
}

<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Master\Tenant;
use App\Models\Admin\Post;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;

class CustomerEarningSummaryController extends Controller
{
    public function showFilter() {
    	$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();
    	return view('report.filter.customer-earning-summary', compact('tenants'));
    }

    public function getQueryByStore($fromDate, $toDate, $store, $fromBarcode, $toBarcode)
    {
        $query = Post::with(['customer'])
                    ->whereHas('customer', function($query) use($fromBarcode, $toBarcode) {
                        $query->whereBetween('CUST_BARCODE', [$fromBarcode, $toBarcode]);
                    })
                    ->whereBetween('POS_POST_DATE', [$fromDate, $toDate])
                    ->leftJoin('z_tenant', 'post.POS_STORE', '=', 'z_tenant.TNT_RECID')
                    ->orderBy('POS_POST_DATE', 'ASC')
                    ->orderBy('z_tenant.TNT_CODE', 'ASC');
        if ($store != 'All') {
            $query->where('POS_STORE', $store);
        }
        return $query;
    }

    public function getQueryByBarcode($fromDate, $toDate, $store, $fromBarcode, $toBarcode)
    {
        $query = Post::with(['customer'])
                    ->whereHas('customer', function($query) use($fromBarcode, $toBarcode) {
                        $query->whereBetween('CUST_BARCODE', [$fromBarcode, $toBarcode]);
                    })
                    ->whereBetween('POS_POST_DATE', [$fromDate, $toDate])
                    ->where('TNT_PNUM', Session::get('pnum'))
                    ->where('TNT_PTYPE', Session::get('ptype'))
                    ->leftJoin('z_tenant', 'post.POS_STORE', '=', 'z_tenant.TNT_RECID')
                    ->orderBy('POS_POST_DATE', 'ASC')
                    ->orderBy('z_tenant.TNT_CODE', 'ASC');
        if ($store != 'All') {
            $query->where('POS_STORE', $store);
        }

        return $query;
    }

    public function getReportParams(Request $request)
    {
        $this->validate($request, [
            'fromDate' => 'required',
            'toDate' => 'required',
            'fromBarcode' => 'required',
            'toBarcode' => 'required',
            'store' => 'required',
            'groupBy' => 'required'
        ]);

        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        $fromBarcode = $request->input('fromBarcode');
        $toBarcode = $request->input('toBarcode');
        $store = $request->input('store');
        $limit = $request->input('limit');
        $groupBy = $request->input('groupBy');

        $title = 'Customer Earning by '.$groupBy.' - Summary';
        $meta = [
            'Date' => $fromDate . ' To ' . $toDate,
            'Store' => ($request->input('store') == 'All') ? $request->input('store') : Tenant::findOrFail($request->input('store'))->TNT_CODE,
            'Grouping By' => $groupBy
        ];

        if ($groupBy == 'Store') {
            $query = $this->getQueryByStore($fromDate, $toDate, $store, $fromBarcode, $toBarcode);
        } elseif ($groupBy == 'Member ID') {
            $query = $this->getQueryByBarcode($fromDate, $toDate, $store, $fromBarcode, $toBarcode);
        }
        $columns = [
            'Store' => 'TNT_CODE',
            'Member ID' => function($result) {
                return $result->customer->CUST_BARCODE;
            },
            'Name' => function($result) {
                return $result->customer->CUST_NAME;
            },
            'Total Receipt' => 'POS_AMOUNT',
            'Earning' => function($result) {
                return $result->POS_POINT_REGULAR + $result->POS_POINT_BONUS;
            },
            'Reward' => 'POS_POINT_REWARD',
            'Total' => function($result) {
                return $result->POS_POINT_REGULAR + $result->POS_POINT_BONUS + $result->POS_POINT_REWARD;
            }
        ];

        return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
        return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
                    ->editColumn('Total Receipt', [
                        'class' => 'right',
                        'displayAs' => function($result) {
                            return 'IDR ' . thousandSeparator($result->POS_AMOUNT);
                        }
                    ])
                    ->editColumn('Earning', [
                        'class' => 'right',
                        'displayAs' => function($result) {
                            return thousandSeparator($result->POS_POINT_REGULAR + $result->POS_POINT_BONUS);
                        }
                    ])
                    ->editColumn('Reward', [
                        'class' => 'right',
                        'displayAs' => function($result) {
                            return thousandSeparator($result->POS_POINT_REWARD);
                        }
                    ])
                    ->editColumn('Total', [
                        'class' => 'right',
                        'displayAs' => function($result) {
                            return thousandSeparator($result->POS_POINT_REGULAR + $result->POS_POINT_BONUS + $result->POS_POINT_REWARD);
                        }
                    ])
                    ->groupBy('Store')
                    ->showTotal([
                        'Total Receipt' => 'idr', 
                        'Earning' => 'point',
                        'Reward' => 'point', 
                        'Total' => 'point'
                    ])
                    ->limit($params['limit']);
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->download('Customer Earning by Summary' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->simple()->download('Customer Earning by Summary' . date('d M Y'));
    }
}

<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Master\Tenant;
use App\Models\Admin\PostPE;
use App\Models\Admin\ZTotalPoint;
use App\Models\Admin\PointExpired;
use Jimmyjs\ReportGenerator\ReportGenerator;
use Jimmyjs\ReportGenerator\ReportMedia\PdfReport;
use Jimmyjs\ReportGenerator\ReportMedia\ExcelReport;
use PDF, DB, Session;

class PointExpiredController extends Controller
{
    public function showFilter()
    {
    	$tenants = Tenant::where('TNT_PNUM', Session::get('pnum'))->where('TNT_PTYPE', Session::get('ptype'))->get();
    	return view('report.filter.point-expired', compact('tenants'));
    }

    public function getQuery($fromBarcode, $toBarcode, $fromDate, $toDate, $store)
    {
        $query = PostPE::select([
                'c.CUST_BARCODE',
                'c.CUST_NAME',
                'p.POINT_BEFORE_PE',
                'TTL_POINT',
                'TNT_CODE',
                'pe.PE_ON'
            ])
            ->from('POST_PE as p')
            ->leftJoin('Customers as c', 'p.CUST_RECID', '=', 'c.CUST_RECID')
            ->leftJoin('Z_TotalPoint', 'c.CUST_RECID', '=', 'TTL_CUST_RECID')
            ->leftJoin('Z_Tenant', 'TNT_RECID', '=', 'c.CUST_STORE')
            ->leftJoin('POINTEXPIRED as pe', 'pe.PE_RECID', '=', 'p.PE_RECID')
            ->whereBetween('CUST_BARCODE', [$fromBarcode, $toBarcode]);
        return $query;
    }

    public function getReportParams(Request $request)
    {
        $this->validate($request, [
            'fromDate' => 'required',
            'toDate' => 'required',
            'fromBarcode' => 'required',
            'toBarcode' => 'required',
            'store' => 'required'
        ]);

        $limit = $request->input('limit') ?: null;
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        $fromBarcode = $request->input('fromBarcode');
        $toBarcode = $request->input('toBarcode');
        $store = $request->input('store');

        $title = ' Point Expired - Summary';
        $meta = [
            'Date' => $fromDate . ' To ' . $toDate,
            'Barcode' => $fromBarcode . ' To ' . $toBarcode,
            'Store' => ($request->input('store') == 'All') ? $request->input('store') : Tenant::findOrFail($request->input('store'))->TNT_CODE
        ];

        $query = $this->getQuery($fromBarcode, $toBarcode, $fromDate, $toDate, $store);

        $columns = [
            'Store' => 'TNT_CODE',
            'Member Id' => 'CUST_BARCODE',
            'Name' => 'CUST_NAME',
            'Post Date' => 'PE_ON',
            'Point Balance Before' =>function($result) {
                return $result->POINT_BEFORE_PE + $result->TTL_POINT;
            },
            'Point Balance Execute' => 'POINT_BEFORE_PE',
            'Point Balance After' => 'TTL_POINT'
        ];

        return compact('title', 'meta', 'query', 'columns', 'limit');
    }

    public function generateReport(ReportGenerator $reportGenerator, Array $params)
    {
        return $reportGenerator->of($params['title'], $params['meta'], $params['query'], $params['columns'])
            ->limit($params['limit'])
            ->setOrientation('landscape');
    }

    public function displayPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->stream();
    }

    public function downloadPdfReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $pdfReport = $this->generateReport(new PdfReport, $params);

        return $pdfReport->download('Point Expired' . date('d M Y'));
    }

    public function downloadExcelReport(Request $request)
    {
        $params = $this->getReportParams($request);
        $excelReport = $this->generateReport(new ExcelReport, $params);

        return $excelReport->download('Point Expired' . date('d M Y'));
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Admin\FormulaEarn;
use App\Models\Admin\FormulaEarnBlood;
use App\Models\Admin\FormulaEarnBrand;
use App\Models\Admin\FormulaEarnCardType;
use App\Models\Admin\FormulaEarnCategory;
use App\Models\Admin\FormulaEarnDepartment;
use App\Models\Admin\FormulaEarnDivision;
use App\Models\Admin\FormulaEarnGender;
use App\Models\Admin\FormulaEarnItems;
use App\Models\Admin\FormulaEarnMember;
use App\Models\Admin\FormulaEarnPayment;
use App\Models\Admin\FormulaEarnReligion;
use App\Models\Admin\FormulaEarnSegment;
use App\Models\Admin\FormulaEarnSubCategory;
use App\Models\Admin\FormulaEarnTenant;
use App\Models\Admin\FormulaEarnVendor;
use App\Models\Admin\Post;
use App\Models\Admin\PostPF;
use App\Models\Admin\PostTemp;
use App\Models\Admin\PostTempPayment;
use App\Models\Admin\PostTest;
use App\Models\Admin\ZTotalPoint;
use App\Models\Admin\RedeemPost;
use App\Models\Counter;
use App\Models\Customer;
use App\Models\CustomerPoint;
use App\Models\Master\Bank;
use App\Models\Master\Brand;
use App\Models\Master\Merchandise;
use App\Models\Master\PaymentMethod;
use App\Models\Master\Product;
use App\Models\Master\Tenant;
use App\Models\Master\Vendor;
use App\Models\Preference;
use Carbon\Carbon;
use DB;
use Datatables;
use Faker\Provider\Uuid;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PDF;
use Session;
use App\Models\Master\Lookup;
use App\Models\PointRandom;

class TransactionController extends Controller {

    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct(Session $session) {
        $this->parent = 'Transactions';
        $this->parent_link = '';
        $this->pnum = $session::has('data') ? $session::get('data')->PNUM : null;
        $this->ptype = $session::has('data') ? $session::get('data')->PTYPE : null;
        // $this->middleware('user-log', ['only' => ['getCustomerToPosting', 'getCustomerToVoid', 'getCustomerToAdjust', 'getCustomerToManual', 'getCustomerToTesting', 'getTransactionToPosting', 'getTransactionToVoid' ,'getTransactionToAdjust', 'getTransactionToManual', 'getTransactionToTesting']]);
    
	}

    public function getCustomerToPosting() {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Posting Transactions';
        $type = 'posting';
        return view('transaction.customer', compact('parent', 'parent_link', 'breadcrumbs', 'type'));
    }

    public function getCustomerToVoid() {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Void Transactions';
        $type = 'void';
        return view('transaction.customer', compact('parent', 'parent_link', 'breadcrumbs', 'type'));
    }

    public function getCustomerToAdjust() {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Adjustment Transactions';
        $type = 'adjustment';
        return view('transaction.customer', compact('parent', 'parent_link', 'breadcrumbs', 'type'));
    }

    public function getCustomerToManual() {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Manual Transaction';
        $type = 'manual';
        return view('transaction.customer', compact('parent', 'parent_link', 'breadcrumbs', 'type'));
    }

    public function getCustomerToManipulate() {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Manipulate Point';
        $type = 'manipulate';
        return view('transaction.customer', compact('parent', 'parent_link', 'breadcrumbs', 'type'));
    }

    public function getCustomerToTesting() {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Testing Earning';
        $type = 'testing';

        return view('transaction.customer', compact('parent', 'parent_link', 'breadcrumbs', 'type'));
    }

    public function getPostingCustomerData() {
        return $this->getCustomerDatatablesData('posting');
    }

    public function getVoidCustomerData() {
        return $this->getCustomerDatatablesData('void');
    }

    public function getAdjustmentCustomerData() {
        return $this->getCustomerDatatablesData('adjustment');
    }

    public function getManualCustomerData() {
        return $this->getCustomerDatatablesData('manual');
    }

    public function getTestingCustomerData() {
        return $this->getCustomerDatatablesData('testing');
    }

    public function getManipulateData() {
        return $this->getCustomerDatatablesData('manipulate');
    }

    protected function getCustomerDatatablesData($type) {
        $customer = Customer::select(['CUST_RECID', 'CUST_NAME', 'CUST_BARCODE', 'CUST_IDCARDS', 'CUST_IDTYPE'])->with(['mobilePhone', 'idType'])->where('CUST_ACTIVE', 1);
        return Datatables::of($customer)
                        ->addColumn('action', function($customer) use($type) {
                            if ($type == 'manipulate') {
                                $view = '<a href="' . route('transaction.' . $type . '.detail', $customer->CUST_RECID) . '"><button type="button" class="btn btn-success btn-xs">Change Point</button></a>';
                                return $view;
                            } else {
                                $view = '<a href="' . route('transaction.' . $type . '.detail', $customer->CUST_RECID) . '"><button type="button" class="btn btn-success btn-xs">View</button></a>';
                                return $view;
                            }
                        })
                        ->addColumn('id', function($customer) {
                            for ($i = 1; $i <= $customer->count(); $i++) {
                                return $i;
                            }
                            return $i;
                        })
                        ->addColumn('CUST_NAME', function($customer) {
                            if ($customer->CUST_NAME) {
                                return $customer->CUST_NAME;
                            } else {
                                return '-';
                            }
                        })
                        ->addColumn('mobilePhone', function($customer) {
                            if ($customer->mobilePhone != null) {
                                return $customer->mobilePhone->PHN_NUMBER;
                            } else {
                                return '-';
                            }
                        })
                        ->addColumn('CUST_BARCODE', function($customer) {
                            if ($customer->CUST_BARCODE) {
                                return $customer->CUST_BARCODE;
                            } else {
                                return '-';
                            }
                        })
                        ->addColumn('idType', function($customer) {
                            if ($customer->idType != null) {
                                return $customer->idType->LOK_DESCRIPTION;
                            } else {
                                return '-';
                            }
                        })
                        ->addColumn('CUST_IDCARDS', function($customer) {
                            if ($customer->CUST_IDCARDS == null) {
                                return '-';
                            }
                            return $customer->CUST_IDCARDS;
                        })
                        ->make(true);
    }

    public function getTransactionToPosting($customer_id) {
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['breadcrumbs'] = 'Posting Transactions';

        // Create guzzle client
        $client = new Client(['http_errors' => false]);
        $uriCustomer = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/' . $customer_id;

        $response = $client->request('GET', $uriCustomer, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());

            $data['customer'] = $contents->DATA;
            return view('transaction.posting', $data);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect('login');
        }
    }

    public function getTransactionToVoid($customer_id) {
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['breadcrumbs'] = 'Void Transactions';

        // Create guzzle client
        $client = new Client(['http_errors' => false]);
        $uriCustomer = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/' . $customer_id;

        $response = $client->request('GET', $uriCustomer, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());

            $data['customer'] = $contents->DATA;
            return view('transaction.void', $data);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect('login');
        }
    }

    public function getTransactionToAdjust($customer_id) {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Adjustment Transactions';
        // Create guzzle client
        $client = new Client(['http_errors' => false]);
        $uriCustomer = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/' . $customer_id;

        $response = $client->request('GET', $uriCustomer, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());

            $customer = $contents->DATA;
            return view('transaction.adjustment', compact('customer_id', 'parent', 'parent_link', 'breadcrumbs', 'customer'));
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect('login');
        }
    }

    public function getTransactionToManual($customer_id) {
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['breadcrumbs'] = 'Manual';

        // Create guzzle client
        $client = new Client(['http_errors' => false]);
        $uriCustomer = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/' . $customer_id;

        $response = $client->request('GET', $uriCustomer, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());

            $data['customer'] = $contents->DATA;
            $data['stores'] = Tenant::all();
            $data['payment_methods'] = PaymentMethod::all();
            $data['banks'] = Bank::all();
            return view('transaction.manual', $data);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect('login');
        }
    }

    public function getTransactionToTesting($customer_id) {
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['breadcrumbs'] = 'Testing';

        // Create guzzle client
        $client = new Client(['http_errors' => false]);
        $uriCustomer = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/' . $customer_id;

        $response = $client->request('GET', $uriCustomer, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());

            $data['customer'] = $contents->DATA;
            $data['stores'] = Tenant::all();
            $data['payment_methods'] = PaymentMethod::all();
            $data['banks'] = Bank::all();
            return view('transaction.testing', $data);
        } elseif ($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect()->back();
        } else {
            return redirect('login');
        }
    }

    public function showManipulatePointForm($customer_id) {
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['breadcrumbs'] = 'Manipulate Point';
        $customer = Customer::findOrFail($customer_id);

        return view('transaction.manipulate', compact('customer'));
    }

    public function getPostingTransactionMasterData($barcode) {

        $pos = Post::where('POS_BARCODE', $barcode)->orderBy('POS_POST_DATE', 'DESC')->get();

        $datatables = app('datatables')->of($pos)
                ->editColumn('POS_POST_DATE', function($data) {
                    return $data->POS_POST_DATE->format('d M Y');
                })
                ->editColumn('POS_POST_TIME', function($data) {
                    return substr($data->POS_POST_TIME, 0, 8);
                })
                ->addColumn('id', function() {
            return null;
        });
        return $datatables->make(true);
    }

    public function getPostingTransactionDetailData($barcode) {
        $data = Post::where('POS_BARCODE', $barcode)->with(['detail'])->orderBy('POS_POST_DATE', 'DESC')->get();
        $datatables = app('datatables')->of($data)
                ->addColumn('id', null);

        return $datatables->make(true);
    }

    public function getPostingTransactionPostingData() {
        $transactions = new Collection();

        for ($i = 1; $i < 100; $i++) {
            $transactions->push([
                'id' => $i + 1,
                'date' => Carbon::now()->format('m-d-Y'),
                'time' => Carbon::now()->format('h:i:s'),
                'amount' => number_format($i * 30000),
                'ld' => $i,
                'rd' => $i
            ]);
        }

        return Datatables::of($transactions)->make(true);
    }

    public function getVoidTransactionData($barcode) {
        $data = Post::where('POS_BARCODE', $barcode)->orderBy('POS_POST_DATE', 'DESC')->get();
        $datatables = app('datatables')->of($data)
                ->editColumn('POS_POST_TIME', function($data) {
                    return substr($data->POS_POST_TIME, 0, 8);
                })
                ->editColumn('POS_AMOUNT', function($data) {
                    return number_format($data->POS_AMOUNT);
                })
                ->addColumn('void', function($data) {
                    $btn = '<input type="checkbox" name="voids[]" value="' . $data->POS_RECID . '" data-plugin="switchery">';

                    return $btn;
                })
                ->addColumn('id', null);

        return $datatables->make(true);
    }

    public function getAdjustmentTransactionData($customerId) {
        $data = Post::where('POS_CUST_RECID', $customerId);

        $datatables = app('datatables')->of($data)
                ->addColumn('id', null)
                ->addColumn('select', function($data) {
                    $selectBtn = '<input type="radio" name="POS_RECID" id="pos-id"  value="' . $data->POS_RECID . '"> ';

                    return $selectBtn;
                })
                ->editColumn('store', function($data) {
            if ($data->store()->count() > 0) {
                return $data->store->TNT_DESC;
            }
            return '-';
        });

        return $datatables->make(true);
    }

    public function manipulatePoint(Customer $customer, Request $request) {
        $this->validate($request, [
            'amount' => 'required|min:0|integer',
            'point' => 'required|min:0|integer',
            'type' => 'required|in:add,minus'
        ]);

        $amount = $request->input('amount');
        $point = $request->input('point');
        $type = $request->input('type');

        if ($type == 'add') {
            $this->addCustomerPoint($point, $amount, $customer);
        } else {
            $this->minusCustomerPoint($point, $amount, $customer);
        }

        return redirect()->back()->with('notif_success', 'Success!');
    }

    private function minusCustomerPoint($point, $amount, Customer $customer) {
        $pnum = Session::get('pnum');
        $ptype = Session::get('ptype');
        $store = Session::get('data')->STORE->id;

        $proptypeCounter = Counter::where('pnum', $pnum)->where('ptype', $ptype)->first();
        if (isEmpty($proptypeCounter)) {
            $proptypeCounter = $this->addNewProptypeCounter($pnum, $ptype);
        }
        $proptypeCounter->redeem_post_count += 1;
        $proptypeCounter->save();
        $redeemPostCount = $proptypeCounter->redeem_post_count;

        $post = new RedeemPost;
        $post->id = strtoupper(Uuid::uuid());
        $post->pnum = $pnum;
        $post->ptype = $ptype;
        $post->customer_id = $customer->CUST_RECID;
        $post->tenant_id = $store;
        $post->post_date = date('Y-m-d');
        $post->post_time = date('H:i:s');
        $post->posted_by = 'INJECT';
        $post->price = $amount;
        $post->point = $point;
        $year = date('y');
        $post->receipt_no = 'RD/' . $year . '/' . prefix($redeemPostCount, 7) . '-A';
        $post->is_void = 0;
        $post->update = 1;
        $post->save();

        $customerPoint = $customer->points()->redeemPoint()->first();
        if ($customerPoint) {
            $customerPoint->point -= $point;
            $customerPoint->save();
        } else {
            $customerPoint = new CustomerPoint;
            $customerPoint->id = Uuid::uuid();
            $customerPoint->customer_id = $customer->CUST_RECID;
            $customerPoint->pnum = $pnum;
            $customerPoint->ptype = $ptype;
            $customerPoint->code = 2;
            $customerPoint->point = 0;
            $customerPoint->save();
        }

        return true;
    }

    private function addCustomerPoint($point, $amount, Customer $customer) {
        $pnum = Session::get('pnum');
        $ptype = Session::get('ptype');

        $post = new Post;
        $post->POS_RECID = Uuid::uuid();
        $post->POS_CUST_RECID = $customer->CUST_RECID;
        $post->POS_PNUM = $pnum;
        $post->POS_PTYPE = $ptype;
        $post->POS_STORE = $customer->CUST_STORE;
        $post->POS_BARCODE = $customer->CUST_BARCODE;
        $post->POS_POST_DATE = date('Y-m-d');
        $post->POS_POST_TIME = date('H:i:s');
        $post->POS_STATION_ID = '-';
        $post->POS_SHIFT_ID = '-';
        $post->POS_DOC_NO = '-';
        $post->POS_CASHIER_ID = '-';
        $post->POS_AMOUNT = 0;
        $post->POS_POINT_LD = 0;
        $post->POS_POINT_REWARD = $point;
        $post->POS_POINT_REGULAR = 0;
        $post->POS_POINT_BONUS = 0;
        $post->POS_RECEIPT_TYPE = 2;
        $post->POS_USERBY = 'INJECT';
        $post->POS_UPDATE = 1;
        $post->POS_TYPE = 0;
        $postCount = $this->getEarningCount($pnum, $ptype);
        $post->POS_RECEIPT_NO = 'EA/' . date('y') . '/' . prefix($postCount, 7) . '-A';
        $post->save();

        $customerPoint = $customer->points()->redeemPoint()->first();
        if ($customerPoint) {
            $customerPoint->point += $point;
            $customerPoint->save();
        } else {
            $customerPoint = new CustomerPoint;
            $customerPoint->id = Uuid::uuid();
            $customerPoint->customer_id = $customer->CUST_RECID;
            $customerPoint->pnum = $pnum;
            $customerPoint->ptype = $ptype;
            $customerPoint->code = 2;
            $customerPoint->point = $point;
            $customerPoint->save();
        }

        return true;
    }

    private function getEarningCount($pnum, $ptype) {
        $proptypeCounter = Counter::where('pnum', $pnum)->where('ptype', $ptype)->first();
        if (empty(trim($proptypeCounter))) {
            $proptypeCounter = $this->addNewProptypeCounter($pnum, $ptype);
        }
        $proptypeCounter->post_count += 1;
        $proptypeCounter->save();

        return $proptypeCounter->post_count;
    }

    public function postTransactionAdjustment(Request $request) {
        $rules = [
            'POS_AMOUNT' => 'required',
            'POS_RECID' => 'required'
        ];
        $this->validate($request, $rules);

        $post = Post::find($request->input('POS_RECID'));

        $point_before = $post->POS_POINT_REGULAR;

        if ($post != null) {
            $post->POS_AMOUNT = $request->input('POS_AMOUNT');
            $post->POS_POINT_LD = $request->input('POS_POINT_LD');
            $post->POS_POINT_REGULAR = $request->input('POS_POINT_REGULAR');
            $post->save();

            $totalPoint = ZTotalPoint::where('TTL_CUST_RECID', $post->POS_CUST_RECID)->redeemPoint()->first();
            $resultPoint = $post->POS_POINT_REGULAR - $point_before;
            if ($totalPoint) {
                $totalPoint->TTL_POINT = $totalPoint->TTL_POINT + $resultPoint;
                $totalPoint->save();
            } else {
                $totalPoint = new ZTotalPoint;
                $totalPoint->TTL_RECID = Uuid::uuid();
                $totalPoint->TTL_PNUM = $this->pnum;
                $totalPoint->TTL_PTYPE = $this->ptype;
                $totalPoint->TTL_CUST_RECID = $post->POS_CUST_RECID;
                $totalPoint->TTL_CODE = 2;
                $totalPoint->TTL_POINT = $resultPoint;
                $totalPoint->save();
            }


            $request->session()->flash('success', 'Data has been updated.');
        } else {
            $request->session()->flash('success', 'Data transaksi tidak tersedia!');
        }


        return redirect()->back();
    }

    public function postTransactionVoidProcess(Request $request) {
        $rules = [
            'voids' => 'required',
        ];
        $messages = [
            'voids.required' => 'Tidak ada transaksi yang dipilih',
        ];
        $this->validate($request, $rules, $messages);

        $dataRequest = $request->except(['_token']);

        foreach ($dataRequest['voids'] as $row) {
            $post = Post::where('POS_RECID', $row);

            $getPost = $post->first();
            $totalPoint = ZTotalPoint::where('TTL_CUST_RECID', $getPost->POS_CUST_RECID)->redeemPoint()->first();
            $resultPoint = $getPost->POS_POINT_REGULAR;
            if ($totalPoint) {
                $totalPoint->TTL_POINT = $totalPoint->TTL_POINT - $resultPoint;
                $totalPoint->save();
            }


            $post->delete();
        }

        $request->session()->flash('success', 'Void Success.');

        //

        return redirect()->back();
    }

    public function postTransactionToPosting(Request $request) {

        $pnum = Session::get('pnum');
        $ptype = Session::get('ptype');

        $rules = [
            'POS_AMOUNT' => 'required',
        ];
        $messages = [
            'POS_AMOUNT.required' => 'Jumlah Transaksi Harus diisi',
        ];
        //$this->validate($request, $rules, $messages);

        $dataRequest = $request->except(['_token']);

        $post = new Post();
        $post['POS_RECID'] = Uuid::uuid();
        $post['POS_STORE'] = $dataRequest['POS_STORE'];
        $post['POS_PNUM'] = $pnum;
        $post['POS_PTYPE'] = $ptype;
        $post['POS_POST_DATE'] = $dataRequest['POS_POST_DATE'];
        $post['POS_POST_TIME'] = date('H:i:s');
        $post['POS_STATION_ID'] = 0;
        $post['POS_SHIFT_ID'] = 0;
        $post['POS_CUST_RECID'] = $dataRequest['POS_CUST_RECID'];
        $post['POS_BARCODE'] = $dataRequest['POS_BARCODE'];
        $post['POS_DOC_NO'] = $dataRequest['POS_DOC_NO'];
        $post['POS_AMOUNT'] = $dataRequest['POS_AMOUNT'];
        $post['POS_POINT_LD'] = $dataRequest['POS_POINT_LD'];
        $post['POS_POINT_REGULAR'] = $dataRequest['POS_POINT_REGULAR'];
        $post['POS_USERBY'] = Session::get('data')->USER_DATA->USER_RECID;
        $post->save();

        // add point to system
        $totalPoint = ZTotalPoint::where('TTL_CUST_RECID', $post['POS_CUST_RECID'])->redeemPoint()->first();
        $resultPoint = $post['POS_POINT_REGULAR'];
        if ($totalPoint) {
            $totalPoint->TTL_POINT = $totalPoint->TTL_POINT + $resultPoint;
            $totalPoint->save();
        }



        $request->session()->flash('success', 'Void Success.');

        return redirect()->back();
    }

    public function postTransactionManualProcess(Request $request) {
        $rules = [
            'STOREID' => 'required',
        ];
        $messages = [
            'STOREID.required' => 'Store field can not be empty.',
        ];
        $this->validate($request, $rules, $messages);

        $dataRequest = $request->except('_token');

        $data['earning'] = $this->earningPointPost($dataRequest);

        $data['customer'] = Customer::find($dataRequest['CUST_RECID']);
        $data['store'] = Tenant::where('TNT_CODE', $dataRequest['STOREID'])->first();
        $data['data'] = $dataRequest;

        return $this->getReceipt($data);
    }

    public function postTransactionTestProcess(Request $request) {
        $rules = [
            'STOREID' => 'required',
            'RCPDATE' => 'required',
            'RCPTIME' => 'required',
            'article' => 'required'
        ];

        $messages = [
            'STOREID.required' => 'Store field can not be empty.',
            'RCPDATE.required' => 'Date field can not be empty.',
            'RCPTIME.required' => 'Time field can not be empty.',
            'article.requried' => 'Article field can not be empty.'
        ];

        $this->validate($request, $rules, $messages);

        $dataRequest = $request->except(['_token', 'article']);

        $this->testingPointPost($dataRequest);

        $data['post_test'] = PostTest::orderBy('POS_POST_DATE', 'DESC')->get();

        return view('transaction.testing_result', $data);
    }

    public function earningPointPost($row_data = []) {

        $pnum = Session::get('pnum');
        $ptype = Session::get('ptype');

        if (!empty($row_data)) {

            $DOC_NO = 'docs.' . date('Y-m-d H:i:s');



            $row_data['INTCODE'] = 1;
            $row_data['ITEMQTY'] = 1;

            $calcBonus = 0;

            $point_bonus = 0;
            $split_bonus = 0;
            $multiple = 0;
            // manual earning tidak ada bonus point, kecuali Bonus Point By All Product

            $row_data['BRAND_RECID'] = 0;
            $row_data['DEPT_RECID'] = 0;
            $row_data['DIV_RECID'] = 0;
            $row_data['CAT_RECID'] = 0;
            $row_data['SUBCAT_RECID'] = 0;
            $row_data['SEGMENT_RECID'] = 0;
            $row_data['VENDOR_RECID'] = 0;

			/*
            $calcBonus = $this->_checkBonusPoint($row_data);

            $point_bonus = $calcBonus['point'];
            $split_bonus = $calcBonus['split'];
            $multiple = $calcBonus['multiple'];
			*/
			$calcBonus = [];
			    $point_bonus = 0;
                $split_bonus = 0;
                $multiple = 0;


            $calcBonusPayment = $this->_checkBonusPayment($row_data, $row_data['TENDNBR']);

            $point_bonus_payment = $calcBonusPayment['point'];
            $multiple_payment = $calcBonusPayment['multiple'];

            DB::table('POSTTEMPPAYMENT')->delete();

            $postTempPayment = new PostTempPayment();
            $postTempPayment->POSTTEMPPAYMENT_RECID = strtoupper(Uuid::uuid());
            $postTempPayment->STOREID = $row_data['STOREID'];  // on windows CSV array start from 1
            $postTempPayment->RCPDATE = $row_data['RCPDATE'];
            $postTempPayment->RCPTIME = $row_data['RCPTIME'];
            $postTempPayment->CARDID = $row_data['CARDID'];
            $postTempPayment->TENDNBR = $row_data['TENDNBR'];
            $postTempPayment->BIN = (!empty($row_data['BIN'])) ? $row_data['BIN'] : '0';
            $postTempPayment->POINT_BONUS = $point_bonus_payment;
            $postTempPayment->MULTIPLE = $multiple_payment;
            $postTempPayment->save();

            DB::table('POSTTEMP')->delete();

            $postTemp = new PostTemp();
            $postTemp->POSTTEMP_RECID = strtoupper(Uuid::uuid());
            $postTemp->STOREID = $row_data['STOREID'];
            $postTemp->RCPDATE = $row_data['RCPDATE'];
            $postTemp->RCPTIME = $row_data['RCPTIME'];
            $postTemp->CARDID = $row_data['CARDID'];
            $postTemp->INTCODE = 0;
            $postTemp->TOTALAFTERDISC = $row_data['TOTALAFTERDISC'];
            $postTemp->POINT_BONUS = $point_bonus;
            $postTemp->SPLIT_BONUS = $split_bonus;
            $postTemp->MULTIPLE = $multiple;
            $postTemp->save();

            $newErn = PostTemp::select('CARDID', DB::raw('SUM(TOTALAFTERDISC) as total'), DB::raw('SUM(POINT_BONUS) as total_point_bonus'), DB::raw('SUM(SPLIT_BONUS) as total_split_bonus'))
                    ->groupBy('CARDID');

            foreach ($newErn->get() as $row) {

                $point = 0;
                $split = 0;

                $bonus_combine_fix = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 0)
                        ->orderBy('point', 'DESC')
                        ->groupBy('INTCODE')
                        ->first();

                $bonus_combine_multiple = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 1)
                        ->orderBy('point', 'DESC')
                        ->groupBy('INTCODE')
                        ->first();


                $bonus_payment_fix = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 0)
                        ->groupBy('TENDNBR')
                        ->orderBy('point', 'DESC')
                        ->first();

                $bonus_payment_multiple = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 1)
                        ->groupBy('TENDNBR')
                        ->orderBy('point', 'DESC')
                        ->first();


                $bonus_payment = (isset($bonus_payment_fix->point) ? $bonus_payment_fix->point : 0) + (isset($bonus_payment_multiple->point) ? $bonus_payment_multiple->point : 0);

                //echo $bonus_payment.' = '.(isset($bonus_payment_fix->point)?$bonus_payment_fix->point:0).' + '.(isset($bonus_payment_multiple->point)?$bonus_payment_multiple->point:0); exit;

                $bonus_combine_fix_point = (isset($bonus_combine_fix->point) ? $bonus_combine_fix->point : 0);
                $bonus_combine_fix_split = (isset($bonus_combine_fix->split) ? $bonus_combine_fix->split : 0);
                $bonus_payment_point = (isset($bonus_payment) ? $bonus_payment : 0);

                $bonus_combine_multiple_point = (isset($bonus_combine_multiple->point) ? $bonus_combine_multiple->point : 0);
                $bonus_combine_multiple_split = (isset($bonus_combine_multiple->split) ? $bonus_combine_multiple->split : 0);

                $point = $bonus_combine_fix_point + $bonus_combine_multiple_point + $bonus_payment_point;
                $split = $bonus_combine_fix_split + $bonus_combine_multiple_split;

                $totalAmount = (isset($bonus_combine_multiple->total) ? $bonus_combine_multiple->total : 0) + (isset($bonus_combine_fix->total) ? $bonus_combine_fix->total : 0);
                $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
                $minimum_transaction = $preference['value'];

                if ($totalAmount < $minimum_transaction) {  //jika kurang dari minimum transaction
                    $point = 0;
                }

                $customer = Customer::where('CUST_BARCODE', $row->CARDID)->first();
                $postTemp = PostTemp::where('CARDID', $row->CARDID)->first();
                $sumErn = $row->total;

                $sumErn_split = $sumErn - $split;

                $ress_point_regular = $this->_checkRegularPoint($postTemp, $sumErn_split, $customer, $row_data['TENDNBR']);

                $point_regular = ($ress_point_regular['point'] == NULL) ? 0 : $ress_point_regular['point'];

                $ld = 0;
                $get_reward = $this->_checkRewardPoint($postTemp, $customer, $sumErn);

                $point_reward = $get_reward['point_fix'] + ($get_reward['point_multiple'] * $point_regular);

                //$point_reward = $get_reward['point'];

                if (isset($customer->CUST_RECID)) {

                    $tenant = Tenant::where('TNT_CODE', $postTemp->STOREID)->first();

                    $post = new Post();
                    $post->POS_RECID = Uuid::uuid();
                    $post->POS_CUST_RECID = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                    $post->POS_PNUM = $pnum;
                    $post->POS_PTYPE = $ptype;
                    $post->POS_STORE = $tenant->TNT_RECID;
                    $post->POS_BARCODE = $row->CARDID;
                    $post->POS_POST_DATE = $postTemp->RCPDATE;
                    $post->POS_POST_TIME = $postTemp->RCPTIME;
                    $post->POS_STATION_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                    $post->POS_SHIFT_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                    $post->POS_DOC_NO = $DOC_NO;
                    $post->POS_CASHIER_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                    $post->POS_AMOUNT = $sumErn;
                    $post->POS_POINT_LD = $ld;
                    $post->POS_POINT_REWARD = $point_reward;
                    $post->POS_POINT_REGULAR = $point_regular;
                    $post->POS_POINT_BONUS = $point;
                    $post->POS_RECEIPT_TYPE = '1';
                    $post->POS_USERBY = (isset(Session::get('data')->USER_DATA->USER_USERNAME)) ? substr(Session::get('data')->USER_DATA->USER_USERNAME, 0, 20) : 'By System';
                    $post->POS_CREATEBY = '-';
                    $post->POS_UPDATE = 1;
                    $post->POS_TYPE = 0;
                    $post->POS_TRANSF_FROM = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                    $post->POS_TRANSF_TO = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                    $post->POS_RECEIPT_NO = isset($postTemp->RCPNBR) ? $postTemp->RCPNBR : '-';
                    $post->TENDNBR = $postTemp->payment->TENDNBR;
                    $post->BIN = $postTemp->payment->BIN;
                    $post->save();

                    $totalPoint = ZTotalPoint::where('TTL_CUST_RECID', $post->POS_CUST_RECID)->redeemPoint()->first();
                    $resultPoint = $point_regular + $point + $point_reward;
                    if ($totalPoint) {
                        $totalPoint->TTL_POINT = $totalPoint->TTL_POINT + $resultPoint;
                        $totalPoint->save();
                    } else {
                        $totalPoint = new ZTotalPoint;
                        $totalPoint->TTL_RECID = Uuid::uuid();
                        $totalPoint->TTL_PNUM = $this->pnum;
                        $totalPoint->TTL_PTYPE = $this->ptype;
                        $totalPoint->TTL_CUST_RECID = $customer->CUST_RECID;
                        $totalPoint->TTL_CODE = 2;
                        $totalPoint->TTL_POINT = $resultPoint;
                        $totalPoint->save();
                    }
                }
            }

            $point_bonus = $point;

            DB::table('POSTTEMP')->delete();
            DB::table('POSTTEMPPAYMENT')->delete();

            return compact('point_regular', 'point_reward', 'point_bonus');
        } else {
            return 'Data not found';
        }
    }

    /* Testing Earning */

    public function testingPointPost($data = []) {

        $pnum = Session::get('pnum');
        $ptype = Session::get('ptype');

        if (!empty($data)) {
            DB::table('POSTTEMP')->delete();
            DB::table('POSTTEMPPAYMENT')->delete();

            $DOC_NO = 'docs.' . date('Y-m-d H:i:s');
            $POSTTEMP_RECID = array();
            $POSTTEMPPAYMENT_RECID = array();

            foreach ($data['products'] as $row_data) {
                $article = Product::find($row_data['id']);
                /* Array per product asign to row_data */
                $row_data['INTCODE'] = $article->PRO_CODE;
                $row_data['RCPTIME'] = $data['RCPTIME'];
                $row_data['RCPDATE'] = $data['RCPDATE'];
                $row_data['CARDID'] = $data['CARDID'];
                $row_data['STOREID'] = $data['STOREID'];
                $row_data['TOTALAFTERDISC'] = $row_data['amount'];
                $row_data['ITEMQTY'] = $row_data['qty'];
                $row_data['TENDNBR'] = $row_data['payment_method'];

                $merchant = Merchandise::where('MERC_CODE', $row_data['INTCODE'])->first();
                $row_data['BRAND_RECID'] = (isset($article->brand->id) ? $article->brand->id : 0);
                $row_data['DEPT_RECID'] = (isset($merchant->department->LOK_RECID) ? $merchant->department->LOK_RECID : 0);
                $row_data['DIV_RECID'] = (isset($merchant->division->LOK_RECID) ? $merchant->division->LOK_RECID : 0);
                $row_data['CAT_RECID'] = (isset($merchant->category->LOK_RECID) ? $merchant->category->LOK_RECID : 0);
                $row_data['SUBCAT_RECID'] = (isset($merchant->subcategory->LOK_RECID) ? $merchant->subcategory->LOK_RECID : 0);
                $row_data['SEGMENT_RECID'] = (isset($merchant->segment->LOK_RECID) ? $merchant->segment->LOK_RECID : 0);
                $row_data['VENDOR_RECID'] = (isset($merchant->vendor->LOK_RECID) ? $merchant->vendor->LOK_RECID : 0);

                /*
            $calcBonus = $this->_checkBonusPoint($row_data);

            $point_bonus = $calcBonus['point'];
            $split_bonus = $calcBonus['split'];
            $multiple = $calcBonus['multiple'];
			*/
			$calcBonus = [];
			    $point_bonus = 0;
                $split_bonus = 0;
                $multiple = 0;

                $calcBonusPayment = $this->_checkBonusPayment($row_data, $row_data['payment_method']);

                $point_bonus_payment = $calcBonusPayment['point'];
                $multiple_payment = $calcBonusPayment['multiple'];

                $postTempPayment = new PostTempPayment();
                $postTempPayment->POSTTEMPPAYMENT_RECID = strtoupper(Uuid::uuid());
                $postTempPayment->STOREID = $data['STOREID'];
                $postTempPayment->RCPDATE = $data['RCPDATE'];
                $postTempPayment->RCPTIME = $data['RCPTIME'];
                $postTempPayment->CARDID = $data['CARDID'];
                $postTempPayment->TENDNBR = $row_data['payment_method'];
                $postTempPayment->BIN = $data['BIN'];
                $postTempPayment->POINT_BONUS = $point_bonus_payment;
                $postTempPayment->MULTIPLE = $multiple_payment;
                $postTempPayment->save();

                $postTemp = new PostTemp();
                $postTemp->POSTTEMP_RECID = strtoupper(Uuid::uuid());
                $postTemp->STOREID = $data['STOREID'];
                $postTemp->INTCODE = $article->PRO_CODE;
                $postTemp->RCPDATE = $data['RCPDATE'];
                $postTemp->RCPTIME = $data['RCPTIME'];
                $postTemp->CARDID = $data['CARDID'];
                $postTemp->TOTALAFTERDISC = $row_data['amount'];
                $postTemp->ITEMQTY = $row_data['qty'];
                $postTemp->POINT_BONUS = $point_bonus;
                $postTemp->SPLIT_BONUS = $split_bonus;
                $postTemp->MULTIPLE = $multiple;
                $postTemp->save();

                //$this->_logsPFBonusProses($postTemp->POSTTEMP_RECID, $calcBonus['pf'], 3, $point_bonus);
                $this->_logsPFBonusProses($postTempPayment->POSTTEMPPAYMENT_RECID, $calcBonusPayment['pf'], 4, $point_bonus_payment);

                $POSTTEMP_RECID[] = $postTemp->POSTTEMP_RECID;
                $POSTTEMPPAYMENT_RECID[] = $postTempPayment->POSTTEMPPAYMENT_RECID;
            }

            $newErn = PostTemp::select('CARDID', DB::raw('SUM(TOTALAFTERDISC) as total'), DB::raw('SUM(POINT_BONUS) as total_point_bonus'), DB::raw('SUM(SPLIT_BONUS) as total_split_bonus'))
                    ->groupBy('CARDID');



            foreach ($newErn->get() as $row) {

                $bonus_combine_fix = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 0)
                        ->orderBy('point', 'DESC')
                        ->groupBy('INTCODE')
                        ->first();

                $bonus_combine_multiple = PostTemp::select(DB::raw('SUM(POINT_BONUS) as point'), DB::raw('SUM(SPLIT_BONUS) as split'), DB::raw('SUM(TOTALAFTERDISC) as total'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 1)
                        ->orderBy('point', 'DESC')
                        ->groupBy('INTCODE')
                        ->first();


                $bonus_payment_fix = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 0)
                        ->groupBy('TENDNBR')
                        ->orderBy('point', 'DESC')
                        ->first();

                $bonus_payment_multiple = PostTempPayment::select(DB::raw('SUM(POINT_BONUS) as point'))
                        ->where('CARDID', $row->CARDID)
                        ->where('MULTIPLE', 1)
                        ->groupBy('TENDNBR')
                        ->orderBy('point', 'DESC')
                        ->first();


                $bonus_payment = (isset($bonus_payment_fix->point) ? $bonus_payment_fix->point : 0) + (isset($bonus_payment_multiple->point) ? $bonus_payment_multiple->point : 0);



                //echo $bonus_payment.' = '.(isset($bonus_payment_fix->point)?$bonus_payment_fix->point:0).' + '.(isset($bonus_payment_multiple->point)?$bonus_payment_multiple->point:0); exit;

                $bonus_combine_fix_point = (isset($bonus_combine_fix->point) ? $bonus_combine_fix->point : 0);
                $bonus_combine_fix_split = (isset($bonus_combine_fix->split) ? $bonus_combine_fix->split : 0);
                $bonus_payment_point = (isset($bonus_payment) ? $bonus_payment : 0);

                $bonus_combine_multiple_point = (isset($bonus_combine_multiple->point) ? $bonus_combine_multiple->point : 0);
                $bonus_combine_multiple_split = (isset($bonus_combine_multiple->split) ? $bonus_combine_multiple->split : 0);

                $point = $bonus_combine_fix_point + $bonus_combine_multiple_point + $bonus_payment_point;
                $split = $bonus_combine_fix_split + $bonus_combine_multiple_split;

                $totalAmount = (isset($bonus_combine_multiple->total) ? $bonus_combine_multiple->total : 0) + (isset($bonus_combine_fix->total) ? $bonus_combine_fix->total : 0);
                $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
                $minimum_transaction = $preference['value'];




                if ($totalAmount < $minimum_transaction) {  //jika kurang dari minimum transaction
                    $point = 0;
                }


                $sumErn = $row->total;

                $postTemp = PostTemp::where('CARDID', $row->CARDID)->first();

                $customer = Customer::where('CUST_BARCODE', $row->CARDID)->first();

                $bonus_grouping = $this->_checkBonusPointGroup($postTemp, $customer, $sumErn);



                $ld_grouping = $this->_checkLDPointGroup($postTemp, $customer, $sumErn);

                $split_grouping = $bonus_grouping['split'];

                $sumErn_split = $sumErn - $split - $split_grouping;

//echo "$sumErn_split = $sumErn - $split - $split_grouping"; exit;



                $temp_paymentMethod = PostTempPayment::where('TENDNBR', $row->TENDNBR)->first();

                $ress_point_regular = $this->_checkRegularPoint($postTemp, $sumErn_split, $customer, $temp_paymentMethod);
                $ress_point_regular_nonsplit = $this->_checkRegularPoint($postTemp, $sumErn, $customer, $temp_paymentMethod);
//dd($ress_point_regular);
                $point_regular = ($ress_point_regular['point'] == NULL) ? 0 : $ress_point_regular['point'];
                $point_regular_nonsplit = ($ress_point_regular_nonsplit['point'] == NULL) ? 0 : $ress_point_regular_nonsplit['point'];


                $ld = 0;
                $get_reward = $this->_checkRewardPoint($postTemp, $customer, $sumErn);
          
				$point_reward = $get_reward['point_fix'] + ($get_reward['point_multiple'] * $point_regular_nonsplit);



                if (isset($customer->CUST_RECID)) {

                    $tenant = Tenant::where('TNT_CODE', $postTemp->STOREID)->first();

                    $post = new PostTest();
                    $post->POS_RECID = Uuid::uuid();
                    $post->POS_CUST_RECID = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                    $post->POS_PNUM = $pnum;
                    $post->POS_PTYPE = $ptype;
                    $post->POS_STORE = $tenant->TNT_RECID;
                    $post->POS_BARCODE = $row->CARDID;
                    $post->POS_POST_DATE = $postTemp->RCPDATE;
                    $post->POS_POST_TIME = $postTemp->RCPTIME;
                    $post->POS_STATION_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                    $post->POS_SHIFT_ID = isset($postTemp->TERMID) ? $postTemp->TERMID : '-';
                    $post->POS_DOC_NO = $DOC_NO;
                    $post->POS_CASHIER_ID = $postTemp->TERMID;
                    $post->POS_AMOUNT = $sumErn;
                    $post->POS_POINT_LD = $ld;
                    $post->POS_POINT_REGULAR = $point_regular;
                    $post->POS_POINT_REWARD = $point_reward;
                    $post->POS_POINT_BONUS = $point + $bonus_grouping['point'];
                    $post->POS_RECEIPT_TYPE = '1';
                    $post->POS_USERBY = (isset(Session::get('data')->USER_DATA->USER_RECID)) ? Session::get('data')->USER_DATA->USER_RECID : 'By System';
                    $post->POS_CREATEBY = '-';
                    $post->POS_UPDATE = 1;
                    $post->POS_TYPE = 0;
                    $post->POS_TRANSF_FROM = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                    $post->POS_TRANSF_TO = (isset($customer->CUST_RECID) ? $customer->CUST_RECID : '-');
                    $post->POS_RECEIPT_NO = $postTemp->RCPNBR;
                    $post->TENDNBR = (isset($postTemp->payment->TENDNBR)) ? $postTemp->payment->TENDNBR : 0;
                    $post->BIN = (isset($postTemp->payment->BIN)) ? $postTemp->payment->BIN : 0;
                    $post->save();

                    $this->_logsPF($post->POS_RECID, $ress_point_regular['pf'], 1);
                    $this->_logsPF($post->POS_RECID, $get_reward['pf'], 2);
                    $this->_logsPF($post->POS_RECID, $bonus_grouping['pf'], 3);
                    
                    $this->_logsPF($post->POS_RECID, $ld_grouping['pf'], 5);
                    
                     $this->_logsPFBonus($POSTTEMP_RECID, $POSTTEMPPAYMENT_RECID, $post->POS_RECID);
                }
            }

            // jika didapatkan lucky draw
            if ($ld_grouping['point'] > 0) {


                // looping sesuai jumlah LD yang didapat
                for ($i = 1; $i <= $ld_grouping['point']; $i++) {

                    //$this->writeLog('Query 15 ke ' . $i, FALSE);
                    $getInc = PointRandom::select('RAND_RECID')->count();
                    //$this->writeLog('Proses Get LD Number ' . $i, FALSE);
                    $kode = 'TESTING-'.$this->_getLDNumber($getInc, $postTemp->STOREID, $post->POS_DOC_NO);

                    //$this->writeLog('Proses Save LD ' . $i, FALSE);
                    $pointRandom = new PointRandom();
                    $pointRandom->RAND_RECID = Uuid::uuid();
                    $pointRandom->RAND_CUST_RECID = $customer->CUST_RECID;
                    $pointRandom->RAND_POS_RECID = $post->POS_RECID;
                    $pointRandom->RAND_TYPE = 1;
                    $pointRandom->RAND_POINT_1 = $kode;   //  incremental (4 digit) ,kode store (4 digit), receipt no (5)
                    $pointRandom->RAND_STS = 0;
                    $pointRandom->RAND_WIN = 0;
                    $pointRandom->save();
                }
                
              

            }

            DB::table('POSTTEMP')->delete();
            DB::table('POSTTEMPPAYMENT')->delete();

            return compact('point_regular', 'point_reward');
        } else {
            return 'Data not found';
        }
    }

    public function _getLDNumber($getInc, $kode_store, $rcp_number) {

        $getIncPlus = $getInc + 1;
        $inc = str_pad($getIncPlus, 10, "0", STR_PAD_LEFT);

        $kode = $inc;
        $checkInc = PointRandom::select('RAND_POINT_1')->where('RAND_POINT_1', $kode)->count();

        if ($checkInc > 0) {
            $this->_getLDNumber($inc, $kode_store, $rcp_number);
        } else {
            return $kode;
        }
    }

    public function _checkRegularPoint($row, $sumErn, $customer, $temp_paymentMethod = null) {
        $point = 0;
        $pf = array();
        $formula = FormulaEarn::where('ERN_TYPE', 0)
                ->where('ERN_FORMULA', 0) // regular point
                ->where('ERN_FORMULA_DTL', 0)
                ->where('ERN_ACTIVE', 1) // active
                ->where('ERN_VALUE', 0)
                ->where('ERN_FRDATE', '<=', $row->RCPDATE) // from date
                ->where('ERN_TODATE', '>=', $row->RCPDATE); // to date
        // to datetime
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

		
		//echo '<pre>';
		//print_r($row); echo '<br/>';
		//print_r($sumErn); echo '<br/>';
		//print_r($customer);
		//exit;
		
		
        $formula_check = $formula->get();


        $formula_load = array();

        //check batas waktu (jam, menit, second) jika formula aktif ditanggal awal dan tanggal ahir
        foreach ($formula_check as $row_check) {
            if (!empty($row_check) && $row_check->ERN_AMOUNT != 0) { // jika dapet point
                if ($row->RCPDATE == $row_check->ERN_FRDATE) { //jika tanggal masuk di batas awal dan batas akhir
                    $formula_time = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME);
                    $row_check_time = $formula_time->first();
                    if (count($row_check_time) > 0) {
                        $formula_load[] = $row_check;
						
                    }
                } elseif ($row->RCPDATE == $row_check->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                    $formula_time = $formula->where('ERN_TOTIME', '>=', $row->RCPTIME);
                    $row_check_time = $formula_time->first();
                    if (count($row_check_time) > 0) {
                        $formula_load[] = $row_check;
						
                    }
                } else {
                    $formula_load[] = $row_check;
				
                }
            }

        }



        $best_point = array(); // untuk mencari best point

        if (!empty($formula_load)) {


            foreach ($formula_load as $formula_ress) {

                if (isset($formula_ress->ERN_AMOUNT)) {

                    $temp1 = (float) $sumErn;
                    $temp2 = (float) $formula_ress->ERN_AMOUNT;

					
                    if ($formula_ress->ERN_MULTIPLE_BASIC == 1) { // jika multiple basic ON
                        $pointamount = floor(@($temp1 / $temp2));
                        // ex : 95.000/50.000 = 1
                        $point = $pointamount * $formula_ress->ERN_POINT;
					//echo "$point = $pointamount * {$formula_ress->ERN_POINT}";
                        //jika normal point
                    } else {
                        if ($sumErn >= $formula_ress->ERN_AMOUNT) {
                            $point = 1 * $formula_ress->ERN_POINT;
							//echo "$point = 1 * {$formula_ress->ERN_POINT}";
                        } else {
                            $point = 0;
                        }
                    }


                    // Min Max Amount
                    if ($formula_ress->ERN_ACTIVE_MAX == 1) {
                        if ($sumErn < $formula_ress->ERN_MIN_AMOUNT) { //jika Belanja Kurang Dari Minimum Amount
                            $point = $point;
                        }

                        if ($sumErn >= $formula_ress->ERN_MIN_AMOUNT && $sumErn <= $formula_ress->ERN_MAX_AMOUNT) {
                            //jika belanja diantara rentang Min Max
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {
                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;
                            }
                        }

                        if ($sumErn > $formula_ress->ERN_MAX_AMOUNT && $formula_ress->ERN_MAX_AMOUNT != 0) { //jika Belanja Lebih Dari Max Amount
                            if ($formula_ress->ERN_MULTIPLE == 1 && $formula_ress->ERN_MULTIPLE_VALUE != 0) {

                                $moreErn = $sumErn - $formula_ress->ERN_MAX_AMOUNT;
                                $moreAmount = floor(@($moreErn / $formula_ress->ERN_AMOUNT));
                                $pointamount = floor(@($formula_ress->ERN_MAX_AMOUNT / $formula_ress->ERN_AMOUNT));
                                $morePoint = $moreAmount * $formula_ress->ERN_POINT;
                                $point_inmax = ($pointamount * $formula_ress->ERN_POINT) * $formula_ress->ERN_MULTIPLE_VALUE;
                                $point = $point_inmax + $morePoint;
                                //point < Max dikalikan multipler dan yang > max ($morePoint) diberikan normal point kemudian keduanya dijumlahkan
                                //                        echo 'Total Belanja = ' . $sumErn . '<br/>';
                                //                        echo 'Max = ' . $formula_ress->ERN_MAX_AMOUNT . '<br/>';
                                //                        echo 'Min = ' . $formula_ress->ERN_MIN_AMOUNT . '<br/>';
                                //                        echo 'Sisa Total Belanja = ' . $moreAmount . '<br/>';
                                //                        echo 'Point Dalam Max = ' . '(' . $pointamount . '*' . $formula_ress->ERN_POINT . ') *' . $formula_ress->ERN_MULTIPLE_VALUE . ' = ' . $point_inmax . '<br/>';
                                //                        echo 'Point Lebih = ' . $moreAmount . ' * ' . $formula_ress->ERN_POINT . ' = ' . $morePoint . '<br/>';
                                //                        echo 'Total Point = ' . $point;
                                //                        exit;
                            }
                        }
                    }

                    if ($formula_ress->ERN_ACTIVE_MAXAMOUNT == 1) { // jika MAX AMOUNT active
                        if ($formula_ress->ERN_ACTIVE_REST == 1) { // jika REST AMOUNT active
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $temp3 = $sumErn - $formula_ress->ERN_MAXAMOUNT_VALUE; //selisih lebih

                                $pointamount = floor(@($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2));
                                $point = $pointamount * $formula_ress->ERN_POINT;

                                $pointamountress = floor(@($temp3 / $formula_ress->ERN_AMOUNT_REST));
                                $pointress = $pointamountress * $formula_ress->ERN_POINT_REST;

                                $point = $point + $pointress; //point normal + point ress
                            }
                        } elseif ($formula_ress->ERN_ACTIVE_REST == 0) { // jika REST AMOUNT inactive
                            if ($sumErn >= $formula_ress->ERN_MAXAMOUNT_VALUE) { // jika belanja lebih dari Max Amount
                                $pointamount = floor(@($formula_ress->ERN_MAXAMOUNT_VALUE / $temp2));
                                $point = $pointamount * $formula_ress->ERN_POINT;
                            }
                        }
                    }

				

					

                    if (isset($customer->CUST_GENDER)) {
                        switch ($customer->CUST_GENDER) {
                            case 'M':
                                $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                                break;
                            case 'F':
                                $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                                break;
                        } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
                    }

                    $formulaMember = FormulaEarnMember::where('MEM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('MEM_LOK_RECID', '=', $customer->CUST_MEMTYPE)->get();
                    $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
                    $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
                    $formulaCardType = FormulaEarnCardType::where('CDTY_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CDTY_LOK_RECID', '=', $customer->CUST_CARDTYPE)->get();
                    //card type di point formula diubah refer ke z_cardtype bukan ke z_lookup kembali

                    if ($temp_paymentMethod != null) {
                        $payment_method_recid = PaymentMethod::where('code', $temp_paymentMethod)->first();
                        $formulaCardPayment = FormulaEarnPayment::where('PAY_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('PAY_LOK_RECID', '=', $payment_method_recid['id'])->get();
                        if (count($formulaCardPayment) < 1) {
                            $point = 0; //gender tidak ditemukan
                            //echo 'payment tidak ditemukan <br/>'; exit;
                        }
                    }

                    //formula ern store belum dibuat model dan table nya

                    $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();
                    $tenant = Tenant::where('TNT_CODE', $row->STOREID)->first();
                    $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                    ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                    ->where('TENT_ACTIVE', 1)->get();




                    if (count($formulaMember) < 1) {
                        $point = 0; //memtype tidak ditemukan
                        //echo 'member type tidak ditemukan <br/>';
                    }
				    if (count($formulaGender) < 1) {
                        $point = 0; //gender tidak ditemukan
                        //echo 'gender tidak ditemukan'; exit;
                    }
                    if (count($formulaReligion) < 1) {
                        $point = 0; //gender tidak ditemukan
                        //echo 'religion tidak ditemukan <br/>';
                    }
                    if (count($formulaCardType) < 1) {
                        $point = 0; //Card Type tidak ditemukan
                        //echo 'card type tidak ditemukan <br/>'; exit;
                    }
                    if (count($formulaBlood) < 1) {
                        $point = 0; //gender tidak ditemukan
                        //echo 'blood tidak ditemukan <br/>';
                    }


                    if (isset($point) && $point < 0) {
                        $point = 0;
                    }

                    if ($point == null) {
                        $point = 0;
                    }
                   
                    $best_point[] = $point;

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                    $pf[] = $pf_founded;
			
					
                }
            }
        }



        $return_point = 0;

        if (count($best_point) != 0) {
            $return_point = max($best_point);
        }

        return ['point' => $return_point, 'pf' => $pf];
    }

    public function _checkRewardPoint($row, $customer, $sumErn) {
        $point = 0;
        $fixed = 0;
        $pf = array();
        $preference = Preference::select('value')->where('key', 'minimum_transaction_reward')->first();
        $minimum_transaction = $preference['value'];

        $formula = FormulaEarn::where('ERN_FORMULA', 2)
                ->where('ERN_ACTIVE', 1)
                ->where('ERN_VALUE', 0)
                ->where('ERN_FRDATE', '<=', $row->RCPDATE)
                ->where('ERN_TODATE', '>=', $row->RCPDATE);  //ketika belanja masuk priode
        //   $formulaRange = $formula->where('ERN_RANGE_AMOUNT',1);

        $formula_check = $formula->get();


        $formula_load = array();

        //check batas waktu (jam, menit, second) jika formula aktif ditanggal awal dan tanggal ahir
        //check batas waktu (jam, menit, second) jika formula aktif ditanggal awal dan tanggal ahir
        foreach ($formula_check as $row_check) {

            if ($row->RCPDATE == $row_check->ERN_FRDATE) { //jika tanggal masuk di batas awal dan batas akhir
                $row_check_time = $formula->where('ERN_FRTIME', '<=', $row->RCPTIME)->first();
                if (count($row_check_time) > 0) {
                    $formula_load[] = $row_check;
                }
            } else {
                $formula_load[] = $row_check;
            }

            if ($row->RCPDATE == $row_check->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                $row_check_time = $formula->where('ERN_TOTIME', '>=', $row->RCPTIME)->first();
                if (count($row_check_time) > 0) {
                    $formula_load[] = $row_check;
                }
            } else {
                $formula_load[] = $row_check;
            }
        }

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point

        if (!empty($formula_load)) {


            foreach ($formula_load as $formula_ress) {

                $point = 0;

                if (isset($formula_ress->ERN_FIXED)) {

                    $fixed = $formula_ress->ERN_FIXED;

                    if ($formula_ress->ERN_FORMULA_DTL == 1) {
                        //Member Get Member Reward
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 2) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);
                        //Birthday Reward
                        if ($cust_dob[1] == $rcpdate[1]) { // kondisi Reward on same Month
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 5) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);

                        $bd = $rcpdate[0] . '-' . $cust_dob[1] . '-' . $cust_dob[2];

                        $cust_trans = strtotime($row->RCPDATE); // tanggal transaksi
                        $cust_bd_weekly_start = strtotime($bd); // tanggal awal periode Birthday
                        $cust_bd_weekly_end = strtotime($bd . " +7 days"); // tanggal akhir periode Birthday

                        if ($cust_trans >= $cust_bd_weekly_start and $cust_trans <= $cust_bd_weekly_end) { // kondisi Reward on same Weekly
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 6) {
                        $cust_dob = explode('-', $customer->CUST_DOB);
                        $rcpdate = explode('-', $row->RCPDATE);
                        //Birthday Reward

                        if ($cust_dob[1] == $rcpdate[1] and $cust_dob[2] == $rcpdate[2]) { // kondisi Reward on same Daily
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 3) {
                        //Butler Service Reward
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 4) {
                        // Reward New Registration dan Update Profile on Web Site
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 7) {
                        // Company Anniversary
                        $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                    } elseif ($formula_ress->ERN_FORMULA_DTL == 8) {
                        // Store Anniversary

                        $tenant = Tenant::where('TNT_CODE', $row->STOREID)->first();

                        $found = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                        ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                        ->where('TENT_ACTIVE', 1)->get();

                        if (count($found) > 0) {
                            $point = ($formula_ress->ERN_FIXED == 1) ? $formula_ress->ERN_POINT : $formula_ress->ERN_MULTIPLE_VALUE;
                        }
                    }

                    // Min Max Amount
                    if ($formula_ress->ERN_ACTIVE_MAX == 1) {

                        if ($sumErn < $formula_ress->ERN_MIN_AMOUNT) { //jika Belanja Kurang Dari Minimum Amount
                            $point = 0;
                        }

                        if ($sumErn > $formula_ress->ERN_MAX_AMOUNT) { //jika Belanja Lebih Dari Minimum Amount
                            $point = 0;
                        }
                    }

                    if ($sumErn < $minimum_transaction) {  // jika dibawah minimum transaction
                        $point = 0;
                    }
                }

                if ($point < 0) {
                    $point = 0;
                }

			
				
                if ($formula_ress->ERN_FIXED == 1) {
                    $best_point_fix[] = $point;
                } else {
					
                    $best_point_multiple[] = $point;
                }

                $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                $pf[] = $pf_founded;
            }
        }


	

        if (count($best_point_fix) != 0) {
            $max_best_point_fix = max($best_point_fix);
        } else {
            $max_best_point_fix = 0;
        }

        if (count($best_point_multiple) != 0) {
            $max_best_point_multiple = max($best_point_multiple);
        } else {
            $max_best_point_multiple = 0;
        }


        $ress = array('point_fix' => $max_best_point_fix, 'point_multiple' => $max_best_point_multiple, 'fixed' => $fixed, 'pf' => $pf);

        return $ress;
    }

    public function _checkBonusPoint($row) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();

        $pf = array();
        $TOTALAFTERDISC = $row['TOTALAFTERDISC'];
        $ITEMQTY = $row['ITEMQTY'];



        $group['brand_qty'] = 0;
        $group['brand_amount'] = 0;
        $group['vendor_qty'] = 0;
        $group['vendor_amount'] = 0;
        $group['dept_qty'] = 0;
        $group['dept_amount'] = 0;
        $group['div_qty'] = 0;
        $group['div_amount'] = 0;
        $group['cat_qty'] = 0;
        $group['cat_amount'] = 0;
        $group['subcat_qty'] = 0;
        $group['subcat_amount'] = 0;
        $group['subcat_qty'] = 0;
        $group['subcat_amount'] = 0;
        $group['segment_qty'] = 0;
        $group['segment_amount'] = 0;
        $group['vendor_qty'] = 0;
        $group['vendor_amount'] = 0;



        $bonus_type = [
            1 => 'Point Earn Bonus by Product',
            9 => 'All Product'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point

        $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
        $minimum_transaction = $preference['value'];

        $formulaItem = array();

        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA', 1)
                    ->where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row['RCPDATE'])
                    ->where('ERN_TODATE', '>=', $row['RCPDATE']);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();

            if (!empty($formula_load)) {
                foreach ($formula_load as $formula_ress) {
                    $point = 0;
                    $check_time = TRUE;

                    if ($row['RCPDATE'] == $formula_ress->ERN_FRDATE) {  //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                    }

                    if ($row['RCPDATE'] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                    }

                    if (isset($row['INTCODE']) && $row['INTCODE'] != '') {

                        if ($key_bonus_type == 1 AND $check_time == TRUE) { // jika produk
                            //Product
                            $product = Product::select('PRO_CODE', 'PRO_RECID')->where('PRO_CODE', $row['INTCODE'])->first();
                            $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('ITM_PRODUCT', '=', $product['PRO_RECID'])->get();
                        } elseif ($key_bonus_type == 2 and $check_time == true) { // jika brand
                            //Brand
                            $product = Product::select('PRO_CODE', 'PRO_DESCR', 'PRO_RECID', 'brand_id')->where('PRO_CODE', $row['INTCODE'])->first();
                            $brands = Brand::select('id', 'code', 'name')->where('id', $product['brand_id'])->first();
                            $formulaItem = FormulaEarnBrand::where('BRAND_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BRAND_BRAND_RECID', '=', $brands['id'])->get();

                            if (count($formulaItem) > 0) {
                                $group['brand_qty'] = $group['brand_qty'] + $ITEMQTY;
                                $group['brand_amount'] = $group['brand_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['brand_qty'] = 0;
                                $group['brand_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                            //Department
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row['INTCODE'])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnDepartment::where('DEPT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DEPT_DEPARTMENT', '=', $merchant->department->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {
                                $group['dept_qty'] = $group['dept_qty'] + $ITEMQTY;
                                $group['dept_amount'] = $group['dept_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['dept_qty'] = 0;
                                $group['dept_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                            //Division
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row['INTCODE'])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnDivision::where('DIV_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('DIV_DIVISION', '=', $merchant->division->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['div_qty'] = $group['div_qty'] + $ITEMQTY;
                                $group['div_amount'] = $group['div_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['div_qty'] = 0;
                                $group['div_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                            //Category
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row['INTCODE'])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnCategory::where('CAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('CAT_CATEGORY', '=', $merchant->category->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {
                                $group['cat_qty'] = $group['cat_qty'] + $ITEMQTY;
                                $group['cat_amount'] = $group['cat_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['cat_qty'] = 0;
                                $group['cat_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                            //Departement
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row['INTCODE'])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnSubCategory::where('SCAT_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SCAT_SUBCATEGORY', '=', $merchant->subcategory->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['subcat_qty'] = $group['subcat_qty'] + $ITEMQTY;
                                $group['subcat_amount'] = $group['subcat_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['subcat_qty'] = 0;
                                $group['subcat_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                            //Departement
                            $product = Product::select('PRO_CODE', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row['INTCODE'])->first();
                            $merchant = Merchandise::where('MERC_CODE', $product['PRO_MERCHANDISE'])->first();
                            $formulaItem = FormulaEarnSegment::where('SEG_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('SEG_SEGMENT', '=', $merchant->segment->LOK_RECID)->get();

                            if (count($formulaItem) > 0) {

                                $group['segment_qty'] = $group['segment_qty'] + $ITEMQTY;
                                $group['segment_amount'] = $group['segment_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['segment_qty'] = 0;
                                $group['segment_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                            $product = Product::select('PRO_CODE', 'PRO_DESCR', 'PRO_RECID', 'PRO_MERCHANDISE')->where('PRO_CODE', $row['INTCODE'])->first();
                            $merchant = $product->vendors()->get();
                            $countVendor = array();
                            foreach ($merchant as $row_vendor) {
                                $formulaVendor = FormulaEarnVendor::where('VND_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('VND_VENDOR_RECID', '=', $row_vendor['PROV_VDR_RECID'])->get();
                                if (count($formulaVendor) > 0) {
                                    $countVendor[] = $row_vendor['PROV_VDR_RECID'];
                                }
                            }

                            $formulaItem = $countVendor;

                            if (count($formulaItem) > 0) {
                                $group['vendor_qty'] = $group['vendor_qty'] + $ITEMQTY;
                                $group['vendor_amount'] = $group['vendor_amount'] + $TOTALAFTERDISC;
                            } else {
                                $group['vendor_qty'] = 0;
                                $group['vendor_amount'] = 0;
                            }
                        } elseif ($key_bonus_type == 9 AND $check_time == TRUE) {

                            $formulaItem = array(0 => 'All Product', 1 => 'All Product');
                        }
                    }


                    if (count($formulaItem) > 0) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount <br/>' : 'by quantity <br/>';
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = $TOTALAFTERDISC - $sisa_bagi; // jika terdapat sisa bagi,, akan ditampung di split
                                    $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;

                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        } elseif ($TOTALAFTERDISC < $formula_ress->ERN_AMOUNT) {
                                            $point = 0;
                                        }
                                        if ($TOTALAFTERDISC < $minimum_transaction) {
                                            $point = 0;
                                        }



                                        $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                                    }
                                }
                            }
                        }





                        if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                            // $row['ITEMQTY'] = $row['ITEMQTY'] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;

                                $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                            }

                            if (env('DEBUG_BONUS', FALSE)) {
                                // echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                            }
                        }
                    }

					$RCPDATE = $row['RCPDATE'];
					
                    $point = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $RCPDATE);

                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }

                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = max($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;



                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }

        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }

    public function _checkLDPointGroup($row, $customer, $sumErn, $fileName = null) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();

        $row['RCPDATE'] = $row->RCPDATE->toDateString();

        $pf = array();


        $bonus_type = [
            1 => 'By Products',
            2 => 'Point Earn LD by Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendors',
            9 => 'All Product'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point

        if ($row['RCPDATE'] != NULL) {


            $row['RCPDATE'] = $row->RCPDATE->toDateString();
            //dd($row->RCPDATE->toDateString());

            foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {
                //$this->writeLog('Query LD Group Ke 1 ', FALSE);
                $formula = FormulaEarn::where('ERN_FORMULA_DTL', 0)
                        ->where('ERN_ACTIVE', 1)
                        ->where('ERN_FRDATE', '<=', $row->RCPDATE->toDateString())
                        ->where('ERN_TODATE', '>=', $row->RCPDATE->toDateString())
                        ->where('ERN_FORMULA', 3);

                $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
                $formula_load = $formula->get();



                if (!empty($formula_load)) {
                    foreach ($formula_load as $formula_ress) {
                        $point = 0;
                        $check_time = TRUE;

                        if ($row->RCPDATE->toDateString() == $formula_ress->ERN_FRDATE) {  //jika tanggal masuk di batas awal dan batas akhir
                            //$this->writeLog('Query LD Group Ke 2 ', FALSE);
                            $formula = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME'])
                                    ->where('ERN_RECID', $formula_ress->ERN_RECID);

                            $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                        }
                        if ($row->RCPDATE->toDateString() == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di batas awal dan batas akhir
                            //$this->writeLog('Query LD Group Ke 2 ', FALSE);
                            $formula = $formula->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                    ->where('ERN_RECID', $formula_ress->ERN_RECID);

                            $check_time = (!empty($formula->first()) ) ? TRUE : FALSE;
                        }


                        if ($key_bonus_type == 1 AND $check_time == TRUE) { // jika produk
                            //Product
                            $ERN_RECID = $formula_ress->ERN_RECID;
                            //$this->writeLog('Query Bonus Point Group Ke 4 ', FALSE);
                            $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->get();


                            $getProduct = array();
                            $rowGroup = array();

                            foreach ($formulaItem as $formulaItem_product) {
                                $count = DB::select("select SUM(ITEMQTY) as qty, 
                            SUM(TOTALAFTERDISC) as total
                            from POSTTEMP  
                            WHERE CARDID = '{$row->CARDID}'
                            AND INTCODE IN (select PRO_CODE from Z_Product_Item where PRO_RECID = '{$formulaItem_product->ITM_PRODUCT}') ");

                                $count = $count[0];
                                $this->writeLog('Query Bonus Point Group Ke 6 ', FALSE);
                                $rowGroup[] = array(
                                    'product_name' => '',
                                    'qty' => $count->qty,
                                    'total' => $count->total,
                                    'post' => []);
                            }
                        } elseif ($key_bonus_type == 2 and $check_time == true) { // jika brand
                            //Brand
                            $ERN_RECID = $formula_ress->ERN_RECID;

                            $formulaItem = $formula_ress->brand;

                            $getProduct = array();
                            $rowGroup = array();

                            foreach ($formulaItem as $formulaItem_brand) {
                                $this->writeLog('Query LD Group Ke 3 ', FALSE);
                                $formulaItem_product = Product::select('*')->where('brand_id', $formulaItem_brand->brand->id)->get();

                                foreach ($formulaItem_product as $formulaItem_row) {
                                    $getProduct[] = $formulaItem_row->PRO_CODE;
                                }
                                // tambah where FILE = .....
                                // Mendapatkan product dari Brand terkait di table POSTEMP
                                //$this->writeLog('Query LD Group Ke 4 ', FALSE);
                                $rowGroup[] = array('brand' => $formulaItem_brand->brand->id,
                                    'brand_name' => $formulaItem_brand->brand->name,
                                    'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                    'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                    'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            }
                        } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                            //Department
                            $formulaItem = $formula_ress->department;

                            $getProduct = array();
                            $rowGroup = array();
                            $mercCode = array();

                            foreach ($formulaItem as $formulaItem_department) {
                                //$this->writeLog('Query LD Group Ke 5 ', FALSE);
                                $merchant = Merchandise::where('MERC_DEPARTMENT', $formulaItem_department->DEPT_DEPARTMENT)->get();

                                foreach ($merchant as $row_merchand) {
                                    $mercCode[] = $row_merchand['MERC_CODE'];
                                }


                                //$this->writeLog('Query LD Group Ke 6 ', FALSE);
                                $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();

                                foreach ($formulaItem_product as $formulaItem_row) {
                                    $getProduct[] = $formulaItem_row->PRO_CODE;
                                }
                                // tambah where FILE = .....
                                //$this->writeLog('Query LD Group Ke 7 ', FALSE);
                                $rowGroup[] = array('merchand' => $formulaItem_department->department->LOK_RECID,
                                    'merchand_name' => $formulaItem_department->department->LOK_DESCRIPTION,
                                    'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                    'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                    'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            }
                        } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                            //Division
                            $formulaItem = $formula_ress->division;

                            $getProduct = array();
                            $rowGroup = array();
                            $mercCode = array();

                            foreach ($formulaItem as $formulaItem_division) {
                                //$this->writeLog('Query LD Group Ke 8 ', FALSE);
                                $merchant = Merchandise::where('MERC_DIVISION', $formulaItem_division->DIV_DIVISION)->get();

                                foreach ($merchant as $row_merchand) {
                                    $mercCode[] = $row_merchand['MERC_CODE'];
                                }


                                //$this->writeLog('Query LD Group Ke 9 ', FALSE);
                                $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                                foreach ($formulaItem_product as $formulaItem_row) {
                                    $getProduct[] = $formulaItem_row->PRO_CODE;
                                }
                                // tambah where FILE = .....
                                //$this->writeLog('Query LD Group Ke 10 ', FALSE);
                                $rowGroup[] = array('merchand' => $formulaItem_division->division->LOK_RECID,
                                    'merchand_name' => $formulaItem_division->division->LOK_DESCRIPTION,
                                    'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                    'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                    'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            }
                        } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                            $formulaItem = $formula_ress->category;

                            $getProduct = array();
                            $rowGroup = array();
                            $mercCode = array();

                            foreach ($formulaItem as $formulaItem_category) {
                                //$this->writeLog('Query LD Group Ke 11 ', FALSE);
                                $merchant = Merchandise::where('MERC_CATEGORY', $formulaItem_category->CAT_CATEGORY)->get();

                                foreach ($merchant as $row_merchand) {
                                    $mercCode[] = $row_merchand['MERC_CODE'];
                                }



                                $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                                foreach ($formulaItem_product as $formulaItem_row) {
                                    $getProduct[] = $formulaItem_row->PRO_CODE;
                                }
                                // tambah where FILE = .....
                                //$this->writeLog('Query LD Group Ke 12 ', FALSE);
                                $rowGroup[] = array('merchand' => $formulaItem_category->category->LOK_RECID,
                                    'merchand_name' => $formulaItem_category->category->LOK_DESCRIPTION,
                                    'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                    'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                    'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            }
                        } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                            $formulaItem = $formula_ress->subcategory;

                            $getProduct = array();
                            $rowGroup = array();
                            $mercCode = array();

                            foreach ($formulaItem as $formulaItem_subcategory) {
                                //$this->writeLog('Query LD Group Ke 13 ', FALSE);
                                $merchant = Merchandise::where('MERC_SUBCATEGORY', $formulaItem_subcategory->SUBCAT_SUBCATEGORY)->get();

                                foreach ($merchant as $row_merchand) {
                                    $mercCode[] = $row_merchand['MERC_CODE'];
                                }


                                //$this->writeLog('Query LD Group Ke 14 ', FALSE);
                                $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                                foreach ($formulaItem_product as $formulaItem_row) {
                                    $getProduct[] = $formulaItem_row->PRO_CODE;
                                }
                                // tambah where FILE = .....
                                //$this->writeLog('Query LD Group Ke 15 ', FALSE);
                                $rowGroup[] = array('merchand' => $formulaItem_subcategory->subcategory->LOK_RECID,
                                    'merchand_name' => $formulaItem_subcategory->subcategory->LOK_DESCRIPTION,
                                    'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                    'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                    'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            }
                        } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                            $formulaItem = $formula_ress->segment;

                            $getProduct = array();
                            $rowGroup = array();
                            $mercCode = array();

                            foreach ($formulaItem as $formulaItem_segment) {
                                //$this->writeLog('Query LD Group Ke 16 ', FALSE);
                                $merchant = Merchandise::where('MERC_SEGMENT', $formulaItem_segment->SEG_SEGMENT)->get();

                                foreach ($merchant as $row_merchand) {
                                    $mercCode[] = $row_merchand['MERC_CODE'];
                                }


                                //$this->writeLog('Query LD Group Ke 17 ', FALSE);
                                $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                                foreach ($formulaItem_product as $formulaItem_row) {
                                    $getProduct[] = $formulaItem_row->PRO_CODE;
                                }
                                // tambah where FILE = .....
                                //$this->writeLog('Query LD Group Ke 18 ', FALSE);
                                $rowGroup[] = array('merchand' => $formulaItem_segment->segment->LOK_RECID,
                                    'merchand_name' => $formulaItem_segment->segment->LOK_DESCRIPTION,
                                    'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                    'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                    'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            }
                        } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                            $formulaItem = $formula_ress->vendor;
                            // dd($formulaItem );
                            $getProduct = array();
                            $rowGroup = array();

                            foreach ($formulaItem as $formulaItem_vendor) {

                                //$this->writeLog('Query LD Group Ke 19 ', FALSE);
                                $vendor = Vendor::where('VDR_RECID', $formulaItem_vendor->VND_VENDOR_RECID)->first(); //dd($vendor);
                                $formulaItem_product = $vendor->products;

                                foreach ($formulaItem_product as $formulaItem_row) {
                                    $getProduct[] = $formulaItem_row->PRO_CODE;
                                }
                                // tambah where FILE = .....
                                //$this->writeLog('Query LD Group Ke 20 ', FALSE);
                                $rowGroup[] = array('vendor' => $formulaItem_vendor->vendor->VDR_RECID,
                                    'vendor_name' => $formulaItem_vendor->vendor->VDR_NAME,
                                    'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                    'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->first()->total,
                                    'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->whereIn('INTCODE', $getProduct)->get());
                            }
                        } elseif ($key_bonus_type == 9 AND $check_time == TRUE) { // jika all product
                            $getProduct = array();
                            $getProduct[] = 'All Product';
                            $rowGroup = array();
                            //$this->writeLog('Query LD Group Ke 21 ', FALSE);
                            $rowGroup[] = array('product name' => 'All',
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->get());
                        }



                        if (!isset($rowGroup['total'])) {
                            $rowGroup['total'] = 0;
                        }
                        if (!isset($rowGroup['qty'])) {
                            $rowGroup['qty'] = 0;
                        }


                        // ...
                        $TOTALAFTERDISC = array_sum(array_column($rowGroup, 'total'));
                        $ITEMQTY = array_sum(array_column($rowGroup, 'qty'));

                        $ITEMPOST = array_sum(array_column($rowGroup, 'post'));



                        if ($TOTALAFTERDISC > 0 and $ITEMQTY > 0) {

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo ($formula_ress->ERN_VALUE == 0) ? 'by amount ' . PHP_EOL : 'by quantity ' . PHP_EOL;
                            }

                            $multiple = $formula_ress->ERN_MULTIPLE;



                            if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                                //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                                //exit;
                                //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                                $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                                $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                                $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                                $point = $pointamount * $formula_ress->ERN_POINT;

                                if (env('DEBUG_BONUS', FALSE)) {
                                    echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '' . PHP_EOL . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                                }

                                if (isset($formula_ress->ERN_MULTIPLE)) {
                                    if ($formula_ress->ERN_MULTIPLE == 1) {
                                        if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                            $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                        }
                                        $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                        $split[] = 0;
                                        $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                                    } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                        if (isset($pointamount)) {
                                            $ITEMQTY_POINT = $ITEMQTY;
                                            if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                                $point = $formula_ress->ERN_POINT;  // Fix Point
                                            }
                                            $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                                        }
                                    }
                                }
                            }





                            if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                                // $row['ITEMQTY'] = $row['ITEMQTY'] / 1000;  //Qty dibagi per-1000
                                //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                                $ITEMQTY_POINT = $ITEMQTY;
                                //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                                if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                    $point = $formula_ress->ERN_POINT;

                                    $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                                }

                                if (env('DEBUG_BONUS', FALSE)) {
                                    echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '' . PHP_EOL . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                                }
                            }
                        }


                        $point = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);

                        $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                        $pf[] = $pf_founded;
                    }



                    if (count($best_point_fix) != 0) {

                        $max_best_point_fix = array_sum($best_point_fix);
                    } else {
                        $max_best_point_fix = 0;
                    }

                    if (count($best_point_multiple) != 0) {

                        $max_best_point_multiple = max($best_point_multiple);
                        $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                        $last_split = $split[$key_max[0]];
                    } else {
                        $max_best_point_multiple = 0;
                    }

                    $last_poin = $max_best_point_fix + $max_best_point_multiple;



                    if ($last_poin < 0) {
                        $last_poin = 0;
                        $last_split = 0;
                    }
                }
            }
        }

        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }

    public function _checkBonusPointGroup($row, $customer, $sumErn) {
        $point = 0;
        $split = array();
        $last_split = 0;
        $sisa_bagi = 0;
        $last_poin = 0;
        $multiple = 0;
        $group = array();

        $row['RCPDATE'] = $row->RCPDATE->toDateString();

        $pf = array();


        $bonus_type = [
		    1 => 'Point Earn by Product',
            2 => 'Point Earn Bonus by Brand',
            3 => 'Department',
            4 => 'Division',
            5 => 'Category',
            6 => 'SubCategory',
            7 => 'Segment',
            8 => 'Vendors',
            9 => 'All Product'
        ];

        $best_point_fix = array(); // untuk mencari best point
        $best_point_multiple = array(); // untuk mencari best point


        $preference = Preference::select('value')->where('key', 'minimum_transaction_bonus')->first();
                    $minimum_transaction = $preference['value'];


        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA', 1)
                    ->where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row['RCPDATE'])
                    ->where('ERN_TODATE', '>=', $row['RCPDATE']);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();



            if (!empty($formula_load)) {
                foreach ($formula_load as $formula_ress) {
                    $point = 0;
                    $check_time = TRUE;

                    if ($row['RCPDATE'] == $formula_ress->ERN_FRDATE) {  //jika tanggal masuk di batas awal 
                        $formula_check = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);
                        $check_time = (!empty($formula_check->first()) ) ? TRUE : FALSE;
                    }

                    if ($row['RCPDATE'] == $formula_ress->ERN_TODATE) {  //jika tanggal masuk di  batas akhir
                        $formula_check = $formula->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);
                        $check_time = (!empty($formula_check->first()) ) ? TRUE : FALSE;
                    }



                     if ($key_bonus_type == 1 AND $check_time == TRUE) { // jika produk
                        //Product
                        $ERN_RECID = $formula_ress->ERN_RECID;
                        //$this->writeLog('Query Bonus Point Group Ke 4 ',FALSE);
                        $formulaItem = FormulaEarnItems::where('ITM_ERN_RECID', '=', $formula_ress->ERN_RECID)->get();


                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_product) {
                            //query terberat sebelumnya disini
                            $count = DB::select("select SUM(ITEMQTY) as qty,
                            SUM(TOTALAFTERDISC) as total
                            from POSTTEMP
                            WHERE CARDID = '{$row->CARDID}' 
                            AND INTCODE IN (select PRO_CODE from Z_Product_Item where PRO_RECID = '{$formulaItem_product->ITM_PRODUCT}') ");

                            $count = $count[0];
                            $rowGroup[] = array(
                                'product_name' => '',
                                'qty' => $count->qty,
                                'total' => $count->total,
                                'post' => []);
                        }


                    } elseif ($key_bonus_type == 2 and $check_time == true) { // jika brand
                        //Brand
                        $ERN_RECID = $formula_ress->ERN_RECID;

                        $formulaItem = $formula_ress->brand;

                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_brand) {

                            $formulaItem_product = Product::select('*')->where('brand_id', $formulaItem_brand->brand->id)->get();

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            // Mendapatkan product dari Brand terkait di table POSTEMP
                            $rowGroup[] = array('brand' => $formulaItem_brand->brand->id,
                                'brand_name' => $formulaItem_brand->brand->name,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 3 AND $check_time == TRUE) { // jika department
                        //Department
                        $formulaItem = $formula_ress->department;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        						
						foreach ($formulaItem as $formulaItem_department) {
                            //query terberat sebelumnya disini
                            $count = DB::select("select SUM(ITEMQTY) as qty,
                            SUM(TOTALAFTERDISC) as total
                            from POSTTEMP
                            WHERE CARDID = '{$row->CARDID}' 
                            AND INTCODE IN (select PRO_CODE from Z_Product_Item 
							 where PRO_MERCHANDISE in (select MERC_CODE from Z_Merchandise where MERC_DEPARTMENT = '{$formulaItem_department->DEPT_DEPARTMENT}')) ");

                            $count = $count[0];
                            $rowGroup[] = array(
                                'merchand' => $formulaItem_department->department->LOK_RECID,
                                'merchand_name' => $formulaItem_department->department->LOK_DESCRIPTION,
                                'qty' => $count->qty,
                                'total' => $count->total,
                                'post' => []);
                        }
						
						
                    } elseif ($key_bonus_type == 4 AND $check_time == TRUE) { // jika Division
                        //Division
                        $formulaItem = $formula_ress->division;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        
						foreach ($formulaItem as $formulaItem_division) {
                            //query terberat sebelumnya disini
                            $count = DB::select("select SUM(ITEMQTY) as qty,
                            SUM(TOTALAFTERDISC) as total
                            from POSTTEMP
                            WHERE CARDID = '{$row->CARDID}' 
                            AND INTCODE IN (select PRO_CODE from Z_Product_Item 
							 where PRO_MERCHANDISE in (select MERC_CODE from Z_Merchandise where MERC_DIVISION = '{$formulaItem_division->DIV_DIVISION}')) ");

                            $count = $count[0];
                            $rowGroup[] = array(
                                'merchand' => $formulaItem_division->division->LOK_RECID,
                                'merchand_name' => $formulaItem_division->division->LOK_DESCRIPTION,
                                'qty' => $count->qty,
                                'total' => $count->total,
                                'post' => []);
                        }
						
                    } elseif ($key_bonus_type == 5 AND $check_time == TRUE) { // jika Category
                        $formulaItem = $formula_ress->category;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

						
						foreach ($formulaItem as $formulaItem_category) {
                            //query terberat sebelumnya disini
                            $count = DB::select("select SUM(ITEMQTY) as qty,
                            SUM(TOTALAFTERDISC) as total
                            from POSTTEMP
                            WHERE CARDID = '{$row->CARDID}' 
                            AND INTCODE IN (select PRO_CODE from Z_Product_Item 
							 where PRO_MERCHANDISE in (select MERC_CODE from Z_Merchandise where MERC_CATEGORY = '{$formulaItem_category->CAT_CATEGORY}')) ");

                            $count = $count[0];
                            $rowGroup[] = array(
                                'merchand' => $formulaItem_category->category->LOK_RECID,
                                'merchand_name' => $formulaItem_category->category->LOK_DESCRIPTION,
                                'qty' => $count->qty,
                                'total' => $count->total,
                                'post' => []);
                        }
						
						
                    } elseif ($key_bonus_type == 6 AND $check_time == TRUE) { // jika Sub Category
                        $formulaItem = $formula_ress->subcategory;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_subcategory) {
                            $merchant = Merchandise::where('MERC_SUBCATEGORY', $formulaItem_subcategory->SUBCAT_SUBCATEGORY)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            $rowGroup[] = array('merchand' => $formulaItem_subcategory->subcategory->LOK_RECID,
                                'merchand_name' => $formulaItem_subcategory->subcategory->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 7 AND $check_time == TRUE) { // jika Segment
                        $formulaItem = $formula_ress->segment;

                        $getProduct = array();
                        $rowGroup = array();
                        $mercCode = array();

                        foreach ($formulaItem as $formulaItem_segment) {
                            $merchant = Merchandise::where('MERC_SEGMENT', $formulaItem_segment->SEG_SEGMENT)->get();

                            foreach ($merchant as $row_merchand) {
                                $mercCode[] = $row_merchand['MERC_CODE'];
                            }



                            $formulaItem_product = Product::whereIn('PRO_MERCHANDISE', $mercCode)->get();
                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }
                            $rowGroup[] = array('merchand' => $formulaItem_segment->segment->LOK_RECID,
                                'merchand_name' => $formulaItem_segment->segment->LOK_DESCRIPTION,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    } elseif ($key_bonus_type == 8 and $check_time == true) { // jika Vendor
                        $formulaItem = $formula_ress->vendor;
                        // dd($formulaItem );
                        $getProduct = array();
                        $rowGroup = array();

                        foreach ($formulaItem as $formulaItem_vendor) {

                            $vendor = Vendor::where('VDR_RECID', $formulaItem_vendor->VND_VENDOR_RECID)->first(); //dd($vendor);
                            $formulaItem_product = $vendor->products;

                            foreach ($formulaItem_product as $formulaItem_row) {
                                $getProduct[] = $formulaItem_row->PRO_CODE;
                            }

                            $rowGroup[] = array('vendor' => $formulaItem_vendor->vendor->VDR_RECID,
                                'vendor_name' => $formulaItem_vendor->vendor->VDR_NAME,
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->whereIn('INTCODE', $getProduct)->get());
                        }
                    }
					elseif ($key_bonus_type == 9 AND $check_time == TRUE) { // jika all product
                         $getProduct = array();
						  $getProduct[] = 'All Product';
                                                  $rowGroup = array();

			  $rowGroup[] = array('product name' => 'All',
                                'qty' => PostTemp::select(DB::raw('SUM(ITEMQTY) as qty'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->first()->qty,
                                'total' => PostTemp::select(DB::raw('SUM(TOTALAFTERDISC) as total'))->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->first()->total,
                                'post' => PostTemp::select('*')->where('CARDID', $row->CARDID)->where('STOREID', $row->STOREID)->get());

                    }



                    if (!isset($rowGroup['total'])) {
                        $rowGroup['total'] = 0;
                    }
                    if (!isset($rowGroup['qty'])) {
                        $rowGroup['qty'] = 0;
                    }


                    // ...
                    $TOTALAFTERDISC = array_sum(array_column($rowGroup, 'total'));
                    $ITEMQTY = array_sum(array_column($rowGroup, 'qty'));

                    $ITEMPOST = array_sum(array_column($rowGroup, 'post'));




                    if ($TOTALAFTERDISC > 0 and $ITEMQTY > 0 and $TOTALAFTERDISC>=$minimum_transaction) {

                        if (env('DEBUG_BONUS', FALSE)) {
                            echo ($formula_ress->ERN_VALUE == 0) ? 'by amount <br/>' : 'by quantity <br/>';
                        }

                        $multiple = $formula_ress->ERN_MULTIPLE;



                        if ($formula_ress->ERN_VALUE == 0) {   // By Amount
                            //echo " {$group['vendor_amount']} + {$group['brand_amount']} + {$group['dept_amount']} + {$group['div_amount']} + {$group['cat_amount']} + {$group['subcat_amount']} + {$group['segment_amount']}";
                            //exit;
                            //$TOTALAFTERDISC_POINT = $TOTALAFTERDISC+$group['vendor_amount']+$group['brand_amount']+$group['dept_amount']+$group['div_amount']+$group['cat_amount']+$group['subcat_amount'];
                            $TOTALAFTERDISC_POINT = $TOTALAFTERDISC;
                            $pointamount = floor(@($TOTALAFTERDISC_POINT / $formula_ress->ERN_AMOUNT));
                            $sisa_bagi = $TOTALAFTERDISC_POINT % $formula_ress->ERN_AMOUNT;
                            $point = $pointamount * $formula_ress->ERN_POINT;

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by amount<hr/>';
                            }

                            if (isset($formula_ress->ERN_MULTIPLE)) {
                                if ($formula_ress->ERN_MULTIPLE == 1) {
                                    if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                        $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                    }
                                    $point = $point * $formula_ress->ERN_MULTIPLE_VALUE;  // Multiple

                                    $split[] = $TOTALAFTERDISC - $sisa_bagi; // jika terdapat sisa bagi,, akan ditampung di split
                                    $best_point_multiple[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                                } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                    if (isset($pointamount)) {
                                        $ITEMQTY_POINT = $ITEMQTY;
                                        if ($TOTALAFTERDISC >= $formula_ress->ERN_AMOUNT) {
                                            $point = $formula_ress->ERN_POINT;  // Fix Point
                                        }
                                        $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                                    }
                                }
                            }
                        }




                        if ($formula_ress->ERN_VALUE == 1) {   // By Quantity
                            // $row['ITEMQTY'] = $row['ITEMQTY'] / 1000;  //Qty dibagi per-1000
                            //$ITEMQTY_POINT = $ITEMQTY+$group['vendor_qty']+$group['brand_qty']+$group['dept_qty']+$group['div_qty']+$group['cat_qty']+$group['subcat_qty']+$group['segment_qty'];
                            $ITEMQTY_POINT = $ITEMQTY;
                            //$pointamount = floor(@($ITEMQTY / $formula_ress->ERN_QTY));

                            if ($ITEMQTY_POINT >= $formula_ress->ERN_QTY) {
                                $point = $formula_ress->ERN_POINT;

                                $best_point_fix[] = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);
                            }

                            if (env('DEBUG_BONUS', FALSE)) {
                                echo $product['PRO_CODE'] . ' ' . $product['PRO_DESCR'] . '<br/>' . $point . ' = ' . $pointamount . ' * ' . $formula_ress->ERN_POINT . ' <br/> by quantity <hr/>';
                            }
                        }
                    }

//dd($row->RCPDATE->toDateString());

                    $point = $this->_filterTransaction($row['CARDID'], $formula_ress->ERN_RECID, $row['STOREID'], $point, $row['RCPDATE']);

                    


                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point, 'split' => (isset($split[count($split) - 1]) ? $split[count($split) - 1] : 0)];
                    $pf[] = $pf_founded;
                }



                if (count($best_point_fix) != 0) {

                    $max_best_point_fix = array_sum($best_point_fix);
                } else {
                    $max_best_point_fix = 0;
                }

                if (count($best_point_multiple) != 0) {

                    $max_best_point_multiple = max($best_point_multiple);
                    $key_max = array_keys($best_point_multiple, max($best_point_multiple));
                    $last_split = $split[$key_max[0]];
                } else {
                    $max_best_point_multiple = 0;
                }

                $last_poin = $max_best_point_fix + $max_best_point_multiple;


                if ($last_poin < 0) {
                    $last_poin = 0;
                    $last_split = 0;
                }
            }
        }



        $return = array('point' => $last_poin, 'split' => $last_split, 'multiple' => $multiple, 'pf' => $pf, 'group' => $group);

        return $return;
    }

    public function _filterTransaction($CARDID, $ERN_RECID, $STOREID, $point, $date, $std_time = false) {
        //$this->writeLog('Filter Transaction ',FALSE);
        $customer = Customer::where('CUST_BARCODE', $CARDID)->where('CUST_ACTIVE', 1)->first();

        if (isset($customer->CUST_RECID)) {

            if (isset($customer->CUST_GENDER)) {
                switch ($customer->CUST_GENDER) {
                    case 'M':
                        $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                        break;
                    case 'F':
                        $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                        break;
                } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
            }

            if ($customer->CUST_GENDER == null) {
                $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'GEN')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                $customer->CUST_GENDER = $lookUp->LOK_RECID; //other
            }
            if ($customer->CUST_BLOOD == null) {
                $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'BLOD')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                $customer->CUST_BLOOD = $lookUp->LOK_RECID; //other
            }
            if ($customer->CUST_RELIGION == null) {
                $lookUp = Lookup::select('LOK_RECID')->where('LOK_CODE', 'RELI')->where('LOK_DESCRIPTION', 'OTHERS')->first();
                $customer->CUST_RELIGION = $lookUp->LOK_RECID; //other
            }

            $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
            $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
            $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();

            $tenant = Tenant::where('TNT_CODE', $STOREID)->first();
            $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                            ->where('TENT_ERN_RECID', $ERN_RECID)
                            ->where('TENT_ACTIVE', 1)->get();

            if (count($formulaTenant) < 1) {
                $point = 0; //memtype tidak ditemukan
                //echo 'store tidak mendapatkan promo '.PHP_EOL;
            }

            if (count($formulaGender) < 1) {
                $point = 0;
                //echo 'gender tidak mendapatkan promo'.PHP_EOL; 
            }
            if (count($formulaReligion) < 1) {
                $point = 0;
                //echo 'religion tidak mendapatkan promo '.PHP_EOL;
            }
            if (count($formulaBlood) < 1) {
                $point = 0;
                //echo 'blood tidak mendapatkan promo '.PHP_EOL;
            }

            if ($this->_availableDays($date, $ERN_RECID, $std_time) == false) {
                $point = 0;
                //echo 'day not avaible '.PHP_EOL;
            }
        } else {
            $point = 0;
            //echo 'Customer Tidak Aktif '.PHP_EOL;
        }

        return $point;
    }

    public function _availableDays($date, $id, $std_time = false) {
        if ($id !== null) {

            $formulaEarn = FormulaEarn::find($id); // Find Formula ID
            //$date .= ' 00:00:00'; 

		
			
			if (is_object($date)) {
			   $date = $date->toDateString();
            }
			
            $day = Carbon::createFromFormat('Y-m-d', $date); // Parse to Carbon object
            //$day = Carbon::now(); // Parse to Carbon object
            //print_r($day->isWednesday()); exit;

            if ($formulaEarn->ERN_SUNDAY == 1 && $day->isSunday()) {
                //echo PHP_EOL.'Its Sunday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_MONDAY == 1 && $day->isMonday()) {
                //echo PHP_EOL.'Its Monday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_TUESDAY == 1 && $day->isTuesday()) {
                //echo PHP_EOL.'Its Tuesday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_WEDNESDAY == 1 && $day->isWednesday()) {
                //echo PHP_EOL.'Its Wednesday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_THURSDAY == 1 && $day->isThursday()) {
                //echo PHP_EOL.'Its Thursday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_FRIDAY == 1 && $day->isFriday()) {
                //echo PHP_EOL.'Its Friday'.PHP_EOL;
                return true;
            } elseif ($formulaEarn->ERN_SATURDAY == 1 && $day->isSaturday()) {
                //echo PHP_EOL.'Its Saturday'.PHP_EOL;
                return true;
            } else {
                //echo PHP_EOL.'Its Not Avaible Day'.PHP_EOL;
                return false;
            }
        }
        return false;
    }

    public function _checkBonusPayment($row, $testing_payment_method) {

        $pf = array();
        $point = 0;
        // $split = 0;
        $last_poin = 0;
        $bonus_type = [
            0 => 'Payment'
        ];

        $multiple = 0;

        foreach ($bonus_type as $key_bonus_type => $val_bonus_type) {

            $formula = FormulaEarn::where('ERN_FORMULA', 1)
                    ->where('ERN_FORMULA_DTL', 0)
                    ->where('ERN_ACTIVE', 1)
                    ->where('ERN_FRDATE', '<=', $row['RCPDATE'])
                    ->where('ERN_TODATE', '>=', $row['RCPDATE']);

            $formula = $formula->where('ERN_FORMULA_TYPE', $key_bonus_type);
            $formula_load = $formula->get();

            if (!empty($formula_load)) {

                foreach ($formula_load as $formula_ress) {
                    //$point = 0;
                    $check_time = true;

                    if ($row['RCPDATE'] == $formula_ress->ERN_FRDATE or $row['RCPDATE'] == $formula_ress->ERN_TODATE) { //jika tanggal masuk di batas awal dan batas akhir
                        $formula = $formula->where('ERN_FRTIME', '<=', $row['RCPTIME']) // from datetime
                                ->where('ERN_TOTIME', '>=', $row['RCPTIME'])
                                ->where('ERN_RECID', $formula_ress->ERN_RECID);

                        $check_time = (!empty($formula > first())) ? true : false;
                    }

                    if ($key_bonus_type == 0 and $check_time == true) { // jika payment
                        $payment_method = PaymentMethod::where('code', $testing_payment_method)->first();
                        $formulaItem = FormulaEarnPayment::where('PAY_ERN_RECID', $formula_ress->ERN_RECID)->where('PAY_LOK_RECID', $payment_method['id'])->get();
                    }

                    if (count($formulaItem) > 0) {

                        $multiple = $formula_ress->ERN_MULTIPLE;

                        if ($formula_ress->ERN_VALUE == 0) { // By Amount
                            $pointamount = floor(@($row['TOTALAFTERDISC'] / $formula_ress->ERN_AMOUNT));
                            $point = $pointamount * $formula_ress->ERN_POINT;
                        }

                        if (isset($formula_ress->ERN_MULTIPLE)) {
                            if ($formula_ress->ERN_MULTIPLE == 1) {
                                if ($formula_ress->ERN_MULTIPLE_VALUE == 0) {
                                    $formula_ress->ERN_MULTIPLE_VALUE = 1;
                                }

                                $point = $point * $formula_ress->ERN_MULTIPLE_VALUE; // Multiple
                            } elseif ($formula_ress->ERN_MULTIPLE == 0) {

                                if (isset($pointamount)) {
                                    $point = @($point / $pointamount); // Fix Point
                                }
                            }
                        }
                    } else {
                        $point = 0;
                    }


                    $customer = Customer::where('CUST_BARCODE', $row['CARDID'])->first();

                    if (isset($customer->CUST_GENDER)) {
                        switch ($customer->CUST_GENDER) {
                            case 'M':
                                $customer->CUST_GENDER = 'B171851A-3E84-47DD-B6DA-0AB84C3EE697';
                                break;
                            case 'F':
                                $customer->CUST_GENDER = 'E0938D53-D62A-4708-9BA3-07E6C6E5ED29';
                                break;
                        } // untuk antisipasi kalo data Customer masih ada yang belum merefer ke z_lookup Gender nya
                    }

                    $formulaGender = FormulaEarnGender::where('GEN_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('GEN_LOK_RECID', '=', $customer->CUST_GENDER)->get();
                    $formulaReligion = FormulaEarnReligion::where('RELI_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('RELI_LOK_RECID', '=', $customer->CUST_RELIGION)->get();
                    $formulaBlood = FormulaEarnBlood::where('BLOD_ERN_RECID', '=', $formula_ress->ERN_RECID)->where('BLOD_LOK_RECID', '=', $customer->CUST_BLOOD)->get();

                    $tenant = Tenant::where('TNT_CODE', $row['STOREID'])->first();
                    $formulaTenant = FormulaEarnTenant::where('TENT_TNT_RECID', $tenant->TNT_RECID)
                                    ->where('TENT_ERN_RECID', $formula_ress->ERN_RECID)
                                    ->where('TENT_ACTIVE', 1)->get();

                    if (count($formulaTenant) < 1) {
                        $point = 0; //memtype tidak ditemukan
                        // echo 'member type tidak ditemukan <br/>';
                    }

                    if (count($formulaGender) < 1) {
                        $point = 0;
                        //echo 'gender tidak ditemukan'; exit;
                    }
                    if (count($formulaReligion) < 1) {
                        $point = 0;
                        //  echo 'religion tidak ditemukan <br/>';
                    }
                    if (count($formulaBlood) < 1) {
                        $point = 0;
                        //  echo 'blood tidak ditemukan <br/>';
                    }
                    $pf_founded = ['id' => $formula_ress->ERN_RECID, 'point' => $point];
                    $pf[] = $pf_founded;
                    // echo $point.'<br/>';
                    $last_poin = $last_poin + $point;
                }

                if ($last_poin < 0) {
                    $last_poin = 0;
                }
            }
        }

        $return = array('point' => $last_poin, 'multiple' => $multiple, 'pf' => $pf);



        return $return;
    }

    public function getReceipt($data) {
        //return view('transaction.receipt', $data);

        $pdf = PDF::loadView('transaction.receipt', $data);
        $pdf->setPaper('a6');
        return $pdf->stream();
    }

    public function writeLog($message, $writeToLog = true) {
        echo '[' . date('d-m-Y H:i:s') . '] [Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message . "" . PHP_EOL;
        if ($writeToLog)
            Log::info('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);

        return true;
    }

    public function writeErrorLog($message, $writeToLog = true) {
        echo '[' . date('d-m-Y H:i:s') . '] : ' . $message . "" . PHP_EOL;
        if ($writeToLog)
            Log::error('[Memory Used: ' . convertByte(memory_get_usage()) . '] : ' . $message);
        session()->push('logsEarning', $message);
        return true;
    }

    private function _logsPF($post_id, $pf, $type) {

        foreach ($pf as $row) {

            $PostPF = new PostPF();
            $PostPF->POSTF_RECID = Uuid::uuid();
            $PostPF->POSTF_POST_RECID = $post_id;
            $PostPF->POSTF_ERN_RECID = $row['id'];
            $PostPF->POSTF_POINT = $row['point'];
            $PostPF->POSTF_TYPE = $type;
            $PostPF->save();
        }
    }

    private function _logsPFBonusProses($post_id, $pf, $type, $point) {

        foreach ($pf as $row) {

            $PostPF = new PostPF();
            $PostPF->POSTF_RECID = Uuid::uuid();
            $PostPF->POSTF_POST_RECID = $post_id;
            $PostPF->POSTF_ERN_RECID = $row['id'];
            $PostPF->POSTF_POINT = $row['point'];
            $PostPF->POSTF_TYPE = $type;
            $PostPF->save();
        }
    }

    private function _logsPFBonus($postBonus, $postPayment, $post_id) {


        $PostPF = PostPF::whereIn('POSTF_POST_RECID', $postBonus);
        $PostPF->update(['POSTF_POST_RECID' => $post_id]);

        $PostPFPay = PostPF::whereIn('POSTF_POST_RECID', $postPayment);
        $PostPFPay->update(['POSTF_POST_RECID' => $post_id]);
    }

}

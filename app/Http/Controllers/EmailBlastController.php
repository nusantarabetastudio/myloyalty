<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CustomerGroup;
use App\Http\Requests;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Session;

class EmailBlastController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;

    public function __construct(Session $session)
    {
        $this->parent = 'Customer';
        $this->parent_link = '';
    }

    public function index()
    {
        $data['breadcrumbs'] = 'Email Blast';
        $data['parent_link'] = $this->parent_link;
        $data['parent'] = $this->parent;
        $data['customerGroups'] = CustomerGroup::orderby('id', 'ASC')->get();
        return view('email-blast.index', $data);
    }

    public function store(Request $request)
    {
        if ($request->input('plaint_text_email')){
            $plainText = $request->input('plaint_text_email');
        }
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'blast/email/' . $request->input('cust-group');
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => [
                'subject' => $request->input('subject_email'),
                'content' => $request->input('email_body'),
                'plainText' => $plainText
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if($response->getStatusCode() == 200){
            $msg = $contents->MSG;
            return redirect()->back()->with('notif_success', $msg);
        }else if($response->getStatusCode() == 422 || $response->getStatusCode() == 404){
            $msg = $contents->MSG;
            return redirect()->back()->with('notif_error', $msg);          
        } else {
            $msg = isset($contents->MSG) ? $contents->MSG : 'ERROR OCCURED!';

            return redirect()->back()->with('notif_error', $msg);  
        }
    }
}

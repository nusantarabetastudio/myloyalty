<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\CustomerGroup;
use App\Models\CustomerGroupSetting;
use App\Models\Master\CardType;
use App\Models\Master\Lookup;
use App\Models\Master\Tenant;
use Faker\Provider\Uuid;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Session, Datatables;


class CustomerGroupController extends Controller
{

    private $parent;
    private $parent_link;

    public function __construct() {
        $this->parent = 'Customer Group';
        $this->parent_link = route('customer.customer-group.index');
    }

    public function index()
    {
        $this->parent = 'Authorization';
        $data['breadcrumbs'] = 'Customer Group';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;

         return view('customer.customer-group.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['breadcrumbs'] = 'New Customer Group';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['memberTypes'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['stores'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['genders'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['maritals'] = Lookup::where('LOK_CODE', 'MART')->get();
        $data['nationalities'] = Lookup::where('LOK_CODE', 'NATL')->get();
        $data['religions'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['bloods'] = Lookup::where('LOK_CODE', 'BLOD')->get();
        $data['cardTypes'] = CardType::orderby('CARD_RECID', 'ASC')->get();

       return view('customer.customer-group.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $CustomerGroup = new CustomerGroup();
        $CustomerGroup->id = Uuid::uuid();
        $CustomerGroup->name = $request->input('form_Name');
        $CustomerGroup->save();

        $masters = $request->input('master');
        foreach ($masters as $masterTable => $masterRecords) {
            foreach ($masterRecords as $record) {
                $CustomerGroupSetting = new CustomerGroupSetting();
                $CustomerGroupSetting->id = Uuid::uuid();
                $CustomerGroupSetting->customer_group_id = $CustomerGroup->id;
                $CustomerGroupSetting->type = $masterTable;
                $CustomerGroupSetting->operator = '=';
                $CustomerGroupSetting->value = $record;
                $CustomerGroupSetting->save();
            }
        }
        $setting = $request->input('setting');
        foreach($setting as $row) {
            if (empty($row['type'])) { continue; }
            $CustomerGroupSetting = new CustomerGroupSetting();
            $CustomerGroupSetting->id = Uuid::uuid();
            $CustomerGroupSetting->customer_group_id = $CustomerGroup->id;
            $CustomerGroupSetting->type = $row['type'];
            if($row['valueDate'] == null) {
                $CustomerGroupSetting->operator = $row['operator'];
                $CustomerGroupSetting->value = $row['values'];
            } else {
                $CustomerGroupSetting->operator = 'between';
                $CustomerGroupSetting->value = $row['valueDate'];
                $CustomerGroupSetting->limit_value = $row['limitValue'];
            }
            $CustomerGroupSetting->save();
        }
        return redirect()->route('customer.customer-group.index')->with('notif_success', 'Data has been saved!');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parent = $this->parent;
        $parent_link = $this->parent_link;
        $breadcrumbs = 'Update Internal Card';
        $data['memberTypes'] = Lookup::where('LOK_CODE', 'MEMT')->get();
        $data['stores'] = Tenant::where('TNT_ACTIVE', 1)->get();
        $data['genders'] = Lookup::where('LOK_CODE', 'GEN')->get();
        $data['maritals'] = Lookup::where('LOK_CODE', 'MART')->get();
        $data['nationalities'] = Lookup::where('LOK_CODE', 'NATL')->get();
        $data['religions'] = Lookup::where('LOK_CODE', 'RELI')->get();
        $data['bloods'] = Lookup::where('LOK_CODE', 'BLOD')->get();
        $data['cardTypes'] = CardType::orderby('CARD_RECID', 'ASC')->get();
        $data['customerGroups'] = CustomerGroup::findOrFail($id);
        $data['customerGroupSettings'] = CustomerGroupSetting::where('customer_group_id', $id)->get();
        $data['mastercustomerGroupSettings'] = CustomerGroupSetting::where('customer_group_id', $id)->whereNotIn('type', ['CUST_STORE','CUST_MEMTYPE','CUST_MARITAL','CUST_NATIONALITY','CUST_BLOOD','CUST_GENDER','CUST_RELIGION','CUST_CARDTYPE'])->get();
        $data['types'] = [
            [
                'type' => 'CUST_CODE',
                'name' => 'Code',
                'dataType' => 'int'
            ],
            [
                'type' => 'CUST_DOB',
                'name' => 'DOB',
                'dataType' => 'date'
            ],
            [
                'type' => 'CUST_JOINDATE',
                'name' => 'Join Date',
                'dataType' => 'date'
            ],
            [
                'type' => 'CUST_EXPIREDATE',
                'name' => 'Expired Date',
                'dataType' => 'date'
            ],
            [
                'type' => 'CUST_EXPENSE_EARN',
                'name' => 'Expense Earning',
                'dataType' => 'int'
            ]
        ];

        $data['operators'] = [
            [
                'type' => '=',
                'name' => 'Equals (=)'
            ],
            [
                'type' => '!=',
                'name' => 'Does not equal (!=)'
            ],
            [
                'type' => '>',
                'name' => 'Greater than (>)'
            ],
            [
                'type' => '<',
                'name' => 'Less than (<)'
            ],
            [
                'type' => '<=',
                'name' => 'Less than or equal to (<=)'
            ],
            [
                'type' => 'between',
                'name' => 'Between'
            ],
            [
                'type' => 'notbetween',
                'name' => 'Not between'
            ]
        ];
        return view('customer.customer-group.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $CustomerGroup = CustomerGroup::findOrFail($id);
        $CustomerGroup->name = $request->input('form_Name');
        $CustomerGroup->save();
        $CustomerGroupSetting = CustomerGroupSetting::where('customer_group_id', $id)->delete();
        $masters = $request->input('master');
        foreach ($masters as $masterTable => $masterRecords) {
            foreach ($masterRecords as $record) {
                $CustomerGroupSetting = new CustomerGroupSetting();
                $CustomerGroupSetting->id = Uuid::uuid();
                $CustomerGroupSetting->customer_group_id = $CustomerGroup->id;
                $CustomerGroupSetting->type = $masterTable;
                $CustomerGroupSetting->operator = '=';
                $CustomerGroupSetting->value = $record;
                $CustomerGroupSetting->save();
            }
        }

        $setting = $request->input('setting');
        foreach($setting as $row) {
            $CustomerGroupSetting = new CustomerGroupSetting();
            $CustomerGroupSetting->id = Uuid::uuid();
            $CustomerGroupSetting->customer_group_id = $CustomerGroup->id;
            $CustomerGroupSetting->type = $row['type'];
            if($row['operator'] !== 'between') {
                $CustomerGroupSetting->operator = $row['operator'];
                $CustomerGroupSetting->value = $row['values'];      
            } else {
                $CustomerGroupSetting->operator = 'between';
                $CustomerGroupSetting->value = $row['valueDate'];
                $CustomerGroupSetting->limit_value = $row['limitValue'];                
            }
            $CustomerGroupSetting->save();
        }
        return redirect()->route('customer.customer-group.index')->with('notif_success', 'Data has been saved!');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = CustomerGroup::find($request->input('id'));
        $data->customerGroupSettings()->delete();
        $data->delete();

        return redirect()->route('customer.customer-group.index')->with('notif_success', 'Data has been deleted!');
    }

    public function getData() {
        $CustomerGroup = CustomerGroup::orderby('id', 'ASC')->get();
        return Datatables::of($CustomerGroup)
            ->addColumn('action', function($CustomerGroup) {
                $edit = '<a href="' .route('customer.customer-group.edit', $CustomerGroup->id). '" style="text-decoration:none"> <button type="button" class="btn btn-primary btn-xs">Edit</button></a>';
                 $delete = '<button type="button" class="btn btn-danger btn-xs" data-id="' . $CustomerGroup->id . '" data-toggle="modal" data-target="#delete-modal">Delete</button>';

                 return $edit. '&nbsp;' . $delete;
            })
            ->make(true);
    }

    public function getCustomerData(CustomerGroup $customerGroup, Request $request)
    {
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);
        $search = $request->get('s', '');
        $type = $request->get('type', '');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'customer/group/'.$customerGroup->id.'?page=' . $page . '&limit=' . $limit . '&s=' . $search;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() == 200) {
            return response()->json($contents);
        } else { // Error 500
            return response()->json('Error occured!');
        }
    }
}

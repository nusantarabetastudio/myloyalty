<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Admin\PostTemp;
use App\Models\Admin\Post;
use App\Models\Admin\PostTest;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function deletePostTemp()
    {
        PostTemp::getQuery()->delete();

        return redirect()->back()->with('success', 'PostTemp has been reset successfully');
    }

    public function deleteEarningPost(){
        Post::getQuery()->delete();

        return redirect()->back()->with('success', 'EarningPost has been reset successfully');
    }

    public function deleteTestingPostTemp(){
        PostTest::getQuery()->delete();

        return redirect()->back()->with('success', 'Testing EarningPost has been reset successfully');
    }

    public function changePassword()
    {
        if(!Session::has('user')) {
            return redirect('login');
        }
         return view('change-password');
    }


    /**
     * Change Password POST
     *
     * @param $request
     * @param $username
     * @return Redirect
     */
    public function changePasswordUpdate(Request $request, $username)
    {
        $this->validate($request, [
            'oldPassword' => 'required',
            'newPassword' => 'required'
        ]);
        if(!Session::has('user')){
            return redirect('login');
        }
        $proptype = Session::get('user');

        $data = $request->except('_token');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/user/'. $username .'/change-password';
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('change-password');

        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            $request->session()->flash('error', 'Old Password Incorrect!');
            return redirect('change-password');
        }
        else {
            return redirect('login');
        }
    }
    /**
     * Edit Profile
     *
     * @return Redirect
     */

    public function profile() {

        $proptype = Session::get('user');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/user/' . $proptype->USER_USERNAME;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['user'] = $contents->DATA->USER_DATA;

            return view('profile', $data);

        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect('login');
        }
        else {
            return redirect('login');
        }

    }

    public function updateProfile(Request $request) {

        $proptype = Session::get('user');
        $data = $request->except('_token');
        $uri = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'security/proptype/'. $proptype->USER_PRT_RECID .'/user/' . $proptype->USER_USERNAME;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', $uri, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ],
            'form_params' => $data
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $msg = $contents->MSG;
            $request->session()->flash('msg', $msg);

            return redirect('profile');

        } elseif($response->getStatusCode() == 422 || $response->getStatusCode() == 404) {
            return redirect('login');
        }
        else {
            return redirect('login');
        }
    }
}

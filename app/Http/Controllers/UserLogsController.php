<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client;
use App\Models\Preference;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Carbon\Carbon;
use Session;

class UserLogsController extends Controller
{
    private $parent;
    private $parent_link;
    private $pnum;
    private $ptype;
    private $code;

    public function __construct()
    {
        $this->parent = 'User';
        $this->parent_link = '';
        $this->admin = Preference::where('key', 'admin')->first()->value;
        $this->superAdmin = Preference::where('key' , 'superadmin')->first()->value;
        // $this->middleware('user-log', ['only' => ['index', 'create', 'edit']]);
    }

    public function index()
    {
        $data['roleId'] = Session::get('role_data')->ROLE_RECID;
        $userName = Session::get('data')->USER_DATA->USER_USERNAME;
        $data['breadcrumbs'] = 'User Logs';
        $data['parent'] = $this->parent;
        $data['parent_link'] = $this->parent_link;
        $data['admin'] = $this->admin;
        $data['superAdmin'] = $this->superAdmin;
        if($data['roleId'] == $this->admin || $data['roleId'] == $this->superAdmin) {
            $data['uri'] = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'user/logs?limit=10&page=1&s=';
        }
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $data['uri'], [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        if($response->getStatusCode() == 200) {
            $contents = json_decode($response->getBody()->getContents());
            $data['userLogs'] = $contents->DATA;
            $data['paginator'] = $contents->paginator;
			 return view('user-logs.index', $data);
        }
    }
    public function getUserLogData(Request $request)
    {
        $userName = Session::get('data')->USER_DATA->USER_USERNAME;
        $limit = $request->get('limit', 10);
        $page = $request->get('page', 1);
        $search = $request->get('s', '');
        $type = $request->get('type', '');
        $userLogUrl = env('API_URL', 'http://apiv1.pendhapa.com/api/v1/') . 'user/logs?limit=' . $limit . '&page=' . $page . '&s=' . $search;
        $client = new Client(['http_errors' => false]);
        $response = $client->request('GET', $userLogUrl, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . env('API_ACCESS_TOKEN')
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());

        if ($response->getStatusCode() == 200) {
            return response()->json($contents);
        } else { // Error 500
            return response()->json('Error occured!');
        }
    }
}
?>
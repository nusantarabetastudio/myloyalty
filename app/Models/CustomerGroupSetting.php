<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerGroupSetting extends Model
{
    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;
}
?>
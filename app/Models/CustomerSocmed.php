<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerSocmed extends Model
{
    protected $table = 'Customers_Socmed';

    protected $primaryKey = 'SMED_RECID';

    public $timestamps = false;

    protected $guarded = [];
}

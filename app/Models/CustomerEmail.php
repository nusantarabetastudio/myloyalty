<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerEmail extends Model
{
    protected $table = 'Customers_Email';

    protected $primaryKey = 'EML_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'EML_CUST_RECID', 'CUST_RECID');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardExchange extends Model
{
    protected $table = 'Customer_Exchange';

    protected $primaryKey = 'EXC_RECID';

    protected $guarded = [];

    public $dates = ['EXC_DATE'];

    public $timestamps = false;

    public $incrementing = false;
}

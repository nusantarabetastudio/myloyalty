<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPoint extends Model
{
    protected $guarded = [];

    public $incrementing = false;

    public $timestamps = false;

    public function customer() {
        return $this->belongsTo(Customer::class, 'id', 'CUST_RECID');
    }

    public function scopeLuckyDrawPoint($qry)
    {
        return $qry->where('code', 1);
    }

    public function scopeRedeemPoint($qry)
    {
        return $qry->where('code', 2);
    }
}

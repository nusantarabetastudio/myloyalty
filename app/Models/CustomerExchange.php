<?php

namespace App\Models;

use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class CustomerExchange extends Model
{
    protected $table = 'Customer_Exchange';

    protected $primaryKey = 'EXC_RECID';
    protected $dates = ['EXC_DATE'];

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'EXC_CUST_RECID', 'CUST_RECID');
    }

    public function store()
    {
        return $this->belongsTo(Tenant::class, 'EXC_TNT_RECID', 'TNT_RECID');
    }
}

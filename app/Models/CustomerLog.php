<?php

namespace App\Models;

use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class CustomerLog extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function store()
    {
        return $this->belongsTo(Tenant::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPicture extends Model
{
    protected $table = 'Customer_Picture';

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];
}

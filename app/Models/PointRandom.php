<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointRandom extends Model
{
    protected $table = 'PointRandom';

    protected $primaryKey = 'RAND_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];
}

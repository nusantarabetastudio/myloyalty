<?php

namespace App\Models;

use App\Models\Admin\Post;
use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'Customers';

    protected $primaryKey = 'CUST_RECID';

    protected $dates = ['CUST_DOB', 'CUST_JOINDATE', 'CUST_SYSTEMDATE', 'CUST_EXPIREDATE', 'CUST_EXTENDATE'];

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public function idType() {
        return $this->hasOne(Lookup::class, 'LOK_RECID', 'CUST_IDTYPE');
    }

    public function gender() {
        return $this->hasOne(Lookup::class, 'LOK_RECID', 'CUST_GENDER');
    }

    public function exchanges() {
        return $this->hasMany('App\Models\CustomerExchange', 'EXC_CUST_RECID', 'CUST_RECID');
    }

    public function exchangesType() {
         return $this->hasOne('App\Models\CustomerExchange', 'EXC_CUST_RECID', 'CUST_RECID');
    }

    public function email() {
        return $this->hasOne('App\Models\CustomerEmail', 'EML_CUST_RECID', 'CUST_RECID');
    }

    public function primaryEmail() {
        return $this->hasOne('App\Models\CustomerEmail', 'EML_CUST_RECID', 'CUST_RECID')->where('EML_TYPE', 1);
    }

    public function logs() {
        return $this->hasMany('App\Models\CustomerLog', 'customer_id', 'CUST_RECID');
    }

    public function emails() {
        return $this->hasMany('App\Models\CustomerEmail', 'EML_CUST_RECID', 'CUST_RECID');
    }

    public function addresses() {
        return $this->hasMany('App\Models\CustomerAddr', 'ADDR_CUST_RECID', 'CUST_RECID');
    }

    public function phones() {
        return $this->hasMany('App\Models\CustomerPhone', 'PHN_CUST_RECID', 'CUST_RECID');
    }

    public function mobilePhone() {
        return $this->hasOne('App\Models\CustomerPhone', 'PHN_CUST_RECID', 'CUST_RECID')->where('PHN_TYPE', 1);
    }

    public function homePhone() {
        return $this->hasOne('App\Models\CustomerPhone', 'PHN_CUST_RECID', 'CUST_RECID')->where('PHN_TYPE', 7);
    }

    public function businessPhone() {
        return $this->hasOne('App\Models\CustomerPhone', 'PHN_CUST_RECID', 'CUST_RECID')->where('PHN_TYPE', 4);
    }

    public function companyPhone() {
        return $this->hasOne('App\Models\CustomerPhone', 'PHN_CUST_RECID', 'CUST_RECID')->where('PHN_TYPE', 10);
    }

    public function facsimilePhone() {
        return $this->hasOne('App\Models\CustomerPhone', 'PHN_CUST_RECID', 'CUST_RECID')->where('PHN_TYPE', 13);
    }

    public function children() {
        return $this->hasMany('App\Models\CustomerChild', 'CHL_CUST_RECID', 'CUST_RECID');
    }

    public function picture() {
        return $this->hasOne('App\Models\CustomerPicture', 'CUST_RECID', 'CUST_RECID');
    }

    public function interests() {
        return $this->hasMany('App\Models\CustomerInterest', 'INT_CUST_RECID', 'CUST_RECID');
    }

    public function points(){
        return $this->hasMany('App\Models\CustomerPoint', 'customer_id', 'CUST_RECID');
    }

    public function rdPoint(){
        return $this->hasOne('App\Models\CustomerPoint', 'customer_id', 'CUST_RECID')->where('code', 2);
    }

    public function addressKtp() {
        return $this->hasMany('App\Models\CustomerAddr', 'ADDR_CUST_RECID', 'CUST_RECID')->where('ADDR_TYPE', 1);
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Master\Tenant', 'CUST_STORE', 'TNT_RECID');
    }

    public function addressNow() {
        return $this->hasMany('App\Models\CustomerAddr', 'ADDR_CUST_RECID', 'CUST_RECID')->where('ADDR_TYPE', 2);
    }

    public function redeemTransactions(){
        return $this->hasMany('App\Models\Admin\RedeemPost', 'customer_id', 'CUST_RECID');
    }

    public function post(){
        return $this->hasMany('App\Models\Admin\Post', 'POS_BARCODE', 'CUST_BARCODE')->orderBy('POS_POST_DATE', 'DESC');
    }

    public function getRedeemPointAttribute()
    {
        return ($this->points()->redeemPoint()->first()) ? $this->points()->redeemPoint()->first()->point : 0;
    }

    public function getLuckyDrawPointAttribute()
    {
        return ($this->points()->luckyDrawPoint()->first()) ? $this->points()->luckyDrawPoint()->first()->point : 0;
    }

    public function pointExpired() {
        return $this->belongsToMany('App\Models\Admin\PointExpired','POST_PE','PE_RECID','CUST_RECID');
    }
}

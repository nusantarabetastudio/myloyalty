<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    protected $table = 'Z_Proptype';

    protected $primaryKey = 'PRT_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    public function property(){
        return $this->belongsTo(Property::class, 'PRT_PRO_RECID', 'PRO_RECID');
    }
}

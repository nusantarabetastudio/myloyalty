<?php

namespace App\Models\Master;

use App\Models\Master\Menu;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
	use SoftDeletes;

	CONST DELETED_AT = 'DELETED_AT';

    protected $with = [];

    protected $table = 'Z_Role';

    protected $primaryKey = 'ROLE_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $hidden = ['DELETED_AT'];
}

<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    protected $table = 'Z_Tenant';

    protected $primaryKey = 'TNT_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    
    public function formulaErnTenant() {
        return $this->hasMany('App\Models\Admin\FormulaEarnTenant', 'TENT_TNT_RECID', 'TNT_RECID');
    }
    
    public function post() {
        return $this->hasMany('App\Models\Admin\Post', 'POS_STORE', 'TNT_CODE');
    }
    public function postTemp() {
        return $this->hasMany('App\Models\Admin\PostTemp', 'POS_STORE', 'TNT_CODE');
    }
}

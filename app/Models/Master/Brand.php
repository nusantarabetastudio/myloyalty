<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    
     protected $primaryKey = 'id';
    
    public $timestamps = false;

    public $incrementing = false;
    


    public function product() {
        return $this->hasMany('App\Models\Master\Product', 'brand_id', $this->primaryKey);
    }
    
}

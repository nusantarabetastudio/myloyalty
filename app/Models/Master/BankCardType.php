<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class BankCardType extends Model
{
    public $timestamps = false;

    public $incrementing = false;

}
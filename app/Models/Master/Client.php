<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'Z_Client';

    protected $primaryKey = 'CLN_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

}

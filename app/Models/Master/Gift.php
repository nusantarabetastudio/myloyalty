<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    protected $table = 'Z_Gift';

    protected $primaryKey = 'GIFT_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    protected $dates = ['GIFT_FROM', 'GIFT_TO'];

    protected $with = ['unit'];

    public function unit() {
        return $this->belongsTo(Lookup::class, 'GIFT_UNIT', 'LOK_RECID');
    }

    public function category() {
        return $this->belongsTo(Lookup::class, 'GIFT_CATEGORY', 'LOK_RECID');
    }

    public function tenants(){
        return $this->hasMany(GiftTenant::class, 'GIFT_GIFT_RECID', $this->primaryKey);
    }
}

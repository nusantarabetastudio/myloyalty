<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use SoftDeletes;

    protected $table = 'Z_Vendors';

    protected $primaryKey = 'VDR_RECID';
        
    protected $guarded = [];

    public $incrementing = false;

    public $timestamps = false;

    public function products(){
        return $this->belongsToMany(Product::class, 'Product_Vendor', 'PROV_VDR_RECID', 'PROV_PRO_RECID');
    }

    public function city(){
        return $this->belongsTo(Lookup::class, 'VDR_CITY', 'LOK_RECID');
    }
}

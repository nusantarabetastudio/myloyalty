<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class GiftTenant extends Model
{
    protected $table = 'Z_Gift_Tenant';

    protected $primaryKey = 'GIFT_CODE';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    public function gift() {
        return $this->belongsTo(Gift::class, 'GIFT_GIFT_RECID', 'GIFT_CODE');
    }

    public function tenant() {
        return $this->belongsTo(Gift::class, 'GIFT_TENANT', 'GIFT_CODE');
    }
}

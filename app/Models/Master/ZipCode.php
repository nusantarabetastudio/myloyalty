<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
    protected $table = 'Z_ZipCode';

    protected $primaryKey = 'ZIP_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    public function lookup() {
        return $this->belongsTo(Lookup::class, 'ZIP_LOOK_RECID', 'ZIP_RECID');
    }
}

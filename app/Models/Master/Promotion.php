<?php

namespace App\Models\Master;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'Z_Promotion';

    protected $primaryKey = 'PRM_RECID';

    protected $dates = ['PRM_FRDATE', 'PRM_TODATE'];

    protected $guarded = [];

    public $incrementing = false;

    public $timestamps = false;

    public function setPRMFRDATEAttribute($value)
    {
        $this->attributes['PRM_FRDATE'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function setPRMTODATEAttribute($value)
    {
        $this->attributes['PRM_TODATE'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    public function tenant() {
        return $this->belongsTo(Tenant::class, 'PRM_TENANT', 'TNT_RECID');
    }

    public function payment() {
        return $this->belongsTo(Lookup::class, 'PRM_PAYMENT', 'LOK_RECID');
    }

}
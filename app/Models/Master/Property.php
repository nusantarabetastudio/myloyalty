<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'Z_Property';

    protected $primaryKey = 'PRO_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    public function client(){
        return $this->belongsTo(Client::class, 'PRO_CN_RECID', 'PRO_RECID');
    }

    public function city(){
        return $this->belongsTo(Lookup::class, 'PRO_CITY', 'LOK_RECID');
    }

    public function country(){
        return $this->belongsTo(Lookup::class, 'PRO_COUNTRY', 'LOK_RECID');
    }

}

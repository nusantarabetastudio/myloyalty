<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProductVendor extends Model
{
    protected $table = 'Product_Vendor';

    protected $primaryKey = 'PROV_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    public function vendor(){
        return $this->belongsTo(Vendor::class, 'PROV_VDR_RECID', 'VDR_RECID');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'PROV_PRO_RECID', 'PRO_RECID');
    }

}

<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
    protected $table = 'Z_CardType';

    protected $primaryKey = 'CARD_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public function getCARDIMAGEAttribute($val){
        if($val !== null){
            return env('URL_CARDTYPE_UPLOADS') . $val;
        }
        return $val;
    }

    public function parent() {
        return $this->belongsTo(CardType::class, 'CARD_GROUP', 'CARD_RECID');
    }
}

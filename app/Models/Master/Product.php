<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'Z_Product_Item';

    protected $primaryKey = 'PRO_RECID';

    protected $guarded = [];

    //protected $with = ['supplier','vendors','brand'];

    public $incrementing = false;

    public $timestamps = false;


    public function merchandise()
    {
        return $this->belongsTo(Merchandise::class, 'PRO_MERCHANDISE', 'MERC_CODE');
    }

    public function unit()
    {
        return $this->belongsTo(Lookup::class, 'PRO_UNIT', 'LOK_RECID');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    public function vendors()
    {
         return $this->belongsToMany(Vendor::class, 'Product_Vendor', 'PROV_PRO_RECID', 'PROV_VDR_RECID');
    }

    public function supplier()
    {
        return $this->hasOne(ProductVendor::class, 'PROV_PRO_RECID', $this->primaryKey);
    }


}

<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'Z_Event';

    protected $primaryKey = 'EVT_RECID';

    protected $guarded = [];

    protected $dates = ['EVT_FRDATE', 'EVT_TODATE'];

    public $timestamps = false;

    public $incrementing = false;

    public function getEVTIMAGE1Attribute($val){
        if($val !== null){
            return env('URL_EVENT_UPLOADS') . $val;
        }
        return $val;
    }

    public function getEVTIMAGE2Attribute($val){
        if($val !== null){
            return env('URL_EVENT_UPLOADS') . $val;
        }
        return $val;
    }

    public function getEVTIMAGE3Attribute($val){
        if($val !== null){
            return env('URL_EVENT_UPLOADS') . $val;
        }
        return $val;
    }

    public function eventCategory(){
        return $this->belongsTo(Merchandise::class, 'EVT_CATEGORY', 'MERC_RECID');
    }
}

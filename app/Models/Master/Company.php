<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'Z_Company';

    protected $primaryKey = 'COMP_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    public function customer() {
        return $this->belongsTo('App\Models\Customer', 'COMP_CUST_RECID', 'CUST_RECID');
    }

    public function city() {
        return $this->belongsTo(Lookup::class, 'COMP_CITY', 'LOK_RECID');
    }

    public function area() {
        return $this->belongsTo(Lookup::class, 'COMP_AREA', 'LOK_RECID');
    }
}

<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lookup extends Model
{
    use SoftDeletes;

    protected $table = 'Z_Lookup';

    protected $primaryKey = 'LOK_RECID';

    protected $guarded = [];

    public $incrementing = false;

    public $timestamps = false;

    public function subCategory() {
    	return $this->hasOne(Lookup::class, 'LOK_MAPPING', 'LOK_MAPPING_PARENT')->where('LOK_CODE', 'SUBC');
    }
    public function category() {
    	return $this->hasOne(Lookup::class, 'LOK_MAPPING', 'LOK_MAPPING_PARENT')->where('LOK_CODE', 'CATG');
    }
    public function division() {
    	return $this->hasOne(Lookup::class, 'LOK_MAPPING', 'LOK_MAPPING_PARENT')->where('LOK_CODE', 'DIVI');
    }
    public function department() {
    	return $this->hasOne(Lookup::class, 'LOK_MAPPING', 'LOK_MAPPING_PARENT')->where('LOK_CODE', 'DEPT');
    }
}

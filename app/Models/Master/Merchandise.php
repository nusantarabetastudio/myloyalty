<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Merchandise extends Model
{
    protected $table = 'Z_Merchandise';

    protected $primaryKey = 'MERC_RECID';

    protected $guarded = [];

    public $incrementing = false;

    public $timestamps = false;

    public function department() {
        return $this->belongsTo(Lookup::class, 'MERC_DEPARTMENT', 'LOK_RECID');
    }

    public function division() {
        return $this->belongsTo(Lookup::class, 'MERC_DIVISION', 'LOK_RECID');
    }

    public function category() {
        return $this->belongsTo(Lookup::class, 'MERC_CATEGORY', 'LOK_RECID');
    }

    public function subcategory() {
        return $this->belongsTo(Lookup::class, 'MERC_SUBCATEGORY', 'LOK_RECID');
    }

    public function segment() {
        return $this->belongsTo(Lookup::class, 'MERC_SEGMENT', 'LOK_RECID');
    }

}

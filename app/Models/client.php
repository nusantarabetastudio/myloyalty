<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    protected $table = 'Z_Client';

    protected $primaryKey = 'CLN_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];
}
?>
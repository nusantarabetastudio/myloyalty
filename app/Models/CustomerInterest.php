<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerInterest extends Model
{
    protected $table = 'Customer_Interest';

    protected $primaryKey = 'INT_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

}

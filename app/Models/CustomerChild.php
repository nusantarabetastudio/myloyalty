<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerChild extends Model
{
    protected $table = 'Customers_Child';

    protected $primaryKey = 'CHL_RECID';

    public $timestamps = false;

    protected $guarded = [];
}

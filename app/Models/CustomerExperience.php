<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerExperience extends Model
{
    protected $table = 'Customer_Experience';

    protected $primaryKey = 'EXP_RECID';

    protected $dates = ['EXP_DATE', 'EXP_DATE_EDIT'];

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

}

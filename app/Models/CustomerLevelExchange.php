<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerLevelExchange extends Model
{
    protected $table = 'Customer_Levelexchange';

    protected $primaryKey = 'EXL_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];
}

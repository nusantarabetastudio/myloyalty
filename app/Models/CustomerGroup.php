<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    public $incrementing = false;
    
    public function customerGroupSettings() {
        return $this->hasMany('App\Models\CustomerGroupSetting');
    }
}
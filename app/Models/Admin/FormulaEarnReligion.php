<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnReligion extends Model
{
    protected $table = 'FormulaEarn_Religion';

    protected $primaryKey = 'RELI_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;
    

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'RELI_ERN_RECID', 'ERN_RECID');
    }

    public function religion() {
        return $this->belongsTo('App\Models\Master\Lookup', 'RELI_LOK_RECID', 'LOK_RECID');
    }
    
}

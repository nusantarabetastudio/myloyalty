<?php

namespace App\Models\Admin;

use App\Models\Master\Brand;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnBrand extends Model
{
    protected $table = 'FormulaEarn_Brand';

    protected $primaryKey = 'BRAND_RECID';

    protected $guarded = [];

    public $incrementing = false;

    public $timestamps = false;

    protected $with = ['formula','brand'];

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'BRAND_ERN_RECID', 'ERN_RECID');
    }

    public function brand() {
        return $this->belongsTo(Brand::class, 'BRAND_BRAND_RECID', 'id');
    }
}

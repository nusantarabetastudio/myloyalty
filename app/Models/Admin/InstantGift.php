<?php

namespace App\Models\Admin;

use App\Models\Customer;
use App\Models\Master\GiftTenant;
use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class InstantGift extends Model
{
    protected $table = 'Instant_Gift';

    protected $primaryKey = 'GIFT_RECID';

    protected $guarded = [];

    protected $dates = ['GIFT_DATE', 'GIFT_DATE_PICKUP'];

    public $incrementing = false;

    public $timestamps = false;

    public function customer(){
        return $this->belongsTo(Customer::class, 'GIFT_CUST_RECID', 'CUST_RECID');
    }

    public function store(){
        return $this->belongsTo(Tenant::class, 'GIFT_STORE', 'TNT_RECID');
    }

    public function giftTenant(){
        return $this->belongsTo(GiftTenant::class, 'GIFT_GIFT_RECID', 'GIFT_CODE');
    }
}

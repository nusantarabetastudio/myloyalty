<?php

namespace App\Models\Admin;


use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnDivision extends Model
{
    protected $table = 'FormulaEarn_Division';

    protected $primaryKey = 'DIV_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;

    protected $with = ['division'];

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'DIV_ERN_RECID', 'ERN_RECID');
    }

    public function division() {
        return $this->belongsTo(Lookup::class, 'DIV_DIVISION', 'LOK_RECID');
    }
    
}

<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Tenant;
use App\Models\Admin\PostTempPayment;

class PostTemp extends Model
{
    protected $table = 'POSTTEMP';

    protected $primaryKey = 'POSTTEMP_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public $dates = ['RCPDATE'];
    protected $dateFormat = 'Y-m-d';

     public function store() {
        return $this->belongsTo(Tenant::class, 'STOREID', 'TNT_CODE');
    }
    
    public function payment()  {
        return $this->belongsTo(PostTempPayment::class, 'RCPNBR', 'RCPNBR');
    }
   
}

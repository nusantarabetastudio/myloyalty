<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnRegularDays extends Model
{
    protected $table = 'FormulaEarn_RegularDays';

    protected $primaryKey = 'DAYS_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;
    

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'DAYS_ERN_RECID', 'ERN_RECID');
    }
    
}

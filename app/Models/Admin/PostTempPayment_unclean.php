<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Tenant;
use App\Models\Admin\PostTemp;

class PostTempPayment_unclean extends Model
{
    protected $table = 'POSTTEMPPAYMENT_unclean';

    protected $primaryKey = 'POSTTEMPPAYMENT_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public $dates = ['RCPDATE'];
    protected $dateFormat = 'Y-m-d';

     public function store() {
        return $this->belongsTo(Tenant::class, 'STOREID', 'TNT_CODE');
    }
    
    public function transaction() {
        return $this->hasMany(PostTemp_unclean::class, 'RCPNBR', 'RCPNBR');
    }
   
}

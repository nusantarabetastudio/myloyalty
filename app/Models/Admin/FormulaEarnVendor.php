<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnVendor extends Model
{
    protected $table = 'FormulaEarn_Vendor';
    protected $primaryKey = 'VND_RECID';
    protected $guarded = [];
    protected $dates = [];
    public $incrementing = false;
    public $timestamps = false;
	
	protected $with = ['formula','vendor'];


    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'VND_ERN_RECID', 'ERN_RECID');
    }

    public function vendor() {
        return $this->belongsTo('App\Models\Master\Vendor', 'VND_VENDOR_RECID', 'VDR_RECID');
    }
}

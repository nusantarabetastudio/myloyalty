<?php

namespace App\Models\Admin;

use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class ProductOut extends Model
{
    protected $table = 'Product_Out';

    protected $primaryKey = 'POUT_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public $dates = ['PIN_POST_DATE'];

    public function productStock(){
        return $this->belongsTo(productStock::class, 'POUT_PRT_RECID', 'id');
    }

    public function postDetail(){
        return $this->belongsTo(PostDetail::class, 'POUT_PSD_RECID', 'PSD_RECID');
    }

    public function card(){
        return $this->belongsTo(Lookup::class, 'POUT_CARD_RECID', 'LOK_RECID');
    }
}

<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ZExpired extends Model
{
    protected $table = 'Z_Expired';

    protected $primaryKey = 'EXPIRED_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    protected $dates = ['START_ON'];
}

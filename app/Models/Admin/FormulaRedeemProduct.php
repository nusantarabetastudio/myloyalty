<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaRedeemProduct extends Model
{
    protected $table = 'FormulaRedeem_Product';

    protected $primaryKey = 'PRDR_RECID';

    protected $guarded = [];

    public $timestamps = false;

    public $incrementing = false;

    public function formulaRedeem(){
        return $this->belongsTo(FormulaRedeem::class, 'PRDR_RDM_RECID', 'RDM_RECID');
    }
}

<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FormulaRedeemLog extends Model
{
    protected $table = 'FormulaRedeem_Log';

    protected $primaryKey = 'RLOG_RECID';

    protected $guarded = [];

    protected $dates = ['RLOG_POSTDATE'];

    public $incrementing = false;

    public $timestamps = false;

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'RLOG_ERN_RECID', 'ERN_RECID');
    }
}

<?php

namespace App\Models\Admin;

use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class FormulaRedeemPayment extends Model
{
    protected $table = 'FormulaRedeem_Payment';

    protected $primaryKey = 'PAYR_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $with = ['payment'];

    public function formulaRedeem() {
        return $this->belongsTo(FormulaRedeem::class, 'PAYR_RDM_RECID', 'RDM_RECID');
    }

    public function payment(){
        return $this->belongsTo(Lookup::class, 'PAYR_LOK_RECID', 'LOK_RECID')->where('LOK_CDE', 'PAYM');
    }

}

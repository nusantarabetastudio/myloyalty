<?php

namespace App\Models\Admin;

use App\Models\Master\Product;
use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class ProductRedeem extends Model
{
    protected $table = 'Product_Redeem';

    protected $primaryKey = 'PRR_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public $dates = ['PRR_FRDATE', 'PRRTODATE'];

    public function product() {
        return $this->belongsTo(Product::class, 'PRR_PRO_RECID', 'PRO_RECID');
    }
    public function tenant() {
        return $this->belongsTo(Tenant::class, 'PRR_TENANT', 'TNT_RECID');
    }
}

<?php

namespace App\Models\Admin;


use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnTenant extends Model
{
    protected $table = 'FormulaEarn_Tenant';

    protected $primaryKey = 'TENT_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;

    public $with = ['tenant'];

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'TENT_ERN_RECID', 'ERN_RECID');
    }

    public function tenant() {
        return $this->belongsTo(Tenant::class, 'TENT_TNT_RECID', 'TNT_RECID');
    }
 
}

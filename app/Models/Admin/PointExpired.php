<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PointExpired extends Model
{
    protected $table = 'POINTEXPIRED';

    protected $primaryKey = 'PE_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    protected $dates = ['PE_ON'];


    public function customer() {
        return $this->belongsToMany('App\Model\Customer', 'POST_PE','CUST_RECID', 'PE_RECID');
    } 

}


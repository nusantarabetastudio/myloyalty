<?php

namespace App\Models\Admin;


use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnSegment extends Model
{
    protected $table = 'FormulaEarn_Segment';

    protected $primaryKey = 'SEG_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;

    protected $with = ['segment'];

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'SEG_ERN_RECID', 'ERN_RECID');
    }

    public function segment() {
        return $this->belongsTo(Lookup::class, 'SEG_SEGMENT', 'LOK_RECID');
    }
    
}

<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnCardType extends Model
{
    protected $table = 'FormulaEarn_CardType';
    protected $primaryKey = 'CDTY_RECID';
    protected $guarded = [];
    protected $dates = [];
    public $incrementing = false;
    public $timestamps = false;
    

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'CDTY_ERN_RECID', 'ERN_RECID');
    }

    public function cardType() {
        return $this->belongsTo('App\Models\Master\CardType', 'CARD_CARD_CODE', 'CARD_RECID');
    }
    
}

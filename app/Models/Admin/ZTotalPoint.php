<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ZTotalPoint extends Model
{
    protected $guarded = [];

    protected $table = 'Z_TotalPoint';

    protected $primaryKey = 'TTL_RECID';

    public $incrementing = false;

    public $timestamps = false;

    public function customer() {
        return $this->belongsTo(Customer::class, 'CUST_RECID', 'TTL_CUST_RECID');
    }

    public function scopeRedeemPoint($qry)
    {
    	return $qry->where('TTL_CODE', 2);
    }
}

<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnDetail extends Model
{
    protected $table = 'FormulaEarn_Detail';

    protected $primaryKey = 'DTL_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;
    

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'DTL_ERN_RECID', 'ERN_RECID');
    }
    
}

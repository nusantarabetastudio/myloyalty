<?php

namespace App\Models\Admin;

use App\Models\Admin\Post;
use App\Models\Admin\FormulaEarn;
use Illuminate\Database\Eloquent\Model;

class PostPF extends Model
{
    protected $table = 'Post_PF';

    protected $primaryKey = 'POSTF_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    protected $dateFormat = 'Y-m-d';

    public function post(){
        return $this->belongsTo(Post::class, 'POSTF_POST_RECID', 'POST_RECID');
    }

    
    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'POSTF_ERN_RECID', 'ERN_RECID');
    }



}

<?php

namespace App\Models\Admin;


use App\Models\Master\PaymentMethod;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnPayment extends Model
{
    protected $table = 'FormulaEarn_Payment';

    protected $primaryKey = 'PAY_RECID';

    protected $guarded = [];

    protected $dates = [];

    protected $with = ['payment'];

    public $incrementing = false;

    public $timestamps = false;
    

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'PAY_ERN_RECID', 'ERN_RECID');
    }

    public function payment() {
        return $this->belongsTo(PaymentMethod::class, 'PAY_LOK_RECID', 'id');
    }
    
}

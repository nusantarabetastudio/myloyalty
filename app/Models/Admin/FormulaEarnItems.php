<?php

namespace App\Models\Admin;


use App\Models\Master\Product;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnItems extends Model
{
    protected $table = 'FormulaEarn_Items';

    protected $primaryKey = 'ITM_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;



    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'ITM_ERN_RECID', 'ERN_RECID');
    }

    public function item() {
        return $this->belongsTo(Product::class, 'ITM_PRODUCT', 'PRO_RECID');
    }
    
}

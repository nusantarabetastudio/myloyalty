<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class GiftOut extends Model
{
    protected $table = 'Gift_Out';

    protected $primaryKey = 'GIFT_RECID';

    protected $guarded = [];

    protected $dates = ['GIFT_POST_DATE', 'GIFT_PROCESS_DATE'];

    public $incrementing = false;

    public $timestamps = false;

    public function gift(){
        return $this->belongsTo('App\Models\Master\Gift', 'GFIN_GIFT_RECID', 'GIFT_RECID');
    }

    public function tenant(){
        return $this->belongsTo('App\Models\Master\Tenant', 'GFIN_TENANT', 'TNT_RECID');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'GIFT_CUST_RECID', 'CUST_RECID');
    }

    // GIFT_POST_RECID NOT YET.
    public function pos(){

    }
}

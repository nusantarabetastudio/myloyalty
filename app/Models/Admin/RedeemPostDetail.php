<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class RedeemPostDetail extends Model
{
    protected $guarded = [];

    public $incrementing = false;

    public $timestamps = false;

    public function redeemProduct() {
    	return $this->belongsTo(RedeemProduct::class)->withTrashed();
    }

    public function redeemPost() {
    	return $this->belongsTo(RedeemPost::class);
    }
}
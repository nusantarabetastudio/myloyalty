<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PostPE extends Model
{
    protected $table = 'POST_PE';

    protected $primaryKey = 'POS_PE_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public function customer() {
        return $this->hasMany('App\Models\Customer', 'CUST_RECID', 'CUST_RECID');
    }

    public function pointExpired() {
        return $this->hasMany('App\Models\Admin\PointExpired', 'PE_RECID', 'PE_RECID');
    }


}

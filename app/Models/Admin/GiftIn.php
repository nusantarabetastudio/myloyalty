<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class GiftIn extends Model
{
    protected $table = 'Gift_In';

    protected $primaryKey = 'GFIN_RECID';

    protected $guarded = [];

    protected $dates = ['GFIN_RECEIVE_DATE'];

    public $incrementing = false;

    public $timestamps = false;

    public function gift(){
        return $this->belongsTo('App\Models\Master\Gift', 'GFIN_GIFT_RECID', 'GIFT_RECID');
    }

    public function tenant(){
        return $this->belongsTo('App\Models\Master\Tenant', 'GFIN_TENANT', 'TNT_RECID');
    }

}

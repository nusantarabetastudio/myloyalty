<?php

namespace App\Models\Admin;

use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class FormulaRedeemTenant extends Model
{
    protected $table = 'FormulaRedeem_Tenant';

    protected $primaryKey = 'RDT_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    protected $with = ['tenant'];

    public function formulaRedeem() {
        return $this->belongsTo(FormulaRedeem::class, 'RDT_RDM_RECID', 'RDM_RECID');
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class, 'RDT_TENANT', 'TNT_RECID');
    }
}

<?php

namespace App\Models\Admin;


use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnCategory extends Model
{
    protected $table = 'FormulaEarn_Category';

    protected $primaryKey = 'CAT_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;

    protected $with = ['category'];

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'CAT_ERN_RECID', 'ERN_RECID');
    }

    public function category() {
        return $this->belongsTo(Lookup::class, 'CAT_CATEGORY', 'LOK_RECID');
    }
    
}

<?php

namespace App\Models\Admin;

use App\Models\Master\Lookup;
use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RedeemProduct extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    public $incrementing = false;
    protected $dates = ['from_date', 'to_date'];
    protected $guarded = [];
}
<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarn extends Model
{
    protected $table = 'FormulaEarn';

    protected $primaryKey = 'ERN_RECID';

    protected $guarded = [];

    protected $dates = ['ERN_FRDATE', 'ERN_TODATE'];
    


    public $incrementing = false;

    public $timestamps = false;
    

    public function scopeRegular($query)
    {
        return $query->where('ERN_FORMULA', 0);
    }

    public function scopeBonus($query){
         return $query->whereIn('ERN_FORMULA', [1,3]);
    }

    public function scopeReward($query){
        return $query->where('ERN_FORMULA', 2);
    }

    public function postPF() {
        return $this->hasMany('App\Models\Admin\PostPF', 'POSTF_POST_RECID', $this->primaryKey);
    }
    public function blood() {
        return $this->hasMany('App\Models\Admin\FormulaEarnBlood', 'BLOD_ERN_RECID', $this->primaryKey);
    }
    public function card() {
        return $this->hasMany('App\Models\Admin\FormulaEarnCard', 'CARD_ERN_RECID', $this->primaryKey);
    }
    public function cardType() {
        return $this->hasMany('App\Models\Admin\FormulaEarnCardType', 'CDTY_ERN_RECID', $this->primaryKey);
    }
    public function category() {
        return $this->hasMany('App\Models\Admin\FormulaEarnCategory', 'CAT_ERN_RECID', $this->primaryKey);
    }
    public function department() {
        return $this->hasMany('App\Models\Admin\FormulaEarnDepartment', 'DEPT_ERN_RECID', $this->primaryKey);
    }
    public function detail() {
        return $this->hasMany('App\Models\Admin\FormulaEarnDetail', 'DTL_ERN_RECID', $this->primaryKey);
    }
    public function division() {
        return $this->hasMany('App\Models\Admin\FormulaEarnDivision', 'DIV_ERN_RECID', $this->primaryKey);
    }
    public function gender() {
        return $this->hasMany('App\Models\Admin\FormulaEarnGender', 'GEN_ERN_RECID', $this->primaryKey);
    }
    public function item() {
        return $this->hasMany('App\Models\Admin\FormulaEarnItem', 'ITM_ERN_RECID', $this->primaryKey);
    }
    public function log() {
        return $this->hasMany('App\Models\Admin\FormulaEarnLog', 'LOG_ERN_RECID', $this->primaryKey);
    }
    public function member() {
        return $this->hasMany('App\Models\Admin\FormulaEarnMember', 'MEM_ERN_RECID', $this->primaryKey);
    }
    public function payment() {
        return $this->hasMany('App\Models\Admin\FormulaEarnPayment', 'PAY_ERN_RECID', $this->primaryKey);
    }
    public function regularDays() {
        return $this->hasMany('App\Models\Admin\FormulaEarnRegularDays', 'DAYS_ERN_RECID', $this->primaryKey);
    }
    public function religion() {
        return $this->hasMany('App\Models\Admin\FormulaEarnReligion', 'RELI_ERN_RECID', $this->primaryKey);
    }
    public function segment() {
        return $this->hasMany('App\Models\Admin\FormulaEarnSegment', 'SEG_ERN_RECID', $this->primaryKey);
    }
    public function subcategory() {
        return $this->hasMany('App\Models\Admin\FormulaEarnSubCategory', 'SCAT_ERN_RECID', $this->primaryKey);
    }
    public function supplier() {
        return $this->hasMany('App\Models\Admin\FormulaEarnSupplier', 'SPL_ERN_RECID', $this->primaryKey);
    }
    public function product() {
        return $this->hasMany('App\Models\Admin\FormulaEarnItems', 'ITM_ERN_RECID', $this->primaryKey);
    }
    public function tenant() {
        return $this->hasMany('App\Models\Admin\FormulaEarnTenant', 'TENT_ERN_RECID', $this->primaryKey);
    }
    public function brand() {
        return $this->hasMany('App\Models\Admin\FormulaEarnBrand', 'BRAND_ERN_RECID', $this->primaryKey);
    }
    public function vendor() {
        return $this->hasMany('App\Models\Admin\FormulaEarnVendor', 'VND_ERN_RECID', $this->primaryKey);
    }
}

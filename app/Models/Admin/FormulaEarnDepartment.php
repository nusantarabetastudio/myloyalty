<?php

namespace App\Models\Admin;


use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnDepartment extends Model
{
    protected $table = 'FormulaEarn_Department';

    protected $primaryKey = 'DEP_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;

    //protected $with = ['department']; 

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'DEP_ERN_RECID', 'ERN_RECID');
    }

    public function department() {
        return $this->belongsTo(Lookup::class, 'DEPT_DEPARTMENT', 'LOK_RECID');
    }
    
}

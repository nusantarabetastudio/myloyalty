<?php

namespace App\Models\Admin;

use App\Models\Master\Product;
use Illuminate\Database\Eloquent\Model;

class PostDetail extends Model
{
    protected $table = 'Post_Dtl';

    protected $primaryKey = 'PSD_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    protected $dates = [
        'PSD_TRAN_DATE', 'PSD_VOID_DATE'
    ];

    protected $with = ['pos', 'article'];

    protected $dateFormat = 'Y-m-d';

    public function pos() {
        return $this->belongsTo(Post::class, 'PSD_POS_RECID', 'POS_RECID');
    }

    public function article() {
        return $this->belongsTo(Product::class, 'PSD_ARTICLE', 'PRO_RECID');
    }
}

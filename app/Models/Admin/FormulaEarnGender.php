<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnGender extends Model
{
    protected $table = 'FormulaEarn_Gender';

    protected $primaryKey = 'GEN_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;
    

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'GEN_ERN_RECID', 'ERN_RECID');
    }

    public function gender() {
        return $this->belongsTo('App\Models\Master\Lookup', 'GEN_LOK_RECID', 'LOK_RECID');
    }
    
}

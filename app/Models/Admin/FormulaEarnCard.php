<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnCard extends Model
{
    protected $table = 'FormulaEarn_Card';
    protected $primaryKey = 'CARD_RECID';
    protected $guarded = [];
    protected $dates = [];
    public $incrementing = false;
    public $timestamps = false;
    
    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'CARD_ERN_RECID', 'ERN_RECID');
    }

    public function card() {
        return $this->belongsTo('App\Models\Master\Lookup', 'BLOD_CARD_CODE', 'LOK_RECID');
    }
    
}

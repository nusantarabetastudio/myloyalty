<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnMember extends Model
{
    protected $table = 'FormulaEarn_Member';

    protected $primaryKey = 'MEM_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;
    

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'MEM_ERN_RECID', 'ERN_RECID');
    }

    public function member() {
        return $this->belongsTo('App\Models\Master\Lookup', 'MEM_LOK_RECID', 'LOK_RECID');
    }
    
}

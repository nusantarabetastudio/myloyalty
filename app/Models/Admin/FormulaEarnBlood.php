<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnBlood extends Model
{
    protected $table = 'FormulaEarn_Blood';
    protected $primaryKey = 'BLOD_RECID';
    protected $guarded = [];
    protected $dates = [];
    public $incrementing = false;
    public $timestamps = false;
    
    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'BLOD_ERN_RECID', 'ERN_RECID');
    }

    public function blood() {
        return $this->belongsTo('App\Models\Master\Lookup', 'BLOD_LOK_RECID', 'LOK_RECID');
    }
}

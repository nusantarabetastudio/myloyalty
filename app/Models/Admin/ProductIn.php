<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductIn extends Model
{
    protected $table = 'ProductIn';

    protected $primaryKey = 'PIN_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public $dates = ['PIN_POST_DATE'];

    public function productTenant(){
        return $this->belongsTo(ProductTenant::class, 'PIN_PRT_RECID', 'PRT_RECID');
    }

}

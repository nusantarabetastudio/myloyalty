<?php

namespace App\Models\Admin;


use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnSubCategory extends Model
{
    protected $table = 'FormulaEarn_SubCategory';

    protected $primaryKey = 'SCAT_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;

    protected $with = ['subcategory'];

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'SCAT_ERN_RECID', 'ERN_RECID');
    }

    public function subcategory() {
        return $this->belongsTo(Lookup::class, 'SCAT_SUBCATEGORY', 'LOK_RECID');
    }
    
}

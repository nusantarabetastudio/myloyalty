<?php

namespace App\Models\Admin;

use App\Models\Customer;
use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class RedeemPost extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $dates = ['post_date'];

    public function details() {
    	return $this->hasMany(RedeemPostDetail::class);
    }

    public function customer() {
    	return $this->belongsTo(Customer::class);
    }

    public function tenant()
    {
    	return $this->belongsTo(Tenant::class);
    }
}

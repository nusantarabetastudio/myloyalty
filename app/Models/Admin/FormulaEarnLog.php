<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class FormulaEarnLog extends Model
{
    protected $table = 'FormulaEarn_Log';

    protected $primaryKey = 'LOG_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;


    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'LOG_ERN_RECID', 'ERN_RECID');
    }
    
}

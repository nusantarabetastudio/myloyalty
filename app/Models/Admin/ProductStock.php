<?php

namespace App\Models\Admin;

use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public function productRedeem() {
        return $this->belongsTo(ProductRedeem::class, 'PRT_PRR_RECID', 'PRR_RECID');
    }

    public function tenant() {
        return $this->belongsTo(Tenant::class, 'PRT_TNT_RECID', 'TNT_RECID');
    }


}

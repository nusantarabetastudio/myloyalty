<?php

namespace App\Models\Admin;

use App\Models\Customer;
use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'Post';

    protected $primaryKey = 'POS_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    protected $dates = ['POS_POST_DATE'];

    protected $with = ['store'];

    protected $dateFormat = 'Y-m-d';

    public function customer(){
        return $this->belongsTo(Customer::class, 'POS_CUST_RECID', 'CUST_RECID');
    }

    public function store(){
        return $this->belongsTo(Tenant::class, 'POS_STORE', 'TNT_RECID');
    }

    public function detail() {
        return $this->hasMany(PostDetail::class, 'PSD_POS_RECID', 'POS_RECID');
    }


    protected function setPosPostDateAttribute($value) {
        $this->attributes['POS_POST_DATE'] = $value;
    }
}

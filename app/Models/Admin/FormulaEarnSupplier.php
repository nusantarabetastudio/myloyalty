<?php

namespace App\Models\Admin;


use App\Models\Master\ProductVendor;
use Illuminate\Database\Eloquent\Model;

class FormulaEarnSupplier extends Model
{
    protected $table = 'FormulaEarn_Supplier';

    protected $primaryKey = 'SPL_RECID';

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public $timestamps = false;

    public function formula() {
        return $this->belongsTo(FormulaEarn::class, 'SPL_ERN_RECID', 'ERN_RECID');
    }

}

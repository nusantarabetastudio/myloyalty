<?php

namespace App\Models\Admin;

use App\Models\Master\Lookup;
use App\Models\Master\Merchandise;
use App\Models\Master\Product;
use App\Models\Master\Tenant;
use Illuminate\Database\Eloquent\Model;

class Postr extends Model
{
    protected $table = 'Postr';

    protected $primaryKey = 'POS_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public $dates = ['POS_DATE'];

    public function tenant() {
        return $this->belongsTo(Tenant::class, 'POS_TENANT', 'TNT_RECID');
    }

    public function category() {
        return $this->belongsTo(Merchandise::class, 'POST_CATEG', 'MERC_RECID');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'POS_PRO', 'PRO_RECID');
    }

    public function payment() {
        return $this->belongsTo(Lookup::class, 'POS_PAY', 'LOK_RECID');
    }

}

<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Tenant;
use App\Models\Admin\PostTemp;

class PostTempPayment extends Model
{
    protected $table = 'POSTTEMPPAYMENT';

    protected $primaryKey = 'POSTTEMPPAYMENT_RECID';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public $dates = ['RCPDATE'];
    protected $dateFormat = 'Y-m-d';

     public function store() {
        return $this->belongsTo(Tenant::class, 'STOREID', 'TNT_CODE');
    }
    
    public function transaction() {
        return $this->hasMany(PostTemp::class, 'RCPNBR', 'RCPNBR');
    }
   
}

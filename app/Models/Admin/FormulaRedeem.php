<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FormulaRedeem extends Model
{
    protected $table = 'FormulaRedeem';

    protected $primaryKey = 'RDM_RECID';

    protected $guarded = [];

    protected $dates = ['RDM_FRDATE', 'RDM_TODATE'];

    public $incrementing = false;

    public $timestamps = false;

    public function scopeGratis($query){
        return $query->where('RDM_FORMULA', 2);
    }

    public function scopeMurah($query){
        return $query->where('RDM_FORMULA', 1);
    }

    public function memberType(){
        return $this->belongsTo('App\Models\Master\Lookup', 'RDM_MEMBER_TYPE', 'LOK_RECID');
    }

    public function payment(){
        return $this->belongsTo('App\Models\Master\Lookup', 'RDM_PAYMENT', 'LOK_RECID');
    }

    public function formulaPayments() {
        return $this->hasMany(FormulaRedeemPayment::class, 'PAYR_RDM_RECID', $this->primaryKey);
    }

    public function formulaProducts()
    {
        return $this->hasMany(FormulaRedeemProduct::class, 'PRDR_RDM_RECID', $this->primaryKey);
    }

    public function formulaTenants()
    {
        return $this->hasMany(FormulaRedeemTenant::class, 'RDT_RDM_RECID', $this->primaryKey);
    }

    public function log()
    {
        return $this->hasMany(FormulaRedeemLog::class, 'RLOG_RDM_RECID', $this->primaryKey);
    }

}

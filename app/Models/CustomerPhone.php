<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPhone extends Model
{
    protected $table = 'Customers_Phone';

    protected $primaryKey = 'PHN_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'PHN_CUST_RECID', 'CUST_RECID');
    }
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $table = 'user_logs';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public $timestamps = true;

    public $incrementing = true;
}
?>
<?php

namespace App\Models;

use App\Models\Master\Lookup;
use Illuminate\Database\Eloquent\Model;

class CustomerAddr extends Model
{
    protected $table = 'Customers_Addr';

    protected $primaryKey = 'ADDR_RECID';

    public $timestamps = false;

    public $incrementing = false;

    protected $guarded = [];

    public function customer() {
        return $this->belongsTo(Customer::class, 'ADDR_CUST_RECID', 'CUST_RECID');
    }

    public function city(){
        return $this->belongsTo(Lookup::class, 'ADDR_CITY', 'LOK_RECID');
    }

    public function country(){
        return $this->belongsTo(Lookup::class, 'ADDR_COUNTRY', 'LOK_RECID');
    }

    public function area(){
        return $this->belongsTo(Lookup::class, 'ADDR_AREA', 'LOK_RECID');
    }

    public function province(){
        return $this->belongsTo(Lookup::class, 'ADDR_PROVINCE', 'LOK_RECID');
    }
}

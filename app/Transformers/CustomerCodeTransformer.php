<?php

namespace App\Transformers;

use App\Models\Customer;
use App\Models\Master\CardType;
use App\Models\Master\Lookup;
use App\Models\Master\Product;
use App\Models\Master\Tenant;
use League\Fractal\TransformerAbstract;

class CustomerCodeTransformer extends TransformerAbstract
{
	protected $date;
	protected $stores;

	public function __construct($date)
	{
		$this->date = $date;
		$this->stores = Tenant::all();
		$this->memberTypes = Lookup::where('LOK_CODE', 'MEMT')->get();
		$this->idTypes = Lookup::where('LOK_CODE', 'TPID')->get();
		$this->genders = Lookup::where('LOK_CODE', 'GEN')->get();
		$this->maritals = Lookup::where('LOK_CODE', 'MART')->get();
		$this->nationalities = Lookup::where('LOK_CODE', 'NATL')->get();
		$this->religions = Lookup::where('LOK_CODE', 'RELI')->get();
		$this->bloods = Lookup::where('LOK_CODE', 'BLOD')->get();
		$this->occupations = Lookup::where('LOK_CODE', 'OCCP')->get();
		$this->cardTypes = CardType::all();
		$this->countries = Lookup::where('LOK_CODE', 'COTR')->get();
		$this->provinces = Lookup::where('LOK_CODE', 'PROV')->get();
		$this->cities = Lookup::where('LOK_CODE', 'CITY')->get();
		$this->areas = Lookup::where('LOK_CODE', 'AREA')->get();
	}

	public function transform(Customer $customer)
	{
		$oldCardExchange = $customer->exchanges->first();

		$result = [
			'CUST_BARCODE' => $customer->CUST_BARCODE,
			'CUST_MEMTYPE' => (in_array($customer->CUST_MEMTYPE, $this->memberTypes->pluck('LOK_RECID')->toArray()))
								? $this->memberTypes->where('LOK_RECID', $customer->CUST_MEMTYPE)->first()->LOK_MAPPING
								: null,
			'CUST_NAME' => $customer->CUST_NAME,
			'CUST_IDTYPE' => (in_array($customer->CUST_IDTYPE, $this->idTypes->pluck('LOK_RECID')->toArray()))
								? $this->idTypes->where('LOK_RECID', $customer->CUST_IDTYPE)->first()->LOK_MAPPING
								: null,
			'CUST_IDCARDS' => $customer->CUST_IDCARDS,
			'CUST_GENDER' => (in_array($customer->CUST_GENDER, $this->genders->pluck('LOK_RECID')->toArray()))
								? substr($this->genders->where('LOK_RECID', $customer->CUST_GENDER)->first()->LOK_DESCRIPTION, 0, 1)
								: null,
			'CUST_MARITAL' => (in_array($customer->CUST_MARITAL, $this->maritals->pluck('LOK_RECID')->toArray()))
								? $this->maritals->where('LOK_RECID', $customer->CUST_MARITAL)->first()->LOK_MAPPING
								: null,
			'CUST_DOB' => $customer->CUST_DOB,
			'CUST_POD' => $customer->CUST_POD,
			'ADDRESS' => (!empty($customer->addressKtp->first()))
							? $customer->addressKtp->first()->ADDR_ADDRESS
							: null,
			'COUNTRY' => (!empty($customer->addressKtp->first()))
							? (
								(in_array($customer->addressKtp->first()->ADDR_COUNTRY, $this->countries->pluck('LOK_RECID')->toArray()))
								? $this->countries->where('LOK_RECID', $customer->addressKtp->first()->ADDR_COUNTRY)->first()->LOK_MAPPING
								: null
							) : null,
			'PROVINCE' => (!empty($customer->addressKtp->first()))
							? (
								(in_array($customer->addressKtp->first()->ADDR_PROVINCE, $this->provinces->pluck('LOK_RECID')->toArray()))
								? $this->provinces->where('LOK_RECID', $customer->addressKtp->first()->ADDR_PROVINCE)->first()->LOK_MAPPING
								: null
							) : null,
			'CITY' => (!empty($customer->addressKtp->first()))
							? (
								(in_array($customer->addressKtp->first()->ADDR_CITY, $this->cities->pluck('LOK_RECID')->toArray()))
								? $this->cities->where('LOK_RECID', $customer->addressKtp->first()->ADDR_CITY)->first()->LOK_MAPPING
								: null
							) : null,
			'AREA' => (!empty($customer->addressKtp->first()))
							? (
								(in_array($customer->addressKtp->first()->ADDR_AREA, $this->areas->pluck('LOK_RECID')->toArray()))
								? $this->areas->where('LOK_RECID', $customer->addressKtp->first()->ADDR_AREA)->first()->LOK_MAPPING
								: null
							) : null,
			'ZIP CODE' => (!empty($customer->addressKtp->first()))
							? $customer->addressKtp->first()->ADDR_POSTAL
							: null,
			'HOME' => (!empty($customer->homePhone))
						? $customer->homePhone->PHN_NUMBER
						: null,
			'OFFICE' => (!empty($customer->businessPhone))
						? $customer->businessPhone->PHN_NUMBER
						: null,
			'MOBILE' => (!empty($customer->mobilePhone))
						? $customer->mobilePhone->PHN_NUMBER
						: null,
			'EMAIL' => (!empty($customer->email))
						? $customer->email->EML_EMAIL
						: null,
			'CUST_NATIONALITY' => (in_array($customer->CUST_NATIONALITY, $this->nationalities->pluck('LOK_RECID')->toArray()))
								? $this->nationalities->where('LOK_RECID', $customer->CUST_NATIONALITY)->first()->LOK_MAPPING
								: null,
			'CUST_BLOOD' => (in_array($customer->CUST_BLOOD, $this->bloods->pluck('LOK_RECID')->toArray()))
								? $this->bloods->where('LOK_RECID', $customer->CUST_BLOOD)->first()->LOK_MAPPING
								: null,
			'CUST_RELIGION' => (in_array($customer->CUST_RELIGION, $this->religions->pluck('LOK_RECID')->toArray()))
								? $this->religions->where('LOK_RECID', $customer->CUST_RELIGION)->first()->LOK_MAPPING
								: null,
			'OLD_CARD_NUMBER' => $oldCardExchange ? $oldCardExchange->EXC_OLDCARD : null,
			'CUST_CARDTYPE' => (in_array($customer->CUST_CARDTYPE, $this->cardTypes->pluck('CARD_RECID')->toArray()))
								? $this->cardTypes->where('CARD_RECID', $customer->CUST_CARDTYPE)->first()->CARD_CODE
								: null,
			'CUST_STORE' => (in_array($customer->CUST_STORE, $this->stores->pluck('TNT_RECID')->toArray()))
								? $this->stores->where('TNT_RECID', $customer->CUST_STORE)->first()->TNT_CODE
								: null,
			'CUST_JOINDATE' => $customer->CUST_JOINDATE,
			'CUST_EXPIREDATE' => $customer->CUST_EXPIREDATE,
			'CUST_EXTENDATE' => $customer->CUST_EXTENDATE,
			'LATEST_POINT_BALANCE' => $customer->rdPoint ? $customer->rdPoint->point : 0,
			'CUST_SYSTEMDATE' => explode(' ', $customer->CUST_SYSTEMDATE)[0],
			'CUST_SYSTEMTIME' => '00:00:00',
			'EXCHANGE_TYPE' => $oldCardExchange ? $oldCardExchange->EXC_TYPE : ' '
		];

		return $result;
	}
}
